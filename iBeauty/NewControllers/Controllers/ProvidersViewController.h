//
//  ProvidersViewController.h
//  iBeauty
//
//  Created by App Innovation on 24/10/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProvidersViewController : UIViewController

@property (nonatomic,strong) NSMutableArray *serviceCategoryArray;
@property (nonatomic,strong) CLLocation *currentLocation;
@property (nonatomic,assign) NSInteger selectedIndex;

@end

NS_ASSUME_NONNULL_END
