//
//  ShowLocationController.m
//  iBeauty
//
//  Created by App Innovation on 14/11/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import "ShowLocationController.h"
#import "Constants.h"
#import "ShowLocationCell.h"
#import "ServicesCategoriesInfo.h"
#import "NSString+iBeautyString.h"
#import "ChangeLocationController.h"

@import MapKit;

static NSString *categoryCellIdentifier = @"ShowLocationCell";

@interface ShowLocationController ()<MKMapViewDelegate,UISearchBarDelegate,CLLocationManagerDelegate,ShowDetailDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic,strong) AppDelegate *delegate;
@property (nonatomic, weak) IBOutlet UICollectionView *aroundYouCollectionView;
@property (strong, nonatomic) NSMutableArray *providerArray;
@property (strong, nonatomic) NSMutableArray *mainArray;
@property (nonatomic,strong) CLLocation *currentLocation;
@property (nonatomic,strong) NSString *currentAddress;
@property (nonatomic,strong) NSString *selectedAddress;
@property (nonatomic,strong) CLLocation *selectedLocation;
@property (nonatomic,strong) NSString *selectedIndex;
@property (nonatomic, weak) IBOutlet UILabel *locationLabel;
@property (nonatomic, weak) IBOutlet UIView *bkgView;
@property (nonatomic, weak) IBOutlet UIView *containerView;

@end

@implementation ShowLocationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.delegate = [AppDelegate sharedInstance];
    [self setUpViews];
    [self setUpAroundCollectionCell];
    [self getProvidersListNearYou];
    [self addObserverToGetLocation];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.delegate.tabbarController.tabBar.hidden = YES;
}

#pragma mark - Set Up Views

- (void)setUpViews {
 
    self.currentLocation = self.delegate.currentLocation;
    if (self.delegate.selectedAddress.length > 0) {
        self.locationLabel.text = self.delegate.selectedAddress;
    }else {
        self.locationLabel.text = self.delegate.currentAddress;
    }
    self.currentAddress = self.delegate.currentAddress;
}

- (void)setUpAroundCollectionCell {
    
    UINib *collectionNib = [UINib nibWithNibName:@"ShowLocationCell" bundle:nil];
    [self.aroundYouCollectionView registerNib:collectionNib forCellWithReuseIdentifier:categoryCellIdentifier];
}

#pragma mark - Add Observer

-(void)addObserverToGetLocation {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getUserSelectedLocation:) name:@"GetUserSelectedLocation" object:nil];
}

#pragma mark - Add Annotation

- (void)getUserSelectedLocation:(NSNotification*)infoDict {
    
    NSDictionary *dictInfo = infoDict.userInfo;
    self.currentLocation = [dictInfo valueForKey:@"SelectedLocation"];
    self.selectedAddress = [dictInfo valueForKey:@"Address"];
    self.selectedIndex = [dictInfo valueForKey:@"SelectedIndex"];
    self.delegate.selectedAddress = self.selectedAddress;

    if ([self.selectedIndex isEqualToString:@"-1"]) {
        [self convertLatLngToString:self.delegate.currentLocation];
    }else {
        [self convertLatLngToString:self.currentLocation];
    }
    self.locationLabel.text = self.selectedAddress;
    [self getProvidersListNearYou];

}

- (void)convertLatLngToString:(CLLocation*)currentLocation {
    
    NSString *latitude = [NSString stringWithFormat:@"%@",[NSNumber numberWithDouble:currentLocation.coordinate.latitude]];
    NSString *longitude = [NSString stringWithFormat:@"%@",[NSNumber numberWithDouble:currentLocation.coordinate.longitude]];
    self.delegate.latitude = latitude;
    self.delegate.longitude = longitude;
    self.delegate.currentLocation = currentLocation;
    
}

#pragma mark - Back Action

-(IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark - Change Location

-(IBAction)changeLocation:(id)sender {
    
    ChangeLocationController *locationController = [self.storyboard instantiateViewControllerWithIdentifier:@"ChangeLocationController"];
    locationController.currentAddress = self.currentAddress;
    locationController.currentLocation = self.currentLocation;
    if (self.selectedIndex == nil) {
        self.selectedIndex = @"-1";
    }
    NSInteger selectedIndex = [self.selectedIndex integerValue];
    locationController.selectedIndex = selectedIndex;

    [self.navigationController pushViewController:locationController animated:YES];
    
}


#pragma mark - Set Map View Location

- (void)setUpMapViewLocation:(double)lat longitude:(double)log providerInfo:(ServicesCategoriesInfo*)providerInfo {
    
    self.mapView.delegate = self;
    [self.mapView setShowsUserLocation:YES];
    
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(lat,log);
    MKCoordinateSpan span = MKCoordinateSpanMake(0.1, 0.1);
    MKCoordinateRegion region = {coordinate, span};
    region = [self.mapView regionThatFits:region];
    [self.mapView setRegion:region];

    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    annotation.coordinate = coordinate;
    annotation.title = providerInfo.location;
    annotation.subtitle = providerInfo.categoryName;
    [self.mapView addAnnotation:annotation];
}


#pragma mark - Get Providers List Around You

- (void)getProvidersListNearYou {
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_SERVICE_PROVIDER_CATEGORIES];
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    NSString *businessId =  @"5ca590942a72b";
    NSString *currentDay =  [[NSString string] getWeekDay];
    NSString *latitude = [NSString stringWithFormat:@"%@",[NSNumber numberWithDouble:self.currentLocation.coordinate.latitude]];
    NSString *longitude = [NSString stringWithFormat:@"%@",[NSNumber numberWithDouble:self.currentLocation.coordinate.longitude]];
    
    NSMutableDictionary *dictInfo = [NSMutableDictionary dictionaryWithObjectsAndKeys:accessToken,@"Authorization",businessId,@"businessCatId",currentDay,@"dayName",latitude,@"Latitude",longitude,@"Longitude", nil];
    
    if (internetStatus != NotReachable) {
        
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonPostData:url params:dictInfo onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                @try {
                    if (completed) {
                        
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSMutableArray *tempInfoArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE) { NSLog(@"Temp Info Array is %@", tempInfoArray); }
                        
                        if ([tempInfoArray isKindOfClass:[NSMutableArray class]]) {
                            
                            ServicesCategoriesInfo *serviceCatObj = [ServicesCategoriesInfo getSharedInstance];
                            self.mainArray = [serviceCatObj serviceProvidersListInfoObject:tempInfoArray];
                            if (APP_DEBUG_MODE) { NSLog(@"Service Provider Array is %@", self.mainArray);}
                            self.providerArray = [self.mainArray mutableCopy];
                            [self setDefaultLocationForSaloon];
                            [self.aroundYouCollectionView reloadData];
                        }else {
                            [self.providerArray removeAllObjects];
                        }
                    }else {
                        if (APP_DEBUG_MODE) { NSLog(@"error is %@", error.localizedDescription);}
                        [SVProgressHUD dismiss];
                    }
                    
                } @catch (NSException *exception) {
                    //[EShopUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                    [SVProgressHUD dismiss];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}

- (void)setDefaultLocationForSaloon {
 
    if (self.providerArray.count > 0) {
        ServicesCategoriesInfo *providerInfo = self.providerArray.firstObject;
        [self setUpMapViewLocation:[providerInfo.latitude doubleValue] longitude:[providerInfo.longitude doubleValue] providerInfo:providerInfo];
    }
    
}

#pragma mark - On Zoom To Current Location

- (IBAction)onZoomToCurrentLocation:(id)sender {
    
    [iBeautyUtility zoomToUserLocationInMapView:self.mapView];
}

#pragma mark - UICollectionViewDataSource methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.providerArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ShowLocationCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:categoryCellIdentifier forIndexPath:indexPath];
    cell.bookButton.tag = indexPath.row;
    cell.delegate = self;
    ServicesCategoriesInfo *infoObj = self.providerArray[indexPath.row];
    [cell configureShowLocationCollectionCell:infoObj];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat screenWidth = UIScreen.mainScreen.bounds.size.width;
    
    CGFloat itemWidth = 0.0f;
    CGFloat itemHeight = 0.0f;
    
    NSString *deviceType = [UIDevice currentDevice].model;
    
    if ([deviceType isEqualToString:@"iPhone"]) {
        itemWidth = floor(screenWidth - 100.0f);
        itemHeight = 235.0f;
    }else {
        itemWidth = floor(screenWidth -400.0f);
        itemHeight = 385.0f;
    }
    
    return CGSizeMake(itemWidth, itemHeight);
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 5.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 5.0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(5.0, 5.0, 5.0, 5.0);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.mapView.annotations) {
        [self.mapView removeAnnotations:self.mapView.annotations];
    }
    [self addSaloonAnnotationOnMapView:self.providerArray[indexPath.row]];
}

- (void)addSaloonAnnotationOnMapView:(ServicesCategoriesInfo*)providerInfo {
 
    double lat = [providerInfo.latitude doubleValue];
    double lng = [providerInfo.longitude doubleValue];
    CLLocation *saloonLoc = [[CLLocation alloc]initWithLatitude:lat longitude:lng];
    
    CLLocationCoordinate2D coordinate = saloonLoc.coordinate;
    
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    annotation.coordinate = coordinate;
    annotation.title = providerInfo.location;
    annotation.subtitle = providerInfo.categoryName;
    [self.mapView addAnnotation:annotation];
    
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(coordinate, 10000.0f, 10000.0f);
    [self.mapView regionThatFits:region];
    [self.mapView setRegion:region animated:YES];
    self.mapView.delegate = self;
    self.mapView.centerCoordinate = coordinate;
    
}

#pragma mark - Search delegate methods

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    if (self.providerArray.count>0) {
        [self.providerArray removeAllObjects];
    }
    if (searchText.length>0) {
        for (ServicesCategoriesInfo *infoObj in self.mainArray) {
            
            if([[infoObj.categoryName uppercaseString] rangeOfString:[searchText uppercaseString]].location != NSNotFound){
                
                if( ![self.providerArray containsObject:infoObj])
                    [self.providerArray addObject:infoObj];
            }
        }
        [self setUpViewsAfterFilter];
    }
}

- (void)setUpViewsAfterFilter {
 
    if (self.providerArray.count > 0) {
        self.bkgView.hidden = NO;
        self.containerView.hidden = NO;
        [self.aroundYouCollectionView reloadData];
    }else {
        self.bkgView.hidden = YES;
        self.containerView.hidden = YES;
    }

}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    // called twice every time
    [searchBar setShowsCancelButton:YES animated:YES];
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    // called only once
    [searchBar setShowsCancelButton:YES animated:YES];
    return YES;
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    
    [searchBar resignFirstResponder];
    [self setUpProviderArray];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
    [searchBar resignFirstResponder];
}

- (void)setUpProviderArray {
 
    self.providerArray = [self.mainArray mutableCopy];
    [self setUpViewsAfterFilter];
}

#pragma mark - Business Detail Delegate

- (void)showBusinessDetail:(NSInteger)SenderTag {
    
    if (APP_DEBUG_MODE) {
        NSLog(@"Sender Tag is %ld",(long)SenderTag);
    }
    
    ServicesCategoriesInfo *infoObj = self.providerArray[SenderTag];
    CategoryDetailController *detailController = [self.storyboard instantiateViewControllerWithIdentifier:kCategoryDetailController];
    detailController.categoryInfoObj = infoObj;
    detailController.currentLocation = self.currentLocation;
    [self.navigationController pushViewController:detailController animated:YES];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
