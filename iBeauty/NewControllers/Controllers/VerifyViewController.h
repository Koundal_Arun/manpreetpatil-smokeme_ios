//
//  VerifyViewController.h
//  iBeauty
//
//  Created by Arun on 16/08/20.
//  Copyright © 2020 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>


NS_ASSUME_NONNULL_BEGIN

@interface VerifyViewController : UIViewController

@property (nonatomic,strong) NSDictionary *otpInfoDict;

@end

NS_ASSUME_NONNULL_END
