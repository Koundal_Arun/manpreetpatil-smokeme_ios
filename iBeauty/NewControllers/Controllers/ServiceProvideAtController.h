//
//  ServiceProvideAtController.h
//  iBeauty
//
//  Created by App Innovation on 19/11/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"

NS_ASSUME_NONNULL_BEGIN

@interface ServiceProvideAtController : UIViewController

@property (nonatomic,strong) ServicesCategoriesInfo *categoryInfoObj;
@property (nonatomic,strong) OffersDetail *offerInfoObj;
@property (nonatomic,assign) BOOL screenStatus;
@property (nonatomic,strong) NSMutableArray  *basketInfoArray;
@property (nonatomic,strong) NSString * basketID;

@end

NS_ASSUME_NONNULL_END
