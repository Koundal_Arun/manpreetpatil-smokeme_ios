//
//  ForgotPasswordController.m
//  iBeauty
//
//  Created by Arun on 16/08/20.
//  Copyright © 2020 App Innovation. All rights reserved.
//

#import "ForgotPasswordController.h"
#import "iBeautyUtility.h"
#import "Constants.h"
#import "VerifyViewController.h"


@import SkyFloatingLabelTextField;

@interface ForgotPasswordController ()<UITextFieldDelegate>

@property (nonatomic,weak)  IBOutlet SkyFloatingLabelTextField *emailTextField;
@property (nonatomic,weak)  IBOutlet UILabel *enterEmailLabel;
@property (nonatomic,weak)  IBOutlet UILabel *otpTitleLabel;
@property (nonatomic,weak)  IBOutlet UIButton *sendBtn;
@property (nonatomic,weak)  IBOutlet UIView *containerView;

@end

@implementation ForgotPasswordController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupViews];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:true];
    [self.navigationController.navigationBar setHidden:NO];
}

- (void)setupViews {
    
    [iBeautyUtility setButtonCorderRound:self.sendBtn];
    [self setStatusBarColor];
}

#pragma mark - Set Status Bar Color

- (void)setStatusBarColor {

    if (@available(iOS 13.0, *)) {
        
        UIView *statusBar = [[UIView alloc]initWithFrame:[UIApplication sharedApplication].keyWindow.windowScene.statusBarManager.statusBarFrame] ;
        [self.view addSubview:statusBar];
        if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
            statusBar.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:130/255.0 blue:90/255.0 alpha:1.0];
        }
    }else {
            UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
            if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
                statusBar.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:130/255.0 blue:90/255.0 alpha:1.0];
            }
    }

}


#pragma mark - Action Methods

-(IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)sendAction:(UIButton*)sender {
    
    BOOL validStatus =  [self checkValidationForForGotPassword];
    
    if (validStatus == YES) {
        
        [self sendForgotCodeToEmail];
    }
    
}

- (void)sendForgotCodeToEmail {
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_FORGOT_PASSWORD];
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSDictionary * parametersDict = [NSDictionary dictionaryWithObjectsAndKeys:self.emailTextField.text,@"email", nil];
    if (APP_DEBUG_MODE) { NSLog(@"parametersDict is %@",parametersDict); }
    
    if (internetStatus != NotReachable) {
        
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonPostData:url params:parametersDict onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                
                @try {
                    if (completed) {
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSDictionary *infoDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE)
                            NSLog(@"Info is %@", infoDict);
                        [self parseResponseInfoDict:infoDict];
                    }else {
                        if (APP_DEBUG_MODE)
                            NSLog(@"error is %@", error.localizedDescription);
                        [SVProgressHUD dismiss];
                    }
                    
                } @catch (NSException *exception) {
                    //[iBeautyUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
    
}


- (void)parseResponseInfoDict:(NSDictionary*)responseDict {
    
    if ([responseDict valueForKey:@"TempCode"] != nil) {
        
        [self switchToverityOTPScreen:responseDict];
        
    }else {
        [iBeautyUtility showAlertMessage:[responseDict valueForKey:@"message"]];
    }
    
}

-(void)switchToverityOTPScreen:(NSDictionary*)dataInfo {
    
    VerifyViewController *verifyVC = [self.storyboard instantiateViewControllerWithIdentifier:kVerifyViewController];
    verifyVC.otpInfoDict = dataInfo;
    [self.navigationController pushViewController:verifyVC animated:YES];
    
}


#pragma mark - check Validations For Forgot Email

- (BOOL)checkValidationForForGotPassword {
    
    if (self.emailTextField.text.length > 0  && ![iBeautyUtility isValidEmailAddress:self.emailTextField.text]) {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"Please enter valid email address", @"")];
        return NO ;
    }
    if (self.emailTextField.text.length < 1) {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"Please enter email address", @"")];
        return NO ;
    }
    
    return YES;
}

#pragma mark - Textfield Delegate Methods

-(BOOL) textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
