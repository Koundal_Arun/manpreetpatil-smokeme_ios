//
//  VerifyViewController.m
//  iBeauty
//
//  Created by Arun on 16/08/20.
//  Copyright © 2020 App Innovation. All rights reserved.
//

#import "VerifyViewController.h"
#import "Constants.h"


@interface VerifyViewController ()<UITextFieldDelegate>

@property (nonatomic,weak)  IBOutlet UIButton *verifyBtn;
@property (nonatomic,weak)  IBOutlet UIButton *sendAgainBtn;
@property (nonatomic,weak)  IBOutlet UIView *containerView;

@property (nonatomic,weak)  IBOutlet UIView *pinView;

@property (nonatomic,weak)  IBOutlet UITextField *textField_1;
@property (nonatomic,weak)  IBOutlet UITextField *textField_2;
@property (nonatomic,weak)  IBOutlet UITextField *textField_3;
@property (nonatomic,weak)  IBOutlet UITextField *textField_4;

@property (nonatomic,strong) NSString *enteredOTPString;

@property (nonatomic,strong) NSString *txtFStr_1;
@property (nonatomic,strong) NSString *txtFStr_2;
@property (nonatomic,strong) NSString *txtFStr_3;
@property (nonatomic,strong) NSString *txtFStr_4;


@end

@implementation VerifyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.verifyBtn.enabled = NO;
    [self.verifyBtn setBackgroundColor:[UIColor lightGrayColor]];
    [self setupViews];
    [self setupToolBarOnPinView];
}

- (void)viewWillAppear:(BOOL)animated {
 
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden = YES;
}

- (void)viewDidAppear:(BOOL)animated {
 
    [super viewDidAppear:YES];
}

- (void)setupViews {
    
    [iBeautyUtility setButtonCorderRound:self.verifyBtn];
    [self.textField_1 becomeFirstResponder];
    [self setStatusBarColor];
}

#pragma mark - Set Status Bar Color

- (void)setStatusBarColor {

    if (@available(iOS 13.0, *)) {
        
        UIView *statusBar = [[UIView alloc]initWithFrame:[UIApplication sharedApplication].keyWindow.windowScene.statusBarManager.statusBarFrame] ;
        [self.view addSubview:statusBar];
        if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
            statusBar.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:130/255.0 blue:90/255.0 alpha:1.0];
        }
    }else {
            UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
            if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
                statusBar.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:130/255.0 blue:90/255.0 alpha:1.0];
            }
    }

}

#pragma mark - Send OTP Again

- (IBAction)sendOTPCodeAgain:(id)sender {
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_FORGOT_PASSWORD];
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSDictionary * parametersDict = [NSDictionary dictionaryWithObjectsAndKeys:[self.otpInfoDict valueForKey:@"Email"],@"email", nil];
    if (APP_DEBUG_MODE) { NSLog(@"parametersDict is %@",parametersDict); }
    
    if (internetStatus != NotReachable) {
        
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonPostData:url params:parametersDict onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                
                @try {
                    if (completed) {
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSDictionary *infoDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE)
                            NSLog(@"Info is %@", infoDict);
                        [self parseResponseInfoDict:infoDict];
                    }else {
                        if (APP_DEBUG_MODE)
                            NSLog(@"error is %@", error.localizedDescription);
                        [SVProgressHUD dismiss];
                    }
                    
                } @catch (NSException *exception) {
                    //[iBeautyUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
    
}


- (void)parseResponseInfoDict:(NSDictionary*)responseDict {
    
    if ([responseDict valueForKey:@"TempCode"] != nil) {
        
        self.otpInfoDict = responseDict;
        
        [iBeautyUtility showAlertMessage:@"OTP has been send again"];

    }else {
        [iBeautyUtility showAlertMessage:[responseDict valueForKey:@"message"]];
    }
    
}

#pragma mark - Verify OTP


-(IBAction)verifyAction:(UIButton*)sender {
    
    self.enteredOTPString = [NSString stringWithFormat:@"%@%@%@%@",self.txtFStr_1,self.txtFStr_2,self.txtFStr_3,self.txtFStr_4];
    if (self.enteredOTPString.length == 4) {
        
        NSString *otpStr = [NSString stringWithFormat:@"%@",[self.otpInfoDict valueForKey:@"TempCode"]];
        if ([self.enteredOTPString isEqualToString:otpStr]) {
            [self openResetPasswordScreen];
            
        }else {
            [iBeautyUtility showAlertMessage:@"Please enter correct OTP"];
        }
        
    }else {
        [iBeautyUtility showAlertMessage:@"Please enter correct OTP"];
    }
    
}

- (void)openResetPasswordScreen {
    
    ResetPasswordController *controller = [self.storyboard instantiateViewControllerWithIdentifier:kResetPassController];
    controller.infoDict = self.otpInfoDict;
    [self.navigationController pushViewController:controller animated:YES];
    
}


#pragma mark - set up Tool Bar On Text View

- (void)setupToolBarOnPinView {
    
    UIToolbar *toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50)];
    toolBar.barStyle = UIBarStyleDefault;
    [toolBar sizeToFit];
    [toolBar setBackgroundColor:[UIColor lightGrayColor]];

    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", @"") style:UIBarButtonItemStyleDone target:self action:@selector(doneAction:)];
    
    UIBarButtonItem *flexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar setItems:@[flexibleItem,doneButton] animated:NO];
    
    self.textField_1.inputAccessoryView = toolBar;
    self.textField_2.inputAccessoryView = toolBar;
    self.textField_3.inputAccessoryView = toolBar;
    self.textField_4.inputAccessoryView = toolBar;

}

- (void)doneAction:(id)sender {
    
    [self.view endEditing:YES];
}

#pragma mark - Textfield Delegate Methods

-(BOOL) textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    
    return YES;

}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField == self.textField_1) {
        
        if (string.length == 1) {
            self.textField_1.text = string;
            self.txtFStr_1 = string;
            [self.textField_1 resignFirstResponder];
            [self.textField_2 becomeFirstResponder];
        }else {
            self.textField_1.text = @"";
            [self.textField_1 becomeFirstResponder];
        }
    }
    
    if (textField == self.textField_2) {
        
        if (string.length == 1) {
            self.textField_2.text = string;
            self.txtFStr_2 = string;
            [self.textField_2 resignFirstResponder];
            [self.textField_3 becomeFirstResponder];
        }else {
            self.textField_2.text = @"";
            self.textField_1.text = self.txtFStr_1;
            [self.textField_2 resignFirstResponder];
            [self.textField_1 becomeFirstResponder];
        }
    }
    
    if (textField == self.textField_3) {
        
        if (string.length == 1) {
            self.textField_3.text = string;
            self.txtFStr_3 = string;
            [self.textField_3 resignFirstResponder];
            [self.textField_4 becomeFirstResponder];
        }else {
            self.textField_3.text = @"";
            self.textField_2.text = self.txtFStr_2;
            [self.textField_3 resignFirstResponder];
            [self.textField_2 becomeFirstResponder];
        }
    }
    
    if (textField == self.textField_4) {
        
        if (string.length == 1) {
            self.textField_4.text = string;
            self.txtFStr_4 = string;
            [self.textField_4 resignFirstResponder];
            self.verifyBtn.enabled = YES;
            [self.verifyBtn setBackgroundColor:[UIColor iBeautyThemeColor]];
        }else {
            self.textField_4.text = @"";
            self.textField_3.text = self.txtFStr_3;
            [self.textField_4 resignFirstResponder];
            [self.textField_3 becomeFirstResponder];
        }
    }
    
    return YES;

}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
