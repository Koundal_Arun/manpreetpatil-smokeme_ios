//
//  AddNewAddressController.m
//  iBeauty
//
//  Created by App Innovation on 07/11/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import "AddNewAddressController.h"
#import "SearchAddressViewController.h"

@interface AddNewAddressController ()<UITextFieldDelegate,UITextViewDelegate>

@property (nonatomic,weak) IBOutlet UITextField  *userNameTextField;
@property (nonatomic,weak) IBOutlet UITextField  *contactTextField;
@property (nonatomic,weak) IBOutlet UITextView   *addressTextView;
@property (nonatomic,weak) IBOutlet UILabel     *placeHolderLabel;

@property (nonatomic,weak) IBOutlet UIButton    *pinLocButton;

@property (nonatomic,weak) IBOutlet UIImageView  *homeImageView;
@property (nonatomic,weak) IBOutlet UIImageView  *workImageView;
@property (nonatomic,weak) IBOutlet UIImageView  *otherImageView;

@property (nonatomic,weak) IBOutlet UIButton     *saveButton;
@property (nonatomic,weak)  IBOutlet UINavigationBar *navBar;
@property (nonatomic, strong) NSString *latString;
@property (nonatomic, strong) NSString *longString;
@property (nonatomic, strong) NSString *addressTitle;
@property (nonatomic,strong)  AppDelegate *delegate;
@property(nonatomic,assign) BOOL pinLocStatus;

@end

@implementation AddNewAddressController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.delegate = [AppDelegate sharedInstance];

    [self setUpViews];
    //[self setGradient];
    [self addObserver];
}

- (void)setUpViews {
    
    self.saveButton.frame = [iBeautyUtility setUpFrame:self.saveButton.frame];
    [iBeautyUtility setButtonCorderRound:self.saveButton];
    //[self.saveButton.layer insertSublayer:[iBeautyUtility setGradientColor_Buttons:self.saveButton] atIndex:0];
    
    [self setUpAddressTitleImageViews];

}

- (void)setUpAddressTitleImageViews {
    
    if (self.modeStatus == YES) {
        
        self.addressTitle =  self.addressInfoObj.addressTitle;
        if ([self.addressTitle isEqualToString : @"Home"]) {
            [self setHomeImageView];
        }else if ([self.addressTitle isEqualToString : @"Work"]) {
            [self setWorkImageView];
        }else {
            [self setOtherImageView];
        }
        [self configureViewsInEditMode];
    }else {
        [self setHomeImageView];
//        [self getServiceProviderLocation:[self.delegate.latitude doubleValue] longitude:[self.delegate.longitude doubleValue]];
        [self setPinLocButtonTitle:@"Pin Your Location"];
    }
    
}

#pragma mark - Set Up Views In Edit Mode

- (void)configureViewsInEditMode {
    
    self.userNameTextField.text = self.addressInfoObj.name;
    self.addressTextView.text = self.addressInfoObj.address;
    self.contactTextField.text = self.addressInfoObj.contactNumber;
    if (![self.addressInfoObj.latitude isEqualToString:@"0.0"]) {
        [self setPinLocButtonTitle:@"Location marked"];
        self.latString = self.addressInfoObj.latitude;
        self.longString = self.addressInfoObj.longitude;
    }else {
        self.latString = @"0.0";
        self.longString = @"0.0";
    }
    if(self.addressTextView.text > 0) {
        self.placeHolderLabel.hidden = YES;
    }
    self.pinLocStatus = YES;
}

#pragma mark - Set Gradient

- (void)setGradient {
    
    self.navBar.frame = [iBeautyUtility setUpFrame:self.navBar.frame];
    CAGradientLayer *gradient = [iBeautyUtility setNavigationGradient:self.navBar];
    [self.navBar setBackgroundImage:[iBeautyUtility imageFromLayer:gradient] forBarMetrics:UIBarMetricsDefault];
}

#pragma mark - Add Observer

-(void)addObserver {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addAnnotationOnMap:) name:@"SendSelectedLocation" object:nil];
    
}

#pragma mark - Add Annotation

- (void)addAnnotationOnMap:(NSNotification*)infoDict {
    
    NSDictionary *dictInfo = infoDict.userInfo;
    self.selectedLocation = [dictInfo valueForKey:@"SelectedLocation"];
    self.pinLocStatus = YES;
    [self getServiceProviderLocation:self.selectedLocation.coordinate.latitude longitude:self.selectedLocation.coordinate.longitude];
    [self setPinLocButtonTitle:@"Location marked"];
}

- (void)setPinLocButtonTitle:(NSString*)title {

    NSMutableAttributedString *titleString = [[NSMutableAttributedString alloc] initWithString:title];
    [titleString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [titleString length])];
    [titleString addAttribute:NSForegroundColorAttributeName value:[UIColor iBeautyThemeColor] range:NSMakeRange(0, [titleString length])];
    [self.pinLocButton setAttributedTitle:titleString forState:UIControlStateNormal];

}

- (void)getServiceProviderLocation:(double)lat longitude:(double)log {

    self.latString = [NSString stringWithFormat:@"%@",[NSNumber numberWithDouble:lat]];
    self.longString = [NSString stringWithFormat:@"%@",[NSNumber numberWithDouble:log]];
}

- (IBAction)backButtonAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)pinLocationAction:(id)sender {
    
    SearchAddressViewController *addressView = [self.storyboard instantiateViewControllerWithIdentifier:kSearchAddressController];
    if (self.addressInfoObj != nil) {
        addressView.addressInfoObj = self.addressInfoObj;
    }
    addressView.latitude = self.latString;
    addressView.longitude = self.longString;
    addressView.modeStatus = self.pinLocStatus;
    [self.navigationController pushViewController:addressView animated:YES];
}

- (IBAction)selectAddressType:(UIButton*)sender {
    
    if (sender.tag == 0) {
        
        [self setHomeImageView];
        
    }else if (sender.tag == 1) {
        
        [self setWorkImageView];
    }else {
        [self setOtherImageView];
    }
    
}

- (void)setHomeImageView {
    
    self.addressTitle = @"Home";
    [self.homeImageView setImage:[UIImage imageNamed:@"Check"]];
    [self.workImageView setImage:[UIImage imageNamed:@"Uncheck"]];
    [self.otherImageView setImage:[UIImage imageNamed:@"Uncheck"]];
}

- (void)setWorkImageView {
    
    self.addressTitle = @"Work";
    [self.homeImageView setImage:[UIImage imageNamed:@"Uncheck"]];
    [self.workImageView setImage:[UIImage imageNamed:@"Check"]];
    [self.otherImageView setImage:[UIImage imageNamed:@"Uncheck"]];
}

- (void)setOtherImageView {
    
    self.addressTitle = @"Other";
    [self.homeImageView setImage:[UIImage imageNamed:@"Uncheck"]];
    [self.workImageView setImage:[UIImage imageNamed:@"Uncheck"]];
    [self.otherImageView setImage:[UIImage imageNamed:@"Check"]];
}

- (IBAction)saveAddressAction:(id)sender {
    
    BOOL validStatus =  [self checkValidationForAddAddress];
    if (self.modeStatus == YES) {
        if (validStatus == YES) {
            [self.view endEditing:YES];
            [self UdateExistingAddressTo_registered_Users];
        }else {
            return;
        }
    }else {
        if (validStatus == YES) {
            [self.view endEditing:YES];
            [self addNewAddressTo_registered_Users];
        }else {
            return;
        }
    }
    
}

#pragma mark - check Validation For Address Parameters

- (BOOL)checkValidationForAddAddress {
    
    
    if (self.userNameTextField.text.length < 1) {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"Please enter name", @"")];
        [self.userNameTextField becomeFirstResponder];
        return NO ;
    }

    if (self.addressTextView.text.length < 1) {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"Please enter address", @"")];
        [self.addressTextView becomeFirstResponder];
        return NO ;
    }
    if (self.contactTextField.text.length < 1) {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"Please enter phone number", @"")];
        [self.contactTextField becomeFirstResponder];
        return NO ;
    }
    if (self.latString.length < 1 || self.longString.length < 1) {
        self.latString = @"0.0";
        self.longString = @"0.0";
    }
    return YES;
    
}

#pragma mark - Udate Existing Address To Registered Users

- (void)UdateExistingAddressTo_registered_Users {
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_UPDATE_ADDRESS];
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSDictionary *parametersDict   = [self getParamerterDictionary_for_RegisteredUser];
    
    if (internetStatus != NotReachable) {
        [SVProgressHUD showWithStatus:KLoadingMessage];
        
        [handler jsonPostData:url params:parametersDict onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                @try {
                    if (completed) {
                        
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSDictionary *tempInfoDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE) { NSLog(@"Temp Info Dict is %@", tempInfoDict); }
                        NSString *userID = [tempInfoDict valueForKey:@"UserId"];
                        
                        if (userID.length>0) {
                            [self backToView];
                        }else {
//                                                        NSArray *array = [completed valueForKey:@"error"];
//                                                        [iBeautyUtility showAlertMessage:array[0]];
                        }
                    }
                } @catch (NSException *exception) {
                    //[EShopUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}

#pragma mark - add New Address To Registered Users

- (void)addNewAddressTo_registered_Users {
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_ADD_ADDRESS];
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSDictionary *parametersDict   = [self getParamerterDictionary_for_RegisteredUser];
    
    if (internetStatus != NotReachable) {
        [SVProgressHUD showWithStatus:KLoadingMessage];
        
        [handler jsonPostData:url params:parametersDict onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                @try {
                    if (completed) {
                        NSError *jsonError;
                        NSData *objectData = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData options:NSJSONReadingMutableContainers error:&jsonError];
                        NSString *userId = [json valueForKey:@"Id"];
                        if (userId.length>0) {
                            self.addressInfoObj = [AddressModel getObjectWithJSONInfo:json screenStatus:NO];
                            [self backToView];
                        }else {
                            [iBeautyUtility showAlertMessage:NSLocalizedString(@"Something went wrong", @"")];
                        }
                    }
                } @catch (NSException *exception) {
                } @finally {
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}

#pragma mark - get Paramerter Dictionary

- (NSDictionary*)getParamerterDictionary_for_RegisteredUser {
    
    NSString *addressId ;
    if (self.modeStatus == YES) {
        addressId = self.addressInfoObj.address_Id;
    }
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    NSDictionary *profileInfo = [[NSUserDefaults standardUserDefaults]valueForKey:KLoginInfo];
    NSString *userID = [profileInfo valueForKey:@"UserID"];
    
    NSDictionary *userDetailDict = [[NSDictionary alloc]init];
    
    NSString *nameStr = [iBeautyUtility trimString:self.userNameTextField.text];
    NSString *contactNoStr = [iBeautyUtility trimString:self.contactTextField.text];
    NSString *addressStr = [iBeautyUtility trimString:self.addressTextView.text];
    NSString *emailStr = @"";
    if ([profileInfo valueForKey:@"email"]  != nil && ![[profileInfo valueForKey:@"email"] isEqual:[NSNull null]]) {
        emailStr = [iBeautyUtility trimString:[profileInfo valueForKey:@"email"]];
    }
    
    if (self.modeStatus == YES) {
        
        userDetailDict = [NSDictionary dictionaryWithObjectsAndKeys:accessToken,@"Authorization",addressId,@"Id", userID,@"UserId",nameStr,@"UserName",contactNoStr,@"ContactNumber",addressStr,@"Address",self.addressTitle,@"AddressTitle",self.latString,@"Latitude",self.longString,@"Longitude",emailStr,@"Email",nil];
    }else {
        
        userDetailDict = [NSDictionary dictionaryWithObjectsAndKeys:accessToken,@"Authorization",userID,@"UserId",nameStr,@"UserName",contactNoStr,@"ContactNumber",addressStr,@"Address",self.addressTitle,@"AddressTitle",self.latString,@"Latitude",self.longString,@"Longitude",emailStr,@"Email", nil];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:userDetailDict forKey:@"userShippingAddress"];
    
    return userDetailDict;
}

- (void)backToView {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}


#pragma mark UITextField Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString *currentString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSInteger length = [currentString length];
    
    if (textField == self.contactTextField) {
        if (length > phoneNumberMaxLength ) {
            return NO;
        }
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return  YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    return YES;
}

#pragma mark - Text view delegates

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    
    if (![self.addressTextView hasText]) {
        self.placeHolderLabel.hidden = NO;
    }
}

- (void) textViewDidChange:(UITextView *)textView
{
    if(![self.addressTextView hasText]) {
        self.placeHolderLabel.hidden = NO;
    }
    else{
        self.placeHolderLabel.hidden = YES;
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
