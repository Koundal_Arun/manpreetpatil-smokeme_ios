//
//  MoreViewController.m
//  iBeauty
//
//  Created by App Innovation on 23/10/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import "MoreViewController.h"
#import "Constants.h"
#import <SafariServices/SafariServices.h>


static NSString *cellIdentifier = @"menuListCell";

@interface MoreViewController ()<UITableViewDelegate,UITableViewDataSource,SFSafariViewControllerDelegate>

@property(nonatomic,strong) NSString *currentIdentifier;
@property(nonatomic,strong) NSArray *menuListArray;
@property(nonatomic,weak)   IBOutlet UITableView *menuListTableView;
@property (nonatomic, assign) NSInteger   intSelectedIndex;
@property (nonatomic,weak)  IBOutlet UINavigationBar *navBar;

@end

@implementation MoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.menuListArray = [NSArray new];
    [self loadMenuData];
    [self setUpTableView];
    //[self setGradient];

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.tabBarController.tabBar.hidden = NO;
}

#pragma mark - Load Menu Data

- (void)loadMenuData {
    
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"MenuList" ofType:@"plist"];
    self.menuListArray = [NSArray arrayWithContentsOfFile:plistPath];
    //    NSLog(@"array is %@",self.menuListArray);
}

#pragma mark - Set Gradient

- (void)setGradient {
    
    self.navBar.frame = [iBeautyUtility setUpFrame:self.navBar.frame];
    CAGradientLayer *gradient = [iBeautyUtility setNavigationGradient:self.navBar];
    [self.navBar setBackgroundImage:[iBeautyUtility imageFromLayer:gradient] forBarMetrics:UIBarMetricsDefault];
    
}

#pragma mark - Set Table View

- (void)setUpTableView {
    
    UINib *cellNib = [UINib nibWithNibName:@"MenuListCell" bundle:nil];
    [self.menuListTableView registerNib:cellNib forCellReuseIdentifier:cellIdentifier];

}

#pragma mark - View Profile Method

- (void)viewProfile:(UIButton*)sender {
    
    MyProfileViewController *viewProfile = [self.storyboard instantiateViewControllerWithIdentifier:kMyProfileViewController];
    [self.navigationController pushViewController:viewProfile animated:YES];
    
}


#pragma mark - UITableViewDelegate


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.menuListArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MenuListCell *menuListCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    NSDictionary *infoDict = self.menuListArray[indexPath.row];
    [menuListCell configureMenuListCell:infoDict rowIndex:indexPath.row savedIndex:self.intSelectedIndex];
    return menuListCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *deviceType = [UIDevice currentDevice].model;
    
    if ([deviceType isEqualToString:@"iPhone"]) {
        return 65.0f;
    }else {
        return 80.0f;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    self.intSelectedIndex = indexPath.row;
    [tableView reloadData];
    NSDictionary *infoDict = self.menuListArray[indexPath.row];
    NSString *command = [infoDict valueForKey:@"command"];
    
    if ([command isEqualToString:@"MyAppointments"]) {
        
        MyAppointmentViewController *appointmentView = [self.storyboard instantiateViewControllerWithIdentifier:kMyAppointmentViewController];
        [self.navigationController pushViewController:appointmentView animated:YES];
        
    }else if ([command isEqualToString:@"ReferAndEarn"]) {
        
        ReferEarnViewController *referView = [self.storyboard instantiateViewControllerWithIdentifier:kRegerEarnViewController];
        [self.navigationController pushViewController:referView animated:YES];
        
    }else if ([command isEqualToString:@"Settings"]) {
        
        SettingsViewController *settingView = [self.storyboard instantiateViewControllerWithIdentifier:kSettingsController];
        [self.navigationController pushViewController:settingView animated:YES];
        
    }else if ([command isEqualToString:@"Feedback"]) {
        
        FeedBackController *feedBackView = [self.storyboard instantiateViewControllerWithIdentifier:kFeedBackController];
        [self.navigationController pushViewController:feedBackView animated:YES];
        
    }else if ([command isEqualToString:@"FAQ"]) {
        
        FAQViewController *faqView = [self.storyboard instantiateViewControllerWithIdentifier:kFAQController];
        [self.navigationController pushViewController:faqView animated:YES];
        
    }else {
        [self openWebsiteUrl];
    }
    
}

- (void)openWebsiteUrl {
    
    SFSafariViewController *svc = [[SFSafariViewController alloc] initWithURL:[NSURL URLWithString:@"http://ibeautyadmin.appinnovation.co.uk"]];
    svc.delegate = self;
    svc.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:svc animated:YES completion:nil];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
