//
//  NearbyViewController.m
//  iBeauty
//
//  Created by App Innovation on 23/10/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import "NearbyViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "AllMassagesCell.h"
#import "NearByAddressHeader.h"
#import "NSString+iBeautyString.h"
#import "ChangeLocationController.h"
#import "ShowLocationController.h"

static NSString *providerCellIdenfier = @"allCategoryCell";

@interface NearbyViewController ()<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,CLLocationManagerDelegate>

@property (nonatomic,weak)   IBOutlet UITableView *providerTableView;
@property (nonatomic,strong) NSMutableArray *providerArray;
@property (nonatomic,strong) NSMutableArray *mainArray;
@property (nonatomic,strong) AppDelegate *delegate;
@property (nonatomic,strong) CLLocation *currentLocation;
@property (nonatomic,strong) NSString *currentAddress;
@property (nonatomic,strong) NSString *selectedAddress;
@property (nonatomic,strong) CLLocation *selectedLocation;
@property (nonatomic,strong) NSString *selectedIndex;

@property (nonatomic,strong) NSString *latitude;
@property (nonatomic,strong) NSString *longitude;
@property (nonatomic,strong) CLLocationManager *locationManager;

@property (nonatomic,weak)  IBOutlet UIButton *showLocation;
@property (nonatomic,weak)  IBOutlet UIView *noProviderView;
@property (nonatomic,weak)  IBOutlet UILabel *noProviderLabel;

@end

@implementation NearbyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.delegate = [AppDelegate sharedInstance];
    self.providerTableView.hidden = YES;
    [self setUpTableViewCell];
    [self addObserverToGetLocation];
    [self setUpViews];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    self.delegate.tabbarController.tabBar.hidden = NO;
    if (self.currentLocation == nil) {
        [self configureLocationManager];
    }else {
        [self getProvidersListNearYou];
    }
}

- (void)setUpViews {
 
    if (self.delegate.selectedAddress.length > 0) {
        self.selectedAddress = self.delegate.selectedAddress;
    }else {
        self.selectedAddress = self.delegate.currentAddress;
    }
    self.currentLocation = self.delegate.currentLocation;
    self.currentAddress = self.delegate.currentAddress;
}

#pragma mark - Configure Location Manager

-(void)configureLocationManager {
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        [self.locationManager requestWhenInUseAuthorization];
    
    [self.locationManager startUpdatingLocation];
    
}

#pragma mark - Update Location

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    self.currentLocation = locations.lastObject;
     
    [self convertLatLngToString:self.currentLocation];
    
    self.latitude = [NSString stringWithFormat:@"%@",[NSNumber numberWithDouble:self.currentLocation.coordinate.latitude]];
    self.longitude = [NSString stringWithFormat:@"%@",[NSNumber numberWithDouble:self.currentLocation.coordinate.longitude]];
    self.delegate.latitude = self.latitude;
    self.delegate.longitude = self.longitude;
    self.delegate.currentLocation = self.currentLocation;
    self.selectedIndex = @"-1";
    
    if (self.currentLocation) {
        [self getAddressFromUserCurrentLocation:self.currentLocation.coordinate.latitude log:self.currentLocation.coordinate.longitude];
        [self.locationManager stopUpdatingLocation];
        [self getProvidersListNearYou];
    }
}

- (void)convertLatLngToString:(CLLocation*)currentLocation {
    
    self.latitude = [NSString stringWithFormat:@"%@",[NSNumber numberWithDouble:currentLocation.coordinate.latitude]];
    self.longitude = [NSString stringWithFormat:@"%@",[NSNumber numberWithDouble:currentLocation.coordinate.longitude]];
    self.delegate.latitude = self.latitude;
    self.delegate.longitude = self.longitude;
    self.delegate.currentLocation = currentLocation;
    
}

#pragma mark - Get Current Location Address

- (void)getAddressFromUserCurrentLocation:(double)lat log:(double)log {
    
    CLGeocoder *ceo = [[CLGeocoder alloc]init];
    CLLocation *loc = [[CLLocation alloc]initWithLatitude:lat longitude:log]; //insert your coordinates
    
    [ceo reverseGeocodeLocation:loc
              completionHandler:^(NSArray *placemarks, NSError *error) {
                  
                  CLPlacemark *placemark = [placemarks objectAtIndex:0];
                  
                  if (placemark) {
                      
                      if (APP_DEBUG_MODE) { NSLog(@"placemark %@",placemark); }
                      //String to hold address
                      self.currentAddress = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
                      self.delegate.currentAddress = self.currentAddress;
                      self.selectedAddress = self.currentAddress;
                      //Print the location to console
                      if (APP_DEBUG_MODE) { NSLog(@"Current Location is  %@",self.currentAddress); }
                      [self loadTableView];
                  }else {
                      if (APP_DEBUG_MODE) { NSLog(@"Could not locate"); }
                  }
              }
     ];
}

#pragma mark - Add Observer

-(void)addObserverToGetLocation {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getUserSelectedLocation:) name:@"GetUserSelectedLocation" object:nil];
}

#pragma mark - Add Annotation

- (void)getUserSelectedLocation:(NSNotification*)infoDict {
    
    NSDictionary *dictInfo = infoDict.userInfo;
    self.currentLocation = [dictInfo valueForKey:@"SelectedLocation"];
    self.selectedAddress = [dictInfo valueForKey:@"Address"];
    self.selectedIndex = [dictInfo valueForKey:@"SelectedIndex"];
    self.delegate.selectedAddress = self.selectedAddress;

    if ([self.selectedIndex isEqualToString:@"-1"]) {
        [self convertLatLngToString:self.delegate.currentLocation];
    }else {
        [self convertLatLngToString:self.currentLocation];
    }
    
    [self getProvidersListNearYou];

}

#pragma mark - Set up All Category Table Cell

- (void)setUpTableViewCell {
    
    UINib *cellNib = [UINib nibWithNibName:@"AllMassagesCell" bundle:nil];
    [self.providerTableView registerNib:cellNib forCellReuseIdentifier:providerCellIdenfier];
    
}

#pragma mark - Get Providers List Around You

- (void)getProvidersListNearYou {
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_SERVICE_PROVIDER_CATEGORIES];
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    NSString *businessId =  @"5ca590942a72b";
    NSString *currentDay =  [[NSString string] getWeekDay];
    NSString *latitude = [NSString stringWithFormat:@"%@",[NSNumber numberWithDouble:self.currentLocation.coordinate.latitude]];
    NSString *longitude = [NSString stringWithFormat:@"%@",[NSNumber numberWithDouble:self.currentLocation.coordinate.longitude]];
    
    NSMutableDictionary *dictInfo = [NSMutableDictionary dictionaryWithObjectsAndKeys:accessToken,@"Authorization",businessId,@"businessCatId",currentDay,@"dayName",latitude,@"Latitude",longitude,@"Longitude", nil];
    
    if (internetStatus != NotReachable) {
        
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonPostData:url params:dictInfo onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                @try {
                    if (completed) {
                        
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSMutableArray *tempInfoArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE) { NSLog(@"Temp Info Array is %@", tempInfoArray); }
                        
                        if ([tempInfoArray isKindOfClass:[NSMutableArray class]]) {
                            
                            ServicesCategoriesInfo *serviceCatObj = [ServicesCategoriesInfo getSharedInstance];
                            self.mainArray = [serviceCatObj serviceProvidersListInfoObject:tempInfoArray];
                            if (APP_DEBUG_MODE) { NSLog(@"Service Provider Array is %@", self.mainArray);}
                            self.providerArray = [serviceCatObj serviceProvidersListInfoObject:tempInfoArray];
                            [self loadTableView];
                        }else {
                            [self.providerArray removeAllObjects];
                        }
                    }else {
                        if (APP_DEBUG_MODE) { NSLog(@"error is %@", error.localizedDescription);}
                        [SVProgressHUD dismiss];
                    }
                    
                } @catch (NSException *exception) {
                    //[EShopUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                    [SVProgressHUD dismiss];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}

- (void)loadTableView {
    
    self.providerTableView.hidden = NO;
    [self setUpTableHeaderView];
    [self.providerTableView reloadData];
}

#pragma mark - Table header view

- (void)setUpTableHeaderView {
    
    NearByAddressHeader *tableHeader = [NearByAddressHeader nearByAddressHeaderView:self.selectedAddress];
    [tableHeader.changeLocBtn addTarget:self action:@selector(changeCurrentLocation:) forControlEvents:UIControlEventTouchUpInside];
    tableHeader.searchBar.delegate = self;
    self.providerTableView.tableHeaderView = tableHeader;
    
}

- (void)changeCurrentLocation:(UIButton*)sender {
    
    ChangeLocationController *locationController = [self.storyboard instantiateViewControllerWithIdentifier:@"ChangeLocationController"];
    locationController.currentAddress = self.currentAddress;
    locationController.currentLocation = self.currentLocation;
    if (self.selectedIndex == nil) {
        self.selectedIndex = @"-1";
    }
    NSInteger selectedIndex = [self.selectedIndex integerValue];
    locationController.selectedIndex = selectedIndex;

    [self.navigationController pushViewController:locationController animated:YES];
    
}

#pragma mark - UITableViewDataSouce

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.providerArray.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    AllMassagesCell *allCatCell = [tableView dequeueReusableCellWithIdentifier:providerCellIdenfier];
    
    if (!allCatCell){
        allCatCell = [[AllMassagesCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:providerCellIdenfier];
    }
    allCatCell.selectionStyle = UITableViewCellSelectionStyleNone;
    ServicesCategoriesInfo *infoObj = self.providerArray[indexPath.row];
    [allCatCell configureAllCategoryCell:infoObj];
    return allCatCell;
    
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ServicesCategoriesInfo *infoObj = self.providerArray[indexPath.row];
    float rowHeight = 12 + infoObj.newNameHeight + 3 + infoObj.newAddressHeight + 7 + 63;
    return rowHeight;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 0.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.providerTableView deselectRowAtIndexPath:indexPath animated:NO];
    
    CategoryDetailController *detailController = [self.storyboard instantiateViewControllerWithIdentifier:kCategoryDetailController];
    ServicesCategoriesInfo *infoObj = self.providerArray[indexPath.row];
    detailController.categoryInfoObj = infoObj;
    detailController.currentLocation = self.currentLocation;
    [self.navigationController pushViewController:detailController animated:YES];
    
}


#pragma mark - Show Location Action

- (IBAction)showLocation:(id)sender {
    
    ShowLocationController *locController = [self.storyboard instantiateViewControllerWithIdentifier:@"ShowLocationController"];
    [self.navigationController pushViewController:locController animated:YES];

}

#pragma mark - Search delegate methods

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    if (self.providerArray.count>0) {
        [self.providerArray removeAllObjects];
    }
    if (searchText.length>0) {
        for (ServicesCategoriesInfo *infoObj in self.mainArray) {
            
            if([[infoObj.categoryName uppercaseString] rangeOfString:[searchText uppercaseString]].location != NSNotFound){
                
                if( ![self.providerArray containsObject:infoObj])
                    [self.providerArray addObject:infoObj];
            }
        }
        [self setUpViewsAfterFilter];
    }
}

- (void)setUpViewsAfterFilter {
 
    if (self.providerArray.count > 0) {
        [self.noProviderView removeFromSuperview];
        [self.providerTableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    }else {
        [self.view addSubview:self.noProviderView];
        self.noProviderView.center = self.view.center;
        [self.providerTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    }
    [self.providerTableView reloadData];

}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    // called twice every time
    [searchBar setShowsCancelButton:YES animated:YES];
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    // called only once
    [searchBar setShowsCancelButton:YES animated:YES];
    return YES;
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    
    [self setUpProviderArray];
    [searchBar resignFirstResponder];

}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
    [self setUpProviderArray];
    [searchBar resignFirstResponder];

}

- (void)setUpProviderArray {
 
    self.providerArray = [self.mainArray mutableCopy];
    [self setUpViewsAfterFilter];

}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
