//
//  ProvidersViewController.m
//  iBeauty
//
//  Created by App Innovation on 24/10/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import "ProvidersViewController.h"
#import "AllMassagesCell.h"
#import "UILabel+FlexibleWidHeight.h"

static NSString *providerCellIdenfier = @"allCategoryCell";

@interface ProvidersViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,weak) IBOutlet UITableView *providerTableView;
@property (nonatomic,strong) NSMutableArray *providerArray;
@property (nonatomic,weak) IBOutlet UIScrollView *scrollViewCategories;
@property (nonatomic,strong)  AppDelegate *delegate;
@property (nonatomic,weak)  IBOutlet UINavigationBar *navBar;

@end

@implementation ProvidersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.delegate = [AppDelegate sharedInstance];
    [self setUpServiceCategoryViews];
    [self setUpTableViewCell];
    //[self setGradient];
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    [self setScrollViewRect];
    self.delegate.tabbarController.tabBar.hidden = YES;

}

#pragma mark - Set Gradient

- (void)setGradient {
    
    self.navBar.frame = [iBeautyUtility setUpFrame:self.navBar.frame];
    CAGradientLayer *gradient = [iBeautyUtility setNavigationGradient:self.navBar];
    [self.navBar setBackgroundImage:[iBeautyUtility imageFromLayer:gradient] forBarMetrics:UIBarMetricsDefault];
}

#pragma mark - Set NavigationBar Width

- (void)setNavigationBarWidth {
    
    CGRect frame = self.navBar.frame;
    frame.size.width = self.view.frame.size.width;
    self.navBar.frame = frame;
}

#pragma mark - Set up All Category Table Cell

- (void)setUpTableViewCell {
    
    UINib *cellNib = [UINib nibWithNibName:@"AllMassagesCell" bundle:nil];
    [self.providerTableView registerNib:cellNib forCellReuseIdentifier:providerCellIdenfier];
    
}

- (void)setScrollViewRect {
    
    self.scrollViewCategories.translatesAutoresizingMaskIntoConstraints = YES;
    
    NSString *deviceType = [UIDevice currentDevice].model;
    if ([deviceType isEqualToString:@"iPhone"]) {
        
        if (IS_IPHONE_4 || IS_IPHONE_5 || IS_IPHONE_6 || IS_IPHONE_6_PLUS) {
            self.scrollViewCategories.frame = CGRectMake(0.0f, 64.0f,SCREEN_WIDTH, self.scrollViewCategories.frame.size.height);
        }else {
            self.scrollViewCategories.frame = CGRectMake(0.0f, 88.0f,SCREEN_WIDTH, self.scrollViewCategories.frame.size.height);
        }
    }else {
        self.scrollViewCategories.frame = CGRectMake(0.0f, 70.0f,SCREEN_WIDTH, self.scrollViewCategories.frame.size.height);
    }
    
}

#pragma mark - Filter Array By Category

- (void)filterArrayBySegmentIndex {
    
    if (APP_DEBUG_MODE) { NSLog(@"service info array is %@",self.serviceCategoryArray);}

    if (self.serviceCategoryArray.count > 0) {
        ServicesCategoriesInfo *infoObj = self.serviceCategoryArray[self.selectedIndex];
        self.providerArray = infoObj.providerCategories;
    }

}

#pragma mark - Init Categories

- (void) initCategories {
    
    for (UIView *subview in self.scrollViewCategories.subviews) {
        [subview removeFromSuperview];
    }
    
    float gap = 20;
    float posX = 10;
    
    for (int index = 0 ; index < self.serviceCategoryArray.count ; index ++) {
        
        ServicesCategoriesInfo *catInfo = self.serviceCategoryArray[index];
        
        NSString *category = catInfo.categoryName;
        
        float width = [UILabel widthOfTextForString:category andFont:[UIFont fontWithName:@"SegoeUI-Semibold" size:17.0] maxSize:(CGSize){MAXFLOAT,31 }];
        
        CGRect frame = CGRectMake(posX, 8, width+18, self.scrollViewCategories.frame.size.height-16);
        [self addTabWithTitle:category frame:frame index:index];
        
        posX = posX + width + gap;
        
    }
    
    self.scrollViewCategories.contentSize = CGSizeMake(posX+10, self.scrollViewCategories.frame.size.height);
    
}

- (void) addTabWithTitle:(NSString *)title frame:(CGRect)frame index:(int)index {
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height)];
    view.tag = index;
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(5, 0, frame.size.width-10, frame.size.height)];
    button.tag = index;
    NSString *catTitle =  [title uppercaseString];
    [button setTitle:catTitle forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont fontWithName:@"SegoeUI-Semibold" size:15.0]];
    
    [button addTarget:self action:@selector(selectCategoryTab:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:button];
    [self.scrollViewCategories addSubview:view];
    
    [iBeautyUtility setPickUpDeliveryViewWithoutBorderColor:view];
    
    if (self.selectedIndex == index) {
        
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        view.backgroundColor = [UIColor iBeautyThemeColor];
        //[view.layer insertSublayer:[iBeautyUtility setGradientColor_Views:view status:YES] atIndex:0];
    }else {
        [button setTitleColor:[UIColor iBeautyThemeColor] forState:UIControlStateNormal];
        view.backgroundColor = [UIColor clearColor];
        //[view.layer insertSublayer:[iBeautyUtility setGradientColor_Views:view status:NO] atIndex:0];
    }
    
}

#pragma mark - Select Category

- (void)selectCategoryTab:(UIButton*)sender {
    
    self.selectedIndex = sender.tag;
    [self setUpServiceCategoryViews];
}

- (void)setUpServiceCategoryViews {
    
    [self initCategories];
    [self filterArrayBySegmentIndex];
    [self.providerTableView reloadData];
}


#pragma mark - UITableViewDataSouce

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
        
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.providerArray.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    AllMassagesCell *allCatCell = [tableView dequeueReusableCellWithIdentifier:providerCellIdenfier];
    
    if (!allCatCell){
        allCatCell = [[AllMassagesCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:providerCellIdenfier];
    }
    allCatCell.selectionStyle = UITableViewCellSelectionStyleNone;
    ServicesCategoriesInfo *infoObj = self.providerArray[indexPath.row];
    [allCatCell configureAllCategoryCell:infoObj];
    return allCatCell;

}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ServicesCategoriesInfo *infoObj = self.providerArray[indexPath.row];
    float rowHeight = 12 + infoObj.newNameHeight + 3 + infoObj.newAddressHeight + 7 + 63;
    return rowHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 0.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.providerTableView deselectRowAtIndexPath:indexPath animated:NO];
    
    CategoryDetailController *detailController = [self.storyboard instantiateViewControllerWithIdentifier:kCategoryDetailController];
    ServicesCategoriesInfo *infoObj = self.providerArray[indexPath.row];
    detailController.categoryInfoObj = infoObj;
    detailController.currentLocation = self.currentLocation;
    [self.navigationController pushViewController:detailController animated:YES];
    
}

- (IBAction)backActionMethod:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
