//
//  ResetPasswordController.m
//  iBeauty
//
//  Created by Arun on 16/08/20.
//  Copyright © 2020 App Innovation. All rights reserved.
//

#import "ResetPasswordController.h"
#import "Constants.h"

@import SkyFloatingLabelTextField;

@interface ResetPasswordController ()<UITextFieldDelegate>

@property (nonatomic,weak)  IBOutlet SkyFloatingLabelTextField *passwordTxtField;
@property (nonatomic,weak)  IBOutlet SkyFloatingLabelTextField *confirmPasswordTxtField;

@property (nonatomic,weak)  IBOutlet UIButton *resetBtn;

@end

@implementation ResetPasswordController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setUpViews];
}

- (void)viewWillAppear:(BOOL)animated {
 
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden = YES;
}

- (void)setUpViews {
    
    [iBeautyUtility setButtonCorderRound:self.resetBtn];
    [self setStatusBarColor];
}

#pragma mark - Set Status Bar Color

- (void)setStatusBarColor {

    if (@available(iOS 13.0, *)) {
        
        UIView *statusBar = [[UIView alloc]initWithFrame:[UIApplication sharedApplication].keyWindow.windowScene.statusBarManager.statusBarFrame] ;
        [self.view addSubview:statusBar];
        if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
            statusBar.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:130/255.0 blue:90/255.0 alpha:1.0];
        }
    }else {
            UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
            if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
                statusBar.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:130/255.0 blue:90/255.0 alpha:1.0];
            }
    }

}

#pragma mark - Reset Password

- (IBAction)resetPassword:(id)sender {
    
    BOOL validStatus =  [self checkValidationForResetPassword];
    
    if (validStatus == YES) {
        
        [self resetPassword];
    }
    
}

#pragma mark - Reset Password

- (void)resetPassword {
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_RESET_PASSWORD];
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSString *emailStr = [NSString stringWithFormat:@"%@",[self.infoDict valueForKey:@"Email"]];
    NSString *codeStr = [NSString stringWithFormat:@"%@",[self.infoDict valueForKey:@"TempCode"]];

    NSDictionary * parametersDict = [NSDictionary dictionaryWithObjectsAndKeys:emailStr,@"email",codeStr,@"code",self.passwordTxtField.text,@"password", nil];
    if (APP_DEBUG_MODE) { NSLog(@"parametersDict is %@",parametersDict); }
    
    if (internetStatus != NotReachable) {
        
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonPostData:url params:parametersDict onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                
                @try {
                    if (completed) {
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSDictionary *infoDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE)
                            NSLog(@"Info is %@", infoDict);
                        [self parseResponseInfoDict:infoDict];
                    }else {
                        if (APP_DEBUG_MODE)
                            NSLog(@"error is %@", error.localizedDescription);
                        [SVProgressHUD dismiss];
                    }
                    
                } @catch (NSException *exception) {
                    //[iBeautyUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
    
}


- (void)parseResponseInfoDict:(NSDictionary*)dictInfo {
    
    if ([dictInfo valueForKey:@"status"] != nil) {
        NSInteger status = [[dictInfo valueForKey:@"status"]integerValue];
        if (status == 200) {
            [self.navigationController popToRootViewControllerAnimated:YES];
            [iBeautyUtility showAlertMessage:[dictInfo valueForKey:@"message"]];
        }else {
            [iBeautyUtility showAlertMessage:[dictInfo valueForKey:@"message"]];
        }
    }
    
}


#pragma mark - Textfield Delegate Methods

- (BOOL)checkValidationForResetPassword {
    
    if (self.passwordTxtField.text.length < 1) {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"Please enter password", @"")];
        return NO ;
    }
    if (self.confirmPasswordTxtField.text.length < 1) {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"Please enter password again", @"")];
        return NO ;
    }
    
    if (![self.passwordTxtField.text isEqualToString:self.confirmPasswordTxtField.text]) {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"Password doesn't match", @"")];
        return NO ;
    }
    
    return YES;
}


#pragma mark - Textfield Delegate Methods

-(BOOL) textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
