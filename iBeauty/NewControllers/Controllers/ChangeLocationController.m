//
//  ChangeLocationController.m
//  iBeauty
//
//  Created by App Innovation on 25/10/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import "ChangeLocationController.h"
#import "CurrentAddressCell.h"
#import "CurrentAddressTableHeader.h"
#import "CurrentAddressTableFooter.h"
#import "Constants.h"
#import "AddNewAddressController.h"

static NSString * addressCelIdentifier = @"CurrentAddressCell";

@interface ChangeLocationController ()<UITableViewDataSource,UITableViewDelegate,CLLocationManagerDelegate>

@property (nonatomic,weak) IBOutlet UITableView  *addressTableView;
@property (nonatomic,strong) NSMutableArray  *addresssArray;
@property (nonatomic,weak) IBOutlet UIButton  *continueButton;
@property (nonatomic,weak)  IBOutlet UINavigationBar *navBar;
@property (nonatomic,strong)  AppDelegate *delegate;
@property (nonatomic,strong) CurrentAddressTableHeader *tableHeader;
@property (nonatomic,strong) CLLocation *selectedLocation;
@property (nonatomic,assign) BOOL filterStatus;
@property (nonatomic,strong) CLLocationManager *locationManager;


@end

@implementation ChangeLocationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.delegate = [AppDelegate sharedInstance];

    [self setUpTableViewCell];
    //[self setGradient];
    [self setUpViews];
}


- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    self.delegate.tabbarController.tabBar.hidden = YES;
    [self getAddressListFor_RegisteredUsers];
}

#pragma mark - Set Up Table Cell

- (void)setUpTableViewCell {
    
    UINib *cellNib = [UINib nibWithNibName:@"CurrentAddressCell" bundle:nil];
    [self.addressTableView registerNib:cellNib forCellReuseIdentifier:addressCelIdentifier];
    [self setUpTableHeight];
    [self setUpTableHeaderView];
    [self setUpTableFooterView];

}

- (void)setUpTableHeight {
    
    CGRect tableFrame = self.addressTableView.frame;
    if (UIScreen.mainScreen.bounds.size.height >= 812.0) {
        tableFrame.size.height = UIScreen.mainScreen.bounds.size.height - 160.0;
    }else {
        tableFrame.size.height = UIScreen.mainScreen.bounds.size.height - 102.0;
    }
    self.addressTableView.frame = tableFrame;
}

- (void)setUpViews {
 
    self.continueButton.frame = [iBeautyUtility setUpFrame:self.continueButton.frame];
    [iBeautyUtility setButtonCorderRound:self.continueButton];
    //[self.continueButton.layer insertSublayer:[iBeautyUtility setGradientColor_Buttons:self.continueButton] atIndex:0];
}

#pragma mark - Set Gradient

- (void)setGradient {
    
    self.navBar.frame = [iBeautyUtility setUpFrame:self.navBar.frame];
    CAGradientLayer *gradient = [iBeautyUtility setNavigationGradient:self.navBar];
    [self.navBar setBackgroundImage:[iBeautyUtility imageFromLayer:gradient] forBarMetrics:UIBarMetricsDefault];
}

#pragma mark - Fetch selected Address

- (void)filterSelectedAddress {
 
    if (self.selectedIndex == -1) {
        
        [self.tableHeader.addressImgView setImage:[UIImage imageNamed:@"Check"]];
    }
}

#pragma mark - Get list of addresss

- (void)getAddressListFor_RegisteredUsers {
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_GET_ADDRESSES];
    
    //NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    
    NSDictionary *profileInfo = [[NSUserDefaults standardUserDefaults]valueForKey:KLoginInfo];
    NSString *userID = [profileInfo valueForKey:@"UserID"];
    
    NSString *urlString = [NSString stringWithFormat:@"%@/%@",url,userID];
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable) {
         [SVProgressHUD showWithStatus:KLoadingMessage];
        [handler jsonGETData:urlString parameters:nil onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                 [SVProgressHUD dismiss];
                @try {
                    if (completed) {
                        if (APP_DEBUG_MODE) { NSLog(@"Address Array is %@", completed);}
                        self.addresssArray  = [AddressModel getCurrentAddressListObj:completed];
                        if (self.filterStatus == false) {
                            [self filterSelectedAddress];
                        }
                        [self getSelectedAddressIndex];
                        [self setUpTableHeaderView];
                        [self.addressTableView reloadData];
                    }else{
                    }
                } @catch (NSException *exception) {
                    //[EShopUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                } @finally {
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}


#pragma mark - Table header view

- (void)setUpTableHeaderView {
    
    self.tableHeader = [CurrentAddressTableHeader currentAddressTableHeaderView:self.currentAddress];
    [self.tableHeader.selectAddrssBtn addTarget:self action:@selector(selectCurrentAddress:) forControlEvents:UIControlEventTouchUpInside];
    if (self.selectedIndex == -1) {
        [self.tableHeader.addressImgView setImage:[UIImage imageNamed:@"Check"]];
    }else {
        [self.tableHeader.addressImgView setImage:[UIImage imageNamed:@"Uncheck"]];
    }
    self.addressTableView.tableHeaderView = self.tableHeader;
    
}

- (void)selectCurrentAddress:(UIButton*)sender {
    
    [self.tableHeader.addressImgView setImage:[UIImage imageNamed:@"Check"]];
    self.selectedIndex = -1;
    [self.addressTableView reloadData];

}

- (void)getSelectedAddressIndex {
    
    for (int i =0;i<self.addresssArray.count;i++) {
        AddressModel *addressInfo = self.addresssArray[i];
        if ([addressInfo.address isEqualToString:self.delegate.selectedAddress]) {
            self.selectedIndex = i;
            self.filterStatus = true;
            break;
        }
    }
    
}

#pragma mark - Table footer view

- (void)setUpTableFooterView {
    
    CurrentAddressTableFooter *tableFooter = [CurrentAddressTableFooter currentAddressTableFooterView];
    [tableFooter.addNewAddress addTarget:self action:@selector(addNewAddress:) forControlEvents:UIControlEventTouchUpInside];
    self.addressTableView.tableFooterView = tableFooter;
    
}

- (void)addNewAddress:(UIButton*)sender {
    
    AddNewAddressController *addressController = [self.storyboard instantiateViewControllerWithIdentifier:@"AddNewAddressController"];
    addressController.modeStatus = NO;
    [self.navigationController pushViewController:addressController animated:YES];
    
}

- (IBAction)backButtonAction:(id)sender {
 
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)continueAction:(id)sender {

    NSString *address = [[NSString alloc]init];

    if (self.selectedIndex == -1) {
        self.selectedLocation = self.currentLocation;
        address = self.currentAddress;
    }else {
        AddressModel *addressInfo = self.addresssArray[self.selectedIndex];
        if ([addressInfo.latitude isEqualToString:@"0.0"]) {
            [self showAlertWithMessage:@"Please pin location first"];
            return;
        }
        double lat = [addressInfo.latitude doubleValue];
        double lng = [addressInfo.longitude doubleValue];
        self.selectedLocation = [[CLLocation alloc]initWithLatitude:lat longitude:lng];
        address = addressInfo.address;
    }
    NSString *selectedIndex = [NSString stringWithFormat:@"%ld",(long)self.selectedIndex];
    NSDictionary *parametersDict  = [NSDictionary dictionaryWithObjectsAndKeys:self.selectedLocation,@"SelectedLocation",address,@"Address",selectedIndex,@"SelectedIndex",nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GetUserSelectedLocation" object:nil userInfo:parametersDict];
    
    [self.navigationController popViewControllerAnimated:YES];

}

- (void)showAlertWithMessage:(NSString *)message {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:message
                                          preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Ok", @"OK action")
                               style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                               { NSLog(@"OK action");}];
    
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - UITableViewDataSouce

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.addresssArray.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CurrentAddressCell *addressCell = [tableView dequeueReusableCellWithIdentifier:addressCelIdentifier];
    
    if (!addressCell){ 
        addressCell = [[CurrentAddressCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:addressCelIdentifier];
    }
    addressCell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (self.selectedIndex == indexPath.row) {
        [addressCell.addressImgView setImage:[UIImage imageNamed:@"Check"]];
    }else {
        [addressCell.addressImgView setImage:[UIImage imageNamed:@"Uncheck"]];
    }
    AddressModel *infoObj = self.addresssArray[indexPath.row];
    [addressCell configureCurrentAddressCell:infoObj];
    [addressCell.pinLocationButton addTarget:self action:@selector(pinLocToAddress:) forControlEvents:UIControlEventTouchUpInside];
    addressCell.pinLocationButton.tag = indexPath.row;
    
    return addressCell;
    
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    AddressModel *infoObj = self.addresssArray[indexPath.row];
    return infoObj.cellHeight;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 50.0f;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 50.0f)];
    UILabel *tempLabel= [[UILabel alloc]initWithFrame:CGRectMake(15,10,tableView.frame.size.width-30,30)];
    tempLabel.textColor = [UIColor darkGrayColor];
    tempLabel.font = [UIFont fontWithName:@"SegoeUI-Semibold" size:18.0];
    tempLabel.text = @"SAVED ADDRESSES"; //NSLocalizedString(@"Saved Addresses", @"");
    headerView.backgroundColor = [UIColor whiteColor];
    [headerView addSubview:tempLabel];
    return headerView;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.addressTableView deselectRowAtIndexPath:indexPath animated:NO];
    self.selectedIndex = indexPath.row;
    self.filterStatus = true;
    [self.addressTableView reloadData];
    [self.tableHeader.addressImgView setImage:[UIImage imageNamed:@"Uncheck"]];

}

- (void)pinLocToAddress:(UIButton*)sender {
    
    AddressModel *infoObj = self.addresssArray[sender.tag];
    AddNewAddressController *addressView = [self.storyboard instantiateViewControllerWithIdentifier:kAddNewAddressController];
    addressView.addressInfoObj = infoObj;
    addressView.modeStatus = YES;
    [self.navigationController pushViewController:addressView animated:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
