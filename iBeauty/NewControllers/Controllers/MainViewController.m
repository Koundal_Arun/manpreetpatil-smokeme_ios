//
//  MainViewController.m
//  iBeauty
//
//  Created by App Innovation on 23/10/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import "MainViewController.h"
#import "Constants.h"

@interface MainViewController ()


@property (nonatomic,strong)AppDelegate *appDelegate;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    [self setUpTabViewController];
}

- (void)setUpTabViewController {
    
    HomesViewController *homeView = [[HomesViewController alloc]init];
    
    homeView = [self.storyboard instantiateViewControllerWithIdentifier:kHomesViewController];
    homeView.tabBarItem.image = [UIImage imageNamed:@"home"];
    UINavigationController *navigationCHomeView = [[UINavigationController alloc]initWithRootViewController:homeView];
    navigationCHomeView.navigationBarHidden = YES;
    [homeView setTitle:@"Home"];
    
    NearbyViewController *nearByView = [[NearbyViewController alloc]init];
    nearByView = [self.storyboard instantiateViewControllerWithIdentifier:kNearByViewController];
    nearByView.tabBarItem.image = [UIImage imageNamed:@"Location"];
    UINavigationController *navigationCNearBy = [[UINavigationController alloc]initWithRootViewController:nearByView];
    navigationCNearBy.navigationBarHidden = YES;
    [nearByView setTitle:@"Nearby"];
    
    OffersViewController *offersView = [[OffersViewController alloc]init];
    offersView = [self.storyboard instantiateViewControllerWithIdentifier:kOffersViewController];
    offersView.tabBarItem.image = [UIImage imageNamed:@"offers"];
    offersView.title = @"Offers";
    UINavigationController *navigationCOffers = [[UINavigationController alloc]initWithRootViewController:offersView];
    navigationCOffers.navigationBarHidden = YES;
    
    MoreViewController *moreView = [[MoreViewController alloc]init];
    moreView = [self.storyboard instantiateViewControllerWithIdentifier:kMoreViewController];
    moreView.tabBarItem.image=[UIImage imageNamed:@"more_icon"];
    UINavigationController *navigationCMoreView = [[UINavigationController alloc]initWithRootViewController:moreView];
    navigationCMoreView.navigationBarHidden = YES;
    [moreView setTitle:@"More"];
    
    NSArray *viewControllers = [NSArray arrayWithObjects:navigationCHomeView,navigationCNearBy,navigationCOffers,navigationCMoreView,nil];
    
    self.appDelegate.tabbarController = [[UITabBarController alloc]init];
    self.appDelegate.tabbarController.viewControllers = viewControllers;
    
    [[UITabBar appearance] setTintColor:[UIColor iBeautyThemeColor]];
    
    [AppDelegate sharedInstance].window.rootViewController = self.appDelegate.tabbarController;
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
