//
//  ResetPasswordController.h
//  iBeauty
//
//  Created by Arun on 16/08/20.
//  Copyright © 2020 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>

@import SkyFloatingLabelTextField;

NS_ASSUME_NONNULL_BEGIN

@interface ResetPasswordController : UIViewController

@property (nonatomic,strong) NSDictionary *infoDict;

@end

NS_ASSUME_NONNULL_END
