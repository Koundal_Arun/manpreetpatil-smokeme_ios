//
//  AddNewAddressController.h
//  iBeauty
//
//  Created by App Innovation on 07/11/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddressModel.h"
#import "Constants.h"

NS_ASSUME_NONNULL_BEGIN

@interface AddNewAddressController : UIViewController

@property(nonatomic,strong) AddressModel *addressInfoObj;
@property(nonatomic,assign) BOOL modeStatus;
@property (nonatomic, strong) CLLocation *selectedLocation;

@end

NS_ASSUME_NONNULL_END
