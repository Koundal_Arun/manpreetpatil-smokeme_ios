//
//  HomesViewController.m
//  iBeauty
//
//  Created by App Innovation on 23/10/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import "HomesViewController.h"
#import "UILabel+FlexibleWidHeight.h"
#import "NSString+iBeautyString.h"
#import "PromotionalAds.h"
#import <CoreLocation/CoreLocation.h>
#import "CurrentAddressHeader.h"
#import "OffersDetailViewController.h"
#import "ChangeLocationController.h"
#import "DXPopover.h"
#import "CommonClass.h"

static NSString *topServicesCellIdentifier = @"TopServicesTableCell";
static NSString *aroundCellIdentifier = @"AroundTableCell";
static NSString *specialOffersCellIdentifier = @"SpecialOffersTableCell";

@interface HomesViewController ()<CLLocationManagerDelegate,UISearchBarDelegate> {
    
    CGFloat _popoverWidth;

}

@property (nonatomic,strong) NSMutableArray *array_TopServices;
@property (nonatomic,strong) NSMutableArray *array_AroundYou;
@property (nonatomic,weak) IBOutlet UITableView  *serviceCategoryTableView;
@property (nonatomic,strong) NSMutableArray *serviceCategoriesArray;

@property (nonatomic,strong) NSString *latitude;
@property (nonatomic,strong) NSString *longitude;
@property (nonatomic,strong) CLLocationManager *locationManager;
@property (nonatomic,strong) CLLocation *currentLocation;
@property (nonatomic,assign) NSInteger count;
@property (nonatomic,strong) NSString *currentAddress;
@property (nonatomic,strong) NSString *selectedAddress;
@property (nonatomic,strong) CLLocation *selectedLocation;
@property (nonatomic,strong) NSString *selectedIndex;
@property (nonatomic,strong) NSDictionary *filterDict;
@property (nonatomic,assign) BOOL filterStatus;

@property (nonatomic,strong) AppDelegate *delegate;
@property (nonatomic,strong) NSMutableArray *filterArray;
@property (nonatomic, strong) CurrentAddressHeader *tableHeader;

// Pop Over
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) DXPopover *popover;
@property (nonatomic, assign) BOOL searchStatus;


@end

@implementation HomesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.delegate = [AppDelegate sharedInstance];
    [self configureLocationManager];
    self.serviceCategoryTableView.hidden = YES;
    [self setUpTableViewCell];
    [self setStatusBarColor];
    [self addObserverToGetLocation];
    [self setPopOVerView];
    [self setUpFilterView];
    [self addObserverToGetFilterParameters];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.delegate.tabbarController.tabBar.hidden = NO;
    if (self.count >= 1) {
        [self getAllServiceCategoriesInfo];
    }
    [self.view endEditing:YES];
    [self.filterArray removeAllObjects];
    [self setUpViewsAfterFilter];
    self.popover = nil;
}

#pragma mark - Set Pop Over View

- (void)setPopOVerView {
    
    self.filterArray = [NSMutableArray new];
    [self resetPopover];
    [self setUpPopOverTableView];
}

#pragma mark - Set Status Bar Color

- (void)setStatusBarColor {

    if (@available(iOS 13.0, *)) {
        
        UIView *statusBar = [[UIView alloc]initWithFrame:[UIApplication sharedApplication].keyWindow.windowScene.statusBarManager.statusBarFrame] ;
        [[UIApplication sharedApplication].keyWindow addSubview:statusBar];
        if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
//            [statusBar.layer insertSublayer:[self setGradientColor_Views:statusBar] atIndex:0];
            statusBar.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:130/255.0 blue:90/255.0 alpha:1.0];
        }
    }else {
            UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
            if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
//                [statusBar.layer insertSublayer:[self setGradientColor_Views:statusBar] atIndex:0];
                statusBar.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:130/255.0 blue:90/255.0 alpha:1.0];
            }
    }

}

- (CAGradientLayer*)setGradientColor_Views:(UIView*)sender {
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = sender.bounds;
    gradient.startPoint = CGPointMake(0, 0);
    gradient.endPoint = CGPointMake(1, 0);
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:233.0/255.0 green:83/255.0 blue:158/255.0 alpha:1.0] CGColor],(id)[[UIColor colorWithRed:240/255.0 green:59.0/255.0 blue:53.0/255.0 alpha:1.0] CGColor], nil];
    
    return gradient;
}

#pragma mark - Set Up TableView Cells

- (void)setUpTableViewCell {
    
    UINib *topServicesNib = [UINib nibWithNibName:@"TopServicesTableCell" bundle:nil];
    [self.serviceCategoryTableView registerNib:topServicesNib forCellReuseIdentifier:topServicesCellIdentifier];
    
    UINib *aroundCellNib = [UINib nibWithNibName:@"AroundTableCell" bundle:nil];
    [self.serviceCategoryTableView registerNib:aroundCellNib forCellReuseIdentifier:aroundCellIdentifier];
    
    UINib *specialOffersNib = [UINib nibWithNibName:@"SpecialOffersTableCell" bundle:nil];
    [self.serviceCategoryTableView registerNib:specialOffersNib forCellReuseIdentifier:specialOffersCellIdentifier];
}

#pragma mark - Add Observer

-(void)addObserverToGetLocation {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getUserSelectedLocation:) name:@"GetUserSelectedLocation" object:nil];
}

#pragma mark - Add Annotation

- (void)getUserSelectedLocation:(NSNotification*)infoDict {
    
    NSDictionary *dictInfo = infoDict.userInfo;
    self.selectedLocation = [dictInfo valueForKey:@"SelectedLocation"];
    self.selectedAddress = [dictInfo valueForKey:@"Address"];
    self.selectedIndex = [dictInfo valueForKey:@"SelectedIndex"];
    self.delegate.selectedAddress = self.selectedAddress;
    if ([self.selectedIndex isEqualToString:@"-1"]) {
        [self convertLatLngToString:self.currentLocation];
    }else {
        [self convertLatLngToString:self.selectedLocation];
    }
    self.filterStatus = NO;
    [self getAllServiceCategoriesInfo];

}

#pragma mark - Configure Location Manager

-(void)configureLocationManager {
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        [self.locationManager requestWhenInUseAuthorization];
    
    [self.locationManager startUpdatingLocation];
    
}

#pragma mark - Update Location

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    self.currentLocation = locations.lastObject;
     
    [self convertLatLngToString:self.currentLocation];
    
    self.latitude = [NSString stringWithFormat:@"%@",[NSNumber numberWithDouble:self.currentLocation.coordinate.latitude]];
    self.longitude = [NSString stringWithFormat:@"%@",[NSNumber numberWithDouble:self.currentLocation.coordinate.longitude]];
    self.delegate.latitude = self.latitude;
    self.delegate.longitude = self.longitude;
    self.delegate.currentLocation = self.currentLocation;
    self.selectedIndex = @"-1";
    
    if ( self.currentLocation && self.count == 0) {
        [self getAddressFromUserCurrentLocation:self.currentLocation.coordinate.latitude log:self.currentLocation.coordinate.longitude];
        self.count += 1;
        [self.locationManager stopUpdatingLocation];
        [self getAllServiceCategoriesInfo];
    }
}

- (void)convertLatLngToString:(CLLocation*)currentLocation {
    
    self.latitude = [NSString stringWithFormat:@"%@",[NSNumber numberWithDouble:currentLocation.coordinate.latitude]];
    self.longitude = [NSString stringWithFormat:@"%@",[NSNumber numberWithDouble:currentLocation.coordinate.longitude]];
    self.delegate.latitude = self.latitude;
    self.delegate.longitude = self.longitude;
    self.delegate.currentLocation = currentLocation;
    
}

#pragma mark - Get Current Location Address

- (void)getAddressFromUserCurrentLocation:(double)lat log:(double)log {
    
    CLGeocoder *ceo = [[CLGeocoder alloc]init];
    CLLocation *loc = [[CLLocation alloc]initWithLatitude:lat longitude:log]; //insert your coordinates
    
    [ceo reverseGeocodeLocation:loc
              completionHandler:^(NSArray *placemarks, NSError *error) {
                  
                  CLPlacemark *placemark = [placemarks objectAtIndex:0];
                  
                  if (placemark) {
                      
                      if (APP_DEBUG_MODE) { NSLog(@"placemark %@",placemark); }
                      //String to hold address
                      self.currentAddress = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
                      self.delegate.currentAddress = self.currentAddress;
                      self.selectedAddress = self.currentAddress;
                      //Print the location to console
                      if (APP_DEBUG_MODE) { NSLog(@"Current Location is  %@",self.currentAddress); }
                  }else {
                      if (APP_DEBUG_MODE) { NSLog(@"Could not locate"); }
                  }
              }
     ];
}

#pragma mark - Get ALL Services Details

- (void)getAllServiceCategoriesInfo {
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_GET_ALL_CATEGORIES];
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    NSString *businessId = @"5ca590942a72b";
    NSString *currentDay =  [[NSString string] getWeekDay];
    
    NSDictionary *dictInfo = [NSDictionary dictionaryWithObjectsAndKeys:accessToken,@"Authorization",businessId,@"businessCatId",self.latitude,@"Latitude",self.longitude,@"Longitude",currentDay,@"dayName",nil];
    
    if (internetStatus != NotReachable) {
        
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonPostData:url params:dictInfo onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //[SVProgressHUD dismiss];
                @try {
                    if (completed) {
                        
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSMutableArray *tempInfoArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE) { NSLog(@"Temp Info array is %@", tempInfoArray); }
                        
                        ServicesCategoriesInfo *serviceCatObj = [ServicesCategoriesInfo getSharedInstance];
                        if (tempInfoArray.count > 0) {
                            self.array_TopServices = [serviceCatObj getAllServicesCategoryList:tempInfoArray];
                        }
                        if (APP_DEBUG_MODE) { NSLog(@"Top Services Array is %@", self.array_TopServices); }
                        [self getProvidersListNearYou];
                    }else {
                        if (APP_DEBUG_MODE) { NSLog(@"error is %@", error.localizedDescription); }
                        [SVProgressHUD dismiss];
                    }
                    
                } @catch (NSException *exception) {
                    [SVProgressHUD dismiss];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        //[iBeautyUtility showAlertMessage:NETWORK_STATUS_INFO];
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [self presentViewController:noInternetController animated:YES completion:nil];
    }
}

#pragma mark - Get Providers List Around You

- (void)getProvidersListNearYou {
    
    NSString *url;
    if (self.filterStatus == NO) {
        url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_SERVICE_PROVIDER_CATEGORIES];
    }else {
        url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_FILTER_PROVIDERS];
    }
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    NSString *businessId =  @"5ca590942a72b";
    NSString *currentDay =  [[NSString string] getWeekDay];
    
    NSMutableDictionary *dictInfo;

    if (self.filterStatus == NO) {
          dictInfo = [NSMutableDictionary dictionaryWithObjectsAndKeys:accessToken,@"Authorization",businessId,@"businessCatId",currentDay,@"dayName",self.latitude,@"Latitude",self.longitude,@"Longitude", nil];
    }else {
        dictInfo = [self getParametersDictForFilter];
    }
    
    if (internetStatus != NotReachable) {
        
        //[SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonPostData:url params:dictInfo onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                @try {
                    if (completed) {
                        
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSMutableArray *tempInfoArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE) { NSLog(@"Temp Info Array is %@", tempInfoArray); }
                        
                        if ([tempInfoArray isKindOfClass:[NSMutableArray class]]) {
                            
                            ServicesCategoriesInfo *serviceCatObj = [ServicesCategoriesInfo getSharedInstance];
                            NSMutableArray *tempArray = [serviceCatObj serviceProvidersListInfoObject:tempInfoArray];
                                if (APP_DEBUG_MODE) { NSLog(@"Service Provider Near you are %@", tempArray);}
                                [self filterArrayByDistance:tempArray];
                        }else {
                            [self.serviceCategoriesArray removeAllObjects];
                        }
                        [self getPromotionalAdsInfo];
                    }else {
                        if (APP_DEBUG_MODE) { NSLog(@"error is %@", error.localizedDescription);}
                        [SVProgressHUD dismiss];
                    }
                    
                } @catch (NSException *exception) {
                    //[EShopUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                    [SVProgressHUD dismiss];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}

- (NSMutableDictionary*)getParametersDictForFilter {
    
    NSMutableDictionary *infoDict = [[NSMutableDictionary alloc]init];
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    NSString *businessId =  @"5ca590942a72b";
    NSString *currentDay =  [[NSString string] getWeekDay];
    NSString *latitude = [NSString stringWithFormat:@"%@",[NSNumber numberWithDouble:self.currentLocation.coordinate.latitude]];
    NSString *longitude = [NSString stringWithFormat:@"%@",[NSNumber numberWithDouble:self.currentLocation.coordinate.longitude]];
    NSString * cityName = @"";
    
    NSMutableArray *serviceProvideAt = [NSMutableArray new];
    
    if ([[self.filterDict valueForKey:@"Home"]integerValue] > 0) {
        NSMutableDictionary *homeDict = [NSMutableDictionary new];
        [homeDict setValue:@"Home" forKey:@"Name"];
        [serviceProvideAt addObject:homeDict];
    }
    
    if ([[self.filterDict valueForKey:@"Shop"]integerValue] > 0) {
        NSMutableDictionary *shopDict = [NSMutableDictionary new];
        [shopDict setValue:@"Shop" forKey:@"Name"];
        [serviceProvideAt addObject:shopDict];
    }
    
    NSMutableArray *serviceProvideFor = [NSMutableArray new];
    
    if ([[self.filterDict valueForKey:@"Men"]integerValue] > 0) {
        NSMutableDictionary *menDict = [NSMutableDictionary new];
        [menDict setValue:@"Men" forKey:@"Name"];
        [serviceProvideFor addObject:menDict];
    }
    if ([[self.filterDict valueForKey:@"Women"]integerValue] > 0) {
        NSMutableDictionary *womenDict = [NSMutableDictionary new];
        [womenDict setValue:@"Women" forKey:@"Name"];
        [serviceProvideFor addObject:womenDict];
    }
    if ([[self.filterDict valueForKey:@"Unisex"]integerValue] > 0) {
        NSMutableDictionary *unisexDict = [NSMutableDictionary new];
        [unisexDict setValue:@"Unisex" forKey:@"Name"];
        [serviceProvideFor addObject:unisexDict];
    }
    
    [infoDict setValue:accessToken forKey:@"Authorization"];
    [infoDict setValue:businessId forKey:@"BusinessCatId"];
    [infoDict setValue:currentDay forKey:@"DayName"];
    [infoDict setValue:cityName forKey:@"City"];
    [infoDict setValue:latitude forKey:@"CurrentLatitude"];
    [infoDict setValue:longitude forKey:@"CurrentLongitude"];
    
    [infoDict setObject:serviceProvideAt forKey:@"ServiceProvideAt"];
    [infoDict setObject:serviceProvideFor forKey:@"ServiceProvideFor"];

    return infoDict;
}

#pragma mark - Filter Array By Distance

- (void)filterArrayByDistance:(NSMutableArray*)providersArray {
    
    self.array_AroundYou = [NSMutableArray new];
    NSSortDescriptor *sortByDistance = [NSSortDescriptor sortDescriptorWithKey:@"providerDistance" ascending:YES];
    NSArray *distanceArray = [providersArray sortedArrayUsingDescriptors:@[sortByDistance]];
    [self.array_AroundYou addObjectsFromArray:distanceArray];
}

#pragma mark - Get Promotional Ads Info

- (void)getPromotionalAdsInfo { 

    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_PROMOTIONAL_ADS];
    
    //NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    
    NSString *urlString = [NSString stringWithFormat:@"%@",url];
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable) {
        
       // [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonGETData:urlString parameters:nil onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                @try {
                    if (completed) {
                        PromotionalAds *instanceObj = [PromotionalAds getSharedInstance];
                        self.array_SpecialPackages = [instanceObj promotionalInfoObj:completed];
                        if (APP_DEBUG_MODE) { NSLog(@"Info array is %@", self.array_SpecialPackages);}
                        [self getPromotionalImagesArray];
                        [self loadTableView];
                    }else {
                        if (APP_DEBUG_MODE)
                            NSLog(@"error is %@", error.localizedDescription);
                    }
                    
                } @catch (NSException *exception) {
                    //[EShopUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
    
}

- (void)loadTableView {
    
    self.serviceCategoryTableView.hidden = NO;
    [self setUpTableHeaderView];
    [self.serviceCategoryTableView reloadData];
    
}

- (void)getPromotionalImagesArray {
    
    self.promotionImages = [NSMutableArray new];
    
    for (PromotionalAds * infoObj in self.array_SpecialPackages) {
        
        [self.promotionImages addObject:[NSString stringWithFormat:@"%@%@",IMAGE_BASE_URL,infoObj.adImage]];
        
        if (APP_DEBUG_MODE) { NSLog(@"Image is %@", [NSString stringWithFormat:@"%@%@",IMAGE_BASE_URL,infoObj.adImage]); }
    }
    
}

#pragma mark - Table header view

- (void)setUpTableHeaderView {
    
    self.tableHeader = [CurrentAddressHeader currentAddressHeaderView:self.selectedAddress];
    [self.tableHeader.changeLocBtn addTarget:self action:@selector(changeCurrentLocation:) forControlEvents:UIControlEventTouchUpInside];
    [self.tableHeader.filterButton addTarget:self action:@selector(filterProviders:) forControlEvents:UIControlEventTouchUpInside];
    self.tableHeader.searchBar.delegate = self;
    self.serviceCategoryTableView.tableHeaderView = self.tableHeader;
//    [self titleShowPopover];
//    [self.popover setHidden:YES];
    [self resetPopover];

}

- (void)changeCurrentLocation:(UIButton*)sender {
    
    ChangeLocationController *locationController = [self.storyboard instantiateViewControllerWithIdentifier:@"ChangeLocationController"];
    locationController.currentAddress = self.currentAddress;
    locationController.currentLocation = self.selectedLocation;
    NSInteger selectedIndex = [self.selectedIndex integerValue];
    locationController.selectedIndex = selectedIndex;

    [self.navigationController pushViewController:locationController animated:YES];
    
}

#pragma mark - Set up Filter View

-(void)setUpFilterView {
    
    [self.filterDict setValue:[NSNumber numberWithInteger:0] forKey:@"Men"];
    [self.filterDict setValue:[NSNumber numberWithInteger:0] forKey:@"Women"];
    [self.filterDict setValue:[NSNumber numberWithInteger:0] forKey:@"Home"];
    [self.filterDict setValue:[NSNumber numberWithInteger:0] forKey:@"Shop"];
}


#pragma mark - Filter Providers

- (void)filterProviders:(UIButton*)sender {

    FilterViewController *filterController = [self.storyboard instantiateViewControllerWithIdentifier:kFilterViewController];
    filterController.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:filterController animated:YES completion:nil];
    
}

#pragma mark - Add Observer

- (void)addObserverToGetFilterParameters {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getFilterInfo:) name:@"selectedFiltersInfo" object:nil];
}


#pragma mark - Filter Services

- (void)getFilterInfo:(NSNotification*)filterInfo {
    
    NSDictionary *dictInfo = filterInfo.userInfo;
    self.filterDict = [dictInfo valueForKey:@"filterInfo"];
    if (APP_DEBUG_MODE) { NSLog(@"Filter info is %@",self.filterDict); }
    
    self.filterStatus = YES;
}

#pragma mark - UITableViewDataSouce

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if (tableView == self.tableView) {
        return 1;

    }else {
        return 3;
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (tableView == self.tableView) {
        return self.filterArray.count;
    }else {
        return 1;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell;
    
    if (tableView == self.tableView) {
        static NSString *cellId = @"cellIdentifier";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        }
        ServicesCategoriesInfo *infoObj = self.filterArray[indexPath.row];
        cell.textLabel.text = infoObj.categoryName;
        return cell;
    }else {
        
        if (indexPath.section == 0) {
            
            TopServicesTableCell *cell = [tableView dequeueReusableCellWithIdentifier:topServicesCellIdentifier];
            cell.servicesDelegate = self;
            cell.topServicesCollectionView.tag = indexPath.section;
            [cell configureTopServicesCollCell:self.array_TopServices];
            return cell;
            
        }else if (indexPath.section == 1) {
            AroundTableCell *aroundCell = [tableView dequeueReusableCellWithIdentifier:aroundCellIdentifier];
            if (!aroundCell){
                aroundCell = [[AroundTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:aroundCellIdentifier];
            }
            aroundCell.aroundDelegate = self;
            aroundCell.aroundYouCollectionView.tag = indexPath.section;
            [aroundCell configureCellWithInfo:self.array_AroundYou];
            return aroundCell;
        }else {
            SpecialOffersTableCell *cell = [tableView dequeueReusableCellWithIdentifier:specialOffersCellIdentifier];
            cell.specialOfferDelegate = self;
            cell.specialOffersCollectionView.tag = indexPath.section;
            [cell configureSpecialOffersCollCell:self.promotionImages];
            return cell;
        }
        
        return cell;
    }

}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == self.tableView) {
        return 50.0f;
    }else {
        if (indexPath.section == 0) {
            return 144.0f;
        }else if (indexPath.section == 1) {
            ServicesCategoriesInfo *infoObj = self.array_AroundYou[indexPath.row];
            return 180.0f + infoObj.providerAddressHeight + 5.0f + 42.0f + 30.0f;
        }else {
            return 200.0f;
        }
    }

}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if (tableView == self.tableView) {
        return 0.0f;
    }else {
        if (section == 0) {
            return 50.f;
        }else if (section == 0) {
            return 50.f;
        }else {
            return 50.f;
        }
    }

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.serviceCategoryTableView deselectRowAtIndexPath:indexPath animated:NO];

    if (tableView == self.tableView) {
        [self.popover dismiss];
        self.popover = nil;
        CategoryDetailController *detailController = [self.storyboard instantiateViewControllerWithIdentifier:kCategoryDetailController];
        ServicesCategoriesInfo *infoObj = self.filterArray[indexPath.row];
        detailController.categoryInfoObj = infoObj;
        detailController.currentLocation = self.currentLocation;
        [self.navigationController pushViewController:detailController animated:YES];
    }
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *headerView;
    
    if (tableView == self.serviceCategoryTableView) {
        
            UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 50.0f)];
            UILabel *tempLabel= [[UILabel alloc]initWithFrame:CGRectMake(15,10,tableView.frame.size.width-30,30)];
            tempLabel.textColor = [UIColor blackColor];
        tempLabel.font = [UIFont fontWithName:@"SegoeUI-Semibold" size:18.0];
            
            UIButton *viewAllButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [viewAllButton setTitle:NSLocalizedString(@"View All", @"") forState:UIControlStateNormal];
            
            if (section == 0) {
                tempLabel.text = @"Top Services"; //NSLocalizedString(@"Top Services", @"");
                viewAllButton.hidden = YES;
            }else if (section == 1) {
                tempLabel.text = @"Around you"; //NSLocalizedString(@"Around you", @"");
                viewAllButton.hidden = NO;
                [viewAllButton addTarget:self action:@selector(viewAroundYou:) forControlEvents:UIControlEventTouchUpInside];
            }else {
                tempLabel.text = @"Special package and offers"; //NSLocalizedString(@"Special package and offers", @"");
                viewAllButton.hidden = NO;
                [viewAllButton addTarget:self action:@selector(viewSpecialPackages:) forControlEvents:UIControlEventTouchUpInside];
            }

            NSString *languageCode =   [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];
            
            if ([languageCode isEqualToString:@"en"]) {
                
                viewAllButton.frame = CGRectMake(tableView.frame.size.width-90.0f, 10.0, 90.0, 30.0);
                
            }else if ([languageCode isEqualToString:@"ar"]) {
                
                viewAllButton.frame = CGRectMake(15.0f, 10.0, 90.0, 30.0);
            }
            viewAllButton.tag = section;
            [viewAllButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            viewAllButton.titleLabel.font = [UIFont fontWithName:@"SegoeUI" size:14.0];
            
            [headerView addSubview:viewAllButton];
        //    [viewAllButton.layer insertSublayer:[iBeautyUtility setGradientColor_Buttons:viewAllButton] atIndex:0];
        //    [iBeautyUtility setButtonCorderRound:viewAllButton];
            headerView.backgroundColor = [UIColor whiteColor];
            [headerView addSubview:tempLabel];
            return headerView;
    }
    return headerView;

}

- (void)viewAroundYou:(UIButton*)sender {
    
    //[self switchToServicesView:0];
    self.delegate.tabbarController.selectedIndex = 1;

}

- (void)viewSpecialPackages:(UIButton*)sender {
    
    self.delegate.tabbarController.selectedIndex = 2;
    
}

- (void)switchToServicesView:(NSInteger)selectedIndex {
    
    ProvidersViewController *detailController = [self.storyboard instantiateViewControllerWithIdentifier:kProvidersViewController];
    detailController.selectedIndex = selectedIndex;
    detailController.currentLocation = self.currentLocation;
    detailController.serviceCategoryArray = self.array_TopServices;
    [self.navigationController pushViewController:detailController animated:YES];
}

#pragma mark - CustomCell Delegate Method

- (void)selectTopServicesCollectionCell:(NSInteger)rowSelected section:(NSInteger)sectionIndex {
    
    if (APP_DEBUG_MODE) { NSLog(@"Row Selected : %ld in section : %ld ",(long)rowSelected, (long)sectionIndex);}
    
    [self switchToServicesView:rowSelected];
    
}

- (void)selectAroundYouCollectionCell:(NSInteger)rowSelected section:(NSInteger)sectionIndex {
    
    if (APP_DEBUG_MODE) {NSLog(@"Row Selected : %ld in section : %ld ",(long)rowSelected, (long)sectionIndex);}
    
    CategoryDetailController *detailController = [self.storyboard instantiateViewControllerWithIdentifier:kCategoryDetailController];
    ServicesCategoriesInfo *infoObj = self.array_AroundYou[rowSelected];
    detailController.categoryInfoObj = infoObj;
    detailController.currentLocation = self.currentLocation;
    [self.navigationController pushViewController:detailController animated:YES];
    
}

- (void)selectSpecialOffersCollectionCell:(NSInteger)rowSelected section:(NSInteger)sectionIndex {
    
    if (APP_DEBUG_MODE) { NSLog(@"Row Selected : %ld in section : %ld ",(long)rowSelected, (long)sectionIndex);}
    
    [self getOfferDetail:self.array_SpecialPackages[rowSelected]];
    
}

#pragma mark - Get Offer Detail

- (void)getOfferDetail:(PromotionalAds*)infoObj {
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_GET_OFFER_DETAIL];
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    NSString *businessId =  infoObj.businessID;
    NSString *offerId =  infoObj.offerId;

    NSMutableDictionary *dictInfo = [NSMutableDictionary dictionaryWithObjectsAndKeys:accessToken,@"Authorization",businessId,@"BusinessId",offerId,@"OfferId", nil];
    
    if (internetStatus != NotReachable) {
        
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonPostData:url params:dictInfo onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                @try {
                    if (completed) {
                        
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSMutableArray *infoArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE) { NSLog(@"Info Array is %@", infoArray); }
                        
                        OffersDetail *offersInfoDetail  = [OffersDetail getSharedInstance];
                        NSMutableArray *offersArray = [offersInfoDetail offersDetailInfoObj:infoArray];
                        if (APP_DEBUG_MODE) { NSLog(@"Offers Array ->>> %@", offersArray); }
                        if (offersArray.count >0) {
                            [self moveToViewOfferDetail:offersArray[0]];
                        }
                    }else {
                        if (APP_DEBUG_MODE) { NSLog(@"error is %@", error.localizedDescription);}
                        [SVProgressHUD dismiss];
                    }
                    
                } @catch (NSException *exception) {
                    //[EShopUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                    [SVProgressHUD dismiss];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
    
}

- (void)moveToViewOfferDetail:(OffersDetail*)offerDetail {
    
    OffersDetailViewController *offerDetailController = [self.storyboard instantiateViewControllerWithIdentifier:kOfferDetailController];
    offerDetailController.offerInfo = offerDetail;
    offerDetailController.offerTypeStatus = NO;
    offerDetailController.screenStatus = NO;
    
    [self.navigationController pushViewController:offerDetailController animated:YES];
}

#pragma mark - Search delegate methods

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    if (self.filterArray.count>0) {
        [self.filterArray removeAllObjects];
    }
    if (searchText.length>0) {
        
        for (ServicesCategoriesInfo *infoObj in self.array_AroundYou) {
            
            if([[infoObj.categoryName uppercaseString] rangeOfString:[searchText uppercaseString]].location != NSNotFound){
                
                if( ![self.filterArray containsObject:infoObj])
                    [self.filterArray addObject:infoObj];
            }
        }
    }
    [self setUpViewsAfterFilter];

}

- (void)setUpViewsAfterFilter {
 
    if (self.filterArray.count > 0) {
        [self.popover setHidden:NO];
    }else {
        [self.popover setHidden:YES];
    }
    [self.tableView reloadData];

}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    
    [self titleShowPopover];
    return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    // called twice every time
    [searchBar setShowsCancelButton:YES animated:YES];
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    // called only once
    self.searchStatus = YES;
    [searchBar setShowsCancelButton:YES animated:YES];
    return YES;
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    
    [self setUpProviderArray];
    [self.popover dismiss];
    [searchBar resignFirstResponder];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
    [self setUpProviderArray];
    [self.popover dismiss];
    [searchBar resignFirstResponder];
}

- (void)setUpProviderArray {
 
    self.searchStatus = false;
    [self setUpViewsAfterFilter];
}

#pragma mark - Pop Over Methods

- (void)resetPopover {
    self.popover = [DXPopover new];
    _popoverWidth = 280.0;
}

- (void)setUpPopOverTableView {
 
    UITableView *blueView = [[UITableView alloc] init];
    blueView.frame = CGRectMake(0, 0, _popoverWidth, 120);
    blueView.dataSource = self;
    blueView.delegate = self;
    self.tableView = blueView;
}

- (void)titleShowPopover {
    
    [self updateTableViewFrame];

    self.popover.contentInset = UIEdgeInsetsMake(20, 5.0, 20, 5.0);
    self.popover.backgroundColor = [UIColor whiteColor];

    CGPoint startPoint =
        CGPointMake(CGRectGetMidX(self.tableHeader.searchBar.frame), CGRectGetMaxY(self.tableHeader.frame) + 10);

    [self.popover showAtPoint:startPoint
               popoverPostion:DXPopoverPositionDown
              withContentView:self.tableView
                       inView:self.tabBarController.view];
    self.popover.didDismissHandler = ^{
    };
}

- (void)updateTableViewFrame {
    CGRect tableViewFrame = self.tableView.frame;
    tableViewFrame.size.width = _popoverWidth;
    self.tableView.frame = tableViewFrame;
    self.popover.contentInset = UIEdgeInsetsZero;
    self.popover.backgroundColor = [UIColor whiteColor];
}

- (void)bounceTargetView:(UIView *)targetView {
    targetView.transform = CGAffineTransformMakeScale(0.9, 0.9);
    [UIView animateWithDuration:0.5
                          delay:0.0
         usingSpringWithDamping:0.3
          initialSpringVelocity:5
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         targetView.transform = CGAffineTransformIdentity;
                     }
                     completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
