//
//  HomesViewController.h
//  iBeauty
//
//  Created by App Innovation on 23/10/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "TopServicesTableCell.h"
#import "AroundTableCell.h"
#import "SpecialOffersTableCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomesViewController : UIViewController<AroundCellDelegate,TopServicesDelegate,SpecialOffersDelegate,UITableViewDataSource,UITableViewDelegate>


@property (nonatomic,strong) NSMutableArray *promotionImages;
@property (nonatomic,strong) NSMutableArray *array_SpecialPackages;

@end

NS_ASSUME_NONNULL_END
