//
//  ServiceProvideAtController.m
//  iBeauty
//
//  Created by App Innovation on 19/11/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import "ServiceProvideAtController.h"
#import "ProvideServiceAt.h"
#import "BasketInfoObj.h"
#import "AddressesViewController.h"

@interface ServiceProvideAtController ()<UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *shopAddressLabel;
@property (weak, nonatomic) IBOutlet UIImageView *shopServiceImgView;
@property (weak, nonatomic) IBOutlet UIImageView *homeServiceImgView;

@property (weak, nonatomic) IBOutlet UIView *addInstructionView;
@property (weak, nonatomic) IBOutlet UITextView *noteTextView;
@property (weak, nonatomic) IBOutlet UILabel *placeHolderLabel;
@property (weak, nonatomic) IBOutlet UIButton *continueButton;

@property (weak, nonatomic) IBOutlet UIButton *shopServiceButton;
@property (weak, nonatomic) IBOutlet UIButton *homeServiceButton;
@property (nonatomic,assign) NSInteger radioBtnCheckStatus;
@property (nonatomic,weak) IBOutlet UILabel *shopServiceL;
@property (nonatomic,weak) IBOutlet UILabel *homeServiceL;
@property (nonatomic,strong) NSString * serviceProvideAt;
@property (nonatomic,weak)  IBOutlet UINavigationBar *navBar;
@property UIToolbar *toolBar;
@property (nonatomic, strong) UIBarButtonItem   *doneButton;


@end

@implementation ServiceProvideAtController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   
    [self setUpRadioButton:self.categoryInfoObj];
    self.continueButton.frame = [iBeautyUtility setUpFrame:self.continueButton.frame];
    [iBeautyUtility setButtonCorderRound:self.continueButton];
    //[self.continueButton.layer insertSublayer:[iBeautyUtility setGradientColor_Buttons:self.continueButton] atIndex:0];
    [self setupToolBarOnTextView];
    [self setGradient];
    
}

#pragma mark - Set Gradient

- (void)setGradient {
    
    self.navBar.frame = [iBeautyUtility setUpFrame:self.navBar.frame];
//    CAGradientLayer *gradient = [iBeautyUtility setNavigationGradient:self.navBar];
//    [self.navBar setBackgroundImage:[iBeautyUtility imageFromLayer:gradient] forBarMetrics:UIBarMetricsDefault];
    [self setUpAddInstructionView];
    self.shopAddressLabel.text = self.categoryInfoObj.location;
}

#pragma mark - Set up views

- (void)setUpAddInstructionView {
    
    CAShapeLayer *instructionViewBorder = [CAShapeLayer layer];
    instructionViewBorder.strokeColor = [UIColor darkGrayColor].CGColor;
    instructionViewBorder.fillColor = nil;
    instructionViewBorder.lineDashPattern = @[@6, @6];
    [self setAddInstructionViewFrame];
    instructionViewBorder.frame = self.addInstructionView.bounds;
    instructionViewBorder.path = [UIBezierPath bezierPathWithRect:self.addInstructionView.bounds].CGPath;
    [self.addInstructionView.layer addSublayer:instructionViewBorder];
    [self setNoteTextForNoteTextView];
}

- (void)setAddInstructionViewFrame {
    
    CGRect frame = self.addInstructionView.frame;
    frame.size.width = self.view.frame.size.width - 40.0f;
    self.addInstructionView.frame = frame;
}

- (void)setNoteTextForNoteTextView {

    if (self.basketInfoArray.count > 0) {
        BasketInfoObj *infoObj = self.basketInfoArray[0];
        if (infoObj.note != nil && ![infoObj.note isEqual:[NSNull null]]) {
            if (infoObj.note.length > 0) {
                self.noteTextView.text = infoObj.note;
                self.placeHolderLabel.hidden = YES;
            }
        }
    }
}

#pragma mark -  Back Action

-(IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -  Set Up Radio button views

- (void)setUpRadioButton:(ServicesCategoriesInfo*)catInfoObj {
    
    self.homeServiceButton.userInteractionEnabled = NO;
    self.shopServiceButton.userInteractionEnabled = NO;
    self.shopServiceL.textColor = [UIColor lightGrayColor];
    self.homeServiceL.textColor = [UIColor lightGrayColor];
    self.shopServiceImgView.image = [UIImage imageNamed:@"UncheckGrey"];
    self.homeServiceImgView.image = [UIImage imageNamed:@"UncheckGrey"];
    
    NSMutableArray *provideServiceAt = [catInfoObj.provideServiceAt mutableCopy];
    NSMutableArray *tempArray = [NSMutableArray new];
    
    for (ProvideServiceAt * provideServiceObj in provideServiceAt) {
        [tempArray addObject:provideServiceObj.name];
    }
    
    if (tempArray.count == 1) {
        self.serviceProvideAt = tempArray[0];
        if (APP_DEBUG_MODE){ NSLog(@"Service provider at value is ->>>>> %@",self.serviceProvideAt); }
        
        if ([self.serviceProvideAt isEqualToString:@"Home"]) {
            self.homeServiceButton.userInteractionEnabled = YES;
            self.homeServiceL.textColor = [UIColor blackColor];
            self.homeServiceImgView.image = [UIImage imageNamed:@"Check"];
            self.serviceProvideAt = @"Home";
            
        }else {
            self.shopServiceButton.userInteractionEnabled = YES;
            self.shopServiceL.textColor = [UIColor blackColor];
            self.shopServiceImgView.image = [UIImage imageNamed:@"Check"];
            self.serviceProvideAt = @"Shop";
        }
    }else {
        self.homeServiceButton.userInteractionEnabled = YES;
        self.homeServiceL.textColor = [UIColor blackColor];
        self.homeServiceImgView.image = [UIImage imageNamed:@"Uncheck"];
        self.shopServiceButton.userInteractionEnabled = YES;
        self.shopServiceL.textColor = [UIColor blackColor];
        self.shopServiceImgView.image = [UIImage imageNamed:@"Check"];
        self.serviceProvideAt = @"Shop";
    }
    
}

#pragma mark -  Select Shop/Home radio buttons

- (IBAction)selectShopHomeServices:(UIButton*)sender {
    
    switch ([sender tag]) {
            
        case 0:
            [self.shopServiceButton setSelected:YES];
            [self.homeServiceButton setSelected:NO];
            
            [self.shopServiceImgView setImage:[UIImage imageNamed:@"Check"]];
            
            if (self.homeServiceButton.userInteractionEnabled == NO) {
                [self.homeServiceImgView setImage:[UIImage imageNamed:@"UncheckGrey"]];
            }else {
                [self.homeServiceImgView setImage:[UIImage imageNamed:@"Uncheck"]];
            }
            self.radioBtnCheckStatus = 1;
            self.serviceProvideAt = @"Shop";
            break;
            
        case 1:
            [self.homeServiceButton setSelected:YES];
            [self.shopServiceButton setSelected:NO];
            [self.homeServiceImgView setImage:[UIImage imageNamed:@"Check"]];
            
            if (self.shopServiceButton.userInteractionEnabled == NO) {
                [self.shopServiceImgView setImage:[UIImage imageNamed:@"UncheckGrey"]];
            }else {
                [self.shopServiceImgView setImage:[UIImage imageNamed:@"Uncheck"]];
            }
            self.radioBtnCheckStatus = 2;
            self.serviceProvideAt = @"Home";
            break;
            
        default:
            break;
    }
    
}

#pragma mark -  Continue button action

- (IBAction)continueAction:(UIButton*)sender {
    
    [self updateBasketFor_NoteAndServiceProvideAt];
}

#pragma mark - Update Basket for Note and Service Provide AT

- (void)updateBasketFor_NoteAndServiceProvideAt {
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_UPDATE_BASKET];
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSDictionary *dictInfo = [self getParametersInfo_For_UpdateNoteAndServiceProvideAT];
    
    if (internetStatus != NotReachable) {
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        [handler jsonPostData:url params:dictInfo onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                @try {
                    if (completed) {
                        
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSDictionary *tempInfoDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE) { NSLog(@"Temp Info Dict is %@", tempInfoDict);}
                        
                        [self checkForServiceProvideAtType];
                        
                    }else {
                        if (APP_DEBUG_MODE) { NSLog(@"error is %@", error.localizedDescription); }
                    }
                    
                } @catch (NSException *exception) {
                    [iBeautyUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}

- (NSMutableDictionary*)getParametersInfo_For_UpdateNoteAndServiceProvideAT {
    
    NSMutableDictionary *basketDict = [[NSMutableDictionary alloc]init];
    NSString *businessId = self.categoryInfoObj.Id;
    [basketDict setValue:businessId forKey:@"BusinessId"];
    
    NSString *customerID = [[NSUserDefaults standardUserDefaults]valueForKey:KUserId];
    [basketDict setValue:customerID forKey:@"CustomerId"];
    
    self.basketID = [[NSUserDefaults standardUserDefaults]valueForKey:@"BasketId"];
    [basketDict setObject:self.basketID forKey:@"BasketId"];
    
    NSMutableArray *appointmentsArray = [NSMutableArray new];
    for (BasketInfoObj *detailInfo in self.basketInfoArray) {
        NSMutableDictionary *appointmentDict = [[NSMutableDictionary alloc]init];
        [appointmentDict setValue:detailInfo.serviceId forKey:@"ServiceId"];
        [appointmentDict setValue:detailInfo.startDateTime forKey:@"StartDateTime"];
        [appointmentDict setValue:detailInfo.endDateTime forKey:@"EndDateTime"];
        [appointmentDict setValue:detailInfo.staffInfo.staffId forKey:@"StaffId"];
        [appointmentDict setValue:detailInfo.appointmentId forKey:@"Id"];
        [appointmentsArray addObject:appointmentDict];
    }
    [basketDict setObject:appointmentsArray forKey:@"UpdatedAppointments"];
    [basketDict setObject:@"" forKey:@"NewAppointments"];
    [basketDict setObject:@"" forKey:@"RemovedAppointments"];
    [basketDict setObject:@"" forKey:@"Products"];
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    [basketDict setValue:accessToken forKey:@"Authorization"];
    [basketDict setValue:self.serviceProvideAt forKey:@"ServiceProvideAt"];
    [basketDict setValue:self.noteTextView.text forKey:@"Note"];
    
    NSString *offerId = @"";
    if (self.screenStatus == YES) {
        offerId = self.offerInfoObj.offerId;
    }
    [basketDict setValue:offerId forKey:@"OfferId"];
    
    if ([self.serviceProvideAt isEqualToString:@"Shop"]) {
        
        NSDictionary *addressDict = [self getDictInfo];
        [basketDict setObject:addressDict forKey:@"CustomerAddress"];
    }
    
    return basketDict;
    
}

- (NSDictionary*)getDictInfo {
    
    NSDictionary *profileInfo = [[NSUserDefaults standardUserDefaults]valueForKey:KLoginInfo];
    
    NSString *nameStr = [iBeautyUtility trimString:[profileInfo valueForKey:@"Name"]];
    NSString *contactNoStr = @"";
    if ([profileInfo valueForKey:@"PhoneNumber"]  != nil && ![[profileInfo valueForKey:@"PhoneNumber"] isEqual:[NSNull null]]) {
        contactNoStr = [iBeautyUtility trimString:[profileInfo valueForKey:@"PhoneNumber"]];
    }
    NSString *userID = [iBeautyUtility trimString:[profileInfo valueForKey:@"UserID"]];
    
    NSString *addressStr = @"";
    AppDelegate *degate = [AppDelegate sharedInstance];
    NSString *latStr = [iBeautyUtility trimString:degate.latitude];
    NSString *longStr = [iBeautyUtility trimString:degate.longitude];
    NSString *emailStr = @"";
    if ([profileInfo valueForKey:@"email"]  != nil && ![[profileInfo valueForKey:@"email"] isEqual:[NSNull null]]) {
        emailStr = [iBeautyUtility trimString:[profileInfo valueForKey:@"email"]];
    }
    
    NSDictionary *userDetailDict = [[NSDictionary alloc]init];
    
    userDetailDict = [NSDictionary dictionaryWithObjectsAndKeys:userID,@"UserId",@"",@"Id", nameStr,@"UserName",contactNoStr,@"ContactNumber",addressStr,@"Address",latStr,@"Latitude",longStr,@"Longitude",emailStr,@"Email",nil];
    
    return userDetailDict;
    
}

#pragma mark - Check for Service Provide AT

- (void)checkForServiceProvideAtType {
    
    if ([self.serviceProvideAt isEqualToString:@"Home"]) {
        
        [self switchToMyAddressesView];
        
    }else {
        [self updateBasketFor_customerAddress];
    }
    
}

#pragma mark - Switch To My Addresses

- (void)switchToMyAddressesView {
        
    AddressesViewController *addressController = [self.storyboard instantiateViewControllerWithIdentifier:kAddressesViewController];
    addressController.serviceDetailsArray = self.basketInfoArray;
    addressController.categoryInfoObj = self.categoryInfoObj;
    addressController.serviceProvideAtString = self.serviceProvideAt;
    addressController.noteString = self.noteTextView.text;
    addressController.offerInfoObj = self.offerInfoObj;
    addressController.screenStatus = self.screenStatus;
    addressController.pickStatus = self.radioBtnCheckStatus;
    
    [self.navigationController pushViewController:addressController animated:YES];
    
}

#pragma mark - Update Basket for Customer Address

- (void)updateBasketFor_customerAddress {
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_UPDATE_BASKET];
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSDictionary *dictInfo = [self getParametersInfo_For_UpdateNoteAndServiceProvideAT];
    
    if (internetStatus != NotReachable) {
        
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonPostData:url params:dictInfo onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                
                @try {
                    if (completed) {
                        
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSMutableArray *tempInfoArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE) { NSLog(@"Temp Info Array is %@", tempInfoArray); }
                        
                        if (self.basketInfoArray.count>0) {
                            [self continueToGetPricing];
                        }
                        
                    }else {
                        if (APP_DEBUG_MODE) { NSLog(@"error is %@", error.localizedDescription); }
                    }
                    
                } @catch (NSException *exception) {
                    [iBeautyUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}

#pragma mark - Get pricing

- (void)continueToGetPricing {
    
    PriceCartViewController *priceCartObj = [self.storyboard instantiateViewControllerWithIdentifier:kPriceCartViewController];
    priceCartObj.serviceDetailsArray = self.basketInfoArray;
    priceCartObj.categoryInfoObj = self.categoryInfoObj;
    priceCartObj.screenStatus = self.screenStatus;
    priceCartObj.pickStatus = self.radioBtnCheckStatus;
    [self.navigationController pushViewController:priceCartObj animated:YES];
}

#pragma mark - set up Tool Bar On Text View

- (void)setupToolBarOnTextView {
    
    self.toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50)];
    self.toolBar.barStyle = UIBarStyleDefault;
    [self.toolBar sizeToFit];
    [self.toolBar setBackgroundColor:[UIColor lightGrayColor]];
    [self buttonWithinToolbar];
    
    self.noteTextView.inputAccessoryView = self.toolBar;
    
}

- (void)buttonWithinToolbar {
    
    self.doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", @"") style:UIBarButtonItemStyleDone target:self action:@selector(doneAction:)];
    
    UIBarButtonItem *flexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [self.toolBar setItems:@[flexibleItem,self.doneButton] animated:NO];
}

- (void)doneAction:(id)sender {
    
    [self.noteTextView resignFirstResponder];
    
}

#pragma mark - Text view delegates

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    
    //self.noteTextView.text = @"";
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    
    if (![self.noteTextView hasText]) {
        self.placeHolderLabel.hidden = NO;
    }
}

- (void) textViewDidChange:(UITextView *)textView
{
    if(![self.noteTextView hasText]) {
        self.placeHolderLabel.hidden = NO;
    }
    else{
        self.placeHolderLabel.hidden = YES;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
