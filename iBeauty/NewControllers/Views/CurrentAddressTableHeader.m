//
//  CurrentAddressTableHeader.m
//  iBeauty
//
//  Created by App Innovation on 25/10/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import "CurrentAddressTableHeader.h"
#import "UILabel+FlexibleWidHeight.h"

@interface CurrentAddressTableHeader  ()

@property (nonatomic,weak ) IBOutlet UILabel * currentAddressLbl;
@property (nonatomic,weak ) IBOutlet UIView * bkgView;

@end

@implementation CurrentAddressTableHeader

+ (instancetype)currentAddressTableHeaderView:(NSString*)locationAddress {
    
    CurrentAddressTableHeader *tableHeaderView = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil] firstObject];
    if (locationAddress.length > 0) {
        tableHeaderView.currentAddressLbl.text = locationAddress;
    }else {
        tableHeaderView.currentAddressLbl.text = @"Current location";
    }
    
//    CGFloat addressHeight = [UILabel heightOfTextForString:locationAddress andFont:[UIFont systemFontOfSize:17] maxSize:CGSizeMake([UIScreen mainScreen].bounds.size.width- 62.0f, MAXFLOAT)];
//
//    tableHeaderView.currentAddressLbl.translatesAutoresizingMaskIntoConstraints = YES;
//    tableHeaderView.bkgView.translatesAutoresizingMaskIntoConstraints = YES;
//
//    if (locationAddress.length < 1) {
//        [tableHeaderView.currentAddressLbl setFrame:CGRectMake(50.0f, 41.0f , [UIScreen mainScreen].bounds.size.width-62.0f, 21.0f)];
//    }else {
//        [tableHeaderView.currentAddressLbl setFrame:CGRectMake(50.0f, 41.0f , [UIScreen mainScreen].bounds.size.width-62.0f, addressHeight)];
//    }
//
//    [tableHeaderView.bkgView setFrame:CGRectMake(50.0f, 0.0f , [UIScreen mainScreen].bounds.size.width, tableHeaderView.currentAddressLbl.frame.origin.y + tableHeaderView.currentAddressLbl.frame.size.height + 10.0f)];

    return tableHeaderView;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
