//
//  NearByAddressHeader.m
//  iBeauty
//
//  Created by App Innovation on 24/10/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import "NearByAddressHeader.h"

@implementation NearByAddressHeader

+ (instancetype)nearByAddressHeaderView:(NSString*)locationAddress {
    
    NearByAddressHeader *tableHeaderView = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil] firstObject];
    tableHeaderView.locAddressLabel.text = locationAddress;
    
    return tableHeaderView;
    
}


@end
