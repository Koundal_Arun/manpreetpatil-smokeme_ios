//
//  AroundTableCell.h
//  iBeauty
//
//  Created by App Innovation on 23/10/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol AroundCellDelegate <NSObject>

- (void)selectAroundYouCollectionCell:(NSInteger)rowSelected section:(NSInteger)sectionIndex;

@end

@interface AroundTableCell : UITableViewCell

@property (weak, nonatomic) id <AroundCellDelegate> aroundDelegate;

@property (nonatomic, weak) IBOutlet UICollectionView *aroundYouCollectionView;

- (void)configureCellWithInfo:(NSMutableArray*)categoriesInfo ;

@end

NS_ASSUME_NONNULL_END
