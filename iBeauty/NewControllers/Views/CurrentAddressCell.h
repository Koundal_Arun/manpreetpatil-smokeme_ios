//
//  CurrentAddressCell.h
//  iBeauty
//
//  Created by App Innovation on 25/10/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddressModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CurrentAddressCell : UITableViewCell

@property (nonatomic,weak ) IBOutlet UIButton * selectAddrssBtn;
@property (nonatomic,weak ) IBOutlet UIImageView * addressImgView;
@property (nonatomic,weak ) IBOutlet UIButton * pinLocationButton;

- (void)configureCurrentAddressCell:(AddressModel*)addressObj;

@end

NS_ASSUME_NONNULL_END
