//
//  CurrentAddressHeader.m
//  iBeauty
//
//  Created by App Innovation on 24/10/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import "CurrentAddressHeader.h"
#import "CommonClass.h"

@implementation CurrentAddressHeader


+ (instancetype)currentAddressHeaderView:(NSString*)locationAddress {
    
    CurrentAddressHeader *tableHeaderView = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil] firstObject];
    if (locationAddress.length > 0) {
        tableHeaderView.locAddressLabel.text = locationAddress;
    }else {
        tableHeaderView.locAddressLabel.text = @"Current location";
    }
    
   NSDictionary *profileInfo = [[NSUserDefaults standardUserDefaults] valueForKey:KLoginInfo];
    tableHeaderView.userNameLabel.text = [NSString stringWithFormat:@"Hello, %@",[profileInfo valueForKey:KProfileName]];

    return tableHeaderView;
}


@end
