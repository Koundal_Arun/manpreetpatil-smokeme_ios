//
//  AroundCollectionCell.m
//  iBeauty
//
//  Created by App Innovation on 23/10/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import "AroundCollectionCell.h"
#import "ProvideServiceAt.h"
#import <HCSStarRatingView/HCSStarRatingView.h>

@interface AroundCollectionCell ()

@property (nonatomic,weak ) IBOutlet UIImageView * categoryImageView;
@property (nonatomic,weak ) IBOutlet UILabel * categoryNameLable;
@property (nonatomic,weak ) IBOutlet UILabel * distanceLable;
@property (nonatomic,weak ) IBOutlet UILabel * locationLable;
@property (nonatomic,weak ) IBOutlet UILabel * ratingLable;
@property (nonatomic,weak ) IBOutlet UIButton * homeButton;
@property (nonatomic,weak ) IBOutlet UIButton * shopButton;
@property (nonatomic,weak ) IBOutlet UIView * bkgView;
@property (nonatomic,weak ) IBOutlet UIImageView * ratingImageView;
@property (nonatomic,weak ) IBOutlet UIButton * bookButton;
@property (nonatomic,weak ) IBOutlet UIView * homeShopView;

@property (nonatomic,strong ) IBOutlet HCSStarRatingView * ratingView;

@end

@implementation AroundCollectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)configureCollectionCell:(ServicesCategoriesInfo*)catInfoObj {
    
    [self.categoryImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",IMAGE_BASE_URL,catInfoObj.categoryImage]] placeholderImage:[UIImage imageNamed:@""]];
    self.categoryNameLable.text = catInfoObj.categoryName;
    
    NSString *distance ;
    
    if (catInfoObj.distance != nil) {
        distance = [NSString stringWithFormat:@"%@ Km",catInfoObj.distance];
    }else {
        distance = @"";
    }
    [self setProviderAddressHeight:catInfoObj];
    self.locationLable.text = [NSString stringWithFormat:@"%@ (%@)",catInfoObj.location,distance];

    [self setUpProvideServiceAtViews:catInfoObj];
    [self setUpStarRatingView:catInfoObj];
}

- (void)setProviderAddressHeight:(ServicesCategoriesInfo*)catInfoObj {
    
    self.bkgView.translatesAutoresizingMaskIntoConstraints = YES;
    self.homeShopView.translatesAutoresizingMaskIntoConstraints = YES;
    self.locationLable.translatesAutoresizingMaskIntoConstraints = YES;
    
    self.locationLable.frame  = CGRectMake(8.0f, self.locationLable.frame.origin.y, SCREEN_WIDTH - 96.0f, catInfoObj.providerAddressHeight);
    self.homeShopView.frame  = CGRectMake(0.0f, self.locationLable.frame.origin.y + self.locationLable.frame.size.height + 5.0f, SCREEN_WIDTH - 80.0f, 42.0f);
    self.bkgView.frame  = CGRectMake(15.0f, 15.0f, SCREEN_WIDTH - 80.0f,180.0f + catInfoObj.providerAddressHeight + 5 + 42.0f);

}

- (void)setUpProvideServiceAtViews:(ServicesCategoriesInfo*)catInfoObj {
    
    [iBeautyUtility setButtonCorderRound:self.homeButton];
    [iBeautyUtility setButtonCorderRound:self.shopButton];
    [self.homeButton setHidden:YES];
    [self.shopButton setHidden:YES];
    
    NSMutableArray *provideServiceAt = catInfoObj.provideServiceAt;
    
    for (ProvideServiceAt * provideServiceObj in provideServiceAt) {
        
        if ( [provideServiceObj.name isEqualToString:@"Home"]) {
            
            [self.homeButton setHidden:NO];
        }
        if ( [provideServiceObj.name isEqualToString:@"Shop"]) {
            
            [self.shopButton setHidden:NO];
        }
    }
    
}

- (void)setUpStarRatingView:(ServicesCategoriesInfo*)catInfoObj {
    
    [self.ratingImageView setHidden:YES];
    [self.ratingLable setHidden:YES];
    
    if (catInfoObj.reviewsMeta != nil && ![[catInfoObj.reviewsMeta valueForKey:@"Rating"] isEqual:[NSNull null]]) {
        
        [self.ratingView setHidden:NO];
        self.ratingView.userInteractionEnabled = false;
        CGFloat ratingValue = [[catInfoObj.reviewsMeta valueForKey:@"Rating"]floatValue];
        self.ratingView.value = ratingValue;
        self.ratingView.maximumValue = 5;
        self.ratingView.minimumValue = 0;
        self.ratingView.tintColor = [UIColor iBeautyThemeColor];
        self.ratingView.allowsHalfStars = YES;
        self.ratingView.accurateHalfStars = YES;
        self.ratingView.filledStarImage = [[UIImage imageNamed:@"star"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        self.ratingView.emptyStarImage = [[UIImage imageNamed:@"star_grey"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        
    }else {
        [self.ratingView setHidden:YES];
    }
    
}


@end
