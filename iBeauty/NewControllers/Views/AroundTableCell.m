//
//  AroundTableCell.m
//  iBeauty
//
//  Created by App Innovation on 23/10/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import "AroundTableCell.h"
#import "Constants.h"
#import "AroundCollectionCell.h"
#import "ServicesCategoriesInfo.h"

static NSString *categoryCellIdentifier = @"AroundCollectionCell";

@interface AroundTableCell()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (strong, nonatomic) NSMutableArray *collectionData;

@end

@implementation AroundTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)configureCellWithInfo:(NSMutableArray*)categoriesInfo {
    
    if (APP_DEBUG_MODE) { NSLog(@"Array is %@",categoriesInfo);}
    _collectionData = categoriesInfo ;
    [self setUpAroundCollectionCell];
    [self.aroundYouCollectionView reloadData];
}

- (void)setUpAroundCollectionCell {
    
    UINib *collectionNib = [UINib nibWithNibName:@"AroundCollectionCell" bundle:nil];
    [self.aroundYouCollectionView registerNib:collectionNib forCellWithReuseIdentifier:categoryCellIdentifier];
}

#pragma mark - UICollectionViewDataSource methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.collectionData.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    AroundCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:categoryCellIdentifier forIndexPath:indexPath];
    ServicesCategoriesInfo *infoObj = self.collectionData[indexPath.row];
    [cell configureCollectionCell:infoObj];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat screenWidth = CGRectGetWidth([UIScreen mainScreen].bounds);
    
    CGFloat itemWidth = 0.0f;
    CGFloat itemHeight = 0.0f;
    
    NSString *deviceType = [UIDevice currentDevice].model;
    
    if ([deviceType isEqualToString:@"iPhone"]) {
        itemWidth = floor(screenWidth - 50.0f);
        ServicesCategoriesInfo *infoObj = self.collectionData[indexPath.row];
        itemHeight = 180.0f + infoObj.providerAddressHeight + 5.0f + 42.0f + 30.0f;
    }else {
        itemWidth = floor(screenWidth -400.0f);
        itemHeight = 360.0f;
    }
    return CGSizeMake(itemWidth, itemHeight);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 5.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 5.0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(5.0, 5.0, 5.0, 5.0);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if ( (self.aroundDelegate != nil) && ([self.aroundDelegate respondsToSelector:@selector(selectAroundYouCollectionCell:section:)]) ) {
        [self.aroundDelegate selectAroundYouCollectionCell:indexPath.item section:collectionView.tag];
        
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
