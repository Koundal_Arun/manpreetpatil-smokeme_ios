//
//  TopServicesTableCell.m
//  iBeauty
//
//  Created by App Innovation on 23/10/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import "TopServicesTableCell.h"
#import "Constants.h"
#import "TopServicesCollectionCell.h"
#import "ServicesCategoriesInfo.h"

static NSString *topServicesIdentifier = @"TopServicesCollectionCell";

@interface TopServicesTableCell()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (strong, nonatomic) NSMutableArray *collectionData;

@end

@implementation TopServicesTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)configureTopServicesCollCell:(NSMutableArray*)categoriesInfo {
    
    if (APP_DEBUG_MODE) { NSLog(@"Array is %@",categoriesInfo);}
    _collectionData = categoriesInfo ;
    [self setUpTopServicesCollectionCell];
    [self.topServicesCollectionView reloadData];
}

- (void)setUpTopServicesCollectionCell {
    
    UINib *collectionNib = [UINib nibWithNibName:@"TopServicesCollectionCell" bundle:nil];
    [self.topServicesCollectionView registerNib:collectionNib forCellWithReuseIdentifier:topServicesIdentifier];
}

#pragma mark - UICollectionViewDataSource methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.collectionData.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    TopServicesCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:topServicesIdentifier forIndexPath:indexPath];
    ServicesCategoriesInfo *infoObj = self.collectionData[indexPath.row];
    [cell configureCollectionCell:infoObj];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    //CGFloat screenWidth = CGRectGetWidth([UIScreen mainScreen].bounds);
    
    CGFloat itemWidth = 0.0f;
    CGFloat itemHeight = 0.0f;
    
    NSString *deviceType = [UIDevice currentDevice].model;
    
    if ([deviceType isEqualToString:@"iPhone"]) {
        itemWidth = 105.0f;
        itemHeight = 144.0f;
    }else {
        itemWidth = 185.0f;
        itemHeight = 225.0f;
    }
    
    return CGSizeMake(itemWidth, itemHeight);
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 5.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 5.0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(5.0, 5.0, 5.0, 5.0);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if ( (self.servicesDelegate != nil) && ([self.servicesDelegate respondsToSelector:@selector(selectTopServicesCollectionCell:section:)]) ) {
        [self.servicesDelegate selectTopServicesCollectionCell:indexPath.item section:collectionView.tag];
        
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
