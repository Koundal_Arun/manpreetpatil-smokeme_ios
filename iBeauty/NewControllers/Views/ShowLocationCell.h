//
//  ShowLocationCell.h
//  iBeauty
//
//  Created by App Innovation on 15/11/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "ServicesCategoriesInfo.h"

NS_ASSUME_NONNULL_BEGIN

@protocol ShowDetailDelegate <NSObject>

- (void)showBusinessDetail:(NSInteger)SenderTag;

@end

@interface ShowLocationCell : UICollectionViewCell

@property (nonatomic,weak ) IBOutlet UIButton * bookButton;

- (void)configureShowLocationCollectionCell:(ServicesCategoriesInfo*)catInfoObj;

@property (nonatomic, weak) id <ShowDetailDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
