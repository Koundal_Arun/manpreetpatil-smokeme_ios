//
//  CurrentAddressTableFooter.m
//  iBeauty
//
//  Created by App Innovation on 25/10/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import "CurrentAddressTableFooter.h"

@implementation CurrentAddressTableFooter

+ (instancetype)currentAddressTableFooterView {
    
    CurrentAddressTableFooter *tableFooterView = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil] firstObject];
    
    return tableFooterView;
}

@end
