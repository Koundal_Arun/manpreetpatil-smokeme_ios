//
//  CurrentAddressCell.m
//  iBeauty
//
//  Created by App Innovation on 25/10/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import "CurrentAddressCell.h"
#import "UILabel+FlexibleWidHeight.h"
#import "NSString+iBeautyString.h"

@interface CurrentAddressCell ()

@property (nonatomic,weak ) IBOutlet UILabel * addressTypeLbl;
@property (nonatomic,weak ) IBOutlet UILabel * addressLabel;
@property (nonatomic,weak ) IBOutlet UILabel * phoneNumberLbl;
@property (nonatomic,weak ) IBOutlet UIView * addressbackg_View;
@property (nonatomic,weak ) IBOutlet UILabel * pinLocLabel;

@end

@implementation CurrentAddressCell


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)configureCurrentAddressCell:(AddressModel*)addressObj {
    
    self.addressTypeLbl.text = addressObj.addressTitle;
    self.addressLabel.text = addressObj.address;
    NSString *phoneNoStr = addressObj.contactNumber;
    
    NSMutableAttributedString *phoneNoString = [phoneNoStr getOrdersAttributedTextWithId:@"Phone no - " openingStatus:phoneNoStr screenType:3];
    self.phoneNumberLbl.attributedText = phoneNoString;
    
    self.addressLabel.translatesAutoresizingMaskIntoConstraints = YES;
    self.phoneNumberLbl.translatesAutoresizingMaskIntoConstraints = YES;
    self.addressbackg_View.translatesAutoresizingMaskIntoConstraints = YES;

    CGFloat addressHeight = [UILabel heightOfTextForString:addressObj.address andFont:[UIFont fontWithName:@"SegoeUI" size:15.0] maxSize:CGSizeMake([UIScreen mainScreen].bounds.size.width- 60.0f, MAXFLOAT)];
    
    [self.addressLabel setFrame:CGRectMake(50.0f, 41.0f , [UIScreen mainScreen].bounds.size.width-60.0f, addressHeight)];
    [self.phoneNumberLbl setFrame:CGRectMake(50.0f, 41.0f + self.addressLabel.frame.size.height + 5.0f , [UIScreen mainScreen].bounds.size.width-60.0f, self.phoneNumberLbl.frame.size.height)];

//   [self.addressbackg_View setFrame:CGRectMake(0.0f, 0.0f, [UIScreen mainScreen].bounds.size.width, self.phoneNumberLbl.frame.origin.y + self.phoneNumberLbl.frame.size.height + 10.0f)];
    [self.addressbackg_View setFrame:CGRectMake(0.0f, 0.0f, [UIScreen mainScreen].bounds.size.width,addressObj.cellHeight)];
    [self setUpPinLocLabel:addressObj];
}

- (void)setUpPinLocLabel:(AddressModel*)addressObj {
    
    if ([addressObj.latitude isEqualToString:@"0.0"]) {
        self.pinLocationButton.hidden = NO;
    }else {
        self.pinLocationButton.hidden = YES;
    }
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
