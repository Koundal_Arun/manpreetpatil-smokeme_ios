//
//  SpecialOffersCollectionCell.m
//  iBeauty
//
//  Created by App Innovation on 23/10/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import "SpecialOffersCollectionCell.h"

@interface SpecialOffersCollectionCell ()

@property (nonatomic,weak ) IBOutlet UIImageView * offerImageView;

@end

@implementation SpecialOffersCollectionCell


- (void)configureSpecialOffersCollectionCell:(NSString*)imageStr {
    
    [self.offerImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",imageStr]] placeholderImage:[UIImage imageNamed:@""]];
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

@end
