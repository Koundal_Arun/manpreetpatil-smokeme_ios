//
//  TopServicesCollectionCell.h
//  iBeauty
//
//  Created by App Innovation on 23/10/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "ServicesCategoriesInfo.h"

NS_ASSUME_NONNULL_BEGIN

@interface TopServicesCollectionCell : UICollectionViewCell


- (void)configureCollectionCell:(ServicesCategoriesInfo*)catInfoObj;


@end

NS_ASSUME_NONNULL_END
