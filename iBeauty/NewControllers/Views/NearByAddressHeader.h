//
//  NearByAddressHeader.h
//  iBeauty
//
//  Created by App Innovation on 24/10/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NearByAddressHeader : UIView

@property (nonatomic,weak) IBOutlet UIButton *changeLocBtn;
@property (nonatomic,weak) IBOutlet UILabel  *locAddressLabel;
@property (nonatomic,weak) IBOutlet UISearchBar  *searchBar;

+ (instancetype)nearByAddressHeaderView:(NSString*)locationAddress;

@end

NS_ASSUME_NONNULL_END
