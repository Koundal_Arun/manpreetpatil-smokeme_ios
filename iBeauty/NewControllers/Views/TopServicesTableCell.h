//
//  TopServicesTableCell.h
//  iBeauty
//
//  Created by App Innovation on 23/10/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol TopServicesDelegate <NSObject>

- (void)selectTopServicesCollectionCell:(NSInteger)rowSelected section:(NSInteger)sectionIndex;

@end

@interface TopServicesTableCell : UITableViewCell

@property (weak, nonatomic) id <TopServicesDelegate> servicesDelegate;

@property (nonatomic, weak) IBOutlet UICollectionView *topServicesCollectionView;

- (void)configureTopServicesCollCell:(NSMutableArray*)categoriesInfo;

@end

NS_ASSUME_NONNULL_END
