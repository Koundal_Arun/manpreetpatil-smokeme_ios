//
//  SpecialOffersTableCell.h
//  iBeauty
//
//  Created by App Innovation on 23/10/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol SpecialOffersDelegate <NSObject>

- (void)selectSpecialOffersCollectionCell:(NSInteger)rowSelected section:(NSInteger)sectionIndex;

@end

@interface SpecialOffersTableCell : UITableViewCell

@property (weak, nonatomic) id <SpecialOffersDelegate> specialOfferDelegate;

@property (nonatomic, weak) IBOutlet UICollectionView *specialOffersCollectionView;

- (void)configureSpecialOffersCollCell:(NSMutableArray*)categoriesInfo;

@end

NS_ASSUME_NONNULL_END
