//
//  ShowLocationCell.m
//  iBeauty
//
//  Created by App Innovation on 15/11/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import "ShowLocationCell.h"
#import "ProvideServiceAt.h"
#import <HCSStarRatingView/HCSStarRatingView.h>


@interface ShowLocationCell ()

@property (nonatomic,weak ) IBOutlet UIImageView * categoryImageView;
@property (nonatomic,weak ) IBOutlet UILabel * categoryNameLable;
@property (nonatomic,weak ) IBOutlet UILabel * distanceLable;
@property (nonatomic,weak ) IBOutlet UILabel * locationLable;
@property (nonatomic,weak ) IBOutlet UIButton * homeButton;
@property (nonatomic,weak ) IBOutlet UIButton * shopButton;
@property (nonatomic,weak ) IBOutlet UIView * bkgView;
@property (nonatomic,strong ) IBOutlet HCSStarRatingView * ratingView;


@end

@implementation ShowLocationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)configureShowLocationCollectionCell:(ServicesCategoriesInfo*)catInfoObj {
    
    [self.categoryImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",IMAGE_BASE_URL,catInfoObj.categoryImage]] placeholderImage:[UIImage imageNamed:@""]];
    self.categoryNameLable.text = catInfoObj.categoryName;
    
    NSString *distance ;
    
    if (catInfoObj.distance != nil) {
        distance = [NSString stringWithFormat:@"%@ Km",catInfoObj.distance];
    }else {
        distance = @"";
    }
    self.locationLable.text = catInfoObj.location;
    self.distanceLable.text = distance;

    [self setUpProvideServiceAtViews:catInfoObj];
    [self setUpStarRatingView:catInfoObj];
}

- (void)setUpProvideServiceAtViews:(ServicesCategoriesInfo*)catInfoObj {
    
    [iBeautyUtility setButtonCorderRound:self.homeButton];
    [iBeautyUtility setButtonCorderRound:self.shopButton];
    [self.homeButton setHidden:YES];
    [self.shopButton setHidden:YES];
    
    NSMutableArray *provideServiceAt = catInfoObj.provideServiceAt;
    
    for (ProvideServiceAt * provideServiceObj in provideServiceAt) {
        
        if ( [provideServiceObj.name isEqualToString:@"Home"]) {
            
            [self.homeButton setHidden:NO];
        }
        if ( [provideServiceObj.name isEqualToString:@"Shop"]) {
            
            [self.shopButton setHidden:NO];
        }
    }
    
}

- (void)setUpStarRatingView:(ServicesCategoriesInfo*)catInfoObj {
    
    if (catInfoObj.reviewsMeta != nil && ![[catInfoObj.reviewsMeta valueForKey:@"Rating"] isEqual:[NSNull null]]) {
        
        [self.ratingView setHidden:NO];
        CGFloat ratingValue = [[catInfoObj.reviewsMeta valueForKey:@"Rating"]floatValue];
        self.ratingView.userInteractionEnabled = false;

        self.ratingView.value = ratingValue;
        self.ratingView.maximumValue = 5;
        self.ratingView.minimumValue = 0;
        self.ratingView.tintColor = [UIColor iBeautyThemeColor];
        self.ratingView.allowsHalfStars = YES;
        self.ratingView.accurateHalfStars = YES;
        self.ratingView.filledStarImage = [[UIImage imageNamed:@"star"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        self.ratingView.emptyStarImage = [[UIImage imageNamed:@"star_grey"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];

    }else {
        [self.ratingView setHidden:YES];
    }
    
}

- (IBAction)showDetailAction:(UIButton*)sender {

    [self.delegate showBusinessDetail:sender.tag];
}


@end
