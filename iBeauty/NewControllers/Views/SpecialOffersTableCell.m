//
//  SpecialOffersTableCell.m
//  iBeauty
//
//  Created by App Innovation on 23/10/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import "SpecialOffersTableCell.h"
#import "Constants.h"
#import "SpecialOffersCollectionCell.h"
#import "ServicesCategoriesInfo.h"

static NSString *categoryCellIdentifier = @"SpecialOffersCollectionCell";

@interface SpecialOffersTableCell()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (strong, nonatomic) NSMutableArray *collectionData;

@end

@implementation SpecialOffersTableCell

- (void)configureSpecialOffersCollCell:(NSMutableArray*)categoriesInfo {
    
    if (APP_DEBUG_MODE) { NSLog(@"Array is %@",categoriesInfo);}
    _collectionData = categoriesInfo ;
    [self setUpAroundCollectionCell];
    [self.specialOffersCollectionView reloadData];
}

- (void)setUpAroundCollectionCell {
    
    UINib *collectionNib = [UINib nibWithNibName:@"SpecialOffersCollectionCell" bundle:nil];
    [self.specialOffersCollectionView registerNib:collectionNib forCellWithReuseIdentifier:categoryCellIdentifier];
}

#pragma mark - UICollectionViewDataSource methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.collectionData.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    SpecialOffersCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:categoryCellIdentifier forIndexPath:indexPath];
    NSString *imageStr = self.collectionData[indexPath.row];
    [cell configureSpecialOffersCollectionCell:imageStr];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    //CGFloat screenWidth = CGRectGetWidth([UIScreen mainScreen].bounds);
    
    CGFloat itemWidth = 0.0f;
    CGFloat itemHeight = 0.0f;
    
    NSString *deviceType = [UIDevice currentDevice].model;
    
    if ([deviceType isEqualToString:@"iPhone"]) {
        itemWidth = 295.0f;
        itemHeight = 200.0f;
    }else {
        itemWidth = 385.0f;
        itemHeight = 425.0f;
    }
    
    return CGSizeMake(itemWidth, itemHeight);
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 5.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 5.0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(5.0, 5.0, 5.0, 5.0);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if ( (self.specialOfferDelegate != nil) && ([self.specialOfferDelegate respondsToSelector:@selector(selectSpecialOffersCollectionCell:section:)]) ) {
        [self.specialOfferDelegate selectSpecialOffersCollectionCell:indexPath.item section:collectionView.tag];
        
    }
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
