//
//  CurrentAddressTableFooter.h
//  iBeauty
//
//  Created by App Innovation on 25/10/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CurrentAddressTableFooter : UIView

@property (nonatomic,weak ) IBOutlet UIButton * addNewAddress;

+ (instancetype)currentAddressTableFooterView;

@end

NS_ASSUME_NONNULL_END
