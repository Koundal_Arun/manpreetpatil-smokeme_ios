//
//  TopServicesCollectionCell.m
//  iBeauty
//
//  Created by App Innovation on 23/10/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import "TopServicesCollectionCell.h"

@interface TopServicesCollectionCell ()

@property (nonatomic,weak ) IBOutlet UIImageView * categoryImageView;
@property (nonatomic,weak ) IBOutlet UILabel * categoryNameLable;
@property (nonatomic,weak ) IBOutlet UILabel * providersCountLable;
@property (nonatomic,weak ) IBOutlet UIView * bkgView;

@end

@implementation TopServicesCollectionCell


- (void)configureCollectionCell:(ServicesCategoriesInfo*)catInfoObj {
    
    [self.categoryImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",IMAGE_BASE_URL,catInfoObj.categoryImage]] placeholderImage:[UIImage imageNamed:@""]];
    self.categoryNameLable.text = catInfoObj.categoryName;
    self.providersCountLable.text = [NSString stringWithFormat:@"%lu Places",(unsigned long)catInfoObj.providerCategories.count];
    [iBeautyUtility setViewBorderRound:self.bkgView];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

@end
