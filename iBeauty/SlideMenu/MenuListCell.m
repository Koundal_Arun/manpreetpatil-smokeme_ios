//
//  MenuListCell.m
//  Rdy4u
//
//  Created by App Innovation on 04/04/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "MenuListCell.h"
#import "Constants.h"

@interface MenuListCell ()

@property (nonatomic ,weak) IBOutlet UIImageView *menuCategoryImageView;
@property (nonatomic ,weak) IBOutlet UILabel *menuCategoryName;

@end

@implementation MenuListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void) configureMenuListCell:(NSDictionary*)infoDict rowIndex:(NSInteger)indexRow savedIndex:(NSInteger)savedIndex {
    
    self.menuCategoryName.hidden = NO;
    self.menuCategoryImageView.hidden = NO;

    NSString *deviceType = [UIDevice currentDevice].model;
    
    if ([deviceType isEqualToString:@"iPhone"]) {
        self.menuCategoryName.font = [UIFont fontWithName:@"SegoeUI" size:17.0];
    }else {
        self.menuCategoryName.font = [UIFont fontWithName:@"SegoeUI" size:20.0];
    }
    self.menuCategoryName.text = [NSString stringWithFormat:@"%@",NSLocalizedString([infoDict valueForKey:@"title"],@"")];

    [self setUpRegisterAsServiceProviderCell:infoDict];
    NSString *command = [infoDict valueForKey:@"command"];

    if (indexRow == savedIndex) {
        [self.menuCategoryImageView setImage:[UIImage imageNamed:[infoDict valueForKey:@"image_selected"]]];
        [self.menuCategoryName setTextColor:[UIColor iBeautyThemeColor]];
    }else {
        [self.menuCategoryImageView setImage:[UIImage imageNamed:[infoDict valueForKey:@"image"]]];
        if ([command isEqualToString:@"RegisterAsServiceProvider"]) {
            [self.menuCategoryName setTextColor:[UIColor iBeautyThemeColor]];
        }else {
            [self.menuCategoryName setTextColor:[UIColor blackColor]];
        }
    }
    [self setUpViewInArabicMode];
}

- (void)setUpRegisterAsServiceProviderCell:(NSDictionary*)infoDict{
    
    NSString *command = [infoDict valueForKey:@"command"];
    
    if ([command isEqualToString:@"RegisterAsServiceProvider"]) {
        self.menuCategoryImageView.hidden = YES;
    }else {
        self.menuCategoryImageView.hidden = NO;
    }
    
}

- (void)setUpViewInArabicMode {
    
    self.menuCategoryImageView.translatesAutoresizingMaskIntoConstraints = YES;
    self.menuCategoryName.translatesAutoresizingMaskIntoConstraints = YES;
    
    NSString *languageCode =   [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];
    
    if ([languageCode isEqualToString:@"en"]) {
        
        self.menuCategoryImageView.frame = CGRectMake(15, 20, self.menuCategoryImageView.frame.size.width, self.menuCategoryImageView.frame.size.height);
        self.menuCategoryName.frame = CGRectMake(56, 22, self.menuCategoryName.frame.size.width, self.menuCategoryName.frame.size.height);

        
    }else if ([languageCode isEqualToString:@"ar"]) {
        
        self.menuCategoryImageView.frame = CGRectMake(280-40, 20, self.menuCategoryImageView.frame.size.width, self.menuCategoryImageView.frame.size.height);
        self.menuCategoryName.frame = CGRectMake(280-self.menuCategoryName.frame.size.width-26-30, 22, self.menuCategoryName.frame.size.width, self.menuCategoryName.frame.size.height);
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
