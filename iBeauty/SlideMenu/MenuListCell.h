//
//  MenuListCell.h
//  Rdy4u
//
//  Created by App Innovation on 04/04/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuListCell : UITableViewCell

- (void) configureMenuListCell:(NSDictionary*)infoDict rowIndex:(NSInteger)indexRow savedIndex:(NSInteger)savedIndex;

@end
