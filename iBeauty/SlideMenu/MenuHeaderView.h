//
//  MenuHeaderView.h
//  iBeauty
//
//  Created by App Innovation on 27/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuHeaderView : UIView

@property (nonatomic,weak) IBOutlet UILabel *label_ProfileName;
@property (nonatomic,weak) IBOutlet UIImageView *imageView_profile;
@property (nonatomic,weak) IBOutlet UIView *view_profile;
@property (nonatomic,weak) IBOutlet UIButton *viewProfileButton;

+ (instancetype)menuHeaderView:(NSDictionary *)profileInfoDict;

@end
