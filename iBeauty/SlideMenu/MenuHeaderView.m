//
//  MenuHeaderView.m
//  iBeauty
//
//  Created by App Innovation on 27/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "MenuHeaderView.h"
#import "Constants.h"

@implementation MenuHeaderView


+ (instancetype)menuHeaderView:(NSDictionary *)profileInfoDict {
    
    MenuHeaderView *tableHeaderView = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class])                                                                         owner:nil options:nil] firstObject];
    
    tableHeaderView.view_profile.translatesAutoresizingMaskIntoConstraints = YES;
    CGRect rect = tableHeaderView.view_profile.frame;
    rect.size.width = 280;
    tableHeaderView.view_profile.frame = rect;
    
    [iBeautyUtility setimageViewRound:tableHeaderView.imageView_profile];
    
    tableHeaderView.label_ProfileName.text = [profileInfoDict valueForKey:KProfileName];
    
    [tableHeaderView.imageView_profile sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[profileInfoDict valueForKey:KProfileImageUrl]]] placeholderImage:[UIImage imageNamed:@"image_placeholder"]];

    return tableHeaderView;
    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
