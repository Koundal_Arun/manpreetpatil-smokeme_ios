//
//  SlideMenuViewController.m
//  iBeauty
//
//  Created by App Innovation on 27/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "SlideMenuViewController.h"
#import "Constants.h"
#import <SafariServices/SafariServices.h>


static NSString *cellIdentifier = @"menuListCell";

@interface SlideMenuViewController ()<SFSafariViewControllerDelegate>

@property(nonatomic,strong) NSString *currentIdentifier;
@property(nonatomic,strong) NSArray *menuListArray;
@property(nonatomic,weak)   IBOutlet UITableView *menuListTableView;
@property (nonatomic, assign) NSInteger   intSelectedIndex;

@end

@implementation SlideMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.menuListArray = [NSArray new];
    [self loadMenuData];
    [self setUpTableView];

}
- (void)loadMenuData {
    
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"MenuList" ofType:@"plist"];
    self.menuListArray = [NSArray arrayWithContentsOfFile:plistPath];
    //    NSLog(@"array is %@",self.menuListArray);
}

- (void)setUpTableView {
    
    UINib *cellNib = [UINib nibWithNibName:@"MenuListCell" bundle:nil];
    [self.menuListTableView registerNib:cellNib forCellReuseIdentifier:cellIdentifier];
    self.menuListTableView.translatesAutoresizingMaskIntoConstraints = YES;
    
    if (IS_IPHONE_4 || IS_IPHONE_5 || IS_IPHONE_6 || IS_IPHONE_6_PLUS) {
        self.menuListTableView.frame = CGRectMake(0.0f, 20.0f, self.view.frame.size.width, [UIScreen mainScreen].bounds.size.height-20.0f);
    }else {
        self.menuListTableView.frame = CGRectMake(0.0f, 44.0f, self.view.frame.size.width, [UIScreen mainScreen].bounds.size.height-84.0f);
    }
    [self setUpTableHeaderView];
}

- (void)setUpTableHeaderView {
    
    NSDictionary *infoDict = [[NSUserDefaults standardUserDefaults]valueForKey:KLoginInfo];
    MenuHeaderView *tableHeader = [MenuHeaderView menuHeaderView:infoDict];
    [tableHeader.viewProfileButton addTarget:self action:@selector(viewProfile:) forControlEvents:UIControlEventTouchUpInside];
    self.menuListTableView.tableHeaderView = tableHeader;
}

#pragma mark - View Profile Method

- (void)viewProfile:(UIButton*)sender {
    
    MyProfileViewController *viewProfile = [self.storyboard instantiateViewControllerWithIdentifier:kMyProfileViewController];
    [self.navigationController pushViewController:viewProfile animated:YES];
    
}


#pragma mark - UITableViewDelegate


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.menuListArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MenuListCell *menuListCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    NSDictionary *infoDict = self.menuListArray[indexPath.row];
    [menuListCell configureMenuListCell:infoDict rowIndex:indexPath.row savedIndex:self.intSelectedIndex];
    return menuListCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *deviceType = [UIDevice currentDevice].model;
    
    if ([deviceType isEqualToString:@"iPhone"]) {
        return 65.0f;
    }else {
        return 80.0f;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    self.intSelectedIndex = indexPath.row;
    [tableView reloadData];
    switch (indexPath.row)
    {
        case 0:
        {
            [self.mm_drawerController setCenterViewController:[self.storyboard instantiateViewControllerWithIdentifier:kServiceCategoryController] withCloseAnimation:YES completion:nil];
            break;
        }
        case 1:
        {
            [self.mm_drawerController setCenterViewController:[self.storyboard instantiateViewControllerWithIdentifier:kMyAppointmentViewController] withCloseAnimation:YES completion:nil];
            break;
        }

        case 2:
        {
            [self.mm_drawerController setCenterViewController:[self.storyboard instantiateViewControllerWithIdentifier:kOffersViewController] withCloseAnimation:YES completion:nil];
            break;
        }

        case 3:
        {
            [self.mm_drawerController setCenterViewController:[self.storyboard instantiateViewControllerWithIdentifier:kRegerEarnViewController] withCloseAnimation:YES completion:nil];
            break;
            
        }
        case 4:
        {
            [self.mm_drawerController setCenterViewController:[self.storyboard instantiateViewControllerWithIdentifier:kSettingsController] withCloseAnimation:YES completion:nil];
            break;
            
        }
        case 5:
        {
            [self.mm_drawerController setCenterViewController:[self.storyboard instantiateViewControllerWithIdentifier:kFeedBackController] withCloseAnimation:YES completion:nil];
            break;
            
        }
        case 6:
        {
            [self.mm_drawerController setCenterViewController:[self.storyboard instantiateViewControllerWithIdentifier:kFAQController] withCloseAnimation:YES completion:nil];
            break;
            
        }
            
        default:
            break;
    }
    
}

- (void)openWebsiteUrl:(UIButton*)sender {
    
    SFSafariViewController *svc = [[SFSafariViewController alloc] initWithURL:[NSURL URLWithString:@"http://ibeautyadmin.appinnovation.co.uk"]];
    svc.delegate = self;
    svc.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:svc animated:YES completion:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
