//
//  BasketPriceSummary.h
//  iBeauty
//
//  Created by App Innovation on 09/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BasketPriceSummary : NSObject

@property (nonatomic,strong) NSString *discount;
@property (nonatomic,strong) NSString *orderPrice;
@property (nonatomic,strong) NSMutableArray *tax;
@property (nonatomic,strong) NSString *totalPrice;
@property (nonatomic,strong) NSMutableArray *paymentOptions;
@property (nonatomic,strong) NSDictionary   *basketInfoDict;
@property (nonatomic,assign) float totalTax;

+ (BasketPriceSummary*)getSharedInstance;

- (BasketPriceSummary*)getBasketPriceSummaryInfoObject:(NSDictionary*)infoDict;


@end
