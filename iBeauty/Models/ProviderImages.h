//
//  ProviderImages.h
//  iBeauty
//
//  Created by App Innovation on 16/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProviderImages : NSObject

@property (nonatomic,assign) NSInteger displayOrder;
@property (nonatomic,strong) NSString *Url;
@property (nonatomic,strong) NSString *imageId;
@property (nonatomic,strong) NSString *type;

+ (ProviderImages*)getSharedInstance;

- (NSMutableArray*)providerImagesInfoObj:(NSMutableArray*)infoArray;

@end
