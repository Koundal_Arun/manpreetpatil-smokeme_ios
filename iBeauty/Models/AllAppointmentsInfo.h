//
//  AllAppointmentsInfo.h
//  iBeauty
//
//  Created by App Innovation on 12/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServicesDetail.h"
#import "StaffDetail.h"


@interface AllAppointmentsInfo : NSObject

@property (nonatomic,strong) NSString *orderId;
@property (nonatomic,strong) NSString *orderDateTime;

@property (nonatomic,strong) NSString *discount;
@property (nonatomic,strong) NSString *orderPrice;
@property (nonatomic,strong) NSString *tax;
@property (nonatomic,strong) NSString *totalPrice;

@property (nonatomic,strong) NSString *providerId;
@property (nonatomic,strong) NSString *providerName;
@property (nonatomic,strong) NSString *providerLocation;
@property (nonatomic,assign) NSInteger paymentOption;
@property (nonatomic,strong) NSString *dateCreatedOn;
@property (nonatomic,strong) NSString *descriptionInfo;
@property (nonatomic,strong) NSString *emailId;
@property (nonatomic,strong) NSString *phone;
@property (nonatomic,strong) NSString *address;
@property (nonatomic,strong) NSString *businessThumb;
@property (nonatomic,strong) NSString *latitude;
@property (nonatomic,strong) NSString *longitude;
@property (nonatomic,assign) BOOL isOfferApplied;
@property (nonatomic,strong) NSString *offerID;
@property (nonatomic,strong) NSString *clientNote;
@property (nonatomic,strong) NSString *serviceProvideAt;
@property (nonatomic,assign) BOOL status;
@property (nonatomic,assign) float providerAddressHeight;
@property (nonatomic,assign) float providerNameHeight;

@property (nonatomic,strong) NSString *serviceName;

@property (nonatomic,strong) NSMutableArray *serviceAtArray;
@property (nonatomic,strong) NSMutableArray *providerImageArray;

@property (nonatomic,strong) NSString *endDateTime;
@property (nonatomic,strong) NSString *startDateTime;

@property (nonatomic,strong) NSMutableArray *displayTagArray;
//@property (nonatomic,assign) float longitude;
//@property (nonatomic,assign) float latitude;
@property (nonatomic,strong) NSString *distance;
@property (nonatomic,strong) NSMutableArray *reviewsArray;

@property (nonatomic,strong) NSString *reviewsMetaCount;
@property (nonatomic,strong) NSString *reviewsMeta_resourceUrl;
@property (nonatomic,strong) NSString *ratings;

@property (nonatomic,strong) NSMutableArray *serviceForArray;

@property (nonatomic,strong) NSMutableArray *servicesArray;
@property (nonatomic,strong) NSMutableArray *servicesCategoriesArray;
@property (nonatomic,strong) NSMutableArray *providerCategoriesArray;

@property (nonatomic,strong) NSMutableArray *appointmentsInfo;

@property (nonatomic,strong) StaffDetail *staffObj;
@property (nonatomic,strong) NSString *appointmentId;
@property (nonatomic,strong) NSString *startTimeAppointment;
@property (nonatomic,strong) NSString *endTimeAppointment;

@property (nonatomic,strong) NSMutableArray *staffArray;

@property (nonatomic,strong) ServicesDetail *serviceDetail;

@property (nonatomic,strong) NSDate *createdDate;
@property (nonatomic,strong) NSDate *endDate;
@property (nonatomic,strong) NSDate *startDate;
@property (nonatomic,assign) NSInteger endDateCount;


+ (AllAppointmentsInfo*)getSharedInstance;

- (NSMutableArray*)allAppointmentsInfoObject:(NSMutableArray*)infoArray;

- (NSMutableArray*)appointmentsInfoObject:(NSMutableArray*)infoArray;


@end
