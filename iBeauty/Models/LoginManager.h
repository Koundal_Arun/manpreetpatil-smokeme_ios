//
//  LoginManager.h
//  iBeauty
//
//  Created by App Innovation on 27/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LoginManager : NSObject

@property (nonatomic,strong) NSDictionary *userProfileInfo;

- (void)loginWithFacebook:(void(^)(id result, NSError *error))completion;

@end
