//
//  DisplayTagsInfo.h
//  iBeauty
//
//  Created by App Innovation on 16/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DisplayTagsInfo : NSObject

@property (nonatomic,strong) NSString *text;
@property (nonatomic,strong) NSString *backGround;
@property (nonatomic,strong) NSString *foreColor;

+ (DisplayTagsInfo*)getSharedInstance;

- (NSMutableArray*)displayTagsInfoObj:(NSMutableArray*)infoArray;

@end
