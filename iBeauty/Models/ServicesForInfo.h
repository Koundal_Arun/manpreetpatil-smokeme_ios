//
//  ServicesForInfo.h
//  iBeauty
//
//  Created by App Innovation on 16/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ServicesForInfo : NSObject

@property (nonatomic,assign) NSInteger Id;
@property (nonatomic,strong) NSString *name;

+ (ServicesForInfo*)getSharedInstance;

- (NSMutableArray*)serviceForInfoObj:(NSMutableArray*)infoArray;


@end
