//
//  PromotionalAds.m
//  iBeauty
//
//  Created by App Innovation on 04/12/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "PromotionalAds.h"


static PromotionalAds *singletonClass = nil;


@implementation PromotionalAds

+(PromotionalAds*)getSharedInstance {
    
    if (singletonClass == nil) {
        singletonClass = [[PromotionalAds allocWithZone:NULL] init];
    }
    return singletonClass;
    
}


- (NSMutableArray*)promotionalInfoObj:(NSMutableArray*)infoArray {
    
    NSMutableArray *promotionalInfoArray = [[NSMutableArray alloc]init];
    
    for (NSDictionary *dictInfo in infoArray) {
        
        PromotionalAds *infoObj = [[PromotionalAds alloc]init];
        
        infoObj.businessID = [dictInfo valueForKey:@"BusinessId"];
        infoObj.createdDateStr = [dictInfo valueForKey:@"CreatedOn"];
        infoObj.endDateStr = [dictInfo valueForKey:@"EndDate"];
        infoObj.adID = [dictInfo valueForKey:@"Id"];
        infoObj.adImage = [dictInfo valueForKey:@"Image"];
        infoObj.isActive = [NSNumber numberWithBool:[dictInfo valueForKey:@"IsActive"]];//[[dictInfo valueForKey:@"IsActive"]boolValue];
        infoObj.offerId = [dictInfo valueForKey:@"OfferId"];
        infoObj.promotionalType = [dictInfo valueForKey:@"PromotionalType"];
        infoObj.referralUrl = [dictInfo valueForKey:@"ReferralUrl"];
        infoObj.descriptionStr = [dictInfo valueForKey:@"Description"];
        infoObj.title = [dictInfo valueForKey:@"Title"];

        [promotionalInfoArray addObject:infoObj];
    }
    
    return promotionalInfoArray;
}


@end
