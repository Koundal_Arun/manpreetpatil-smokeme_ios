//
//  ServicesCategoriesInfo.h
//  iBeauty
//
//  Created by App Innovation on 28/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ServicesCategoriesInfo : NSObject

@property (nonatomic,strong) NSMutableArray *displayTags;
@property (nonatomic,assign) NSString *distance;
@property (nonatomic,strong) NSString *Id;
@property (nonatomic,strong) NSMutableArray *images;
@property (nonatomic,strong) NSString *categoryImage;
@property (nonatomic,strong) NSString *latitude;
@property (nonatomic,strong) NSString *longitude;
@property (nonatomic,strong) NSString *location;
@property (nonatomic,strong) NSString *categoryName;
@property (nonatomic,strong) NSString *providerDescription;
@property (nonatomic,strong) NSMutableArray *provideServiceAt;
@property (nonatomic,strong) NSMutableArray *providerCategories;
@property (nonatomic,strong) NSMutableArray *reviews;
@property (nonatomic,strong) NSDictionary *reviewsMeta;
@property (nonatomic,strong) NSMutableArray *ServiceCategories;
@property (nonatomic,strong) NSMutableArray *ServiceFor;
@property (nonatomic,strong) NSMutableArray *Services;
@property (nonatomic,strong) NSMutableArray *StandardOpeningHours;
@property (nonatomic,strong) NSString *rating;
@property (nonatomic,assign) NSInteger reviewsCount;
@property (nonatomic,assign) NSInteger sectionIndex;
@property (nonatomic,strong) NSMutableArray *offersArray;
@property (nonatomic,strong) NSMutableArray *staffsArray;
@property (nonatomic,assign) BOOL registerType;
@property (nonatomic,assign) float providerAddressHeight;
@property (nonatomic,assign) float providerNameHeight;
@property (nonatomic,assign) float newAddressHeight;
@property (nonatomic,assign) float newNameHeight;

@property (nonatomic,strong) NSString *businessCatId;
@property (nonatomic,assign) NSInteger displayOrder;
@property (nonatomic,assign) BOOL isActive;

@property (nonatomic,assign) NSInteger providerDistance;
@property (nonatomic,strong) NSDictionary *openingHours;


@property (nonatomic,strong) NSMutableArray *productsArray;


+ (ServicesCategoriesInfo*)getSharedInstance;

- (NSMutableArray*)serviceCategoryInfoObject:(NSMutableArray*)infoArray;

- (ServicesCategoriesInfo*)serviceProvideInfoObject:(NSDictionary*)dictInfo;

- (NSMutableArray*)serviceProvidersListInfoObject:(NSMutableArray*)infoArray;

- (NSMutableArray*)getServiceProvidersInfo_For_Offers:(NSMutableArray*)infoArray;

- (NSMutableArray*)getAllServicesCategoryList:(NSMutableArray*)infoArray;

@end
