//
//  ProvideServiceAt.h
//  iBeauty
//
//  Created by App Innovation on 16/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProvideServiceAt : NSObject


@property (nonatomic,strong) NSString *Id;
@property (nonatomic,strong) NSString *name;
@property (nonatomic,assign) NSInteger status;
@property (nonatomic,strong) NSString *BusinessId;
@property (nonatomic,strong) NSString *CreatedOn;

+ (ProvideServiceAt*)getSharedInstance;

- (NSMutableArray*)provideServiceAtInfoObj:(NSMutableArray*)infoArray;

- (NSMutableArray*)provideProductAtInfoObj:(NSMutableArray*)infoArray;


@end
