//
//  StandardOpeningHours.m
//  iBeauty
//
//  Created by App Innovation on 17/01/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import "StandardOpeningHours.h"

/*                    Dayname = Wednesday;
 Fromtime = "09:00:00";
 Id = 5c2f31ca13d76;
 Status = 1;
 Totime = "18:00:00";*/

static StandardOpeningHours *singletonClass = nil;

@implementation StandardOpeningHours

+ (StandardOpeningHours*)getSharedInstance {
    
    if (singletonClass == nil) {
        singletonClass = [[StandardOpeningHours allocWithZone:NULL] init];
    }
    return singletonClass;
}

- (NSMutableArray*)getStandardOpeningHours:(NSMutableArray*)infoArray {
    
    NSMutableArray *hoursInfoArray = [[NSMutableArray alloc]init];
    
    for (NSDictionary *dictInfo in infoArray) {
        
        StandardOpeningHours *infoObj = [[StandardOpeningHours alloc]init];
        
        infoObj.Id = [dictInfo valueForKey:@"Id"];
        infoObj.dayName = [dictInfo valueForKey:@"Dayname"];
        infoObj.fromTime = [dictInfo valueForKey:@"Fromtime"];
        infoObj.toTime = [dictInfo valueForKey:@"Totime"];
        infoObj.status = [[dictInfo valueForKey:@"Status"]integerValue];

        [hoursInfoArray addObject:infoObj];
    }
    

    return hoursInfoArray;
}

@end
