//
//  BasketInfoObj.h
//  iBeauty
//
//  Created by App Innovation on 06/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StaffDetail.h"
#import "ServicesDetail.h"
#import "ServiceTimeSlot.h"

@class ServicesDetail;


@interface BasketInfoObj : NSObject

@property (nonatomic, strong) NSString *CustomerId;
@property (nonatomic, strong) NSString *businessId;
@property (nonatomic, strong) NSString *basketId;
@property (nonatomic, strong) NSMutableArray *productsArray;
@property (nonatomic, strong) NSString *endDateTime;
@property (nonatomic, strong) NSString *startDateTime;
@property (nonatomic, strong) NSString *appointmentId;
@property (nonatomic, assign) NSString *serviceId;
@property (nonatomic, assign) NSString *staffId;
@property (nonatomic, strong) NSMutableDictionary *serviceInfoDict;
@property (nonatomic, strong) NSMutableArray *staffArray;
@property (nonatomic, strong) NSMutableArray *servicesArray;
@property (nonatomic, strong) NSString *serviceProvideAt;
@property (nonatomic, strong) NSString *note;

@property (nonatomic, strong) ServicesDetail *serviceInfoObj;

@property (nonatomic, strong) ServiceTimeSlot *slotInfObj;
@property (nonatomic, strong) StaffDetail *staffInfo;


+ (BasketInfoObj*)getSharedInstance;

- (NSMutableArray*)basketDetailInfoObject:(NSDictionary*)infoDict;

@end
