//
//  BasketInfoObj.m
//  iBeauty
//
//  Created by App Innovation on 06/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "BasketInfoObj.h"

static BasketInfoObj *singletonClass = nil;

@implementation BasketInfoObj


+ (BasketInfoObj*)getSharedInstance {
    
    if (singletonClass == nil) {
        singletonClass = [[BasketInfoObj allocWithZone:NULL] init];
    }
    return singletonClass;
    
}

- (NSMutableArray*)basketDetailInfoObject:(NSDictionary*)infoDict {
    
    NSMutableArray *basketDetailInfo = [[NSMutableArray alloc]init];
    
    NSMutableArray *appointmentArray = [NSMutableArray new];
    appointmentArray = [infoDict valueForKey:@"Appointments"];
    
    for (NSDictionary *appointmentInfo in appointmentArray) {
        
        BasketInfoObj *infoObj = [[BasketInfoObj alloc]init];
        infoObj.businessId = [infoDict valueForKey:@"BusinessId"];
        infoObj.CustomerId = [NSString stringWithFormat:@"%@",[infoDict valueForKey:@"CustomerId"]];
        infoObj.basketId = [infoDict valueForKey:@"Id"];
        infoObj.productsArray = [infoDict valueForKey:@"Products"];
        infoObj.serviceProvideAt = [infoDict valueForKey:@"ServiceProvideAt"];
        infoObj.note = [infoDict valueForKey:@"Note"];

        infoObj.startDateTime = [appointmentInfo valueForKey:@"StartDateTime"];
        infoObj.endDateTime = [appointmentInfo valueForKey:@"EndDateTime"];
        
        if (![[appointmentInfo valueForKey:@"StartDateTime"] isEqual:[NSNull null]] && ![[appointmentInfo valueForKey:@"EndDateTime"] isEqual:[NSNull null]] && ![[appointmentInfo valueForKey:@"StartDateTime"] isEqualToString:@"0000-00-00 00:00:00"]) {
            infoObj.slotInfObj = [ServiceTimeSlot getTimeSlotInfo:infoObj.startDateTime endTime:infoObj.endDateTime];
        }
        
        infoObj.appointmentId = [appointmentInfo valueForKey:@"Id"];
        
       if (![[appointmentInfo valueForKey:@"Staff"] isEqual:[NSNull null]]) {
            infoObj.staffId = [appointmentInfo valueForKey:@"StaffId"];
        }
        
        StaffDetail *staffObj = [StaffDetail getSharedInstance];
        if ([appointmentInfo valueForKey:@"Staff"]  != nil && ![[appointmentInfo valueForKey:@"Staff"] isEqual:[NSNull null]]) {
            infoObj.staffArray = [staffObj staffDetailInfoObject:[appointmentInfo valueForKey:@"Staff"]];
        }
        infoObj.serviceInfoDict = [appointmentInfo valueForKey:@"ServiceInfo"];
        if (![[infoObj.serviceInfoDict valueForKey:@"ServiceId"] isEqual:[NSNull null]]) {
            infoObj.serviceId = [infoObj.serviceInfoDict valueForKey:@"ServiceId"];
            
            //getServiceDetailInfoObjectOnly
            
        ServicesDetail *serviceDetailObj = [ServicesDetail getSharedInstance];
            infoObj.serviceInfoObj = [serviceDetailObj getServiceDetailInfoObjectOnly:[appointmentInfo valueForKey:@"ServiceInfo"]];
            
        }
        [basketDetailInfo addObject:infoObj];
        
    }
    
    return basketDetailInfo;
    
}



@end
