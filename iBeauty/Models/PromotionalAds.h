//
//  PromotionalAds.h
//  iBeauty
//
//  Created by App Innovation on 04/12/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN


@interface PromotionalAds : NSObject

@property (nonatomic,strong) NSString *businessID;
@property (nonatomic,strong) NSString *createdDateStr;
@property (nonatomic,strong) NSString *endDateStr;
@property (nonatomic,strong) NSString *adID;
@property (nonatomic,strong) NSString *adImage;
@property (nonatomic,assign) BOOL isActive;
@property (nonatomic,strong) NSString *offerId;
@property (nonatomic,strong) NSString *promotionalType;
@property (nonatomic,strong) NSString *referralUrl;
@property (nonatomic,strong) NSString *descriptionStr;
@property (nonatomic,strong) NSString *title;


+ (PromotionalAds*)getSharedInstance;

- (NSMutableArray*)promotionalInfoObj:(NSMutableArray*)infoArray;


@end

NS_ASSUME_NONNULL_END
