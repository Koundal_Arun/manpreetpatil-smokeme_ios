//
//  OffersImages.m
//  iBeauty
//
//  Created by App Innovation on 19/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "OffersImages.h"

static OffersImages *singletonClass = nil;

@implementation OffersImages


+(OffersImages*)getSharedInstance {
    
    if (singletonClass == nil) {
        singletonClass = [[OffersImages allocWithZone:NULL] init];
    }
    return singletonClass;
    
}

- (NSMutableArray*)offersImagesInfoObj:(NSMutableArray*)infoArray {
    
    NSMutableArray *offersImagesInfo = [[NSMutableArray alloc]init];
    
    for (NSDictionary *dictInfo in infoArray) {
        
        OffersImages *infoObj = [[OffersImages alloc]init];
        infoObj.displayOrder = [[dictInfo valueForKey:@"DisplayOrder"]integerValue];
        infoObj.Url = [dictInfo valueForKey:@"Url"];
        
        [offersImagesInfo addObject:infoObj];
    }
    
    return offersImagesInfo;
}


@end
