//
//  ServicesDetail.h
//  iBeauty
//
//  Created by App Innovation on 02/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StaffDetail.h"
#import "ServiceTimeSlot.h"

@class StaffDetail;
@class ServiceTimeSlot;

@interface ServicesDetail : NSObject

@property (nonatomic,strong) NSString  *categoryName;
@property (nonatomic,strong) NSString  *serviceId;
@property (nonatomic,strong) NSString  *serviceCategoryId;
@property (nonatomic,strong) NSString  *serviceImage;
@property (nonatomic,strong) NSString  *serviceDescription;
@property (nonatomic,strong) NSString  *serviceAmount;
@property (nonatomic,strong) NSString  *salesPrice;
@property (nonatomic,strong) NSString  *serviceAmountCurrency;
@property (nonatomic,strong) NSString  *serviceTime;
@property (nonatomic,strong) NSString  *serviceTimeUnits;
@property (nonatomic,strong) StaffDetail  *staffInfo;
@property (nonatomic,strong) ServiceTimeSlot  *slotInfo;

@property (nonatomic,assign) NSInteger increaseIndex;
@property (nonatomic,assign) NSInteger decreaseIndex;

//// service category

@property (nonatomic,assign) NSInteger categoryDisplayOrder;
@property (nonatomic,strong) NSString *categoryImageUrl;
@property (nonatomic,strong) NSString *categoryName_service;

@property (nonatomic,assign) BOOL viewMoreStatus;
@property (nonatomic,assign) float descriptionHeight;
@property (nonatomic,assign) float nameHeight;


+(ServicesDetail*)getSharedInstance;
- (NSMutableArray*)serviceDetailInfoObject:(NSMutableArray*)infoArray;
- (NSMutableDictionary*)filterServicesArrayAccordingToServiceId:(NSMutableArray*)servicesDetailArray;
- (NSMutableArray*)serviceDetailInfoObject_Appointments:(NSMutableArray*)infoArray;

- (ServicesDetail*)getServiceDetailInfoObjectOnly:(NSDictionary*)dictInfo;

- (ServicesDetail*)getServiceDetailInfo_ForStaffObject:(NSDictionary*)dictInfo;


@end
