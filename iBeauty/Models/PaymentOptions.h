//
//  PaymentOptions.h
//  iBeauty
//
//  Created by App Innovation on 09/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PaymentOptions : NSObject

@property (nonatomic,assign) NSInteger iD;
@property (nonatomic,strong) NSString *name;

+ (PaymentOptions*)getSharedInstance;

- (NSMutableArray*)getPaymentOptionsInfoObject:(NSDictionary*)infoDict;

@end
