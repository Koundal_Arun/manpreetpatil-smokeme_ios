//
//  ServiceTimeSlot.m
//  iBeauty
//
//  Created by App Innovation on 05/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "ServiceTimeSlot.h"
#import "NSDate+iBeautyDateFormatter.h"

static ServiceTimeSlot *singletonClass = nil;


@implementation ServiceTimeSlot

+ (ServiceTimeSlot*)getSharedInstance {
    
    if (singletonClass == nil) {
        singletonClass = [[ServiceTimeSlot allocWithZone:NULL] init];
    }
    return singletonClass;
    
}

- (NSMutableArray*)serviceTimeSlotsInfoObject:(NSMutableArray*)infoArray {
    
    NSMutableArray *serviceSlotsInfo = [[NSMutableArray alloc]init];
    
    for (NSDictionary *dictInfo in infoArray) {
        
        ServiceTimeSlot *infoObj = [[ServiceTimeSlot alloc]init];

        infoObj.startDateTime = [dictInfo valueForKey:@"StartDateTime"];
        infoObj.endDateTime = [dictInfo valueForKey:@"EndDateTime"];
        
        NSDate *startDate = [NSDate convertNSStringToNSDate:infoObj.startDateTime format:DATE_FORMAT_NEW_SLOT];
        infoObj.startTime = [startDate convertDateValueToNSString:SETTING_TIME_FORMAT_12 localeStatus:YES];
        
        NSDate *endDate = [NSDate convertNSStringToNSDate:infoObj.endDateTime format:DATE_FORMAT_NEW_SLOT];
        infoObj.endTime = [endDate convertDateValueToNSString:SETTING_TIME_FORMAT_12 localeStatus:YES];
        
        [serviceSlotsInfo addObject:infoObj];
        
    }
    
    return serviceSlotsInfo;
    
}

+ (ServiceTimeSlot*)getTimeSlotInfo:(NSString*)startTime endTime:(NSString*)endTime {
    
    ServiceTimeSlot *infoObj = [[ServiceTimeSlot alloc]init];
    
    infoObj.startDateTime = startTime;
    infoObj.endDateTime = endTime;
    
    NSDate *startDate = [NSDate convertNSStringToNSDate:infoObj.startDateTime format:DATE_FORMAT_APPOINTMENT_DETAIL];
    infoObj.startTime = [startDate convertDateValueToNSString:SETTING_TIME_FORMAT_12 localeStatus:YES];
    
    NSDate *endDate = [NSDate convertNSStringToNSDate:infoObj.endDateTime format:DATE_FORMAT_APPOINTMENT_DETAIL];
    infoObj.endTime = [endDate convertDateValueToNSString:SETTING_TIME_FORMAT_12 localeStatus:YES];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    
    NSDate *slotDate = [NSDate convertNSStringToNSDate:infoObj.startDateTime format:DATE_FORMAT_APPOINTMENT_DETAIL];
    NSString *selectedDateStr = [dateFormatter stringFromDate:slotDate];
    if (APP_DEBUG_MODE)
        NSLog(@"did select date str is %@",selectedDateStr);
    NSDate *selectedDate = [NSDate convertNSStringToNSDate:selectedDateStr format:CALENDAR_DATE_FORMAT];
    infoObj.selectedDate = selectedDate;
    return infoObj;

}



@end
