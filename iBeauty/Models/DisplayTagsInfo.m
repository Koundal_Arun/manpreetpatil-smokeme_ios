//
//  DisplayTagsInfo.m
//  iBeauty
//
//  Created by App Innovation on 16/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "DisplayTagsInfo.h"

@implementation DisplayTagsInfo

static DisplayTagsInfo *singletonClass = nil;

+(DisplayTagsInfo*)getSharedInstance {
    
    if (singletonClass == nil) {
        singletonClass = [[DisplayTagsInfo allocWithZone:NULL] init];
    }
    return singletonClass;
    
}

- (NSMutableArray*)displayTagsInfoObj:(NSMutableArray*)infoArray {
    
    NSMutableArray *displayTagsInfo = [[NSMutableArray alloc]init];
    
    for (NSDictionary *dictInfo in infoArray) {
        
        DisplayTagsInfo *infoObj = [[DisplayTagsInfo alloc]init];
        infoObj.text = [dictInfo valueForKey:@"Text"];
        infoObj.backGround = [dictInfo valueForKey:@"Background"];
        infoObj.foreColor = [dictInfo valueForKey:@"ForeColour"];

        [displayTagsInfo addObject:infoObj];
    }
    
    return displayTagsInfo;
}

@end
