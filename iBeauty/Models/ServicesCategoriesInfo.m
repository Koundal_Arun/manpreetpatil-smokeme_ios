//
//  ServicesCategoriesInfo.m
//  iBeauty
//
//  Created by App Innovation on 28/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "ServicesCategoriesInfo.h"
#import "ProviderImages.h"
#import "ProvideServiceAt.h"
#import "DisplayTagsInfo.h"
#import "ServicesForInfo.h"
#import "ServicesDetail.h"
#import "BusinessReviews.h"
#import "OffersDetail.h"
#import "StandardOpeningHours.h"
#import "Constants.h"
#import "UILabel+FlexibleWidHeight.h"

static ServicesCategoriesInfo *singletonClass = nil;

@implementation ServicesCategoriesInfo

+ (ServicesCategoriesInfo*)getSharedInstance {
    
    if (singletonClass == nil) {
        singletonClass = [[ServicesCategoriesInfo allocWithZone:NULL] init];
    }
    return singletonClass;
    
}


- (NSMutableArray*)serviceCategoryInfoObject:(NSMutableArray*)infoArray {
    
    NSMutableArray *serviceCategory = [[NSMutableArray alloc]init];
    
    for (NSDictionary *providerInfo in infoArray) {
        
        ServicesCategoriesInfo *infoObj = [[ServicesCategoriesInfo alloc]init];

        NSDictionary *dictInfo = [providerInfo valueForKey:@"business"];
        
        infoObj.Id = [dictInfo valueForKey:@"Id"];
        if (APP_DEBUG_MODE) { NSLog(@"Provider id is %@",infoObj.Id);}
        
        if (([dictInfo valueForKey:@"Description"])  != nil) {
            infoObj.providerDescription = [dictInfo valueForKey:@"Description"];
        }
        
        if (([dictInfo valueForKey:@"Latitude"])  != nil) {
            infoObj.latitude = [dictInfo valueForKey:@"Latitude"];
        }
        
        if (([dictInfo valueForKey:@"Longitude"])  != nil) {
            infoObj.longitude = [dictInfo valueForKey:@"Longitude"];
        }
        if ([dictInfo valueForKey:@"Location"]  != nil) {
            infoObj.location = [dictInfo valueForKey:@"Location"];
        }
        
        if ([dictInfo valueForKey:@"Name"]  != nil) {
            infoObj.categoryName = [dictInfo valueForKey:@"Name"];
        }
        if ([dictInfo valueForKey:@"RegisterType"]  != nil) {
            infoObj.registerType = [[dictInfo valueForKey:@"RegisterType"]boolValue];
        }
        
        ProvideServiceAt *serviceAtObj = [ProvideServiceAt getSharedInstance];
        infoObj.provideServiceAt = [serviceAtObj provideServiceAtInfoObj:[dictInfo valueForKey:@"ProvideServiceAt"]];

        NSMutableArray  *standardOpeningHours = [dictInfo valueForKey:@"StandardOpeningHours"];
        if (standardOpeningHours.count > 0) {
            StandardOpeningHours *hoursObj = [StandardOpeningHours getSharedInstance];
            infoObj.StandardOpeningHours = [hoursObj getStandardOpeningHours:[dictInfo valueForKey:@"StandardOpeningHours"]];
        }

        BusinessReviews *reviewsInfo = [BusinessReviews getSharedInstance];
        if ([dictInfo valueForKey:@"Reviews"]  != nil && ![[dictInfo valueForKey:@"Reviews"] isEqual:[NSNull null]]) {
            infoObj.reviews = [reviewsInfo businessReviewsInfoObj:[dictInfo valueForKey:@"Reviews"]];
        }
        
        if ([dictInfo valueForKey:@"ReviewsMeta"]  != nil && ![[dictInfo valueForKey:@"ReviewsMeta"] isEqual:[NSNull null]]) {
            NSArray *reviews = [dictInfo valueForKey:@"ReviewsMeta"];
            infoObj.reviewsMeta = reviews[0];
            if ([infoObj.reviewsMeta valueForKey:@"Rating"]  != nil && ![[infoObj.reviewsMeta valueForKey:@"Rating"] isEqual:[NSNull null]]) {
                infoObj.rating = [infoObj.reviewsMeta valueForKey:@"Rating"];
            }
            infoObj.reviewsCount = [[infoObj.reviewsMeta valueForKey:@"Count"]integerValue];
        }
        
        ServicesForInfo *serviceForObj = [ServicesForInfo getSharedInstance];
        
        if ([dictInfo valueForKey:@"ServiceFor"]  != nil && ![[dictInfo valueForKey:@"ServiceFor"] isEqual:[NSNull null]]) {
            infoObj.ServiceFor = [serviceForObj serviceForInfoObj:[dictInfo valueForKey:@"ServiceFor"]];
        }
        
        ServicesDetail *detailObj = [ServicesDetail getSharedInstance];
        if ([dictInfo valueForKey:@"Services"]  != nil && ![[dictInfo valueForKey:@"Services"] isEqual:[NSNull null]]) {
            infoObj.Services = [detailObj serviceDetailInfoObject_Appointments:[dictInfo valueForKey:@"Services"]];
        }

        if ([dictInfo valueForKey:@"ServiceCategories"]  != nil && ![[dictInfo valueForKey:@"ServiceCategories"] isEqual:[NSNull null]]) {
            infoObj.ServiceCategories = [dictInfo valueForKey:@"ServiceCategories"];
        }
        if ([dictInfo valueForKey:@"ProviderCategories"]  != nil && ![[dictInfo valueForKey:@"ProviderCategories"] isEqual:[NSNull null]]) {
            infoObj.providerCategories = [dictInfo valueForKey:@"ProviderCategories"];
        }

        ProviderImages *providerImagesObj = [ProviderImages getSharedInstance];
        if ([dictInfo valueForKey:@"Images"]  != nil && ![[dictInfo valueForKey:@"Images"] isEqual:[NSNull null]]) {
            infoObj.images = [providerImagesObj providerImagesInfoObj:[dictInfo valueForKey:@"Images"]];
            if (infoObj.images.count > 0) {
                providerImagesObj = infoObj.images[0];
            }
            infoObj.categoryImage = providerImagesObj.Url;
        }

        OffersDetail *offersDetail  = [OffersDetail getSharedInstance];
        if ([dictInfo valueForKey:@"Offers"]  != nil && ![[dictInfo valueForKey:@"Offers"] isEqual:[NSNull null]]) {
            infoObj.offersArray = [offersDetail offersDetailInfoObjFor_SingleProvider:[dictInfo valueForKey:@"Offers"]];
        }
        StaffDetail *staffObj = [StaffDetail getSharedInstance];
        if ([dictInfo valueForKey:@"Staff"]  != nil && ![[dictInfo valueForKey:@"Staff"] isEqual:[NSNull null]]) {
            infoObj.staffsArray = [staffObj staffDetailInfoObject:[dictInfo valueForKey:@"Staff"]];
        }

        [serviceCategory addObject:infoObj];
    }
    
    return serviceCategory;
}

- (ServicesCategoriesInfo*)serviceProvideInfoObject:(NSDictionary*)dictInfo {
    
    ServicesCategoriesInfo *infoObj = [[ServicesCategoriesInfo alloc]init];
    infoObj.Id = [dictInfo valueForKey:@"Id"];
    
    ProviderImages *providerImagesObj = [ProviderImages getSharedInstance];
    infoObj.images = [providerImagesObj providerImagesInfoObj:[dictInfo valueForKey:@"Images"]];
    
    if ([dictInfo valueForKey:@"Location"]  != nil) {
        infoObj.location = [dictInfo valueForKey:@"Location"];
    }
    
    if ([dictInfo valueForKey:@"Name"]  != nil) {
        infoObj.categoryName = [dictInfo valueForKey:@"Name"];
    }
    ProvideServiceAt *serviceAtObj = [ProvideServiceAt getSharedInstance];
    infoObj.provideServiceAt = [serviceAtObj provideServiceAtInfoObj:[dictInfo valueForKey:@"ProvideServiceAt"]];
    
    NSMutableArray  *standardOpeningHours = [dictInfo valueForKey:@"StandardOpeningHours"];
    if (standardOpeningHours.count > 0) {
        StandardOpeningHours *hoursObj = [StandardOpeningHours getSharedInstance];
        infoObj.StandardOpeningHours = [hoursObj getStandardOpeningHours:[dictInfo valueForKey:@"StandardOpeningHours"]];
    }
    
    DisplayTagsInfo *displayTagObj = [DisplayTagsInfo getSharedInstance];
    
    if ([dictInfo valueForKey:@"DisplayTags"]  != nil && ![[dictInfo valueForKey:@"DisplayTags"] isEqual:[NSNull null]]) {
        infoObj.displayTags = [displayTagObj displayTagsInfoObj:[dictInfo valueForKey:@"DisplayTags"]];
    }
    
    if (([dictInfo valueForKey:@"Latitude"])  != nil) {
        infoObj.latitude = [dictInfo valueForKey:@"Latitude"];
    }
    
    if (([dictInfo valueForKey:@"Longitude"])  != nil) {
        infoObj.longitude = [dictInfo valueForKey:@"Longitude"];
    }
    if ([dictInfo valueForKey:@"Distance"]  != nil && ![[dictInfo valueForKey:@"Distance"] isEqual:[NSNull null]]) {
        
        float distance = [[dictInfo valueForKey:@"Distance"]floatValue];
        int roundedUp = roundf(distance);
        infoObj.providerDistance = roundedUp;
        infoObj.distance = [NSString stringWithFormat:@"%d",roundedUp];
    }
    
    BusinessReviews *reviewsInfo = [BusinessReviews getSharedInstance];
    
    if ([dictInfo valueForKey:@"Reviews"]  != nil && ![[dictInfo valueForKey:@"Reviews"] isEqual:[NSNull null]]) {
        infoObj.reviews = [reviewsInfo businessReviewsInfoObj:[dictInfo valueForKey:@"Reviews"]];
    }

    if ([dictInfo valueForKey:@"ReviewsMeta"]  != nil && ![[dictInfo valueForKey:@"ReviewsMeta"] isEqual:[NSNull null]]) {
        infoObj.reviewsMeta = [dictInfo valueForKey:@"ReviewsMeta"];
        if ([infoObj.reviewsMeta valueForKey:@"Rating"]  != nil && ![[infoObj.reviewsMeta valueForKey:@"Rating"] isEqual:[NSNull null]]) {
            infoObj.rating = [infoObj.reviewsMeta valueForKey:@"Rating"];
        }
        infoObj.reviewsCount = [[infoObj.reviewsMeta valueForKey:@"Count"]integerValue];
    }
    
    ServicesForInfo *serviceForObj = [ServicesForInfo getSharedInstance];
    
    if ([dictInfo valueForKey:@"ServiceFor"]  != nil && ![[dictInfo valueForKey:@"ServiceFor"] isEqual:[NSNull null]]) {
        infoObj.ServiceFor = [serviceForObj serviceForInfoObj:[dictInfo valueForKey:@"ServiceFor"]];
    }
    
    ServicesDetail *detailObj = [ServicesDetail getSharedInstance];
    if ([dictInfo valueForKey:@"Services"]  != nil && ![[dictInfo valueForKey:@"Services"] isEqual:[NSNull null]]) {
        infoObj.Services = [detailObj serviceDetailInfoObject_Appointments:[dictInfo valueForKey:@"Services"]];
    }
    
    if ([dictInfo valueForKey:@"ServiceCategories"]  != nil && ![[dictInfo valueForKey:@"ServiceCategories"] isEqual:[NSNull null]]) {
        infoObj.ServiceCategories = [dictInfo valueForKey:@"ServiceCategories"];
    }
    if ([dictInfo valueForKey:@"ProviderCategories"]  != nil && ![[dictInfo valueForKey:@"ProviderCategories"] isEqual:[NSNull null]]) {
        infoObj.providerCategories = [dictInfo valueForKey:@"ProviderCategories"];
    }
    
    return infoObj;
}

- (NSMutableArray*)serviceProvidersListInfoObject:(NSMutableArray*)infoArray {

    NSMutableArray *providerList = [[NSMutableArray alloc]init];
    NSMutableArray *tempArray = [[NSMutableArray alloc]init];

    for (NSDictionary *dictInfo in infoArray) {
        
        ServicesCategoriesInfo *infoObj = [[ServicesCategoriesInfo alloc]init];
        
        infoObj.Id = [dictInfo valueForKey:@"Id"];
        if (APP_DEBUG_MODE) { NSLog(@"Provider id is %@",infoObj.Id);}

        if ([dictInfo valueForKey:@"Name"]  != nil && ![[dictInfo valueForKey:@"Name"] isEqual:[NSNull null]]) {
            infoObj.categoryName = [dictInfo valueForKey:@"Name"];
            infoObj.newNameHeight = [UILabel heightOfTextForString:infoObj.categoryName andFont:[UIFont fontWithName:@"SegoeUI-Semibold" size:17.0] maxSize:(CGSize){SCREEN_WIDTH-220.0f, MAXFLOAT}];
        }
        if ([dictInfo valueForKey:@"Address"]  != nil && ![[dictInfo valueForKey:@"Address"] isEqual:[NSNull null]]) {
            infoObj.location = [dictInfo valueForKey:@"Address"];
            infoObj.newAddressHeight = [UILabel heightOfTextForString:infoObj.location andFont:[UIFont fontWithName:@"SegoeUI" size:15.0] maxSize:(CGSize){SCREEN_WIDTH-142.0f, MAXFLOAT}];
        }
        
        if ([dictInfo valueForKey:@"ImageUrl"]  != nil && ![[dictInfo valueForKey:@"ImageUrl"] isEqual:[NSNull null]]) {
            infoObj.categoryImage = [dictInfo valueForKey:@"ImageUrl"];
        }
        
        if ([dictInfo valueForKey:@"BusinessThumb"]  != nil && ![[dictInfo valueForKey:@"BusinessThumb"] isEqual:[NSNull null]]) {
            infoObj.categoryImage = [dictInfo valueForKey:@"BusinessThumb"];
        }
        if ([dictInfo valueForKey:@"Longitude"]  != nil && ![[dictInfo valueForKey:@"Longitude"] isEqual:[NSNull null]]) {
            infoObj.longitude = [dictInfo valueForKey:@"Longitude"];
        }
        if ([dictInfo valueForKey:@"Latitude"]  != nil && ![[dictInfo valueForKey:@"Latitude"] isEqual:[NSNull null]]) {
            infoObj.latitude = [dictInfo valueForKey:@"Latitude"];
        }
        if ([dictInfo valueForKey:@"Distance"]  != nil && ![[dictInfo valueForKey:@"Distance"] isEqual:[NSNull null]]) {
            
            float distance = [[dictInfo valueForKey:@"Distance"]floatValue];
            int roundedUp = roundf(distance);
            infoObj.providerDistance = roundedUp;
            infoObj.distance = [NSString stringWithFormat:@"%d",roundedUp];
            if (infoObj.location) {
                NSString *addLocStr = [NSString stringWithFormat:@"%@ (%@ KM)",infoObj.location,infoObj.distance];
                infoObj.providerAddressHeight = [UILabel heightOfTextForString:addLocStr andFont:[UIFont fontWithName:@"SegoeUI" size:12.0] maxSize:(CGSize){SCREEN_WIDTH-96.0f, MAXFLOAT}];
            }
        }
        
        ProvideServiceAt *serviceAtObj = [ProvideServiceAt getSharedInstance];
        infoObj.provideServiceAt = [serviceAtObj provideServiceAtInfoObj:[dictInfo valueForKey:@"ProvideAt"]];
        
        infoObj.openingHours = [dictInfo valueForKey:@"Openinghour"];
        
        if ([dictInfo valueForKey:@"Reviews"]  != nil && ![[dictInfo valueForKey:@"Reviews"] isEqual:[NSNull null]]) {
            NSArray *reviews = [dictInfo valueForKey:@"Reviews"];
            infoObj.reviewsMeta = reviews[0];
            if ([infoObj.reviewsMeta valueForKey:@"Rating"]  != nil && ![[infoObj.reviewsMeta valueForKey:@"Rating"] isEqual:[NSNull null]]){
                infoObj.rating = [infoObj.reviewsMeta valueForKey:@"Rating"];
            }
            infoObj.reviewsCount = [[infoObj.reviewsMeta valueForKey:@"Count"]integerValue];

        }

        [tempArray addObject:infoObj];
    }
    NSSortDescriptor *sortByDistance = [NSSortDescriptor sortDescriptorWithKey:@"providerDistance" ascending:YES];
    NSArray *distanceArray = [tempArray sortedArrayUsingDescriptors:@[sortByDistance]];
    [providerList addObjectsFromArray:distanceArray];
    
    return providerList;
}

- (NSMutableArray*)getServiceProvidersInfo_For_Offers:(NSMutableArray*)infoArray  {
    
    NSMutableArray *serviceCategory = [[NSMutableArray alloc]init];
    
    for (NSDictionary *providerInfo in infoArray) {
        
        ServicesCategoriesInfo *infoObj = [[ServicesCategoriesInfo alloc]init];
        
        NSDictionary *dictInfo = [providerInfo valueForKey:@"ServiceProvider"];
        
        infoObj.Id = [dictInfo valueForKey:@"Id"];
        if (APP_DEBUG_MODE) { NSLog(@"Provider id is %@",infoObj.Id);}

        if (([dictInfo valueForKey:@"Latitude"])  != nil) {
            infoObj.latitude = [dictInfo valueForKey:@"Latitude"];
        }
        
        if (([dictInfo valueForKey:@"Longitude"])  != nil) {
            infoObj.longitude = [dictInfo valueForKey:@"Longitude"];
        }
        if ([dictInfo valueForKey:@"Location"]  != nil) {
            infoObj.location = [dictInfo valueForKey:@"Location"];
        }
        
        if ([dictInfo valueForKey:@"Name"]  != nil) {
            infoObj.categoryName = [dictInfo valueForKey:@"Name"];
        }
        
        ProvideServiceAt *serviceAtObj = [ProvideServiceAt getSharedInstance];
        infoObj.provideServiceAt = [serviceAtObj provideServiceAtInfoObj:[dictInfo valueForKey:@"ProvideServiceAt"]];
        
        if ([dictInfo valueForKey:@"Reviews"]  != nil && ![[dictInfo valueForKey:@"Reviews"] isEqual:[NSNull null]]) {
            NSArray *reviews = [dictInfo valueForKey:@"Reviews"];
            infoObj.reviewsMeta = reviews[0];
            if ([infoObj.reviewsMeta valueForKey:@"Rating"]  != nil && ![[infoObj.reviewsMeta valueForKey:@"Rating"] isEqual:[NSNull null]]){
                infoObj.rating = [infoObj.reviewsMeta valueForKey:@"Rating"];
            }
            infoObj.reviewsCount = [[infoObj.reviewsMeta valueForKey:@"Count"]integerValue];
        }
        
        OffersDetail *offersDetail  = [OffersDetail getSharedInstance];
        if ([dictInfo valueForKey:@"Offers"]  != nil && ![[dictInfo valueForKey:@"Offers"] isEqual:[NSNull null]]) {
            infoObj.offersArray = [offersDetail allOffers_DetailInfoObj:[dictInfo valueForKey:@"Offers"] providerId:infoObj.Id];
        }
        
        if (infoObj.offersArray.count>0) {
            [serviceCategory addObject:infoObj];
        }
    }
    
    return serviceCategory;
    
}
/*        "Name": "Women Hair cut",
 "Id": "5ca595b681ac9",
 "Image": "18243736becb1b332f954d0f836f341a.jpg",
 "BusinessCatId": "5ca590942a72b",
 "DisplayOrder": "1",
 "IsActive": "1",
 "providers": [
 {
 "Id": "5ca6f77279201",
 "Name": "Natalie Day Spa",
 "Address": " Tabuk Street, Riyadh Saudi Arabia",
 "Longitude": "36.56619079999996",
 "Latitude": "28.3835079",
 "Distance": "2404.5208324942523",
 "BusinessThumb": "2650105a5c2e04082e7163c85906f6b4.jpeg",
 "Openinghour": {
 "Dayname": "Thursday",
 "Fromtime": "09:00 AM ",
 "Totime": "06:00 PM "
 },
 "ProvideAt": [
 {
 "Id": "5d495b536b401",
 "ProvideAt": "Home"
 },
 {
 "Id": "5d495b536c359",
 "ProvideAt": "Shop"
 }
 ],
 "Reviews": [
 {
 "Reviews": "10",
 "Rating": "3.7"
 }
 ]
 },*/

#pragma mark - Get All Services List

- (NSMutableArray*)getAllServicesCategoryList:(NSMutableArray*)infoArray {
    
    NSMutableArray *servicesCategory = [[NSMutableArray alloc]init];
    
    for (NSDictionary *serviceInfo in infoArray) {
        
        ServicesCategoriesInfo *infoObj = [[ServicesCategoriesInfo alloc]init];
        
        infoObj.categoryName = [serviceInfo valueForKey:@"Name"];
        infoObj.Id = [serviceInfo valueForKey:@"Id"];
        infoObj.categoryImage = [serviceInfo valueForKey:@"Image"];
        infoObj.businessCatId = [serviceInfo valueForKey:@"BusinessCatId"];
        infoObj.displayOrder = [[serviceInfo valueForKey:@"DisplayOrder"]integerValue];
        infoObj.isActive = [[serviceInfo valueForKey:@"IsActive"]boolValue];
        
        if ([serviceInfo valueForKey:@"providers"]  != nil && ![[serviceInfo valueForKey:@"providers"] isEqual:[NSNull null]]) {
            infoObj.providerCategories = [self serviceProvidersListInfoObject:[serviceInfo valueForKey:@"providers"]];
        }
        [servicesCategory addObject:infoObj];
    }
    
    return servicesCategory;
}



@end
