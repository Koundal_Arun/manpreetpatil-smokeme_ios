//
//  ServicesForInfo.m
//  iBeauty
//
//  Created by App Innovation on 16/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "ServicesForInfo.h"

static ServicesForInfo *singletonClass = nil;

@implementation ServicesForInfo

+(ServicesForInfo*)getSharedInstance {
    
    if (singletonClass == nil) {
        singletonClass = [[ServicesForInfo allocWithZone:NULL] init];
    }
    return singletonClass;
    
}

- (NSMutableArray*)serviceForInfoObj:(NSMutableArray*)infoArray {
    
    NSMutableArray *serviceForInfo = [[NSMutableArray alloc]init];
    
    for (NSDictionary *dictInfo in infoArray) {
        
        ServicesForInfo *infoObj = [[ServicesForInfo alloc]init];
        infoObj.Id = [[dictInfo valueForKey:@"Id"]integerValue];
        infoObj.name = [dictInfo valueForKey:@"ProvideFor"];
        
        [serviceForInfo addObject:infoObj];
    }
    
    return serviceForInfo;
}


@end
