//
//  BusinessReviews.m
//  iBeauty
//
//  Created by App Innovation on 23/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "BusinessReviews.h"

static BusinessReviews *singletonClass = nil;


@implementation BusinessReviews

+(BusinessReviews*)getSharedInstance {
    
    if (singletonClass == nil) {
        singletonClass = [[BusinessReviews allocWithZone:NULL] init];
    }
    return singletonClass;
    
}

- (NSMutableArray*)businessReviewsInfoObj:(NSMutableArray*)infoArray {
    
    NSMutableArray *businessReviewsInfo = [[NSMutableArray alloc]init];
    
    for (NSDictionary *dictInfo in infoArray) {
        
        BusinessReviews *infoObj = [[BusinessReviews alloc]init];
        
        infoObj.iD = [[dictInfo valueForKey:@"Id"]integerValue];
        infoObj.reviewedBy = [dictInfo valueForKey:@"ReviewedBy"];
        infoObj.reviewDate = [dictInfo valueForKey:@"ReviewDate"];
        infoObj.reviewText = [dictInfo valueForKey:@"ReviewText"];
        infoObj.rating = [[dictInfo valueForKey:@"Rating"]floatValue];
        infoObj.imageUrl = [dictInfo valueForKey:@"ImageUrl"];

        [businessReviewsInfo addObject:infoObj];
    }
    
    return businessReviewsInfo;
    
}


@end
