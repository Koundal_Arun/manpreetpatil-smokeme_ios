//
//  ProviderImages.m
//  iBeauty
//
//  Created by App Innovation on 16/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "ProviderImages.h"

static ProviderImages *singletonClass = nil;

@implementation ProviderImages

+(ProviderImages*)getSharedInstance {
    
    if (singletonClass == nil) {
        singletonClass = [[ProviderImages allocWithZone:NULL] init];
    }
    return singletonClass;
    
}

- (NSMutableArray*)providerImagesInfoObj:(NSMutableArray*)infoArray {
    
    NSMutableArray *providerImagesInfo = [[NSMutableArray alloc]init];
    
    for (NSDictionary *dictInfo in infoArray) {
        
        ProviderImages *infoObj = [[ProviderImages alloc]init];
//        infoObj.displayOrder = [[dictInfo valueForKey:@"DisplayOrder"]integerValue];
        infoObj.Url = [dictInfo valueForKey:@"ImageUrl"];
        infoObj.imageId = [dictInfo valueForKey:@"Id"];

        if ([dictInfo valueForKey:@"Type"]  != nil && ![[dictInfo valueForKey:@"Type"] isEqual:[NSNull null]]) {
            infoObj.type = [dictInfo valueForKey:@"Type"];
        }
        [providerImagesInfo addObject:infoObj];
    }
    
    return providerImagesInfo;
}


@end
