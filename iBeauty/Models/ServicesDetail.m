//
//  ServicesDetail.m
//  iBeauty
//
//  Created by App Innovation on 02/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "ServicesDetail.h"
#import "UILabel+FlexibleWidHeight.h"

static ServicesDetail *singletonClass = nil;

@implementation ServicesDetail

+ (ServicesDetail*)getSharedInstance {
    
    if (singletonClass == nil) {
        singletonClass = [[ServicesDetail allocWithZone:NULL] init];
    }
    return singletonClass;
    
}

- (NSMutableArray*)serviceDetailInfoObject:(NSMutableArray*)infoArray {
    
    NSMutableArray *serviceDetailInfo = [[NSMutableArray alloc]init];
    NSMutableArray *tempInfo = [[NSMutableArray alloc]init];

    for (NSDictionary *dictInfo in infoArray) {
        
        ServicesDetail *infoObj = [[ServicesDetail alloc]init];
//        NSMutableArray *categoryArray = [dictInfo valueForKey:@"Categories"];
//        NSDictionary *catDict = categoryArray[0];
//        infoObj.serviceCategoryId = [catDict valueForKey:@"Id"];
//        infoObj.serviceDescription = [dictInfo valueForKey:@"Description"];
//        infoObj.serviceId = [dictInfo valueForKey:@"Id"];
//        infoObj.serviceImage = [dictInfo valueForKey:@"ImageUrl"];
//        infoObj.categoryName = [dictInfo valueForKey:@"Name"];
//        NSDictionary *priceDict = [dictInfo valueForKey:@"Price"];
//        infoObj.serviceAmount = [priceDict valueForKey:@"Amount"];
//        infoObj.salesPrice = [priceDict valueForKey:@"SalePrice"];
//        infoObj.serviceAmountCurrency = [priceDict valueForKey:@"Currency"];
//        NSDictionary *timeDict = [dictInfo valueForKey:@"ServiceTime"];
//        infoObj.serviceTime = [timeDict valueForKey:@"Amount"];
//        infoObj.serviceTimeUnits = [timeDict valueForKey:@"Unit"];
        infoObj = [self getServiceDetailInfoObjectOnly:dictInfo];
        [tempInfo addObject:infoObj];

    }
    
    NSSortDescriptor *sortById = [NSSortDescriptor sortDescriptorWithKey:@"serviceCategoryId" ascending:YES];
    NSArray *sortedArray = [tempInfo sortedArrayUsingDescriptors:@[sortById]];
    [serviceDetailInfo addObjectsFromArray:sortedArray];
    
    return serviceDetailInfo;
    
}

- (NSMutableDictionary*)filterServicesArrayAccordingToServiceId:(NSMutableArray*)servicesDetailArray {
    
    NSString *serviceCategoryId = @"";
    
    NSMutableDictionary *serviceListDict = [NSMutableDictionary new];
    NSMutableArray *serviceListArray ;
    NSMutableArray *serviceSectionArray = [NSMutableArray new];
    

    for (int i =0;i<servicesDetailArray.count;i++) {
        
        ServicesDetail *detailObject = servicesDetailArray[i];
        
        NSLog(@"Service Category Id is %@",detailObject.serviceCategoryId);
        
        NSString *catId = detailObject.serviceCategoryId;
        
        if (![catId  isEqualToString:serviceCategoryId]) {
            
            serviceListArray = [[NSMutableArray alloc]init];
            
            [serviceListArray addObject:detailObject];
            [serviceSectionArray addObject:catId];
            serviceCategoryId = catId;
        }else {
            [serviceListArray addObject:detailObject];
            
            serviceCategoryId = catId;
        }
        if (serviceListArray.count>0){
            [serviceListDict setObject:serviceListArray forKey:[NSString stringWithFormat:@"%@",serviceCategoryId]];
        }
     }
    
//    NSMutableArray *reverseArray = [[[serviceSectionArray reverseObjectEnumerator] allObjects] mutableCopy];

    [serviceListDict setObject:serviceSectionArray forKey:@"servicesSectionArray"];
    return serviceListDict;
}

- (NSMutableArray*)serviceDetailInfoObject_Appointments:(NSMutableArray*)infoArray {
    
    NSMutableArray *serviceDetailInfo = [[NSMutableArray alloc]init];
    NSMutableArray *tempInfo = [[NSMutableArray alloc]init];
    
    for (NSDictionary *dictInfo in infoArray) {
        
        ServicesDetail *infoObj = [[ServicesDetail alloc]init];
        
        infoObj = [self getServiceDetailInfoObjectOnly:dictInfo];
        [tempInfo addObject:infoObj];
        
    }
    
//    NSSortDescriptor *sortById = [NSSortDescriptor sortDescriptorWithKey:@"categoryName_service" ascending:YES];
    NSSortDescriptor *sortById = [NSSortDescriptor sortDescriptorWithKey:@"categoryName_service" ascending:YES selector:@selector(caseInsensitiveCompare:)];
    NSArray *sortedArray = [tempInfo sortedArrayUsingDescriptors:@[sortById]];
    [serviceDetailInfo addObjectsFromArray:sortedArray];
    
    return serviceDetailInfo;
    
}

- (ServicesDetail*)getServiceDetailInfoObjectOnly:(NSDictionary*)dictInfo {
    
    ServicesDetail *infoObj = [[ServicesDetail alloc]init];
    
    id catInfo = [dictInfo valueForKey:@"Categories"];
    NSDictionary *catDict;
    if ([catInfo isKindOfClass: [NSDictionary class]]) {
        catDict = catInfo;
    }else {
        catDict = catInfo[0];
    }
    infoObj.serviceCategoryId = [catDict valueForKey:@"Id"];
    infoObj.categoryDisplayOrder = [[catDict valueForKey:@"DisplayOrder"]integerValue];
    infoObj.categoryImageUrl = [catDict valueForKey:@"ImageUrl"];
    infoObj.categoryName_service = [catDict valueForKey:@"Name"];
    
    infoObj.serviceDescription = [dictInfo valueForKey:@"Description"];
    infoObj.serviceId = [dictInfo valueForKey:@"Id"];
    infoObj.serviceImage = [dictInfo valueForKey:@"ImageUrl"];
    infoObj.categoryName = [dictInfo valueForKey:@"Name"];
    NSDictionary *priceDict = [dictInfo valueForKey:@"Price"];
    infoObj.serviceAmount = [priceDict valueForKey:@"Amount"];
    infoObj.salesPrice = [priceDict valueForKey:@"SalePrice"];
    infoObj.serviceAmountCurrency = [priceDict valueForKey:@"Currency"];
    NSDictionary *timeDict = [dictInfo valueForKey:@"ServiceTime"];
    infoObj.serviceTime = [timeDict valueForKey:@"Amount"];
    infoObj.serviceTimeUnits = [timeDict valueForKey:@"Unit"];
    
    NSString *nameString = [NSString stringWithFormat:@"%@ (%@ min)",infoObj.categoryName,infoObj.serviceTime];
    
    float nameHeight = [UILabel heightOfTextForString:nameString andFont:[UIFont fontWithName:@"SegoeUI" size:15.0] maxSize:(CGSize){[UIScreen mainScreen].bounds.size.width
        -125, MAXFLOAT}];
    infoObj.nameHeight = nameHeight;
    
    float descriptionHeight = [UILabel heightOfTextForString:infoObj.serviceDescription andFont:[UIFont fontWithName:@"SegoeUI" size:15.0] maxSize:(CGSize){[UIScreen mainScreen].bounds.size.width-125, MAXFLOAT}];
    infoObj.descriptionHeight = descriptionHeight;
    
    return infoObj;
}

- (ServicesDetail*)getServiceDetailInfo_ForStaffObject:(NSDictionary*)dictInfo {
    
    ServicesDetail *infoObj = [[ServicesDetail alloc]init];
    
    NSDictionary *catDict = [dictInfo valueForKey:@"Categories"];
    infoObj.serviceCategoryId = [catDict valueForKey:@"Id"];
    infoObj.categoryDisplayOrder = [[catDict valueForKey:@"DisplayOrder"]integerValue];
    infoObj.categoryImageUrl = [catDict valueForKey:@"ImageUrl"];
    infoObj.categoryName_service = [catDict valueForKey:@"Name"];
    
    infoObj.serviceDescription = [dictInfo valueForKey:@"Description"];
    infoObj.serviceId = [dictInfo valueForKey:@"Id"];
    infoObj.serviceImage = [dictInfo valueForKey:@"ImageUrl"];
    infoObj.categoryName = [dictInfo valueForKey:@"Name"];
    
    
    return infoObj;
}



@end
