//
//  ProvideServiceAt.m
//  iBeauty
//
//  Created by App Innovation on 16/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "ProvideServiceAt.h"

static ProvideServiceAt *singletonClass = nil;

@implementation ProvideServiceAt

+(ProvideServiceAt*)getSharedInstance {
    
    if (singletonClass == nil) {
        singletonClass = [[ProvideServiceAt allocWithZone:NULL] init];
    }
    return singletonClass;
    
}

- (NSMutableArray*)provideServiceAtInfoObj:(NSMutableArray*)infoArray {
    
    NSMutableArray *provideServiceAtInfo = [[NSMutableArray alloc]init];
    
    for (NSDictionary *dictInfo in infoArray) {
        
        ProvideServiceAt *infoObj = [[ProvideServiceAt alloc]init];
        infoObj.Id = [dictInfo valueForKey:@"Id"];
        infoObj.name = [dictInfo valueForKey:@"ProvideAt"];

        [provideServiceAtInfo addObject:infoObj];
    }
    
    return provideServiceAtInfo;
}

- (NSMutableArray*)provideProductAtInfoObj:(NSMutableArray*)infoArray {
    
    NSMutableArray *provideProductAtInfo = [[NSMutableArray alloc]init];
    
    for (NSDictionary *dictInfo in infoArray) {
        
        ProvideServiceAt *infoObj = [[ProvideServiceAt alloc]init];
        infoObj.Id = [dictInfo valueForKey:@"Id"];
        infoObj.name = [dictInfo valueForKey:@"ProvideAt"];
        infoObj.CreatedOn = [dictInfo valueForKey:@"CreatedOn"];
        infoObj.BusinessId = [dictInfo valueForKey:@"BusinessId"];
        infoObj.status = [[dictInfo valueForKey:@"Status"]integerValue];

        [provideProductAtInfo addObject:infoObj];
    }
    
    return provideProductAtInfo;
    
}



@end
