//
//  PaymentOrderInfo.m
//  iBeauty
//
//  Created by App Innovation on 12/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "PaymentOrderInfo.h"
#import "ServicesDetail.h"
#import "AllAppointmentsInfo.h"
#import "TaxInfoModel.h"
#import "OffersDetail.h"

static PaymentOrderInfo *singletonClass = nil;


@implementation PaymentOrderInfo

+(PaymentOrderInfo*)getSharedInstance {
    
    if (singletonClass == nil) {
        singletonClass = [[PaymentOrderInfo allocWithZone:NULL] init];
    }
    return singletonClass;
    
}

- (PaymentOrderInfo*)getPaymentOrderSummaryInfoObject:(NSDictionary*)infoDict {
    
    NSDictionary *priceSummaryDict = [NSDictionary new];
    priceSummaryDict = [infoDict valueForKey:@"BasketPriceSummary"];
    
    PaymentOrderInfo *infoObj = [[PaymentOrderInfo alloc]init];
    infoObj.orderId = [infoDict valueForKey:@"BookingId"];
    infoObj.discount = [priceSummaryDict valueForKey:@"Discount"];
    infoObj.orderPrice = [priceSummaryDict valueForKey:@"OrderPrice"];
    infoObj.tax = [priceSummaryDict valueForKey:@"Tax"];
    infoObj.totalPrice = [priceSummaryDict valueForKey:@"TotalPrice"];
    infoObj.orderDateTime = [infoDict valueForKey:@"OrderDateTime"];
    infoObj.serviceProvider = [infoDict valueForKey:@"ServiceProvider"];
    AllAppointmentsInfo *detailObj = [AllAppointmentsInfo getSharedInstance];
    
    if ([infoDict valueForKey:@"Services"]  != nil && ![[infoDict valueForKey:@"Services"] isEqual:[NSNull null]]) {
        infoObj.appointmentsInfo = [detailObj appointmentsInfoObject:[infoDict valueForKey:@"Services"]];
    }
    return infoObj;
    
}


@end
