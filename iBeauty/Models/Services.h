//
//  Services.h
//  iBeauty
//
//  Created by App Innovation on 27/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Services : NSObject

@property (nonatomic,strong) NSString *serviceImage;
@property (nonatomic,strong) NSString *serviceID;
@property (nonatomic,assign) NSInteger serviceDisplayOrder;
@property (nonatomic,strong) NSString *serviceName;

+(Services*)getSharedInstance;

- (NSMutableArray*)serviceInfoObject:(NSMutableArray*)infoArray;

@end
