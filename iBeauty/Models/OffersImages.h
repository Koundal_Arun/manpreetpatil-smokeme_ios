//
//  OffersImages.h
//  iBeauty
//
//  Created by App Innovation on 19/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OffersImages : NSObject

@property (nonatomic,assign) NSInteger displayOrder;
@property (nonatomic,strong) NSString *Url;

+ (OffersImages*)getSharedInstance;

- (NSMutableArray*)offersImagesInfoObj:(NSMutableArray*)infoArray;

@end
