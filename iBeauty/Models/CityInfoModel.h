//
//  CityInfoModel.h
//  iBeauty
//
//  Created by App Innovation on 18/12/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <Foundation/Foundation.h>


NS_ASSUME_NONNULL_BEGIN

@interface CityInfoModel : NSObject

@property (nonatomic,strong) NSString *cityID;
@property (nonatomic,strong) NSString *cityName_en;
@property (nonatomic,strong) NSString *cityName_ar;
@property (nonatomic,strong) NSString *state_en;
@property (nonatomic,strong) NSString *state_ar;

+ (CityInfoModel*)getSharedInstance;

- (NSMutableArray*)getCityInfoArray:(NSMutableArray*)infoArray;

@end

NS_ASSUME_NONNULL_END
