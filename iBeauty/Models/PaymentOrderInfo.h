//
//  PaymentOrderInfo.h
//  iBeauty
//
//  Created by App Innovation on 12/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PaymentOrderInfo : NSObject

@property (nonatomic,strong) NSString *orderId;
@property (nonatomic,strong) NSString *discount;
@property (nonatomic,strong) NSString *orderPrice;
@property (nonatomic,strong) NSString *tax;
@property (nonatomic,strong) NSString *totalPrice;
@property (nonatomic,strong) NSMutableArray *appointmentsInfo;
@property (nonatomic,strong) NSString *orderDateTime;
@property (nonatomic,strong) NSMutableArray *serviceProvider;
@property (nonatomic,strong) NSMutableArray *taxArray;
@property (nonatomic,strong) NSMutableArray *productsArray;
@property (nonatomic,strong) NSMutableArray *offersArray;

+ (PaymentOrderInfo*)getSharedInstance;
- (PaymentOrderInfo*)getPaymentOrderSummaryInfoObject:(NSDictionary*)infoDict; // Service

@end
