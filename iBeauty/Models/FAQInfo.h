//
//  FAQInfo.h
//  iBeauty
//
//  Created by App Innovation on 06/11/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FAQInfo : NSObject


@property (nonatomic,assign) NSInteger iD;
@property (nonatomic,strong) NSString *Question;
@property (nonatomic,strong) NSString *Answer;
@property (nonatomic,strong) NSString *createdOn;
@property (nonatomic,strong) NSString *updatedOn;
@property (nonatomic,assign) NSInteger status;
@property (nonatomic,strong) NSString *type;
@property (nonatomic,assign) float  questionHeight;
@property (nonatomic,assign) float  answerHeight;

+ (FAQInfo*)getSharedInstance;

- (NSMutableArray*)FAQInfoObj:(NSMutableArray*)infoArray;

@end

NS_ASSUME_NONNULL_END
