//
//  CityInfoModel.m
//  iBeauty
//
//  Created by App Innovation on 18/12/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "CityInfoModel.h"

static CityInfoModel *singletonClass = nil;

@implementation CityInfoModel

+ (CityInfoModel*)getSharedInstance {
    
    if (singletonClass == nil) {
        singletonClass = [[CityInfoModel allocWithZone:NULL] init];
    }
    return singletonClass;
    
}

- (NSMutableArray*)getCityInfoArray:(NSMutableArray*)infoArray {
    
    NSMutableArray *cityInfoArray = [[NSMutableArray alloc]init];
    
    for (NSDictionary *dictInfo in infoArray) {
        
        CityInfoModel *infoObj = [[CityInfoModel alloc]init];
        
        infoObj.cityID = [NSString stringWithFormat:@"%@",[dictInfo valueForKey:@"CityId"]];
        infoObj.cityName_en = [dictInfo valueForKey:@"CityName_en"];
        infoObj.cityName_ar = [dictInfo valueForKey:@"CityName_ar"];
        infoObj.state_en = [dictInfo valueForKey:@"State_en"];
        infoObj.state_ar = [dictInfo valueForKey:@"State_ar"];

        [cityInfoArray addObject:infoObj];
    }
    
    return cityInfoArray;
    
}
@end
