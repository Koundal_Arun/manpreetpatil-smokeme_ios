//
//  PaymentOptions.m
//  iBeauty
//
//  Created by App Innovation on 09/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "PaymentOptions.h"

static PaymentOptions *singletonClass = nil;

@implementation PaymentOptions

+ (PaymentOptions*)getSharedInstance {
    
    if (singletonClass == nil) {
        singletonClass = [[PaymentOptions allocWithZone:NULL] init];
    }
    return singletonClass;
}


- (NSMutableArray*)getPaymentOptionsInfoObject:(NSDictionary*)infoDict {
    
    NSMutableArray *paymentOptionsInfo = [[NSMutableArray alloc]init];
    
    NSMutableArray *paymentOptionsArray = [NSMutableArray new];
    paymentOptionsArray = [infoDict valueForKey:@"PaymentOptions"];
    
    for (NSDictionary *paymentInfo in paymentOptionsArray) {
        
        PaymentOptions *infoObj = [[PaymentOptions alloc]init];
        infoObj.iD = [[paymentInfo valueForKey:@"Id"]integerValue];
        infoObj.name = [paymentInfo valueForKey:@"Name"];
        [paymentOptionsInfo addObject:infoObj];
        
    }
    
    return paymentOptionsInfo;
    
}

@end
