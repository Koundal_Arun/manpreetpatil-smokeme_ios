//
//  BusinessReviews.h
//  iBeauty
//
//  Created by App Innovation on 23/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BusinessReviews : NSObject

@property (nonatomic,assign) NSInteger iD;
@property (nonatomic,strong) NSString *reviewedBy;
@property (nonatomic,strong) NSString *reviewDate;
@property (nonatomic,strong) NSString *reviewText;
@property (nonatomic,assign) float rating;
@property (nonatomic,strong) NSString *businessId;
@property (nonatomic,strong) NSString *imageUrl;


+ (BusinessReviews*)getSharedInstance;

- (NSMutableArray*)businessReviewsInfoObj:(NSMutableArray*)infoArray;

@end
