//
//  Services.m
//  iBeauty
//
//  Created by App Innovation on 27/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "Services.h"
#import "Constants.h"

static Services *singletonClass = nil;

@implementation Services

+(Services*)getSharedInstance {
    
    if (singletonClass == nil) {
        singletonClass = [[Services allocWithZone:NULL] init];
    }
    return singletonClass;
    
}

- (NSMutableArray*)serviceInfoObject:(NSMutableArray*)infoArray {
    
   NSSortDescriptor * sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Name" ascending:YES];
   NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
   NSArray *sortedArray = [infoArray sortedArrayUsingDescriptors:sortDescriptors];
    
    NSMutableArray *servicesObjectArray = [[NSMutableArray alloc]init];
    NSMutableArray *tempObjectArray = [[NSMutableArray alloc]init];

    for (NSDictionary *infoDict in sortedArray) {
        
        Services *infoObject = [[Services alloc]init];
        infoObject.serviceImage = [infoDict valueForKey:KServiceImage];
        infoObject.serviceName = [infoDict valueForKey:KServiceName];
        infoObject.serviceID = [NSString stringWithFormat:@"%@",[infoDict valueForKey:KServiceId]];
        infoObject.serviceDisplayOrder = [[infoDict valueForKey:KServiceDisplayOrder]integerValue];
        [tempObjectArray addObject:infoObject];
    }
    
    NSSortDescriptor *sortByDisplayOrder = [NSSortDescriptor sortDescriptorWithKey:@"serviceDisplayOrder" ascending:YES];
    NSArray *infoCategoryArray = [tempObjectArray sortedArrayUsingDescriptors:@[sortByDisplayOrder]];
    [servicesObjectArray addObjectsFromArray:infoCategoryArray];

    return servicesObjectArray;
}

@end
