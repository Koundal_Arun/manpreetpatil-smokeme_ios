//
//  TaxInfoModel.h
//  iBeauty
//
//  Created by App Innovation on 20/11/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TaxInfoModel : NSObject

@property (nonatomic,strong) NSString *taxName;
@property (nonatomic,assign) float taxAmount;
@property (nonatomic,assign) float taxPercentage;

+ (TaxInfoModel*)getSharedInstance;

- (NSMutableArray*)getTaxInfoObjectsArray:(NSMutableArray*)infoArray;

@end

NS_ASSUME_NONNULL_END
