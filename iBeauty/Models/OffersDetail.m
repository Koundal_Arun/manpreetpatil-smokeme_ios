//
//  OffersDetail.m
//  iBeauty
//
//  Created by App Innovation on 19/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "OffersDetail.h"
#import "ServicesDetail.h"
#import "OffersImages.h"
#import "ServicesCategoriesInfo.h"
#import "ProvideServiceAt.h"
#import "NSDate+iBeautyDateFormatter.h"

static OffersDetail *singletonClass = nil;

@implementation OffersDetail

+(OffersDetail*)getSharedInstance {
    
    if (singletonClass == nil) {
        singletonClass = [[OffersDetail allocWithZone:NULL] init];
    }
    return singletonClass;
    
}

- (NSMutableArray*)allOffers_DetailInfoObj:(NSMutableArray*)infoArray providerId:(NSString*)providerId {
    
    NSMutableArray *offersDetailInfo = [[NSMutableArray alloc]init];
    
    for (NSDictionary *dictInfo in infoArray) {
        
        OffersDetail *infoObj = [[OffersDetail alloc]init];
        
        infoObj.providerId = providerId;
        infoObj.offerId = [dictInfo valueForKey:@"Id"];
        infoObj.offerTitle = [dictInfo valueForKey:@"Name"];
        infoObj.discountType = [dictInfo valueForKey:@"DiscountType"];
        infoObj.detailDesc = [dictInfo valueForKey:@"Description"];
        
        NSDictionary *priceDict = [dictInfo valueForKey:@"Price"];
        infoObj.amount = [priceDict valueForKey:@"Amount"];
        infoObj.amount = [priceDict valueForKey:@"Currency"];
        
        ServicesDetail *detailObj = [ServicesDetail getSharedInstance];
        if ([dictInfo valueForKey:@"Services"]  != nil && ![[dictInfo valueForKey:@"Services"] isEqual:[NSNull null]]) {
            infoObj.servicesArray = [detailObj serviceDetailInfoObject_Appointments:[dictInfo valueForKey:@"Services"]];
        }

        [offersDetailInfo addObject:infoObj];
    }
    return offersDetailInfo;

}

- (NSMutableArray*)offersDetailInfoObj:(NSMutableArray*)infoArray {
    
    NSMutableArray *offersDetailInfo = [[NSMutableArray alloc]init];
    
    for (NSDictionary *dictInfo in infoArray) {
        
//        NSDictionary *dictInfo = [offerDict valueForKey:@"Offers"];
        
        OffersDetail *infoObj = [[OffersDetail alloc]init];
        infoObj.offerId = [dictInfo valueForKey:@"Id"];
        infoObj.offerTitle = [dictInfo valueForKey:@"Name"];
        infoObj.detailDesc = [dictInfo valueForKey:@"Description"];
        infoObj.discountType = [dictInfo valueForKey:@"DiscountType"];
        if ([dictInfo valueForKey:@"DiscountValue"]  != nil && ![[dictInfo valueForKey:@"DiscountValue"] isEqual:[NSNull null]]) {
            infoObj.discountValue = [[dictInfo valueForKey:@"DiscountValue"]integerValue];
        }
        infoObj.discountPrice = [dictInfo valueForKey:@"DiscountPrice"];
        infoObj.totalPrice = [dictInfo valueForKey:@"TotalPrice"];

        infoObj.offerStartDateTime = [dictInfo valueForKey:@"OfferStartDateTime"];
        infoObj.offerEndDateTime = [dictInfo valueForKey:@"OfferEndDateTime"];
        infoObj.offerFrom = [dictInfo valueForKey:@"OfferFrom"];
        infoObj.offer_EndDate = [NSDate convertNSStringToNSDate:[dictInfo valueForKey:@"OfferEndDateTime"] format:DATE_FORMAT_APPOINTMENT_DETAIL];
        infoObj.offer_StartDate = [NSDate convertNSStringToNSDate:[dictInfo valueForKey:@"OfferStartDateTime"] format:DATE_FORMAT_APPOINTMENT_DETAIL];

        NSDictionary *providerDict = [dictInfo valueForKey:@"ServiceProvider"];
        infoObj.providerId = [providerDict valueForKey:@"Id"];
        infoObj.providerName = [providerDict valueForKey:@"Name"];
        infoObj.providerAddress = [providerDict valueForKey:@"Address"];
        infoObj.latitude = [providerDict valueForKey:@"Latitude"];
        infoObj.longitude = [providerDict valueForKey:@"Longitude"];

        NSDictionary *offerImagesDict = [dictInfo valueForKey:@"OfferImages"];
        infoObj.imageUrl = [offerImagesDict valueForKey:@"Url"];
        
        ProvideServiceAt *serviceAtObj = [ProvideServiceAt getSharedInstance];
        infoObj.provideServiceAt = [serviceAtObj provideServiceAtInfoObj:[dictInfo valueForKey:@"ServiceProvideAt"]];

        ServicesDetail *detailObj = [ServicesDetail getSharedInstance];
        if ([dictInfo valueForKey:@"Services"]  != nil && ![[dictInfo valueForKey:@"Services"] isEqual:[NSNull null]]) {
            infoObj.servicesArray = [detailObj serviceDetailInfoObject_Appointments:[dictInfo valueForKey:@"Services"]];
        }
        if ([dictInfo valueForKey:@"TermConditions"]  != nil && ![[dictInfo valueForKey:@"TermConditions"] isEqual:[NSNull null]]) {
            infoObj.termsAndConditions = [dictInfo valueForKey:@"TermConditions"];
        }
        [offersDetailInfo addObject:infoObj];
    }
    
    return offersDetailInfo;
    
}

- (NSMutableArray*)offersDetailInfoObj_promotionalOffers:(NSMutableArray*)infoArray {
    
    NSMutableArray *offersDetailInfo = [[NSMutableArray alloc]init];
    
    for (NSDictionary *dictInfo in infoArray) {
        
//        NSDictionary *dictInfo = [offerDict valueForKey:@"Offers"];
        
        OffersDetail *infoObj = [[OffersDetail alloc]init];
        infoObj.offerId = [dictInfo valueForKey:@"Id"];
        infoObj.offerTitle = [dictInfo valueForKey:@"Name"];
        infoObj.detailDesc = [dictInfo valueForKey:@"Description"];
        infoObj.discountType = [dictInfo valueForKey:@"DiscountType"];
        if ([dictInfo valueForKey:@"DiscountValue"]  != nil && ![[dictInfo valueForKey:@"DiscountValue"] isEqual:[NSNull null]]) {
            infoObj.discountValue = [[dictInfo valueForKey:@"DiscountValue"]integerValue];
        }
        infoObj.discountPrice = [dictInfo valueForKey:@"DiscountPrice"];
        infoObj.totalPrice = [dictInfo valueForKey:@"TotalPrice"];
        
        infoObj.offerStartDateTime = [dictInfo valueForKey:@"OfferStartDateTime"];
        infoObj.offerEndDateTime = [dictInfo valueForKey:@"OfferEndDateTime"];
        infoObj.offerFrom = [dictInfo valueForKey:@"OfferFrom"];
        infoObj.offer_EndDate = [NSDate convertNSStringToNSDate:[dictInfo valueForKey:@"OfferEndDateTime"] format:DATE_FORMAT_APPOINTMENT_DETAIL];
        infoObj.offer_StartDate = [NSDate convertNSStringToNSDate:[dictInfo valueForKey:@"OfferStartDateTime"] format:DATE_FORMAT_APPOINTMENT_DETAIL];

        NSDictionary *providerDict = [dictInfo valueForKey:@"ServiceProvider"];
        infoObj.providerId = [providerDict valueForKey:@"Id"];
        infoObj.providerName = [providerDict valueForKey:@"Name"];
        infoObj.providerAddress = [providerDict valueForKey:@"Address"];
        infoObj.latitude = [providerDict valueForKey:@"Latitude"];
        infoObj.longitude = [providerDict valueForKey:@"Longitude"];
        
        NSDictionary *offerImagesDict = [dictInfo valueForKey:@"OfferImages"];
        infoObj.imageUrl = [offerImagesDict valueForKey:@"Url"];
        
        ProvideServiceAt *serviceAtObj = [ProvideServiceAt getSharedInstance];
        infoObj.provideServiceAt = [serviceAtObj provideServiceAtInfoObj:[dictInfo valueForKey:@"ServiceProvideAt"]];
        
        ServicesDetail *detailObj = [ServicesDetail getSharedInstance];
        if ([dictInfo valueForKey:@"Services"]  != nil && ![[dictInfo valueForKey:@"Services"] isEqual:[NSNull null]]) {
            infoObj.servicesArray = [detailObj serviceDetailInfoObject_Appointments:[dictInfo valueForKey:@"Services"]];
        }
        if ([dictInfo valueForKey:@"TermConditions"]  != nil && ![[dictInfo valueForKey:@"TermConditions"] isEqual:[NSNull null]]) {
            infoObj.termsAndConditions = [dictInfo valueForKey:@"TermConditions"];
        }
        [offersDetailInfo addObject:infoObj];
    }
    
    return offersDetailInfo;
    
}

- (NSMutableArray*)offersDetailInfoObjFor_SingleProvider:(NSMutableArray*)infoArray {

    NSMutableArray *offersDetailInfo = [[NSMutableArray alloc]init];
    
    for (NSDictionary *dictInfo in infoArray) {
        
        OffersDetail *infoObj = [[OffersDetail alloc]init];
        infoObj.offerId = [dictInfo valueForKey:@"Id"];
        infoObj.offerTitle = [dictInfo valueForKey:@"Name"];
        infoObj.imageUrl = [dictInfo valueForKey:@"ImageUrl"];
        infoObj.discountType = [dictInfo valueForKey:@"DiscountType"];
        infoObj.discountValue = [[dictInfo valueForKey:@"DiscountValue"]integerValue];
        infoObj.totalPrice = [dictInfo valueForKey:@"TotalPrice"];
        infoObj.detailDesc = [dictInfo valueForKey:@"Description"];
        infoObj.offerEndDateTime = [dictInfo valueForKey:@"ExpiryDate"];
        infoObj.discountPrice = [dictInfo valueForKey:@"DiscountPrice"];
        infoObj.offerFrom = [dictInfo valueForKey:@"OfferFrom"];
        infoObj.offer_EndDate = [NSDate convertNSStringToNSDate:[dictInfo valueForKey:@"ExpiryDate"] format:DATE_FORMAT_APPOINTMENT_DETAIL];
        
        ServicesDetail *detailObj = [ServicesDetail getSharedInstance];
        if ([dictInfo valueForKey:@"Services"]  != nil && ![[dictInfo valueForKey:@"Services"] isEqual:[NSNull null]]) {
            infoObj.servicesArray = [detailObj serviceDetailInfoObject_Appointments:[dictInfo valueForKey:@"Services"]];
        }
        
        [offersDetailInfo addObject:infoObj];
    }
    
    return offersDetailInfo;
    
}

- (NSMutableArray*)offersDetailInfoObjFor_Products_SingleSeller:(NSMutableArray*)infoArray {
    
    NSMutableArray *offersDetailInfo = [[NSMutableArray alloc]init];
    
    for (NSDictionary *dictInfo in infoArray) {
        
        OffersDetail *infoObj = [[OffersDetail alloc]init];
        infoObj.offerId = [dictInfo valueForKey:@"Id"];
        infoObj.offerTitle = [dictInfo valueForKey:@"Name"];
        infoObj.imageUrl = [dictInfo valueForKey:@"ImageUrl"];
        infoObj.discountType = [dictInfo valueForKey:@"DiscountType"];
        infoObj.discountValue = [[dictInfo valueForKey:@"DiscountValue"]integerValue];
        infoObj.totalPrice = [dictInfo valueForKey:@"TotalPrice"];
        infoObj.detailDesc = [dictInfo valueForKey:@"Description"];
        infoObj.offerEndDateTime = [dictInfo valueForKey:@"ExpiryDate"];
        infoObj.discountPrice = [dictInfo valueForKey:@"DiscountPrice"];
        infoObj.offerFrom = [dictInfo valueForKey:@"OfferFrom"];
        infoObj.offer_EndDate = [NSDate convertNSStringToNSDate:[dictInfo valueForKey:@"ExpiryDate"] format:DATE_FORMAT_APPOINTMENT_DETAIL];

        NSDictionary *providerDict ;
        
        if ([dictInfo valueForKey:@"ServiceProvider"]  != nil && ![[dictInfo valueForKey:@"ServiceProvider"] isEqual:[NSNull null]]) {
            
            providerDict = [dictInfo valueForKey:@"ServiceProvider"];
            infoObj.providerId = [providerDict valueForKey:@"Id"];
            infoObj.providerName = [providerDict valueForKey:@"Name"];
            infoObj.providerAddress = [providerDict valueForKey:@"Address"];
            infoObj.latitude = [providerDict valueForKey:@"Latitude"];
            infoObj.longitude = [providerDict valueForKey:@"Longitude"];
        }
        
        ProvideServiceAt *serviceAtObj = [ProvideServiceAt getSharedInstance];
        infoObj.provideServiceAt = [serviceAtObj provideServiceAtInfoObj:[dictInfo valueForKey:@"ServiceProvideAt"]];
        
        [offersDetailInfo addObject:infoObj];
    }
    
    return offersDetailInfo;
    
}


@end
