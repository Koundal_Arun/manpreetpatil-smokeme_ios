//
//  FAQInfo.m
//  iBeauty
//
//  Created by App Innovation on 06/11/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "FAQInfo.h"
#import "UILabel+FlexibleWidHeight.h"
#import "Constants.h"

static FAQInfo *singletonClass = nil;

@implementation FAQInfo

+(FAQInfo*)getSharedInstance {
    
    if (singletonClass == nil) {
        singletonClass = [[FAQInfo allocWithZone:NULL] init];
    }
    return singletonClass;
    
}

- (NSMutableArray*)FAQInfoObj:(NSMutableArray*)infoArray {
    
    NSMutableArray *FAQInfoArray = [[NSMutableArray alloc]init];
    
    for (NSDictionary *dictInfo in infoArray) {
        
        FAQInfo *infoObj = [[FAQInfo alloc]init];
        
        infoObj.iD = [[dictInfo valueForKey:@"Id"]integerValue];
        
        NSString *questionStr = [NSString stringWithFormat:@"%@",[dictInfo valueForKey:@"Question"]];
        infoObj.Question = questionStr;

        NSString *answerStr = [NSString stringWithFormat:@"%@",[dictInfo valueForKey:@"Answer"]];
        infoObj.Answer = answerStr;
        
        infoObj.createdOn = [dictInfo valueForKey:@"CreatedOn"];
        infoObj.updatedOn = [dictInfo valueForKey:@"UpdatedOn"];
        infoObj.type = [dictInfo valueForKey:@"Type"];
        infoObj.status = [[dictInfo valueForKey:@"Status"]integerValue];

        infoObj.questionHeight = [UILabel heightOfTextForString:infoObj.Question andFont:[UIFont fontWithName:@"SegoeUI" size:16.0] maxSize:(CGSize){SCREEN_WIDTH-50, MAXFLOAT}];

        infoObj.answerHeight = [UILabel heightOfTextForString:infoObj.Answer andFont:[UIFont fontWithName:@"SegoeUI" size:14.0] maxSize:(CGSize){SCREEN_WIDTH-30, MAXFLOAT}];
        
        [FAQInfoArray addObject:infoObj];
    }
    
    return FAQInfoArray;
    
}

@end
