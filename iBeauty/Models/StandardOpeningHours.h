//
//  StandardOpeningHours.h
//  iBeauty
//
//  Created by App Innovation on 17/01/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN


@interface StandardOpeningHours : NSObject

@property (nonatomic,strong) NSString *dayName;
@property (nonatomic,strong) NSString *fromTime;
@property (nonatomic,strong) NSString *toTime;
@property (nonatomic,assign) NSInteger status;
@property (nonatomic,strong) NSString *Id;

+ (StandardOpeningHours*)getSharedInstance;

- (NSMutableArray*)getStandardOpeningHours:(NSMutableArray*)infoArray;


@end

NS_ASSUME_NONNULL_END
