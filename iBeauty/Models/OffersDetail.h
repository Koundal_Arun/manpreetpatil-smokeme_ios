//
//  OffersDetail.h
//  iBeauty
//
//  Created by App Innovation on 19/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServicesCategoriesInfo.h"

@interface OffersDetail : NSObject

@property (nonatomic,strong) ServicesCategoriesInfo *serviceProvideInfo;

@property (nonatomic,strong) NSString *offerId;
@property (nonatomic,strong) NSString *offerTitle;
@property (nonatomic,strong) NSString *discountType;
@property (nonatomic,strong) NSString *discountPrice;
@property (nonatomic,assign) NSInteger discountValue;
@property (nonatomic,strong) NSString *totalPrice;
@property (nonatomic,strong) NSString *offerStartDateTime;
@property (nonatomic,strong) NSString *offerEndDateTime;
@property (nonatomic,strong) NSDate  *offer_EndDate;
@property (nonatomic,strong) NSDate  *offer_StartDate;

@property (nonatomic,assign) NSInteger increaseIndex;
@property (nonatomic,assign) NSInteger decreaseIndex;

@property (nonatomic,strong) NSString *imageUrl;
@property (nonatomic,strong) NSString *detailDesc;
@property (nonatomic,strong) NSString *amount;
@property (nonatomic,strong) NSString *currency;
@property (nonatomic,strong) NSString *providerId;
@property (nonatomic,strong) NSString *providerName;
@property (nonatomic,strong) NSString *providerAddress;
@property (nonatomic,strong) NSString *latitude;
@property (nonatomic,strong) NSString *longitude;

@property (nonatomic,strong) NSString *termsAndConditions;
@property (nonatomic,strong) NSString *offerFrom;

@property (nonatomic,strong) NSMutableArray *serviceCategoryArray;
@property (nonatomic,strong) NSMutableArray *servicesArray;
@property (nonatomic,strong) NSMutableArray *offerImagesArray;
@property (nonatomic,strong) NSMutableArray *provideServiceAt;

@property (nonatomic,strong) NSMutableArray *productsArray;

@property (nonatomic,assign) NSInteger quantitySelected;
@property (nonatomic,strong) NSString *offerBasketId;


+ (OffersDetail*)getSharedInstance;

- (NSMutableArray*)offersDetailInfoObj:(NSMutableArray*)infoArray;

- (NSMutableArray*)offersDetailInfoObj_promotionalOffers:(NSMutableArray*)infoArray;

- (NSMutableArray*)allOffers_DetailInfoObj:(NSMutableArray*)infoArray providerId:(NSString*)providerId;

- (NSMutableArray*)offersDetailInfoObjFor_SingleProvider:(NSMutableArray*)infoArray;


- (NSMutableArray*)offersDetailInfoObjFor_Products_SingleSeller:(NSMutableArray*)infoArray;


@end
