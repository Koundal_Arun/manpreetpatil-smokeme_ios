//
//  ServiceTimeSlot.h
//  iBeauty
//
//  Created by App Innovation on 05/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ServiceTimeSlot : NSObject

@property (nonatomic,strong) NSString *startDateTime;
@property (nonatomic,strong) NSString *endDateTime;
@property (nonatomic,strong) NSString *endTime;
@property (nonatomic,strong) NSString *startTime;
@property (nonatomic,assign) NSInteger selectStatus;
@property (nonatomic,strong) NSDate  *selectedDate;


+ (ServiceTimeSlot*)getSharedInstance;
- (NSMutableArray*)serviceTimeSlotsInfoObject:(NSMutableArray*)infoArray;

+ (ServiceTimeSlot*)getTimeSlotInfo:(NSString*)startTime endTime:(NSString*)endTime;

@end
