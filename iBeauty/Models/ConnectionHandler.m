//
//  ConnectionHandler.m
//  CanoeApp
//
//  Created by HashBrown Systems on 14/03/14.
//  Copyright (c) 2014 hashbrown. All rights reserved.
//

#import "ConnectionHandler.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "CommonClass.h"


@implementation ConnectionHandler
@synthesize manager;

static ConnectionHandler *sharedInstance = nil;

#pragma mark -- Initialization

+ (ConnectionHandler *)getSharedInstance {
    
    if (!sharedInstance){
        sharedInstance = [[super allocWithZone:NULL]init];
        [sharedInstance initializeConnection];
    }
    return sharedInstance;
}

- (void)initializeConnection {
    
    if (manager==nil) {
        manager = [AFHTTPSessionManager manager];
    }
}

#pragma mark -- Cancel Requests

- (void)stopAllRequests {
    
    [manager.operationQueue cancelAllOperations];
}

- (void)cancelAllHTTPOperationsWithPath:(NSString *)path
{
    [[manager session] getTasksWithCompletionHandler:^(NSArray *dataTasks, NSArray *uploadTasks, NSArray *downloadTasks) {
        [self cancelTasksInArray:dataTasks withPath:path];
        [self cancelTasksInArray:uploadTasks withPath:path];
        [self cancelTasksInArray:downloadTasks withPath:path];
    }];
}

- (void)cancelTasksInArray:(NSArray *)tasksArray withPath:(NSString *)path
{
    for (NSURLSessionTask *task in tasksArray) {
        NSRange range = [[[[task currentRequest]URL] absoluteString] rangeOfString:path];
        if (range.location != NSNotFound){
            NSLog(@"------ Request canceled --------");
            [task cancel];
        }
    }
}

#pragma mark -- Server Requests

-(void)jsonGETData:(NSString *)url parameters:(NSDictionary*)params onCompletion:(void(^)(id completed, NSError *error))completion {
    
    [self initializeConnection];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    [manager.requestSerializer setValue:accessToken forHTTPHeaderField:@"Authorization"];

    //NSString *urlString = [NSString stringWithFormat:@"%@?Authorization=%@",url,accessToken];

    [manager.requestSerializer setValue:@"public,max-age=1800" forHTTPHeaderField:@"Cache-Control"];
    [manager.requestSerializer setValue:@"max-age=1800" forHTTPHeaderField:@"Expires"];
    manager.requestSerializer.cachePolicy = NSURLRequestReturnCacheDataElseLoad;

//    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
//    [manager.requestSerializer setValue:accessToken forHTTPHeaderField:@"Authorization"];

    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    url = [url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    [manager GET:url parameters:params headers:nil progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        completion(responseObject,nil);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"failer %@",error);
        NSDictionary *dic = error.userInfo;
        if (![[dic objectForKey:@"NSLocalizedDescription"] isEqualToString:@"cancelled"]){
            completion(nil,error);
        }
        else {
            NSLog(@"No response");
        }
    }];
}

-(void) jsonPostData:(NSString *)url params:(NSDictionary *)params onCompletion:(void(^)(id completed, NSError *error))completion {

  NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
  NSString *contentType = @"";
    
    if (params) {
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:nil];
        NSString * postString =   [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
        contentType = @"application/json";
        NSLog(@"json ->>>>> is %@",postString);
        NSData *postData = [postString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        [request setHTTPBody:postData];
    }else {
        [request setHTTPBody:nil];
    }

    NSURL *urlString = [NSURL URLWithString:url];
    [request setURL:urlString];
    [request setHTTPMethod:@"POST"];
    [request setValue:contentType forHTTPHeaderField:@"Content-Type"];
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    [request setValue:accessToken forHTTPHeaderField:@"Authorization"];

//    [request setValue:@"text/json" forHTTPHeaderField:@"Accept"];
    
    NSURLSession *session = [NSURLSession sharedSession];

    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      
                                      if (error) {
                                          // Handle error...
                                          completion(nil,error);
                                          return;
                                      }
                                      
                                      if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
//                                          NSLog(@"Response HTTP Status code: %ld\n", (long)[(NSHTTPURLResponse *)response statusCode]);
//                                          NSLog(@"Response HTTP Headers:\n%@\n", [(NSHTTPURLResponse *)response allHeaderFields]);
                                      }
                                      
                                      NSString* body = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                      completion(body,nil);
                                  }];
    [task resume];
  
}

-(void)jsonPostDataForImage:(NSString *)url params:(NSDictionary *)params data:(NSData *)imageData onCompletion:(void(^)(id completed, NSError*error))completion{
    
    NSString *version = [params valueForKey:@"version"];
    NSString *token = [params valueForKey:@"token"];

//  NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:nil];
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:url parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:imageData name:@"imagetoadd" fileName:@"productImage.jpg" mimeType:@"image/jpeg"];
//        [formData appendPartWithFormData:jsonData name:kImageUpload];

    } error:nil];
    
    request.timeoutInterval = 120;
    [request setValue:version forHTTPHeaderField:@"X-App-Version"];
    [request setValue:token forHTTPHeaderField:@"Authorization"];

    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    defaultSessionConfiguration.requestCachePolicy = NSURLRequestReloadIgnoringCacheData;
    defaultSessionConfiguration.timeoutIntervalForRequest = 120;
    defaultSessionConfiguration.timeoutIntervalForResource = 120;
    AFURLSessionManager *sessionManager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];

    NSURLSessionUploadTask *uploadTask = [sessionManager uploadTaskWithStreamedRequest:request
                  progress:^(NSProgress * _Nonnull uploadProgress) {
                      //(float)totalBytesRead / totalBytesExpectedToRead;
//                      [SVProgressHUD showProgress:uploadProgress.fractionCompleted status:@"Uploading"];
                      if (uploadProgress.fractionCompleted == 100.0) {
                          dispatch_async(dispatch_get_main_queue(), ^{
//                              [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
                          });
                      }
                  }
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      if (error) {
                          NSLog(@"Error: %@", error);
                          completion(nil,error);
                      } else {
                          NSLog(@"%@ %@", response, responseObject);
                          completion(responseObject,nil);
                      }
                  }];
    [uploadTask resume];

}
- (void)jsonPostDataForUpdateProfileImage:(NSString *)url params:(NSDictionary *)params data:(NSData *)imageData onCompletion:(void(^)(id completed,  NSError*error))completion {
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:nil];
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:url parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:imageData name:@"imagetoadd" fileName:@"profileImage.jpg" mimeType:@"image/jpeg"];
        [formData appendPartWithFormData:jsonData name:kImageUpload];
    } error:nil];
    request.timeoutInterval = 120;
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    defaultSessionConfiguration.requestCachePolicy = NSURLRequestReloadIgnoringCacheData;
    defaultSessionConfiguration.timeoutIntervalForRequest = 120;
    defaultSessionConfiguration.timeoutIntervalForResource = 120;
    AFURLSessionManager *sessionManager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSURLSessionUploadTask *uploadTask = [sessionManager uploadTaskWithStreamedRequest:request
                                                                              progress:^(NSProgress * _Nonnull uploadProgress) {
                                                                                  //(float)totalBytesRead / totalBytesExpectedToRead;
                                                                                  [SVProgressHUD showProgress:uploadProgress.fractionCompleted status:@"Uploading"];
                                                                                  if (uploadProgress.fractionCompleted == 100.0) {
                                                                                      dispatch_async(dispatch_get_main_queue(), ^{
//                                                                                          [SVProgressHUD showWithStatus:@"Loading. . ."];
                                                                                      });
                                                                                  }
                                                                              }
                                                                     completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                                                                         if (error) {
                                                                             NSLog(@"Error: %@", error);
                                                                             completion(nil,error);
                                                                         } else {
                                                                             NSLog(@"%@ %@", response, responseObject);
                                                                             completion(responseObject,nil);
                                                                         }
                                                                     }];
    [uploadTask resume];
    
}

- (void)jsonDELETE_DATA:(NSString *)url header:(NSString*)header parameters:(NSDictionary*)params version:(NSString*)version onCompletion:(void(^)(id completed,NSError *error))completion {
    
    [self initializeConnection];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setStringEncoding:NSUTF8StringEncoding];
    
    [manager.requestSerializer setValue:header forHTTPHeaderField:@"Authorization"];
    [manager.requestSerializer setValue:@"text/json" forHTTPHeaderField:@"Accept"];
    [manager.requestSerializer setValue:version forHTTPHeaderField:@"X-Api-Version"];

    manager.requestSerializer.HTTPMethodsEncodingParametersInURI = [NSSet setWithObjects:@"GET", @"HEAD", nil];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    url = [url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    [manager DELETE:url parameters:params headers:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        completion(responseObject,nil);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"failer %@",error.localizedDescription);
        NSDictionary *dic=error.userInfo;
        if (![[dic objectForKey:@"NSLocalizedDescription"] isEqualToString:@"cancelled"]){
            completion(nil,error);
        }
        else {
            NSLog(@"No response");
        }
    }];
    
}


- (void)jsonDELETE_DATA:(NSString *)url header:(NSString*)header parameters:(NSDictionary*)params onCompletion:(void(^)(id completed,NSError *error))completion {
    
    [self initializeConnection];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:header forHTTPHeaderField:@"Authorization"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    manager.requestSerializer.HTTPMethodsEncodingParametersInURI = [NSSet setWithObjects:@"GET", @"HEAD", nil];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    url = [url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    [manager DELETE:url parameters:params headers:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        completion(responseObject,nil);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"failer %@",error.localizedDescription);
        NSDictionary *dic=error.userInfo;
        if (![[dic objectForKey:@"NSLocalizedDescription"] isEqualToString:@"cancelled"]){
            completion(nil,error);
        }
        else {
            NSLog(@"No response");
        }
    }];
    
}

-(void)jsonDownloadImage:(NSString *)url header:(NSString*)header onCompletion:(void(^)(id completed,NSError *error))completion {
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    manager.responseSerializer = [AFImageResponseSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    NSURL *URL = [NSURL URLWithString:url];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
        return [documentsDirectoryURL URLByAppendingPathComponent:[response suggestedFilename]];
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        NSLog(@"File downloaded to: %@", filePath);
    }];
    [downloadTask resume];
}

-(void)jsonGETDataHTTPWithDictionary:(NSString *)url params:(NSDictionary *)dic onCompletion:(void(^)(id completed))completion {
    
    [self initializeConnection];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager GET:url parameters:dic headers:nil progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"AfNetworkResponse %@",responseObject);
        completion(responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"failer %@",error);
        NSDictionary *dic=error.userInfo;
        if (![[dic objectForKey:@"NSLocalizedDescription"] isEqualToString:@"cancelled"]) {
            completion(error);
        }
        else {
            NSLog(@"No response");
        }
    }];
}


#pragma mark - Get service url string

+ (NSString *)getServiceURL:(NSInteger)type {
    
    NSString *strSubUrl = @"";
    switch (type){
            
        case TYPE_SERVICE_SERVICECATEGORIES:
            strSubUrl = [NSString stringWithFormat:@"businessprovidercategories"];
            break;
        case TYPE_SERVICE_SERVICE_PROVIDER_CATEGORIES:
            strSubUrl = [NSString stringWithFormat:@"serviceproviders"];
            break;
        case TYPE_SERVICE_SERVICE_PROVIDER_DETAIL:
            strSubUrl = [NSString stringWithFormat:@"serviceproviderDetails"];
            break;
        case TYPE_SERVICE_GET_STAFF_DETAILS:
            strSubUrl = [NSString stringWithFormat:@"staff/en?service="];
            break;
        case TYPE_SERVICE_GET_DATE_TIME_SLOTS:
            strSubUrl = [NSString stringWithFormat:@"slots"];
            break;
        case TYPE_SERVICE_CREATE_BASKET:
            strSubUrl = [NSString stringWithFormat:@"basket/create"];
            break;
        case TYPE_SERVICE_UPDATE_BASKET:
            strSubUrl = [NSString stringWithFormat:@"basket/"];
            break;
        case TYPE_SERVICE_GET_BASKET:
            strSubUrl = [NSString stringWithFormat:@"basket/"];
            break;
        case TYPE_SERVICE_PAYMENT_ORDERS:
            strSubUrl = [NSString stringWithFormat:@"basket/"];
            break;
        case TYPE_SERVICE_ALL_APPOINTMENTS:
            strSubUrl = [NSString stringWithFormat:@"order?"];
            break;
        case TYPE_SERVICE_ALL_OFFERS:
            strSubUrl = [NSString stringWithFormat:@"businessoffer"];
            break;
        case TYPE_SERVICE_LOGIN:
            strSubUrl = [NSString stringWithFormat:@"login"];
            break;
        case TYPE_SERVICE_REGISTER:
            strSubUrl = [NSString stringWithFormat:@"register"];
            break;
    }
    
    NSString *strURL = [NSString stringWithFormat:@"%@%@",STR_BASE_URL,strSubUrl];
    return strURL;
}

+ (NSString *)getServiceURL_NewAPIs:(NSInteger)type {
    
    NSString *languageCode =   [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];

    NSString *strSubUrl = @"";
    switch (type){
            
        case TYPE_SERVICE_LOGIN:
            strSubUrl = [NSString stringWithFormat:@"auth/token"];
            break;
        case TYPE_SERVICE_REGISTER:
            strSubUrl = [NSString stringWithFormat:@"auth/register"];
            break;
        case TYPE_SERVICE_SERVICECATEGORIES:
            strSubUrl = [NSString stringWithFormat:@"servicesapi/businessprovidercategories/%@",languageCode];
            break;
        case TYPE_SERVICE_SERVICE_PROVIDER_CATEGORIES:
            strSubUrl = [NSString stringWithFormat:@"servicesapi/serviceproviders/%@",languageCode];
            break;
        case TYPE_SERVICE_SERVICE_PROVIDER_DETAIL:
            strSubUrl = [NSString stringWithFormat:@"servicesapi/serviceproviderDetails/%@",languageCode];
            break;
        case TYPE_SERVICE_GET_DATE_TIME_SLOTS:
            strSubUrl = [NSString stringWithFormat:@"servicesapi/getslots/%@",languageCode];
            break;
        case TYPE_SERVICE_CREATE_BASKET:
            strSubUrl = [NSString stringWithFormat:@"servicesapi/createbasket/%@",languageCode];
            break;
        case TYPE_SERVICE_UPDATE_BASKET:
            strSubUrl = [NSString stringWithFormat:@"servicesapi/updatebasket/%@",languageCode];
            break;
        case TYPE_SERVICE_GET_BASKET:
            strSubUrl = [NSString stringWithFormat:@"servicesapi/getbasket/%@",languageCode];
            break;
        case TYPE_SERVICE_PRICING:
            strSubUrl = [NSString stringWithFormat:@"servicesapi/pricing/%@",languageCode];
            break;
        case TYPE_SERVICE_PAYMENT_ORDERS:
            strSubUrl = [NSString stringWithFormat:@"servicesapi/payment/%@",languageCode];
            break;
        case TYPE_SERVICE_ALL_APPOINTMENTS:
            strSubUrl = [NSString stringWithFormat:@"servicesapi/getBookings/%@",languageCode];
            break;
        case TYPE_SERVICE_ALL_OFFERS:
            strSubUrl = [NSString stringWithFormat:@"api/Api/businessoffer/%@",languageCode];
            break;
        case TYPE_SERVICE_ADD_ADDRESS:
            strSubUrl = [NSString stringWithFormat:@"servicesapi/add_address"];
            break;
        case TYPE_SERVICE_UPDATE_ADDRESS:
            strSubUrl = [NSString stringWithFormat:@"servicesapi/update_address"];
            break;
        case TYPE_SERVICE_GET_ADDRESSES:
            strSubUrl = [NSString stringWithFormat:@"servicesapi/user_address"];
            break;
        case TYPE_SERVICE_DELETE_ADDRESS:
            strSubUrl = [NSString stringWithFormat:@"servicesapi/delete_address"];
            break;
        case TYPE_SERVICE_CHANGE_PASSWORD:
            strSubUrl = [NSString stringWithFormat:@"auth/changePassword"];
            break;
        case TYPE_SERVICE_FAQ:
            strSubUrl = [NSString stringWithFormat:@"servicesapi/faq/%@",languageCode];
            break;
        case TYPE_SERVICE_RATING:
            strSubUrl = [NSString stringWithFormat:@"/servicesapi/addReviews"];
            break;
        case TYPE_SERVICE_FEEDBACK:
            strSubUrl = [NSString stringWithFormat:@"/servicesapi/CustomerFeedback"];
            break;
        case TYPE_SERVICE_PROMOTIONAL_ADS:
            strSubUrl = [NSString stringWithFormat:@"/servicesapi/PromotionalAds"];
            break;
        case TYPE_SERVICE_PROMOTIONAL_OFFER:
            strSubUrl = [NSString stringWithFormat:@"/servicesapi/GetOfferDetails/%@",languageCode];
            break;
        case TYPE_SERVICE_CANCEL_APPOINTMENT:
            strSubUrl = [NSString stringWithFormat:@"/servicesapi/cancelBookedAppointment/%@",languageCode];
            break;
        case TYPE_SERVICE_CANCEL_PACKAGE:
            strSubUrl = [NSString stringWithFormat:@"/servicesapi/cancelBookedPackage/%@",languageCode];
            break;
        case TYPE_SERVICE_BUSINESS_CITIES:
            strSubUrl = [NSString stringWithFormat:@"/servicesapi/businessCities"];
            break;
        case TYPE_SERVICE_PROVIDERBY_CITIES:
            strSubUrl = [NSString stringWithFormat:@"servicesapi/BusinessListByCity/%@",languageCode];
            break;
        case TYPE_SERVICE_FILTER_PROVIDERS:
            strSubUrl = [NSString stringWithFormat:@"servicesapi/filterServiceProvider/%@",languageCode];
            break;
        case TYPE_SERVICE_SERVICES_OFFER_LIST:
            strSubUrl = [NSString stringWithFormat:@"servicesapi/GetServiceOffers/%@",languageCode];
            break;
        case TYPE_SERVICE_CLEAR_SERVICE_BASKET:
            strSubUrl = [NSString stringWithFormat:@"servicesapi/clearBasket"];
            break;
        case TYPE_SERVICE_GET_ALL_CATEGORIES:
            strSubUrl = [NSString stringWithFormat:@"servicesapi/allservicecategories/%@",languageCode];
            break;
        case TYPE_SERVICE_GET_OFFER_DETAIL:
            strSubUrl = [NSString stringWithFormat:@"servicesapi/GetOfferDetails/%@",languageCode];
            break;
        case TYPE_SERVICE_FORGOT_PASSWORD:
            strSubUrl = [NSString stringWithFormat:@"auth/forgotPassword/%@",languageCode];
            break;
        case TYPE_SERVICE_RESET_PASSWORD:
            strSubUrl = [NSString stringWithFormat:@"auth/resetPassword"];
                break;
    }
    
    NSString *strURL = [NSString stringWithFormat:@"%@%@",STR_BASE_URL_NEW,strSubUrl];
    return strURL;
}

+ (NSString *)getServiceURL_Products:(NSInteger)type {
    
    NSString *languageCode =   [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];
    
    NSString *strSubUrl = @"";
    
    switch (type) {
            
        case TYPE_SERVICE_PRODUCT_SELLERS:
            strSubUrl = [NSString stringWithFormat:@"productApi/ProductSellerList/%@",languageCode];
            break;
        case TYPE_SERVICE_PRODUCT_SELLER_DETAIL:
            strSubUrl = [NSString stringWithFormat:@"productApi/ProductSellerDetail/%@",languageCode];
            break;
        case TYPE_SERVICE_PRODUCT_CREATE_BASKET:
            strSubUrl = [NSString stringWithFormat:@"productApi/createbasket/%@",languageCode];
            break;
        case TYPE_SERVICE_GET_PRODUCT_BASKET:
            strSubUrl = [NSString stringWithFormat:@"productApi/getbasket/%@",languageCode];
            break;
        case TYPE_SERVICE_GET_PRODUCT_UPDATE_BASKET:
            strSubUrl = [NSString stringWithFormat:@"productApi/updatebasket/%@",languageCode];
            break;
        case TYPE_SERVICE_GET_PRODUCTS_PRICING_INFO:
            strSubUrl = [NSString stringWithFormat:@"productApi/pricing/%@",languageCode];
            break;
        case TYPE_SERVICE_GET_PRODUCTS_PAYMENT_ORDER:
            strSubUrl = [NSString stringWithFormat:@"productApi/payment/%@",languageCode];
            break;
        case TYPE_SERVICE_GET_PRODUCTS_CUSTOMERS_ORDER:
            strSubUrl = [NSString stringWithFormat:@"productApi/getBookings/%@",languageCode];
            break;
        case TYPE_SERVICE_GET_PRODUCTS_BY_CITY:
            strSubUrl = [NSString stringWithFormat:@"productApi/BusinessListByCity/%@",languageCode];
            break;
        case TYPE_SERVICE_CANCEL_ORDER_PRODUCT:
            strSubUrl = [NSString stringWithFormat:@"productApi/cancelOrder/%@",languageCode];
            break;
        case TYPE_SERVICE_BUSINESS_PROVIDER_INFO:
            strSubUrl = [NSString stringWithFormat:@"productApi/BusinessDetailInfo/%@",languageCode];
            break;
        case TYPE_SERVICE_PRODUCTS_OFFER_LIST:
            strSubUrl = [NSString stringWithFormat:@"productApi/GetProductOffers/%@",languageCode];
            break;
        case TYPE_SERVICE_CLEAR_PRODUCT_BASKET:
            strSubUrl = [NSString stringWithFormat:@"productApi/clearBasket"];
            break;
    }
    
    NSString *strURL = [NSString stringWithFormat:@"%@%@",STR_BASE_URL_NEW,strSubUrl];
    return strURL;
    
}



@end
