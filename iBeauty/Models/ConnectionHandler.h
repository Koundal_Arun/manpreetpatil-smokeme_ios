//
//  ConnectionHandler.h
//  CanoeApp
//
//  Created by HashBrown Systems on 14/03/14.
//  Copyright (c) 2014 hashbrown. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

@interface ConnectionHandler : NSObject<NSURLSessionDelegate> {
    
    AFHTTPSessionManager *manager;
}

@property (strong, nonatomic) AFHTTPSessionManager *manager;

+ (ConnectionHandler *)getSharedInstance;

- (void)stopAllRequests;

- (void)cancelAllHTTPOperationsWithPath:(NSString *)path;

- (void)jsonGETData:(NSString *)url parameters:(NSDictionary*)params onCompletion:(void(^)(id completed,NSError *error))completion;

- (void)jsonPostData:(NSString *)url params:(NSDictionary *)params onCompletion:(void(^)(id completed, NSError*error))completion;

- (void)jsonPostDataForImage:(NSString *)url params:(NSDictionary *)params data:(NSData *)imageData onCompletion:(void(^)(id completed,  NSError*error))completion;

- (void)jsonDELETE_DATA:(NSString *)url header:(NSString*)header parameters:(NSDictionary*)params version:(NSString*)version onCompletion:(void(^)(id completed,NSError *error))completion;

- (void)jsonDELETE_DATA:(NSString *)url header:(NSString*)header parameters:(NSDictionary*)params onCompletion:(void(^)(id completed,NSError *error))completion;

-(void)jsonDownloadImage:(NSString *)url header:(NSString*)header onCompletion:(void(^)(id completed,NSError *error))completion;

- (void)jsonGETDataHTTPWithDictionary:(NSString *)url params:(NSDictionary *)dic onCompletion:(void(^)(id completed))completion;

+ (NSString *)getServiceURL:(NSInteger)type ;
+ (NSString *)getServiceURL_NewAPIs:(NSInteger)type;
+ (NSString *)getServiceURL_Products:(NSInteger)type;

@end
