//
//  BasketPriceSummary.m
//  iBeauty
//
//  Created by App Innovation on 09/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "BasketPriceSummary.h"
#import "TaxInfoModel.h"


static BasketPriceSummary *singletonClass = nil;

@implementation BasketPriceSummary

+ (BasketPriceSummary*)getSharedInstance {
    
    if (singletonClass == nil) {
        singletonClass = [[BasketPriceSummary allocWithZone:NULL] init];
    }
    return singletonClass;
    
}

- (BasketPriceSummary*)getBasketPriceSummaryInfoObject:(NSDictionary*)infoDict {
    
    NSDictionary *priceSummaryDict = [NSDictionary new];
    priceSummaryDict = [infoDict valueForKey:@"BasketPriceSummery"];
    
    BasketPriceSummary *infoObj = [[BasketPriceSummary alloc]init];
    infoObj.discount = [priceSummaryDict valueForKey:@"Discount"];
    infoObj.orderPrice = [priceSummaryDict valueForKey:@"OrderPrice"];
    TaxInfoModel *taxInfoObj = [TaxInfoModel getSharedInstance];
    infoObj.tax = [taxInfoObj getTaxInfoObjectsArray:[priceSummaryDict valueForKey:@"Tax"]];
    infoObj.totalTax = [self totolTaxAmount:infoObj.tax];
    infoObj.totalPrice = [priceSummaryDict valueForKey:@"TotalPrice"];
    infoObj.basketInfoDict = [infoDict valueForKey:@"Basket"];
    
    return infoObj;
    
}

- (float)totolTaxAmount:(NSMutableArray*)taxInfoArray {
    
    float taxAmount = 0.0;
    
    for (TaxInfoModel *taxInfo in taxInfoArray) {
        taxAmount = taxAmount + taxInfo.taxAmount;
    }
    
    return taxAmount;
}


@end
