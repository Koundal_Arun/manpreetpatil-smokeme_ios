//
//  TaxInfoModel.m
//  iBeauty
//
//  Created by App Innovation on 20/11/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "TaxInfoModel.h"

static TaxInfoModel *singletonClass = nil;

@implementation TaxInfoModel

+ (TaxInfoModel*)getSharedInstance {
    
    if (singletonClass == nil) {
        singletonClass = [[TaxInfoModel allocWithZone:NULL] init];
    }
    return singletonClass;
    
}

- (NSMutableArray *)getTaxInfoObjectsArray:(NSMutableArray*)infoArray {
    
    NSMutableArray *taxInfoArray = [NSMutableArray new];
    
    for(NSDictionary *dictInfo in infoArray) {
        
        TaxInfoModel *infoObj = [[TaxInfoModel alloc]init];
        infoObj.taxName = [dictInfo valueForKey:@"Name"];
        infoObj.taxAmount = [[dictInfo valueForKey:@"TaxAmount"]floatValue];
        infoObj.taxPercentage = [[dictInfo valueForKey:@"TaxPercentage"]floatValue];
        [taxInfoArray addObject:infoObj];
    }
    
    return taxInfoArray;
}


@end
