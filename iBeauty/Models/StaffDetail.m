//
//  StaffDetail.m
//  iBeauty
//
//  Created by App Innovation on 04/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "StaffDetail.h"

static StaffDetail *singletonClass = nil;

@implementation StaffDetail

+ (StaffDetail*)getSharedInstance {
    
    if (singletonClass == nil) {
        singletonClass = [[StaffDetail allocWithZone:NULL] init];
    }
    return singletonClass;
    
}

- (NSMutableArray*)staffDetailInfoObject:(NSMutableArray*)infoArray {
    
    NSMutableArray *staffDetailInfo = [[NSMutableArray alloc]init];
    
    for (NSDictionary *dictInfo in infoArray) {
        
        StaffDetail *infoObj = [[StaffDetail alloc]init];
        infoObj.gender = [dictInfo valueForKey:@"Gender"];
        infoObj.staffId = [dictInfo valueForKey:@"Id"];
        infoObj.staffImage = [dictInfo valueForKey:@"ImageUrl"];
        infoObj.staffName = [dictInfo valueForKey:@"Name"];
        infoObj.nationality = [dictInfo valueForKey:@"Nationaility"];
        infoObj.staffDescription = [dictInfo valueForKey:@"Description"];
        
        infoObj.staffEmailId = [dictInfo valueForKey:@"Email"];
        infoObj.staffMobileNumber = [dictInfo valueForKey:@"MobileNumber"];
        infoObj.availableToday = [[dictInfo valueForKey:@"AvailableToday"]boolValue];
        infoObj.isSelected = [[dictInfo valueForKey:@"isSelected"]boolValue];

        if ([dictInfo valueForKey:@"Services"]) {
            ServicesDetail *detailObj = [ServicesDetail getSharedInstance];
            infoObj.Services = [detailObj serviceDetailInfoObject:[dictInfo valueForKey:@"Services"]];
        }
        
        [staffDetailInfo addObject:infoObj];
    }
    
   return staffDetailInfo;
    
}

- (StaffDetail*)staffDetailInfoObjectOnly:(NSDictionary*)dictInfo {
    
    StaffDetail *infoObj = [[StaffDetail alloc]init];
    infoObj.gender = [dictInfo valueForKey:@"Gender"];
    infoObj.staffId = [dictInfo valueForKey:@"Id"];
    infoObj.staffImage = [dictInfo valueForKey:@"ImageUrl"];
    infoObj.staffName = [dictInfo valueForKey:@"Name"];
    infoObj.nationality = [dictInfo valueForKey:@"Nationaility"];
    infoObj.staffDescription = [dictInfo valueForKey:@"Description"];
    infoObj.isSelected = [[dictInfo valueForKey:@"isSelected"]boolValue];

    ServicesDetail *detailObj = [ServicesDetail getSharedInstance];
    infoObj.Services = [detailObj serviceDetailInfoObject_Appointments:[dictInfo valueForKey:@"Services"]];
//    infoObj.Services = [dictInfo valueForKey:@"Services"];
    
    return infoObj;
}



@end
