//
//  AllAppointmentsInfo.m
//  iBeauty
//
//  Created by App Innovation on 12/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "AllAppointmentsInfo.h"
#import "ServicesDetail.h"
#import "ProvideServiceAt.h"
#import "DisplayTagsInfo.h"
#import "ServicesForInfo.h"
#import "ProviderImages.h"
#import "StaffDetail.h"
#import "NSDate+iBeautyDateFormatter.h"
#import "Constants.h"
#import "UILabel+FlexibleWidHeight.h"


static AllAppointmentsInfo *singletonClass = nil;

@implementation AllAppointmentsInfo

+(AllAppointmentsInfo*)getSharedInstance {
    
    if (singletonClass == nil) {
        singletonClass = [[AllAppointmentsInfo allocWithZone:NULL] init];
    }
    return singletonClass;
    
}

- (NSMutableArray*)allAppointmentsInfoObject:(NSMutableArray*)infoArray {
    
    NSMutableArray *tempArray = [[NSMutableArray alloc]init];
    
    for (NSDictionary *dictInfo in infoArray) {
        
        AllAppointmentsInfo *infoObj = [[AllAppointmentsInfo alloc]init];
        
        infoObj.orderId = [dictInfo valueForKey:@"bookingId"];
        
        NSDictionary *paymentInfoDict = [dictInfo valueForKey:@"BasketPriceSummary"];
        NSLog(@"payment Info Dict is %@",paymentInfoDict);
        
        infoObj.discount = [paymentInfoDict valueForKey:@"Discount"];
        infoObj.orderPrice = [paymentInfoDict valueForKey:@"OrderPrice"];
        infoObj.tax = [paymentInfoDict valueForKey:@"Tax"];
        infoObj.totalPrice = [paymentInfoDict valueForKey:@"TotalPrice"];
        
        infoObj.appointmentsInfo = [self appointmentsInfoObject:[dictInfo valueForKey:@"Appointments"]];
        infoObj.endDateCount = [self getCancelAppointmentStatus:infoObj.appointmentsInfo];

        infoObj.providerId = [NSString stringWithFormat:@"%@",[dictInfo valueForKey:@"BusinessId"]];
        NSLog(@"provider id is %@",infoObj.providerId);
        infoObj.paymentOption = [[dictInfo valueForKey:@"PaymentOption"]integerValue];
        infoObj.dateCreatedOn = [dictInfo valueForKey:@"CreatedOn"];
        infoObj.createdDate = [NSDate convertNSStringToNSDate:[dictInfo valueForKey:@"CreatedOn"] format:DATE_FORMAT_APPOINTMENT_DETAIL];

        infoObj.providerName = [dictInfo valueForKey:@"Name"];
        infoObj.providerNameHeight = [UILabel heightOfTextForString:infoObj.providerName andFont:[UIFont fontWithName:@"SegoeUI-Semibold" size:17.0] maxSize:(CGSize){SCREEN_WIDTH-110, MAXFLOAT}];
        infoObj.descriptionInfo = [dictInfo valueForKey:@"Description"];
        infoObj.emailId = [dictInfo valueForKey:@"Email"];
        infoObj.phone = [dictInfo valueForKey:@"Phone"];
        infoObj.address = [dictInfo valueForKey:@"Address"];
        infoObj.providerAddressHeight = [UILabel heightOfTextForString:infoObj.address andFont:[UIFont fontWithName:@"SegoeUI" size:14.0] maxSize:(CGSize){SCREEN_WIDTH-110, MAXFLOAT}];
        infoObj.businessThumb = [dictInfo valueForKey:@"BusinessThumb"];
        infoObj.latitude = [dictInfo valueForKey:@"Latitude"];
        infoObj.longitude = [dictInfo valueForKey:@"Longitude"];
        infoObj.clientNote = [dictInfo valueForKey:@"ClientNote"];
        infoObj.serviceProvideAt = [dictInfo valueForKey:@"ServiceProvideAt"];
        infoObj.status = [[dictInfo valueForKey:@"Status"]boolValue];

        infoObj.isOfferApplied = [[dictInfo valueForKey:@"isOfferApplied"]boolValue];
        infoObj.offerID = [dictInfo valueForKey:@"OfferId"];

        [tempArray addObject:infoObj];
        
    }

    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createdDate" ascending:false];
    [tempArray sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    NSMutableArray *appointmentsDetailInfo = [NSMutableArray new];
    [appointmentsDetailInfo addObjectsFromArray:tempArray];
    
    return appointmentsDetailInfo;
    
}

- (NSMutableArray*)appointmentsInfoObject:(NSMutableArray*)infoArray {
    
    NSMutableArray *appointmentsDetailInfo = [[NSMutableArray alloc]init];
    
    for (NSDictionary *dictInfo in infoArray) {
        
        AllAppointmentsInfo *infoObj = [[AllAppointmentsInfo alloc]init];
        infoObj.appointmentId = [dictInfo valueForKey:@"Id"];
        infoObj.serviceName = [dictInfo valueForKey:@"ServiceName"];
        infoObj.startTimeAppointment = [dictInfo valueForKey:@"StartDateTime"];
        infoObj.endTimeAppointment = [dictInfo valueForKey:@"EndDateTime"];
        
        infoObj.endDate = [NSDate convertNSStringToNSDate:[dictInfo valueForKey:@"EndDateTime"] format:DATE_FORMAT_APPOINTMENT_DETAIL];
        infoObj.startDate = [NSDate convertNSStringToNSDate:[dictInfo valueForKey:@"StartDateTime"] format:DATE_FORMAT_APPOINTMENT_DETAIL];

        infoObj.status = [[dictInfo valueForKey:@"Status"]boolValue];

        ServicesDetail *serviceDetailObj = [ServicesDetail getSharedInstance];
        infoObj.serviceDetail = [serviceDetailObj getServiceDetailInfoObjectOnly:[dictInfo valueForKey:@"ServiceInfo"]];
        
        StaffDetail *detailObj = [StaffDetail getSharedInstance];
        if ([dictInfo valueForKey:@"Staff"]  != nil && ![[dictInfo valueForKey:@"Staff"] isEqual:[NSNull null]]) {
            infoObj.staffArray = [detailObj staffDetailInfoObject:[dictInfo valueForKey:@"Staff"]];
        }

        [appointmentsDetailInfo addObject:infoObj];

    }
    return appointmentsDetailInfo;

}

- (NSInteger)getCancelAppointmentStatus:(NSMutableArray*)appointmentsArray {
    
    NSInteger count = 0;
    
    for (AllAppointmentsInfo*appointmentObj in appointmentsArray) {
    
        NSComparisonResult result = [[NSDate date] compare:appointmentObj.endDate];
        if (result == NSOrderedDescending) {
            
            count += 1;
        }
    }
    
    return count;
}



@end
