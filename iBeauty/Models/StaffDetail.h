//
//  StaffDetail.h
//  iBeauty
//
//  Created by App Innovation on 04/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServicesDetail.h"


@class ServicesDetail;

@interface StaffDetail : NSObject

@property (nonatomic,strong) NSString *staffImage;
@property (nonatomic,strong) NSString *staffName;
@property (nonatomic,strong) NSString *staffRegion;
@property (nonatomic,strong) NSString *staffId;
@property (nonatomic,strong) NSString *gender;
@property (nonatomic,strong) NSString *nationality;
@property (nonatomic,strong) NSString *staffDescription;
@property (nonatomic,strong) NSMutableArray *Services;

@property (nonatomic,strong) NSString *staffMobileNumber;
@property (nonatomic,strong) NSString *staffEmailId;
@property (nonatomic,assign) BOOL availableToday;
@property (nonatomic,assign) BOOL isSelected;


@property (nonatomic,strong) ServicesDetail *detailInfo;


+ (StaffDetail*)getSharedInstance;

- (NSMutableArray*)staffDetailInfoObject:(NSMutableArray*)infoArray;

- (StaffDetail*)staffDetailInfoObjectOnly:(NSDictionary*)dictInfo;

@end
