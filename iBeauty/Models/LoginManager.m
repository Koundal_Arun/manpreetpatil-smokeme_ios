//
//  LoginManager.m
//  iBeauty
//
//  Created by App Innovation on 27/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "LoginManager.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "Constants.h"

@implementation LoginManager


- (void)loginWithFacebook:(void(^)(id result, NSError *error))completion {
    
    AppDelegate *delegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    delegate.loginManager = [[FBSDKLoginManager alloc] init];
    [delegate.loginManager logOut];
    
    if ([FBSDKAccessToken currentAccessToken]) {
        // [APPDELEGATE switchToHomeController];
    } else {
        
        [delegate.loginManager logInWithPermissions:@[@"email",@"public_profile"] fromViewController:nil handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            if (error) {
                
            } else {
                if (result.token) {
                    [self getFacebookUserProfileInfo:^(id result, NSError *error) {
                        if (result) {
                            completion (result,nil);
                        }else{
                            completion (nil,error);
                        }
                    }];
                }else {
                    
                }
            }
        }];
    }
}


#pragma mark - Find Facebook User Profile Info


- (void)getFacebookUserProfileInfo:(void(^)(id result, NSError *error))completion{
    
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
    
    FBSDKGraphRequest *requestMe = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"name,email,gender,first_name,last_name,picture.type(large)"}];
    
    FBSDKGraphRequestConnection *connection = [[FBSDKGraphRequestConnection alloc] init];
    
    [connection addRequest:requestMe completionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
        if(result)
        {
            if (APP_DEBUG_MODE)
                NSLog(@"Response is %@",result);
            
            if (completion) {
                completion (result,nil);
            }
            
        }else {
            completion (nil,error);
            
        }
    }];
    
    [connection start];
}


@end
