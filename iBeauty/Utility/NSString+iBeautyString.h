//
//  NSString+iBeautyString.h
//  iBeauty
//
//  Created by App Innovation on 02/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import "AddressModel.h"
#import "StandardOpeningHours.h"

@interface NSString (iBeautyString)

- (NSMutableAttributedString*)getAttributedTextWithTimeString:(NSString*)timeString openingStatus:(NSString*)openingStr;
- (NSString*)getOpengingHourString:(NSDictionary*)infoObj;
- (NSString*)getWeekDay;
+ (NSString *)parseCompleteAddress:(AddressModel* )addressInfo;
- (NSMutableAttributedString*)getAppointmentBookingAttributedTextWithId:(NSString*)titleString openingStatus:(NSString*)IdStr;
- (NSString*)getOpengingHourStringFromModel:(StandardOpeningHours*)openingHour;
- (NSInteger)weekDayNumber;
- (NSMutableAttributedString*)getOrdersAttributedTextWithId:(NSString*)titleString openingStatus:(NSString*)IdStr screenType:(NSInteger)type;

+ (NSString*)getOrderStatus:(NSInteger)status;

@end
