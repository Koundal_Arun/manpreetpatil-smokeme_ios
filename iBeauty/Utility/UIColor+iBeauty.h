//
//  UIColor+iBeauty.h
//  iBeauty
//
//  Created by App Innovation on 27/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (iBeauty)

+ (UIColor *)iBeautyThemeColor;
+ (UIColor *)facebookThemeColor;
+ (UIColor *)googleThemeColor;
+ (UIColor *)settingHeaderColor;
+ (UIColor*)getOrderStatusColor:(NSInteger)status;
+ (UIColor*)scrollViewTexturedBkgColor;
+ (UIColor *)cancelAppointmentThemeColor;

@end
