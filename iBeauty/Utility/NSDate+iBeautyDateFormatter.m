//
//  NSDate+iBeautyDateFormatter.m
//  iBeauty
//
//  Created by App Innovation on 02/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "NSDate+iBeautyDateFormatter.h"
#import "Constants.h"

@implementation NSDate (iBeautyDateFormatter)


+ (NSDate *)convertNSStringToNSDate:(NSString *)dateInString format: (NSString *)format {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone localTimeZone]; //[NSTimeZone timeZoneWithName:@"UTC"];
    dateFormatter.locale = [NSLocale currentLocale];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:format];
    NSDate *date = [dateFormatter dateFromString:dateInString];
    return date;
}

- (NSString *)convertDateValueToNSString:(NSString *)format localeStatus:(BOOL)status {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone localTimeZone]; //[NSTimeZone timeZoneWithName:@"UTC"];
    
    if (status == false) {
        dateFormatter.locale = [NSLocale currentLocale];
    }else {
        dateFormatter.locale = [NSLocale localeWithLocaleIdentifier:[[NSLocale preferredLanguages] firstObject]];
    }

    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:format];
    NSString *dateString = [dateFormatter stringFromDate:self];
    return dateString;
}

- (NSString *)convertDateToNSString:(NSString *)format date:(NSDate*)date {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    NSString *dateString = [dateFormatter stringFromDate:date];
    return dateString;
}

- (NSString *)convertTimeStringToFormattedNSString:(NSString *)timeString {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = SETTING_TIME_FORMAT_24;
    NSDate *date = [dateFormatter dateFromString:timeString];
    dateFormatter.dateFormat = SETTING_TIME_FORMAT_12;
    dateFormatter.locale = [NSLocale localeWithLocaleIdentifier:[[NSLocale preferredLanguages] firstObject]];
//    dateFormatter.locale = [NSLocale currentLocale];
    NSString *timeDateString = [dateFormatter stringFromDate:date];
    return timeDateString;
}

- (NSString *)getOpeningStringBasedOnStartAndEndTime:(NSDictionary*)openingHourDict format:(NSString*)timeFormat {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    dateFormatter.locale = [NSLocale currentLocale];
    dateFormatter.locale = [NSLocale localeWithLocaleIdentifier:[[NSLocale preferredLanguages] firstObject]];
    dateFormatter.dateFormat = timeFormat;
    NSDate *startDate = [dateFormatter dateFromString:[openingHourDict valueForKey:@"Fromtime"]];
    NSDate *endDate = [dateFormatter dateFromString:[openingHourDict valueForKey:@"Totime"]];
    
    NSDate *finalStartDate = [self getDateFromCurrentDate:startDate];
    NSDate *finalEndDate = [self getDateFromCurrentDate:endDate];
    
    NSComparisonResult startDateResult = [finalStartDate compare:[NSDate date]];
    NSComparisonResult endDateResult = [finalEndDate compare:[NSDate date]];

    NSString *timeDateString = @"";
    
    if (startDateResult == NSOrderedAscending && endDateResult == NSOrderedDescending) {
        NSString *openNowString = NSLocalizedString(@"Open Now", @"");
        timeDateString = [NSString stringWithFormat:@"(%@)",openNowString];
    }else {
        NSString *closedString = NSLocalizedString(@"Closed", @"");
        timeDateString = [NSString stringWithFormat:@"(%@)",closedString];
    }

    return timeDateString;
}

- (NSString *)getOpeningStringBasedOnStartAndEndTimeForAllDaysOpeningTime:(StandardOpeningHours*)infoObj format:(NSString*)timeFormat {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = timeFormat;
    NSDate *startDate = [dateFormatter dateFromString:infoObj.fromTime];
    NSDate *endDate = [dateFormatter dateFromString:infoObj.toTime];
    
    NSDate *finalStartDate = [self getDateFromCurrentDate:startDate];
    NSDate *finalEndDate = [self getDateFromCurrentDate:endDate];
    
    NSComparisonResult startDateResult = [finalStartDate compare:[NSDate date]];
    NSComparisonResult endDateResult = [finalEndDate compare:[NSDate date]];
    
    NSString *timeDateString = @"";
    
    if (startDateResult == NSOrderedAscending && endDateResult == NSOrderedDescending) {
        NSString *openNowString = NSLocalizedString(@"Open Now", @"");
        timeDateString = [NSString stringWithFormat:@"(%@)",openNowString];
    }else {
        NSString *closedString = NSLocalizedString(@"Closed", @"");
        timeDateString = [NSString stringWithFormat:@"(%@)",closedString];
    }
    
    return timeDateString;
    
}


- (NSDate *)getDateFromCurrentDate:(NSDate*)timeDate {
    
    NSDateComponents *dateComp = [[NSDateComponents alloc] init];
    
    NSDateComponents *currentDateComp = [[NSCalendar currentCalendar] components: NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:[NSDate date]];
    
    NSDateComponents *timeComp = [[NSCalendar currentCalendar] components: NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond fromDate:timeDate];
    
    [dateComp setYear:currentDateComp.year];
    [dateComp setMonth:currentDateComp.month];
    [dateComp setDay:currentDateComp.day];
    [dateComp setHour:timeComp.hour];
    [dateComp setMinute:timeComp.minute];
    [dateComp setSecond:timeComp.second];
    
    NSDate *expectedDate = [[NSCalendar currentCalendar] dateFromComponents:dateComp];
    
    return expectedDate;
}

- (NSDate *)withFormat:(NSString *)format {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    dateFormatter.locale = [NSLocale localeWithLocaleIdentifier:en_US];
    
    NSString *dateTest = [self convertDateToNSString:format date:self];
    
    NSDate *newDate = [dateFormatter dateFromString:dateTest];
    
    return newDate;
}
@end
