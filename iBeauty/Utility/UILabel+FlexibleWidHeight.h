//
//  UILabel+FlexibleWidHeight.h
//  iBeauty
//
//  Created by App Innovation on 05/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (FlexibleWidHeight)


+ (void)fixHeightOfThisLabel:(UILabel *)aLabel ;
+ (CGFloat)heightOfTextForString:(NSString *)aString andFont:(UIFont *)aFont maxSize:(CGSize)aSize ;
+ (void)fixWidthOfThisLabel:(UILabel *)aLabel;
+ (CGFloat)widthOfTextForString:(NSString *)aString andFont:(UIFont *)aFont maxSize:(CGSize)aSize;

@end
