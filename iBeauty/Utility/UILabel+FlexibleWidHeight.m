//
//  UILabel+FlexibleWidHeight.m
//  iBeauty
//
//  Created by App Innovation on 05/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "UILabel+FlexibleWidHeight.h"

@implementation UILabel (FlexibleWidHeight)


+ (void)fixHeightOfThisLabel:(UILabel *)aLabel {
    
    aLabel.frame = CGRectMake(aLabel.frame.origin.x,
                              aLabel.frame.origin.y,
                              aLabel.frame.size.width,
                              [self heightOfTextForString:aLabel.text
                                                  andFont:aLabel.font
                                                  maxSize:CGSizeMake(aLabel.frame.size.width, MAXFLOAT)]);
}

+ (CGFloat)heightOfTextForString:(NSString *)aString andFont:(UIFont *)aFont maxSize:(CGSize)aSize {
    
    // iOS7 or Greater
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
    {
        CGSize sizeOfText = [aString boundingRectWithSize: aSize
                                                  options: (NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                               attributes: [NSDictionary dictionaryWithObject:aFont forKey:NSFontAttributeName]
                                                  context: nil].size;
        
        return ceilf(sizeOfText.height);
    }
    
    CGRect textRect = [aString boundingRectWithSize:aSize
                                            options:NSStringDrawingUsesLineFragmentOrigin
                                         attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:13]}
                                            context:nil];
    CGSize size = textRect.size;
    return ceilf(size.height);
    
}

+ (void)fixWidthOfThisLabel:(UILabel *)aLabel {
    
    aLabel.frame = CGRectMake(aLabel.frame.origin.x,
                              aLabel.frame.origin.y,
                              [self widthOfTextForString:aLabel.text
                                                 andFont:aLabel.font
                                                 maxSize:CGSizeMake(MAXFLOAT, aLabel.frame.size.height)],
                              aLabel.frame.size.height);
}

+ (CGFloat)widthOfTextForString:(NSString *)aString andFont:(UIFont *)aFont maxSize:(CGSize)aSize {
    
    // iOS7 or Greater
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
    {
        CGSize sizeOfText = [aString boundingRectWithSize: aSize
                                                  options: (NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                               attributes: [NSDictionary dictionaryWithObject:aFont
                                                                                       forKey:NSFontAttributeName]
                                                  context: nil].size;
        
        return ceilf(sizeOfText.width);
    }

    // iOS6
    CGSize textSize = [aString boundingRectWithSize: aSize
                                            options: (NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                         attributes: [NSDictionary dictionaryWithObject:aFont forKey:NSFontAttributeName]
                                            context: nil].size;
    return ceilf(textSize.width);
}

@end
