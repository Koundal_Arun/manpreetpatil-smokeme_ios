//
//  iBeautyUtility.m
//  iBeauty
//
//  Created by App Innovation on 27/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "iBeautyUtility.h"
#import "Constants.h"
#import "iBeautyUtility.h"
#import "UIColor+iBeauty.h"
#import "BookAppointmentController.h"

@implementation iBeautyUtility

+ (NSString *)trimString:(NSString *)string {
    return [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

+ (NSString*)removeSpecialCharacterFromString:(NSString *)string {
    
    NSString *specialCharacterString = @"!~`@#$%^&*-+();:={}[],.<>?\\/\"\'";
    NSCharacterSet *specialCharacterSet = [NSCharacterSet
                                           characterSetWithCharactersInString:specialCharacterString];
    
    if ([string.lowercaseString rangeOfCharacterFromSet:specialCharacterSet].length) {
        
        NSCharacterSet *notAllowedChars = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"] invertedSet];
        NSString *resultString = [[string componentsSeparatedByCharactersInSet:notAllowedChars] componentsJoinedByString:@""];
        
        return resultString;
    }
    return string;
}

+ (BOOL) isValidEmailAddress:(NSString *)email {
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", stricterFilterString];
    if ([email containsString:@" "]) {
        return NO;
    }
    return [emailTest evaluateWithObject:email];
}

+ (void)setimageViewRound:(UIImageView *)imageView {
    
    imageView.contentMode = UIViewContentModeScaleToFill;
    NSInteger cornerRadius = imageView.frame.size.width / 2;
    imageView.layer.cornerRadius = cornerRadius;
    imageView.layer.masksToBounds = YES;
    imageView.layer.borderColor = [UIColor clearColor].CGColor;
    imageView.layer.borderWidth = 0.0;
}

+ (void)setImageViewCornerRound:(UIImageView *)imageView {
    
    NSInteger cornerRadius = 10;
    imageView.layer.cornerRadius = cornerRadius;
    imageView.layer.masksToBounds = YES;
    imageView.layer.borderColor = [UIColor clearColor].CGColor;
    imageView.layer.borderWidth = 0.0;
}

+ (void)setLabelRound:(UILabel *)label {
    
    NSInteger cornerRadius = 10;

    if (label.tag == 1) {
        cornerRadius = 12;
    }
    label.layer.cornerRadius = cornerRadius;
    label.layer.masksToBounds = YES;
    label.layer.borderColor = [UIColor clearColor].CGColor;
    label.layer.borderWidth = 0.0;
    label.backgroundColor = [UIColor iBeautyThemeColor];
}

+ (void)setCancelViewBorderRound:(UIView *)view {
    
    view.backgroundColor = [UIColor clearColor];
    view.layer.borderWidth = 0.5;
    view.layer.masksToBounds = YES;
    view.layer.cornerRadius = 6;
    view.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

+ (void)setViewBorderRound:(UIView *)view {
    
    view.backgroundColor = [UIColor clearColor];
    view.layer.borderWidth = 0.5;
    view.layer.masksToBounds = YES;
    
    if (view.tag == 0) {

        view.layer.cornerRadius = view.frame.size.width / 2;
        view.layer.borderColor = [UIColor darkGrayColor].CGColor;
        view.backgroundColor = [UIColor whiteColor];
    }
    
   else if (view.tag == 1) {
       view.layer.borderColor = [UIColor darkGrayColor].CGColor;
       view.layer.cornerRadius = 15;
        
    }else if (view.tag == 2) {
        view.layer.borderColor = [UIColor facebookThemeColor].CGColor;
        view.layer.cornerRadius = 15;

    }else if (view.tag == 3) {
        view.layer.borderColor = [UIColor googleThemeColor].CGColor;
        view.layer.cornerRadius = 15;
        
    }else if (view.tag == 4) {
        
        view.layer.borderColor = [UIColor clearColor].CGColor;
        view.layer.cornerRadius = 10;
        
    }else if (view.tag == 5) {
        
        view.layer.borderColor = [UIColor clearColor].CGColor;
        view.layer.cornerRadius = 10;
        view.backgroundColor = [UIColor whiteColor];
        
    }else if (view.tag == 6) {
        
        view.layer.borderColor = [UIColor clearColor].CGColor;
        view.layer.cornerRadius = view.frame.size.width / 2;
        view.backgroundColor = [UIColor darkGrayColor];
        
    }else if (view.tag == 7) {
        
        view.layer.cornerRadius = view.frame.size.width / 2;
        view.layer.borderColor = [UIColor iBeautyThemeColor].CGColor;
        
    }else if (view.tag == 15) {
        
        view.layer.cornerRadius = view.frame.size.width / 2;
        view.layer.borderColor = [UIColor clearColor].CGColor;
        view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    }
    
}

+ (void)setPickUpDeliveryViewWithBorderColor:(UIView *)view {
    
    view.layer.borderWidth = 0.5;
    view.layer.masksToBounds = YES;
    view.layer.borderColor = [UIColor iBeautyThemeColor].CGColor;
    view.layer.cornerRadius = 15;
}

+ (void)setPickUpDeliveryViewWithoutBorderColor:(UIView *)view {
    
    view.layer.borderWidth = 0.5;
    view.layer.masksToBounds = YES;
    view.layer.borderColor = [UIColor clearColor].CGColor;
    view.layer.cornerRadius = 15;
}

+ (void)setBookViewCornerRound:(UIView *)view {
    
    view.layer.borderWidth = 0.0;
    view.layer.masksToBounds = YES;
    view.layer.borderColor = [UIColor clearColor].CGColor;
    view.layer.cornerRadius = 12;
}

+ (void)selectSlotViewCornerRound:(UIView *)view selectStatus:(NSInteger)status {
    
    view.layer.borderWidth = 0.5;
    view.layer.masksToBounds = YES;
    
    if (status == 0) {
        view.layer.borderColor = [UIColor darkGrayColor].CGColor;
        view.backgroundColor = [UIColor clearColor];

    }else {
        view.layer.borderColor = [UIColor clearColor].CGColor;
        view.backgroundColor = [UIColor iBeautyThemeColor];
    }
    view.layer.cornerRadius = 15;
}

+ (void)setShadowToview:(UIView*)view {
    
    view.layer.cornerRadius = 2;
    view.layer.shadowOffset = CGSizeMake(2.0f, 0.0f);
    view.layer.shadowOpacity = 0.1f;
    
}

+ (void)setButtonCorderRound:(UIButton *)button {
    
    button.layer.borderWidth = 0.0;
    button.layer.masksToBounds = YES;
    button.layer.borderColor = [UIColor clearColor].CGColor;
    
    if (button.tag == 11) {
        
        button.layer.cornerRadius = 10.0;
        
    }else if (button.tag == 10) {
        
        button.layer.cornerRadius = 6.0;
        button.backgroundColor = [UIColor iBeautyThemeColor];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

    }else if (button.tag == 15) {
        
        button.layer.cornerRadius = 15.0;
        button.layer.borderWidth = 0.5;
        button.layer.borderColor = [UIColor lightGrayColor].CGColor;

    }
    else {
        button.layer.cornerRadius = 15.0;
    }

}

+ (void)setCancelAppointmentButtonCorderRound:(UIButton *)button {
    
    button.layer.cornerRadius = 15.0;
    button.layer.borderWidth = 0.5;
    button.layer.masksToBounds = YES;
    button.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

#pragma mark - Gradient Color

+ (CAGradientLayer*)setGradientColor_Buttons:(UIButton*)sender {
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = sender.bounds;
    gradient.startPoint = CGPointZero;
    gradient.endPoint = CGPointMake(1, 0);
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:233.0/255.0 green:83/255.0 blue:158/255.0 alpha:1.0] CGColor],(id)[[UIColor colorWithRed:240/255.0 green:59.0/255.0 blue:53.0/255.0 alpha:1.0] CGColor], nil];

    return gradient;
}

+ (CAGradientLayer*)addGradientColor_FilterButtons:(UIButton*)sender {
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = sender.bounds;
    gradient.startPoint = CGPointZero;
    gradient.endPoint = CGPointMake(1, 0);
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:233.0/255.0 green:83/255.0 blue:158/255.0 alpha:1.0] CGColor],(id)[[UIColor colorWithRed:240/255.0 green:59.0/255.0 blue:53.0/255.0 alpha:1.0] CGColor], nil];
    
    return gradient;
}

+ (CAGradientLayer*)removeGradientColor_FilterButtons:(UIButton*)sender {
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = sender.bounds;
    gradient.startPoint = CGPointZero;
    gradient.endPoint = CGPointMake(1, 0);
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:255.0/255.0 green:255/255.0 blue:255/255.0 alpha:1.0] CGColor],(id)[[UIColor colorWithRed:255/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0] CGColor], nil];
    
    return gradient;
}

#pragma mark - Gradient Color

+ (CAGradientLayer*)setGradientColor_Views:(UIView*)sender status:(BOOL)gradStatus {
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = sender.bounds;
    gradient.startPoint = CGPointMake(0, 0);
    gradient.endPoint = CGPointMake(1, 0);
    if (gradStatus == YES) {
        gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:233.0/255.0 green:83/255.0 blue:158/255.0 alpha:1.0] CGColor],(id)[[UIColor colorWithRed:240/255.0 green:59.0/255.0 blue:53.0/255.0 alpha:1.0] CGColor], nil];
    }else {
        gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:255.0/255.0 green:255/255.0 blue:255/255.0 alpha:1.0] CGColor],(id)[[UIColor colorWithRed:255/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0] CGColor], nil];
    }
    
    return gradient;
}

+ (CAGradientLayer*)setNavigationGradient:(UINavigationBar*)navBar {
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = navBar.bounds;
    gradient.startPoint = CGPointMake(0, 1);
    gradient.endPoint = CGPointMake(0, 1);
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:233.0/255.0 green:83/255.0 blue:158/255.0 alpha:1.0] CGColor],(id)[[UIColor colorWithRed:240/255.0 green:59.0/255.0 blue:53.0/255.0 alpha:1.0] CGColor], nil];
    
    return gradient;
}

+ (CGRect)setUpFrame:(CGRect)frame {
    
    CGRect tempFrame = frame;
    tempFrame.size.width = [UIScreen mainScreen].bounds.size.width;
    return tempFrame;
    
}

+ (CGRect)setUpButtonFrame:(CGRect)frame {
    
    CGRect tempFrame = frame;
    tempFrame.size.width = [UIScreen mainScreen].bounds.size.width - 30.0f;
    return tempFrame;
    
}

+ (CAGradientLayer*)setSegmentGradient:(UISegmentedControl*)segment {
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = segment.bounds;
    gradient.startPoint = CGPointMake(0, 1);
    gradient.endPoint = CGPointMake(0, 1);
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:233.0/255.0 green:83/255.0 blue:158/255.0 alpha:1.0] CGColor],(id)[[UIColor colorWithRed:240/255.0 green:59.0/255.0 blue:53.0/255.0 alpha:1.0] CGColor], nil];
    
    return gradient;
}

+ (CAGradientLayer*)removeSegmentGradient:(UISegmentedControl*)segment {
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = segment.bounds;
    gradient.startPoint = CGPointMake(0, 1);
    gradient.endPoint = CGPointMake(0, 1);
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:255.0/255.0 green:255/255.0 blue:255/255.0 alpha:1.0] CGColor],(id)[[UIColor colorWithRed:255/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0] CGColor], nil];

    return gradient;
}


+ (UIImage *)imageFromLayer:(CALayer *)layer {
    
    UIGraphicsBeginImageContext([layer frame].size);
    
    [layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return outputImage;
}


+ (void)setOrderScreenButtonCorderRound:(UIButton *)button {
    
    button.layer.masksToBounds = YES;
    button.layer.cornerRadius = 15.0;
    button.layer.borderWidth = 0.5;
    button.layer.borderColor = [UIColor lightGrayColor].CGColor;
}


+ (void)setBookNowButtonCorderRound:(UIButton *)button {

    button.layer.borderWidth = 0.0;
    button.layer.masksToBounds = YES;
    button.layer.borderColor = [UIColor clearColor].CGColor;
    button.layer.cornerRadius = 6.0;
    button.backgroundColor = [UIColor iBeautyThemeColor];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
}
+ (void)setfilterButtonProperties:(UIButton *)button selectStatus:(BOOL)status {
    
    button.layer.borderWidth = 0.5;
    button.layer.masksToBounds = YES;
    
    if (status == NO) {
        button.layer.borderColor = [UIColor darkGrayColor].CGColor;
        button.backgroundColor = [UIColor clearColor];
        button.titleLabel.textColor = [UIColor darkGrayColor];
        [button setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];

    }else {
        button.layer.borderColor = [UIColor clearColor].CGColor;
        button.backgroundColor = [UIColor iBeautyThemeColor];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    button.layer.cornerRadius = 15.0;
    
}


+ (void)setIncreseDecreaseButtonCordersRound:(UIButton *)button {
    
    button.layer.cornerRadius = button.frame.size.width / 2;
    button.layer.borderWidth = 1.0;
    button.layer.borderColor = [UIColor darkGrayColor].CGColor;
    button.layer.masksToBounds = YES;

}
+ (void)setLabelCorderRound:(UILabel *)label {
    
    label.layer.borderWidth = 0.0;
    label.layer.masksToBounds = YES;
    label.layer.borderColor = [UIColor clearColor].CGColor;
    
    if (label.tag == 12) {
        
        label.layer.cornerRadius = label.frame.size.width / 2;
        label.layer.borderWidth = 0.5;
        label.layer.borderColor = [UIColor lightGrayColor].CGColor;
    }
    
}

+ (void)setTextViewCorderRound:(UITextView *)textView {
    
    textView.layer.cornerRadius = 5.0;
    
    textView.layer.borderColor = [UIColor clearColor].CGColor;
    textView.layer.borderWidth = 0.0;
    textView.layer.masksToBounds = YES;
}


+ (void)showAlertMessage:(NSString *)message {
    
    UIWindow *window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    window.rootViewController = [UIViewController new];
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:nil
                                          message:message
                                          preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"Ok", @"OK action")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   NSLog(@"OK action");
                               }];
    
    [alertController addAction:okAction];
    [window makeKeyAndVisible];
    //[window.rootViewController presentViewController:alertController animated:YES completion:nil];
    [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:alertController animated:YES completion:nil];
}

+ (void)showAlertMessageForClearBasket:(NSString *)message {
    
    UIWindow *window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    window.rootViewController = [UIViewController new];
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:NSLocalizedString(@"Session expired", @"")
                                          message:message
                                          preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"Ok", @"OK action")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action) {
                                   BookAppointmentController *appointmentController = [[BookAppointmentController alloc]init];
                                   [appointmentController clearBasket_services:YES];
                                   AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
                                   delegate.tabbarController.selectedIndex = 0;
                               }];
    
    [alertController addAction:okAction];
    [window makeKeyAndVisible];
    [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:alertController animated:YES completion:nil];
}


+ (NSString *) getVersionNumber {
    
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
}

+ (void)zoomToUserLocationInMapView:(MKMapView *)mapView {
    CLLocation *location = mapView.userLocation.location;
    if (location) {
        CLLocationCoordinate2D coordinate = location.coordinate;
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(coordinate, 10000.0f, 10000.0f);
        [mapView setRegion:region animated:YES];
    }
}


@end
