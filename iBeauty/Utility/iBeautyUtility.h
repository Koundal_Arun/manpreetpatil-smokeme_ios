//
//  iBeautyUtility.h
//  iBeauty
//
//  Created by App Innovation on 27/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@import MapKit;

@interface iBeautyUtility : NSObject

+ (NSString *)trimString:(NSString *)string;
+ (BOOL) isValidEmailAddress:(NSString *)email;
+ (void)setimageViewRound:(UIImageView *)imageView;
+ (void)setImageViewCornerRound:(UIImageView *)imageView;
+ (void)setViewBorderRound:(UIView *)view;
+ (void)setCancelViewBorderRound:(UIView *)view;
+ (void)setButtonCorderRound:(UIButton *)button;
+ (void)setCancelAppointmentButtonCorderRound:(UIButton *)button;
+ (void)setIncreseDecreaseButtonCordersRound:(UIButton *)button;
+ (void)setTextViewCorderRound:(UITextView *)textView;
+ (void)showAlertMessage:(NSString *)message;
+ (NSString *) getVersionNumber;
+ (void)setLabelCorderRound:(UILabel *)label;
+ (void)selectSlotViewCornerRound:(UIView *)view selectStatus:(NSInteger)status;
+ (void)setLabelRound:(UILabel *)label;
+ (void)setfilterButtonProperties:(UIButton *)button selectStatus:(BOOL)status;
+ (void)setBookNowButtonCorderRound:(UIButton *)button;
+ (void)setPickUpDeliveryViewWithBorderColor:(UIView *)view;
+ (void)setPickUpDeliveryViewWithoutBorderColor:(UIView *)view;
+ (void)setOrderScreenButtonCorderRound:(UIButton *)button;
+ (void)zoomToUserLocationInMapView:(MKMapView *)mapView;
+ (void)showAlertMessageForClearBasket:(NSString *)message;
+ (void)setBookViewCornerRound:(UIView *)view;

+ (CAGradientLayer*)setGradientColor_Buttons:(UIButton*)sender;
+ (CAGradientLayer*)setGradientColor_Views:(UIView*)sender status:(BOOL)gradStatus;
+ (UIImage *)imageFromLayer:(CALayer *)layer;
+ (CAGradientLayer*)setNavigationGradient:(UINavigationBar*)navBar;
+ (CAGradientLayer*)removeGradientColor_FilterButtons:(UIButton*)sender;
+ (CAGradientLayer*)addGradientColor_FilterButtons:(UIButton*)sender;
+ (CAGradientLayer*)removeSegmentGradient:(UISegmentedControl*)segment;
+ (CAGradientLayer*)setSegmentGradient:(UISegmentedControl*)segment;
+ (CGRect)setUpFrame:(CGRect)frame;
+ (CGRect)setUpButtonFrame:(CGRect)frame;

@end
