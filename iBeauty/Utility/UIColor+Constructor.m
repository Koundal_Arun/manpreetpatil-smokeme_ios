//  Created by Priyaka chhetri on 12/12/2013.
//  Copyright (c) 2013 Orange Labs. All rights reserved.

#import "UIColor+Constructor.h"

@implementation UIColor (Constructor)
+ (UIColor *)colorWithRGBHex:(UInt32)hex
{
    return colorWithRGBHex(hex);
}

@end
