

#import <UIKit/UIKit.h>
NS_INLINE UIColor* colorWithRGBHex(NSUInteger hex)
{
    return [UIColor colorWithRed:(((hex >> 16) & 0xFF) / 255.0f)
                           green:(((hex >> 8) & 0xFF) / 255.0f)
                            blue:((hex & 0xFF) / 255.0f)
                           alpha:1.0f];
}
@interface UIColor (Constructor)

@end
