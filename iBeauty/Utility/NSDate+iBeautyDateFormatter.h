//
//  NSDate+iBeautyDateFormatter.h
//  iBeauty
//
//  Created by App Innovation on 02/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import "StandardOpeningHours.h"

@interface NSDate (iBeautyDateFormatter)

- (NSString *)convertTimeStringToFormattedNSString:(NSString *)timeString;

- (NSString *)getOpeningStringBasedOnStartAndEndTime:(NSDictionary*)infoObj format:(NSString*)timeFormat ;

- (NSString *)getOpeningStringBasedOnStartAndEndTimeForAllDaysOpeningTime:(StandardOpeningHours*)infoObj format:(NSString*)timeFormat ;

+ (NSDate *)convertNSStringToNSDate:(NSString *)dateInString format: (NSString *)format;

- (NSString *)convertDateValueToNSString:(NSString *)format localeStatus:(BOOL)status;

- (NSString *)convertDateToNSString:(NSString *)format date:(NSDate*)date;

- (NSDate *)withFormat:(NSString *)format;

@end
