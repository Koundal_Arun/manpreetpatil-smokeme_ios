//
//  UIColor+iBeauty.m
//  iBeauty
//
//  Created by App Innovation on 27/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "UIColor+iBeauty.h"
#import "UIColor+Constructor.h"
#import "Constants.h"

@implementation UIColor (iBeauty)

+ (UIColor *)iBeautyThemeColor {
    
    return colorWithRGBHex(0xFF825A);

}
+ (UIColor *)facebookThemeColor {
    
    return colorWithRGBHex(0x3A5999);

}
+ (UIColor *)googleThemeColor {
    
    return colorWithRGBHex(0xFF3F3D);

}
+ (UIColor *)settingHeaderColor {
    
    return colorWithRGBHex(0xEFEFF4);
    
}
+ (UIColor*)scrollViewTexturedBkgColor {
    
    return colorWithRGBHex(0x6F7179);

}
+ (UIColor *)cancelAppointmentThemeColor {
    
    return colorWithRGBHex(0xF2F2F2);
    
}


+ (UIColor*)getOrderStatusColor:(NSInteger)status {
    
    UIColor *statusColor ;
    
    if (status == Placed) {
        statusColor =  colorWithRGBHex(0xF453A8);
        
    }else if (status == Shipped) {
        statusColor =  colorWithRGBHex(0xF453A8);

    }else if (status == Delivered) {
        statusColor =  colorWithRGBHex(0x00BE72);

    }else  {
        statusColor =  [UIColor redColor];
    }
    
    return statusColor;
}

@end
