//
//  NSString+iBeautyString.m
//  iBeauty
//
//  Created by App Innovation on 02/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "NSString+iBeautyString.h"
#import "Constants.h"
#import "NSDate+iBeautyDateFormatter.h"

@implementation NSString (iBeautyString)

- (NSMutableAttributedString*)getAttributedTextWithTimeString:(NSString*)timeString openingStatus:(NSString*)openingStr {
    
    NSString *timeWithOpengingString = [NSString stringWithFormat:@"%@ %@",timeString,openingStr];
    
    // Define general attributes for the entire text
    NSDictionary *attribs = @{
                              NSForegroundColorAttributeName:[UIColor blackColor],
                              NSFontAttributeName: [UIFont fontWithName:@"SegoeUI" size:13.0]
                              };
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:timeWithOpengingString attributes:attribs];
    
    UIFont *boldFont = [UIFont fontWithName:@"SegoeUI" size:13.0];
    NSRange range = [timeWithOpengingString rangeOfString:timeString];
    [attributedText setAttributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor],NSFontAttributeName:boldFont} range:range];
    
    return attributedText;
}

- (NSMutableAttributedString*)getAppointmentBookingAttributedTextWithId:(NSString*)titleString openingStatus:(NSString*)IdStr {
    
    NSString *timeWithOpengingString = [NSString stringWithFormat:@"%@ %@",titleString,IdStr];
    
    // Define general attributes for the entire text
    NSDictionary *attribs = @{ NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName: [UIFont fontWithName:@"SegoeUI" size:14.0]
                              };
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:timeWithOpengingString attributes:attribs];
    
    UIFont *boldFont = [UIFont fontWithName:@"SegoeUI" size:14.0];
    NSRange range = [timeWithOpengingString rangeOfString:IdStr];
    [attributedText setAttributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor],NSFontAttributeName:boldFont} range:range];
    
    return attributedText;
}

- (NSMutableAttributedString*)getOrdersAttributedTextWithId:(NSString*)titleString openingStatus:(NSString*)IdStr screenType:(NSInteger)type {
    
    NSString *timeWithOpengingString = [NSString stringWithFormat:@"%@ %@",titleString,IdStr];
    
    NSDictionary *attribs;
    UIFont *boldFont ;
    if (type == 1) {
        boldFont = [UIFont fontWithName:@"SegoeUI" size:16.0];
        attribs = @{ NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName: [UIFont systemFontOfSize:16 weight:UIFontWeightSemibold] };
    }else if (type == 2) {
        boldFont = [UIFont fontWithName:@"SegoeUI" size:17.0];
        attribs = @{ NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName: [UIFont fontWithName:@"SegoeUI-Semibold" size:17.0] };
    }else {
        boldFont = [UIFont fontWithName:@"SegoeUI" size:15.0];
        attribs = @{ NSForegroundColorAttributeName:[UIColor scrollViewTexturedBkgColor],NSFontAttributeName: [UIFont fontWithName:@"SegoeUI" size:15.0] };
    }
    
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:timeWithOpengingString attributes:attribs];
    
    NSRange range = [timeWithOpengingString rangeOfString:IdStr];
    [attributedText setAttributes:@{NSForegroundColorAttributeName: [UIColor blackColor],NSFontAttributeName:boldFont} range:range];
    
    return attributedText;
}

- (NSString*)getOpengingHourString:(NSDictionary*)openingHourDict {
    
    NSString *startTimeStr = [[NSDate date] convertTimeStringToFormattedNSString:[openingHourDict valueForKey:@"Fromtime"]];
    NSString *endTimeStr = [[NSDate date] convertTimeStringToFormattedNSString:[openingHourDict valueForKey:@"Totime"]];
    NSString *finalString = [NSString stringWithFormat:@"%@-%@",startTimeStr,endTimeStr];
    return finalString;
}

- (NSString*)getOpengingHourStringFromModel:(StandardOpeningHours*)openingHour {
    
    NSString *startTimeStr = [[NSDate date] convertTimeStringToFormattedNSString:openingHour.fromTime];
    NSString *endTimeStr = [[NSDate date] convertTimeStringToFormattedNSString:openingHour.toTime];
    NSString *finalString = [NSString stringWithFormat:@"%@-%@",startTimeStr,endTimeStr];
    return finalString;
}

- (NSString*)getWeekDay {
    
    NSCalendar* calender = [NSCalendar currentCalendar];
    NSDateComponents* component = [calender components:NSCalendarUnitWeekday fromDate:[NSDate date]];
    NSInteger weekday = [component weekday]; // 1 for Sunday, 2 for Monday, and so on...
    NSString *currentDay;
    if (weekday == 1) {
        
        currentDay = @"Sunday";
        
    }else if (weekday == 2) {
        
        currentDay = @"Monday";
        
    }else if (weekday == 3) {
        currentDay = @"Tuesday";

    }else if (weekday == 4) {
        currentDay = @"Wednesday";

    }else if (weekday == 5) {
        currentDay = @"Thursday";

    }else if (weekday == 6) {
        
        currentDay = @"Friday";
        
    }else if (weekday == 7) {
        
        currentDay = @"Saturday";
    }
    
    return  currentDay;
}

- (NSInteger)weekDayNumber {
    
    NSCalendar* calender = [NSCalendar currentCalendar];
    NSDateComponents* component = [calender components:NSCalendarUnitWeekday fromDate:[NSDate date]];
    NSInteger weekday = [component weekday]; // 1 for Sunday, 2 for Monday, and so on...
    return  weekday;
}

+ (NSString *)parseCompleteAddress:(AddressModel* )addressInfo {
    
    NSString *formatString = @"";
    NSString *mobileNumber = @"";
    NSString *email = @"";
    
    if (addressInfo.address.length > 0) {
        formatString = [NSString stringWithFormat:@"%@",addressInfo.address];
    }
    
    if (addressInfo.email.length > 0) {
        
        email = [NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"Email", @""),addressInfo.email];
        
        if (formatString.length > 0) {
            formatString = [NSString stringWithFormat:@"%@, %@",formatString,email];
        }else {
            formatString = email;
        }
    }

    if (addressInfo.contactNumber.length > 0) {

        mobileNumber = [NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"Phone number", @""),addressInfo.contactNumber];
        
        if (formatString.length > 0) {
            formatString = [NSString stringWithFormat:@"%@, %@",formatString,mobileNumber];
        }else {
            formatString = mobileNumber;
        }
    }
    
    if (APP_DEBUG_MODE) { NSLog(@"full address is %@", formatString); }
    
    return formatString;
    
}


/*
+ (NSString *)parseCompleteAddress:(AddressModel* )addressInfo {
    
    NSString *formatString = @"";
    NSString *mobileNumber = @"";
    NSString *pinCode = @"";
    
    if (addressInfo.flatNo.length > 0) {
        formatString = [NSString stringWithFormat:@"%@",addressInfo.flatNo];
    }
    if (addressInfo.locality.length > 0) {
        if (formatString.length > 0) {
            formatString = [NSString stringWithFormat:@"%@, %@",formatString,addressInfo.locality];
        }else {
            formatString = addressInfo.locality;
        }
    }
    if (addressInfo.city.length > 0) {
        
        if (formatString.length > 0) {
            formatString = [NSString stringWithFormat:@"%@, %@",formatString,addressInfo.city];
        }else {
            formatString = addressInfo.city;
        }
    }
    if (addressInfo.state.length > 0) {
        if (formatString.length > 0) {
            formatString = [NSString stringWithFormat:@"%@, %@",formatString,addressInfo.state];
        }else {
            formatString = addressInfo.state;
        }
    }
    if (addressInfo.country.length > 0) {
        
        if (formatString.length > 0) {
            formatString = [NSString stringWithFormat:@"%@, %@",formatString,addressInfo.country];
        }else {
            formatString = addressInfo.country;
        }
    }

    if (addressInfo.pinCode.length > 0) {
        pinCode = [NSString stringWithFormat:@"Post code: %@",addressInfo.pinCode];
        
        if (formatString.length > 0) {
            formatString = [NSString stringWithFormat:@"%@, %@",formatString,pinCode];
        }else {
            formatString = pinCode;
        }
    }
    if (addressInfo.mobileNumber.length > 0) {
        
        mobileNumber = [NSString stringWithFormat:@"Phone number: %@",addressInfo.mobileNumber];
        
        if (formatString.length > 0) {
            formatString = [NSString stringWithFormat:@"%@, %@",formatString,mobileNumber];
        }else {
            formatString = mobileNumber;
        }
    }
    
    if (APP_DEBUG_MODE) { NSLog(@"full address is %@", formatString); }
    
    return formatString;
}
 
 */

+ (NSString*)getOrderStatus:(NSInteger)status {
    
    NSString *statusString = @"";
    
    if (status == Placed) {
        statusString =  NSLocalizedString(@"Order Placed", @"");
        
    }else if (status == Shipped) {
        statusString =  NSLocalizedString(@"Order Shipped", @"");

    }else if (status == Delivered) {
        statusString =  NSLocalizedString(@"Order Delivered", @"");

    }else  {
        statusString =  NSLocalizedString(@"Order Cancelled", @"");
    }
    
    return statusString;
}


@end
