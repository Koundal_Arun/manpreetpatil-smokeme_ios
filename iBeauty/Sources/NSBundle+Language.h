//
//  NSBundle+Language.h
//  iBeauty
//
//  Created by App Innovation on 15/10/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifdef USE_ON_FLY_LOCALIZATION

@interface NSBundle (Language)

+ (void)setLanguage:(NSString *)language;

@end

#endif
