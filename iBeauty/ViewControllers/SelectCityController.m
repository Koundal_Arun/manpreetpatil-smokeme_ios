
//
//  SelectCityController.m
//  iBeauty
//
//  Created by App Innovation on 18/12/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "SelectCityController.h"
#import "CityInfoCell.h"
#import "Constants.h"
#import "CityInfoModel.h"


static NSString *cityCellIdentifier = @"CityInfoCell";

@interface SelectCityController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,weak) IBOutlet UITableView *cityTableView;
@property (nonatomic,strong) NSMutableArray *cityInfoArray;
@property (nonatomic,weak) IBOutlet UIBarButtonItem *cancelButton;
@property (nonatomic,weak) IBOutlet UINavigationItem *navigationItem;
@property (nonatomic,weak)  IBOutlet UINavigationBar *navBar;

@end

@implementation SelectCityController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.cityInfoArray = [NSMutableArray new];
    self.cityTableView.hidden = YES;
    [self getCityInfo];
    [self setUpViewForLocalization];
    //[self setGradient];
}

#pragma mark - Set Gradient

- (void)setGradient {
    
    self.navBar.frame = [iBeautyUtility setUpFrame:self.navBar.frame];
    CAGradientLayer *gradient = [iBeautyUtility setNavigationGradient:self.navBar];
    [self.navBar setBackgroundImage:[iBeautyUtility imageFromLayer:gradient] forBarMetrics:UIBarMetricsDefault];
}

#pragma mark - Set Up location label

- (void)setUpViewForLocalization {
    
    self.navigationItem.title = NSLocalizedString(@"Select City", @"");
    [self.cancelButton setTitle: NSLocalizedString(@"Cancel", @"")];

}

- (IBAction)backAction:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)getCityInfo {
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_BUSINESS_CITIES];
    
    //NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    
    NSString *urlString = [NSString stringWithFormat:@"%@",url];
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable) {
        
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonGETData:urlString parameters:nil onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                
                @try {
                    if (completed) {
                        if (APP_DEBUG_MODE) { NSLog(@"Value is %@", completed);}
                        CityInfoModel *infoModel  = [CityInfoModel getSharedInstance];
                        NSMutableArray *tempCityArray = [infoModel getCityInfoArray:completed];
                        [self filterOutCitiesInfo:tempCityArray];
                        self.cityTableView.hidden = NO;
                        [self.cityTableView reloadData];
                    }else {
                        if (APP_DEBUG_MODE)
                            NSLog(@"error is %@", error.localizedDescription);
                        [SVProgressHUD dismiss];
                    }
                    [self.cityTableView reloadData];
                    
                } @catch (NSException *exception) {
                    //[EShopUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                    [SVProgressHUD dismiss];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
    
}

- (void)filterOutCitiesInfo:(NSMutableArray*)citiesArray {
    
    if (citiesArray.count > 0) {
        
        [self.cityInfoArray insertObject:NSLocalizedString(@"Current Location", @"") atIndex:0];
        
        NSString *languageCode = [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];

        for (CityInfoModel *cityInfo in citiesArray) {
            
            if ([languageCode isEqualToString:@"en"]) {
                
                [self.cityInfoArray addObject:cityInfo.cityName_en];

            }else if ([languageCode isEqualToString:@"ar"]) {
                
                [self.cityInfoArray addObject:cityInfo.cityName_ar];
            }
        }
    }

}

#pragma mark - UITableViewDataSouce

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.cityInfoArray.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    CityInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:cityCellIdentifier];
    
    if (!cell){
        cell = [[CityInfoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cityCellIdentifier];
    }
    [cell configureCellWithCityInfo:self.cityInfoArray[indexPath.row]];
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 44.0f;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *cityInfo = [NSDictionary dictionaryWithObjectsAndKeys:self.cityInfoArray[indexPath.row],@"cityInfo", nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"getCityInfoObject" object:nil userInfo:cityInfo];
    [self dismissViewControllerAnimated:YES completion:nil];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
