//
//  SettingsViewController.m
//  iBeauty
//
//  Created by App Innovation on 27/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "SettingsViewController.h"
#import "Constants.h"
#import "SettingsTableCell.h"
#import "TSLanguageManager.h"

#import "LanguageManager.h"

static NSString *settingCellIdentifier = @"SettingsCell";

@interface SettingsViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) NSMutableArray *settingsOptionList;
@property (nonatomic,weak) IBOutlet UITableView *settingTableView;
@property (nonatomic,strong) NSString *languageString;
@property (nonatomic,strong) AppDelegate *delegate;
@property (nonatomic,weak)  IBOutlet UINavigationBar *navBar;

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.delegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];

    [self getSettingOptions];
    [self setUpTableView];
    [self getLanguageAccordingToLanguageCode];
    //[self setGradient];

}

#pragma mark - View Will Appear

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    self.tabBarController.tabBar.hidden = YES;
}

#pragma mark - Set Gradient

- (void)setGradient {
    
    self.navBar.frame = [iBeautyUtility setUpFrame:self.navBar.frame];
    CAGradientLayer *gradient = [iBeautyUtility setNavigationGradient:self.navBar];
    [self.navBar setBackgroundImage:[iBeautyUtility imageFromLayer:gradient] forBarMetrics:UIBarMetricsDefault];
}

- (void)getLanguageAccordingToLanguageCode {
    
  NSString *languageCode =   [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];

    if ([languageCode isEqualToString:@"en"]) {
        
        self.languageString = @"English";
        
    }else if ([languageCode isEqualToString:@"ar"]) {
        
        self.languageString = @"Arabic";

    }
}

#pragma mark - Get Settings Options

- (void)getSettingOptions {
    
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"Settings" ofType:@"plist"];
    self.settingsOptionList = [NSMutableArray arrayWithContentsOfFile:plistPath];
    if (APP_DEBUG_MODE)
        NSLog(@"Setting list is %@",self.settingsOptionList);
}

#pragma mark - Set up table view

- (void)setUpTableView {
    
    UINib *cellNib = [UINib nibWithNibName:@"SettingsTableCell" bundle:nil];
    [self.settingTableView registerNib:cellNib forCellReuseIdentifier:settingCellIdentifier];
    
}

#pragma mark -UITableViewDataSouce

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return self.settingsOptionList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSArray *arrayList = self.settingsOptionList[section];
    
    if (section == 0) {
        
        return arrayList.count;
        
    }else {
        
        return arrayList.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SettingsTableCell *cell = [tableView dequeueReusableCellWithIdentifier:settingCellIdentifier];
    
    NSArray *arrayList = self.settingsOptionList[indexPath.section];

    [cell configureSettingCell:arrayList[indexPath.row] indexPath:indexPath language:self.languageString];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 62.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 50.f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.settingTableView deselectRowAtIndexPath:indexPath animated:NO];

    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            [self actionSheetForChooseLanguage:tableView];
        }
        
    }else {
        if (indexPath.row == 3) {
            [self showAlertDailogForLogOut];
        }
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 60.0f)];
    UILabel *tempLabel= [[UILabel alloc]initWithFrame:CGRectMake(15,15,tableView.frame.size.width-30,30)];
    tempLabel.textColor = [UIColor blackColor];
    tempLabel.font = [UIFont fontWithName:@"SegoeUI" size:18.0];
    
    if (section == 0) {
        tempLabel.text = NSLocalizedString(@"General",@"");
    }else{
        tempLabel.text = NSLocalizedString(@"About",@"");
    }
    headerView.backgroundColor = [UIColor settingHeaderColor];

    [headerView addSubview:tempLabel];
    return headerView;
}

- (void)showAlertDailogForLogOut {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:NSLocalizedString(@"Are you sure? You want to Logout",@"")
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"NO",@"")
                                                           style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                               [self dismissViewControllerAnimated:YES completion:^{
                                                               }];
                                                           }];
    UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"YES",@"")
                                                           style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                               [self logOutSettings];
                                                           }];
    
    [alert addAction:cancelAction];
    [alert addAction:deleteAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Log Out Method

- (void)logOutSettings {

    NSString *loginType = [[NSUserDefaults standardUserDefaults]valueForKey:KLoginType];
    
    if ([loginType isEqualToString:@"facebook"]) {
        [self.delegate.loginManager logOut];
    }else if ([loginType isEqualToString:@"google"]){
        [[GIDSignIn sharedInstance] signOut];
    }
    
    [[NSUserDefaults standardUserDefaults]setValue:@"" forKey:KLoginStatus];
    [[NSUserDefaults standardUserDefaults]setObject:nil forKey:KLoginInfo];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    LoginViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:kLoginController];
    [self.delegate mainViewSwitch:loginVC];

}

#pragma mark - Action method for Drop Down

- (void)actionSheetForChooseLanguage:(UITableView*)tableView {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Select Language",@"") message:nil
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *englishAction = [UIAlertAction actionWithTitle:@"English"
                                                        style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                            self.languageString = @"English";
                                                            [self.settingTableView reloadData];
                                                            [[NSUserDefaults standardUserDefaults] setValue:@"en" forKey:kLMSelectedLanguageKey];
                                                            [LanguageManager saveLanguageByIndex:0];
                                                            [self goToHomeScreen];
                                                        }];
    UIAlertAction *saudiArabiaAction = [UIAlertAction actionWithTitle:@"العربية"
                                                     style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                         self.languageString = @"Arabic";
                                                         [self.settingTableView reloadData];
                                                         [[NSUserDefaults standardUserDefaults] setValue:@"ar" forKey:kLMSelectedLanguageKey];
                                                         [LanguageManager saveLanguageByIndex:1];
                                                         [self goToHomeScreen];
                                                     }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel",@"")
                                                           style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                               [self dismissViewControllerAnimated:YES completion:^{
                                                               }];
                                                           }];
    [alert addAction:englishAction];
    [alert addAction:saudiArabiaAction];
    [alert addAction:cancelAction];
    
    NSString *deviceType = [UIDevice currentDevice].model;
    
    if (![deviceType isEqualToString:@"iPhone"]) {
 
        [alert setModalPresentationStyle:UIModalPresentationPopover];
        UIPopoverPresentationController *popPresenter = [alert popoverPresentationController];
        popPresenter.sourceView = tableView;
        popPresenter.sourceRect = CGRectMake(self.view.frame.size.width-(self.settingTableView.frame.size.width-300.0f-35.0f), -200.0f, self.settingTableView.frame.size.width-200.0f, 300.0f);
    }
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)goToHomeScreen {
    
    HomesViewController *homeController = [self.storyboard instantiateViewControllerWithIdentifier:kHomesViewController];
    [self.delegate mainViewSwitch:homeController];
    
}

- (void)reloadRootViewController {
    
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    NSString *storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    delegate.window.rootViewController = [storyboard instantiateInitialViewController];
}

#pragma mark - Back Action Method

- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Show menu action method

- (IBAction)showMenu:(id)sender {
    
    NSString *languageCode =   [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];
    
    if ([languageCode isEqualToString:@"en"]) {
        
        [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
        
    }else if ([languageCode isEqualToString:@"ar"]) {
        
        [self.mm_drawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:nil];
    }
}
*/

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
