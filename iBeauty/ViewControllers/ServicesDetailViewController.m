//
//  ServicesDetailViewController.m
//  iBeauty
//
//  Created by App Innovation on 02/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "ServicesDetailViewController.h"
#import "ServicesDetailCollCell.h"
#import "ServicesDetail.h"
#import "UILabel+FlexibleWidHeight.h"
#import "BasketInfoObj.h"

static NSString *servicesDetailCellIdentifier  = @"servicesDetailCollCell";

@interface ServicesDetailViewController ()

@property (nonatomic,weak) IBOutlet UISegmentedControl *segmentControl;
@property (nonatomic,assign) NSInteger segmentIndex;
@property (nonatomic,strong) NSMutableArray *servicesDetailArray;
@property (nonatomic,weak) IBOutlet UICollectionView *collectionView;

@property (nonatomic,weak) IBOutlet UIButton *bookNow;
@property (nonatomic,weak) IBOutlet UILabel  *appointmentsNumberLabel;
@property (nonatomic,strong) NSMutableDictionary *servicesdictInfo;
@property (nonatomic,strong) NSMutableArray *servicesSectionArray;
@property (nonatomic,strong) NSMutableArray *allServicesArray;
@property (nonatomic,strong) NSMutableArray *selectedServicesArray;
@property (nonatomic,assign) NSInteger totalAppointments;

@property (nonatomic,strong) NSMutableArray *servicesInfoArray;

@property (nonatomic,weak) IBOutlet UIScrollView *scrollViewCategories;

@property (nonatomic,assign) BOOL decrementStatus;

@property (nonatomic,weak) IBOutlet UILabel  *yourAppointmentLabel;

@property (nonatomic,strong) NSString *basketID;
@property (nonatomic,strong) NSMutableArray *basketInfoArray;
@property (nonatomic,strong) AppDelegate   *delegate;
@property (nonatomic,weak)  IBOutlet UINavigationBar *navBar;

@end

@implementation ServicesDetailViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.decrementStatus = NO;
    self.segmentIndex = 0;
    self.selectedServicesArray = [NSMutableArray new];
    
    [self setUpCollectionView];
    [self getServicesFromParentServiceCategories];
    [self loadNotificationObserverForAppointmentsInfo];
    
    self.yourAppointmentLabel.text = NSLocalizedString(@"Your Appointments", @"");
    self.delegate = [AppDelegate sharedInstance];
    
    //[self setGradient];
    [self setUpSegmentControlTint];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    [self setScrollViewRect];
    [self checkForBasketExistance];

}

#pragma mark - Set Gradient

- (void)setGradient {
    
    self.navBar.frame = [iBeautyUtility setUpFrame:self.navBar.frame];
    CAGradientLayer *gradient = [iBeautyUtility setNavigationGradient:self.navBar];
    [self.navBar setBackgroundImage:[iBeautyUtility imageFromLayer:gradient] forBarMetrics:UIBarMetricsDefault];
}

#pragma mark - Set Segment Control Tint

- (void)setUpSegmentControlTint {

    if (@available(iOS 13.0, *)) {

        [self.segmentControl setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor], NSFontAttributeName:  [UIFont fontWithName:@"SegoeUI" size:15.0]} forState:UIControlStateSelected];
        [self.segmentControl setSelectedSegmentTintColor:[UIColor iBeautyThemeColor]];
        
    } else {

        [self.segmentControl setTintColor:[UIColor iBeautyThemeColor]];
    }
    
}

#pragma mark - Set Scroll View Rect

- (void)setScrollViewRect {
    
    self.scrollViewCategories.translatesAutoresizingMaskIntoConstraints = YES;
    
    NSString *deviceType = [UIDevice currentDevice].model;
    if ([deviceType isEqualToString:@"iPhone"]) {
        
        if (IS_IPHONE_4 || IS_IPHONE_5 || IS_IPHONE_6 || IS_IPHONE_6_PLUS) {
            self.scrollViewCategories.frame = CGRectMake(0.0f, 64.0f,SCREEN_WIDTH, self.scrollViewCategories.frame.size.height);
        }else {
            self.scrollViewCategories.frame = CGRectMake(0.0f, 88.0f,SCREEN_WIDTH, self.scrollViewCategories.frame.size.height);
        }
    }else {
        self.scrollViewCategories.frame = CGRectMake(0.0f, 70.0f,SCREEN_WIDTH, self.scrollViewCategories.frame.size.height);
    }

}

#pragma mark -  Check For Basket Existance

- (void)checkForBasketExistance {
    
    self.basketID = [[NSUserDefaults standardUserDefaults]valueForKey:@"BasketId"];
    
    if (self.basketID.length > 0) {
        [self getExistingBasket];
    }
}

#pragma mark -  Get Basket For Services


- (void)getExistingBasket {
        
    NSString *businessID = self.categoryInfoObj.Id;
    NSString *customerID = [[NSUserDefaults standardUserDefaults]valueForKey:KUserId];
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_GET_BASKET];
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    
    NSDictionary *dictInfo = [NSDictionary dictionaryWithObjectsAndKeys:accessToken,@"Authorization",businessID,@"BusinessId",customerID,@"CustomerId",self.basketID,@"BasketId",nil];
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable) {
        
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonPostData:url params:dictInfo onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                
                @try {
                    if (completed) {
                        
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSDictionary *tempInfoDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE)
                            NSLog(@"Temp Info Dict is %@", tempInfoDict);
                        
                        BasketInfoObj *infoObj = [BasketInfoObj getSharedInstance];
                        self.basketInfoArray = [infoObj basketDetailInfoObject:tempInfoDict];
                        [self getExistingServicesInfo];
                        
                        if (APP_DEBUG_MODE) { NSLog(@"Response Array is %@", self.basketInfoArray); }
                        
                    }else {
                        if (APP_DEBUG_MODE)
                            NSLog(@"error is %@", error.localizedDescription);
                    }
                    
                } @catch (NSException *exception) {
                    [iBeautyUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
    
}

#pragma mark - Get Existing services

- (void)getExistingServicesInfo {
    
    BasketInfoObj *infoObj;
    if (self.basketInfoArray.count > 0) {
        self.decrementStatus = YES;
        infoObj = self.basketInfoArray[0];
        if ([infoObj.businessId isEqualToString:self.categoryInfoObj.Id]) {
            
            [self removeTotalAppointments];

            for (BasketInfoObj*basketObj in self.basketInfoArray) {
                
                NSString *serviceId = basketObj.serviceId;
                
                if (APP_DEBUG_MODE) { NSLog(@"Basket service Id  is %@",serviceId); }
                
                for (ServicesDetail *infoDetail in self.allServicesArray) {
                    
                    if (APP_DEBUG_MODE) { NSLog(@"Service Detail Id is %@",infoDetail.serviceId); }

                    if ([infoDetail.serviceId isEqualToString:serviceId]) {
                        NSInteger increaseIndex = infoDetail.increaseIndex;
                        infoDetail.increaseIndex = increaseIndex + 1;
                    }
                }
            }
            
        }else {
            [self removeTotalAppointments];
            self.decrementStatus = NO;
            [self.basketInfoArray removeAllObjects];
        }
        
        [self addTotalAppointments];
    }
}

#pragma mark -  Create Basket For Services

- (void)createBasketForServices {
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_CREATE_BASKET];
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSDictionary *dictInfo = [self getCreateBasketParametersInfo];
    
    if (internetStatus != NotReachable) {
        
//        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonPostData:url params:dictInfo onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
//                [SVProgressHUD dismiss];
                
                @try {
                    if (completed) {
                        
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSDictionary *tempInfoDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        BasketInfoObj *infoObj = [BasketInfoObj getSharedInstance];
                        self.basketInfoArray = [infoObj basketDetailInfoObject:tempInfoDict];
                        if(self.basketInfoArray.count>0) {
                            infoObj = self.basketInfoArray[0];
                            if (APP_DEBUG_MODE) { NSLog(@"Basket Id is %@", infoObj.basketId); }
                            [[NSUserDefaults standardUserDefaults]setValue:infoObj.basketId forKey:@"BasketId"];
                            [[NSUserDefaults standardUserDefaults]synchronize];
                            [self.delegate clearBasket];
                        }
                        if (APP_DEBUG_MODE)
                            NSLog(@"Response Array is %@", self.basketInfoArray);
                    }else {
                        if (APP_DEBUG_MODE)
                            NSLog(@"error is %@", error.localizedDescription);
                    }
                    
                } @catch (NSException *exception) {
                    // [iBeautyUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}

- (NSMutableDictionary*)getCreateBasketParametersInfo {
    
    NSMutableDictionary *basketDict = [[NSMutableDictionary alloc]init];
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    
    NSString *businessID = self.categoryInfoObj.Id;
    [basketDict setValue:businessID forKey:@"BusinessId"];
    
    NSString *customerID = [[NSUserDefaults standardUserDefaults]valueForKey:KUserId];
    [basketDict setValue:customerID forKey:@"CustomerId"];
    
    NSMutableArray *appointmentsArray = [NSMutableArray new];
    
    for (ServicesDetail *detailInfo in self.selectedServicesArray) {
        
        NSMutableDictionary *appointmentDict = [[NSMutableDictionary alloc]init];
        [appointmentDict setValue:detailInfo.serviceId forKey:@"ServiceId"];
        [appointmentDict setValue:[NSNumber numberWithInteger:0] forKey:@"StaffId"];
        [appointmentsArray addObject:appointmentDict];
    }
    [basketDict setObject:appointmentsArray forKey:@"Appointments"];
    [basketDict setObject:@"" forKey:@"Products"];
    
    NSString *offerId = @"";
    [basketDict setValue:offerId forKey:@"OfferId"];
    [basketDict setValue:accessToken forKey:@"Authorization"];
    
    return basketDict;
    
}


#pragma mark - Init Categories

- (void) initCategories {
    
    for (UIView *subview in self.scrollViewCategories.subviews) {
        [subview removeFromSuperview];
    }
    
    float gap = 20;
    float posX = 10;
    
    for (int index = 0 ; index < self.servicesSectionArray.count ; index ++) {
        
        NSString *key = [NSString stringWithFormat:@"%@",self.servicesSectionArray[index]];
        NSMutableArray *tempArray = [self.servicesdictInfo valueForKey:key];
        ServicesDetail *serviceObj = tempArray[0];
        NSString *category = serviceObj.categoryName_service;
        
        float width = [UILabel widthOfTextForString:category andFont:[UIFont fontWithName:@"SegoeUI" size:15.0] maxSize:(CGSize){MAXFLOAT,31 }];
        
        CGRect frame = CGRectMake(posX, 8, width+18, self.scrollViewCategories.frame.size.height-16);
        [self addTabWithTitle:category frame:frame index:index];
        
        posX = posX + width + gap;

    }
    
    self.scrollViewCategories.contentSize = CGSizeMake(posX+10, self.scrollViewCategories.frame.size.height);
    
}

- (void) addTabWithTitle:(NSString *)title frame:(CGRect)frame index:(int)index {
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height)];
    view.tag = index;

    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(5, 0, frame.size.width-10, frame.size.height)];
    button.tag = index;
    [button setTitle:title forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont fontWithName:@"SegoeUI" size:15.0]];
    
    [button addTarget:self action:@selector(selectCategoryTab:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:button];
    [self.scrollViewCategories addSubview:view];
    
    [iBeautyUtility setPickUpDeliveryViewWithoutBorderColor:view];

    if (self.segmentIndex == index) {
        
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        view.backgroundColor = [UIColor iBeautyThemeColor];
        //[view.layer insertSublayer:[iBeautyUtility setGradientColor_Views:view status:YES] atIndex:0];
    }else {
        [button setTitleColor:[UIColor iBeautyThemeColor] forState:UIControlStateNormal];
        view.backgroundColor = [UIColor clearColor];
        //[view.layer insertSublayer:[iBeautyUtility setGradientColor_Views:view status:NO] atIndex:0];
    }

}

- (void)loadNotificationObserverForAppointmentsInfo {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getAppointmentsInfo:) name:@"selectedAppointmentsInfo" object:nil];

}

- (void)getAppointmentsInfo:(NSNotification*)appointmentInfo {
    
    NSMutableArray *infoArray = [[NSMutableArray alloc]init];
    
    if (APP_DEBUG_MODE)
        NSLog(@"appointmenst Info  is %@",appointmentInfo);
    NSDictionary *appointmentDict = appointmentInfo.userInfo;
    if ([appointmentDict valueForKey:@"appointmentsInfo"]) {
        infoArray = [appointmentDict valueForKey:@"appointmentsInfo"];
    }
    
    self.decrementStatus =  [[appointmentDict valueForKey:@"DecrementStatus"]boolValue];
    self.servicesInfoArray = [NSMutableArray new];
    
    if (self.decrementStatus == NO) {
        
        if (self.selectedServicesArray.count>0) {
            [self.selectedServicesArray removeAllObjects];
        }
    }

    if (infoArray.count>0) {
        
        NSMutableArray *allServicesArray = [NSMutableArray new];
        for (int i = 0;i<self.servicesSectionArray.count;i++) {
            NSString *key = [NSString stringWithFormat:@"%@",self.servicesSectionArray[i]];
            [allServicesArray addObjectsFromArray:[self.servicesdictInfo valueForKey:key]];
        }
        
        for (int i =0;i<infoArray.count;i++) {
            NSString *deletedServiceID = infoArray[i];
            for (ServicesDetail *detailInfo in allServicesArray) {
                if ([deletedServiceID  isEqualToString:detailInfo.serviceId]) {
                    if (detailInfo.increaseIndex >= 1) {
                        detailInfo.increaseIndex = detailInfo.increaseIndex-1;
                    }
                }
            }
        }
    }

    [self addTotalAppointments];
}


#pragma mark - Get Services From Parent Service Categories

- (void)getServicesFromParentServiceCategories {
    
    ServicesDetail *detailInfo = [ServicesDetail getSharedInstance];
    self.allServicesArray = self.categoryInfoObj.Services;
    self.servicesdictInfo = [detailInfo filterServicesArrayAccordingToServiceId:self.allServicesArray];
    if (APP_DEBUG_MODE)
        NSLog(@"service info dict is %@",self.servicesdictInfo);
    
    self.servicesSectionArray = [self.servicesdictInfo valueForKey:@"servicesSectionArray"];

    [self setUpSegmentControl];
    
    [self.collectionView reloadData];

}

#pragma mark - Book Now

- (IBAction)BookNow:(id)sender {
    
    if (self.totalAppointments>0) {
        
        BookAppointmentController *appointmentController = [self.storyboard instantiateViewControllerWithIdentifier:kAppointmentViewController];
        appointmentController.appointmentsDetailArray = self.selectedServicesArray;
        
        if(self.decrementStatus == YES) {
            appointmentController.appointmentsArray_new = self.servicesInfoArray;
        }
        appointmentController.categoryInfoObj = self.categoryInfoObj;
        
        [self.navigationController pushViewController:appointmentController animated:YES];
        
    }else {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"Please select services", @"")];
    }
}

#pragma mark - Segmented Control

- (void)setUpSegmentControl {
    
    [self selectSelectedSegment];
    
    [self initCategories];
    [self filterArrayBySegmentIndex];
}

#pragma mark - select selected segment

- (void)selectSelectedSegment {
    
    if (self.servicesCountArray.count>0) {
        Services *serviceInfo = self.servicesCountArray[self.segmentIndexStatus];
        
        for (int i = 0; i<self.servicesSectionArray.count; i++) {
            
            NSString *catString = self.servicesSectionArray[i];
            
            if ([catString isEqualToString:serviceInfo.serviceID]) {
                self.segmentIndex = i;
            }
        }
    }
    
}


#pragma mark - Filter Array By Category

- (void)filterArrayBySegmentIndex {
    
    if (self.servicesSectionArray.count == 1) {
        self.segmentIndex = 0;
    }
    if (self.servicesSectionArray.count > 0) {
        
        NSString *key = [NSString stringWithFormat:@"%@",self.servicesSectionArray[self.segmentIndex]];
        self.allServicesArray = self.categoryInfoObj.Services;
        self.servicesDetailArray = [self filterServicesArrayAccordingToServiceId:self.allServicesArray categoryId:key];
        
    }
    if (APP_DEBUG_MODE)
        NSLog(@"service info array is %@",self.servicesDetailArray);
}


- (NSMutableArray*)filterServicesArrayAccordingToServiceId:(NSMutableArray*)servicesDetailArray categoryId:(NSString*)serviceCatId {
    
    NSMutableArray *serviceListArray = [NSMutableArray new];
    
    for (int i =0;i<servicesDetailArray.count;i++) {
        
        ServicesDetail *detailObject = servicesDetailArray[i];
        
        if (APP_DEBUG_MODE)
            NSLog(@"Service Category Id is %@",detailObject.serviceCategoryId);
        
        NSString *catId = detailObject.serviceCategoryId;
        
        if ([catId  isEqualToString:serviceCatId]) {
            
            [serviceListArray addObject:detailObject];
        }
    }
    
    return serviceListArray;
}


#pragma mark - Select Category

- (void)selectCategoryTab:(UIButton*)sender {
    
    self.segmentIndex = sender.tag;
    
    [self initCategories];

    [self filterArrayBySegmentIndex];
    
    [self.collectionView reloadData];
}


- (IBAction)segmentControlValueChanged:(UISegmentedControl*)sender {
    
    self.segmentIndex = sender.selectedSegmentIndex;
    
    [self filterArrayBySegmentIndex];
    
    [self.collectionView reloadData];
}


#pragma mark - Back Action

- (IBAction)backActionMethod:(id)sender {
    
    if (self.basketInfoArray.count < 1) {
        [self createBasketForServices];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
    
//    [[NSUserDefaults standardUserDefaults]setValue:@"" forKey:@"BasketId"];
//    [[NSUserDefaults standardUserDefaults]synchronize];
//    [self removeTotalAppointments];
    
}

#pragma mark - Set Up Collection View

- (void)setUpCollectionView {
    
    UINib *collectionNib = [UINib nibWithNibName:@"ServicesDetailCollCell" bundle:nil];
    [self.collectionView registerNib:collectionNib forCellWithReuseIdentifier:servicesDetailCellIdentifier];
    [iBeautyUtility setButtonCorderRound:self.bookNow];
    //[self.bookNow.layer insertSublayer:[iBeautyUtility setGradientColor_Buttons:self.bookNow] atIndex:0];

}

#pragma mark - CollectioView DataSource and Delegate methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.servicesDetailArray.count;
    
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ServicesDetailCollCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:servicesDetailCellIdentifier forIndexPath:indexPath];
    
    ServicesDetail *detailObj = self.servicesDetailArray[indexPath.row];
    [cell configureDetailCollectionCellWithInfo:detailObj];
    
    [cell.increaseButton addTarget:self action:@selector(addAppointments:) forControlEvents:UIControlEventTouchUpInside];
    cell.increaseButton.tag = indexPath.row;
    [cell.decreaseButton addTarget:self action:@selector(decreaseAppointments:) forControlEvents:UIControlEventTouchUpInside];
    cell.decreaseButton.tag = indexPath.row;
    [cell.viewMoreBtn addTarget:self action:@selector(viewMoreDescription:) forControlEvents:UIControlEventTouchUpInside];
    cell.viewMoreBtn.tag = indexPath.row;
    
//    if (self.decrementStatus == YES) {
//        cell.decreaseButton.hidden = YES;
//    }else {
        cell.decreaseButton.hidden = NO;
//    }
    
    return cell;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    CGFloat screenWidth = CGRectGetWidth([UIScreen mainScreen].bounds);
    CGFloat itemWidth = 0.0f, itemHeight = 0.0f;
    itemWidth = floor(screenWidth);
    
    ServicesDetail *detailObj = self.servicesDetailArray[indexPath.row];

    CGFloat nameHeight = detailObj.nameHeight;
    
    CGFloat descriptionHeight = detailObj.descriptionHeight;
    
    if (descriptionHeight > 21.0f) {
        
        if (detailObj.viewMoreStatus == YES) {
            itemHeight = 10 + nameHeight + 10 + descriptionHeight + 5 + 25 + 5 + 36 + 10 + 8;
        }else {
            itemHeight = 10 + nameHeight + 10 + 21.0f + 5 + 25 + 5 + 36 + 10 + 8;
        }
        
    }else {
        if (descriptionHeight == 0) {
            itemHeight = 10 + nameHeight + 10 + 36 + 10 + 8;
        }else {
            itemHeight = 10 + nameHeight + 10 + descriptionHeight + 10 + 36 + 10 + 8;
        }
    }
    
    return CGSizeMake(itemWidth, itemHeight);
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 1.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 1.0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
}


- (void)viewMoreDescription:(UIButton*)sender {
    
    ServicesDetail *detailObj = self.servicesDetailArray[sender.tag];
    
    if (detailObj.viewMoreStatus == YES) {
        detailObj.viewMoreStatus = NO;
    }else {
        detailObj.viewMoreStatus = YES;
    }
    
    [self.collectionView reloadData];

}

#pragma mark - Update basket for New Appointment


- (void)updateBasketForNewAppointments:(ServicesDetail*)serviceDetail {
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_UPDATE_BASKET];
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSDictionary *dictInfo = [self getBasketParametersInfoFor_NewAppointments:serviceDetail];
    
    if (internetStatus != NotReachable) {
        
        [handler jsonPostData:url params:dictInfo onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                @try {
                    if (completed) {
                        
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSDictionary *tempInfoDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE)
                            NSLog(@"Temp Info Dict is %@", tempInfoDict);
                        BasketInfoObj *infoObj = [BasketInfoObj getSharedInstance];
                        self.basketInfoArray = [infoObj basketDetailInfoObject:tempInfoDict];
                        
                        if (APP_DEBUG_MODE)
                            NSLog(@"Response Array is %@", self.basketInfoArray);

                    }else {
                        if (APP_DEBUG_MODE)
                            NSLog(@"error is %@", error.localizedDescription);
                    }
                    
                } @catch (NSException *exception) {
                    [iBeautyUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
    
}

- (NSMutableDictionary*)getBasketParametersInfoFor_NewAppointments:(ServicesDetail*)detailInfo {
    
    NSMutableDictionary *basketDict = [[NSMutableDictionary alloc]init];
    NSString *businessId = self.categoryInfoObj.Id;
    [basketDict setValue:businessId forKey:@"BusinessId"];
    
    NSString *customerID = [[NSUserDefaults standardUserDefaults]valueForKey:KUserId];
    [basketDict setValue:customerID forKey:@"CustomerId"];
    
    self.basketID = [[NSUserDefaults standardUserDefaults]valueForKey:@"BasketId"];
    [basketDict setObject:self.basketID forKey:@"BasketId"];
    
    NSMutableArray *appointmentsArray = [NSMutableArray new];
    NSMutableDictionary *appointmentDict = [[NSMutableDictionary alloc]init];
    
    [appointmentDict setValue:detailInfo.serviceId forKey:@"ServiceId"];
    [appointmentDict setValue:@"" forKey:@"StartDateTime"];
    [appointmentDict setValue:@"" forKey:@"EndDateTime"];
    [appointmentDict setValue:[NSNumber numberWithInteger:0] forKey:@"StaffId"];
    [appointmentsArray addObject:appointmentDict];
    
    [basketDict setObject:@"" forKey:@"UpdatedAppointments"];
    [basketDict setObject:appointmentsArray forKey:@"NewAppointments"];
    [basketDict setObject:@"" forKey:@"RemovedAppointments"];
    [basketDict setObject:@"" forKey:@"Products"];
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    [basketDict setValue:accessToken forKey:@"Authorization"];
    
    NSString *offerId = @"";
    [basketDict setValue:offerId forKey:@"OfferId"];
    
    return basketDict;
    
}


#pragma mark - Add/Delete Appointments

- (void)emptyProductBasket {
    
    NSString *basketID =  [[NSUserDefaults standardUserDefaults] valueForKey:@"ProductBasketId"];
    
    if (basketID.length > 0) {
        [self clearBasket:Product];
//        [[NSUserDefaults standardUserDefaults]setValue:@"" forKey:@"ProductBasketId"];
//        [[NSUserDefaults standardUserDefaults]synchronize];
    }
}

- (void)emptyServiceBasket {
    
    NSString *basketID =  [[NSUserDefaults standardUserDefaults] valueForKey:@"BasketId"];
    
    if (basketID.length > 0) {
        [self clearBasket:Service];
//        [[NSUserDefaults standardUserDefaults]setValue:@"" forKey:@"BasketId"];
//        [[NSUserDefaults standardUserDefaults]synchronize];
    }
}

- (void)addAppointments:(UIButton*)sender {
    
    [self emptyProductBasket];
    
    ServicesDetail*detailInfo = self.servicesDetailArray[sender.tag];
    detailInfo.increaseIndex += 1;
    
    if (self.basketInfoArray.count > 0) {
        
        [self updateBasketForNewAppointments:detailInfo];
        
    }else {

        [self emptyServiceBasket];

//        if(self.decrementStatus == NO) {
            [self.selectedServicesArray addObject:detailInfo];
//        }else {
//            [self.servicesInfoArray addObject:detailInfo];
//        }
    }

    [self addTotalAppointments];
}

- (void)decreaseAppointments:(UIButton*)sender {
    
    ServicesDetail*detailInfo = self.servicesDetailArray[sender.tag];
    if (detailInfo.increaseIndex >= 1) { //&& self.decrementStatus == NO
        detailInfo.increaseIndex -= 1;
        detailInfo.staffInfo = nil;
        detailInfo.slotInfo = nil;
        NSInteger index = [self getServiceIdToDelete:detailInfo];
        if (self.basketInfoArray.count > 0) {
            [self deleteAppointmentFromBasket:index];
        }
        [self.selectedServicesArray removeObject:detailInfo];
    }
    [self addTotalAppointments];

}

- (NSInteger)getServiceIdToDelete:(ServicesDetail*)detailInfo {
    
    NSInteger indexValue = 0;
    NSString *serviceID = detailInfo.serviceId;
    
    for (int i =0;i<self.basketInfoArray.count;i++) {
        
        BasketInfoObj *infoObj = self.basketInfoArray[i];
        
        if ([infoObj.serviceId isEqualToString:serviceID]) {
            indexValue = i;
            break;
        }
    }
    
    return indexValue;
    
}

- (void)addTotalAppointments {
    
    self.totalAppointments = 0;
    
    for (int i = 0;i<self.servicesSectionArray.count;i++) {
        
        NSString *key = [NSString stringWithFormat:@"%@",self.servicesSectionArray[i]];
        NSMutableArray * valuesArray = [self.servicesdictInfo valueForKey:key];
        
        for (ServicesDetail *detailInfo in valuesArray) {
            
            if (detailInfo.increaseIndex>0) {
                
                self.totalAppointments = self.totalAppointments + detailInfo.increaseIndex;
            }
        }
    }
    NSString *serviceAdded = [self setUpServicesAddedLabel];
    self.appointmentsNumberLabel.text = [NSString stringWithFormat:@"%ld %@",(long)self.totalAppointments,serviceAdded];
    [self.collectionView reloadData];

}

- (NSString *)setUpServicesAddedLabel {
    
    NSString *itemsAdded = @"";
    
    if (self.totalAppointments == 0 || self.totalAppointments == 1) {
        itemsAdded = NSLocalizedString(@"Service added", @"");
    }else {
        itemsAdded = NSLocalizedString(@"Services added", @"");
    }
    return itemsAdded;
}

- (void)removeTotalAppointments {
    
    self.totalAppointments = 0;
    
    for (int i = 0;i<self.servicesSectionArray.count;i++) {
        
        NSString *key = [NSString stringWithFormat:@"%@",self.servicesSectionArray[i]];
        
        NSMutableArray * valuesArray = [self.servicesdictInfo valueForKey:key];
        
        for (ServicesDetail *detailInfo in valuesArray) {
            
            if (detailInfo.increaseIndex>0) {
                
                detailInfo.increaseIndex = 0;
                detailInfo.staffInfo = nil;
                detailInfo.slotInfo = nil;
                
            }
        }
    }
    
    if (self.selectedServicesArray.count>0) {
        [self.selectedServicesArray removeAllObjects];
    }
}

#pragma mark - Delete Appointment

- (void)deleteAppointmentFromBasket:(NSInteger)serviceIndex {

    BasketInfoObj *basketObj = self.basketInfoArray[serviceIndex];
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_UPDATE_BASKET];
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    NSDictionary *dictInfo ;
    basketObj.slotInfObj = nil;
    basketObj.staffInfo = nil;
    dictInfo = [self getBasketInfoTo_DeleteBasket:basketObj.appointmentId];
    
    if (internetStatus != NotReachable) {
        
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonPostData:url params:dictInfo onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                @try {
                    if (completed) {
                        
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSDictionary *tempInfoDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE) { NSLog(@"Temp Info Dict is %@", tempInfoDict); }
                        BasketInfoObj *infoObj = [BasketInfoObj getSharedInstance];
                        self.basketInfoArray = [infoObj basketDetailInfoObject:tempInfoDict];
                        if (APP_DEBUG_MODE) { NSLog(@"Response Array is %@", self.basketInfoArray);}

                        if (self.basketInfoArray.count > 0) {
                            infoObj = self.basketInfoArray[0];
                            [[NSUserDefaults standardUserDefaults]setValue:infoObj.basketId forKey:@"BasketId"];
                        }
                        if (APP_DEBUG_MODE) { NSLog(@"Basket Id is %@", infoObj.basketId); }
                        
                    }else {
                        if (APP_DEBUG_MODE) { NSLog(@"error is %@", error.localizedDescription);}
                    }
                    
                } @catch (NSException *exception) {
                    [iBeautyUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}

- (NSMutableDictionary*)getBasketInfoTo_DeleteBasket:(NSString*)appointmentId {
    
    NSMutableDictionary *basketDict = [[NSMutableDictionary alloc]init];
    
    NSString *businessID = self.categoryInfoObj.Id;
    [basketDict setValue:businessID forKey:@"BusinessId"];
    
    NSString *customerID = [[NSUserDefaults standardUserDefaults]valueForKey:KUserId];
    [basketDict setValue:customerID forKey:@"CustomerId"];
    
    NSMutableArray *appointmentsArray = [NSMutableArray new];
    NSMutableDictionary *appointmentDict = [[NSMutableDictionary alloc]init];
    [appointmentDict setValue:appointmentId forKey:@"Id"];
    [appointmentsArray addObject:appointmentDict];
    [basketDict setObject:@"" forKey:@"UpdatedAppointments"];
    [basketDict setObject:@"" forKey:@"NewAppointments"];
    [basketDict setObject:appointmentsArray forKey:@"RemovedAppointments"];
    [basketDict setObject:@"" forKey:@"Products"];
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    [basketDict setValue:accessToken forKey:@"Authorization"];
    
    NSString *basketId = [[NSUserDefaults standardUserDefaults] valueForKey:@"BasketId"];
    [basketDict setValue:basketId forKey:@"BasketId"];
    
    return basketDict;
    
}

#pragma mark - Clear Basket


- (void)clearBasket:(NSInteger)basketType {
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSString *url;

    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    
    NSString *basketId = @"";
    
    if (basketType == Service) {
        basketId = [[NSUserDefaults standardUserDefaults]valueForKey:@"BasketId"];
        url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_CLEAR_SERVICE_BASKET];
    }else {
        basketId = [[NSUserDefaults standardUserDefaults]valueForKey:@"ProductBasketId"];
        url = [ConnectionHandler getServiceURL_Products:TYPE_SERVICE_CLEAR_PRODUCT_BASKET];
    }

    NSDictionary *dictInfo = [NSDictionary dictionaryWithObjectsAndKeys:accessToken,@"Authorization",basketId,@"BasketId", nil];
    
    if (internetStatus != NotReachable) {
        
//        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonPostData:url params:dictInfo onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
//                [SVProgressHUD dismiss];
                
                @try {
                    
                    if (completed) {
                        
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSDictionary *tempInfoDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE)
                            NSLog(@"Temp Info Dict is %@", tempInfoDict);
                        
                        NSInteger status = [[tempInfoDict valueForKey:@"status"]integerValue];
                        if (status == 200) {
                            if (basketType == Service) {
                                [[NSUserDefaults standardUserDefaults]setValue:@"" forKey:@"BasketId"];
                                [[NSUserDefaults standardUserDefaults]synchronize];
                                [self.delegate.loopTimer invalidate];
                            }else {
                                [[NSUserDefaults standardUserDefaults]setValue:@"" forKey:@"ProductBasketId"];
                                [[NSUserDefaults standardUserDefaults]synchronize];
                            }
                        }
                        
                    }else {
                        if (APP_DEBUG_MODE)
                            NSLog(@"error is %@", error.localizedDescription);
                    }
                    
                } @catch (NSException *exception) {
                    [iBeautyUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
