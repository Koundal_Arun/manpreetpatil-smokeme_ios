//
//  ServicesDetailViewController.h
//  iBeauty
//
//  Created by App Innovation on 02/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"

@interface ServicesDetailViewController : UIViewController

@property (nonatomic,strong) ServicesCategoriesInfo *categoryInfoObj;
@property (nonatomic,strong) Services *servicesInfoObj;
@property (nonatomic,strong) NSMutableArray *servicesCountArray;
@property (nonatomic,assign) NSInteger segmentIndexStatus;

@end
