//
//  SelectStaffViewController.h
//  iBeauty
//
//  Created by App Innovation on 04/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServicesDetail.h"
#import "StaffDetail.h"
#import "ServicesCategoriesInfo.h"


@interface SelectStaffViewController : UIViewController

@property (nonatomic,strong) ServicesDetail *serviceInfo;
@property (nonatomic,assign) NSInteger  serviceIndex;

@property (nonatomic,strong) ServicesCategoriesInfo *serviceCatInfo;
@property (nonatomic,strong) NSMutableArray *staffInfoArray;

@end
