//
//  AppointmentDetailViewController.m
//  iBeauty
//
//  Created by App Innovation on 13/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "AppointmentDetailViewController.h"
#import "AppointmentDetailCell.h"
#import "ServicesDetail.h"
#import "AppointmentDetailHeader.h"
#import "AppointmentDetailFooter.h"
#import "CancelAppointmentViewController.h"

static NSString *detailCellIdentifier = @"AppointmentDetailCell";
static NSString *headerCellIdentifier = @"AppointmentDetailHeader";
static NSString *footerCellIdentifier = @"AppointmentDetailFooter";

@interface AppointmentDetailViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,CLLocationManagerDelegate>

@property (nonatomic,weak) IBOutlet UICollectionView *appointmentDetailCollView;
@property (nonatomic,strong)  NSMutableArray *appointmentDetailArray;

@property (nonatomic,strong) CLLocationManager *locationManager;
@property (nonatomic,strong) CLLocation *currentLocation;
@property (nonatomic,assign) NSInteger count;
@property (nonatomic,weak)  IBOutlet UINavigationBar *navBar;

@end

@implementation AppointmentDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.appointmentDetailArray = self.appointmentDetailInfo.appointmentsInfo;
    [self configureLocationManager];
    [self cancelAppointmentObserver];
    AppDelegate *delegate = [AppDelegate sharedInstance];
    delegate.navigateStatus = NO;
    //[self setGradient];
}

- (void)viewWillAppear:(BOOL)animated {
 
    [super viewWillAppear:YES];
    [self.appointmentDetailCollView reloadData];
}

#pragma mark - Set Gradient

- (void)setGradient {
    
    self.navBar.frame = [iBeautyUtility setUpFrame:self.navBar.frame];
    CAGradientLayer *gradient = [iBeautyUtility setNavigationGradient:self.navBar];
    [self.navBar setBackgroundImage:[iBeautyUtility imageFromLayer:gradient] forBarMetrics:UIBarMetricsDefault];
}

#pragma mark - Add Oberver

- (void)cancelAppointmentObserver {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cancelSelectedAppointment:) name:@"cancelAppointment" object:nil];
    
}

- (void)cancelSelectedAppointment:(NSNotification*)notification {
    
    NSDictionary *infoDict = notification.userInfo;
    if (APP_DEBUG_MODE) { NSLog(@"Info is %@",infoDict);}
    NSInteger index = [[infoDict valueForKey:@"index"]integerValue];
    if (self.appointmentDetailArray.count > 0) {
        AllAppointmentsInfo *infoObj = self.appointmentDetailArray[index];
        infoObj.status = NO;
    }
    
    [self.appointmentDetailCollView reloadData];
}


#pragma mark - Configure Location Manager

-(void)configureLocationManager {
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        [self.locationManager requestWhenInUseAuthorization];
    
    [self.locationManager startUpdatingLocation];
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    self.currentLocation = locations.lastObject;
    
    if ( self.currentLocation && self.count == 0) {
        self.count += 1;
        [self.locationManager stopUpdatingLocation];
    }
}

#pragma mark - Back Action

- (IBAction)backActionMethod:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark - CollectioView DataSource and Delegate methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.appointmentDetailArray.count;
    
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    AppointmentDetailCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:detailCellIdentifier forIndexPath:indexPath];
    AllAppointmentsInfo *detailObj = self.appointmentDetailArray[indexPath.row];
    [cell configureAppointmentDetailCell:detailObj mainAppointmentObj:self.appointmentDetailInfo];
    
    [cell.cancelButton addTarget:self action:@selector(cancelAppointment:) forControlEvents:UIControlEventTouchUpInside];
    cell.cancelButton.tag = indexPath.row;

    return cell;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat screenWidth = CGRectGetWidth([UIScreen mainScreen].bounds);
    
    CGFloat itemWidth = 0.0f,itemHeight = 0.0f;
    
    AllAppointmentsInfo *detailObj = self.appointmentDetailArray[indexPath.row];
    NSMutableArray *staffArray =  detailObj.staffArray;
    
    StaffDetail *staffInfo;
    if (staffArray.count>0) {
        staffInfo = staffArray[0];
    }
    itemHeight = 80.0f;
    
    if (staffInfo) {
        itemHeight = 107.0f;
    }
    itemWidth = floor(screenWidth);
    
    return CGSizeMake(itemWidth, itemHeight);
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
   
   CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
   CGFloat screenHeight = 0.0f;
   
   NSString *deviceType = [UIDevice currentDevice].model;
   if ([deviceType isEqualToString:@"iPhone"]) {
       screenHeight = 207 + 40 + 10 + self.appointmentDetailInfo.providerNameHeight + 5 + self.appointmentDetailInfo.providerAddressHeight + 10 + 10 + 21.0f + 12.0f;
   }else {
       screenHeight = 207 + 40 + 10 + self.appointmentDetailInfo.providerNameHeight + 5 + self.appointmentDetailInfo.providerAddressHeight + 10 + 10 + 21.0f + 12.0f;
   }
    
   return CGSizeMake(screenWidth,screenHeight);
   
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    
    if (self.appointmentDetailInfo.isOfferApplied == YES) {
        if (self.appointmentDetailInfo.endDateCount > 0) {
            return CGSizeMake(SCREEN_WIDTH, 0.0f);
        }else {
            return CGSizeMake(SCREEN_WIDTH, 70.0f);
        }
    }else {
        return CGSizeMake(0.0f, 0.0f);
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 1.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 1.0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    id reusableview = [[UICollectionReusableView alloc]init];
    
    if (kind == UICollectionElementKindSectionHeader) {
        
        AppointmentDetailHeader *collHeaderView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:headerCellIdentifier forIndexPath:indexPath];
        [collHeaderView.getDirectionButton addTarget:self action:@selector(viewProviderInMap:) forControlEvents:UIControlEventTouchUpInside];
        [collHeaderView configureAppointmentDetailHeaderView:self.appointmentDetailInfo];
        
        reusableview = collHeaderView;
        
    }else if (kind == UICollectionElementKindSectionFooter) {
        
        AppointmentDetailFooter *collFooterView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:footerCellIdentifier forIndexPath:indexPath];
        collFooterView.appointmentCancelButton.frame = [iBeautyUtility setUpButtonFrame:collFooterView.appointmentCancelButton.frame];
//        [collFooterView.appointmentCancelButton.layer insertSublayer:[iBeautyUtility setGradientColor_Buttons:collFooterView.appointmentCancelButton] atIndex:0];

        if (self.appointmentDetailInfo.status == YES) {
            collFooterView.appointmentCancelButton.userInteractionEnabled = YES;
            [collFooterView.appointmentCancelButton setTitle:NSLocalizedString(@"Cancel Appointment", @"") forState:UIControlStateNormal];
            collFooterView.cancelView.backgroundColor = [UIColor clearColor];
            [iBeautyUtility setButtonCorderRound:collFooterView.appointmentCancelButton];
        }else {
            collFooterView.appointmentCancelButton.userInteractionEnabled = NO;
            [collFooterView.appointmentCancelButton setTitle:NSLocalizedString(@"Cancelled", @"") forState:UIControlStateNormal];
            collFooterView.appointmentCancelButton.backgroundColor = [UIColor cancelAppointmentThemeColor];
            [collFooterView.appointmentCancelButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            collFooterView.cancelView.backgroundColor = [UIColor clearColor];
            [iBeautyUtility setCancelAppointmentButtonCorderRound:collFooterView.appointmentCancelButton];
        }
        [collFooterView.appointmentCancelButton addTarget:self action:@selector(cancelAppointment:) forControlEvents:UIControlEventTouchUpInside];
        [collFooterView.appointmentCancelButton setTag:10];
        reusableview = collFooterView;
        
    }
    
    return reusableview;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
}

#pragma mark - Cancel Appointment

- (void)cancelAppointment:(UIButton*)sender {
    
    [self alertActionForCancel:sender.tag];
    
}

- (void)alertActionForCancel:(NSInteger)indexRow {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Are you sure? You want to Cancel",@"") message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"NO",@"")
                                                           style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                               [self dismissViewControllerAnimated:YES completion:^{
                                                               }];
                                                           }];
    
    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"YES",@"")
                                                        style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                            [self moveToCancelAppointmentView:indexRow];
                                                        }];
    [alert addAction:cancelAction];
    [alert addAction:yesAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}


#pragma mark - Navigate to Cancel Appointment View

- (void)moveToCancelAppointmentView:(NSInteger)rowIndex {
    
    if (self.appointmentDetailInfo.isOfferApplied == YES) {
        
        [self moveToCancelAppointmentViewWhenOfferApplide];
        
    }else {
        [self moveToCancelAppointmentViewWhen_NofferApplide:rowIndex];
    }
    
}

- (void)moveToCancelAppointmentViewWhenOfferApplide {
    
    CancelAppointmentViewController *cancelController = [self.storyboard instantiateViewControllerWithIdentifier:kCancelAppointmentController];
    cancelController.appointmentOrderObj = self.appointmentDetailInfo;
    cancelController.cancelType = 1;
    [self.navigationController pushViewController:cancelController animated:YES];
}

- (void)moveToCancelAppointmentViewWhen_NofferApplide:(NSInteger)rowIndex {
    
    CancelAppointmentViewController *cancelController = [self.storyboard instantiateViewControllerWithIdentifier:kCancelAppointmentController];
    cancelController.appointmentOrderObj = self.appointmentDetailInfo;
    cancelController.appointmentOrderDetailObj = self.appointmentDetailArray[rowIndex];
    cancelController.appointmentsNumber = self.appointmentDetailArray.count;
    cancelController.selectedIndex = rowIndex;
    cancelController.cancelType = 1;
    [self.navigationController pushViewController:cancelController animated:YES];
    
}

#pragma mark - View Provider In Map

- (void)viewProviderInMap:(UIButton*)sender {
    
    [self actionSheetForChooseMapTypeNavigation:sender];
}

#pragma mark - Action method for Navigate Options

- (void)actionSheetForChooseMapTypeNavigation:(UIButton*)sender {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Select Option",@"") message:nil
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *firstOption = [UIAlertAction actionWithTitle:NSLocalizedString(@"Apple Map",@"")
                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                              [self navigateWithAppleMap];
                                                          }];
    UIAlertAction *secongOption = [UIAlertAction actionWithTitle:NSLocalizedString(@"Google Map",@"")
                                                           style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                               [self navigateWithGoogleMap];
                                                           }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel",@"")
                                                           style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                               [self dismissViewControllerAnimated:YES completion:^{
                                                               }];
                                                           }];
    [alert addAction:firstOption];
    [alert addAction:secongOption];
    [alert addAction:cancelAction];
    
    NSString *deviceType = [UIDevice currentDevice].model;
    
    if (![deviceType isEqualToString:@"iPhone"]) {
        
        [alert setModalPresentationStyle:UIModalPresentationPopover];
        UIPopoverPresentationController *popPresenter = [alert popoverPresentationController];
        popPresenter.sourceView = sender;
        popPresenter.sourceRect = sender.bounds;
    }
    
    [self presentViewController:alert animated:YES completion:nil];
}


- (void)navigateWithAppleMap {
    
    NSString* directionsURL = [NSString stringWithFormat:@"http://maps.apple.com/?saddr=%f,%f&daddr=%f,%f",self.currentLocation.coordinate.latitude, self.currentLocation.coordinate.longitude, [self.appointmentDetailInfo.latitude doubleValue], [self.appointmentDetailInfo.longitude doubleValue]];
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(openURL:options:completionHandler:)]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: directionsURL] options:@{} completionHandler:^(BOOL success) {}];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: directionsURL] options:@{} completionHandler:nil];
    }
    
}

- (void)navigateWithGoogleMap {
    
    if ([[UIApplication sharedApplication] canOpenURL:
         [NSURL URLWithString:@"comgooglemaps://"]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"comgooglemaps://?saddr=%f,%f&daddr=%f,%f&zoom=14&views=traffic",self.currentLocation.coordinate.latitude, self.currentLocation.coordinate.longitude, [self.appointmentDetailInfo.latitude doubleValue], [self.appointmentDetailInfo.longitude doubleValue]]] options:@{} completionHandler:nil];
    } else {
        NSLog(@"Can't use comgooglemaps://");
        [self alertActionForOpenGoogleMapInBrowser];
    }
    
}

- (void)alertActionForOpenGoogleMapInBrowser {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Don't have Google Maps",@"") message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"NO",@"")
                                                           style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                               [self dismissViewControllerAnimated:YES completion:^{
                                                               }];
                                                           }];
    
    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"YES",@"")
                                                        style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                            [self openGoogleMapWithBrowser];
                                                        }];
    [alert addAction:cancelAction];
    [alert addAction:yesAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}


- (void)openGoogleMapWithBrowser {
    
    NSString *directionString = [NSString stringWithFormat:@"https://www.google.co.in/maps/dir/?saddr=%f,%f&daddr=%f,%f&directionsmode=driving",self.currentLocation.coordinate.latitude, self.currentLocation.coordinate.longitude, [self.appointmentDetailInfo.latitude doubleValue], [self.appointmentDetailInfo.longitude doubleValue]];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: directionString] options:@{} completionHandler:nil];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
