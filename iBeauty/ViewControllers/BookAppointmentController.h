//
//  BookAppointmentController.h
//  iBeauty
//
//  Created by App Innovation on 03/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "OffersDetail.h"

@interface BookAppointmentController : UIViewController

@property (nonatomic,strong) NSMutableArray *appointmentsDetailArray;
@property (nonatomic,strong) NSMutableArray *appointmentsArray_new;

@property (nonatomic,strong) ServicesCategoriesInfo *categoryInfoObj;

@property (nonatomic,strong) OffersDetail *offerInfoObj;
@property (nonatomic,assign) BOOL screenStatus;

- (void)clearBasket_services:(BOOL)screenStatus;

@end
