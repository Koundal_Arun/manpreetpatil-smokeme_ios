//
//  RegisterViewController.h
//  iBeauty
//
//  Created by App Innovation on 27/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <Google/SignIn.h>

@import GoogleSignIn;

@interface RegisterViewController : UIViewController<GIDSignInDelegate>

-(void)registerAccountMethod:(void(^)(id result, NSError *error))completion;

@end
