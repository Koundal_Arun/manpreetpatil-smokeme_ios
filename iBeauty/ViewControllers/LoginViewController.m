//
//  LoginViewController.m
//  iBeauty
//
//  Created by App Innovation on 27/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "LoginViewController.h"
#import "Constants.h"
#import "EnterInformationController.h"
#import "LanguageManager.h"

@import SkyFloatingLabelTextField;

@interface LoginViewController ()<UITextFieldDelegate>

@property (nonatomic,weak) IBOutlet UIView *IBCircleView;
@property (nonatomic,weak) IBOutlet UIView *emailView;
@property (nonatomic,weak) IBOutlet UIView *passwordView;
@property (nonatomic,weak) IBOutlet SkyFloatingLabelTextField *emailTextField;
@property (nonatomic,weak) IBOutlet SkyFloatingLabelTextField *passwordTextField;

@property (nonatomic,weak) IBOutlet UIButton *loginButton;

@property (nonatomic,weak) IBOutlet UIView *loginView;
@property (nonatomic,weak) IBOutlet UIView *signUpView;
@property (nonatomic,weak) IBOutlet UIView *loginBtnView;
@property (nonatomic,weak) IBOutlet UIView *socialBkgView;
@property (nonatomic,weak) IBOutlet UIView *socialLoginView;
@property (nonatomic,weak) IBOutlet UIView *BkgView;
@property (nonatomic,weak) IBOutlet UIView *containerView;
@property (nonatomic,weak) IBOutlet UIScrollView *scrollView;

@property (nonatomic,weak) IBOutlet UISegmentedControl *segmentControl;
@property (nonatomic,assign) NSInteger segmentIndex;

@property (nonatomic,weak) IBOutlet UIButton *facebookButton;
@property (nonatomic,weak) IBOutlet UIButton *googleButton;
@property (nonatomic,weak) IBOutlet UIButton *appleButton;
@property (nonatomic,weak) IBOutlet UIButton *forgotPassButton;

@property (nonatomic,weak) IBOutlet UIButton *registerButton;

@property (nonatomic,strong) NSDictionary *profileInfoDict;
@property (nonatomic,strong) NSString *loginType;

@property (nonatomic,weak) IBOutlet UIView *chooseLanguageView;
@property (nonatomic,weak) IBOutlet UILabel *languageLabel;
@property (nonatomic,weak) IBOutlet UIButton *languageButton;
@property (nonatomic,strong) NSString *languageString;

@property (nonatomic,weak) IBOutlet UILabel *iBeautyLable;
@property (nonatomic,weak) IBOutlet UILabel *orWithLabel;
@property (nonatomic,weak) IBOutlet UILabel *noAccountLabel;
@property (nonatomic,weak) IBOutlet UILabel *emailLabel;
@property (nonatomic,weak) IBOutlet UILabel *passwordLabel;
@property (nonatomic,weak) IBOutlet UILabel *facebookLabel;
@property (nonatomic,weak) IBOutlet UILabel *googleLabel;

@property UIToolbar *toolBar;
@property (nonatomic, strong) UIBarButtonItem   *doneButton;

#pragma mark - Register IBOutlets
@property (nonatomic,weak) IBOutlet SkyFloatingLabelTextField *signup_userNameTxtField;
@property (nonatomic,weak) IBOutlet SkyFloatingLabelTextField *signup_emailTxtField;
@property (nonatomic,weak) IBOutlet SkyFloatingLabelTextField *signup_passwordTxtField;
@property (nonatomic,weak) IBOutlet SkyFloatingLabelTextField *signup_confirmPasswordTxtField;


@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setUpViews];

    [self showGoogleNotificationListener];
    [self addGetEmailTextOberver];
    [self getLanguageAccordingToLanguageCode];
    [self setupToolBarOnTextfields];

    if (@available(iOS 13.0, *)) {
           [self observeAppleSignInState];
           [self setupUI];
       }
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    //[GIDSignIn sharedInstance].delegate = self;
    
    [GIDSignIn sharedInstance].presentingViewController = self;
    // Automatically sign in the user.
    [[GIDSignIn sharedInstance] restorePreviousSignIn];

}

#pragma mark - Oberve Apple Sign In State

- (void)observeAppleSignInState {
    if (@available(iOS 13.0, *)) {
        NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
        [center addObserver:self selector:@selector(handleSignInWithAppleStateChanged:) name:ASAuthorizationAppleIDProviderCredentialRevokedNotification object:nil];
    }
}

- (void)handleSignInWithAppleStateChanged:(id)noti {
    NSLog(@"%s", __FUNCTION__);
    NSLog(@"%@", noti);
}

#pragma mark - set Up views

- (void)setUpViews {
    
    //[iBeautyUtility setViewBorderRound:self.IBCircleView];
    //[iBeautyUtility setViewBorderRound:self.emailView];
    //[iBeautyUtility setViewBorderRound:self.passwordView];
    
    [iBeautyUtility setButtonCorderRound:self.loginButton];
    CGRect buttonFrame = self.loginButton.frame;
    buttonFrame.size.width = [UIScreen mainScreen].bounds.size.width-40.0f;
    self.loginButton.frame = buttonFrame;
    self.segmentIndex = 0;
    [self setUpAutoLayoutContraints];
    [self setUpLoginView];
    [self setUpSegmentViewTint];
    [self setStatusBarColor];
}

#pragma mark - Set Status Bar Color

- (void)setStatusBarColor {

    if (@available(iOS 13.0, *)) {
        
        UIView *statusBar = [[UIView alloc]initWithFrame:[UIApplication sharedApplication].keyWindow.windowScene.statusBarManager.statusBarFrame] ;
        [self.view addSubview:statusBar];
        if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
            statusBar.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:130/255.0 blue:90/255.0 alpha:1.0];
        }
    }else {
            UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
            if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
                statusBar.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:130/255.0 blue:90/255.0 alpha:1.0];
            }
    }

}

#pragma mark - Set SegmentViewTint

- (void)setUpSegmentViewTint {

    if (@available(iOS 13.0, *)) {

        [self.segmentControl setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor],NSFontAttributeName: [UIFont fontWithName:@"SegoeUI" size:15.0]} forState:UIControlStateSelected];
        [self.segmentControl setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor iBeautyThemeColor], NSFontAttributeName: [UIFont fontWithName:@"SegoeUI" size:15.0]} forState:UIControlStateNormal];
        [self.segmentControl setSelectedSegmentTintColor:[UIColor iBeautyThemeColor]];
        self.segmentControl.layer.borderWidth = 1.0;
        self.segmentControl.layer.cornerRadius = 10.0;
        self.segmentControl.layer.borderColor = [[UIColor iBeautyThemeColor]CGColor];
    } else {

       [self.segmentControl setTintColor:[UIColor iBeautyThemeColor]];

    }

}

#pragma mark - Choose Language

- (void)getLanguageAccordingToLanguageCode {
    
    NSString *languageCode =   [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];

    if ([languageCode isEqualToString:@"en"]) {
        
        self.languageString = @"English";
        
    }else if ([languageCode isEqualToString:@"ar"]) {
        
        self.languageString = @"Arabic";
        
    }
    
    [self setUpViewsLocalization];

}

- (void)setUpTextFieldsCursorAccordingToLanguage {
    
    NSString *languageCode =   [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];
    
    if ([languageCode isEqualToString:@"en"]) {
        
        self.emailTextField.textAlignment = NSTextAlignmentLeft;
        self.passwordTextField.textAlignment = NSTextAlignmentLeft;
        
    }else if ([languageCode isEqualToString:@"ar"]) {
        
        self.emailTextField.textAlignment = NSTextAlignmentRight;
        self.passwordTextField.textAlignment = NSTextAlignmentRight;
    }
}


- (IBAction)ChooseLanguageAction:(UIButton*)sender {
    
    [self actionSheetForChooseLanguage:sender];
}

#pragma mark - SegmentView Value Changed

-(IBAction)SegmentViewValueChanged:(UISegmentedControl *)SControl {
    
    if (SControl.selectedSegmentIndex==0){
        self.segmentIndex = 0;
        [self setUpLoginView];
    }else {
        self.segmentIndex = 1;
        [self setUpSignUpView];
    }

}

#pragma mark - Set Up Login View

-(void)setUpLoginView {
    
    [self.signUpView removeFromSuperview];
    self.loginView.hidden = NO;
    
    self.loginView.frame = CGRectMake(self.loginView.frame.origin.x,self.loginView.frame.origin.y, self.view.frame.size.width - 50.0f, self.loginView.frame.size.height);
    self.containerView.frame = CGRectMake(self.containerView.frame.origin.x,self.containerView.frame.origin.y, self.view.frame.size.width - 50.0f, 82.0f + self.loginView.frame.size.height);
    self.loginBtnView.frame = CGRectMake(self.loginBtnView.frame.origin.x,self.containerView.frame.origin.y + self.containerView.frame.size.height + 40.0f, self.view.frame.size.width - 50.0f,self.loginBtnView.frame.size.height);
    self.socialBkgView.frame = CGRectMake(self.socialBkgView.frame.origin.x,self.containerView.frame.origin.y + self.containerView.frame.size.height + 40.0f + self.loginBtnView.frame.size.height + 20.0f , self.view.frame.size.width - 50.0f,self.socialBkgView.frame.size.height);
    
    [self setScrollViewContentSize];
    [self setUpLoginBtnView];

}


#pragma mark - Set Up SignUp View

-(void)setUpSignUpView {
    
    self.loginView.hidden = YES;

    self.signUpView.frame = CGRectMake(0.0f, 82.0f, self.view.frame.size.width-50.0, self.signUpView.frame.size.height);
    [self.containerView addSubview:self.signUpView];
    self.containerView.frame = CGRectMake(self.containerView.frame.origin.x,self.containerView.frame.origin.y, self.view.frame.size.width - 50.0f, 82.0f + self.signUpView.frame.size.height);
    self.loginBtnView.frame = CGRectMake(self.loginBtnView.frame.origin.x,self.containerView.frame.origin.y + self.containerView.frame.size.height + 40.0f, self.view.frame.size.width - 50.0f,self.loginBtnView.frame.size.height);
    self.socialBkgView.frame = CGRectMake(self.socialBkgView.frame.origin.x,self.containerView.frame.origin.y + self.containerView.frame.size.height + 40.0f + self.loginBtnView.frame.size.height + 20.0f , self.view.frame.size.width - 50.0f,self.socialBkgView.frame.size.height);
    
    [self setScrollViewContentSize];
    [self setUpLoginBtnView];
}

#pragma mark - Set up Login Button Text

- (void)setUpLoginBtnView {
    
    if (self.segmentIndex == 0) {
        [self.loginButton setTitle:NSLocalizedString(@"LOGIN",@"") forState:UIControlStateNormal];
    }else {
        [self.loginButton setTitle:NSLocalizedString(@"SIGNUP",@"") forState:UIControlStateNormal];
    }
    
}

#pragma mark - Set Up AutoLayout For

-(void)setUpAutoLayoutContraints {
    
    self.loginView.translatesAutoresizingMaskIntoConstraints = YES;
    self.containerView.translatesAutoresizingMaskIntoConstraints = YES;
    self.loginBtnView.translatesAutoresizingMaskIntoConstraints = YES;
    self.socialBkgView.translatesAutoresizingMaskIntoConstraints = YES;

}

#pragma mark - Scroll View Content Size

- (void)setScrollViewContentSize {
    
    CGFloat viewHeight = 0.0f;
    viewHeight = 220.0f + self.containerView.frame.size.height + 40.0f;
    viewHeight += self.loginBtnView.frame.size.height + 20.0 + self.socialBkgView.frame.size.height + 20.0;
    self.BkgView.frame = CGRectMake(self.BkgView.frame.origin.x,self.BkgView.frame.origin.y, self.view.frame.size.width, viewHeight);
    self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.BkgView.frame.size.height);

}


#pragma mark - Action method for Drop Down

- (void)actionSheetForChooseLanguage:(UIButton*)button {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Choose Language",@"") message:nil
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *englishAction = [UIAlertAction actionWithTitle:@"English"
                                                            style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                self.languageString = @"English";
                                                                [[NSUserDefaults standardUserDefaults] setValue:@"en" forKey:kLMSelectedLanguageKey];
                                                                [LanguageManager saveLanguageByIndex:0];
                                                                [self setUpViewsLocalization];
                                                            }];
    UIAlertAction *saudiArabiaAction = [UIAlertAction actionWithTitle:@"العربية"
                                                                style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                    self.languageString = @"Arabic";
                                                                    [[NSUserDefaults standardUserDefaults] setValue:@"ar" forKey:kLMSelectedLanguageKey];
                                                                    [LanguageManager saveLanguageByIndex:1];
                                                                    [self setUpViewsLocalization];
                                                                }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel",@"")
                                                           style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                               [self dismissViewControllerAnimated:YES completion:^{
                                                               }];
                                                           }];
    [alert addAction:englishAction];
    [alert addAction:saudiArabiaAction];
    [alert addAction:cancelAction];
    
    NSString *deviceType = [UIDevice currentDevice].model;
    
    if (![deviceType isEqualToString:@"iPhone"]) {
        
        [alert setModalPresentationStyle:UIModalPresentationPopover];
        UIPopoverPresentationController *popPresenter = [alert popoverPresentationController];
        popPresenter.sourceView = button;
        popPresenter.sourceRect = button.bounds;
    }
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)setUpViewsLocalization {
    
    self.languageLabel.text = NSLocalizedString(self.languageString,@"");
    self.iBeautyLable.text = NSLocalizedString(@"iBeauty",@"");
    self.emailLabel.text = NSLocalizedString(@"EMAIL-ID",@"");
    self.passwordLabel.text = NSLocalizedString(@"PASSWORD",@"");
    [self.loginButton setTitle:NSLocalizedString(@"LOGIN",@"") forState:UIControlStateNormal];
    self.orWithLabel.text = NSLocalizedString(@"OR WITH",@"");
    self.facebookLabel.text =  NSLocalizedString(@"Facebook",@"");
    self.googleLabel.text =  NSLocalizedString(@"Google",@"");
    self.noAccountLabel.text =  NSLocalizedString(@"Don't have an account?",@"");
    [self setRegisterButtonAttributedTitle];
    [self setUpTextFieldsCursorAccordingToLanguage];
}

- (void)setRegisterButtonAttributedTitle {
 
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithAttributedString:[self.registerButton attributedTitleForState:UIControlStateNormal]];
    [attributedString replaceCharactersInRange:NSMakeRange(0, attributedString.length) withString:NSLocalizedString(@"REGISTER",@"")];
    [self.registerButton setAttributedTitle:attributedString forState:UIControlStateNormal];
}

#pragma mark - Add Oberver

- (void)addGetEmailTextOberver {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getEmailText:) name:@"enterEmailViaFBGoogleLogin" object:nil];
    
}

- (void)getEmailText:(NSNotification*)emailInfo {
    
    NSDictionary *userEmailInfo = emailInfo.userInfo;
    if (APP_DEBUG_MODE) { NSLog(@"User Email is %@",userEmailInfo); }
    
    NSString *emailString = [userEmailInfo valueForKey:KEmail];
    
    NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
    [newDict addEntriesFromDictionary:self.profileInfoDict];
    [newDict setObject:emailString forKey:@"email"];
    
    if (APP_DEBUG_MODE)
        NSLog(@"User Info Dict is %@",newDict);
    
    NSDictionary *profileInfo = [self getUserProfileInfo:newDict];
    [self registerAccountMethod:profileInfo];
    
}

#pragma mark - Log In With App

- (IBAction)logInWithApp:(id)sender {
    
    if (self.segmentIndex == 0) {
        [self callLoginAction];
    }else {
        [self registerUsersAccount];
    }
    
}

#pragma mark - User Login Action

- (void)callLoginAction {
    
    BOOL validStatus =  [self checkValidationForuser_LoginAccount];
    
    if (validStatus == YES) {
        
        self.loginType = @"InApp";
      NSDictionary *parametersDict  = [NSDictionary dictionaryWithObjectsAndKeys:self.emailTextField.text,@"email",self.passwordTextField.text,@"password",nil];
        [self loginActionMethod:parametersDict];
        
    }else {
        return;
    }
}

#pragma mark - Register User

- (void)registerUsersAccount {
    
    BOOL validStatus =  [self checkValidationForuser_RegisterAccount];
    if (validStatus == YES) {
        self.loginType = @"app";
        [self registerUserAccountMethod:^(id result, NSError *error){
            
            NSDictionary *dataDict = [result valueForKey:@"data"];

            if ([dataDict isKindOfClass:[NSDictionary class]]) {
                [self setUpUserInfo:dataDict];
            }
            NSString *existanceStr = [result valueForKey:@"message"];//["User already exist"];
                
            if ([existanceStr isEqualToString:@"User already exist"]) {
                [iBeautyUtility showAlertMessage:[result valueForKey:@"message"]];
            }else {
                [iBeautyUtility showAlertMessage:NSLocalizedString(@"Registered successfully. Please login to proceed", @"")];
                [self.navigationController popViewControllerAnimated:YES];
            }
        }];
    }else {
        return;
    }
}

#pragma mark - check Validations For Login

- (BOOL)checkValidationForuser_LoginAccount {
    
    if (self.emailTextField.text.length > 0  && ![iBeautyUtility isValidEmailAddress:self.emailTextField.text]) {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"Please enter valid email address", @"")];
        return NO ;
    }
    if (self.emailTextField.text.length < 1) {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"Please enter email address", @"")];
        return NO ;
    }
    if (self.passwordTextField.text.length < 1) {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"Please enter password", @"")];
        return NO ;
    }
    return YES;
}

#pragma mark - check Validations For Register

- (BOOL)checkValidationForuser_RegisterAccount {
    
    if (self.signup_userNameTxtField.text.length < 1) {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"Please enter user name", @"")];
        return NO ;
    }
    if (self.signup_emailTxtField.text.length > 0  && ![iBeautyUtility isValidEmailAddress:self.signup_emailTxtField.text]) {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"Please enter valid email address", @"")];
        return NO ;
    }
    if (self.signup_emailTxtField.text.length < 1) {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"Please enter email address", @"")];
        return NO ;
    }
    if (self.signup_passwordTxtField.text.length < 1) {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"Please enter password", @"")];
        return NO ;
    }
    return YES;
}

#pragma mark - Get access token

- (void)loginActionMethod:(NSDictionary*)parametersDict {
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_LOGIN];
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (APP_DEBUG_MODE)
        NSLog(@"parametersDict is %@",parametersDict);
    
    if (internetStatus != NotReachable) {
        
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonPostData:url params:parametersDict onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                
                @try {
                    if (completed) {
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSDictionary *userInfoDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE)
                            NSLog(@"User info is %@", userInfoDict);
                        
                        if ([userInfoDict valueForKey:@"token"]) {
                            [[NSUserDefaults standardUserDefaults]setValue:[userInfoDict valueForKey:@"token"] forKey:KAccessToken];
                            [[NSUserDefaults standardUserDefaults]setValue:self.loginType forKey:KLoginType];
                            [[NSUserDefaults standardUserDefaults]synchronize];
                            [self setUpUserInfo:[userInfoDict valueForKey:@"userInfo"]];
                        }else {
                            [iBeautyUtility showAlertMessage:NSLocalizedString(@"Wrong username or password", @"")];
                            [self emptyTextFields];
                        }
                    }else {
                        if (APP_DEBUG_MODE)
                            NSLog(@"error is %@", error.localizedDescription);
                        [SVProgressHUD dismiss];
                    }
                    
                } @catch (NSException *exception) {
                    //[iBeautyUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}

- (void)emptyTextFields {
    
    self.emailTextField.text = @"";
    self.passwordTextField.text = @"";
    [self.emailTextField becomeFirstResponder];
}

#pragma mark - Register Account Method

-(void)registerAccountMethod:(NSDictionary*)userInfo {
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_REGISTER];
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSDictionary *parametersDict  = [self getUserInfoDict:userInfo];
    
    if (APP_DEBUG_MODE)
        NSLog(@"parametersDict is %@",parametersDict);
    
    if (internetStatus != NotReachable) {
        
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonPostData:url params:parametersDict onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                
                @try {
                    if (completed) {
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSDictionary *userInfoDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE)
                            NSLog(@"User info is %@", userInfoDict);
                        
                        if ([userInfoDict valueForKey:@"token"]) {
                            [[NSUserDefaults standardUserDefaults]setValue:[userInfoDict valueForKey:@"token"] forKey:KAccessToken];
                            [[NSUserDefaults standardUserDefaults]setValue:self.loginType forKey:KLoginType];
                            [[NSUserDefaults standardUserDefaults]synchronize];
                            [self setUpUserInfo:[userInfoDict valueForKey:@"userInfo"]];
//                            [iBeautyUtility showAlertMessage:NSLocalizedString(@"Logged in successfully", @"")];
                        }else {
//                            [iBeautyUtility showAlertMessage:@"Something went wrong."];
                            [iBeautyUtility showAlertMessage:[userInfoDict valueForKey:@"message"]];
                        }

                    }else {
                        if (APP_DEBUG_MODE)
                            NSLog(@"error is %@", error.localizedDescription);
                        [SVProgressHUD dismiss];
                    }
                    
                } @catch (NSException *exception) {
                    //[iBeautyUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}

#pragma mark - Register Account Method

-(void)registerUserAccountMethod:(void(^)(id result, NSError *error))completion {
    
   // {"name":"","username":"","email":"","phone":"","password":"","birthday":"","accounttype":"","imageurl":"","fguserid":""}
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_REGISTER];
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    
    if (APP_DEBUG_MODE) { NSLog(@"accessToken is %@",accessToken); }
    
    NSDictionary *parametersDict  = [NSDictionary dictionaryWithObjectsAndKeys:accessToken,@"Authorization",self.signup_userNameTxtField.text,@"name",self.signup_userNameTxtField.text,@"username",@"",@"phone",self.signup_emailTxtField.text,@"email",self.signup_passwordTxtField.text,@"password",@"",@"birthday",self.loginType,@"accounttype",@"",@"imageurl",@"",@"fguserid",nil];
    
    if (APP_DEBUG_MODE)
        NSLog(@"parametersDict is %@",parametersDict);
    
    if (internetStatus != NotReachable) {
        
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonPostData:url params:parametersDict onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                
                @try {
                    if (completed) {
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSDictionary *userInfoDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE)
                            NSLog(@"User info is %@", userInfoDict);
                             completion (userInfoDict,nil);
                    }else {
                        if (APP_DEBUG_MODE)
                            NSLog(@"error is %@", error.localizedDescription);
                        [SVProgressHUD dismiss];
                    }
                    
                } @catch (NSException *exception) {
                    //[iBeautyUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}

#pragma mark - Get User ProfileInfo from FB/Google

-(NSDictionary*)getUserInfoDict:(NSDictionary *)userInfo {

    // {"name":"","username":"","email":"","phone":"","password":"","birthday":"","accounttype":"","imageurl":"","fguserid":""}

    NSString *name = [NSString stringWithFormat:@"%@",[iBeautyUtility trimString:[userInfo valueForKey:KProfileName]]];
    NSString *phone = @"";
    NSString *password = @"";
    NSString *birthday = @"";
    NSString *email = [NSString stringWithFormat:@"%@",[iBeautyUtility trimString:[userInfo valueForKey:KEmailId]]];
    NSString *imageURLString = [NSString stringWithFormat:@"%@",[iBeautyUtility trimString:[userInfo valueForKey:KProfileImageUrl]]];;
    NSString *accountType  = self.loginType;
    NSString *userID       = [NSString stringWithFormat:@"%@",[iBeautyUtility trimString:[userInfo valueForKey:KLoginIdentifier]]];

    NSDictionary *parametersDict  = [NSDictionary dictionaryWithObjectsAndKeys:name,@"name",name,@"username",phone,@"phone",email,@"email",password,@"password",birthday,@"birthday",accountType,@"accounttype",imageURLString,@"imageurl",userID,@"fguserid",nil];
    
    return parametersDict;
    
}

-(void)setUpUserInfo:(NSDictionary*)userInfoDict {
    
    NSDictionary *userProfileInfo = [self getUserProfileInfoByInAppLogin:userInfoDict];
    
    [self saveUserInfo:userProfileInfo];
    [self goTohomeScreen];
}

#pragma mark - Google Login

- (IBAction)googleLogin:(id)sender {
    
    AppDelegate *delegate = [AppDelegate sharedInstance];
    delegate.oberverStatus = NO;

    self.loginType = @"google";
    [[GIDSignIn sharedInstance] signIn];
    
}

- (void)showGoogleNotificationListener{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveGoogleAuthInfoNotification:) name:@"ToggleAuthUINotification" object:nil];
}
- (void)receiveGoogleAuthInfoNotification:(NSNotification*)notification {
    
    NSDictionary *userProfileInfo = notification.userInfo;
    if (APP_DEBUG_MODE)
        NSLog(@"User Profile Info%@",userProfileInfo);
    
    [self goToEnterEmailScreen:userProfileInfo type:1];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ToggleAuthUINotification" object:nil];
}

#pragma mark - Facebook Login

- (IBAction)facebookLogin:(id)sender {
    
    self.loginType = @"facebook";

    LoginManager *loginMananger = [[LoginManager alloc]init];
    [loginMananger loginWithFacebook:^(id result, NSError *error) {
        
        [SVProgressHUD dismiss];
        
        if (result) {
            if (APP_DEBUG_MODE)
                NSLog(@"Response is %@",result);
            
            [self goToEnterEmailScreen:result type:0];

        }else{
            if (APP_DEBUG_MODE)
                NSLog(@"error is %@",error.localizedDescription);
            [iBeautyUtility showAlertMessage:ERROR_MESSAGE];
        }
    }];
    
}

- (void)goToEnterEmailScreen:(NSDictionary*)result type:(NSInteger)type {
    
    self.profileInfoDict = result;
    NSString *emailString = [result valueForKey:@"email"];
    
    if (emailString.length>0) {

        if (type == 0) {
            NSDictionary *profileInfo = [self getUserProfileInfo:result];
            [self registerAccountMethod:profileInfo];
        }else {
            [self registerAccountMethod:result];
        }

    }else {
    
      EnterInformationController *infoController = [self.storyboard instantiateViewControllerWithIdentifier:kEnterInformationController];
      infoController.emailText = emailString;
        infoController.status = NO;
      infoController.modalPresentationStyle = UIModalPresentationFullScreen;
      [self presentViewController:infoController animated:YES completion:nil];
   }
}

- (void)goTohomeScreen {
    
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    MainViewController *mainVC = [self.storyboard instantiateViewControllerWithIdentifier:kMainViewController];
    [delegate setRootViewController:mainVC];
}

#pragma mark - Save Users Info

-(void)saveUserInfo:(NSDictionary*)profileInfo {
    
    [[NSUserDefaults standardUserDefaults]setObject:profileInfo forKey:KLoginInfo];
    [[NSUserDefaults standardUserDefaults]setValue:@"Success" forKey:KLoginStatus];
    [[NSUserDefaults standardUserDefaults]setValue:[profileInfo valueForKey:KUserId] forKey:KUserId];
    
}

#pragma mark - Go To ForgotPassword Screen


- (IBAction)goToForgotPasswordScreen:(id)sender {
    
    ForgotPasswordController *forgotPassController = [self.storyboard instantiateViewControllerWithIdentifier:kForgotPassController];
    [self.navigationController pushViewController:forgotPassController animated:YES];
    
}

#pragma mark - set up Tool Bar On Text fields

- (void)setupToolBarOnTextfields {
    
    self.toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50)];
    self.toolBar.barStyle = UIBarStyleDefault;
    [self.toolBar sizeToFit];
    [self.toolBar setBackgroundColor:[UIColor lightGrayColor]];
    [self buttonWithinToolbar];
    
    self.emailTextField.inputAccessoryView = self.toolBar;
    self.passwordTextField.inputAccessoryView = self.toolBar;
}
- (void)buttonWithinToolbar {
    
    self.doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", @"") style:UIBarButtonItemStyleDone target:self action:@selector(doneAction:)];
    
    UIBarButtonItem *flexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [self.toolBar setItems:@[flexibleItem,self.doneButton] animated:NO];
}

- (void)doneAction:(id)sender {
    
    [self.emailTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];

}

# pragma mark - Textfield Delegate Methods

-(BOOL) textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

# pragma mark - Get User Profiles Info Dict

- (NSDictionary*)getUserProfileInfo:(NSDictionary*)userInfo {

    NSString *email = [NSString stringWithFormat:@"%@",[iBeautyUtility trimString:[userInfo valueForKey:@"email"]]];
    NSString *userId = [NSString stringWithFormat:@"%@",[iBeautyUtility trimString:[userInfo valueForKey:@"id"]]];
    NSString *fullName = [NSString stringWithFormat:@"%@",[iBeautyUtility trimString:[userInfo valueForKey:@"name"]]];
    NSDictionary *pictureInfo = [[userInfo valueForKey:@"picture"]valueForKey:@"data"];
    NSString *imageURLString = [NSString stringWithFormat:@"%@",[iBeautyUtility trimString:[pictureInfo valueForKey:@"url"]]];
    NSString *loginType   = @"Facebook";

    NSDictionary *parametersDict   = [NSDictionary dictionaryWithObjectsAndKeys:fullName,KProfileName,imageURLString,KProfileImageUrl,loginType,KLoginType,userId,KLoginIdentifier,email,KEmailId,nil];

    return parametersDict;
}

- (NSDictionary*)getUserProfileInfoByInAppLogin:(NSDictionary*)userInfo {
    
    NSString *loginType = [NSString stringWithFormat:@"%@",@"app"];
    NSString *birthday  = [NSString stringWithFormat:@"%@",[iBeautyUtility trimString:[userInfo valueForKey:@"Birthday"]]];
    NSString *createdOn  = [NSString stringWithFormat:@"%@",[iBeautyUtility trimString:[userInfo valueForKey:@"CreatedOn"]]];
    NSString *email = [NSString stringWithFormat:@"%@",[iBeautyUtility trimString:[userInfo valueForKey:@"Email"]]];
    NSString *fbUserId = [NSString stringWithFormat:@"%@",[iBeautyUtility trimString:[userInfo valueForKey:@"FGUserId"]]];
    NSString *imageURLString = [NSString stringWithFormat:@"%@",[iBeautyUtility trimString:[userInfo valueForKey:@"ImageUrl"]]];
    NSString *fullName = [NSString stringWithFormat:@"%@",[iBeautyUtility trimString:[userInfo valueForKey:@"Name"]]];
    NSString *password = [NSString stringWithFormat:@"%@",[iBeautyUtility trimString:[userInfo valueForKey:@"Password"]]];
    NSString *phoneNo = [NSString stringWithFormat:@"%@",[iBeautyUtility trimString:[userInfo valueForKey:@"PhoneNumber"]]];
    NSString *userID = [NSString stringWithFormat:@"%@",[iBeautyUtility trimString:[userInfo valueForKey:@"UserId"]]];
    NSString *userName = [NSString stringWithFormat:@"%@",[iBeautyUtility trimString:[userInfo valueForKey:@"Username"]]];
    
    NSDictionary *parametersDict   = [NSDictionary dictionaryWithObjectsAndKeys:loginType,KLoginType,birthday,KBirthday,createdOn,KCreatedOn,email,KEmailId,fbUserId,KFBUserId,imageURLString,KProfileImageUrl,fullName,KProfileName,password,KPassword,phoneNo,KPhoneNumber,userID,KUserId,userName,KUserName,nil];
    
    return parametersDict;
}

#pragma mark - Setup UI for Apple Login

- (void)setupUI {
    
    if (@available(iOS 13.0, *)) {
    // Sign In With Apple Button
    ASAuthorizationAppleIDButton *appleIDButton = [ASAuthorizationAppleIDButton new];
        
    appleIDButton.frame =  CGRectMake(180.0, 5.0,40.0, 40.0);
        
    appleIDButton.cornerRadius = CGRectGetHeight(appleIDButton.frame) * 0.25;
    [self.socialLoginView addSubview:appleIDButton];
    [appleIDButton addTarget:self action:@selector(handleAuthrization:) forControlEvents:UIControlEventTouchUpInside];
    }
    
}

#pragma mark - Actions

- (void)handleAuthrization:(UIButton *)sender {
    if (@available(iOS 13.0, *)) {
        // A mechanism for generating requests to authenticate users based on their Apple ID.
        ASAuthorizationAppleIDProvider *appleIDProvider = [ASAuthorizationAppleIDProvider new];
        
        // Creates a new Apple ID authorization request.
        ASAuthorizationAppleIDRequest *request = appleIDProvider.createRequest;
        // The contact information to be requested from the user during authentication.
        request.requestedScopes = @[ASAuthorizationScopeFullName, ASAuthorizationScopeEmail];
        
        // A controller that manages authorization requests created by a provider.
        ASAuthorizationController *controller = [[ASAuthorizationController alloc] initWithAuthorizationRequests:@[request]];
        
        // A delegate that the authorization controller informs about the success or failure of an authorization attempt.
        controller.delegate = self;
        
        // A delegate that provides a display context in which the system can present an authorization interface to the user.
        controller.presentationContextProvider = self;
        
        // starts the authorization flows named during controller initialization.
        [controller performRequests];
    }
}


#pragma mark - Delegate

 - (void)authorizationController:(ASAuthorizationController *)controller didCompleteWithAuthorization:(ASAuthorization *)authorization  API_AVAILABLE(ios(13.0)){
    
    NSLog(@"%s", __FUNCTION__);
    NSLog(@"%@", controller);
    NSLog(@"%@", authorization);
    
    NSLog(@"authorization.credential：%@", authorization.credential);
    
    if ([authorization.credential isKindOfClass:[ASAuthorizationAppleIDCredential class]]) {
        // ASAuthorizationAppleIDCredential
        ASAuthorizationAppleIDCredential *appleIDCredential = authorization.credential;
        NSString *user = appleIDCredential.user;
        NSLog(@"user：%@", user);
        
        NSString *familyName = appleIDCredential.fullName.familyName;
        NSLog(@"familyName：%@", familyName);

        NSString *givenName = appleIDCredential.fullName.givenName;
        NSLog(@"givenName：%@", givenName);

        NSString *email = appleIDCredential.email;
        NSLog(@"email：%@", email);
        self.loginType = @"apple";
        
        NSDictionary *infoDict = [[NSDictionary alloc]init];
        [infoDict setValue:givenName forKey:KProfileName];
        [infoDict setValue:email forKey:KEmailId];
        [infoDict setValue:@"" forKey:KProfileImageUrl];
        [infoDict setValue:user forKey:KLoginIdentifier];

        if (givenName.length > 0 && email.length > 0) {
            [self registerAccountMethod:infoDict];
        }else {
            EnterInformationController *infoController = [self.storyboard instantiateViewControllerWithIdentifier:kEnterInformationController];
            infoController.emailText = email;
              infoController.status = NO;
            infoController.modalPresentationStyle = UIModalPresentationFullScreen;
            [self presentViewController:infoController animated:YES completion:nil];
        }
        
    }
     
}
 

- (void)authorizationController:(ASAuthorizationController *)controller didCompleteWithError:(NSError *)error  API_AVAILABLE(ios(13.0)){
    
    NSLog(@"%s", __FUNCTION__);
    NSLog(@"error ：%@", error);
    NSString *errorMsg = nil;
    switch (error.code) {
        case ASAuthorizationErrorCanceled:
            errorMsg = @"ASAuthorizationErrorCanceled";
            break;
        case ASAuthorizationErrorFailed:
            errorMsg = @"ASAuthorizationErrorFailed";
            break;
        case ASAuthorizationErrorInvalidResponse:
            errorMsg = @"ASAuthorizationErrorInvalidResponse";
            break;
        case ASAuthorizationErrorNotHandled:
            errorMsg = @"ASAuthorizationErrorNotHandled";
            break;
        case ASAuthorizationErrorUnknown:
            errorMsg = @"ASAuthorizationErrorUnknown";
            break;
    }
    if (error.localizedDescription) {
        if(APP_DEBUG_MODE) { NSLog(@"Error is %@",errorMsg); }
    }
    NSLog(@"controller requests：%@", controller.authorizationRequests);

}
 
//! Tells the delegate from which window it should present content to the user.
 - (ASPresentationAnchor)presentationAnchorForAuthorizationController:(ASAuthorizationController *)controller  API_AVAILABLE(ios(13.0)){
    
    NSLog(@"window：%s", __FUNCTION__);
    return self.view.window;
}

- (void)dealloc {
    
    if (@available(iOS 13.0, *)) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:ASAuthorizationAppleIDProviderCredentialRevokedNotification object:nil];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
