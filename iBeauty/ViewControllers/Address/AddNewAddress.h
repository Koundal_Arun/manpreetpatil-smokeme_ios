//
//  AddNewAddress.h
//  EShop
//
//  Created by App Innovation on 21/11/17.
//  Copyright © 2017 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddNewAddress : UIView

@property (nonatomic,weak) IBOutlet UIView    *backgrounView;
@property (nonatomic,weak) IBOutlet UIButton  *addNewAddress;

+ (instancetype)footerView;

@end
