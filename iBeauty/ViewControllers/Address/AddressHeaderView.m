//
//  AddressHeaderView.m
//  iBeauty
//
//  Created by App Innovation on 24/01/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import "AddressHeaderView.h"
#import "Constants.h"

@implementation AddressHeaderView


+ (instancetype)addressHeaderView:(ServicesCategoriesInfo*)infoObj {
    
    AddressHeaderView *tableHeaderView = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class])                                                                         owner:nil options:nil] firstObject];
    
    tableHeaderView.nameLabel.text = infoObj.categoryName;
    tableHeaderView.addressLabel.text = infoObj.location;
    
    NSString *languageCode =   [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];
    if ([languageCode isEqualToString:@"en"]) {
        tableHeaderView.nameLabel.textAlignment = NSTextAlignmentLeft;
        tableHeaderView.addressLabel.textAlignment = NSTextAlignmentLeft;
    }else if ([languageCode isEqualToString:@"ar"]) {
        tableHeaderView.nameLabel.textAlignment = NSTextAlignmentRight;
        tableHeaderView.addressLabel.textAlignment = NSTextAlignmentRight;
    }
    return tableHeaderView;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
