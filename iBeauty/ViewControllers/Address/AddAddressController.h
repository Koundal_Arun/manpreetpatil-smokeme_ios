//
//  AddAddressController.h
//  EShop
//
//  Created by App Innovation on 21/11/17.
//  Copyright © 2017 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddressModel.h"
#import "Constants.h"

@interface AddAddressController : UIViewController

@property(nonatomic,strong) AddressModel *addressInfoObj;
@property(nonatomic,assign) BOOL modeStatus;
@property(nonatomic,assign) BOOL backStatus;

@property (nonatomic,strong) NSMutableArray *serviceDetailsArray;
@property (nonatomic,strong) ServicesCategoriesInfo *categoryInfoObj;
@property (nonatomic,strong) NSString *serviceProvideAtString;
@property (nonatomic,strong) NSString *noteString;

@property (nonatomic,assign) BOOL screenStatus;
@property (nonatomic,assign) NSInteger pickStatus;

@property(nonatomic,assign) BOOL navigateStatus;
@property (nonatomic, strong) CLLocation *selectedLocation;
@property (nonatomic, strong) NSString *addressString;

@end
