//
//  AddressesViewController.h
//  EShop
//
//  Created by App Innovation on 28/11/17.
//  Copyright © 2017 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "OffersDetail.h"

@interface AddressesViewController : UIViewController

@property (nonatomic,strong)  NSMutableArray *addressListArray;

@property (nonatomic,strong) NSMutableArray *serviceDetailsArray;
@property (nonatomic,strong) ServicesCategoriesInfo *categoryInfoObj;

@property (nonatomic,strong) NSString *serviceProvideAtString;
@property (nonatomic,strong) NSString *noteString;

@property (nonatomic,strong) OffersDetail *offerInfoObj;
@property (nonatomic,assign) BOOL screenStatus;
@property (nonatomic,assign) NSInteger pickStatus;

@property (nonatomic,strong) NSMutableArray *offersDetailsArray;


@end
