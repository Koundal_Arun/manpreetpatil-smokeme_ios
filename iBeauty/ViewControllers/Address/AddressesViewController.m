//
//  AddressesViewController.m
//  EShop
//
//  Created by App Innovation on 28/11/17.
//  Copyright © 2017 App Innovation. All rights reserved.
//

#import "AddressesViewController.h"
#import "CommonClass.h"
#import "Constants.h"
#import "AddressCell.h"
#import "AddAddressController.h"
#import "AddNewAddress.h"
#import "BasketInfoObj.h"
#import "AddressHeaderView.h"
#import "ProvideServiceAt.h"
#import "SearchAddressViewController.h"
#import "AddNewAddressController.h"


static NSString *addressCellIdentifier  = @"addressCell";

@interface AddressesViewController ()

@property (nonatomic,weak) IBOutlet UITableView *addressInfo_tableView;

@property (nonatomic,assign) NSInteger selectedCell;

@property (nonatomic,assign) NSInteger selectedButtonTag;
@property (nonatomic,weak)   IBOutlet UIView *noAddressView;
@property (nonatomic,weak)   IBOutlet UIButton *addAddress;
@property (nonatomic,weak)   IBOutlet UIButton *continueButton;
@property (nonatomic,weak)   IBOutlet UINavigationItem *navigationItem;

@property (nonatomic,weak)   IBOutlet UIView *deliveryPickUpView;
@property (nonatomic,weak)   IBOutlet UIView *deliveryView;
@property (nonatomic,weak)   IBOutlet UIView *PickUpView;
@property (nonatomic,weak)   IBOutlet UIButton *pickUpButton;
@property (nonatomic,weak)   IBOutlet UIButton *deliveryButton;
@property (nonatomic,weak)   IBOutlet UILabel *deliveryLabel;
@property (nonatomic,weak)   IBOutlet UILabel *pickUpLabel;
@property (nonatomic,strong) NSString *productProvideAt;

@property (nonatomic,weak)  IBOutlet UINavigationBar *navBar;

@end

@implementation AddressesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setUpViews];
    [self setUpTableViewCell];
    self.selectedCell = 0;
    
    self.continueButton.frame = [iBeautyUtility setUpFrame:self.continueButton.frame];
    [iBeautyUtility setButtonCorderRound:self.continueButton];
    
    //[self.continueButton.layer insertSublayer:[iBeautyUtility setGradientColor_Buttons:self.continueButton] atIndex:0];

    [self setUpLocalizationViews];
    //[self setGradient];

}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self getAddressListFor_RegisteredUsers];
}

#pragma mark - Set Gradient

- (void)setGradient {
    
    self.navBar.frame = [iBeautyUtility setUpFrame:self.navBar.frame];
    CAGradientLayer *gradient = [iBeautyUtility setNavigationGradient:self.navBar];
    [self.navBar setBackgroundImage:[iBeautyUtility imageFromLayer:gradient] forBarMetrics:UIBarMetricsDefault];
}

#pragma mark - SetUp Views

- (void)setUpViews {
    self.addressInfo_tableView.translatesAutoresizingMaskIntoConstraints = YES;
    self.deliveryPickUpView.translatesAutoresizingMaskIntoConstraints = YES;

    self.deliveryPickUpView.hidden = YES;
    
    if (IS_IPHONE_X || IS_IPHONE_XS_MAX) {
        
        self.addressInfo_tableView.frame = CGRectMake(self.addressInfo_tableView.frame.origin.x, 88.0f, SCREEN_WIDTH, SCREEN_HEIGHT-88.0f-59.0f-34.0f);

    }else {
        self.addressInfo_tableView.frame = CGRectMake(self.addressInfo_tableView.frame.origin.x, 64.0f, SCREEN_WIDTH, SCREEN_HEIGHT-64.0f-59.0f);
    }
    //[self.addressInfo_tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];

}


- (void)setUpLocalizationViews {
    
    self.navigationItem.title = NSLocalizedString(@"My Addresses", @"");
    self.deliveryLabel.text = NSLocalizedString(@"Delivery", @"");
    self.pickUpLabel.text = NSLocalizedString(@"Pickup", @"");
    [self.continueButton setTitle:NSLocalizedString(@"Continue", @"") forState:UIControlStateNormal];

}

#pragma mark - back button

- (IBAction)BackButton:(UIBarButtonItem*)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)DeliveryAction:(UIButton*)sender {
    
    [self deliveryViewSetUp];

}

- (void)deliveryViewSetUp {
    
    [iBeautyUtility setPickUpDeliveryViewWithBorderColor:self.deliveryView];
    [iBeautyUtility setPickUpDeliveryViewWithoutBorderColor:self.PickUpView];
    self.selectedButtonTag = 1;
    self.addressInfo_tableView.tableHeaderView = nil;
    [self setUpFooterView];
    [self setUpViewWhen_NoAddresses];
    [self.addressInfo_tableView reloadData];
    self.productProvideAt = @"Delivery";
}

- (IBAction)PickupAction:(UIButton*)sender {
    
    [self pickUPViewSetUp];

}

- (void)pickUPViewSetUp {
    
    [iBeautyUtility setPickUpDeliveryViewWithBorderColor:self.PickUpView];
    [iBeautyUtility setPickUpDeliveryViewWithoutBorderColor:self.deliveryView];
    self.selectedButtonTag = 2;
    self.addressInfo_tableView.tableFooterView = nil;
    [self setUpHeaderView];
    [self.addressInfo_tableView setHidden:NO];
    [self.noAddressView removeFromSuperview];
    [self.addressInfo_tableView reloadData];
    self.productProvideAt = @"Pickup";
}

#pragma mark - set Up Table Cell

- (void)setUpTableViewCell {
    
    [self.addressInfo_tableView registerNib:[UINib nibWithNibName:@"AddressCell" bundle:nil] forCellReuseIdentifier:addressCellIdentifier];
    [self setUpFooterView];
    
}
- (void)setUpFooterView {
    
    AddNewAddress *tableFooter = [AddNewAddress footerView];
    [tableFooter.addNewAddress addTarget:self action:@selector(addAddress:) forControlEvents:UIControlEventTouchUpInside];
    self.addressInfo_tableView.tableFooterView = tableFooter;
    [iBeautyUtility setViewBorderRound:tableFooter.backgrounView];
}

- (void)setUpHeaderView {
    
    AddressHeaderView *tableHeader = [AddressHeaderView addressHeaderView:self.categoryInfoObj];
    self.addressInfo_tableView.tableHeaderView = tableHeader;
    [iBeautyUtility setViewBorderRound:tableHeader.bkgView];
}

#pragma mark - Navigate to add new address screen

- (IBAction)addNewAddress:(UIBarButtonItem*)sender {
    
    AddNewAddressController *addressView = [self.storyboard instantiateViewControllerWithIdentifier:kAddNewAddressController];
    addressView.modeStatus = NO;
    [self.navigationController pushViewController:addressView animated:YES];
//    [self presentViewController:addressView animated:YES completion:nil];
}

#pragma mark - Get list of addresss

- (void)getAddressListFor_RegisteredUsers {
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_GET_ADDRESSES];
    
    //NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    
    NSDictionary *profileInfo = [[NSUserDefaults standardUserDefaults]valueForKey:KLoginInfo];
    NSString *userID = [profileInfo valueForKey:@"UserID"];
    
    NSString *urlString = [NSString stringWithFormat:@"%@/%@",url,userID];
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable) {
        // [SVProgressHUD showWithStatus:KLoadingMessage];
        [handler jsonGETData:urlString parameters:nil onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                // [SVProgressHUD dismiss];
                @try {
                    if (completed) {

                        self.addressListArray  = [AddressModel getAddressListObj:completed screenStatus:NO];
                        [self setUpViewWhen_NoAddresses];
                        [self.addressInfo_tableView reloadData];
                    }else{
                    }
                } @catch (NSException *exception) {
                    //[EShopUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                } @finally {
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}

#pragma mark - Set Up empty screen when no address

- (void)setUpViewWhen_NoAddresses {
    
    if (self.addressListArray.count > 0) {
        
        [self.addressInfo_tableView setHidden:NO];
        [self.noAddressView removeFromSuperview];
    }else {
        
        [self.view addSubview:self.noAddressView];
        [self.noAddressView setCenter:self.view.center];
        [iBeautyUtility setButtonCorderRound:self.addAddress];
        [self.addAddress addTarget:self action:@selector(addAddress:) forControlEvents:UIControlEventTouchUpInside];
        [self.addressInfo_tableView setHidden:YES];
    }

}
- (void)addAddress:(UIButton*)sender {
    
    AddNewAddressController *addressView = [self.storyboard instantiateViewControllerWithIdentifier:kAddNewAddressController];
    addressView.modeStatus = NO;
    [self.navigationController pushViewController:addressView animated:YES];
//    [self presentViewController:addressView animated:YES completion:nil];
}


#pragma mark -UITableViewDataSouce

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.addressListArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    AddressCell *cell = [tableView dequeueReusableCellWithIdentifier:addressCellIdentifier];
    AddressModel *infoObj = self.addressListArray[indexPath.row];
    
    if (self.selectedCell == indexPath.row) {
        
        cell.selectImageView.image = [UIImage imageNamed:@"check_box"];
        
    }else {
        cell.selectImageView.image = [UIImage imageNamed:@"un-check_box"];
    }
    
    [cell.editButton addTarget:self action:@selector(editAddress:) forControlEvents:UIControlEventTouchUpInside];
    cell.editButton.tag = indexPath.row;
    [cell.deleteButton addTarget:self action:@selector(deleteAddress:) forControlEvents:UIControlEventTouchUpInside];
    cell.deleteButton.tag = indexPath.row;
    
    [cell configureAddressCell:infoObj];
    return cell;
}


#pragma mark - UITableView Delegate Methods


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    AddressModel *infoObj = self.addressListArray[indexPath.row];

    return infoObj.cellHeight;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.addressInfo_tableView deselectRowAtIndexPath:indexPath animated:NO];
    self.selectedCell = indexPath.row;
    [self.addressInfo_tableView reloadData];
}

#pragma mark - edit existing address

- (void)editAddress:(UIButton*)sender {
    
    AddressModel *infoObj = self.addressListArray[sender.tag];
    AddNewAddressController *addressView = [self.storyboard instantiateViewControllerWithIdentifier:kAddNewAddressController];
    addressView.addressInfoObj = infoObj;
    addressView.modeStatus = YES;
    [self.navigationController pushViewController:addressView animated:YES];

}

#pragma mark - delete existing address

- (void)deleteAddress:(UIButton*)sender {
    
    AddressModel *infoObj = self.addressListArray[sender.tag];
    [self showAlertDialogForDeleteAddress:infoObj];
}

- (void)DeleteExistingAddressTo_registered_Users:(AddressModel*)addressInfoObj {
    
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
//    NSDictionary *profileInfo = [[NSUserDefaults standardUserDefaults]valueForKey:KLoginInfo];
//    NSString *userID = [profileInfo valueForKey:@"UserID"];
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_DELETE_ADDRESS];
    
    url = [NSString stringWithFormat:@"%@/%@/%@",url,addressInfoObj.address_Id,accessToken];
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];

    
    if (internetStatus != NotReachable) {
        [SVProgressHUD showWithStatus:KLoadingMessage];
        
        [handler jsonDELETE_DATA:url header:accessToken parameters:nil onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                @try {
                    if (completed) {
                        if (APP_DEBUG_MODE) { NSLog(@"dict is %@",completed); }
                        [self.addressListArray removeObject:addressInfoObj];
                        [self setUpViewWhen_NoAddresses];
                        [self.addressInfo_tableView reloadData];
                    }
                } @catch (NSException *exception) {
                    //[EShopUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}

#pragma mark - Methods and Alert Diaologs for handling events

- (void)showAlertDialogForDeleteAddress:(AddressModel *)infoObj {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:NSLocalizedString(@"Are you sure? You want to Delete", @"")
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"NO", @"")
                                                           style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                   {
                                       [self dismissViewControllerAnimated:YES completion:nil];
                                   }];
    
    UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"YES", @"")
                                                           style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                   {
                                       [self DeleteExistingAddressTo_registered_Users:infoObj];
                                   }];
    [alert addAction:cancelAction];
    [alert addAction:deleteAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (IBAction)continueToProceed:(UIButton*)sender {
    
    if (self.addressListArray.count > 0) {
        [self updateBasketFor_customerAddress];
    }else {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"Please add address", @"")];
    }
    
}


#pragma mark - Update Basket for Customer Address

- (void)updateBasketFor_customerAddress {
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_UPDATE_BASKET];
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSDictionary *dictInfo = [self getParametersInfo_For_UpdateNoteAndServiceProvideAT];
    
    if (internetStatus != NotReachable) {
        
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonPostData:url params:dictInfo onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                
                @try {
                    if (completed) {
                        
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSMutableArray *tempInfoArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE)
                            NSLog(@"Temp Info Array is %@", tempInfoArray);
//                        BasketInfoObj *infoObj = [BasketInfoObj getSharedInstance];
//                        NSMutableArray *basketInfoArray = [infoObj basketDetailInfoObject:tempInfoArray];
//                        if (APP_DEBUG_MODE)
//                         NSLog(@"Response Array is %@",basketInfoArray);
                        if (self.serviceDetailsArray.count>0) {
                            [self continueToGetPricing];
                        }

                    }else {
                        if (APP_DEBUG_MODE) { NSLog(@"error is %@", error.localizedDescription);}
                    }
                    
                } @catch (NSException *exception) {
                    [iBeautyUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}

#pragma mark - Get pricing

- (void)continueToGetPricing {
    
    PriceCartViewController *priceCartObj = [self.storyboard instantiateViewControllerWithIdentifier:kPriceCartViewController];
    priceCartObj.serviceDetailsArray = self.serviceDetailsArray;
    priceCartObj.categoryInfoObj = self.categoryInfoObj;
    if ([self.serviceProvideAtString isEqualToString:@"Home"]) {
        AddressModel *addressInfoObj = self.addressListArray[self.selectedCell];
        priceCartObj.addressModel = addressInfoObj;
    }
    priceCartObj.screenStatus = self.screenStatus;
    [self.navigationController pushViewController:priceCartObj animated:YES];

}


- (NSMutableDictionary*)getParametersInfo_For_UpdateNoteAndServiceProvideAT {
    
    NSMutableDictionary *basketDict = [[NSMutableDictionary alloc]init];
    NSString *businessId = self.categoryInfoObj.Id;
    [basketDict setValue:businessId forKey:@"BusinessId"];
    
    NSString *customerID = [[NSUserDefaults standardUserDefaults]valueForKey:KUserId];
    [basketDict setValue:customerID forKey:@"CustomerId"];
    
    NSString *basketId = [[NSUserDefaults standardUserDefaults]valueForKey:@"BasketId"];
    [basketDict setObject:basketId forKey:@"BasketId"];
    
    NSMutableArray *appointmentsArray = [NSMutableArray new];
    
    for (BasketInfoObj *detailInfo in self.serviceDetailsArray) {
        
        NSMutableDictionary *appointmentDict = [[NSMutableDictionary alloc]init];
        [appointmentDict setValue:detailInfo.serviceId forKey:@"ServiceId"];
        [appointmentDict setValue:detailInfo.startDateTime forKey:@"StartDateTime"];
        [appointmentDict setValue:detailInfo.endDateTime forKey:@"EndDateTime"];
        [appointmentDict setValue:detailInfo.staffInfo.staffId forKey:@"StaffId"];
        [appointmentDict setValue:detailInfo.appointmentId forKey:@"Id"];
        [appointmentsArray addObject:appointmentDict];
    }
    [basketDict setObject:appointmentsArray forKey:@"UpdatedAppointments"];
    [basketDict setObject:@"" forKey:@"NewAppointments"];
    [basketDict setObject:@"" forKey:@"RemovedAppointments"];
    [basketDict setObject:@"" forKey:@"Products"];
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    [basketDict setValue:accessToken forKey:@"Authorization"];
    [basketDict setValue:self.serviceProvideAtString forKey:@"ServiceProvideAt"];
    [basketDict setValue:self.noteString forKey:@"Note"];
    
    NSString *offerId = @"";
    if (self.offerInfoObj) {
        offerId = self.offerInfoObj.offerId;
    }

    [basketDict setValue:offerId forKey:@"OfferId"];
    
    NSDictionary *addressDict = [self getDictInfo_Delivery];
    
    [basketDict setObject:addressDict forKey:@"CustomerAddress"];
    
    return basketDict;
    
}


- (NSDictionary*)getDictInfo_Delivery {
    
    AddressModel *addressInfoObj = self.addressListArray[self.selectedCell];
    
    NSString *nameStr = [iBeautyUtility trimString:addressInfoObj.name];
    NSString *contactNoStr = [iBeautyUtility trimString:addressInfoObj.contactNumber];
    NSString *userID = [[NSUserDefaults standardUserDefaults]valueForKey:KUserId];
    NSString *addressStr = [iBeautyUtility trimString:addressInfoObj.address];
    NSString *latStr = [iBeautyUtility trimString:addressInfoObj.latitude];
    NSString *longStr = [iBeautyUtility trimString:addressInfoObj.longitude];
    NSString *emailStr = [iBeautyUtility trimString:addressInfoObj.email];
    
    NSDictionary *userDetailDict = [[NSDictionary alloc]init];
    
    userDetailDict = [NSDictionary dictionaryWithObjectsAndKeys:userID,@"UserId",addressInfoObj.address_Id,@"Id", userID,@"UserId",nameStr,@"UserName",contactNoStr,@"ContactNumber",addressStr,@"Address",latStr,@"Latitude",longStr,@"Longitude",emailStr,@"Email",nil];
    
    return userDetailDict;
    
}

- (NSDictionary*)getDictInfo_PickUp {
    
    NSDictionary *profileInfo = [[NSUserDefaults standardUserDefaults]valueForKey:KLoginInfo];
    
    NSString *nameStr = [iBeautyUtility trimString:[profileInfo valueForKey:@"Name"]];
    NSString *contactNoStr = @"";
    if ([profileInfo valueForKey:@"PhoneNumber"]  != nil && ![[profileInfo valueForKey:@"PhoneNumber"] isEqual:[NSNull null]]) {
        contactNoStr = [iBeautyUtility trimString:[profileInfo valueForKey:@"PhoneNumber"]];
    }
    NSString *userID = [iBeautyUtility trimString:[profileInfo valueForKey:@"UserID"]];
    
    NSString *addressStr = @"";
    AppDelegate *degate = [AppDelegate sharedInstance];
    NSString *latStr = [iBeautyUtility trimString:degate.latitude];
    NSString *longStr = [iBeautyUtility trimString:degate.longitude];
    NSString *emailStr = @"";
    if ([profileInfo valueForKey:@"email"]  != nil && ![[profileInfo valueForKey:@"email"] isEqual:[NSNull null]]) {
        emailStr = [iBeautyUtility trimString:[profileInfo valueForKey:@"email"]];
    }
    
    NSDictionary *userDetailDict = [[NSDictionary alloc]init];
    
    userDetailDict = [NSDictionary dictionaryWithObjectsAndKeys:userID,@"UserId",@"",@"Id", nameStr,@"UserName",contactNoStr,@"ContactNumber",addressStr,@"Address",latStr,@"Latitude",longStr,@"Longitude",emailStr,@"Email",nil];
    
    return userDetailDict;
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
