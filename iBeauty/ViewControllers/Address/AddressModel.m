//
//  AddressModel.m
//  EShop
//
//  Created by App Innovation on 20/11/17.
//  Copyright © 2017 App Innovation. All rights reserved.
//

#import "AddressModel.h"
#import "iBeautyUtility.h"
#import "UILabel+FlexibleWidHeight.h"
#import "NSString+iBeautyString.h"


#define kContactNumber          @"ContactNumber"
#define kId                     @"Id"
#define kStatus                 @"Status"
#define kUseriD                 @"UserId"
#define kUsername               @"UserName"
#define kAddress                @"Address"
#define kLatitude               @"Latitude"
#define kLongitude              @"Longitude"
#define kUserEmail              @"Email"
#define kAddressTitle           @"AddressTitle"


@implementation AddressModel

+ (AddressModel *) getObjectWithJSONInfo:(NSDictionary *)jsonDict screenStatus:(BOOL)status {
    
    AddressModel *infoObj = [[AddressModel alloc] init];
    
    infoObj.contactNumber  = [iBeautyUtility trimString:[jsonDict valueForKey:kContactNumber]];
    if (APP_DEBUG_MODE) { NSLog(@"Contact number is %@",infoObj.contactNumber); }

    infoObj.address_Id  = [jsonDict valueForKey:kId];
    infoObj.userID  = [jsonDict valueForKey:kUseriD];
    infoObj.name  = [jsonDict valueForKey:kUsername];
    infoObj.status  = [[jsonDict valueForKey:kStatus]integerValue];
    infoObj.latitude  = [jsonDict valueForKey:kLatitude];
    infoObj.longitude  = [jsonDict valueForKey:kLongitude];
    infoObj.address  = [jsonDict valueForKey:kAddress];
    infoObj.email  = [jsonDict valueForKey:kUserEmail];
    infoObj.addressTitle  = [jsonDict valueForKey:kAddressTitle];

    NSString *completeAddress = [iBeautyUtility trimString:[NSString parseCompleteAddress:infoObj]];
    infoObj.completeAddress = [iBeautyUtility trimString:completeAddress];
    if (APP_DEBUG_MODE) {NSLog(@"Address is  %@",infoObj.contactNumber);}

    if (status == NO) {
        infoObj.cellHeight = [AddressModel calculateCellHeightAccordingToAddress:infoObj.address name:infoObj.name];
    }else {
        infoObj.cellHeight = [AddressModel calculateCellHeightAccordingToProfileScreen:infoObj.address name:infoObj.name];
    }

    return infoObj;
}

+ (NSMutableArray *)getAddressListObj:(NSMutableArray *)jsonResponse screenStatus:(BOOL)status {
    
    NSMutableArray *addressArray = [NSMutableArray array];
    
    for (NSDictionary *dict in jsonResponse) {
        AddressModel *modalObj = [AddressModel getObjectWithJSONInfo:dict screenStatus:status];
        [addressArray addObject:modalObj];
    }
    return [NSMutableArray arrayWithArray:addressArray];
    
}

+ (float)calculateCellHeightAccordingToAddress:(NSString*)completeAddress name:(NSString*)name {
    
    float height = 0.0f , addressHeight = 0.0f;
    
    if (name.length > 0) {
        addressHeight = [UIScreen mainScreen].bounds.size.width- 62.0f;
        height = [UILabel heightOfTextForString:completeAddress andFont:[UIFont fontWithName:@"SegoeUI" size:15.0] maxSize:CGSizeMake(addressHeight, MAXFLOAT)];
        height = height + 98.0f;
    }else {
        addressHeight = [UIScreen mainScreen].bounds.size.width- 125.0f;
        height = [UILabel heightOfTextForString:completeAddress andFont:[UIFont fontWithName:@"SegoeUI" size:15.0] maxSize:CGSizeMake(addressHeight, MAXFLOAT)];
        height = height + 40.0f;
    }
    return height;
}

+ (float)calculateCellHeightAccordingToProfileScreen:(NSString*)completeAddress name:(NSString*)name {
    
    float height = 0.0f;
    height = [UILabel heightOfTextForString:completeAddress andFont:[UIFont fontWithName:@"SegoeUI" size:15.0] maxSize:CGSizeMake([UIScreen mainScreen].bounds.size.width-118.0f, MAXFLOAT)];

    if (name.length > 0) {
        height = height + 65.0f;
    }else {
        height = height + 40.0f;
    }
    return height;
    
}

#pragma mark - Get Current Address

+ (NSMutableArray *)getCurrentAddressListObj:(NSMutableArray *)jsonResponse {
    
    NSMutableArray *addressArray = [NSMutableArray array];
    
    for (NSDictionary *dict in jsonResponse) {
        AddressModel *modalObj = [AddressModel getAddressObject:dict];
        [addressArray addObject:modalObj];
    }
    return [NSMutableArray arrayWithArray:addressArray];
}

+ (AddressModel *) getAddressObject:(NSDictionary *)jsonDict {
    
    AddressModel *infoObj = [[AddressModel alloc] init];
    
    infoObj.contactNumber  = [iBeautyUtility trimString:[jsonDict valueForKey:kContactNumber]];
    if (APP_DEBUG_MODE) { NSLog(@"Contact number is %@",infoObj.contactNumber); }
    
    infoObj.address_Id  = [jsonDict valueForKey:kId];
    infoObj.userID  = [jsonDict valueForKey:kUseriD];
    infoObj.name  = [jsonDict valueForKey:kUsername];
    infoObj.status  = [[jsonDict valueForKey:kStatus]integerValue];
    infoObj.latitude  = [jsonDict valueForKey:kLatitude];
    infoObj.longitude  = [jsonDict valueForKey:kLongitude];
    infoObj.address  = [jsonDict valueForKey:kAddress];
    infoObj.email  = [jsonDict valueForKey:kUserEmail];
    infoObj.addressTitle  = [jsonDict valueForKey:@"AddressTitle"];

    NSString *completeAddress = [iBeautyUtility trimString:[NSString parseCompleteAddress:infoObj]];
    infoObj.completeAddress = [iBeautyUtility trimString:completeAddress];
    if (APP_DEBUG_MODE) {NSLog(@"Address is  %@",infoObj.contactNumber);}
    
    float height = [UILabel heightOfTextForString:infoObj.address andFont:[UIFont fontWithName:@"SegoeUI" size:15.0] maxSize:CGSizeMake([UIScreen mainScreen].bounds.size.width-60.0f, MAXFLOAT)];
    
    infoObj.cellHeight = height + 78.0f;

    return infoObj;
}

@end
