//
//  AddressModel.h
//  EShop
//
//  Created by App Innovation on 20/11/17.
//  Copyright © 2017 App Innovation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AddressModel : NSObject


@property (copy, nonatomic) NSString *address_Id;
@property (copy, nonatomic) NSString *city;
@property (copy, nonatomic) NSString *locality;
@property (copy, nonatomic) NSString *flatNo;
@property (copy, nonatomic) NSString *pinCode;
@property (copy, nonatomic) NSString *state;
@property (copy, nonatomic) NSString *landmark;
@property (copy, nonatomic) NSString *country;
@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *contactNumber;
@property (copy, nonatomic) NSString *completeAddress;
@property (copy, nonatomic) NSString *mobileNumber;
@property (copy, nonatomic) NSString *userID;
@property (assign, nonatomic) float  cellHeight;
@property (assign, nonatomic) NSInteger  status;
@property (strong, nonatomic) NSString *latitude;
@property (strong, nonatomic) NSString *longitude;
@property (copy, nonatomic)   NSString *address;
@property (copy, nonatomic)   NSString *email;
@property (strong, nonatomic) NSString *addressTitle;

+ (NSMutableArray *)getAddressListObj:(NSMutableArray *)jsonResponse screenStatus:(BOOL)status;
+ (AddressModel *) getObjectWithJSONInfo:(NSDictionary *)jsonDict screenStatus:(BOOL)status;

+ (NSMutableArray *)getCurrentAddressListObj:(NSMutableArray *)jsonResponse; 

@end
