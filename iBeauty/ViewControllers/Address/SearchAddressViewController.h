//
//  SearchAddressViewController.h
//  iBeauty
//
//  Created by App Innovation on 18/03/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Mapkit/MapKit.h>
#import "Constants.h"

NS_ASSUME_NONNULL_BEGIN

@interface SearchAddressViewController : UIViewController

@property(nonatomic,strong) AddressModel *addressInfoObj;
@property(nonatomic,assign) BOOL modeStatus;
@property(nonatomic,assign) BOOL backStatus;

@property (nonatomic,strong) NSMutableArray *serviceDetailsArray;
@property (nonatomic,strong) ServicesCategoriesInfo *categoryInfoObj;
@property (nonatomic,strong) NSString *serviceProvideAtString;
@property (nonatomic,strong) NSString *noteString;

@property (nonatomic,assign) BOOL screenStatus;
@property (nonatomic,assign) NSInteger pickStatus;
@property(nonatomic,assign) BOOL navigateStatus;

@property(nonatomic,strong) NSString *latitude;
@property(nonatomic,strong) NSString *longitude;


@end

NS_ASSUME_NONNULL_END
