//
//  AddNewAddress.m
//  EShop
//
//  Created by App Innovation on 21/11/17.
//  Copyright © 2017 App Innovation. All rights reserved.
//

#import "AddNewAddress.h"

@implementation AddNewAddress


+ (instancetype)footerView {
    
    AddNewAddress *tableFooterView = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class])                                                                         owner:nil options:nil] firstObject];
    
    [tableFooterView.addNewAddress setTitle:NSLocalizedString(@"+ Add New Address", @"") forState:UIControlStateNormal];
    
   return tableFooterView;
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
