//
//  AddAddressController.m
//  EShop
//
//  Created by App Innovation on 21/11/17.
//  Copyright © 2017 App Innovation. All rights reserved.
//

#import "AddAddressController.h"
#import "CommonClass.h"
#import "Constants.h"
#import "BasketInfoObj.h"
#import "AddressesViewController.h"
#import "AppDelegate.h"
#import "SearchAddressViewController.h"
#import "MyProfileViewController.h"

@import MapKit;

@interface AddAddressController()<MKMapViewDelegate,UITextFieldDelegate,UITextViewDelegate>

@property (nonatomic,weak) IBOutlet UILabel *city_Label;
@property (nonatomic,weak) IBOutlet UILabel *locality_Label;
@property (nonatomic,weak) IBOutlet UILabel *flatNo_Label;
@property (nonatomic,weak) IBOutlet UILabel *pinCode_Label;
@property (nonatomic,weak) IBOutlet UILabel *state_Label;
@property (nonatomic,weak) IBOutlet UILabel *country_Label;
@property (nonatomic,weak) IBOutlet UILabel *mobileNo_Label;

@property (nonatomic,weak) IBOutlet UILabel *name_Label;
@property (nonatomic,weak) IBOutlet UILabel *email_Label;

@property (nonatomic,weak) IBOutlet UITextField *city_Textfield;
@property (nonatomic,weak) IBOutlet UITextField *locality_Textfield;
@property (nonatomic,weak) IBOutlet UITextField *flatNo_Textfield;
@property (nonatomic,weak) IBOutlet UITextField *pinCode_Textfield;
@property (nonatomic,weak) IBOutlet UITextField *state_Textfield;
@property (nonatomic,weak) IBOutlet UITextField *country_Textfield;
@property (nonatomic,weak) IBOutlet UITextField *mobileNo_Textfield;

@property (nonatomic,weak) IBOutlet UITextField *name_Textfield;
@property (nonatomic,weak) IBOutlet UITextField *email_Textfield;

@property (nonatomic,weak) IBOutlet UIView *name_View;
@property (nonatomic,weak) IBOutlet UIView *contactNo_View;
@property (nonatomic,weak) IBOutlet UIView *flatNo_View;
@property (nonatomic,weak) IBOutlet UIView *locality_View;
@property (nonatomic,weak) IBOutlet UIView *statePinCode_View;
@property (nonatomic,weak) IBOutlet UIView *country_View;
@property (nonatomic,weak) IBOutlet UIView *city_View;
@property (nonatomic,weak) IBOutlet UIView *mobileNo_View;
@property (nonatomic,weak) IBOutlet UIView *state_View;
@property (nonatomic,weak) IBOutlet UIView *pinCode_View;

@property (nonatomic,strong) NSDictionary *userDetailDict;

@property (nonatomic,weak) IBOutlet UIScrollView *scrollView;

@property (nonatomic,weak) IBOutlet UIButton *pickCurrentAddressButton;

@property (nonatomic,strong) AppDelegate *delegate;

@property UIToolbar *toolBar;
@property (nonatomic, strong) UIBarButtonItem   *doneButton;

@property (nonatomic,weak) IBOutlet UINavigationItem *navigationItem;
@property (nonatomic,weak) IBOutlet UIBarButtonItem  *cancelButton;
@property (nonatomic,weak) IBOutlet UIBarButtonItem  *saveAddress;

@property (nonatomic,weak) IBOutlet UITextView *addressTextView;
@property (nonatomic,weak) IBOutlet UITextField *contactNumberTextField;
@property (nonatomic,weak) IBOutlet UILabel *addAddressLabel;
@property (nonatomic,weak) IBOutlet UILabel *contactNumberLabel;
@property (nonatomic,weak) IBOutlet UILabel *placeHolderLabel;

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) NSString *latString;
@property (nonatomic, strong) NSString *longString;
@property (weak, nonatomic) IBOutlet UIView *currentView;

@property (nonatomic, assign) BOOL searchStatus;
@property (nonatomic,weak)  IBOutlet UINavigationBar *navBar;

- (IBAction)saveAddress:(id)sender;

@end


@implementation AddAddressController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.delegate = [AppDelegate sharedInstance];

    [self setupFieldsViewRound];

    [self getAddressFromMapViewCenterCoordinates:self.selectedLocation.coordinate.latitude log:self.selectedLocation.coordinate.longitude];

    [self setupToolBarOnTextfields];
    [self setUpLocalizationViews];
    [self setUpTextFieldsCursorAccordingToLanguage];
    [self setUpTextViewCursorAccordingToLanguage];
    
    if (self.modeStatus == YES) {
        [self setUpTextFieldsValues_WhenEditAddress];
    }else {
        
        NSDictionary *profileInfo = [[NSUserDefaults standardUserDefaults]valueForKey:KLoginInfo];
        
        if ([profileInfo valueForKey:@"email"]) {
            self.email_Textfield.text = [iBeautyUtility trimString:[profileInfo valueForKey:@"email"]];
        }
        if ([profileInfo valueForKey:@"Name"]) {
            self.name_Textfield.text = [iBeautyUtility trimString:[profileInfo valueForKey:@"Name"]];
        }
    }
    
//    [self addObserver];
    //[self setGradient];
}

#pragma mark - Set Gradient

- (void)setGradient {
    
    self.navBar.frame = [iBeautyUtility setUpFrame:self.navBar.frame];
    CAGradientLayer *gradient = [iBeautyUtility setNavigationGradient:self.navBar];
    [self.navBar setBackgroundImage:[iBeautyUtility imageFromLayer:gradient] forBarMetrics:UIBarMetricsDefault];
}

- (void)setUpLocalizationViews {
    
    self.navigationItem.title = NSLocalizedString(@"Add Address", @"");
    [self.cancelButton setTitle:NSLocalizedString(@"Back", @"")];

}

- (void)setUpMapViewLocation:(double)lat log:(double)log address:(NSString*)address {
    
    self.mapView.mapType = MKMapTypeStandard;
    self.mapView.delegate = self;
    [self.mapView setShowsUserLocation:YES];
    [self getServiceProviderLocation:lat longitude:log];
    [self addAnnotationInMapView:lat longitude:log address:address];
    
}


- (void)getServiceProviderLocation:(double)lat longitude:(double)log {
    
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(lat,log);
    MKCoordinateSpan span = MKCoordinateSpanMake(0.1, 0.1);
    MKCoordinateRegion region = {coordinate, span};
    region = [self.mapView regionThatFits:region];
    [self.mapView setRegion:region];
    
    self.latString = [NSString stringWithFormat:@"%@",[NSNumber numberWithDouble:coordinate.latitude]];
    self.longString = [NSString stringWithFormat:@"%@",[NSNumber numberWithDouble:coordinate.longitude]];
}

- (void)addAnnotationInMapView:(double)lat longitude:(double)log address:(NSString*)address {
    
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    [annotation setCoordinate:CLLocationCoordinate2DMake(lat,log)];
    [annotation setTitle:address];
    [annotation setSubtitle:@""];
    [self.mapView addAnnotation:annotation];
    
}

- (void)setUpTextViewCursorAccordingToLanguage {
    
    self.placeHolderLabel.text = NSLocalizedString(@"Write address", @"");
    
    NSString *languageCode = [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];
    
    if ([languageCode isEqualToString:@"en"]) {
        
        self.addressTextView.textAlignment = NSTextAlignmentLeft;
        
    }else if ([languageCode isEqualToString:@"ar"]) {
        
        self.addressTextView.textAlignment = NSTextAlignmentRight;
    }
}

#pragma mark - Add Observer

-(void)addObserver {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addAnnotationOnMap:) name:@"SelectedLocation" object:nil];
    
}

#pragma mark - Add Annotation

- (void)addAnnotationOnMap:(NSNotification*)infoDict {
    
    NSDictionary *dictInfo = infoDict.userInfo;
    self.selectedLocation = [dictInfo valueForKey:@"SelectedLocation"];
    self.addressString = [dictInfo valueForKey:@"Address"];
    
    self.searchStatus = YES;
    
    if (self.mapView.annotations) {
        [self.mapView removeAnnotations:self.mapView.annotations];
    }
    [self getServiceProviderLocation:self.selectedLocation.coordinate.latitude longitude:self.selectedLocation.coordinate.longitude];

    [self addAnnotationInMapView:self.selectedLocation.coordinate.latitude longitude:self.selectedLocation.coordinate.longitude address:self.addressString];
    
}

#pragma mark - On Zoom To Current Location

- (IBAction)onZoomToCurrentLocation:(id)sender {
    
    [iBeautyUtility zoomToUserLocationInMapView:self.mapView];
}


#pragma mark - Fill Current Location Address

- (void)getAddressFromMapViewCenterCoordinates:(double)lat log:(double)log {
    
    CLGeocoder *ceo = [[CLGeocoder alloc]init];
    CLLocation *loc = [[CLLocation alloc]initWithLatitude:lat longitude:log]; //insert your coordinates
    
    [ceo reverseGeocodeLocation:loc
              completionHandler:^(NSArray *placemarks, NSError *error) {
                  
                  CLPlacemark *placemark = [placemarks objectAtIndex:0];
                  
                  if (placemark) {
                      
                      if (APP_DEBUG_MODE) { NSLog(@"placemark %@",placemark); }
                      //String to hold address
                      NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
                      if (APP_DEBUG_MODE) { NSLog(@"addressDictionary %@", placemark.addressDictionary); }
                      
                      //Print the location to console
                      if (APP_DEBUG_MODE) { NSLog(@"Current Location is  %@",locatedAt); }
                      
                      if (placemark.addressDictionary) {
                          
                          [self setUpMapViewLocation:lat log:log address:locatedAt];
                      }
                      
                  }else {
                      if (APP_DEBUG_MODE) { NSLog(@"Could not locate"); }
                  }
              }
     ];
}

- (void)setUpTextFieldsForCurrentAddress:(NSDictionary*)addressInfo {
    
    if ([addressInfo valueForKey:@"SubLocality"]) {
        self.locality_Textfield.text = [addressInfo valueForKey:@"SubLocality"];
    }
    if ([addressInfo valueForKey:@"City"]) {
        self.city_Textfield.text = [addressInfo valueForKey:@"City"];
    }
    if ([addressInfo valueForKey:@"State"]) {
        self.state_Textfield.text = [addressInfo valueForKey:@"State"];
    }
    if ([addressInfo valueForKey:@"Country"]) {
        self.country_Textfield.text = [addressInfo valueForKey:@"Country"];
    }
    if ([addressInfo valueForKey:@"ZIP"]) {
        self.pinCode_Textfield.text = [addressInfo valueForKey:@"ZIP"];
    }

}

#pragma mark - Text fields Cursors

- (void)setUpTextFieldsCursorAccordingToLanguage {
    
    NSString *languageCode =   [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];
    
    if ([languageCode isEqualToString:@"en"]) {
        
        self.contactNumberTextField.textAlignment = NSTextAlignmentLeft;
        self.name_Textfield.textAlignment = NSTextAlignmentLeft;
        self.email_Textfield.textAlignment = NSTextAlignmentLeft;
        
        self.name_Label.textAlignment = NSTextAlignmentLeft;
        self.email_Label.textAlignment = NSTextAlignmentLeft;
        self.contactNumberLabel.textAlignment = NSTextAlignmentLeft;
        self.addAddressLabel.textAlignment = NSTextAlignmentLeft;
        self.placeHolderLabel.textAlignment = NSTextAlignmentLeft;

    }else if ([languageCode isEqualToString:@"ar"]) {

        
        self.contactNumberTextField.textAlignment = NSTextAlignmentRight;
        self.name_Textfield.textAlignment = NSTextAlignmentRight;
        self.email_Textfield.textAlignment = NSTextAlignmentRight;
        
        self.name_Label.textAlignment = NSTextAlignmentRight;
        self.email_Label.textAlignment = NSTextAlignmentRight;
        self.contactNumberLabel.textAlignment = NSTextAlignmentRight;
        self.addAddressLabel.textAlignment = NSTextAlignmentRight;
        self.placeHolderLabel.textAlignment = NSTextAlignmentRight;

    }
}

- (void)setUpTextFieldsValues_WhenEditAddress {
    
    if (self.addressInfoObj.address.length > 0) {
        [self.placeHolderLabel setHidden:YES];
    }
    self.addressTextView.text = [iBeautyUtility trimString:self.addressInfoObj.address];
    self.contactNumberTextField.text = [iBeautyUtility trimString:[NSString stringWithFormat:@"%@",self.addressInfoObj.contactNumber]];
    self.name_Textfield.text = [iBeautyUtility trimString:self.addressInfoObj.name];
    self.email_Textfield.text = [iBeautyUtility trimString:self.addressInfoObj.email];

}

- (void)setupFieldsViewRound {

    [self.saveAddress setTitle:NSLocalizedString(@"Save", @"")];

    [self.addAddressLabel setText:NSLocalizedString(@"Add Address", @"")];
    [self.contactNumberLabel setText:NSLocalizedString(@"Contact Number", @"")];
    [self.name_Label setText:NSLocalizedString(@"Name", @"")];
    [self.email_Label setText:NSLocalizedString(@"Email", @"")];

    self.contactNumberTextField.placeholder = NSLocalizedString(@"Enter contact number", @"");
    self.name_Textfield.placeholder = NSLocalizedString(@"Enter name", @"");
    self.email_Textfield.placeholder = NSLocalizedString(@"Enter email", @"");

}


#pragma mark - set up Tool Bar On Text fields

- (void)setupToolBarOnTextfields {
    
    self.toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50)];
    self.toolBar.barStyle = UIBarStyleDefault;
    [self.toolBar sizeToFit];
    [self.toolBar setBackgroundColor:[UIColor lightGrayColor]];
    [self buttonWithinToolbar];
    
    self.contactNumberTextField.inputAccessoryView = self.toolBar;
    self.addressTextView.inputAccessoryView = self.toolBar;
    self.name_Textfield.inputAccessoryView = self.toolBar;
    self.email_Textfield.inputAccessoryView = self.toolBar;

}
- (void)buttonWithinToolbar {
    
    self.doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", @"") style:UIBarButtonItemStyleDone target:self action:@selector(doneAction:)];
    
    UIBarButtonItem *flexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [self.toolBar setItems:@[flexibleItem,self.doneButton] animated:NO];
}

- (void)doneAction:(id)sender {
    
    [self.addressTextView resignFirstResponder];
    [self.contactNumberTextField resignFirstResponder];
    [self.name_Textfield resignFirstResponder];
    [self.email_Textfield resignFirstResponder];

}

#pragma mark - Cancel Button

- (IBAction)cancelButton:(UIBarButtonItem*)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)backToView {
    
    if (self.navigateStatus == YES) {
        
        for (UIViewController *controller in self.navigationController.viewControllers) {
            
            if ([controller isKindOfClass:[MyProfileViewController class]]) {
                
                [self.navigationController popToViewController:controller animated:YES];
                
                break;
            }
        }
    }else {
        
        for (UIViewController *controller in self.navigationController.viewControllers) {
            
            if ([controller isKindOfClass:[AddressesViewController class]]) {
                
                [self.navigationController popToViewController:controller animated:YES];
                
                break;
            }
        }
    }
    
}


#pragma mark - save Address

-(IBAction)saveAddress:(id)sender {
    
    BOOL validStatus =  [self checkValidationForAddAddress];
    if (self.modeStatus == YES) {
        if (validStatus == YES) {
            [self UdateExistingAddressTo_registered_Users];
        }else {
            return;
        }
    }else {
        if (validStatus == YES) {
            [self addNewAddressTo_registered_Users];
        }else {
            return;
        }
    }
}

#pragma mark - check Validation For Address Parameters


- (BOOL)checkValidationForAddAddress {
    
    if (self.contactNumberTextField.text.length < 1) {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"Please enter phone number", @"")];
        [self.contactNumberTextField becomeFirstResponder];
        return NO ;
    }
    
    if (self.name_Textfield.text.length < 1) {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"Please enter name", @"")];
        [self.name_Textfield becomeFirstResponder];
        return NO ;
    }
    
    if (self.email_Textfield.text.length < 1) {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"Please enter email", @"")];
        [self.email_Textfield becomeFirstResponder];
        return NO ;
    }
    
    return YES;
    
}


- (BOOL)checkValidationForAccountParameters {

    if (self.mobileNo_Textfield.text.length < 1) {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"Please enter phone number", @"")];
        [self.mobileNo_Textfield becomeFirstResponder];
        return NO ;
    }
    return YES;
}

#pragma mark - add New Address To Registered Users

- (void)addNewAddressTo_registered_Users {
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_ADD_ADDRESS];

    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSDictionary *parametersDict   = [self getParamerterDictionary_for_RegisteredUser];
    
    if (internetStatus != NotReachable) {
        [SVProgressHUD showWithStatus:KLoadingMessage];
        
        [handler jsonPostData:url params:parametersDict onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                @try {
                    if (completed) {
                        NSError *jsonError;
                        NSData *objectData = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData options:NSJSONReadingMutableContainers error:&jsonError];
                        NSString *userId = [json valueForKey:@"Id"];
                        if (userId.length>0) {
                            self.addressInfoObj = [AddressModel getObjectWithJSONInfo:json screenStatus:NO];
                            if (self.backStatus == YES) {
                                [self switchToAddressViewController];
                            }else {
                                [self backToView];
                            }
                        }else {
                            [iBeautyUtility showAlertMessage:NSLocalizedString(@"Something went wrong", @"")];
                        }
                    }
                } @catch (NSException *exception) {
                } @finally {
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}

- (void)switchToAddressViewController {
    
    AddressesViewController *addressController = [self.storyboard instantiateViewControllerWithIdentifier:kAddressesViewController];
    addressController.serviceDetailsArray = self.serviceDetailsArray;
    addressController.categoryInfoObj = self.categoryInfoObj;
    addressController.serviceProvideAtString = self.serviceProvideAtString;
    addressController.noteString = self.noteString;
    addressController.screenStatus = self.screenStatus;
    [self.navigationController pushViewController:addressController animated:YES];
}


#pragma mark - Update Basket for Note and Service Provide AT

- (void)updateBasketFor_customerAddress {
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_UPDATE_BASKET];
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSDictionary *dictInfo = [self getParametersInfo_For_UpdateNoteAndServiceProvideAT];
    
    if (internetStatus != NotReachable) {
        
        [handler jsonPostData:url params:dictInfo onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                @try {
                    if (completed) {
                        
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSMutableArray *tempInfoArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE) { NSLog(@"Temp Info Array is %@", tempInfoArray); }
                        
                        //                        BasketInfoObj *infoObj = [BasketInfoObj getSharedInstance];
                        //                        NSMutableArray *basketInfoArray = [infoObj basketDetailInfoObject:tempInfoArray];
                        //                        if (APP_DEBUG_MODE)
                        //                         NSLog(@"Response Array is %@",basketInfoArray);
                        
                        if (self.serviceDetailsArray.count>0) {
                            [self continueToGetPricing];
                        }
                        
                    }else {
                        if (APP_DEBUG_MODE)
                            NSLog(@"error is %@", error.localizedDescription);
                    }
                    
                } @catch (NSException *exception) {
                    [iBeautyUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}

#pragma mark - Get pricing

- (void)continueToGetPricing {
    
    PriceCartViewController *priceCartObj = [self.storyboard instantiateViewControllerWithIdentifier:kPriceCartViewController];
    priceCartObj.serviceDetailsArray = self.serviceDetailsArray;
    priceCartObj.categoryInfoObj = self.categoryInfoObj;
    priceCartObj.addressModel = self.addressInfoObj;
    priceCartObj.pickStatus = self.pickStatus;
    [self.navigationController pushViewController:priceCartObj animated:YES];
}


- (NSMutableDictionary*)getParametersInfo_For_UpdateNoteAndServiceProvideAT {
    
    NSMutableDictionary *basketDict = [[NSMutableDictionary alloc]init];
    NSString *businessId = self.categoryInfoObj.Id;
    [basketDict setValue:businessId forKey:@"BusinessId"];
    
    NSString *customerID = [[NSUserDefaults standardUserDefaults]valueForKey:KUserId];
    [basketDict setValue:customerID forKey:@"CustomerId"];
    
    NSString *basketId = [[NSUserDefaults standardUserDefaults]valueForKey:@"BasketId"];
    [basketDict setObject:basketId forKey:@"BasketId"];
    
    NSMutableArray *appointmentsArray = [NSMutableArray new];
    
    for (BasketInfoObj *detailInfo in self.serviceDetailsArray) {
        
        NSMutableDictionary *appointmentDict = [[NSMutableDictionary alloc]init];
        [appointmentDict setValue:detailInfo.serviceId forKey:@"ServiceId"];
        [appointmentDict setValue:detailInfo.startDateTime forKey:@"StartDateTime"];
        [appointmentDict setValue:detailInfo.endDateTime forKey:@"EndDateTime"];
        [appointmentDict setValue:detailInfo.staffId forKey:@"StaffId"];
        [appointmentDict setValue:detailInfo.appointmentId forKey:@"Id"];
        [appointmentsArray addObject:appointmentDict];
    }
    [basketDict setObject:appointmentsArray forKey:@"UpdatedAppointments"];
    [basketDict setObject:@"" forKey:@"NewAppointments"];
    [basketDict setObject:@"" forKey:@"RemovedAppointments"];
    [basketDict setObject:@"" forKey:@"Products"];
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    [basketDict setValue:accessToken forKey:@"Authorization"];
    [basketDict setValue:self.serviceProvideAtString forKey:@"ServiceProvideAt"];
    [basketDict setValue:self.noteString forKey:@"Note"];
    
    NSDictionary *addressDict = [self getDictInfo];
    
    [basketDict setObject:addressDict forKey:@"CustomerAddress"];
    
    return basketDict;
    
}

#pragma mark - Udate Existing Address To Registered Users

- (void)UdateExistingAddressTo_registered_Users {
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_UPDATE_ADDRESS];

    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSDictionary *parametersDict   = [self getParamerterDictionary_for_RegisteredUser];
    
    if (internetStatus != NotReachable) {
        [SVProgressHUD showWithStatus:KLoadingMessage];
        
        [handler jsonPostData:url params:parametersDict onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                @try {
                    if (completed) {
                        
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSDictionary *tempInfoDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE) { NSLog(@"Temp Info Dict is %@", tempInfoDict); }
                        NSString *userID = [tempInfoDict valueForKey:@"UserId"];
                        
                        if (userID.length>0) {
                            [self backToView];
                        }else {
//                            NSArray *array = [completed valueForKey:@"error"];
//                            [iBeautyUtility showAlertMessage:array[0]];
                        }
                    }
                } @catch (NSException *exception) {
                    //[EShopUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}

#pragma mark - get Paramerter Dictionary

 - (NSDictionary*)getParamerterDictionary_for_RegisteredUser {
    
    NSString *addressId ;
    if (self.modeStatus == YES) {
        addressId = self.addressInfoObj.address_Id;
    }
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    NSDictionary *profileInfo = [[NSUserDefaults standardUserDefaults]valueForKey:KLoginInfo];
    NSString *userID = [profileInfo valueForKey:@"UserID"];
    
    NSDictionary *userDetailDict = [[NSDictionary alloc]init];
     
    NSString *nameStr = [iBeautyUtility trimString:self.name_Textfield.text];
    NSString *contactNoStr = [iBeautyUtility trimString:self.contactNumberTextField.text];
    NSString *addressStr = [iBeautyUtility trimString:self.addressTextView.text];
    NSString *emailStr = [iBeautyUtility trimString:self.email_Textfield.text];
 
    if (self.modeStatus == YES) {
        
        userDetailDict = [NSDictionary dictionaryWithObjectsAndKeys:accessToken,@"Authorization",addressId,@"Id", userID,@"UserId",nameStr,@"UserName",contactNoStr,@"ContactNumber",addressStr,@"Address",self.latString,@"Latitude",self.longString,@"Longitude",emailStr,@"Email",nil];
    }else {
        
        userDetailDict = [NSDictionary dictionaryWithObjectsAndKeys:accessToken,@"Authorization",userID,@"UserId",nameStr,@"UserName",contactNoStr,@"ContactNumber",addressStr,@"Address",self.latString,@"Latitude",self.longString,@"Longitude",emailStr,@"Email", nil];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:userDetailDict forKey:@"userShippingAddress"];
    
    return userDetailDict;
}


#pragma mark - Get Customers address Dictionary

- (NSDictionary*)getDictInfo {

    NSString *nameStr = [iBeautyUtility trimString:self.addressInfoObj.name];
    NSString *contactNoStr = [iBeautyUtility trimString:self.addressInfoObj.contactNumber];
    NSString *userID = [[NSUserDefaults standardUserDefaults]valueForKey:KUserId];
    NSString *addressStr = [iBeautyUtility trimString:self.addressInfoObj.address];
    NSString *latStr = [iBeautyUtility trimString:self.addressInfoObj.latitude];
    NSString *longStr = [iBeautyUtility trimString:self.addressInfoObj.longitude];
    NSString *emailStr = [iBeautyUtility trimString:self.addressInfoObj.email];

    NSDictionary *userDetailDict = [[NSDictionary alloc]init];
    
    userDetailDict = [NSDictionary dictionaryWithObjectsAndKeys:userID,@"UserId",self.addressInfoObj.address_Id,@"Id", userID,@"UserId",nameStr,@"UserName",contactNoStr,@"ContactNumber",addressStr,@"Address",latStr,@"Latitude",longStr,@"Longitude",emailStr,@"Email",nil];
    
    return userDetailDict;
    
}

#pragma mark UITextField Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString *currentString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSInteger length = [currentString length];
    
    if (textField == self.contactNumberTextField) {
        if (length > phoneNumberMaxLength ) {
            return NO;
        }
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return  YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    return YES;
}

#pragma mark - Text view delegates

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    
//    self.addressTextView.text = @"";
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    
    if (![self.addressTextView hasText]) {
        self.placeHolderLabel.hidden = NO;
    }
}

- (void) textViewDidChange:(UITextView *)textView
{
    if(![self.addressTextView hasText]) {
        self.placeHolderLabel.hidden = NO;
    }
    else{
        self.placeHolderLabel.hidden = YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
