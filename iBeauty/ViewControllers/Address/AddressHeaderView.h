//
//  AddressHeaderView.h
//  iBeauty
//
//  Created by App Innovation on 24/01/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServicesCategoriesInfo.h"

NS_ASSUME_NONNULL_BEGIN

@interface AddressHeaderView : UIView

@property (nonatomic,weak) IBOutlet UIView    *bkgView;
@property (nonatomic,weak) IBOutlet UILabel   *nameLabel;
@property (nonatomic,weak) IBOutlet UILabel   *addressLabel;

+ (instancetype)addressHeaderView:(ServicesCategoriesInfo*)infoObj;

@end

NS_ASSUME_NONNULL_END
