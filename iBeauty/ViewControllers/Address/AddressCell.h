//
//  AddressCell.h
//  EShop
//
//  Created by App Innovation on 20/11/17.
//  Copyright © 2017 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddressModel.h"

@interface AddressCell : UITableViewCell

@property (nonatomic,weak) IBOutlet UIButton *checkBox;
@property (nonatomic,weak) IBOutlet UIButton *editButton;
@property (nonatomic,weak) IBOutlet UIButton *deleteButton;
@property (nonatomic,weak) IBOutlet UIImageView *selectImageView;

- (void)configureAddressCell:(AddressModel*)addressObj;

@end
