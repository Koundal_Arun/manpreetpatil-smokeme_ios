//
//  AddressCell.m
//  EShop
//
//  Created by App Innovation on 20/11/17.
//  Copyright © 2017 App Innovation. All rights reserved.
//

#import "AddressCell.h"
#import "iBeautyUtility.h"
#import "UILabel+FlexibleWidHeight.h"
#import "Constants.h"
#import "NSString+iBeautyString.h"

@interface AddressCell ()

@property (nonatomic,weak) IBOutlet UILabel *userName;
@property (nonatomic,weak) IBOutlet UILabel *userAddress;
@property (nonatomic,weak) IBOutlet UIView  *addressbackg_View;
@property (nonatomic,weak) IBOutlet UILabel *phoneNumberLabel;

@end


@implementation AddressCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

#pragma mark - Configure Address Cell

- (void)configureAddressCell:(AddressModel*)addressObj {
    
    [self setUpAddressViewsText:addressObj];

   CGFloat addressHeight = [UILabel heightOfTextForString:addressObj.address andFont:[UIFont fontWithName:@"SegoeUI" size:15.0] maxSize:CGSizeMake([UIScreen mainScreen].bounds.size.width- 62.0f, MAXFLOAT)];

    if (addressObj.name.length > 0) {
        
        [self setUpAddressCell_With_NameField:addressObj addressTextHeight:addressHeight];

    }else {
        [self setUpAddressCell_WithOut_NameField:addressObj addressTextHeight:addressHeight];
    }

    [self.addressbackg_View setFrame:CGRectMake(7.0f, 7.0f, [UIScreen mainScreen].bounds.size.width-14, addressObj.cellHeight-14.0f)];

    [iBeautyUtility setViewBorderRound:self.addressbackg_View];
}

#pragma mark - Set Up Address Views Text

- (void)setUpAddressViewsText:(AddressModel*)addressObj {
    
    self.userName.text = addressObj.name;
    self.userAddress.text = addressObj.address;
    NSString *phoneNoStr = addressObj.contactNumber;
    NSMutableAttributedString *phoneNoString = [phoneNoStr getOrdersAttributedTextWithId:@"Phone no - " openingStatus:phoneNoStr screenType:3];
    self.phoneNumberLabel.attributedText = phoneNoString;
    self.userAddress.translatesAutoresizingMaskIntoConstraints = YES;
    self.userName.translatesAutoresizingMaskIntoConstraints = YES;
    self.phoneNumberLabel.translatesAutoresizingMaskIntoConstraints = YES;
    
}

#pragma mark - Set Up Address Cell - With Name

- (void)setUpAddressCell_With_NameField:(AddressModel*)addressObj addressTextHeight:(CGFloat)addressHeight {
    
    NSString *languageCode = [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];

    self.userName.hidden = NO;
    
    if ([languageCode isEqualToString:@"en"]) {
        
        [self.userName setFrame:CGRectMake(38.0f, self.userName.frame.origin.y, [UIScreen mainScreen].bounds.size.width-145, self.userName.frame.size.height)];
        [self.userAddress setFrame:CGRectMake(38.0f, 44.0f, [UIScreen mainScreen].bounds.size.width-62, addressHeight)];
        self.userAddress.textAlignment = NSTextAlignmentLeft;
        [self.phoneNumberLabel setFrame:CGRectMake(38.0f, 44.0f + addressHeight + 7.0f, [UIScreen mainScreen].bounds.size.width-62, 21.0f)];
        self.phoneNumberLabel.textAlignment = NSTextAlignmentLeft;

    }else if ([languageCode isEqualToString:@"ar"]) {
        
        [self.userName setFrame:CGRectMake(115.0f, self.userName.frame.origin.y, [UIScreen mainScreen].bounds.size.width-135, self.userName.frame.size.height)];
        [self.userAddress setFrame:CGRectMake(38.0f, 44.0f, [UIScreen mainScreen].bounds.size.width-62, addressHeight)];
        self.userAddress.textAlignment = NSTextAlignmentRight;
        [self.phoneNumberLabel setFrame:CGRectMake(38.0f, 44.0f + addressHeight + 7.0f, [UIScreen mainScreen].bounds.size.width-62, 21.0f)];
        self.phoneNumberLabel.textAlignment = NSTextAlignmentRight;
    }
}

#pragma mark - Set Up Address Cell - Without Name

- (void)setUpAddressCell_WithOut_NameField:(AddressModel*)addressObj addressTextHeight:(CGFloat)addressHeight {
    
    NSString *languageCode = [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];

    self.userName.hidden = YES;
    
    if ([languageCode isEqualToString:@"en"]) {
        
        [self.userAddress setFrame:CGRectMake(38.0f, 13.0f, [UIScreen mainScreen].bounds.size.width-125, addressHeight)];
        self.userAddress.textAlignment = NSTextAlignmentLeft;
        
    }else if ([languageCode isEqualToString:@"ar"]) {
        
        [self.userAddress setFrame:CGRectMake(60.0f, 13.0f, [UIScreen mainScreen].bounds.size.width-125, addressHeight)];
        self.userAddress.textAlignment = NSTextAlignmentRight;
    }
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
