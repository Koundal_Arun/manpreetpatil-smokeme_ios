//
//  SearchAddressViewController.m
//  iBeauty
//
//  Created by App Innovation on 18/03/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import "SearchAddressViewController.h"
#import "AddAddressController.h"
#import "DXPopover.h"

@import MapKit;

@interface SearchAddressViewController ()<MKMapViewDelegate,UISearchBarDelegate,MKLocalSearchCompleterDelegate,UIGestureRecognizerDelegate,CLLocationManagerDelegate,UITableViewDelegate,UITableViewDataSource> {
    
    CGFloat _popoverWidth;

}

@property(nonatomic, weak) IBOutlet UILabel *instructionLabel;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIView *searchView;

@property (assign, nonatomic) BOOL searchStatus;
@property (strong, nonatomic) MKLocalSearchCompletion *item;
@property (strong, nonatomic) MKPlacemark *placemark;

@property (strong, nonatomic) MKLocalSearchCompleter *completer;
@property(nonatomic, strong) NSArray <MKLocalSearchCompletion *> *resultsArray;
@property (strong, nonatomic) NSString *searchText;
@property (weak, nonatomic) IBOutlet UIView *currentView;
@property (strong, nonatomic) CLLocation *selectedLocation;
@property (strong, nonatomic) NSString   *addressString;
@property (nonatomic,strong) AppDelegate *delegate;
@property (nonatomic, weak) IBOutlet UISegmentedControl *mapTypeSegment;

@property (nonatomic,weak) IBOutlet UINavigationItem *navigationItem;
@property (nonatomic,weak) IBOutlet UIBarButtonItem *backButton;
@property (nonatomic,weak) IBOutlet UIBarButtonItem *nextButton;
@property (nonatomic,weak)  IBOutlet UINavigationBar *navBar;

// Pop Over
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) DXPopover *popover;

@end

@implementation SearchAddressViewController {
 
    MKLocalSearch *localSearch;
    MKLocalSearchResponse *results;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.delegate = [AppDelegate sharedInstance];
    self.mapView.mapType = MKMapTypeStandard;
    [self setUpPinMarkedLocation];
    [self setUpSearchCompleter];
    [self addPanGestureOnMapView];
    [self setUpViewForLocalization];
    //[self setGradient];
    [self setUpSegmentControlTint];
    [self setPopOVerView];
}

-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:YES];
    
}

#pragma mark - Set Pop Over View

- (void)setPopOVerView {
    
    [self resetPopover];
    [self setUpPopOverTableView];
}

#pragma mark - Set Up Map View - Edit Mode

- (void)setUpPinMarkedLocation {
    
    if (self.modeStatus == YES) {
        
        if(self.latitude.length > 0 && ![self.latitude isEqualToString:@"0.0"]) {
            [self setUpMapPin_Marked_Location];
        }else {
            [self setUpMapPin_Current_Location];
        }
    }else {
        [self setUpMapPin_Current_Location];
    }
}

- (void)setUpMapPin_Marked_Location {
    
    [self getAddressFromMapViewCenterCoordinates:[self.latitude doubleValue] longitude:[self.longitude doubleValue] showTitle:NO];
    [self setUpMapViewLocation:[self.latitude doubleValue] longitude:[self.longitude doubleValue]];
    
}

- (void)setUpMapPin_Current_Location {
    
    [self getAddressFromMapViewCenterCoordinates:[self.delegate.latitude doubleValue] longitude:[self.delegate.longitude doubleValue] showTitle:YES];
    [self setUpMapViewLocation:[self.delegate.latitude doubleValue] longitude:[self.delegate.longitude doubleValue]];
}

#pragma mark - Set Gradient

- (void)setGradient {
    
    self.navBar.frame = [iBeautyUtility setUpFrame:self.navBar.frame];
    CAGradientLayer *gradient = [iBeautyUtility setNavigationGradient:self.navBar];
    [self.navBar setBackgroundImage:[iBeautyUtility imageFromLayer:gradient] forBarMetrics:UIBarMetricsDefault];
}

#pragma mark - Set Segment Control Tint

- (void)setUpSegmentControlTint {

    if (@available(iOS 13.0, *)) {

        [self.mapTypeSegment setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor], NSFontAttributeName: [UIFont fontWithName:@"SegoeUI" size:15.0]} forState:UIControlStateSelected];
        [self.mapTypeSegment setSelectedSegmentTintColor:[UIColor iBeautyThemeColor]];
        
    } else {

        [self.mapTypeSegment setTintColor:[UIColor iBeautyThemeColor]];
    }
    
}

#pragma mark - Set Up localization Views

- (void)setUpViewForLocalization {
    
    self.navigationItem.title = NSLocalizedString(@"Select Location", @"");
    [self.backButton setTitle: NSLocalizedString(@"Back", @"")];
    [self.nextButton setTitle: NSLocalizedString(@"Save", @"")];
    self.instructionLabel.text = NSLocalizedString(@"Move map to select your location", @"");
    self.searchBar.placeholder = NSLocalizedString(@"Search location", @"");
    [[UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setTitle:NSLocalizedString(@"Cancel", @"")];

    [self setUpNamesForSegmentControl];

}

- (void)setUpNamesForSegmentControl {
    
    NSString * firstIndexName = NSLocalizedString(@"Standard", @"");;
    NSString * secondIndexName = NSLocalizedString(@"Hybrid", @"");;
    [self.mapTypeSegment setTitle:firstIndexName forSegmentAtIndex:0];
    [self.mapTypeSegment setTitle:secondIndexName forSegmentAtIndex:1];

}

#pragma mark - Set Map View Location


- (void)setUpMapViewLocation:(double)lat longitude:(double)log {
    
    self.mapView.delegate = self;
    [self.mapView setShowsUserLocation:YES];
    
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(lat,log);
    MKCoordinateSpan span = MKCoordinateSpanMake(0.1, 0.1);
    MKCoordinateRegion region = {coordinate, span};
    region = [self.mapView regionThatFits:region];
    [self.mapView setRegion:region];

}


#pragma mark - Add Pan Gesture On Map View

- (void)addPanGestureOnMapView {
    
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc]init];
    [panGesture addTarget:self action:@selector(getMapViewLocation:)];
    panGesture.delegate = self;
    [self.mapView addGestureRecognizer:panGesture];
    
}

- (void)getMapViewLocation:(UIGestureRecognizer*)panGesture {
    
    if (panGesture.state == UIGestureRecognizerStateEnded) {
        
        [self getAddressFromMapViewCenterCoordinates:self.mapView.centerCoordinate.latitude longitude:self.mapView.centerCoordinate.longitude showTitle:YES];
    }
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (void)getAddressFromMapViewCenterCoordinates:(double)lat longitude:(double)log showTitle:(BOOL)showStatus {
    
    self.selectedLocation = [[CLLocation alloc]initWithLatitude:lat longitude:log];
    
    CLGeocoder *ceo = [[CLGeocoder alloc]init];
    
    [ceo reverseGeocodeLocation:self.selectedLocation
              completionHandler:^(NSArray *placemarks, NSError *error) {
                  
                  CLPlacemark *placemark = [placemarks objectAtIndex:0];
                  self.placemark = [[MKPlacemark alloc] initWithPlacemark:placemark];
                  if (placemark) {
                      
                      if (APP_DEBUG_MODE) { NSLog(@"placemark %@",placemark); }
                      //String to hold address
                      NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
                      if (APP_DEBUG_MODE) { NSLog(@"addressDictionary %@", placemark.addressDictionary); }
                      
                      self.addressString = locatedAt;
                      
                      if(showStatus == YES) {
                          self.instructionLabel.text = self.addressString;
                      }
                      //Print the location to console
                      if (APP_DEBUG_MODE) { NSLog(@"Current Location %@",locatedAt); }
                      if (showStatus == NO) {
                          if (self.mapView.annotations) {
                              [self.mapView removeAnnotations:self.mapView.annotations];
                          }
                          MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
                          [annotation setCoordinate:self.placemark.coordinate];
                          [annotation setTitle:self.addressString];
                          [self.mapView addAnnotation:annotation];
                      }

                  }
                  else {
                      if (APP_DEBUG_MODE) { NSLog(@"Could not locate"); }
                  }
              }
     ];
}

#pragma mark - Cancel Button

- (IBAction)cancelButton:(UIBarButtonItem*)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - save Address

-(IBAction)saveAddress:(id)sender {
    
    NSDictionary *parametersDict  = [NSDictionary dictionaryWithObjectsAndKeys:self.selectedLocation,@"SelectedLocation",self.addressString,@"Address",nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SendSelectedLocation" object:nil userInfo:parametersDict];
    
    [self.navigationController popViewControllerAnimated:YES];

}

#pragma mark - On Zoom To Current Location

- (IBAction)onZoomToCurrentLocation:(id)sender {
    
    [iBeautyUtility zoomToUserLocationInMapView:self.mapView];
}

#pragma mark - Segment Control For map type

- (IBAction)mapSatelliteSegmentControlTapped:(UISegmentedControl *)sender {
    
    [sender setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]} forState:UIControlStateSelected];
    
    switch (((UISegmentedControl *) sender).selectedSegmentIndex) {
        case 0:
            self.mapView.mapType = MKMapTypeStandard;
            break;
        case 1:
            self.mapView.mapType = MKMapTypeHybrid;
            break;
            
        default:
            break;
    }
}

- (void)setUpSearchCompleter {
    
    self.completer = [[MKLocalSearchCompleter alloc] init];
    self.completer.delegate = self;
    self.completer.filterType = MKSearchCompletionFilterTypeLocationsAndQueries;
    self.completer.region = self.mapView.region;
    
}


- (void)getAnnotationFromMKLocalSearchCompleter:(NSString*)locationStr {
    
    NSString *location = locationStr;
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    CLLocationDistance radius = 10000;
    CLCircularRegion *region = [[CLCircularRegion alloc] initWithCenter:self.mapView.centerCoordinate radius:radius identifier:@"CurrentRegion"];
    if (@available(iOS 11.0, *)) {
        [geocoder geocodeAddressString:location inRegion:region preferredLocale:[NSLocale currentLocale] completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
            
            if (placemarks && placemarks.count > 0) {
                
                CLPlacemark *topResult = [placemarks objectAtIndex:0];
                self.placemark = [[MKPlacemark alloc] initWithPlacemark:topResult];
                
                CLLocationCoordinate2D coordinate = self.placemark.coordinate;
                MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(coordinate, 10000.0f, 10000.0f);
                
                [self.mapView regionThatFits:region];
                [self.mapView setRegion:region animated:YES];
                self.mapView.delegate = self;
                self.mapView.centerCoordinate = coordinate;
                [self.mapView addAnnotation:self.placemark];
                NSString *locationAddress = [[topResult.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
                
                self.selectedLocation = [[CLLocation alloc]initWithLatitude:self.mapView.centerCoordinate.latitude longitude:self.mapView.centerCoordinate.longitude];
                self.addressString = locationAddress;
                self.instructionLabel.text = self.addressString;
                if (APP_DEBUG_MODE) { NSLog(@"Location address is --- %@",locationAddress); }
            }
        }];
    } else {
        // Fallback on earlier versions
    }
}

#pragma mark - MKLocalSearchCompleter

- (void) completerDidUpdateResults:(MKLocalSearchCompleter *)completer {
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
//    for (MKLocalSearchCompletion *completion in completer.results) {
//        NSLog(@"------ %@",completion.description);
//    }
    self.resultsArray = completer.results ;
    if (APP_DEBUG_MODE) { NSLog(@"Results Array is --- %@",self.resultsArray); }
    if (self.resultsArray.count >0) {
        if (self.searchStatus == NO) {
            self.searchStatus = YES;
            [self titleShowPopover];
        }
        [self.popover setHidden:NO];
        [self.tableView reloadData];
    }else {
        [self.popover setHidden:YES];
    }

}


- (void) completer:(MKLocalSearchCompleter *)completer didFailWithError:(NSError *)error {
    
    if (APP_DEBUG_MODE) { NSLog(@"Completer failed with error: %@",error.description); }
}

#pragma mark - Pop Over Methods

- (void)resetPopover {
    self.popover = [DXPopover new];
    _popoverWidth = SCREEN_WIDTH - 40;
}

- (void)setUpPopOverTableView {
 
    UITableView *blueView = [[UITableView alloc] init];
    blueView.frame = CGRectMake(0, 0, _popoverWidth, 220);
    blueView.dataSource = self;
    blueView.delegate = self;
    self.tableView = blueView;
    [self.popover setHidden:YES];

}

- (void)titleShowPopover {
    
    [self updateTableViewFrame];

    self.popover.contentInset = UIEdgeInsetsMake(20, 0.0, 20, 0.0);
    self.popover.backgroundColor = [UIColor whiteColor];

    CGPoint startPoint =
        CGPointMake(20.0f,self.searchView.frame.origin.y + self.searchView.frame.size.height);

    [self.popover showAtPoint:startPoint
               popoverPostion:DXPopoverPositionDown
              withContentView:self.tableView
                       inView:self.tabBarController.view];
    self.popover.didDismissHandler = ^{
    };
}

- (void)updateTableViewFrame {
    CGRect tableViewFrame = self.tableView.frame;
    tableViewFrame.size.width = _popoverWidth;
    self.tableView.frame = tableViewFrame;
    self.popover.contentInset = UIEdgeInsetsZero;
    self.popover.backgroundColor = [UIColor whiteColor];
}

- (void)bounceTargetView:(UIView *)targetView {
    targetView.transform = CGAffineTransformMakeScale(0.9, 0.9);
    [UIView animateWithDuration:0.5
                          delay:0.0
         usingSpringWithDamping:0.3
          initialSpringVelocity:5
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         targetView.transform = CGAffineTransformIdentity;
                     }
                     completion:nil];
}

#pragma mark - Search delegate methods

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    if (searchText.length>0) {

        self.searchText = searchText;
        [self performSelector:@selector(getLocationResult) withObject:self afterDelay:3.0 ];
    }
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    // called twice every time
    [searchBar setShowsCancelButton:YES animated:YES];
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    // called only once
    [searchBar setShowsCancelButton:YES animated:YES];
    return YES;
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    
    self.searchStatus = NO;
    [searchBar resignFirstResponder];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
    self.searchStatus = NO;
    [searchBar resignFirstResponder];
}

- (void)getLocationResult {
    
    self.completer.queryFragment = self.searchText;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
}

#pragma mark - UITableViewDataSouce

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.resultsArray.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellId = @"cellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    MKLocalSearchCompletion *infoObj = self.resultsArray[indexPath.row];
    NSString *addressString = [NSString stringWithFormat:@"%@ (%@)",infoObj.title,infoObj.subtitle];
    cell.textLabel.text = addressString;
    return cell;

}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 50.0f;

}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 0.0f;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];

    [self.popover dismiss];
    self.searchStatus = NO;
    
    self.item = self.resultsArray[indexPath.row];
    
    if (self.mapView.annotations) {
        [self.mapView removeAnnotations:self.mapView.annotations];
    }
    NSString *addressString = [NSString stringWithFormat:@"%@ (%@)",self.item.title,self.item.subtitle];
    if (self.item.title.length > 0) {
        [self getAnnotationFromMKLocalSearchCompleter:addressString];
    }
    [self.searchBar resignFirstResponder];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
