//
//  MyAppointmentViewController.m
//  iBeauty
//
//  Created by App Innovation on 27/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "MyAppointmentViewController.h"
#import "Constants.h"
#import "AllAppointmentsCell.h"
#import "AllAppointmentsInfo.h"
#import "ReviewRatingController.h"

static NSString *allAppointmentsCellIdentifier = @"AllAppointmentsCell";

@interface MyAppointmentViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,weak) IBOutlet UITableView *allAppointmentTableView;
@property (nonatomic,strong) NSMutableArray *allAppointmentArray;
@property (nonatomic,weak) IBOutlet UIView *noAppointmentView;
@property (nonatomic,weak)  IBOutlet UINavigationBar *navBar;

@end

@implementation MyAppointmentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //[self setGradient];

}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    [self getAllAppointmentsInfoObject];
    self.tabBarController.tabBar.hidden = YES;

}

#pragma mark - Set Gradient

- (void)setGradient {
    
    self.navBar.frame = [iBeautyUtility setUpFrame:self.navBar.frame];
    CAGradientLayer *gradient = [iBeautyUtility setNavigationGradient:self.navBar];
    [self.navBar setBackgroundImage:[iBeautyUtility imageFromLayer:gradient] forBarMetrics:UIBarMetricsDefault];
}

#pragma mark - View For No Appointments Available

- (void)viewForNoAppointmentsAvailable {

    if (self.allAppointmentArray.count<1) {
        [self.view addSubview:self.noAppointmentView];
        self.noAppointmentView.center = self.view.center;
        self.allAppointmentTableView.hidden = YES;
    }else {
        self.allAppointmentTableView.hidden = NO;
        [self.noAppointmentView removeFromSuperview];
    }
//    [self.allAppointmentTableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    [self.allAppointmentTableView reloadData];
}

#pragma mark - Back Action Method

- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Write Review Rating

- (void)giveRatingToCompletedAppointments:(UIButton*)sender {
    
    ReviewRatingController *reviewController = [self.storyboard instantiateViewControllerWithIdentifier:kReviewRatingController];
    reviewController.appointmentInfo = self.allAppointmentArray[sender.tag];
    reviewController.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:reviewController animated:YES completion:nil];
    
}


#pragma mark - UITableViewDataSouce

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.allAppointmentArray.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    AllAppointmentsCell *cell = [tableView dequeueReusableCellWithIdentifier:allAppointmentsCellIdentifier];
        
    if (!cell){
        cell = [[AllAppointmentsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:allAppointmentsCellIdentifier];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.delegateObj = self;
    cell.collectionView_Categories.tag = indexPath.row;
    AllAppointmentsInfo *appointmentInfo = self.allAppointmentArray[indexPath.row];
    [cell configureCellWithInfo:appointmentInfo];
    [cell.writeAReviewButton addTarget:self action:@selector(giveRatingToCompletedAppointments:) forControlEvents:UIControlEventTouchUpInside];
    cell.writeAReviewButton.tag = indexPath.row;
    return cell;
    
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 328.0f;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.allAppointmentTableView deselectRowAtIndexPath:indexPath animated:NO];
    AppointmentDetailViewController *detailController  = [self.storyboard instantiateViewControllerWithIdentifier:kAppointmentDetailController];
    AllAppointmentsInfo *detailObj = self.allAppointmentArray[indexPath.row];
    detailController.appointmentDetailInfo = detailObj;
    [self.navigationController pushViewController:detailController animated:YES];
    
}


#pragma mark - CustomCell Delegate Method

- (void)delMethod_didSelectCollection:(NSInteger)rowSelected section:(NSInteger)sectionIndex {
    
    if (APP_DEBUG_MODE) { NSLog(@"CollectionView row Selected : %ld in section : %ld ",(long)rowSelected, (long)sectionIndex); }
    
}

#pragma mark - Get Appointments

- (void)getAllAppointmentsInfoObject {
    
    NSString *customerID = [[NSUserDefaults standardUserDefaults]valueForKey:KUserId];
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_ALL_APPOINTMENTS];
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];

    NSDictionary *dictInfo = [NSDictionary dictionaryWithObjectsAndKeys:accessToken,@"Authorization",customerID,@"CustomerId",nil];
    
    if (internetStatus != NotReachable) {
        
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonPostData:url params:dictInfo onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                
                @try {
                    if (completed) {
                        
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSMutableArray *tempInfoArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE)
                            NSLog(@"Appointment info Array is %@", tempInfoArray);
                        AllAppointmentsInfo *appointmentInfo = [AllAppointmentsInfo getSharedInstance];
                        self.allAppointmentArray = [appointmentInfo allAppointmentsInfoObject:tempInfoArray];
                        if (APP_DEBUG_MODE)
                            NSLog(@"All Appointment Array  is %@", self.allAppointmentArray );
                        [self viewForNoAppointmentsAvailable];
                        [self switchToDetailView];
                    }else {
                        if (APP_DEBUG_MODE)
                            NSLog(@"error is %@", error.localizedDescription);
                    }
                    
                } @catch (NSException *exception) {
                    [iBeautyUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
    
}

#pragma mark - Switch to Detail View

- (void)switchToDetailView {
    
    AppDelegate *delegate = [AppDelegate sharedInstance];
    
    NSString *bookingId = [delegate.dictInfo valueForKey:@"BookingId"];
    
    if (delegate.navigateStatus == YES) {
        
        AppointmentDetailViewController *detailController  = [self.storyboard instantiateViewControllerWithIdentifier:kAppointmentDetailController];
        
        AllAppointmentsInfo *detailObj;
        
        for (AllAppointmentsInfo *infoObj in self.allAppointmentArray) {
            
            if ([bookingId isEqualToString:infoObj.orderId]) {
                detailObj = infoObj;
            }
        }
        detailController.appointmentDetailInfo = detailObj;
        [self.navigationController pushViewController:detailController animated:YES];
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
