//
//  OffersDetailViewController.h
//  iBeauty
//
//  Created by App Innovation on 11/09/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OffersDetail.h"
#import "PromotionalAds.h"
#import "ServicesCategoriesInfo.h"

@interface OffersDetailViewController : UIViewController

@property (nonatomic,strong) OffersDetail *offerInfo;
@property (nonatomic,assign) BOOL screenStatus;
@property (nonatomic,assign) NSInteger indexValue;
@property (nonatomic,strong) PromotionalAds *promotionOfferInfo;

@property (nonatomic,assign) BOOL offerTypeStatus;

@property (nonatomic,strong) ServicesCategoriesInfo *categoryInfoObj;

@end
