//
//  SelectTimeSlotController.m
//  iBeauty
//
//  Created by App Innovation on 04/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "SelectTimeSlotController.h"
#import "FSCalendar.h"
#import "SlotCollectionCell.h"
#import "NSDate+iBeautyDateFormatter.h"

static NSString *slotCellIdentifier = @"SlotCollectionCell";

@interface SelectTimeSlotController ()<FSCalendarDelegate,FSCalendarDataSource,UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic,strong) NSMutableArray *staffInfoArray;
@property (nonatomic,strong) NSMutableArray *slotsInfoArray;
@property (nonatomic,weak)   IBOutlet UIBarButtonItem  *doneButton;

@property (weak, nonatomic) FSCalendar *calendar;
@property (weak, nonatomic) UIButton *previousButton;
@property (weak, nonatomic) UIButton *nextButton;

@property (strong, nonatomic) NSCalendar *gregorian;
@property (weak,   nonatomic)   IBOutlet NSLayoutConstraint *calendarHeightConstraint;
@property (strong, nonatomic) NSDateFormatter *dateFormatter;

@property (weak, nonatomic) IBOutlet UINavigationBar *navigationBar;
@property (nonatomic, assign)  NSInteger previousMonth;

@property (weak, nonatomic) IBOutlet UIView *appointmentsSlotView;
@property (weak, nonatomic) IBOutlet UIView *slotsView;
@property (weak, nonatomic) IBOutlet UIView *noSlotsView;
@property (weak, nonatomic) IBOutlet UILabel *noSlotLabel;

@property (nonatomic,weak) IBOutlet UICollectionView *slotCollectionView;
@property (nonatomic,assign) NSInteger selectedIndex;
@property (nonatomic,strong) NSDate *selectedDate;
@property (nonatomic,strong) ServiceTimeSlot *timeSlotObj;

@property (nonatomic,strong) NSString *deviceType;

@property UINavigationBar *navBar;

- (void)previousClicked:(id)sender;
- (void)nextClicked:(id)sender;

@end

@implementation SelectTimeSlotController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.timeSlotObj = self.serviceInfo.slotInfo;
    if (self.timeSlotObj.selectedDate != nil) {
        _calendar.appearance.todayColor = [UIColor clearColor];
        _calendar.appearance.titleTodayColor = [UIColor blackColor];
        [_calendar selectDate:self.timeSlotObj.selectedDate scrollToDate:YES];
        NSString *dateString =  [self.timeSlotObj.selectedDate convertDateValueToNSString:CALENDAR_DATE_FORMAT localeStatus:false];
        self.selectedDate = self.timeSlotObj.selectedDate;
        [self getDateTimeSlotsInfo:dateString];
    }else {
        _calendar.appearance.todayColor = [UIColor iBeautyThemeColor];
        _calendar.appearance.titleTodayColor = [UIColor whiteColor];
//        NSString *todayDateStr =  [self.dateFormatter stringFromDate:[NSDate date]];
        self.selectedDate = [NSDate date];
        NSString *todayDateStr =  [[NSDate date] convertDateValueToNSString:CALENDAR_DATE_FORMAT localeStatus:false];
        [self getDateTimeSlotsInfo:todayDateStr];
        self.selectedIndex = -1;
    }

    [self setUpViews];
    [self setCalendarLocale];
    [self setStatusBarColor];
}

#pragma mark - Set Gradient

- (void)setGradient {
    
    self.navBar.frame = [iBeautyUtility setUpFrame:self.navBar.frame];
    CAGradientLayer *gradient = [iBeautyUtility setNavigationGradient:self.navBar];
    [self.navBar setBackgroundImage:[iBeautyUtility imageFromLayer:gradient] forBarMetrics:UIBarMetricsDefault];
    
}

#pragma mark - Set Status Bar Color

- (void)setStatusBarColor {

    if (@available(iOS 13.0, *)) {
        
        UIView *statusBar = [[UIView alloc]initWithFrame:[UIApplication sharedApplication].keyWindow.windowScene.statusBarManager.statusBarFrame] ;
        [self.view addSubview:statusBar];
        if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
            
            statusBar.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:130/255.0 blue:90/255.0 alpha:1.0];
            //[statusBar.layer insertSublayer:[self setGradientColor_Views:statusBar] atIndex:0];
        }
    }else {
            UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
            if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
                
                statusBar.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:130/255.0 blue:90/255.0 alpha:1.0];
               // [statusBar.layer insertSublayer:[self setGradientColor_Views:statusBar] atIndex:0];
            }
    }

}

- (CAGradientLayer*)setGradientColor_Views:(UIView*)sender {
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = sender.bounds;
    gradient.startPoint = CGPointMake(0, 0);
    gradient.endPoint = CGPointMake(1, 0);
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:233.0/255.0 green:83/255.0 blue:158/255.0 alpha:1.0] CGColor],(id)[[UIColor colorWithRed:240/255.0 green:59.0/255.0 blue:53.0/255.0 alpha:1.0] CGColor], nil];
    
    return gradient;
}

- (void)setCalendarLocale {
    
    NSString *languageCode =   [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];
    if ([languageCode isEqualToString:@"en"]) {
        self.calendar.locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
    }else if ([languageCode isEqualToString:@"ar"]) {
        self.calendar.locale = [NSLocale localeWithLocaleIdentifier:@"ar_SA"];
    }
    
}

- (void)setUpViews {
    
    [self setBarButtonItems];
    
    if ([self.deviceType isEqualToString:@"iPhone"]) {
        
        if (IS_IPHONE_4 || IS_IPHONE_5 || IS_IPHONE_6 || IS_IPHONE_6_PLUS) {
            [self.appointmentsSlotView setFrame:CGRectMake(0.0f, 64+self.calendar.frame.size.height, self.view.frame.size.width, [UIScreen mainScreen].bounds.size.height -64-self.calendar.frame.size.height)];
        }else {
            [self.appointmentsSlotView setFrame:CGRectMake(0.0f, 88+self.calendar.frame.size.height, self.view.frame.size.width, [UIScreen mainScreen].bounds.size.height -64-self.calendar.frame.size.height)];
        }
    }else {
        [self.appointmentsSlotView setFrame:CGRectMake(0.0f, 64+self.calendar.frame.size.height, self.view.frame.size.width, [UIScreen mainScreen].bounds.size.height -64-self.calendar.frame.size.height)];
    }

    [self.view addSubview:self.appointmentsSlotView];

}

- (void)setBarButtonItems {
    
    if ([self.deviceType isEqualToString:@"iPhone"]) {
        
        if (IS_IPHONE_4 || IS_IPHONE_5 || IS_IPHONE_6 || IS_IPHONE_6_PLUS) {
            self.navBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 20.0f, self.view.frame.size.width, 44)];
        }else {
            self.navBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 44.0f, self.view.frame.size.width, 44)];
        }
    }else {
        self.navBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 20.0f, self.view.frame.size.width, 44)];
    }

    UINavigationItem* navItem = [[UINavigationItem alloc] initWithTitle:NSLocalizedString(@"Schedule appointment", @"")];
    //[self.navBar setBarTintColor:[UIColor iBeautyThemeColor]];
    //[self setGradient];
    self.navBar.translucent = NO;

    self.navBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};

    
    UIBarButtonItem* cancelBtn = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", @"") style:UIBarButtonItemStylePlain target:self action:@selector(closeActionMethod:)];
    cancelBtn.tintColor = [UIColor whiteColor];
    navItem.leftBarButtonItem = cancelBtn;

    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", @"") style:UIBarButtonItemStylePlain target:self action:@selector(doneButton:)];
    doneBtn.tintColor = [UIColor whiteColor];
    navItem.rightBarButtonItem = doneBtn;
    
    [self.navBar setItems:@[navItem]];
    [self.view addSubview:self.navBar];
    
}

#pragma mark - Get staff details

- (void)getDateTimeSlotsInfo:(NSString*)dateString {
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_GET_DATE_TIME_SLOTS];
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSString *serviceId = self.serviceId;
    NSString *businessId = self.categoryInfoObj.Id;
    NSString *staffId = self.staffInfo.staffId;
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    
    NSDictionary *dictInfo = [NSDictionary dictionaryWithObjectsAndKeys:accessToken,@"Authorization",businessId,@"BusinessId",dateString,@"Date",serviceId,@"Service",staffId,@"Staff", nil];
    
    if (internetStatus != NotReachable) {
        
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonPostData:url params:dictInfo onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                @try {
                    if (completed) {
                        
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSMutableArray *tempInfoArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE)
                            NSLog(@"Response Array is %@", tempInfoArray);
                        
                        if ([tempInfoArray isKindOfClass:[NSDictionary class]]) {
                            
//                            [iBeautyUtility showAlertMessage:[tempInfoArray valueForKey:@"response"]];
                            [self.slotsInfoArray removeAllObjects];
                            [self.slotCollectionView reloadData];
                            [self setUpNoSlotsView:self.slotsInfoArray];

                        }else {
                            
                            if (tempInfoArray.count == 1) {
                                NSDictionary *dictInfo = tempInfoArray[0];
                                [self setUpNoSlotsView:tempInfoArray];
                                if (![dictInfo valueForKey:@"StartDateTime"]) {
                                    [self.slotsInfoArray removeAllObjects];
                                    [self setUpNoSlotsView:tempInfoArray];
                                }
                            }else if (tempInfoArray.count == 0) {
                                [self setUpNoSlotsView:tempInfoArray];
                            }
                            
                            if (tempInfoArray.count>0) {
                                ServiceTimeSlot *timeSlotObj = [ServiceTimeSlot getSharedInstance];
                                NSMutableArray *filterArray =   [self filterAvaialbleSlots:tempInfoArray];
                                self.slotsInfoArray = [timeSlotObj serviceTimeSlotsInfoObject:filterArray];
                                [self setUpNoSlotsView:self.slotsInfoArray];
                            }
                        }

                        if (self.timeSlotObj != nil) {
                            
                            [self setSelectedTimeSlot];
                        }
                        
                        [self.slotCollectionView reloadData];
                    }else {
                        if (APP_DEBUG_MODE)
                            NSLog(@"error is %@", error.localizedDescription);
                    }
                    
                } @catch (NSException *exception) {
//                    [iBeautyUtility showAlertMessage:NO_MESSAGE];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}

- (void)setSelectedTimeSlot {
    
    for (int i = 0; i < self.slotsInfoArray.count; i++) {
        ServiceTimeSlot *infoObj = self.slotsInfoArray[i];
        if ([infoObj.startTime isEqualToString:self.timeSlotObj.startTime]) {
            infoObj.selectStatus = 1;
            self.selectedIndex = i;
        }else {
            infoObj.selectStatus = 0;
        }
    }
}

#pragma mark - Set Up No Slots View

- (void)setUpNoSlotsView:(NSMutableArray*)slotsArray {
    
    if (slotsArray.count == 0 || slotsArray.count == 1) {
        self.slotCollectionView.hidden = YES;
        [self.noSlotsView setFrame:CGRectMake(0.0f, self.slotsView.frame.size.height/2-self.noSlotsView.frame.size.height/2, SCREEN_WIDTH, self.noSlotsView.frame.size.height)];
        [self.slotsView addSubview:self.noSlotsView];
        self.noSlotLabel.text = NSLocalizedString(@"No slot avilable", @"");
    }else {
        self.slotCollectionView.hidden = NO;
        [self.noSlotsView removeFromSuperview];
    }
    
}

#pragma mark - filter available slots

- (NSMutableArray *)filterAvaialbleSlots:(NSMutableArray*)slotsArray {
    
    NSMutableArray *filterSlotsArray = [NSMutableArray new];
    
    
    for (NSDictionary *infoDict in slotsArray) {
        
        NSString *startTime = [infoDict valueForKey:@"StartDateTime"];
        
        NSDate *startDate =  [NSDate convertNSStringToNSDate:startTime format:DATE_FORMAT_NEW_SLOT];
        NSString *todayDateStr = [startDate convertDateToNSString:DATE_FORMAT_NEW_SLOT date:[NSDate date]];
        NSDate *todayDate =  [NSDate convertNSStringToNSDate:todayDateStr format:DATE_FORMAT_NEW_SLOT];
        if ([todayDate compare:startDate] == NSOrderedAscending) {
            [filterSlotsArray addObject:infoDict];
        }
    }
    
    return filterSlotsArray;
}


#pragma mark - CollectioView DataSource and Delegate methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.slotsInfoArray.count;
    
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    SlotCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:slotCellIdentifier forIndexPath:indexPath];
    
    ServiceTimeSlot *slotObj = self.slotsInfoArray[indexPath.row];
    [cell configureTimeSlotCollectionCell:slotObj];
    return cell;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat screenWidth = CGRectGetWidth([UIScreen mainScreen].bounds);
    
    CGFloat itemWidth = 0.0f;
    
    itemWidth = floor(screenWidth/4-5);
    
    return CGSizeMake(itemWidth,43.0f);
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 1.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 1.0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    return UIEdgeInsetsMake(5.0, 5.0, 5.0, 5.0);
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    self.selectedIndex = indexPath.row;
    [self setSlotStatus];
    [self.slotCollectionView reloadData];
    
}

- (void)setSlotStatus {
    
    for (int i = 0; i < self.slotsInfoArray.count; i++) {
        ServiceTimeSlot *infoObj = self.slotsInfoArray[i];
        if (i == self.selectedIndex) {
            infoObj.selectStatus = 1;
        }else {
            infoObj.selectStatus = 0;
        }
    }
    
}


#pragma mark - Load View Clicked

- (void)loadView {
    
    self.deviceType = [UIDevice currentDevice].model;

    self.dateFormatter = [[NSDateFormatter alloc] init];
    self.dateFormatter.dateFormat = @"yyyy-MM-dd";
    
    self.gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];

    UIView *view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.view = view;
    
    // 450 for iPad and 300 for iPhone
    CGFloat height = [[UIDevice currentDevice].model hasPrefix:@"iPad"] ? 450 : 300;
    FSCalendar *calendar;
    
    if ([self.deviceType isEqualToString:@"iPhone"]) {
        
        if (IS_IPHONE_4 || IS_IPHONE_5 || IS_IPHONE_6 || IS_IPHONE_6_PLUS) {
            calendar = [[FSCalendar alloc] initWithFrame:CGRectMake(0, 64, view.frame.size.width, height)];
            
        }else {
            calendar = [[FSCalendar alloc] initWithFrame:CGRectMake(0, 88, view.frame.size.width, height)];
        }
    }else {
        calendar = [[FSCalendar alloc] initWithFrame:CGRectMake(0, 64, view.frame.size.width, height)];
    }

    calendar.dataSource = self;
    calendar.delegate = self;
    calendar.backgroundColor = [UIColor whiteColor];
    calendar.appearance.headerMinimumDissolvedAlpha = 0;
    calendar.appearance.caseOptions = FSCalendarCaseOptionsHeaderUsesUpperCase;
    [self.view addSubview:calendar];
    self.calendar = calendar;
    [self calendarViewsSetUp];
    
    UIButton *previousButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];

    if ([self.deviceType isEqualToString:@"iPhone"]) {
        
        if (IS_IPHONE_4 || IS_IPHONE_5 || IS_IPHONE_6 || IS_IPHONE_6_PLUS) {
            previousButton.frame = CGRectMake(0, 64+5, 95, 34);
            nextButton.frame = CGRectMake(CGRectGetWidth(self.view.frame)-95, 64+5, 95, 34);
        }else {
            previousButton.frame = CGRectMake(0, 88+5, 95, 34);
            nextButton.frame = CGRectMake(CGRectGetWidth(self.view.frame)-95, 88+5, 95, 34);
        }
    }else {
        previousButton.frame = CGRectMake(0, 69+5, 95, 34);
        nextButton.frame = CGRectMake(CGRectGetWidth(self.view.frame)-95, 69+5, 95, 34);
    }

    previousButton.backgroundColor = [UIColor whiteColor];
    previousButton.titleLabel.font = [UIFont fontWithName:@"SegoeUI" size:15.0];
    [previousButton setImage:[UIImage imageNamed:@"icon_prev"] forState:UIControlStateNormal];
    [previousButton addTarget:self action:@selector(previousClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:previousButton];
    self.previousButton = previousButton;
    [self.previousButton setHidden:YES];
    
    nextButton.backgroundColor = [UIColor whiteColor];
    nextButton.titleLabel.font = [UIFont fontWithName:@"SegoeUI" size:15.0];
    [nextButton setImage:[UIImage imageNamed:@"icon_next"] forState:UIControlStateNormal];
    [nextButton addTarget:self action:@selector(nextClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:nextButton];
    self.nextButton = nextButton;
  
}

- (void)calendarViewsSetUp {
    
    _calendar.allowsMultipleSelection = NO;
    _calendar.appearance.weekdayTextColor = [UIColor blackColor];
    _calendar.appearance.headerTitleColor = [UIColor blackColor];
    _calendar.appearance.eventDefaultColor = [UIColor iBeautyThemeColor];
    _calendar.appearance.selectionColor = [UIColor iBeautyThemeColor];
    _calendar.appearance.todayColor = [UIColor iBeautyThemeColor];
    _calendar.appearance.todaySelectionColor = [UIColor iBeautyThemeColor];
}


#pragma mark - Previous Clicked

- (void)previousClicked:(id)sender {
    
    NSDate *currentMonth = self.calendar.currentPage;
    
    NSComparisonResult result = [currentMonth compare:[NSDate date]];
    
    if (result == NSOrderedAscending) {
        [self.previousButton setHidden:YES];
    }else {
        NSDate *previousMonth = [[NSCalendar currentCalendar] dateByAddingUnit:NSCalendarUnitMonth value:-1 toDate:currentMonth options:0];
        [self.calendar setCurrentPage:previousMonth animated:YES];
    }

}

#pragma mark - Next Clicked

- (void)nextClicked:(id)sender {
    
    NSDate *currentMonth = self.calendar.currentPage;
    NSDate *nextMonth = [[NSCalendar currentCalendar] dateByAddingUnit:NSCalendarUnitMonth value:1 toDate:currentMonth options:0];
    [self.calendar setCurrentPage:nextMonth animated:YES];
    
    [self.previousButton setHidden:NO];

}

#pragma mark - <FSCalendarDelegate>

- (void)calendar:(FSCalendar *)calendar boundingRectWillChange:(CGRect)bounds animated:(BOOL)animated {
    
    // NSLog(@"%@",(calendar.scope==FSCalendarScopeWeek?@"week":@"month"));
    _calendarHeightConstraint.constant = CGRectGetHeight(bounds);
    [self.view layoutIfNeeded];
}

- (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition {
    
    NSString *selectedDateStr = [self.dateFormatter stringFromDate:date];
    if (APP_DEBUG_MODE)
       NSLog(@"did select date str is %@",selectedDateStr);
    self.selectedDate = [NSDate convertNSStringToNSDate:selectedDateStr format:CALENDAR_DATE_FORMAT];
    
    NSMutableArray *selectedDates = [NSMutableArray arrayWithCapacity:calendar.selectedDates.count];
    [calendar.selectedDates enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [selectedDates addObject:[self.dateFormatter stringFromDate:obj]];
    }];
    if (APP_DEBUG_MODE)
        NSLog(@"selected dates is %@",selectedDates);
    if (monthPosition == FSCalendarMonthPositionNext || monthPosition == FSCalendarMonthPositionPrevious) {
        [calendar setCurrentPage:date animated:YES];
    }
    self.timeSlotObj = nil;
    _calendar.appearance.todayColor = [UIColor clearColor];
    _calendar.appearance.titleTodayColor = [UIColor blackColor];

    NSString *todayDateStr =  [date convertDateValueToNSString:CALENDAR_DATE_FORMAT localeStatus:false];

    [self getDateTimeSlotsInfo:todayDateStr];
}

- (void)calendarCurrentPageDidChange:(FSCalendar *)calendar {
    
    NSLog(@"%s %@", __FUNCTION__, [self.dateFormatter stringFromDate:calendar.currentPage]);
    
    NSDate *currentMonth = calendar.currentPage;
    
    NSComparisonResult result = [currentMonth compare:[NSDate date]];
    
    if (result == NSOrderedAscending) {
        [self.previousButton setHidden:YES];
    }else {
        [self.previousButton setHidden:NO];
    }
}

// FSCalendarDelegate
- (BOOL)calendar:(FSCalendar *)calendar shouldSelectDate:(NSDate *)date {
    
//    if ([dateShouldNotBeSelected]) {
//        return NO;
//    }
    return YES;
}


// FSCalendarDataSource
- (NSDate *)minimumDateForCalendar:(FSCalendar *)calendar {
    
    if (self.screenStatus == NO) {
        return [NSDate date];
    }else {
        NSString *nowDateString = [[NSDate date] convertDateToNSString:CALENDAR_DATE_FORMAT date:[NSDate date]];
        NSDate *nowDate = [NSDate convertNSStringToNSDate:nowDateString format:CALENDAR_DATE_FORMAT];
        NSDate *startDate = [self.offerInfo.offer_StartDate withFormat:CALENDAR_DATE_FORMAT];

        if ([nowDate compare:startDate] == NSOrderedDescending || [nowDate compare:startDate] == NSOrderedSame) {
            return nowDate;
        }else {
            return startDate;
        }
    }
}

- (NSDate *)maximumDateForCalendar:(FSCalendar *)calendar {
    
    NSDate *maximumDate;
    NSCalendar *theCalendar = [NSCalendar currentCalendar];

    if (self.screenStatus == NO) {
        NSDateComponents *yearComponent = [[NSDateComponents alloc] init];
        yearComponent.year = 1;
        maximumDate = [theCalendar dateByAddingComponents:yearComponent toDate:[NSDate date] options:0];
    }else {
    
        maximumDate = self.offerInfo.offer_EndDate;
    }

    return maximumDate;
}

#pragma mark - UIBarButton Item Action

- (IBAction)doneButton:(UIBarButtonItem*)sender {
    
    NSMutableDictionary *dictInfo = [[NSMutableDictionary alloc]init];
    if (self.selectedIndex == -1) {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"Please select time", @"")];
        return;
    }
    ServiceTimeSlot *infoObj = self.slotsInfoArray[self.selectedIndex];
    if (self.selectedDate != nil) {
        infoObj.selectedDate = self.selectedDate;
    }else {
        infoObj.selectedDate = [NSDate date];
    }
    [dictInfo setObject:infoObj forKey:@"slotInfo"];
    [dictInfo setValue:[NSNumber numberWithInteger:self.slotIndex] forKey:@"Index"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"selectedSLotInfo" object:nil userInfo:dictInfo];
    
    [self dismissViewControllerAnimated:YES completion:nil];

}

#pragma mark - Back Action Method

- (IBAction)closeActionMethod:(UIBarButtonItem*)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 
*/

@end
