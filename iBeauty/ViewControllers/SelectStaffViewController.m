//
//  SelectStaffViewController.m
//  iBeauty
//
//  Created by App Innovation on 04/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "SelectStaffViewController.h"
#import "SelectStaffCollCell.h"
#import "Constants.h"

static NSString *staffCellIdentifier = @"selectStaffCell";

@interface SelectStaffViewController ()<UICollectionViewDataSource,UICollectionViewDelegate>

@property (nonatomic,strong) NSMutableArray *staffMaleArray;
@property (nonatomic,strong) NSMutableArray *staffFemaleArray;

@property (nonatomic,weak) IBOutlet UICollectionView *staffCollectionView;
@property (nonatomic,weak) IBOutlet UIBarButtonItem  *maleButton;
@property (nonatomic,weak) IBOutlet UIBarButtonItem  *femaleButton;
@property (nonatomic,strong) NSMutableArray *tempInfoArray;
@property (nonatomic,weak)  IBOutlet UINavigationBar *navBar;

@end

@implementation SelectStaffViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    [self getStaffDetailsInfo];
    
    if (APP_DEBUG_MODE) { NSLog(@"Staff Info Array is %@",self.staffInfoArray); }
    [self filterArrayAccordingToGender];
    //[self setGradient];
    [self setStatusBarColor];

}

#pragma mark - Set Gradient

- (void)setGradient {
    
    self.navBar.frame = [iBeautyUtility setUpFrame:self.navBar.frame];
    CAGradientLayer *gradient = [iBeautyUtility setNavigationGradient:self.navBar];
    [self.navBar setBackgroundImage:[iBeautyUtility imageFromLayer:gradient] forBarMetrics:UIBarMetricsDefault];
}

#pragma mark - Set Status Bar Color

- (void)setStatusBarColor {

    if (@available(iOS 13.0, *)) {
        
        UIView *statusBar = [[UIView alloc]initWithFrame:[UIApplication sharedApplication].keyWindow.windowScene.statusBarManager.statusBarFrame] ;
        [self.view addSubview:statusBar];
        if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
            
            statusBar.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:130/255.0 blue:90/255.0 alpha:1.0];
            //[statusBar.layer insertSublayer:[self setGradientColor_Views:statusBar] atIndex:0];
        }
    }else {
            UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
            if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
                statusBar.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:130/255.0 blue:90/255.0 alpha:1.0];

                //[statusBar.layer insertSublayer:[self setGradientColor_Views:statusBar] atIndex:0];
            }
    }

}

- (CAGradientLayer*)setGradientColor_Views:(UIView*)sender {
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = sender.bounds;
    gradient.startPoint = CGPointMake(0, 0);
    gradient.endPoint = CGPointMake(1, 0);
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:233.0/255.0 green:83/255.0 blue:158/255.0 alpha:1.0] CGColor],(id)[[UIColor colorWithRed:240/255.0 green:59.0/255.0 blue:53.0/255.0 alpha:1.0] CGColor], nil];
    
    return gradient;
}

#pragma mark - Get staff details

- (void)getStaffDetailsInfo {
    
    NSString *url = [ConnectionHandler getServiceURL:TYPE_SERVICE_GET_STAFF_DETAILS];
    url = [NSString stringWithFormat:@"%@%ld",url,(long)self.serviceInfo.serviceId];
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable) {
        
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonGETData:url parameters:nil onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                
                @try {
                    if (completed) {
                        StaffDetail *staffObj = [StaffDetail getSharedInstance];
                        self.tempInfoArray = [staffObj staffDetailInfoObject:completed];
                        if (APP_DEBUG_MODE)
                            NSLog(@"Services Array is %@", self.tempInfoArray);
                        [self filterArrayAccordingToGender];
                        [self.staffCollectionView reloadData];
                    }else {
                        if (APP_DEBUG_MODE)
                            NSLog(@"error is %@", error.localizedDescription);
                    }
                    
                } @catch (NSException *exception) {
                    //[EShopUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}

#pragma mark - Filter Array accordintToGender

- (void)filterArrayAccordingToGender {
    
    self.staffMaleArray = [NSMutableArray new];
    self.staffFemaleArray = [NSMutableArray new];
    
    for (StaffDetail *staffInfo in self.staffInfoArray) {
        
        if ([staffInfo.gender isEqualToString:@"1"]) {
            
            [self.staffMaleArray addObject:staffInfo];
        }else {
            [self.staffFemaleArray addObject:staffInfo];
        }
    }
    if (self.staffInfoArray.count<1) {
        self.staffInfoArray = [self.staffMaleArray mutableCopy];
    }
}


#pragma mark - UIBarButton Item Action

- (IBAction)selectMaleFemale:(UIBarButtonItem*)sender {
    
    if (sender.tag == 0) {

        self.staffInfoArray = [self.staffMaleArray mutableCopy];
    }else {
        
        self.staffInfoArray = [self.staffFemaleArray mutableCopy];

    }
    [self.staffCollectionView reloadData];

}


#pragma mark - CollectioView DataSource and Delegate methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.staffInfoArray.count;
    
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    SelectStaffCollCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:staffCellIdentifier forIndexPath:indexPath];
    
    StaffDetail *detailObj = self.staffInfoArray[indexPath.row];
    [cell configureStaffCollectionCell:detailObj];
    return cell;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat screenWidth = CGRectGetWidth([UIScreen mainScreen].bounds);
    
    CGFloat itemWidth = 0.0f;
    
    itemWidth = floor(screenWidth);
    
    return CGSizeMake(itemWidth, 90.0f);
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 1.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 1.0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    StaffDetail *infoObj = self.staffInfoArray[indexPath.row];
    infoObj.isSelected = YES;
    NSMutableDictionary *dictInfo = [[NSMutableDictionary alloc]init];
    [dictInfo setObject:infoObj forKey:@"staffInfo"];
    [dictInfo setValue:[NSNumber numberWithInteger:self.serviceIndex] forKey:@"Index"];

    [[NSNotificationCenter defaultCenter] postNotificationName:@"selectedStaffInfo" object:nil userInfo:dictInfo];
    
    [self dismissViewControllerAnimated:YES completion:nil];

}

#pragma mark - Back Action Method

- (IBAction)closeActionMethod {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
