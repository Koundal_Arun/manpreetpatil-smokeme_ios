//
//  ReferEarnViewController.m
//  iBeauty
//
//  Created by App Innovation on 27/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "ReferEarnViewController.h"
#import "Constants.h"

@interface ReferEarnViewController ()

@property (nonatomic,weak) IBOutlet UIView *noCouponsToReferView;
@property (nonatomic,weak) IBOutlet UILabel *noCouponsLabel;
@property (nonatomic,weak) IBOutlet UIButton *inviteFriendButton;
@property (nonatomic,weak)  IBOutlet UINavigationBar *navBar;

@end

@implementation ReferEarnViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self noCouponsForRefersEarnView];
    self.inviteFriendButton.frame = [iBeautyUtility setUpButtonFrame:self.inviteFriendButton.frame];
    [iBeautyUtility setButtonCorderRound:self.inviteFriendButton];
    
    //[self.inviteFriendButton.layer insertSublayer:[iBeautyUtility setGradientColor_Buttons:self.inviteFriendButton] atIndex:0];

    //[self setGradient];

}

#pragma mark - View Will Appear

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    self.tabBarController.tabBar.hidden = YES;
}

#pragma mark - Set Gradient

- (void)setGradient {
    
    self.navBar.frame = [iBeautyUtility setUpFrame:self.navBar.frame];
    CAGradientLayer *gradient = [iBeautyUtility setNavigationGradient:self.navBar];
    [self.navBar setBackgroundImage:[iBeautyUtility imageFromLayer:gradient] forBarMetrics:UIBarMetricsDefault];
}

#pragma mark - No Orders View

- (void)noCouponsForRefersEarnView {
    
    [self.view addSubview:self.noCouponsToReferView];
    self.noCouponsToReferView.center = self.view.center;
    
    self.noCouponsLabel.text = NSLocalizedString(@"No Coupons Available",@"");
    
}

#pragma mark - Back Action Method

- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Invite Friend

- (IBAction)inviteFriendWithAppLink {
    
    NSMutableArray *items = [NSMutableArray new];
    NSURL *url = [NSURL URLWithString:@"https://itunes.apple.com/us/app/iBeauty/id733059900?mt=8"];
    [items addObject:@"Try iBeauty app:"];
    [items addObject:url];
    
    // build an activity view controller
    UIActivityViewController *controller = [[UIActivityViewController alloc]initWithActivityItems:items applicationActivities:nil];
    [self presentActivityController:controller];
}

- (void)presentActivityController:(UIActivityViewController *)controller {
    
    // for iPad: make the presentation a Popover
    controller.modalPresentationStyle = UIModalPresentationPopover;
    [self presentViewController:controller animated:YES completion:nil];
    
    UIPopoverPresentationController *popController = [controller popoverPresentationController];
    popController.permittedArrowDirections = UIPopoverArrowDirectionAny;
    popController.barButtonItem = self.navigationItem.leftBarButtonItem;
    
    // access the completion handler
    controller.completionWithItemsHandler = ^(NSString *activityType,BOOL completed,NSArray *returnedItems,NSError *error){
        // react to the completion
        if (completed) {
        } else {
        }
        if (error) {
        }
    };
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
