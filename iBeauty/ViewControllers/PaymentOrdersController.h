//
//  PaymentOrdersController.h
//  iBeauty
//
//  Created by App Innovation on 12/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "BasketPriceSummary.h"
#import "PaymentOrderInfo.h"

@interface PaymentOrdersController : UIViewController

@property (nonatomic,strong) NSMutableArray   *paymentOrdersArray;
@property (nonatomic,strong) BasketPriceSummary   *basketInfo;
@property (nonatomic,strong) ServicesCategoriesInfo *categoryInfoObj;
@property (nonatomic,strong) NSMutableDictionary *transactionInfoDict;
@property (nonatomic,assign) NSInteger paymentMethodIndex;

@property (nonatomic,strong) NSMutableArray   *orderOffersArray;
@property (nonatomic,strong) PaymentOrderInfo * paymentOrderInfo;

@end
