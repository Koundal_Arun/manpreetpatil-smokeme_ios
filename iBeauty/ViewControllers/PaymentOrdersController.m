//
//  PaymentOrdersController.m
//  iBeauty
//
//  Created by App Innovation on 12/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "PaymentOrdersController.h"
#import "Constants.h"
#import "PaymentOrderHeaderView.h"
#import "PaymentOrderCollCell.h"
#import "PaymentOptions.h"
#import "BasketInfoObj.h"


static NSString *paymentCellIdentifier  = @"PaymentOrderCollCell";
static NSString *paymentHeaderIdentifier  = @"PaymentOrderHeaderView";

@interface PaymentOrdersController ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic,strong) NSString * basketID;
@property (nonatomic,weak) IBOutlet UICollectionView *paymentOrdersCollectionView;
@property (nonatomic,weak) IBOutlet UIButton *backToHomeButton;
@property (nonatomic,weak)  IBOutlet UINavigationBar *navBar;

@end

@implementation PaymentOrdersController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.backToHomeButton.frame = [iBeautyUtility setUpButtonFrame:self.backToHomeButton.frame];
    [iBeautyUtility setButtonCorderRound:self.backToHomeButton];
    
    //[self.backToHomeButton.layer insertSublayer:[iBeautyUtility setGradientColor_Buttons:self.backToHomeButton] atIndex:0];

    //[self setGradient];

}

#pragma mark - Set Gradient

- (void)setGradient {
    
    self.navBar.frame = [iBeautyUtility setUpFrame:self.navBar.frame];
    CAGradientLayer *gradient = [iBeautyUtility setNavigationGradient:self.navBar];
    [self.navBar setBackgroundImage:[iBeautyUtility imageFromLayer:gradient] forBarMetrics:UIBarMetricsDefault];
}

#pragma mark - Back Action

- (IBAction)backActionMethod:(id)sender {
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}

#pragma mark - CollectioView DataSource and Delegate methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.paymentOrdersArray.count;
    
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    PaymentOrderCollCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:paymentCellIdentifier forIndexPath:indexPath];
    
    BasketInfoObj *basketInfo = self.paymentOrdersArray[indexPath.row];
    [cell configurePaymentOrderCollectionCell:basketInfo];
    
    return cell;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat screenWidth = CGRectGetWidth([UIScreen mainScreen].bounds);
    
    CGFloat itemWidth = 0.0f;
    
    itemWidth = floor(screenWidth);
    
    return CGSizeMake(itemWidth, 35.0f);
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    
    CGFloat screenWidth = CGRectGetWidth([UIScreen mainScreen].bounds);
    
    if (section == 0) {
        return CGSizeMake(screenWidth, 170.0f);
    }else {
        return CGSizeMake(0.0f, 0.0f);
    }
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    
    return CGSizeMake(0.0f, 0.0f);
 
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 1.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 1.0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    id reusableview = [[UICollectionReusableView alloc]init];
    
    if (kind == UICollectionElementKindSectionHeader) {
        
        if (indexPath.section == 0) {
            
            PaymentOrderHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:paymentHeaderIdentifier forIndexPath:indexPath];
            [headerView configurePaymentOrdersCollectionHeaderView:self.paymentOrderInfo];
            reusableview = headerView;
        }

    }
    
    return reusableview;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
