//
//  AppointmentDetailViewController.h
//  iBeauty
//
//  Created by App Innovation on 13/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AllAppointmentsInfo.h"
#import <CoreLocation/CoreLocation.h>

@interface AppointmentDetailViewController : UIViewController

@property (nonatomic,strong) AllAppointmentsInfo *appointmentDetailInfo;

@end
