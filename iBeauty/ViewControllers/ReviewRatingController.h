//
//  ReviewRatingController.h
//  iBeauty
//
//  Created by App Innovation on 14/11/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AllAppointmentsInfo.h"

NS_ASSUME_NONNULL_BEGIN

@interface ReviewRatingController : UIViewController

@property (nonatomic,strong) AllAppointmentsInfo *appointmentInfo;

@end

NS_ASSUME_NONNULL_END
