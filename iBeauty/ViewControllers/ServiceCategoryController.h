//
//  ServiceCategoryController.h
//  iBeauty
//
//  Created by App Innovation on 28/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "RecommendedCategoryCell.h"

@interface ServiceCategoryController : UIViewController<CustomCellDelegate,UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) Services *serviceInfo;
@property (nonatomic,strong) NSMutableArray *promotionImages;
@property (nonatomic,strong) NSMutableArray *promotionAdsInfo;

@property (nonatomic,assign) BOOL providerStatus;
@property (nonatomic,strong) NSString *selectedCityName;

@end
