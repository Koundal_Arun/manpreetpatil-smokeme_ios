//
//  CategoryDetailController.m
//  iBeauty
//
//  Created by App Innovation on 29/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "CategoryDetailController.h"
#import "ServicesCollectionCell.h"
#import "NSString+iBeautyString.h"
#import "NSDate+iBeautyDateFormatter.h"
#import "AboutCollectionCell.h"
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "OffersCollectionCell.h"
#import "OffersDetail.h"
#import "ProviderCollectionHeaderView.h"
#import "BusinessReviews.h"
#import "OffersDetailViewController.h"
#import "ServicesDetailCollCell.h"
#import "StandardOpeningHours.h"
#import "UILabel+FlexibleWidHeight.h"


static NSString *servicesCellIdentifier = @"ServicesCell";
static NSString *offersCellIdentifier = @"OfferCollectionCell";
static NSString *aboutCellIdentifier = @"aboutCollCell";
static NSString *servicesDetailCellIdentifier  = @"servicesDetailCollCell";
static NSString *prductsDetailCellIdentifier  = @"ProductDetailCollCell";

static NSString *provideHeaderViewIdentifier = @"ProviderCollectionHeaderView";
static NSString *offerProductIdentifier = @"OfferProductCell";

@interface CategoryDetailController ()<UICollectionViewDelegate,UICollectionViewDataSource,CLLocationManagerDelegate,MKMapViewDelegate>

@property (nonatomic,strong) NSMutableArray *servicesArray;
@property (nonatomic,strong) NSMutableArray *offersArray;
@property (nonatomic,strong) NSMutableArray *reviewsArray;


@property (nonatomic,weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic,assign) NSInteger segmentIndex;

@property (nonatomic,weak) IBOutlet UIView *noOffersAvailableView;
@property (nonatomic,weak) IBOutlet UILabel *noOffersLable;

@property (nonatomic, weak) IBOutlet UIView *providerLocationView;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, weak) IBOutlet MKMapView *mapView;

@property (nonatomic, weak) IBOutlet UIView *currentView;
@property (nonatomic, weak) IBOutlet UILabel *currentDayValue;
@property (nonatomic, weak) IBOutlet UILabel *currentDayTitle;
@property (nonatomic, weak) IBOutlet UIView *dayTwoView;
@property (nonatomic, weak) IBOutlet UILabel *dayTwoValue;
@property (nonatomic, weak) IBOutlet UILabel *dayTwoTitle;
@property (nonatomic, weak) IBOutlet UIView *dayThreeView;
@property (nonatomic, weak) IBOutlet UILabel *dayThreeValue;
@property (nonatomic, weak) IBOutlet UILabel *dayThreeTitle;
@property (nonatomic, weak) IBOutlet UIView *dayFourView;
@property (nonatomic, weak) IBOutlet UILabel *dayFourValue;
@property (nonatomic, weak) IBOutlet UILabel *dayFourTitle;
@property (nonatomic, weak) IBOutlet UIView *dayFiveView;
@property (nonatomic, weak) IBOutlet UILabel *dayFiveValue;
@property (nonatomic, weak) IBOutlet UILabel *dayFiveTitle;
@property (nonatomic, weak) IBOutlet UIView *daySixView;
@property (nonatomic, weak) IBOutlet UILabel *daySixValue;
@property (nonatomic, weak) IBOutlet UILabel *daySixTitle;
@property (nonatomic, weak) IBOutlet UIView *daySevenView;
@property (nonatomic, weak) IBOutlet UILabel *daySevenValue;
@property (nonatomic, weak) IBOutlet UILabel *daySevenTitle;
@property (nonatomic, weak) IBOutlet UILabel *providerDescription;
@property (nonatomic, weak) IBOutlet UILabel *openingHoursLabel;

@property (nonatomic, weak) IBOutlet UIView *mapBkgView;
@property (nonatomic, weak) IBOutlet UIView *timingBkgView;

@property (nonatomic, weak) IBOutlet UIView *navigateView;
@property (nonatomic, weak) IBOutlet UIButton *navigateButton;

@property (nonatomic, weak) IBOutlet UIView *bookAppointmentView;
@property (nonatomic, weak) IBOutlet UIButton *bookAppointmentButton;

@property (nonatomic, weak) IBOutlet UIView *cartView;
@property (nonatomic, weak) IBOutlet UIButton *bookNowButton;
@property (nonatomic, weak) IBOutlet UILabel *cartItemLabel;

@property (nonatomic,strong) NSMutableArray *selectedServicesArray;
@property (nonatomic,strong) NSMutableArray *selectedOffersArray;
@property (nonatomic,strong) NSMutableArray *selectedProductsArray;

@property (nonatomic,assign) NSInteger totalAppointments;
@property (nonatomic,strong) NSString *distanceString;
@property (nonatomic,strong) NSString *deviceType;
@property (nonatomic,strong) NSString *currentDay;
@property (nonatomic,assign) CGFloat descriptionHeight;
@property (nonatomic,weak) IBOutlet UIView *viewMore;
@property (nonatomic,weak) IBOutlet UIImageView *arrowImageView;
@property (nonatomic,weak) IBOutlet UILabel *viewMoreLabel;
@property (nonatomic,weak) IBOutlet UIButton *viewMoreButton;
@property (nonatomic,assign) BOOL viewMoreStatus;
@property (nonatomic,strong) NSString *backStatus;
@property (nonatomic,assign) NSInteger imagePagerCount;
@property (nonatomic,weak) IBOutlet UILabel *getDirectionLabel;
@property (nonatomic,weak) IBOutlet UIView *allReviewsView;
@property (nonatomic,weak) IBOutlet UILabel *allReviewsLabel;

@property (nonatomic,strong) NSString *productBasketID;
@property (nonatomic,strong) NSMutableArray *productsInfoArray;
@property (nonatomic,strong) NSMutableArray *offersInfoArray;
@property (nonatomic,strong) AppDelegate *delegate;

@end

@implementation CategoryDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.imagePagerCount = 0;
    self.segmentIndex = 0;
    [self setUpViews];
    [self setUpCollectionView];
    self.delegate = [AppDelegate sharedInstance];

}


- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    
    if (APP_DEBUG_MODE) { NSLog(@"Select service array is %@",self.selectedServicesArray);}
    
    self.viewMoreLabel.text = NSLocalizedString(@"View More",@"");
    
    if (self.businessID.length < 1) {
        
        [self getServiceProviderDetailInfo:self.categoryInfoObj.Id];
    }else {
        self.collectionView.hidden = NO;
        [self setUpCategoryDetailView];
    }
    self.delegate.tabbarController.tabBar.hidden = YES;
}


#pragma mark - Get Service Provider Detail Info

- (void)getServiceProviderDetailInfo:(NSString*)businessID {
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_SERVICE_PROVIDER_DETAIL];
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSString *serviceId = businessID;
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];

    self.currentDay =  [[NSString string] getWeekDay];
    
    NSDictionary *dictInfo = [NSDictionary dictionaryWithObjectsAndKeys:accessToken,@"Authorization",serviceId,@"businessId",self.currentDay,@"dayName",nil];
    
    if (internetStatus != NotReachable) {
        
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonPostData:url params:dictInfo onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                @try {
                    if (completed) {
                        
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSMutableArray *tempInfoArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE)
                            NSLog(@"Temp Info array is %@", tempInfoArray);
                        
                        ServicesCategoriesInfo *serviceCatObj = [ServicesCategoriesInfo getSharedInstance];
                        self.mainInfoArray = [serviceCatObj serviceCategoryInfoObject:tempInfoArray];
                        
                        if (APP_DEBUG_MODE)
                             NSLog(@"Provider Info Array is %@", self.mainInfoArray);
                        
                        if (self.mainInfoArray.count > 0) {
                            self.collectionView.hidden = NO;
                            [self setUpCategoryDetailView];
                        }
                        
                    }else {
                        if (APP_DEBUG_MODE)
                            NSLog(@"error is %@", error.localizedDescription);
                        [SVProgressHUD dismiss];
                    }
                    
                } @catch (NSException *exception) {
                    //[EShopUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                    [SVProgressHUD dismiss];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}


- (void)deleteExisting_OffersFromSelectedList:(NSMutableArray *)deletedArray {
    
    NSMutableArray *tempArray = [self.selectedOffersArray mutableCopy];
    
    for (OffersDetail *existingOfferInfo in deletedArray) {
        
        for (OffersDetail *currentOfferInfo in tempArray) {
            
            if ([existingOfferInfo.offerId isEqualToString:currentOfferInfo.offerId]) {
                
                [self.selectedOffersArray removeObject:currentOfferInfo];
            }
        }
    }
    
}


#pragma mark - Load Segment

- (void)setUpCategoryDetailView {
    
    Services *serviceObj = [Services getSharedInstance];
    ServicesCategoriesInfo *catInfoObj = self.mainInfoArray[0];
    self.servicesArray = [serviceObj serviceInfoObject:catInfoObj.ServiceCategories];
    self.offersArray = catInfoObj.offersArray;
    self.reviewsArray = catInfoObj.reviews;
    self.categoryInfoObj = catInfoObj;
    self.categoryInfoObj.distance = self.distanceString;
    [self setUpSegmentControl];
}

#pragma mark - Set Up Views

- (void)setUpViews {
    
    NSString *itemsAdded = [self setUpItemsAddedLabel];
    self.cartItemLabel.text = [NSString stringWithFormat:@"%ld %@",(long)self.totalAppointments,itemsAdded];
    
    self.bookAppointmentView.hidden = YES;
    self.cartView.hidden = YES;
    self.collectionView.hidden = YES;
    [self setViewsBasedUponServiceType];
    
    self.selectedOffersArray = [NSMutableArray new];
    self.selectedServicesArray = [NSMutableArray new];

    self.bookAppointmentButton.frame = [iBeautyUtility setUpButtonFrame:self.bookAppointmentButton.frame];
    [iBeautyUtility setButtonCorderRound:self.bookAppointmentButton];
    //[self.bookAppointmentButton.layer insertSublayer:[iBeautyUtility setGradientColor_Buttons:self.bookAppointmentButton] atIndex:0];
    [iBeautyUtility setButtonCorderRound:self.bookNowButton];
    //[self.bookNowButton.layer insertSublayer:[iBeautyUtility setGradientColor_Buttons:self.bookNowButton] atIndex:0];

    if (self.categoryInfoObj.distance.length > 0) {
        self.distanceString = self.categoryInfoObj.distance;
    }
    
    self.deviceType = [UIDevice currentDevice].model;
}

- (void)setViewsBasedUponServiceType {
    
    [self.bookNowButton setTitle:NSLocalizedString(@"Book Now", @"") forState:UIControlStateNormal];
    
}


#pragma mark - Back Action

- (void)backActionMethod:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}


#pragma mark - Book Appointment Button Action

- (IBAction)bookAppointment:(id)sender {
    
    if (self.servicesArray.count > 0) {
        [self selectServices:0];
    }
    
}

#pragma mark - Book/Buy Appointment/Products


- (IBAction)bookAppointmentForOffers:(UIButton*)sender {

    if (self.selectedServicesArray.count>0) {
        
        BookAppointmentController *appointmentController = [self.storyboard instantiateViewControllerWithIdentifier:kAppointmentViewController];
        
        appointmentController.appointmentsDetailArray = self.selectedServicesArray;
        appointmentController.categoryInfoObj = self.categoryInfoObj;
        [self.navigationController pushViewController:appointmentController animated:YES];
        
    }else {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"Please select offers", @"")];
    }
        
}

#pragma mark - Set Up Collection View

- (void)setUpCollectionView {
    
    self.servicesArray = [NSMutableArray new];
    self.offersArray   = [NSMutableArray new];
    
    if (self.mainInfoArray.count < 1) {
        self.mainInfoArray = [NSMutableArray new];
    }
    
    UINib *collectionNib = [UINib nibWithNibName:@"ServicesCollectionCell" bundle:nil];
    [self.collectionView registerNib:collectionNib forCellWithReuseIdentifier:servicesCellIdentifier];
    
    UINib *offersCollectionNib = [UINib nibWithNibName:@"ServicesDetailCollCell" bundle:nil];
    [self.collectionView registerNib:offersCollectionNib forCellWithReuseIdentifier:servicesDetailCellIdentifier];
    
    UINib *aboutcollNib = [UINib nibWithNibName:@"AboutCollectionCell" bundle:nil];
    [self.collectionView registerNib:aboutcollNib forCellWithReuseIdentifier:aboutCellIdentifier];
    
    UINib *productDetailNib = [UINib nibWithNibName:@"ProductDetailCollCell" bundle:nil];
    [self.collectionView registerNib:productDetailNib forCellWithReuseIdentifier:prductsDetailCellIdentifier];
    
    UINib *offerProductNib = [UINib nibWithNibName:@"OfferProductCell" bundle:nil];
    [self.collectionView registerNib:offerProductNib forCellWithReuseIdentifier:offerProductIdentifier];
    
}

#pragma mark - Segmented Control

- (void)setUpSegmentControl {
    
    self.collectionView.translatesAutoresizingMaskIntoConstraints = YES;
    
    if (self.segmentIndex == 0) {
        
        if (self.servicesArray.count>0) {
            
            [self.noOffersAvailableView removeFromSuperview];
            
            self.bookAppointmentView.hidden = NO;
            self.cartView.hidden = YES;

        }else {
            [self viewForNoOffersAvailable:0];
            self.bookAppointmentView.hidden = YES;
        }
        [self setUpCollectionViewHeightWithCartView];
        
    }else if (self.segmentIndex == 1) {
        
        self.cartView.hidden = YES;

        if (self.offersArray.count > 0) {
            
            [self.noOffersAvailableView removeFromSuperview];
            self.cartView.hidden = YES;
            [self setUpCollectionViewHeightWithoutCartView];

        }else {
            [self viewForNoOffersAvailable:1];
            self.cartView.hidden = YES;
            [self setUpCollectionViewHeightWithoutCartView];
        }
        self.bookAppointmentView.hidden = YES;
        
    }else if (self.segmentIndex == 2) {
        [self.noOffersAvailableView removeFromSuperview];
        self.bookAppointmentView.hidden = YES;
        self.cartView.hidden = YES;
        [self setUpCollectionViewHeightWithoutCartView];
    }else {
        [self.noOffersAvailableView removeFromSuperview];
        self.bookAppointmentView.hidden = YES;
        self.cartView.hidden = YES;
    }
    
    [self.collectionView reloadData];
}

- (void)setUpCollectionViewHeightWithCartView {
    
    if(IS_IPHONE_X) {
        [self.collectionView setFrame:CGRectMake(0.0f, 20.0f, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-20-55.0f-34.0f)];
    }else {
        [self.collectionView setFrame:CGRectMake(0.0f, 20.0f, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-20-55.0f)];
    }
}

- (void)setUpCollectionViewHeightWithoutCartView {
    
    if(IS_IPHONE_X) {
        [self.collectionView setFrame:CGRectMake(0.0f, 20.0f, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-20-34.0f)];
    }else {
        [self.collectionView setFrame:CGRectMake(0.0f, 20.0f, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-20)];
    }
}

- (void)segmentControlValueChanged:(UISegmentedControl*)sender {
    
    self.segmentIndex = sender.selectedSegmentIndex;
    
    [self setUpSegmentControl];
    [self.collectionView reloadData];
}


#pragma mark - View For No Offers Available

- (void)viewForNoOffersAvailable:(NSInteger)segmentIndex {
    
    if ([self.deviceType isEqualToString:@"iPhone"]) {
        if (IS_IPHONE_5) {
            [self.noOffersAvailableView setFrame:CGRectMake(0.0f, 400.0f, [UIScreen mainScreen].bounds.size.width, self.noOffersAvailableView.frame.size.height)];
        }else {
            [self.noOffersAvailableView setFrame:CGRectMake(0.0f, 450.0f, [UIScreen mainScreen].bounds.size.width, self.noOffersAvailableView.frame.size.height)];
        }
    }else {
        [self.noOffersAvailableView setFrame:CGRectMake(0.0f, 550.0f, [UIScreen mainScreen].bounds.size.width, self.noOffersAvailableView.frame.size.height)];
    }

    [self.view addSubview:self.noOffersAvailableView];
    
    if (segmentIndex == 0) {
        self.noOffersLable.text = NSLocalizedString(@"No Service available at moment.", @"");
    }else {
        self.noOffersLable.text = NSLocalizedString(@"No Offers available at moment.", @"");
    }
    
}

#pragma mark - Map view set up

- (void)addServiceMapView:(ProviderCollectionHeaderView*)headerView {
    
    CGFloat YRect = 0.0f;

    if ([self.deviceType isEqualToString:@"iPhone"]) {
        
        if (IS_IPHONE_6 || IS_IPHONE_7 || IS_IPHONE_X ) {
            
            YRect = 362;
            
        }else if (IS_IPHONE_5) {
            YRect = 331;
        }else {
            YRect = 377;
        }
    }else {
        YRect = 472;
    }

    [self getTimeViewHeight];

    [self.providerLocationView setFrame:CGRectMake(0.0f, YRect, [UIScreen mainScreen].bounds.size.width, 46.0f + self.descriptionHeight + 20.0f + 378.0f)];
    self.mapView.delegate = self;
    [self.mapView setShowsUserLocation:NO];
    [self getServiceProviderLocation];
    [self setUpMapView];
    [headerView addSubview:self.providerLocationView];
    [self setAboutViewLocalization];
}

- (void)getServiceProviderLocation {
    
//    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(30.71293456735512, 76.69027539997791);
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([self.categoryInfoObj.latitude doubleValue],[self.categoryInfoObj.longitude doubleValue]);
    MKCoordinateSpan span = MKCoordinateSpanMake(0.6, 0.6);
    MKCoordinateRegion region = {coordinate, span};
    region = [self.mapView regionThatFits:region];
    [self.mapView setRegion:region];
    
}

#pragma mark - All Review View Set up

- (void)addAllReviewsView:(ProviderCollectionHeaderView*)headerView {
    
    CGFloat YRect = 0.0f;

      if ([self.deviceType isEqualToString:@"iPhone"]) {
          
          if (IS_IPHONE_6 || IS_IPHONE_7 || IS_IPHONE_X ) {
              
              YRect = 362;
              
          }else if (IS_IPHONE_5) {
              YRect = 331;
          }else {
              YRect = 377;
          }
      }else {
          YRect = 472;
      }

      [self.allReviewsView setFrame:CGRectMake(0.0f, YRect, [UIScreen mainScreen].bounds.size.width, 50.0f)];
    self.allReviewsLabel.text = [NSString stringWithFormat:@"All Reviews (%ld)",(long)self.categoryInfoObj.reviewsCount];
      [headerView addSubview:self.allReviewsView];
}

- (IBAction)viewMoreButton:(UIButton*)sender {
    
    if (self.viewMoreStatus == NO) {
        self.viewMoreStatus = YES;
        self.viewMoreLabel.text = NSLocalizedString(@"View Less",@"");
        self.arrowImageView.image = [UIImage imageNamed:@"up-arrow"];
    }else {
        self.viewMoreStatus = NO;
        self.viewMoreLabel.text = NSLocalizedString(@"View More",@"");
        self.arrowImageView.image = [UIImage imageNamed:@"down-arrow"];
    }
    [self.collectionView reloadData];

}

- (void)setAboutViewLocalization {
    
    self.openingHoursLabel.text = NSLocalizedString(@"Opening Hours",@"");
}

- (void)setUpMapView {
    
    NSMutableArray *openingHoursInfo = [self getArrayWithCurrentDayAtTop];
    
    if (openingHoursInfo.count > 0) {
        
        for (int i = 0; i < openingHoursInfo.count; i++) {
            
            StandardOpeningHours *hoursInfo = openingHoursInfo[i];

            if (i == 0) {
                
                self.currentDayTitle.text = hoursInfo.dayName;
                NSString *finalString  = [[NSString string] getOpengingHourStringFromModel:hoursInfo];
                if (hoursInfo.status == 1) {
                    self.currentDayValue.text = [NSString stringWithFormat:@": %@",finalString];
                    self.currentView.backgroundColor = [UIColor systemGreenColor];
                }else {
                    self.currentDayValue.text = [NSString stringWithFormat:NSLocalizedString(@": Closed",@"")];
                    self.currentView.backgroundColor = [UIColor systemRedColor];
                }
            }
            if (i == 1) {
                
                self.dayTwoTitle.text = hoursInfo.dayName;
                NSString *finalString  = [[NSString string] getOpengingHourStringFromModel:hoursInfo];
                if (hoursInfo.status == 1) {
                    self.dayTwoValue.text = [NSString stringWithFormat:@": %@",finalString];
                    self.dayTwoView.backgroundColor = [UIColor systemGreenColor];
                }else {
                    self.dayTwoValue.text = [NSString stringWithFormat:NSLocalizedString(@": Closed",@"")];
                    self.dayTwoView.backgroundColor = [UIColor systemRedColor];
                }
            }
            if (i == 2) {
                
                self.dayThreeTitle.text = hoursInfo.dayName;
                NSString *finalString  = [[NSString string] getOpengingHourStringFromModel:hoursInfo];
                if (hoursInfo.status == 1) {
                    self.dayThreeValue.text = [NSString stringWithFormat:@": %@",finalString];
                    self.dayThreeView.backgroundColor = [UIColor systemGreenColor];
                }else {
                    self.dayThreeValue.text = [NSString stringWithFormat:NSLocalizedString(@": Closed",@"")];
                    self.dayThreeView.backgroundColor = [UIColor systemRedColor];

                }
            }
            if (i == 3) {
                
                self.dayFourTitle.text = hoursInfo.dayName;
                NSString *finalString  = [[NSString string] getOpengingHourStringFromModel:hoursInfo];
                if (hoursInfo.status == 1) {
                    self.dayFourValue.text = [NSString stringWithFormat:@": %@",finalString];
                    self.dayFourView.backgroundColor = [UIColor systemGreenColor];
                }else {
                    self.dayFourValue.text = [NSString stringWithFormat:NSLocalizedString(@": Closed",@"")];
                    self.dayFourView.backgroundColor = [UIColor systemRedColor];
                }
            }
            if (i == 4) {
                
                self.dayFiveTitle.text = hoursInfo.dayName;
                NSString *finalString  = [[NSString string] getOpengingHourStringFromModel:hoursInfo];
                if (hoursInfo.status == 1) {
                    self.dayFiveValue.text = [NSString stringWithFormat:@": %@",finalString];
                    self.dayFiveView.backgroundColor = [UIColor systemGreenColor];
                }else {
                    self.dayFiveValue.text = [NSString stringWithFormat:NSLocalizedString(@": Closed",@"")];
                    self.dayFiveView.backgroundColor = [UIColor systemRedColor];
                }
            }
            if (i == 5) {
                
                self.daySixTitle.text = hoursInfo.dayName;
                NSString *finalString  = [[NSString string] getOpengingHourStringFromModel:hoursInfo];
                if (hoursInfo.status == 1) {
                    self.daySixValue.text = [NSString stringWithFormat:@": %@",finalString];
                    self.daySixView.backgroundColor = [UIColor systemGreenColor];
                }else {
                    self.daySixValue.text = [NSString stringWithFormat:NSLocalizedString(@": Closed",@"")];
                    self.daySixView.backgroundColor = [UIColor systemRedColor];
                }
            }
            if (i == 6) {
                
                self.daySevenTitle.text = hoursInfo.dayName;
                NSString *finalString  = [[NSString string] getOpengingHourStringFromModel:hoursInfo];
                if (hoursInfo.status == 1) {
                    self.daySevenValue.text = [NSString stringWithFormat:@": %@",finalString];
                    self.daySevenView.backgroundColor = [UIColor systemGreenColor];
                }else {
                    self.daySevenValue.text = [NSString stringWithFormat:NSLocalizedString(@": Closed",@"")];
                    self.daySevenView.backgroundColor = [UIColor systemRedColor];
                }
            }
        }
        
    }
    
    [self addPinAnnotation:self.categoryInfoObj];
    //[iBeautyUtility setViewBorderRound:self.navigateView];
}


- (void)getTimeViewHeight {
    
    self.timingBkgView.translatesAutoresizingMaskIntoConstraints = YES;
    self.providerDescription.translatesAutoresizingMaskIntoConstraints = YES;
    
    self.providerDescription.numberOfLines = 0;
    self.providerDescription.frame = CGRectMake(self.providerDescription.frame.origin.x, self.providerDescription.frame.origin.y, [UIScreen mainScreen].bounds.size.width-30.0f, self.descriptionHeight);
    self.providerDescription.text = self.categoryInfoObj.providerDescription;
    
    self.getDirectionLabel.text = [NSString stringWithFormat:@"Get direction - %@ KM",self.categoryInfoObj.distance];
    
    self.timingBkgView.frame = CGRectMake(self.timingBkgView.frame.origin.x, 46.0f + self.descriptionHeight + 20.0f, [UIScreen mainScreen].bounds.size.width-16.0f,378.0f);
    
}

- (NSMutableArray *)getArrayWithCurrentDayAtTop {
    
    NSMutableArray *tempArray = self.categoryInfoObj.StandardOpeningHours;
    NSMutableArray *hoursInfoArray1 = [NSMutableArray new];
    NSMutableArray *hoursInfoArray2 = [NSMutableArray new];
    NSMutableArray *finalArray = [NSMutableArray new];

    if (tempArray.count > 0) {
        int j = 0;
        for (int i = 0; i < tempArray.count; i++) {
            StandardOpeningHours *infoObj = tempArray[i];
            
            if ([self.currentDay isEqualToString:infoObj.dayName]) {
                
                j = 1;
            }

            if (j == 1) {
                [hoursInfoArray1 addObject:infoObj];
            }else {
                [hoursInfoArray2 addObject:infoObj];
            }
        }
        
        [finalArray addObjectsFromArray:hoursInfoArray1];
        [finalArray addObjectsFromArray:hoursInfoArray2];
        if (APP_DEBUG_MODE)
           NSLog(@"Final sorted array is %@",finalArray);
    }
    
    return finalArray;
}

- (void)addPinAnnotation:(ServicesCategoriesInfo*)categoryInfoObj {
    
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    float lat = [categoryInfoObj.latitude floatValue];
    float lang = [categoryInfoObj.longitude  floatValue];
    [annotation setCoordinate:CLLocationCoordinate2DMake(lat,lang)];
    [annotation setTitle:categoryInfoObj.categoryName];
    [annotation setSubtitle:@""];
    [self.mapView addAnnotation:annotation];
    
}

#pragma mark - Get Service Provider Detail Info

- (IBAction)navigateActionMethod:(UIButton*)sender {
    
    [self actionSheetForChooseMapTypeNavigation:sender];
}

#pragma mark - Action method for Navigate Options

- (void)actionSheetForChooseMapTypeNavigation:(UIButton*)sender {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Select Option",@"") message:nil
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *firstOption = [UIAlertAction actionWithTitle:NSLocalizedString(@"Apple Map",@"")
                                                            style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                [self navigateWithAppleMap];
                                                            }];
    UIAlertAction *secongOption = [UIAlertAction actionWithTitle:NSLocalizedString(@"Google Map",@"")
                                                            style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                [self navigateWithGoogleMap];
                                                            }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel",@"")
                                                           style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                               [self dismissViewControllerAnimated:YES completion:^{
                                                               }];
                                                           }];
    [alert addAction:firstOption];
    [alert addAction:secongOption];
    [alert addAction:cancelAction];
    
    NSString *deviceType = [UIDevice currentDevice].model;
    
    if (![deviceType isEqualToString:@"iPhone"]) {
        
        [alert setModalPresentationStyle:UIModalPresentationPopover];
        UIPopoverPresentationController *popPresenter = [alert popoverPresentationController];
        popPresenter.sourceView = sender;
        popPresenter.sourceRect = sender.bounds;
    }
    
    [self presentViewController:alert animated:YES completion:nil];
}


- (void)navigateWithAppleMap {
    
    NSString* directionsURL = [NSString stringWithFormat:@"http://maps.apple.com/?saddr=%f,%f&daddr=%f,%f",self.currentLocation.coordinate.latitude, self.currentLocation.coordinate.longitude, [self.categoryInfoObj.latitude doubleValue], [self.categoryInfoObj.longitude doubleValue]];
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(openURL:options:completionHandler:)]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: directionsURL] options:@{} completionHandler:^(BOOL success) {}];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: directionsURL] options:@{} completionHandler:nil];
    }
    
}

- (void)navigateWithGoogleMap {
    
    if ([[UIApplication sharedApplication] canOpenURL:
         [NSURL URLWithString:@"comgooglemaps://"]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"comgooglemaps://?saddr=%f,%f&daddr=%f,%f&zoom=14&views=traffic",self.currentLocation.coordinate.latitude, self.currentLocation.coordinate.longitude, [self.categoryInfoObj.latitude doubleValue], [self.categoryInfoObj.longitude doubleValue]]] options:@{} completionHandler:nil];
    } else {
        NSLog(@"Can't use comgooglemaps://");
        [self alertActionForOpenGoogleMapInBrowser];
    }
    
}

- (void)alertActionForOpenGoogleMapInBrowser {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Don't have Google Maps",@"") message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"NO",@"")
                                                           style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                               [self dismissViewControllerAnimated:YES completion:^{
                                                               }];
                                                           }];
    
    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"YES",@"")
                                                           style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                               [self openGoogleMapWithBrowser];
                                                           }];
    [alert addAction:cancelAction];
    [alert addAction:yesAction];

    [self presentViewController:alert animated:YES completion:nil];
}


- (void)openGoogleMapWithBrowser {
    
    NSString *directionString = [NSString stringWithFormat:@"https://www.google.co.in/maps/dir/?saddr=%f,%f&daddr=%f,%f&directionsmode=driving",self.currentLocation.coordinate.latitude, self.currentLocation.coordinate.longitude, [self.categoryInfoObj.latitude doubleValue], [self.categoryInfoObj.longitude doubleValue]];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: directionString] options:@{} completionHandler:nil];
    
}


#pragma mark - CollectioView DataSource and Delegate methods


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    if (self.segmentIndex == 0) {
        
        return self.servicesArray.count;

    }else if (self.segmentIndex == 1){
        
        return self.offersArray.count;

    }else if (self.segmentIndex == 2) {
        return 0 ;
    }else {
       return self.reviewsArray.count;
    }
    
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionViewCell *cell;
    
    if (self.segmentIndex == 0) {
        
        ServicesCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:servicesCellIdentifier forIndexPath:indexPath];
        Services *servicesInfo = self.servicesArray[indexPath.row];
        [cell configureCollectionCellWithInfo:servicesInfo];
        
        return cell;
        
    }else if (self.segmentIndex == 1) {
        
        ServicesDetailCollCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:servicesDetailCellIdentifier forIndexPath:indexPath];
        OffersDetail *offerInfo = self.offersArray[indexPath.row];
        [cell configureOffersDetailCollectionCellWithInfo:offerInfo];
        [cell.increaseButton addTarget:self action:@selector(addAppointments:) forControlEvents:UIControlEventTouchUpInside];
        cell.increaseButton.tag = indexPath.row;
        [cell.decreaseButton addTarget:self action:@selector(decreaseAppointments:) forControlEvents:UIControlEventTouchUpInside];
        cell.decreaseButton.tag = indexPath.row;
        [cell.viewDetailBtn addTarget:self action:@selector(viewDetailOffers:) forControlEvents:UIControlEventTouchUpInside];
        cell.viewDetailBtn.tag = indexPath.row;
        return cell;
        
    }else if (self.segmentIndex == 3) {
        AboutCollectionCell *aboutcell = [collectionView dequeueReusableCellWithReuseIdentifier:aboutCellIdentifier forIndexPath:indexPath];
        BusinessReviews *reviewObj = self.reviewsArray[indexPath.row];
        [aboutcell configureAboutCollectionCell:reviewObj];
        return aboutcell;
    }
    return cell;
    
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat screenWidth = CGRectGetWidth([UIScreen mainScreen].bounds);
    CGFloat itemWidth = 0.0f, itemHeight = 0.0f;
    
     if (self.segmentIndex == 0) {
         
         itemWidth = floor(screenWidth / 3 - 5);
         itemHeight = 175.0f;
         
     }else if (self.segmentIndex == 1) {
         
         itemWidth = screenWidth;
         itemHeight = 120.0f;
         
     }else if (self.segmentIndex == 2) {
         itemWidth = 0.0f;
         itemHeight = 0.0f;
     }else {
         itemWidth = floor(screenWidth-10);
         itemHeight = 119.0f;
     }
    
    return CGSizeMake(itemWidth, itemHeight);
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 1.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 1.0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    
    if (self.segmentIndex == 0) {
        
        return UIEdgeInsetsMake(7.0, 3.0, 5.0, 3.0);

    }else if (self.segmentIndex == 1) {
        
        return UIEdgeInsetsMake(7.0, 3.0, 5.0, 3.0);

    }else if (self.segmentIndex == 2) {
        return UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
    }else {
        return UIEdgeInsetsMake(2.0, 2.0, 2.0, 2.0);
    }
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    id reusableview = [[UICollectionReusableView alloc]init];
    
    if (kind == UICollectionElementKindSectionHeader) {
        
        ProviderCollectionHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:provideHeaderViewIdentifier forIndexPath:indexPath];
        
        if (self.categoryInfoObj.images.count > 0 && self.imagePagerCount == 0) {
            
            [headerView configureProviderCollectionHeaderView:self.categoryInfoObj distanceString:self.distanceString];
            self.imagePagerCount = 1;
            
        }else if (self.categoryInfoObj.images.count < 1 && self.imagePagerCount == 0) {
            
            [headerView configureProviderCollectionHeaderView:self.categoryInfoObj distanceString:self.distanceString];
        }
        
        headerView.imagePager.translatesAutoresizingMaskIntoConstraints = YES;
        if ([self.deviceType isEqualToString:@"iPhone"]) {
            
            [headerView.imagePager setFrame:CGRectMake(0.0f, headerView.imagePager.frame.origin.y,[UIScreen mainScreen].bounds.size.width,  headerView.imagePager.frame.size.height)];
        }else {
            [headerView.imagePager setFrame:CGRectMake(0.0f, headerView.imagePager.frame.origin.y,[UIScreen mainScreen].bounds.size.width,  300.0f)];
        }
        
        [headerView.imagePager setDataSource:headerView.imagePager.dataSource];
        [headerView.imagePager setDelegate:headerView.imagePager.delegate];
        
        [self setSegmentTitle:headerView.segmentControl];
        [headerView.segmentControl addTarget:self action:@selector(segmentControlValueChanged:) forControlEvents:UIControlEventValueChanged];
        [headerView.backButton addTarget:self action:@selector(backActionMethod:) forControlEvents:UIControlEventTouchUpInside];
        
        if (self.segmentIndex == 2) {
            [self addServiceMapView:headerView];
            [self.allReviewsView removeFromSuperview];
        }else if (self.segmentIndex == 3) {
            [self.providerLocationView removeFromSuperview];
            [self addAllReviewsView:headerView];
        }else {
            [self.providerLocationView removeFromSuperview];
            [self.allReviewsView removeFromSuperview];
        }

        reusableview = headerView;
    }
    
    return reusableview;
}

- (void)setSegmentTitle:(UISegmentedControl*)segmentControl {
    
    if (self.reviewsArray.count > 0) {
        if (segmentControl.numberOfSegments != 4) {
            [segmentControl insertSegmentWithTitle:@"REVIEWS" atIndex:4 animated:NO];
        }
    }
    [segmentControl setTitle:NSLocalizedString(@"Services", @"") forSegmentAtIndex:0];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    
    CGFloat screenWidth = CGRectGetWidth([UIScreen mainScreen].bounds);
    CGFloat sectionHeight = 0.0f;

    self.descriptionHeight = 0.0f;
    self.descriptionHeight = [UILabel heightOfTextForString:self.categoryInfoObj.providerDescription andFont: [UIFont fontWithName:@"SegoeUI" size:15.0] maxSize:(CGSize){screenWidth-30, MAXFLOAT}];
    
    if ([self.deviceType isEqualToString:@"iPhone"]) {
        
        if (self.segmentIndex == 0 || self.segmentIndex == 1) {
            
            if (IS_IPHONE_6 || IS_IPHONE_7 || IS_IPHONE_X) {
                
                sectionHeight = 362;
                
            }else if (IS_IPHONE_5) {
                sectionHeight = 331;
            }else {
                sectionHeight = 372;
            }
        }else if (self.segmentIndex == 2){
            sectionHeight = 362 + 46 + self.descriptionHeight + 15.0f + 378.0f;
        }else {
            sectionHeight = 362 + 50;
        }
    }else {
        if (self.segmentIndex == 0 || self.segmentIndex == 1) {
            sectionHeight = 472;
        }else if (self.segmentIndex == 2) {
            sectionHeight = 472 + 46 + self.descriptionHeight + 15.0f + 378.0f;
        }else {
            sectionHeight = 472 + 50.0f;
        }
    }
    
    return CGSizeMake(screenWidth,sectionHeight);
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.segmentIndex == 0) {
        
        [self selectServices:indexPath.row];
        
    }else if (self.segmentIndex == 1) {

        [self viewOffersServicesDetail:indexPath.row];
    }
}


#pragma mark - Select Services


- (void)selectServices:(NSInteger)indexPath {
    
    ServicesDetailViewController *detailController = [self.storyboard instantiateViewControllerWithIdentifier:kServicesDetailViewController];
    detailController.categoryInfoObj = self.mainInfoArray[0];
    Services *servicesInfo = self.servicesArray[indexPath];
    detailController.servicesCountArray = self.servicesArray;
    detailController.segmentIndexStatus = indexPath;
    detailController.servicesInfoObj = servicesInfo;
    [self.navigationController pushViewController:detailController animated:YES];
    
}


#pragma mark - View Offers Services Detail

- (void)viewOffersServicesDetail:(NSInteger)indexPath {
    
    OffersDetail *detailObj = self.offersArray[indexPath];
    
    OffersDetailViewController *offerDetailController = [self.storyboard instantiateViewControllerWithIdentifier:kOfferDetailController];
    offerDetailController.offerInfo = detailObj;
    offerDetailController.indexValue = indexPath;
    offerDetailController.categoryInfoObj = self.categoryInfoObj;
    offerDetailController.screenStatus = NO;
    
    [self.navigationController pushViewController:offerDetailController animated:YES];
}

-(void)viewOfferDetail:(UIButton*)sender {
    
    OffersDetail *detailObj = self.offersArray[sender.tag];
    
    OffersDetailViewController *offerDetailController = [self.storyboard instantiateViewControllerWithIdentifier:kOfferDetailController];
    offerDetailController.offerInfo = detailObj;
    offerDetailController.screenStatus = NO;
    [self.navigationController pushViewController:offerDetailController animated:YES];
    
}

#pragma mark - Add/Delete Offers Appointments

- (void)addAppointments:(UIButton*)sender {
    
    [self selectOfferServices:sender.tag];
    
}

- (void)selectOfferServices:(NSInteger)index {
    
    OffersDetail*detailInfo = self.offersArray[index];
    if (detailInfo.increaseIndex < 1) {
        detailInfo.increaseIndex += 1;
        [self.selectedOffersArray addObject:detailInfo];
    }else {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"You can't add more than one service", @"")];
    }
    [self addTotalAppointments];
    
}

- (void)decreaseAppointments:(UIButton*)sender {
    
    OffersDetail*detailInfo = self.offersArray[sender.tag];
    if (detailInfo.increaseIndex>=1) {
        detailInfo.increaseIndex -= 1;
        [self.selectedOffersArray removeObject:detailInfo];
    }
    [self addTotalAppointments];

}

- (void)addTotalAppointments {
    
    self.selectedServicesArray = [NSMutableArray new];
    
    for (OffersDetail *detailInfo in self.selectedOffersArray) {
        
        [self.selectedServicesArray addObjectsFromArray:detailInfo.servicesArray];
    }
    
    self.totalAppointments = 0;
    
    for (OffersDetail *detailInfo in self.offersArray) {
            
        if (detailInfo.increaseIndex>0) {
                
            self.totalAppointments = self.totalAppointments + detailInfo.increaseIndex;
        }
    }
    NSString *itemsAdded = [self setUpItemsAddedLabel];
    self.cartItemLabel.text = [NSString stringWithFormat:@"%ld %@",self.totalAppointments,itemsAdded];
    [self.collectionView reloadData];
}

#pragma mark - View Detail Offers

- (void)viewDetailOffers:(UIButton*)sender {
    
    [self viewOffersServicesDetail:sender.tag];

}

#pragma mark - Clear Basket

- (void)clearBasket:(NSInteger)basketType {
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSString *url;
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    
    NSString *basketId = @"";
    
    if (basketType == Service) {
        basketId = [[NSUserDefaults standardUserDefaults]valueForKey:@"BasketId"];
        url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_CLEAR_SERVICE_BASKET];
    }else {
        basketId = [[NSUserDefaults standardUserDefaults]valueForKey:@"ProductBasketId"];
        url = [ConnectionHandler getServiceURL_Products:TYPE_SERVICE_CLEAR_PRODUCT_BASKET];
    }
    
    NSDictionary *dictInfo = [NSDictionary dictionaryWithObjectsAndKeys:accessToken,@"Authorization",basketId,@"BasketId", nil];
    
    if (internetStatus != NotReachable) {
        
        // [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonPostData:url params:dictInfo onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                // [SVProgressHUD dismiss];
                
                @try {
                    
                    if (completed) {
                        
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSDictionary *tempInfoDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE)
                            NSLog(@"Temp Info Dict is %@", tempInfoDict);
                        
                        NSInteger status = [[tempInfoDict valueForKey:@"status"]integerValue];
                        if (status == 200) {
                            if (basketType == Service) {
                                [[NSUserDefaults standardUserDefaults]setValue:@"" forKey:@"BasketId"];
                                [[NSUserDefaults standardUserDefaults]synchronize];
                                self.delegate = [AppDelegate sharedInstance];
                                [self.delegate.loopTimer invalidate];
                            }else {
                                [[NSUserDefaults standardUserDefaults]setValue:@"" forKey:@"ProductBasketId"];
                                [[NSUserDefaults standardUserDefaults]synchronize];
                            }
                        }
                        
                    }else {
                        if (APP_DEBUG_MODE)
                            NSLog(@"error is %@", error.localizedDescription);
                    }
                    
                } @catch (NSException *exception) {
                    [iBeautyUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}


#pragma mark - Product Offers increase/decrease

- (void)emptyServiceBasket {
    
    NSString *basketID =  [[NSUserDefaults standardUserDefaults] valueForKey:@"BasketId"];
    
    if (basketID.length > 0) {
        [self clearBasket:Service];
    }
}

- (void)emptyProductBasket {
    
    NSString *basketID =  [[NSUserDefaults standardUserDefaults] valueForKey:@"ProductBasketId"];
    
    if (basketID.length > 0) {
        [self clearBasket:Product];
    }
}


- (NSString *)setUpItemsAddedLabel {
    
    NSString *itemsAdded = @"";
    
    if (self.totalAppointments == 0 || self.totalAppointments == 1) {
        itemsAdded = NSLocalizedString(@"Item added", @"");
    }else {
        itemsAdded = NSLocalizedString(@"Items added", @"");
    }
    return itemsAdded;
}


- (NSMutableDictionary*)getBasketInfo_New_Offers:(OffersDetail*)detailInfo type:(BOOL)update {
    
    NSMutableDictionary *basketDict = [[NSMutableDictionary alloc]init];
    NSString *businessId = self.categoryInfoObj.Id;
    [basketDict setValue:businessId forKey:@"BusinessId"];
    
    NSString *customerID = [[NSUserDefaults standardUserDefaults]valueForKey:KUserId];
    [basketDict setValue:customerID forKey:@"CustomerId"];
    
    self.productBasketID = [[NSUserDefaults standardUserDefaults]valueForKey:@"ProductBasketId"];
    [basketDict setObject:self.productBasketID forKey:@"BasketId"];
    
    NSMutableArray *productsArray_New = [NSMutableArray new];
    
    NSMutableDictionary *productDict = [[NSMutableDictionary alloc]init];
    [productDict setValue:detailInfo.offerId forKey:@"OfferId"];
    [productDict setValue:[NSNumber numberWithInteger:detailInfo.increaseIndex] forKey:@"Quantity"];
    [productsArray_New addObject:productDict];
    
    NSMutableArray *offersArray_Updated = [NSMutableArray new];
    for (OffersDetail *detailInfo in self.offersInfoArray) {
        
        NSMutableDictionary *offerDict = [[NSMutableDictionary alloc]init];
        [offerDict setValue:detailInfo.offerBasketId forKey:@"Id"];
        [offerDict setValue:detailInfo.offerId forKey:@"OfferId"];
        [offerDict setValue:[NSNumber numberWithInteger:detailInfo.quantitySelected] forKey:@"Quantity"];
        [offersArray_Updated addObject:offerDict];
    }
    
    if (update == YES) {
        
        [basketDict setObject:offersArray_Updated forKey:@"UpdatedOffers"];
        [basketDict setObject:@"" forKey:@"NewOffers"];

    }else {
        [basketDict setObject:@"" forKey:@"UpdatedOffers"];
        [basketDict setObject:productsArray_New forKey:@"NewOffers"];
    }
    
    [basketDict setObject:@"" forKey:@"RemovedItems"];
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    [basketDict setValue:accessToken forKey:@"Authorization"];
    
    [basketDict setValue:@"" forKey:@"OfferId"];
    [basketDict setValue:@"" forKey:@"Note"];
    [basketDict setValue:@"" forKey:@"CustomerAddress"];
    [basketDict setValue:@"" forKey:@"ServiceProvideAt"];
    
    return basketDict;

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
