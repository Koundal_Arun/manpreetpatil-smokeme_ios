//
//  HomeViewController.m
//  iBeauty
//
//  Created by App Innovation on 27/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "HomeViewController.h"
#import "Constants.h"
#import "ServicesCollectionCell.h"
#import "ServicesCollectionHeader.h"
#import "PromotionalAds.h"
#import "OffersDetailViewController.h"
#import <SafariServices/SafariServices.h>

#import "SelectCityController.h"
#import "CityInfoModel.h"
#import "NSString+iBeautyString.h"

#import "ProductSellerCell.h"

static NSString *servicesCellIdentifier = @"ServicesCell";
static NSString *servicesCollHeaderIdentifier = @"ServicesCollectionHeader";
static NSString *productSellerCellIdentifier = @"ProductSeller";


@interface HomeViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,KIImagePagerDelegate,KIImagePagerDataSource,SFSafariViewControllerDelegate,CLLocationManagerDelegate>

@property (nonatomic,strong) NSMutableArray *servicesArray;
@property (nonatomic,strong) NSMutableArray *productsArray;
@property (nonatomic,strong) NSMutableArray *mainInfoArray;
@property (nonatomic,strong) NSMutableArray *promotionalAdsArray;
@property (nonatomic,strong) NSMutableArray *promotionImagesArray;

//@property (nonatomic,weak)  IBOutlet UISegmentedControl *segmentControl;
@property (nonatomic,weak)  IBOutlet UICollectionView *collectionView;

@property (nonatomic,weak)  IBOutlet UIView *blankScreenView;
@property (nonatomic,weak)  IBOutlet UILabel *noDataLabel;

@property (nonatomic,weak) IBOutlet UIView *cityView;
@property (nonatomic,weak) IBOutlet UILabel *cityNameLabel;
@property (nonatomic,weak) IBOutlet UIButton *selectCityButton;
@property (nonatomic,weak) IBOutlet UILabel *cityTitle;

@property (nonatomic,assign) BOOL providerStatus;

@property (nonatomic,strong) AppDelegate *delegate;

@property (nonatomic,strong) CLLocationManager *locationManager;
@property (nonatomic,strong) CLLocation *currentLocation;
@property (nonatomic,assign) NSInteger count;

@property (nonatomic,assign) NSInteger imagePagerCount;
@property (nonatomic,weak)  IBOutlet UINavigationBar *navBar;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.count = 0;
    self.imagePagerCount =0;
    [self configureLocationManager];
    [self setUpCollectionView];
    [self getPromotionalAdsInfo];
    [self addObseverForCitySelection];
    
    CAGradientLayer *gradient = [iBeautyUtility setNavigationGradient:self.navBar];
    [self.navBar setBackgroundImage:[iBeautyUtility imageFromLayer:gradient] forBarMetrics:UIBarMetricsDefault];

}

- (void) viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
}

#pragma mark - Configure Location Manager

-(void)configureLocationManager {
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        [self.locationManager requestWhenInUseAuthorization];
    
    [self.locationManager startUpdatingLocation];
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    self.currentLocation = locations.lastObject;
    self.delegate = [AppDelegate sharedInstance];
    self.delegate.latitude = [NSString stringWithFormat:@"%@",[NSNumber numberWithDouble:self.currentLocation.coordinate.latitude]];
    self.delegate.longitude = [NSString stringWithFormat:@"%@",[NSNumber numberWithDouble:self.currentLocation.coordinate.longitude]];
    
    if ( self.currentLocation && self.count == 0) {
        self.count += 1;
        [self.locationManager stopUpdatingLocation];
    }
}

#pragma mark - Set Up Collection View

- (void)setUpCollectionView {
    
    self.servicesArray = [NSMutableArray new];
    self.productsArray = [NSMutableArray new];
    self.mainInfoArray = [NSMutableArray new];
    
    UINib *collectionNib = [UINib nibWithNibName:@"ServicesCollectionCell" bundle:nil];
    [self.collectionView registerNib:collectionNib forCellWithReuseIdentifier:servicesCellIdentifier];
    UINib *productSellerNib = [UINib nibWithNibName:@"ProductSellerCell" bundle:nil];
    [self.collectionView registerNib:productSellerNib forCellWithReuseIdentifier:productSellerCellIdentifier];
    
    [self setUpViewForLocalization];
    [self setUpLocationLabel];

}


#pragma mark - Set Up location label

- (void)setUpViewForLocalization {
    
    self.cityTitle.text = NSLocalizedString(@"Selected Location", @"");
}


#pragma mark - Set Up location label

- (void)setUpLocationLabel {
    
    self.delegate = [AppDelegate sharedInstance];
    
    NSString *languageCode =   [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];
    if ([languageCode isEqualToString:@"en"]) {
        if ([self.delegate.selectedCityName isEqualToString:@"الموقع الحالي"]) {
            self.delegate.selectedCityName = @"Current Location";
        }
    }else if ([languageCode isEqualToString:@"ar"]) {
        if ([self.delegate.selectedCityName isEqualToString:@"Current Location"]) {
            self.delegate.selectedCityName = @"الموقع الحالي";
        }
    }

    if ([self.delegate.selectedCityName isEqualToString:NSLocalizedString(@"Current Location", @"")]) {
        self.providerStatus = NO;
    }else {
        self.providerStatus = YES;
    }
    self.cityNameLabel.text = self.delegate.selectedCityName;
}


#pragma mark - City Selection Oberver

- (void)addObseverForCitySelection {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getCityInfo:) name:@"getCityInfoObject" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(clearLocationInfo:) name:@"clearLocationInfo" object:nil];
}


#pragma mark - Set City info object

- (void)getCityInfo:(NSNotification*)Info {
    
    NSDictionary *cityInfoDict = Info.userInfo;
    if (APP_DEBUG_MODE)
        NSLog(@"Info is %@",cityInfoDict);
    NSString *city =  [cityInfoDict valueForKey:@"cityInfo"];
    self.delegate.selectedCityName = city;
    self.cityNameLabel.text = self.delegate.selectedCityName;
    
    if ([city isEqualToString:NSLocalizedString(@"Current Location", @"")]) {
        self.providerStatus = NO;
        self.delegate.selectedCityName = NSLocalizedString(@"Current Location", @"");
    }else {
        self.providerStatus = YES;
    }
}

#pragma mark - Clear Location Info

- (void)clearLocationInfo:(NSNotification*)Info {
    
    self.delegate.selectedCityName = NSLocalizedString(@"Current Location", @"");
    self.cityNameLabel.text = self.delegate.selectedCityName;
    self.providerStatus = NO;
}


#pragma mark - Select City View

- (IBAction)selectCityView:(id)sender {
    
    SelectCityController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:kSelectCityController];
    viewController.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:viewController animated:YES completion:nil];
}

#pragma mark - Segment Control

- (void)setUpSegmentControl {
    
    [self selectSegmentIndex];
}

- (void)selectSegmentIndex {
    
    self.mainInfoArray = self.servicesArray;
    [self noDataAvaialbleView];
    
}

- (void)noDataAvaialbleView {
    
    [self.collectionView setHidden:NO];

    [self.blankScreenView setFrame:CGRectMake(0.0f, 365.0f, self.blankScreenView.frame.size.width, self.blankScreenView.frame.size.height)];
    
    if (self.mainInfoArray.count < 1 ) {
        [self.view addSubview:self.blankScreenView];
    }else {
        [self.blankScreenView removeFromSuperview];
    }
    self.noDataLabel.text = NSLocalizedString(@"Services not available", @"");
    
    [self.collectionView reloadData];
}

#pragma mark - Get Service Categories

- (void)getServiceCategories {
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_SERVICECATEGORIES];
    
    //NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    
    NSString *urlString = [NSString stringWithFormat:@"%@",url];
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable) {
        
//        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonGETData:urlString parameters:nil onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];

                @try {
                    if (completed) {
                        Services *serviceObj = [Services getSharedInstance];
                        self.servicesArray = [serviceObj serviceInfoObject:completed];
                        if (APP_DEBUG_MODE)
                            NSLog(@"Services Array is %@", self.servicesArray);
                    }else {
                        if (APP_DEBUG_MODE)
                            NSLog(@"error is %@", error.localizedDescription);
                        [SVProgressHUD dismiss];
                    }
                    [self setUpSegmentControl];
                    
                } @catch (NSException *exception) {
                    //[EShopUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                    [SVProgressHUD dismiss];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}


#pragma mark - Get Promotional Ads Info

- (void)getPromotionalAdsInfo {
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_PROMOTIONAL_ADS];
    
    //NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    
    NSString *urlString = [NSString stringWithFormat:@"%@",url];
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable) {
        
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonGETData:urlString parameters:nil onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                @try {
                    if (completed) {
                        PromotionalAds *instanceObj = [PromotionalAds getSharedInstance];
                        self.promotionalAdsArray = [instanceObj promotionalInfoObj:completed];
                        if (APP_DEBUG_MODE)
                        NSLog(@"Info array is %@", self.promotionalAdsArray);
                        [self getPromotionalImagesArray];
                        [self getServiceCategories];
                    }else {
                        if (APP_DEBUG_MODE)
                        NSLog(@"error is %@", error.localizedDescription);
                        [self getServiceCategories];
                    }
                    
                } @catch (NSException *exception) {
                    //[EShopUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
    
}


- (void)getPromotionalImagesArray {
    
    self.promotionImagesArray = [NSMutableArray new];
    
    for (PromotionalAds * infoObj in self.promotionalAdsArray) {
        
        [self.promotionImagesArray addObject:[NSString stringWithFormat:@"%@%@",IMAGE_BASE_URL,infoObj.adImage]];
    }

}

#pragma mark - CollectioView DataSource and Delegate methods


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.servicesArray.count;
    
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    ServicesCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:servicesCellIdentifier forIndexPath:indexPath];
    Services *servicesInfo = [Services getSharedInstance];
    servicesInfo = self.servicesArray[indexPath.row];
    [cell configureCollectionCellWithInfo:servicesInfo];
    
    return cell;

}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat screenWidth = CGRectGetWidth([UIScreen mainScreen].bounds);
    
    CGFloat itemWidth = 0.0f, itemHeight = 0.0f;
    
    itemHeight = 160.0f;
    
    if (IS_IPHONE_5) {
        
        itemWidth = floor(screenWidth / 3 - 10);
        
    }else {
        itemWidth = floor(screenWidth / 3 - 5);
    }

    return CGSizeMake(itemWidth, itemHeight);
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 1.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 1.0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    if (IS_IPHONE_5) {
        return UIEdgeInsetsMake(2.0, 5.0, 2.0, 5.0);
    }else {
        return UIEdgeInsetsMake(7.0, 3.0, 5.0, 3.0);
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenHeight = 0.0f;
    
    NSString *deviceType = [UIDevice currentDevice].model;
    if ([deviceType isEqualToString:@"iPhone"]) {
        screenHeight = 281.0f;
    }else {
        screenHeight = 408 ; //350.0f;
    }

    return CGSizeMake(screenWidth,screenHeight);
    
 }

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    id reusableview = [[UICollectionReusableView alloc]init];
    
    if (kind == UICollectionElementKindSectionHeader) {
        
        ServicesCollectionHeader *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:servicesCollHeaderIdentifier forIndexPath:indexPath];
        
        if (self.imagePagerCount == 0) {
            if (self.promotionImagesArray.count > 0) {
                headerView.imagePager.delegate = self;
                headerView.imagePager.dataSource = self;
                self.imagePagerCount = 1;
            }else {
                headerView.imagePager.dataSource = nil;
            }
            [headerView configureCollectionHeaderView];
        }
        reusableview = headerView;
    }
    
    return reusableview;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    Services *servicesInfo = [Services getSharedInstance];
    servicesInfo = self.mainInfoArray[indexPath.row];
    ServiceCategoryController *categoryController = [self.storyboard instantiateViewControllerWithIdentifier:kServiceCategoryController];
    categoryController.serviceInfo = servicesInfo;
    categoryController.promotionImages = self.promotionImagesArray;
    categoryController.promotionAdsInfo = self.promotionalAdsArray;
    categoryController.selectedCityName = self.cityNameLabel.text;
    categoryController.providerStatus = self.providerStatus;
    [self.navigationController pushViewController:categoryController animated:YES];
    
}


#pragma mark - Show menu action method

- (IBAction)showMenu:(id)sender {
    
    NSString *languageCode =   [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];
    
    if ([languageCode isEqualToString:@"en"]) {
        
        [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];

    }else if ([languageCode isEqualToString:@"ar"]) {
        
        [self.mm_drawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:nil];

    }
}


#pragma mark - KIImagePager DataSource

- (NSArray *) arrayWithImages:(KIImagePager*)pager {
    
//    return @[[UIImage imageNamed:@"image1"],[UIImage imageNamed:@"image2"],[UIImage imageNamed:@"image1"],[UIImage imageNamed:@"image2"]];
    if (self.promotionImagesArray.count>0) {
        return self.promotionImagesArray;
    }
    return nil;
}

- (UIViewContentMode) contentModeForImage:(NSUInteger)image inPager:(KIImagePager *)pager {
    
    return UIViewContentModeScaleToFill;
}

- (NSArray*)arrayWithProviderInfo:(KIImagePager *)pager {
    
    return nil;
}

- (NSString *) captionForImageAtIndex:(NSInteger)index inPager:(KIImagePager *)pager {
    
    NSString *captionStr = [self getPromotionTitle:index];
    return captionStr;
}

#pragma mark - Get Promotion Title

- (NSString*)getPromotionTitle:(NSInteger)index {
    
    NSString *promotionalTitle = @"";
    
    PromotionalAds *infoObj = self.promotionalAdsArray[index];
    
    promotionalTitle = [NSString stringWithFormat:@"%@",infoObj.title];
    
    return promotionalTitle;
    
}


#pragma mark - KIImagePager Delegate

- (void) imagePager:(KIImagePager *)imagePager didScrollToIndex:(NSUInteger)index {
    
    //NSLog(@"%s %lu", __PRETTY_FUNCTION__, (unsigned long)index);
}

- (void) imagePager:(KIImagePager *)imagePager didSelectImageAtIndex:(NSUInteger)index {
    
//    NSLog(@"%s %lu", __PRETTY_FUNCTION__, (unsigned long)index);
    
    if (APP_DEBUG_MODE) { NSLog(@"Index is %ld", index); }
    PromotionalAds *adInfo = self.promotionalAdsArray[index];
    if ([adInfo.promotionalType isEqualToString:@"Internal"]) {
        
        [self getPromotionOfferData:adInfo];
        
    }else {
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:adInfo.referralUrl] options:@{} completionHandler:nil];
        SFSafariViewController *svc = [[SFSafariViewController alloc] initWithURL:[NSURL URLWithString:adInfo.referralUrl]];
        svc.delegate = self;
        svc.modalPresentationStyle = UIModalPresentationFullScreen;
        [self presentViewController:svc animated:YES completion:nil];
    }
}


#pragma mark - Safari view controller delegate

- (void)safariViewControllerDidFinish:(SFSafariViewController *)controller {
    [self dismissViewControllerAnimated:true completion:nil];
}

#pragma mark - get Promotion Offer Information

- (void)getPromotionOfferData:(PromotionalAds*)adInfo {
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_PROMOTIONAL_OFFER];
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    
    NSDictionary *dictInfo = [NSDictionary dictionaryWithObjectsAndKeys:accessToken,@"Authorization",adInfo.businessID,@"BusinessId",adInfo.offerId,@"OfferId",nil];
    
    if (internetStatus != NotReachable) {
        
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonPostData:url params:dictInfo onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                
                @try {
                    if (completed) {
                        
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSMutableArray *infoArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE) { NSLog(@"Info Array is %@", infoArray); }
                        
                        NSMutableArray *offersArray;
                        OffersDetail *offersInfoDetail  = [OffersDetail getSharedInstance];
                        OffersDetailViewController *offerDetailController = [self.storyboard instantiateViewControllerWithIdentifier:kOfferDetailController];
                        if (infoArray.count>0) {
                            NSDictionary *dictInfo = infoArray[0];
                            if ([[dictInfo valueForKey:OfferFrom] isEqualToString:ServiceProvider]) {
                                offersArray = [offersInfoDetail offersDetailInfoObj_promotionalOffers:infoArray];
                            }else {
                                offersArray = [offersInfoDetail offersDetailInfoObjFor_Products_SingleSeller:infoArray];
                            }
                        }
                        
                        if (APP_DEBUG_MODE) { NSLog(@"Offers Array ->>> %@", offersArray);}
                        
                        if (offersArray.count>0) {

                            offerDetailController.offerInfo = offersArray[0];
                            offerDetailController.promotionOfferInfo = adInfo;
                            offerDetailController.offerTypeStatus = YES;
                            [self.navigationController pushViewController:offerDetailController animated:YES];
                        }
                        
                    }else {
                        if (APP_DEBUG_MODE) { NSLog(@"error is %@", error.localizedDescription);}
                    }
                    
                } @catch (NSException *exception) {
//                    [iBeautyUtility showAlertMessage:NSLocalizedString(@"No Response", @"")];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
