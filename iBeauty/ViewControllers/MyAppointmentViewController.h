//
//  MyAppointmentViewController.h
//  iBeauty
//
//  Created by App Innovation on 27/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AllAppointmentsCell.h"

@interface MyAppointmentViewController : UIViewController<AllAppointmentCellDelegate>

@end
