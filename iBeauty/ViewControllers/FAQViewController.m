//
//  FAQViewController.m
//  iBeauty
//
//  Created by App Innovation on 27/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "FAQViewController.h"
#import "Constants.h"
#import "FAQInfo.h"
#import "FAQCellTableViewCell.h"


static NSString *faqInfoIdentifier = @"FaqCell";

@interface FAQViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,strong) NSMutableArray *FAQListArray;
@property (nonatomic,weak) IBOutlet UITableView *FAQListTableView;
@property (nonatomic,assign) NSInteger openSection;
@property (nonatomic,weak)  IBOutlet UINavigationBar *navBar;

@end

@implementation FAQViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self getFAQList];
    [self setUpTableView];
    self.openSection = -1;
    //[self setGradient];

}

#pragma mark - View Will Appear

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    self.tabBarController.tabBar.hidden = YES;
}

#pragma mark - Set Gradient

- (void)setGradient {
    
    self.navBar.frame = [iBeautyUtility setUpFrame:self.navBar.frame];
    CAGradientLayer *gradient = [iBeautyUtility setNavigationGradient:self.navBar];
    [self.navBar setBackgroundImage:[iBeautyUtility imageFromLayer:gradient] forBarMetrics:UIBarMetricsDefault];
}

- (void)setUpTableView {
    
    UINib *cellNib = [UINib nibWithNibName:@"FAQCellTableViewCell" bundle:nil];
    [self.FAQListTableView registerNib:cellNib forCellReuseIdentifier:faqInfoIdentifier];
    
}

#pragma mark - Get FAQ List

- (void)getFAQList {
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_FAQ];
    
    //NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    
    NSString *urlString = [NSString stringWithFormat:@"%@",url];
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable) {
        
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonGETData:urlString parameters:nil onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                
                @try {
                    if (completed) {
                        
                        FAQInfo *infoObj = [FAQInfo getSharedInstance];
                        self.FAQListArray = [infoObj FAQInfoObj:completed];
                        
                        if (APP_DEBUG_MODE) { NSLog(@"FAQ Info Array is %@", self.FAQListArray);}
                        [self.FAQListTableView reloadData];
                    }else {
                        if (APP_DEBUG_MODE) { NSLog(@"error is %@", error.localizedDescription);}
                        [SVProgressHUD dismiss];
                    }
                    
                } @catch (NSException *exception) {
                    //[EShopUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                    [SVProgressHUD dismiss];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}


#pragma mark - UITableViewDataSouce

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    

    return self.FAQListArray.count;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (self.openSection == section) {
        return 1;
    }else {
        return 0;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FAQCellTableViewCell *faqInfoCell = [tableView dequeueReusableCellWithIdentifier:faqInfoIdentifier];
        
    if (!faqInfoCell){
        faqInfoCell = [[FAQCellTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:faqInfoIdentifier];
     }
    
    [faqInfoCell ConfigureCellWithAnswer:self.FAQListArray[indexPath.section]];
    
    return faqInfoCell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FAQInfo *infoObj = self.FAQListArray[indexPath.section];
    
    if (self.openSection == indexPath.section) {
        return infoObj.answerHeight+20.0f;
    }else {
        return 0;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    FAQInfo *infoObj = self.FAQListArray[section];
    
    return infoObj.questionHeight+20.0f;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    FAQInfo *infoObj = self.FAQListArray[section];

    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width,  infoObj.questionHeight+20.0f)];
    UILabel *questionLabel= [[UILabel alloc]initWithFrame:CGRectMake(15,10,tableView.frame.size.width-50,infoObj.questionHeight)];
    questionLabel.textColor = [UIColor blackColor];
    questionLabel.font = [UIFont fontWithName:@"SegoeUI" size:16.0];
    [headerView addSubview:questionLabel];
    questionLabel.text = infoObj.Question;
    questionLabel.numberOfLines = 0;
    
    UILabel *underLineLabel= [[UILabel alloc]initWithFrame:CGRectMake(0,headerView.frame.size.height-1,tableView.frame.size.width,0.5)];
    underLineLabel.backgroundColor = [UIColor lightGrayColor];
    [headerView addSubview:underLineLabel];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(tableView.frame.size.width-26.0f,headerView.frame.size.height/2-5.0f,10.0f,10.0f)];
    [headerView addSubview:imageView];
    
    if (self.openSection == section) {
        [imageView setImage:[UIImage imageNamed:@"down_arrow"]];
    }else {
        [imageView setImage:[UIImage imageNamed:@"up_arrow"]];
    }
    
    UIButton *showAnswerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    showAnswerButton.frame = CGRectMake(0, 0, tableView.frame.size.width,  infoObj.questionHeight+20.0f);
    [showAnswerButton addTarget:self action:@selector(showAnswer:) forControlEvents:UIControlEventTouchUpInside];
        showAnswerButton.tag = section;
    [headerView addSubview:showAnswerButton];
    
    headerView.backgroundColor = [UIColor whiteColor];
    
    return headerView;
}

- (void)showAnswer:(UIButton *)sender {
    
    if (self.openSection == sender.tag) {
        self.openSection = -1;
    }else {
        self.openSection = sender.tag;
    }
    

    [self.FAQListTableView reloadData];
}

#pragma mark - Back Action Method

- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
