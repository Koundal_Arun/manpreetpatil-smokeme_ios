//
//  BookAppointmentController.m
//  iBeauty
//
//  Created by App Innovation on 03/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "BookAppointmentController.h"
#import "BookAppointmentCollCell.h"
#import "BookAppointmentView.h"
#import "StaffDetail.h"
#import "ServiceTimeSlot.h"
#import "NSDate+iBeautyDateFormatter.h"
#import "BasketInfoObj.h"
#import "ProvideServiceAt.h"
#import "UILabel+FlexibleWidHeight.h"
#import "NSString+iBeautyString.h"
#import "AddressModel.h"
#import "AddAddressController.h"
#import "AddressesViewController.h"
#import "SearchAddressViewController.h"

static NSString *appointmentCellIdentifier  = @"BookAppointmentCollCell";
static NSString *appointmentViewIdentifier  = @"BookAppointmentView";

@interface BookAppointmentController ()<UICollectionViewDelegate,UICollectionViewDataSource,UITextViewDelegate>

@property (nonatomic,weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic,weak) IBOutlet UIButton *continueToPay;
//@property (nonatomic,strong) StaffDetail *staffInfo;
//@property (nonatomic,strong) ServiceTimeSlot *slotInfo;
@property (nonatomic,strong) NSMutableArray  *basketInfoArray;
@property (nonatomic,strong) NSMutableArray  *deleteInfoArray;
@property (nonatomic,assign) BOOL  deleteStatus;
@property (nonatomic,strong) NSString * basketID;

@property (nonatomic,weak) IBOutlet UIButton *shopServiceButton;
@property (nonatomic,weak) IBOutlet UIButton *homeServiceButton;
@property (nonatomic,weak) IBOutlet UIImageView *shopServiceImgView;
@property (nonatomic,weak) IBOutlet UIImageView *homeServiceImgView;
@property (nonatomic,assign) NSInteger radioBtnCheckStatus;
@property (nonatomic,weak) IBOutlet UILabel *shopServiceL;
@property (nonatomic,weak) IBOutlet UILabel *homeServiceL;
@property (nonatomic,strong) NSString * serviceProvideAt;
@property (nonatomic,weak) IBOutlet UITextView * noteTextView;
@property (nonatomic,weak) IBOutlet UILabel *placeHolderLabel;

@property (nonatomic,strong) NSMutableArray *addressListArray;

@property UIToolbar *toolBar;
@property (nonatomic, strong) UIBarButtonItem   *doneButton;
@property (nonatomic, strong) AppDelegate   *delegate;

@property (nonatomic,weak)  IBOutlet UINavigationBar *navBar;

@end

@implementation BookAppointmentController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (APP_DEBUG_MODE) { NSLog(@"appointmentArray is %@",self.appointmentsDetailArray);}
    
    self.continueToPay.frame = [iBeautyUtility setUpFrame:self.continueToPay.frame];
    [iBeautyUtility setButtonCorderRound:self.continueToPay];
    
    //[self.continueToPay.layer insertSublayer:[iBeautyUtility setGradientColor_Buttons:self.continueToPay] atIndex:0];

    [self loadNotificationObserverStaffInfo];
    self.deleteInfoArray = [NSMutableArray new];
    [self setUpTextViewCursorAccordingToLanguage];
    [self setupToolBarOnTextView];
    //[self setGradient];

}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    self.continueToPay.userInteractionEnabled = YES;
    [self getServiceProviderInfo];
}

#pragma mark - Set Gradient

- (void)setGradient {
    
    self.navBar.frame = [iBeautyUtility setUpFrame:self.navBar.frame];
    CAGradientLayer *gradient = [iBeautyUtility setNavigationGradient:self.navBar];
    [self.navBar setBackgroundImage:[iBeautyUtility imageFromLayer:gradient] forBarMetrics:UIBarMetricsDefault];
}

- (void)setUpTextViewCursorAccordingToLanguage {
    
    self.placeHolderLabel.text = NSLocalizedString(@"Write your note", @"");
    
    NSString *languageCode =   [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];
    
    if ([languageCode isEqualToString:@"en"]) {
        
        self.noteTextView.textAlignment = NSTextAlignmentLeft;
        
    }else if ([languageCode isEqualToString:@"ar"]) {
        
        self.noteTextView.textAlignment = NSTextAlignmentRight;
    }
}

#pragma mark -  Get Service Provider Data Info

- (void)getServiceProviderInfo {
    
    if (self.screenStatus == YES) {
        
        [self getServiceProviderDetailInfo];
        
    }else {
        [self checkForBasketExistance];
        //[self setUpRadioButton:self.categoryInfoObj];
    }
    
}

- (void)getServiceProviderDetailInfo {
    
    //    if (self.deleteBtnHideStatus == YES) {
    [[NSUserDefaults standardUserDefaults]setValue:@"" forKey:@"BasketId"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    //    }
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_SERVICE_PROVIDER_DETAIL];
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSString *serviceId;
    if (self.categoryInfoObj) {
        serviceId = self.categoryInfoObj.Id;
    }else {
        serviceId = self.offerInfoObj.providerId;
    }
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    
    NSString *currentDay =  [[NSString string] getWeekDay];
    
    NSDictionary *dictInfo = [NSDictionary dictionaryWithObjectsAndKeys:accessToken,@"Authorization",serviceId,@"businessId",currentDay,@"dayName",nil];
    
    if (internetStatus != NotReachable) {
        
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonPostData:url params:dictInfo onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //                [SVProgressHUD dismiss];
                @try {
                    if (completed) {
                        
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSMutableArray *tempInfoArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        
                        if (APP_DEBUG_MODE) { NSLog(@"Temp Info array is %@", tempInfoArray); }
                        
                        ServicesCategoriesInfo *serviceCatObj = [ServicesCategoriesInfo getSharedInstance];
                        
                        NSMutableArray *serviceProviderInfo = [serviceCatObj serviceCategoryInfoObject:tempInfoArray];
                        
                        if (APP_DEBUG_MODE) { NSLog(@"Provider Info Array is %@", serviceProviderInfo); }
                        
                        if (serviceProviderInfo.count > 0) {
                            self.categoryInfoObj = serviceProviderInfo[0];
                            //[self setUpRadioButton:self.categoryInfoObj];
                        }
                        [self checkForBasketExistance];
                    }else {
                        if (APP_DEBUG_MODE)
                            NSLog(@"error is %@", error.localizedDescription);
                        [SVProgressHUD dismiss];
                    }
                    
                } @catch (NSException *exception) {
                    [SVProgressHUD dismiss];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}


#pragma mark -  Set Up Radio button views

- (void)setUpRadioButton:(ServicesCategoriesInfo*)catInfoObj {
    
    self.homeServiceButton.userInteractionEnabled = NO;
    self.shopServiceButton.userInteractionEnabled = NO;
    self.shopServiceL.textColor = [UIColor lightGrayColor];
    self.homeServiceL.textColor = [UIColor lightGrayColor];
    self.shopServiceImgView.image = [UIImage imageNamed:@"UncheckGrey"];
    self.homeServiceImgView.image = [UIImage imageNamed:@"UncheckGrey"];
    
    NSMutableArray *provideServiceAt = catInfoObj.provideServiceAt;
    NSMutableArray *tempArray = [NSMutableArray new];
    
    for (ProvideServiceAt * provideServiceObj in provideServiceAt) {
        
        [tempArray addObject:provideServiceObj.name];
    }
    
    if (tempArray.count == 1) {
        self.serviceProvideAt = tempArray[0];
        if (APP_DEBUG_MODE){ NSLog(@"Service provider at value is ->>>>>>>>>>>>>>>>> %@",self.serviceProvideAt); }
        
        if ([self.serviceProvideAt isEqualToString:@"Home"]) {
            self.homeServiceButton.userInteractionEnabled = YES;
            self.homeServiceL.textColor = [UIColor blackColor];
            self.homeServiceImgView.image = [UIImage imageNamed:@"Check"];
            self.serviceProvideAt = @"Home";
            
        }else {
            self.shopServiceButton.userInteractionEnabled = YES;
            self.shopServiceL.textColor = [UIColor blackColor];
            self.shopServiceImgView.image = [UIImage imageNamed:@"Check"];
            self.serviceProvideAt = @"Shop";
        }
    }else {
        self.homeServiceButton.userInteractionEnabled = YES;
        self.homeServiceL.textColor = [UIColor blackColor];
        self.homeServiceImgView.image = [UIImage imageNamed:@"UncheckGrey"];
        self.shopServiceButton.userInteractionEnabled = YES;
        self.shopServiceL.textColor = [UIColor blackColor];
        self.shopServiceImgView.image = [UIImage imageNamed:@"Check"];
        self.serviceProvideAt = @"Shop";
    }
    
}

#pragma mark -  Check For Basket Existance

- (void)checkForBasketExistance {
    
    self.basketID = [[NSUserDefaults standardUserDefaults]valueForKey:@"BasketId"];
    
    if (self.basketID.length>0) {
        [self getExistingBasket];
    }else {
        [self createBasketForServices];
    }
}

#pragma mark -  Get Basket For Services


- (void)getExistingBasket {
    
    //{{baseurl}}/basket/6059e499-68aa-4564-9b60-355a698398a3?businessId=1&customerId=123
    
    NSString *businessID = self.categoryInfoObj.Id;
    NSString *customerID = [[NSUserDefaults standardUserDefaults]valueForKey:KUserId];
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_GET_BASKET];
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    
    NSDictionary *dictInfo = [NSDictionary dictionaryWithObjectsAndKeys:accessToken,@"Authorization",businessID,@"BusinessId",customerID,@"CustomerId",self.basketID,@"BasketId",nil];
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable) {
        
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonPostData:url params:dictInfo onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                
                @try {
                    if (completed) {
                        
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSDictionary *tempInfoDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE)
                            NSLog(@"Temp Info Dict is %@", tempInfoDict);
                        
                        BasketInfoObj *infoObj = [BasketInfoObj getSharedInstance];
                        self.basketInfoArray = [infoObj basketDetailInfoObject:tempInfoDict];
                        [self getStaffInfoToAppointmentDetail:self.basketInfoArray];
                        
                        if (APP_DEBUG_MODE)
                            NSLog(@"Response Array is %@", self.basketInfoArray);
                        [self updateExistingBasket];
                    }else {
                        if (APP_DEBUG_MODE)
                            NSLog(@"error is %@", error.localizedDescription);
                    }
                    
                } @catch (NSException *exception) {
                    [iBeautyUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
    
}

#pragma mark -  Update Basket Methods for new appointments

- (void)updateExistingBasket {
    
    //    if (self.appointmentsDetailArray.count != self.basketInfoArray.count) {
    
    //        for (ServicesDetail *detailObj in self.appointmentsDetailArray) {
    //            for (BasketInfoObj *infoObj in self.basketInfoArray) {
    //                if (![infoObj.serviceId isEqualToString:detailObj.serviceId]) {
    //                    [self deleteExistingAppointment:infoObj];
    //                }
    //            }
    //        }
    
    //        for (ServicesDetail *detailObj in self.appointmentsDetailArray) {
    //            for (BasketInfoObj *infoObj in self.basketInfoArray) {
    //                if (![infoObj.serviceId isEqualToString:detailObj.serviceId]) {
    //                    [self.appointmentsArray_new addObject:detailObj];
    //                }
    //            }
    //        }
    //    }
    if (APP_DEBUG_MODE)
        NSLog(@"New Appointments Array is %@", self.appointmentsArray_new);
    if (self.appointmentsArray_new.count>0) {
        [self updateBasketForNewAppointments];
    }
    
}

- (void)updateBasketForNewAppointments {
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_UPDATE_BASKET];
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSDictionary *dictInfo = [self getBasketParametersInfoFor_NewAppointments];
    
    if (internetStatus != NotReachable) {
        
        [handler jsonPostData:url params:dictInfo onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                @try {
                    if (completed) {
                        
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSDictionary *tempInfoDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE)
                            NSLog(@"Temp Info Dict is %@", tempInfoDict);
                        BasketInfoObj *infoObj = [BasketInfoObj getSharedInstance];
                        self.basketInfoArray = [infoObj basketDetailInfoObject:tempInfoDict];
                        [self getStaffInfoToAppointmentDetail:self.basketInfoArray];
                        if (APP_DEBUG_MODE)
                            NSLog(@"Response Array is %@", self.basketInfoArray);
                        
                        if (self.appointmentsArray_new.count>0) {
                            [self.appointmentsArray_new removeAllObjects];
                        }
                    }else {
                        if (APP_DEBUG_MODE)
                            NSLog(@"error is %@", error.localizedDescription);
                    }
                    
                } @catch (NSException *exception) {
                    [iBeautyUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
    
}

- (NSMutableDictionary*)getBasketParametersInfoFor_NewAppointments {
    
    NSMutableDictionary *basketDict = [[NSMutableDictionary alloc]init];
    NSString *businessId = self.categoryInfoObj.Id;
    [basketDict setValue:businessId forKey:@"BusinessId"];
    
    NSString *customerID = [[NSUserDefaults standardUserDefaults]valueForKey:KUserId];
    [basketDict setValue:customerID forKey:@"CustomerId"];
    
    self.basketID = [[NSUserDefaults standardUserDefaults]valueForKey:@"BasketId"];
    [basketDict setObject:self.basketID forKey:@"BasketId"];
    
    NSMutableArray *appointmentsArray = [NSMutableArray new];
    for (ServicesDetail *detailInfo in self.appointmentsArray_new) {
        NSMutableDictionary *appointmentDict = [[NSMutableDictionary alloc]init];
        [appointmentDict setValue:detailInfo.serviceId forKey:@"ServiceId"];
        [appointmentDict setValue:@"" forKey:@"StartDateTime"];
        [appointmentDict setValue:@"" forKey:@"EndDateTime"];
        [appointmentDict setValue:[NSNumber numberWithInteger:0] forKey:@"StaffId"];
        [appointmentsArray addObject:appointmentDict];
    }
    [basketDict setObject:@"" forKey:@"UpdatedAppointments"];
    [basketDict setObject:appointmentsArray forKey:@"NewAppointments"];
    [basketDict setObject:@"" forKey:@"RemovedAppointments"];
    [basketDict setObject:@"" forKey:@"Products"];
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    [basketDict setValue:accessToken forKey:@"Authorization"];
    
    NSString *offerId = @"";
    if (self.screenStatus == YES) {
        offerId = self.offerInfoObj.offerId;
    }
    
    [basketDict setValue:offerId forKey:@"OfferId"];
    
    return basketDict;
    
}


#pragma mark -  Create Basket For Services

- (void)createBasketForServices {
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_CREATE_BASKET];
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSDictionary *dictInfo = [self getCreateBasketParametersInfo];
    
    if (internetStatus != NotReachable) {
        
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonPostData:url params:dictInfo onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                @try {
                    if (completed) {
                        
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSDictionary *tempInfoDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        BasketInfoObj *infoObj = [BasketInfoObj getSharedInstance];
                        self.basketInfoArray = [infoObj basketDetailInfoObject:tempInfoDict];
                        if(self.basketInfoArray.count>0) {
                            infoObj = self.basketInfoArray[0];
                            if (APP_DEBUG_MODE) { NSLog(@"Basket Id is %@", infoObj.basketId); }
                            
                            [[NSUserDefaults standardUserDefaults]setValue:infoObj.basketId forKey:@"BasketId"];
                            [[NSUserDefaults standardUserDefaults]synchronize];
                            [self getStaffInfoToAppointmentDetail:self.basketInfoArray];
                            self.delegate = [AppDelegate sharedInstance];
                            [self.delegate clearBasket];
                        }
                        if (APP_DEBUG_MODE)
                            NSLog(@"Response Array is %@", self.basketInfoArray);
                    }else {
                        if (APP_DEBUG_MODE)
                            NSLog(@"error is %@", error.localizedDescription);
                    }
                    
                } @catch (NSException *exception) {
                    //                    [iBeautyUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}

- (void)getStaffInfoToAppointmentDetail:(NSMutableArray*)basketArray {
    
    for (int i =0; i<basketArray.count; i++) {
        
        BasketInfoObj *infoObj = basketArray[i];
        
        StaffDetail *staffInfo;
        
        if (infoObj.staffArray.count>0) {
            
            for (StaffDetail *staffObj in infoObj.staffArray) {
                
                if (staffObj.isSelected == YES) {
                    staffInfo = staffObj;
                }
            }
            if (staffInfo == nil) {
                staffInfo = infoObj.staffArray[0];
            }
        }
        infoObj.staffInfo = staffInfo;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.collectionView reloadData];
    });
    
}

- (NSMutableDictionary*)getCreateBasketParametersInfo {
    
    NSMutableDictionary *basketDict = [[NSMutableDictionary alloc]init];
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    
    NSString *businessID = self.categoryInfoObj.Id;
    [basketDict setValue:businessID forKey:@"BusinessId"];
    
    NSString *customerID = [[NSUserDefaults standardUserDefaults]valueForKey:KUserId];
    [basketDict setValue:customerID forKey:@"CustomerId"];
    
    NSMutableArray *appointmentsArray = [NSMutableArray new];
    for (ServicesDetail *detailInfo in self.appointmentsDetailArray) {
        NSMutableDictionary *appointmentDict = [[NSMutableDictionary alloc]init];
        [appointmentDict setValue:detailInfo.serviceId forKey:@"ServiceId"];
        //        [appointmentDict setValue:@"" forKey:@"StartDateTime"];
        //        [appointmentDict setValue:@"" forKey:@"EndDateTime"];
        [appointmentDict setValue:[NSNumber numberWithInteger:0] forKey:@"StaffId"];
        [appointmentsArray addObject:appointmentDict];
    }
    [basketDict setObject:appointmentsArray forKey:@"Appointments"];
    [basketDict setObject:@"" forKey:@"Products"];
    
    //    NSString *createdDateStr = [[NSDate date] convertDateValueToNSString:DATE_FORMAT];
    //    [basketDict setValue:createdDateStr forKey:@"CreatedOn"];
    //    [basketDict setValue:createdDateStr forKey:@"UpdatedOn"];
    
    NSString *offerId = @"";
    if (self.screenStatus == YES) {
        offerId = self.offerInfoObj.offerId;
    }
    //    else {
    //        NSMutableArray *offerArray = self.categoryInfoObj.offersArray;
    //        if (offerArray.count>0) {
    //            OffersDetail *infoObj = offerArray[0];
    //            offerId = infoObj.offerId;
    //        }
    //    }
    [basketDict setValue:offerId forKey:@"OfferId"];
    [basketDict setValue:accessToken forKey:@"Authorization"];
    
    return basketDict;
    
}


- (NSInteger)serviceTypeId {
    
    NSInteger businessId = [[[NSUserDefaults standardUserDefaults]valueForKey:@"serviceType"]integerValue];
    return businessId;
    
}

#pragma mark  Load Staff Info

- (void)loadNotificationObserverStaffInfo {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getStaffInfo:) name:@"selectedStaffInfo" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getSlotInfo:) name:@"selectedSLotInfo" object:nil];
    
}

#pragma mark - Back Action

- (IBAction)backActionMethod:(id)sender {
    
    if (self.screenStatus == NO) {
        [self setUpDeleteInfoNotificationObject];
    }else {
        [self clearBasket_services:NO];
        [self.navigationController popViewControllerAnimated:YES];
    }
}


#pragma mark - Clear Basket


- (void)clearBasket_services:(BOOL)screenStatus {
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_CLEAR_SERVICE_BASKET];
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    
    NSString *basketId = [[NSUserDefaults standardUserDefaults]valueForKey:@"BasketId"];
    
    NSDictionary *dictInfo = [NSDictionary dictionaryWithObjectsAndKeys:accessToken,@"Authorization",basketId,@"BasketId", nil];
    
    if (internetStatus != NotReachable) {
        
        //[SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        [handler jsonPostData:url params:dictInfo onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //[SVProgressHUD dismiss];
                @try {
                    
                    if (completed) {
                        
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSDictionary *tempInfoDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE) { NSLog(@"Temp Info Dict is %@", tempInfoDict); }
                        
                        NSInteger status = [[tempInfoDict valueForKey:@"status"]integerValue];
                        if (status == 200) {
                            [[NSUserDefaults standardUserDefaults]setValue:@"" forKey:@"BasketId"];
                            [[NSUserDefaults standardUserDefaults]synchronize];
                            if (screenStatus == YES) {
                                self.delegate = [AppDelegate sharedInstance];
                                [self.delegate.loopTimer invalidate];
                                [self goToHomeScreen];
                            }
                        }
                        
                    }else {
                        if (APP_DEBUG_MODE){ NSLog(@"error is %@", error.localizedDescription);}
                    }
                    
                } @catch (NSException *exception) {
                    [iBeautyUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                } @finally { }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}

- (void)goToHomeScreen {
    
    self.delegate = [AppDelegate sharedInstance];
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    HomeViewController *homeController = [storyboard instantiateViewControllerWithIdentifier:kHomeViewController];
//    [self.delegate mainViewSwitch:homeController];
    self.delegate.tabbarController.selectedIndex = 0;
    
    //    [self.delegate.navCGlobal popToRootViewControllerAnimated:YES];
    
    //    [self.mm_drawerController setCenterViewController:[storyboard instantiateViewControllerWithIdentifier:kHomeViewController] withCloseAnimation:YES completion:nil];
    
}

#pragma mark -  Delete Info Notification Object

- (void)setUpDeleteInfoNotificationObject {
    
    NSMutableDictionary *dictInfo = [[NSMutableDictionary alloc]init];
    
    if (self.deleteInfoArray.count>0) {
        [dictInfo setObject:self.deleteInfoArray forKey:@"appointmentsInfo"];
    }
    
    if (self.basketInfoArray.count<1) {
        [dictInfo setValue:[NSNumber numberWithBool:NO] forKey:@"DecrementStatus"];
        
    }else {
        [dictInfo setValue:[NSNumber numberWithBool:YES] forKey:@"DecrementStatus"];
    }
    
    //    if (self.screenStatus == NO) {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"selectedAppointmentsInfo" object:nil userInfo:dictInfo];
    //    }else  {
    //        [[NSNotificationCenter defaultCenter] postNotificationName:@"selectedOfferServicesInfo" object:nil userInfo:dictInfo];
    //    }
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark -  Header Action Method

- (IBAction)headerActionMethod:(id)sender {
    
    [self setUpDeleteInfoNotificationObject];
    
}

#pragma mark - CollectioView DataSource and Delegate methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.basketInfoArray.count;
    
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    BookAppointmentCollCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:appointmentCellIdentifier forIndexPath:indexPath];
    BasketInfoObj *basketInfoObj = self.basketInfoArray[indexPath.row];
    [cell configureCollectionCell:basketInfoObj screenStatus:self.screenStatus];
    
    cell.deleteButton.tag = indexPath.row;
    [cell.deleteButton addTarget:self action:@selector(deleteAppointment:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.staffButton.tag = indexPath.row;
    [cell.staffButton addTarget:self action:@selector(selectStaff:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.dateTimeButton.tag = indexPath.row;
    [cell.dateTimeButton addTarget:self action:@selector(selectDateTimeSlot:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.staffDeleteButton.tag = indexPath.row;
    [cell.staffDeleteButton addTarget:self action:@selector(deleteSelectedStaff:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat screenWidth = CGRectGetWidth([UIScreen mainScreen].bounds);
    
    CGFloat itemWidth = 0.0f, itemHeight = 0.0f;
    
    itemWidth = floor(screenWidth);
    itemHeight = [self calculateCellHeight:indexPath.row];
    return CGSizeMake(itemWidth, itemHeight);
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 1.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 1.0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    id reusableview = [[UICollectionReusableView alloc]init];
    
    if (kind == UICollectionElementKindSectionHeader) {
        
        BookAppointmentView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:appointmentViewIdentifier forIndexPath:indexPath];
        
        NSString *title = NSLocalizedString(@"Services", @"");
        [headerView configureHeaderViewWithTitle:title status:self.screenStatus];
        
        reusableview = headerView;
    }
    
    return reusableview;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
}


- (CGFloat)calculateCellHeight:(NSInteger)index {
    
    CGFloat containerViewWidth = [UIScreen mainScreen].bounds.size.width;
    containerViewWidth = containerViewWidth - 16;
    
    //    ServicesDetail *detailObj = self.appointmentsDetailArray[index];
    
    //    ServiceTimeSlot *slotInfo = detailObj.slotInfo;
    //    NSString *dateString = [slotInfo.selectedDate convertDateValueToNSString:SLOT_DATE_FORMAT];
    //    NSString *dateTimeString = [NSString stringWithFormat:@"%@ at %@",dateString,slotInfo.startTime];
    
    //    CGFloat dateStringWidth = [UILabel widthOfTextForString:dateTimeString andFont:[UIFont systemFontOfSize:13] maxSize:(CGSize){MAXFLOAT,21 }];
    //    CGFloat dateViewWidth = 7 + 20+5+dateStringWidth+7;
    
    //    NSString *anyStaff = @"";
    //    CGFloat staffViewWidth = 0, staffStringWidth = 0;
    CGFloat  cellHeight = 0;
    
    //    if (detailObj.staffInfo) {
    //        anyStaff = detailObj.staffInfo.staffName;
    //        staffStringWidth = [UILabel widthOfTextForString:anyStaff andFont:[UIFont systemFontOfSize:13] maxSize:(CGSize){MAXFLOAT,21 }];
    //        staffViewWidth = 7 + 20 + 5 + staffStringWidth + 36;
    //    }else {
    //        anyStaff = @"Any staff";
    //        staffStringWidth = [UILabel widthOfTextForString:anyStaff andFont:[UIFont systemFontOfSize:13] maxSize:(CGSize){MAXFLOAT,21 }];
    //        staffViewWidth = 7 + 20 + 5 + staffStringWidth;
    //    }
    
    //    CGFloat estimatedWidth = containerViewWidth-dateViewWidth-30;
    //
    //    if (estimatedWidth < staffViewWidth) {
    //
    //        cellHeight = 166 + 18.0f;
    //    }else {
    cellHeight = 166.0f;
    //    }
    return cellHeight;
}

#pragma mark - Delete  Basket Appointments

- (void)deleteAppointment:(UIButton*)sender {
    
    [self showAlertDailogForDeleteBasketAppointments:sender.tag];
    
}

- (NSMutableDictionary*)getBasketInfoTo_DeleteBasket:(NSString*)appointmentId {
    
    NSMutableDictionary *basketDict = [[NSMutableDictionary alloc]init];
    
    NSString *businessID = self.categoryInfoObj.Id;
    [basketDict setValue:businessID forKey:@"BusinessId"];
    
    NSString *customerID = [[NSUserDefaults standardUserDefaults]valueForKey:KUserId];
    [basketDict setValue:customerID forKey:@"CustomerId"];
    
    NSMutableArray *appointmentsArray = [NSMutableArray new];
    NSMutableDictionary *appointmentDict = [[NSMutableDictionary alloc]init];
    [appointmentDict setValue:appointmentId forKey:@"Id"];
    [appointmentsArray addObject:appointmentDict];
    [basketDict setObject:@"" forKey:@"UpdatedAppointments"];
    [basketDict setObject:@"" forKey:@"NewAppointments"];
    [basketDict setObject:appointmentsArray forKey:@"RemovedAppointments"];
    [basketDict setObject:@"" forKey:@"Products"];
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    [basketDict setValue:accessToken forKey:@"Authorization"];
    
    NSString *basketId = [[NSUserDefaults standardUserDefaults] valueForKey:@"BasketId"];
    [basketDict setValue:basketId forKey:@"BasketId"];
    
    return basketDict;
    
}


#pragma mark - Update Basket for existing appointments

- (void)updateBasketByAppointmentId:(NSInteger)serviceIndex loadingStatus:(NSInteger)loadStatus updateOperationType:(NSInteger)operationType {
    
    //    ServicesDetail *detailObj = self.appointmentsDetailArray[serviceIndex];
    //    BasketInfoObj *basketObj;
    //    for (BasketInfoObj *infoObj in self.basketInfoArray) {
    //        if ([infoObj.serviceId isEqualToString:detailObj.serviceId]) {
    //            basketObj = infoObj;
    //        }
    //    }
    
    BasketInfoObj *basketObj = self.basketInfoArray[serviceIndex];
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_UPDATE_BASKET];
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    NSDictionary *dictInfo ;
    if (loadStatus == Delete) {
        basketObj.slotInfObj = nil;
        basketObj.staffInfo = nil;
        dictInfo = [self getBasketInfoTo_DeleteBasket:basketObj.appointmentId];
    }else {
        dictInfo = [self getBasketInfoTo_UpdateBasket:basketObj operationType:operationType index:serviceIndex];
    }
    
    if (internetStatus != NotReachable) {
        
        if (loadStatus != Update) {
            [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        }
        
        [handler jsonPostData:url params:dictInfo onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                @try {
                    if (completed) {
                        
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSDictionary *tempInfoDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE)
                            NSLog(@"Temp Info Dict is %@", tempInfoDict);
                        BasketInfoObj *infoObj = [BasketInfoObj getSharedInstance];
                        self.basketInfoArray = [infoObj basketDetailInfoObject:tempInfoDict];
                        [self getStaffInfoToAppointmentDetail:self.basketInfoArray];
                        
                        if (self.basketInfoArray.count>0) {
                            infoObj = self.basketInfoArray[0];
                            [[NSUserDefaults standardUserDefaults]setValue:infoObj.basketId forKey:@"BasketId"];
                        }
                        //                        else {
                        //                            [[NSUserDefaults standardUserDefaults]setValue:@"" forKey:@"BasketId"];
                        //                        }
                        
                        if (APP_DEBUG_MODE)
                            NSLog(@"Basket Id is %@", infoObj.basketId);
                        
                        if (self.deleteStatus == YES) {
                            [self.deleteInfoArray addObject:basketObj.serviceId];
                            self.deleteStatus = NO;
                        }
                        [self.collectionView reloadData];
                        
                        if (self.basketInfoArray.count<1) {
                            //                            [[NSUserDefaults standardUserDefaults]setValue:@"" forKey:@"BasketId"];
                            [self setUpDeleteInfoNotificationObject];
                        }
                        if (APP_DEBUG_MODE)
                            NSLog(@"Response Array is %@", self.basketInfoArray);
                        
                    }else {
                        if (APP_DEBUG_MODE)
                            NSLog(@"error is %@", error.localizedDescription);
                    }
                    
                } @catch (NSException *exception) {
                    [iBeautyUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}

- (NSMutableDictionary*)getBasketInfoTo_UpdateBasket:(BasketInfoObj*)infoObj operationType:(NSInteger)type index:(NSInteger)serviceIndex {
    
    NSMutableDictionary *basketDict = [[NSMutableDictionary alloc]init];
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    [basketDict setValue:accessToken forKey:@"Authorization"];
    
    NSString *businessID = self.categoryInfoObj.Id;
    [basketDict setValue:businessID forKey:@"BusinessId"];
    
    NSString *customerID = [[NSUserDefaults standardUserDefaults]valueForKey:KUserId];
    [basketDict setValue:customerID forKey:@"CustomerId"];
    
    NSString *basketId = [[NSUserDefaults standardUserDefaults] valueForKey:@"BasketId"];
    [basketDict setValue:basketId forKey:@"BasketId"];
    
    NSMutableArray *appointmentsArray = [NSMutableArray new];
    
    NSMutableDictionary *appointmentDict = [[NSMutableDictionary alloc]init];
    
    [appointmentDict setValue:infoObj.appointmentId forKey:@"Id"];
    [appointmentDict setValue:infoObj.serviceId forKey:@"ServiceId"];
    
    if (type == 1) {
        [appointmentDict setValue: infoObj.staffInfo.staffId forKey:@"StaffId"];
        [appointmentDict setValue:infoObj.startDateTime forKey:@"StartDateTime"];
        [appointmentDict setValue:infoObj.endDateTime forKey:@"EndDateTime"];
    }else {
        BasketInfoObj *infoInfo = self.basketInfoArray[serviceIndex];
        [appointmentDict setValue:infoObj.staffInfo.staffId forKey:@"StaffId"];
        [appointmentDict setValue:infoInfo.slotInfObj.startDateTime forKey:@"StartDateTime"];
        [appointmentDict setValue:infoInfo.slotInfObj.endDateTime forKey:@"EndDateTime"];
    }
    [appointmentsArray addObject:appointmentDict];
    
    [basketDict setObject:appointmentsArray forKey:@"UpdatedAppointments"];
    
    NSString *offerId = @"";
    if (self.screenStatus == YES) {
        offerId = self.offerInfoObj.offerId;
    }
    //    else {
    //        NSMutableArray *offerArray = self.categoryInfoObj.offersArray;
    //        if (offerArray.count>0) {
    //            OffersDetail *infoObj = offerArray[0];
    //            offerId = infoObj.offerId;
    //        }
    //    }
    [basketDict setValue:offerId forKey:@"OfferId"];
    
    //    [basketDict setObject:@"" forKey:@"NewAppointments"];
    //    [basketDict setObject:@"" forKey:@"RemovedAppointments"];
    [basketDict setObject:@"" forKey:@"Products"];
    
    return basketDict;
    
}


#pragma mark - Delete Existing Appointments


- (void)deleteExistingAppointment:(BasketInfoObj*)basketObj {
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_UPDATE_BASKET];
    
    //    url = [NSString stringWithFormat:@"%@%@/update",url,basketObj.basketId];
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSDictionary *dictInfo = [self getBasketInfoTo_DeleteBasket:basketObj.appointmentId];
    
    if (internetStatus != NotReachable) {
        
        [handler jsonPostData:url params:dictInfo onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                @try {
                    if (completed) {
                        
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSMutableArray *tempInfoArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE)
                            NSLog(@"Temp Info Array is %@", tempInfoArray);
                        
                        if (APP_DEBUG_MODE)
                            NSLog(@"Response Array is %@", self.basketInfoArray);
                        
                    }else {
                        if (APP_DEBUG_MODE)
                            NSLog(@"error is %@", error.localizedDescription);
                    }
                    
                } @catch (NSException *exception) {
                    [iBeautyUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}


#pragma mark - Select Staff

- (void)selectStaff:(UIButton*)sender {
    
    SelectStaffViewController *staffController = [self.storyboard instantiateViewControllerWithIdentifier:kSelectStaffViewController];
    staffController.serviceIndex = sender.tag;
    staffController.serviceCatInfo = self.categoryInfoObj;
    BasketInfoObj *basketInfoObj = self.basketInfoArray[sender.tag];
    staffController.staffInfoArray = basketInfoObj.staffArray;
    staffController.modalPresentationStyle = UIModalPresentationFullScreen;
    [self.navigationController presentViewController:staffController animated:YES completion:nil];
    
}

#pragma mark - Select DateTime Slot for Appointment

- (void)selectDateTimeSlot:(UIButton*)sender {
    
    SelectTimeSlotController *timeSlotController = [self.storyboard instantiateViewControllerWithIdentifier:kSelectTimeSlotController];
    BasketInfoObj *detailObj = self.basketInfoArray[sender.tag];
    timeSlotController.serviceId = detailObj.serviceId;
    timeSlotController.slotIndex = sender.tag;
    timeSlotController.staffInfo = detailObj.staffInfo;
    timeSlotController.categoryInfoObj = self.categoryInfoObj;
    timeSlotController.offerInfo = self.offerInfoObj;
    timeSlotController.screenStatus = self.screenStatus;
    timeSlotController.modalPresentationStyle = UIModalPresentationFullScreen;
    [self.navigationController presentViewController:timeSlotController animated:YES completion:nil];
    
}

#pragma mark - Delete Selected Staff

- (void)deleteSelectedStaff:(UIButton*)sender {
    
    BasketInfoObj *basketObj = self.basketInfoArray[sender.tag];
    basketObj.staffInfo = nil;
    BasketInfoObj *defaultBasket = self.basketInfoArray[0];
    basketObj.staffInfo = defaultBasket.staffArray[0];
    [self.collectionView reloadData];
    
}


#pragma mark - Delete basket Alert message

- (void)showAlertDailogForDeleteBasketAppointments:(NSInteger)index {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:NSLocalizedString(@"Are you sure? You want to Delete", @"")
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"NO", @"")
                                                           style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                               [self dismissViewControllerAnimated:YES completion:^{
                                                               }];
                                                           }];
    UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"YES", @"")
                                                           style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                               [self updateBasketByAppointmentId:index loadingStatus:Delete updateOperationType:0];
                                                               self.deleteStatus = YES;
                                                           }];
    
    [alert addAction:cancelAction];
    [alert addAction:deleteAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Get Staff/Slot info Notification Method

- (void)getStaffInfo:(NSNotification*)staffInfo {
    
    if (APP_DEBUG_MODE)
        NSLog(@"Staff info is %@",staffInfo);
    NSDictionary *staffDict = staffInfo.userInfo;
    StaffDetail *staffInfoObj;
    if ([staffDict valueForKey:@"staffInfo"]) {
        staffInfoObj = [staffDict valueForKey:@"staffInfo"];
    }
    NSInteger index = [[staffDict valueForKey:@"Index"]integerValue];
    BasketInfoObj *basketInfo = self.basketInfoArray[index];
    basketInfo.staffInfo = staffInfoObj;
    [self updateBasketByAppointmentId:index loadingStatus:Update updateOperationType:1];
    [self.collectionView reloadData];
}

- (void)getSlotInfo:(NSNotification*)slotInfo {
    
    if (APP_DEBUG_MODE)
        NSLog(@"Staff info is %@",slotInfo);
    NSDictionary *slotDict = slotInfo.userInfo;
    ServiceTimeSlot *slotInfoObj;
    if ([slotDict valueForKey:@"slotInfo"]) {
        slotInfoObj = [slotDict valueForKey:@"slotInfo"];
    }
    NSInteger index = [[slotDict valueForKey:@"Index"]integerValue];
    BasketInfoObj *detailInfo = self.basketInfoArray[index];
    detailInfo.slotInfObj = slotInfoObj;
    [self updateBasketByAppointmentId:index loadingStatus:Update updateOperationType:2];
    [self.collectionView reloadData];
    
}

#pragma mark -  Select Shop/Home radio buttons


- (IBAction)selectShopHomeServices:(UIButton*)sender {
    
    switch ([sender tag]) {
            
        case 0:
            [self.shopServiceButton setSelected:YES];
            [self.homeServiceButton setSelected:NO];
            
            [self.shopServiceImgView setImage:[UIImage imageNamed:@"Check"]];
            
            if (self.homeServiceButton.userInteractionEnabled == NO) {
                [self.homeServiceImgView setImage:[UIImage imageNamed:@"UncheckGrey"]];
            }else {
                [self.homeServiceImgView setImage:[UIImage imageNamed:@"Uncheck"]];
            }
            self.radioBtnCheckStatus = 1;
            self.serviceProvideAt = @"Shop";
            break;
            
        case 1:
            [self.homeServiceButton setSelected:YES];
            [self.shopServiceButton setSelected:NO];
            [self.homeServiceImgView setImage:[UIImage imageNamed:@"Check"]];
            
            if (self.shopServiceButton.userInteractionEnabled == NO) {
                
                [self.shopServiceImgView setImage:[UIImage imageNamed:@"UncheckGrey"]];
            }else {
                [self.shopServiceImgView setImage:[UIImage imageNamed:@"Uncheck"]];
            }
            
            self.radioBtnCheckStatus = 2;
            self.serviceProvideAt = @"Home";
            break;
            
        default:
            break;
    }
    
}

- (IBAction)continueToPay:(id)sender {
    
    NSInteger countStatus =  [self getAlertMessageIfNoTimeSlotSelected];
    if (countStatus  == 0) {
//        if (self.serviceProvideAt.length<1) {
//            [iBeautyUtility showAlertMessage:NSLocalizedString(@"Please select service provide at option", @"")];
//        }else {
//            self.continueToPay.userInteractionEnabled = NO;
//            [self updateBasketFor_NoteAndServiceProvideAt];
//        }
        ServiceProvideAtController *provideAtController = [self.storyboard instantiateViewControllerWithIdentifier:kServiceProvideAtController];
        provideAtController.categoryInfoObj = self.categoryInfoObj;
        provideAtController.basketID = self.basketID;
        provideAtController.offerInfoObj = self.offerInfoObj;
        provideAtController.basketInfoArray = self.basketInfoArray;
        [self.navigationController pushViewController:provideAtController animated:YES];
        
    }else {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"Please select date time", @"")];
    }
    
}

- (NSInteger)getAlertMessageIfNoTimeSlotSelected {
    
    NSInteger count = 0;
    for (BasketInfoObj *basketInfo in self.basketInfoArray) {
        
        if (basketInfo.slotInfObj == nil) {
            count += 1;
        }
    }
    
    return count;
    
}


#pragma mark - Update Basket for Note and Service Provide AT

- (void)updateBasketFor_NoteAndServiceProvideAt {
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_UPDATE_BASKET];
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSDictionary *dictInfo = [self getParametersInfo_For_UpdateNoteAndServiceProvideAT];
    
    if (internetStatus != NotReachable) {
        
        [handler jsonPostData:url params:dictInfo onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                @try {
                    if (completed) {
                        
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSDictionary *tempInfoDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE)
                            NSLog(@"Temp Info Dict is %@", tempInfoDict);
                        //                        BasketInfoObj *infoObj = [BasketInfoObj getSharedInstance];
                        //                        self.basketInfoArray = [infoObj basketDetailInfoObject:tempInfoDict];
                        //                       [self getStaffInfoToAppointmentDetail:self.basketInfoArray];
                        
                        [self checkForServiceProvideAtType];
                        
                        if (APP_DEBUG_MODE)
                            NSLog(@"Response Array is %@", self.basketInfoArray);
                    }else {
                        if (APP_DEBUG_MODE)
                            NSLog(@"error is %@", error.localizedDescription);
                    }
                    
                } @catch (NSException *exception) {
                    [iBeautyUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}

- (void)checkForServiceProvideAtType {
    
    if ([self.serviceProvideAt isEqualToString:@"Home"]) {
        
        [self getAddressListForCustomers];
        
    }else {
        [self updateBasketFor_customerAddress];
    }
    
}

#pragma mark - Update Basket for Customer Address

- (void)updateBasketFor_customerAddress {
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_UPDATE_BASKET];
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSDictionary *dictInfo = [self getParametersInfo_For_UpdateNoteAndServiceProvideAT];
    
    if (internetStatus != NotReachable) {
        
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonPostData:url params:dictInfo onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                
                @try {
                    if (completed) {
                        
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSMutableArray *tempInfoArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE)
                            NSLog(@"Temp Info Array is %@", tempInfoArray);
                        
                        if (self.basketInfoArray.count>0) {
                            [self continueToGetPricing];
                        }
                        
                    }else {
                        if (APP_DEBUG_MODE)
                            NSLog(@"error is %@", error.localizedDescription);
                    }
                    
                } @catch (NSException *exception) {
                    [iBeautyUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}

#pragma mark - Get pricing

- (void)continueToGetPricing {
    
    PriceCartViewController *priceCartObj = [self.storyboard instantiateViewControllerWithIdentifier:kPriceCartViewController];
    priceCartObj.serviceDetailsArray = self.basketInfoArray;
    priceCartObj.categoryInfoObj = self.categoryInfoObj;
    priceCartObj.screenStatus = self.screenStatus;
    priceCartObj.pickStatus = self.radioBtnCheckStatus;
    [self.navigationController pushViewController:priceCartObj animated:YES];
}

- (NSMutableDictionary*)getParametersInfo_For_UpdateNoteAndServiceProvideAT {
    
    NSMutableDictionary *basketDict = [[NSMutableDictionary alloc]init];
    NSString *businessId = self.categoryInfoObj.Id;
    [basketDict setValue:businessId forKey:@"BusinessId"];
    
    NSString *customerID = [[NSUserDefaults standardUserDefaults]valueForKey:KUserId];
    [basketDict setValue:customerID forKey:@"CustomerId"];
    
    self.basketID = [[NSUserDefaults standardUserDefaults]valueForKey:@"BasketId"];
    [basketDict setObject:self.basketID forKey:@"BasketId"];
    
    NSMutableArray *appointmentsArray = [NSMutableArray new];
    for (BasketInfoObj *detailInfo in self.basketInfoArray) {
        NSMutableDictionary *appointmentDict = [[NSMutableDictionary alloc]init];
        [appointmentDict setValue:detailInfo.serviceId forKey:@"ServiceId"];
        [appointmentDict setValue:detailInfo.startDateTime forKey:@"StartDateTime"];
        [appointmentDict setValue:detailInfo.endDateTime forKey:@"EndDateTime"];
        [appointmentDict setValue:detailInfo.staffInfo.staffId forKey:@"StaffId"];
        [appointmentDict setValue:detailInfo.appointmentId forKey:@"Id"];
        [appointmentsArray addObject:appointmentDict];
    }
    [basketDict setObject:appointmentsArray forKey:@"UpdatedAppointments"];
    [basketDict setObject:@"" forKey:@"NewAppointments"];
    [basketDict setObject:@"" forKey:@"RemovedAppointments"];
    [basketDict setObject:@"" forKey:@"Products"];
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    [basketDict setValue:accessToken forKey:@"Authorization"];
    [basketDict setValue:self.serviceProvideAt forKey:@"ServiceProvideAt"];
    [basketDict setValue:self.noteTextView.text forKey:@"Note"];
    
    NSString *offerId = @"";
    if (self.screenStatus == YES) {
        offerId = self.offerInfoObj.offerId;
    }
    [basketDict setValue:offerId forKey:@"OfferId"];
    
    if ([self.serviceProvideAt isEqualToString:@"Shop"]) {
        
        NSDictionary *addressDict = [self getDictInfo];
        [basketDict setObject:addressDict forKey:@"CustomerAddress"];
    }
    
    return basketDict;
    
}

- (NSDictionary*)getDictInfo {
    
    NSDictionary *profileInfo = [[NSUserDefaults standardUserDefaults]valueForKey:KLoginInfo];
    
    NSString *nameStr = [iBeautyUtility trimString:[profileInfo valueForKey:@"Name"]];
    NSString *contactNoStr = @"";
    if ([profileInfo valueForKey:@"PhoneNumber"]  != nil && ![[profileInfo valueForKey:@"PhoneNumber"] isEqual:[NSNull null]]) {
        contactNoStr = [iBeautyUtility trimString:[profileInfo valueForKey:@"PhoneNumber"]];
    }
    NSString *userID = [iBeautyUtility trimString:[profileInfo valueForKey:@"UserID"]];
    
    NSString *addressStr = @"";
    AppDelegate *degate = [AppDelegate sharedInstance];
    NSString *latStr = [iBeautyUtility trimString:degate.latitude];
    NSString *longStr = [iBeautyUtility trimString:degate.longitude];
    NSString *emailStr = @"";
    if ([profileInfo valueForKey:@"email"]  != nil && ![[profileInfo valueForKey:@"email"] isEqual:[NSNull null]]) {
        emailStr = [iBeautyUtility trimString:[profileInfo valueForKey:@"email"]];
    }
    
    NSDictionary *userDetailDict = [[NSDictionary alloc]init];
    
    userDetailDict = [NSDictionary dictionaryWithObjectsAndKeys:userID,@"UserId",@"",@"Id", nameStr,@"UserName",contactNoStr,@"ContactNumber",addressStr,@"Address",latStr,@"Latitude",longStr,@"Longitude",emailStr,@"Email",nil];
    
    return userDetailDict;
    
}

#pragma mark - Get list of addresss

- (void)getAddressListForCustomers {
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_GET_ADDRESSES];
    
    //NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    
    NSDictionary *profileInfo = [[NSUserDefaults standardUserDefaults]valueForKey:KLoginInfo];
    NSString *userID = [profileInfo valueForKey:@"UserID"];
    
    NSString *urlString = [NSString stringWithFormat:@"%@/%@",url,userID];
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable) {
        // [SVProgressHUD showWithStatus:KLoadingMessage];
        [handler jsonGETData:urlString parameters:nil onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                // [SVProgressHUD dismiss];
                @try {
                    if (completed) {
                        
                        self.addressListArray  = [AddressModel getAddressListObj:completed screenStatus:NO];
                        [self switchAccordingToAddressInfoData];
                        
                    }else{
                        [self switchAccordingToAddressInfoData];
                    }
                } @catch (NSException *exception) {
                    //[EShopUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}

- (void)switchAccordingToAddressInfoData {
    
    if (self.addressListArray.count > 0) {
        
        AddressesViewController *addressController = [self.storyboard instantiateViewControllerWithIdentifier:kAddressesViewController];
        addressController.addressListArray = self.addressListArray;
        addressController.serviceDetailsArray = self.basketInfoArray;
        addressController.categoryInfoObj = self.categoryInfoObj;
        addressController.serviceProvideAtString = self.serviceProvideAt;
        addressController.noteString = self.noteTextView.text;
        addressController.offerInfoObj = self.offerInfoObj;
        addressController.screenStatus = self.screenStatus;
        addressController.pickStatus = self.radioBtnCheckStatus;
        
        [self.navigationController pushViewController:addressController animated:YES];
        
    }else {
        
        SearchAddressViewController *addressView = [self.storyboard instantiateViewControllerWithIdentifier:kSearchAddressController];
        addressView.backStatus = YES;
        addressView.serviceDetailsArray = self.basketInfoArray;
        addressView.categoryInfoObj = self.categoryInfoObj;
        addressView.serviceProvideAtString = self.serviceProvideAt;
        addressView.noteString = self.noteTextView.text;
        addressView.screenStatus = self.screenStatus;
        addressView.pickStatus = self.radioBtnCheckStatus;
        [self.navigationController pushViewController:addressView animated:YES];
    }
    
}

#pragma mark - set up Tool Bar On Text View

- (void)setupToolBarOnTextView {
    
    self.toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50)];
    self.toolBar.barStyle = UIBarStyleDefault;
    [self.toolBar sizeToFit];
    [self.toolBar setBackgroundColor:[UIColor lightGrayColor]];
    [self buttonWithinToolbar];
    
    self.noteTextView.inputAccessoryView = self.toolBar;
    
}

- (void)buttonWithinToolbar {
    
    self.doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", @"") style:UIBarButtonItemStyleDone target:self action:@selector(doneAction:)];
    
    UIBarButtonItem *flexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [self.toolBar setItems:@[flexibleItem,self.doneButton] animated:NO];
}

- (void)doneAction:(id)sender {
    
    [self.noteTextView resignFirstResponder];
    
}

#pragma mark - Text view delegates

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    
    self.noteTextView.text = @"";
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    
    if (![self.noteTextView hasText]) {
        self.placeHolderLabel.hidden = NO;
    }
}

- (void) textViewDidChange:(UITextView *)textView
{
    if(![self.noteTextView hasText]) {
        self.placeHolderLabel.hidden = NO;
    }
    else{
        self.placeHolderLabel.hidden = YES;
    }
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
