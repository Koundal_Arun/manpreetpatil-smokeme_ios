//
//  FilterViewController.m
//  iBeauty
//
//  Created by App Innovation on 30/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "FilterViewController.h"
#import "Constants.h"

@interface FilterViewController ()

@property (nonatomic,weak) IBOutlet UIButton *menButton;
@property (nonatomic,weak) IBOutlet UIButton *womenButton;
@property (nonatomic,weak) IBOutlet UIButton *unisexButton;
@property (nonatomic,weak) IBOutlet UIButton *homeButton;
@property (nonatomic,weak) IBOutlet UIButton *shopButton;

@property (nonatomic,weak) IBOutlet UIButton *cancelButton;
@property (nonatomic,weak) IBOutlet UIButton *applyButton;

@property (nonatomic,assign) BOOL menStatus;
@property (nonatomic,assign) BOOL womenStatus;
@property (nonatomic,assign) BOOL unisexStatus;
@property (nonatomic,assign) BOOL homeStatus;
@property (nonatomic,assign) BOOL shopStatus;
@property (nonatomic,strong) NSMutableDictionary *filerValues;

@property (nonatomic,weak)  IBOutlet UINavigationBar *navBar;

@end

@implementation FilterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setStatusBarColor];
    [self setUpFilterButtonViews];
    //[self setGradient];
    [self setNeedsStatusBarAppearanceUpdate];
}

#pragma mark - Set Status Bar Color

- (void)setStatusBarColor {

    if (@available(iOS 13.0, *)) {
        
        UIView *statusBar = [[UIView alloc]initWithFrame:[UIApplication sharedApplication].keyWindow.windowScene.statusBarManager.statusBarFrame] ;
        [self.view addSubview:statusBar];
        if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
            statusBar.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:130/255.0 blue:90/255.0 alpha:1.0];
            //[statusBar.layer insertSublayer:[self setGradientColor_Views:statusBar] atIndex:0];
        }
    }else {
            UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
            if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
                statusBar.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:130/255.0 blue:90/255.0 alpha:1.0];
                //[statusBar.layer insertSublayer:[self setGradientColor_Views:statusBar] atIndex:0];
            }
    }

}

-(UIStatusBarStyle)preferredStatusBarStyle {
   return UIStatusBarStyleLightContent;
}

- (CAGradientLayer*)setGradientColor_Views:(UIView*)sender {
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = sender.bounds;
    gradient.startPoint = CGPointMake(0, 0);
    gradient.endPoint = CGPointMake(1, 0);
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:233.0/255.0 green:83/255.0 blue:158/255.0 alpha:1.0] CGColor],(id)[[UIColor colorWithRed:240/255.0 green:59.0/255.0 blue:53.0/255.0 alpha:1.0] CGColor], nil];
    
    return gradient;
}

#pragma mark - Set Gradient

- (void)setGradient {
    
    self.navBar.frame = [iBeautyUtility setUpFrame:self.navBar.frame];
    CAGradientLayer *gradient = [iBeautyUtility setNavigationGradient:self.navBar];
    [self.navBar setBackgroundImage:[iBeautyUtility imageFromLayer:gradient] forBarMetrics:UIBarMetricsDefault];
    
//    [self.menButton.layer insertSublayer:[iBeautyUtility removeGradientColor_FilterButtons:self.menButton] atIndex:0];
//    [self.womenButton.layer insertSublayer:[iBeautyUtility removeGradientColor_FilterButtons:self.womenButton] atIndex:0];
//    [self.unisexButton.layer insertSublayer:[iBeautyUtility removeGradientColor_FilterButtons:self.unisexButton] atIndex:0];
//    [self.homeButton.layer insertSublayer:[iBeautyUtility removeGradientColor_FilterButtons:self.homeButton] atIndex:0];
//    [self.shopButton.layer insertSublayer:[iBeautyUtility removeGradientColor_FilterButtons:self.shopButton] atIndex:0];

}

#pragma mark - Set Filter Views

- (void)setUpFilterButtonViews {
    
    self.filerValues = [[NSMutableDictionary alloc]init];
    
    [iBeautyUtility setfilterButtonProperties:self.menButton selectStatus:NO];
    [iBeautyUtility setfilterButtonProperties:self.womenButton selectStatus:NO];
    [iBeautyUtility setfilterButtonProperties:self.unisexButton selectStatus:NO];
    [iBeautyUtility setfilterButtonProperties:self.homeButton selectStatus:NO];
    [iBeautyUtility setfilterButtonProperties:self.shopButton selectStatus:NO];

    [iBeautyUtility setfilterButtonProperties:self.cancelButton selectStatus:NO];
    [iBeautyUtility setfilterButtonProperties:self.applyButton selectStatus:YES];
    
    //[self.applyButton.layer insertSublayer:[iBeautyUtility setGradientColor_Buttons:self.applyButton] atIndex:0];

    [self setUpViewInArabic];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setUpViewInArabic {
    
    [self.unisexButton setTitle:NSLocalizedString(@"Unisex",@"") forState:UIControlStateNormal];
    
}

#pragma mark - Men Action

- (IBAction)menActionMethod {
    
    if (self.menStatus == NO) {
        self.menStatus = YES;
        [iBeautyUtility setfilterButtonProperties:self.menButton selectStatus:self.menStatus];
        //[self.menButton.layer replaceSublayer:[[self.menButton.layer sublayers] objectAtIndex:0] with:[iBeautyUtility setGradientColor_Buttons:self.menButton]];

        [self.filerValues setValue:[NSNumber numberWithInteger:1] forKey:@"Men"];
    }else {
        self.menStatus = NO;
        [iBeautyUtility setfilterButtonProperties:self.menButton selectStatus:self.menStatus];
        //[self.menButton.layer replaceSublayer:[[self.menButton.layer sublayers] objectAtIndex:0] with:[iBeautyUtility removeGradientColor_FilterButtons:self.menButton]];
        [self.filerValues setValue:[NSNumber numberWithInteger:0] forKey:@"Men"];
    }
    
}
#pragma mark - Women Action

- (IBAction)woMenActionMethod {
    
    if (self.womenStatus == NO) {
        self.womenStatus = YES;
        [iBeautyUtility setfilterButtonProperties:self.womenButton selectStatus:self.womenStatus];
        //[self.womenButton.layer replaceSublayer:[[self.womenButton.layer sublayers] objectAtIndex:0] with:[iBeautyUtility setGradientColor_Buttons:self.womenButton]];
        [self.filerValues setValue:[NSNumber numberWithInteger:2] forKey:@"Women"];

    }else {
        self.womenStatus = NO;
        [iBeautyUtility setfilterButtonProperties:self.womenButton selectStatus:self.womenStatus];
        [self.filerValues setValue:[NSNumber numberWithInteger:0] forKey:@"Women"];
       // [self.womenButton.layer replaceSublayer:[[self.womenButton.layer sublayers] objectAtIndex:0] with:[iBeautyUtility removeGradientColor_FilterButtons:self.womenButton]];
    }
}

#pragma mark - Unisex Action

- (IBAction)unisexActionMethod {
    
    if (self.unisexStatus == NO) {
        self.unisexStatus = YES;
        [iBeautyUtility setfilterButtonProperties:self.unisexButton selectStatus:self.unisexStatus];
        [self.filerValues setValue:[NSNumber numberWithInteger:3] forKey:@"Unisex"];
        //[self.unisexButton.layer replaceSublayer:[[self.unisexButton.layer sublayers] objectAtIndex:0] with:[iBeautyUtility setGradientColor_Buttons:self.unisexButton]];

    }else {
        self.unisexStatus = NO;
        [iBeautyUtility setfilterButtonProperties:self.unisexButton selectStatus:self.unisexStatus];
        [self.filerValues setValue:[NSNumber numberWithInteger:0] forKey:@"Unisex"];
        //[self.unisexButton.layer replaceSublayer:[[self.unisexButton.layer sublayers] objectAtIndex:0] with:[iBeautyUtility removeGradientColor_FilterButtons:self.unisexButton]];
    }
}

#pragma mark - Home Action

- (IBAction)homeActionMethod {
    
    if (self.homeStatus == NO) {
        self.homeStatus = YES;
        [iBeautyUtility setfilterButtonProperties:self.homeButton selectStatus:self.homeStatus];
        [self.filerValues setValue:[NSNumber numberWithInteger:1] forKey:@"Home"];
        //[self.homeButton.layer replaceSublayer:[[self.homeButton.layer sublayers] objectAtIndex:0] with:[iBeautyUtility setGradientColor_Buttons:self.homeButton]];

    }else {
        self.homeStatus = NO;
        [iBeautyUtility setfilterButtonProperties:self.homeButton selectStatus:self.homeStatus];
        [self.filerValues setValue:[NSNumber numberWithInteger:0] forKey:@"Home"];
        //[self.homeButton.layer replaceSublayer:[[self.homeButton.layer sublayers] objectAtIndex:0] with:[iBeautyUtility removeGradientColor_FilterButtons:self.homeButton]];

    }
}

#pragma mark - Shop Action

- (IBAction)shopActionMethod {
    
    if (self.shopStatus == NO) {
        self.shopStatus = YES;
        [iBeautyUtility setfilterButtonProperties:self.shopButton selectStatus:self.shopStatus];
        [self.filerValues setValue:[NSNumber numberWithInteger:2] forKey:@"Shop"];
        //[self.shopButton.layer replaceSublayer:[[self.shopButton.layer sublayers] objectAtIndex:0] with:[iBeautyUtility setGradientColor_Buttons:self.shopButton]];

    }else {
        self.shopStatus = NO;
        [iBeautyUtility setfilterButtonProperties:self.shopButton selectStatus:self.shopStatus];
        [self.filerValues setValue:[NSNumber numberWithInteger:0] forKey:@"Shop"];
        //[self.shopButton.layer replaceSublayer:[[self.shopButton.layer sublayers] objectAtIndex:0] with:[iBeautyUtility removeGradientColor_FilterButtons:self.shopButton]];

    }
}

#pragma mark - Cancel Action

- (IBAction)cancelActionMethod {
    
    [self dismissViewControllerAnimated:YES completion:nil];

}

#pragma mark - Apply Action

- (IBAction)applyActionMethod {
    
    NSMutableDictionary *filterInfo = [[NSMutableDictionary alloc]init];
    [filterInfo setObject:self.filerValues forKey:@"filterInfo"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"selectedFiltersInfo" object:nil userInfo:filterInfo];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

#pragma mark - Back Action Method

- (IBAction)closeActionMethod {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
