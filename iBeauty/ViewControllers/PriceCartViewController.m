//
//  PriceCartViewController.m
//  iBeauty
//
//  Created by App Innovation on 09/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "PriceCartViewController.h"
#import "PriceCartCell.h"
#import "PriceCartFooterView.h"
#import "Constants.h"
#import "BasketPriceSummary.h"
#import "PaymentOptions.h"
#import "PaymentOptionsCell.h"
#import "OrderSummaryHeaderView.h"
#import "iBeautyUtility.h"
#import "BasketInfoObj.h"
#import "OrderTaxInfoCollCell.h"
#import "AddressFooterView.h"
#import "PriceCartHeaderView.h"
#import "PriceCarFooterCollView.h"
#import "PaymentOrderInfo.h"
#import "NSDate+iBeautyDateFormatter.h"

//#import <paytabs-iOS/paytabs_iOS.h>

//#import "PayTabCardReaderViewController.h"


#define IS_IPAD UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad


static NSString *cartCellIdentifier  = @"PriceCartCell";
static NSString *paymentOptionsCellIdentifier  = @"PaymentOptionsCell";
static NSString *paymentHeaderIdentifier  = @"OrderSummaryHeader";
static NSString *cartHeaderViewIdentifier  = @"PriceCartHeaderView";
static NSString *cartFooterViewIdentifier  = @"PriceCartFooterCollView";
static NSString *taxAmountCellIdentifier  = @"TaxInfoCell";
static NSString *addressFooterViewIdentifier  = @"AddressFooterView";


@interface PriceCartViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic,strong) NSMutableArray *cartInfoArray;
@property (nonatomic,weak) IBOutlet UICollectionView *cartCollectionView;
@property (nonatomic,weak) IBOutlet UIButton *continueButton;
@property (nonatomic,strong) NSString * basketID;
@property (nonatomic,strong) BasketPriceSummary *priceSummaryInfo;
@property (nonatomic,strong) NSMutableArray *paymentOptionsArray;
@property (nonatomic,strong) NSMutableArray *taxAmountsArray;
@property (nonatomic,assign) NSInteger paymentOption;
@property (nonatomic,assign) BOOL onlinePayment;
@property (nonatomic,strong) AppDelegate *delegate;

@property (nonatomic,weak)  IBOutlet UINavigationBar *navBar;

@end

@interface PriceCartViewController () {
    
    //PTFWInitialSetupViewController *paytabsVC;
}

@end

@implementation PriceCartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.paymentOption = 0;
    
    self.continueButton.frame = [iBeautyUtility setUpFrame:self.continueButton.frame];
    [iBeautyUtility setButtonCorderRound:self.continueButton];
    
    //[self.continueButton.layer insertSublayer:[iBeautyUtility setGradientColor_Buttons:self.continueButton] atIndex:0];

    self.cartCollectionView.hidden = YES;
    [self getBasketItemsPriceInfo];
    //    self.paymentOptionsArray = [NSMutableArray arrayWithObjects:@"Pay at Store",@"Pay by Credit Card", nil];
    [self setUpCollectionView];
    self.delegate = [AppDelegate sharedInstance];
    //[self setGradient];

}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
}

#pragma mark - Set Gradient

- (void)setGradient {
    
    self.navBar.frame = [iBeautyUtility setUpFrame:self.navBar.frame];
    CAGradientLayer *gradient = [iBeautyUtility setNavigationGradient:self.navBar];
    [self.navBar setBackgroundImage:[iBeautyUtility imageFromLayer:gradient] forBarMetrics:UIBarMetricsDefault];
}

- (void)setUpCollectionView {
    
    UINib *addressNib = [UINib nibWithNibName:@"AddressFooterView" bundle:nil];
    [self.cartCollectionView registerNib:addressNib forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:addressFooterViewIdentifier];
    
    UINib *headerViewNib = [UINib nibWithNibName:@"PriceCartHeaderView" bundle:nil];
    [self.cartCollectionView registerNib:headerViewNib forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:cartHeaderViewIdentifier];
    
    UINib *footerViewNib = [UINib nibWithNibName:@"PriceCarFooterCollView" bundle:nil];
    [self.cartCollectionView registerNib:footerViewNib forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:cartFooterViewIdentifier];
}

- (NSMutableDictionary*)getTransactionDictInfo:(BOOL)callStatus {
    
    NSMutableDictionary *dict  = [[NSMutableDictionary alloc]init];
    
    if (callStatus == YES) {
        
        NSString *pt_response_code = [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"pt_response_code"]];
        NSString *pt_transaction_id = [[NSUserDefaults standardUserDefaults] objectForKey:@"pt_transaction_id"];
        NSString *pt_description = [[NSUserDefaults standardUserDefaults] objectForKey:@"pt_description"];
        
        [dict setValue:pt_response_code forKey:@"pt_response_code"];
        [dict setValue:pt_transaction_id forKey:@"pt_transaction_id"];
        [dict setValue:pt_description forKey:@"pt_description"];
        
    }else {
        
        [dict setValue:@"100" forKey:@"pt_response_code"];
        [dict setValue:@"123" forKey:@"pt_transaction_id"];
        [dict setValue:@"" forKey:@"pt_description"];
    }
    
    return dict;
    
}

#pragma mark - Swith to Payment complete screen

- (void)switchToPaymentHistoryScreen:(NSMutableDictionary*)transactionInfo {
    
    [self getPaymentDoneFor_Services:transactionInfo];
    
}

- (void)switchToOrderHistoryScreen:(PaymentOrderInfo*)paymentOrderInfo {
    
    [self getDateAndTitleFrompaymentOrderInfo:paymentOrderInfo];
    
    PaymentOrdersController *orderController = [self.storyboard instantiateViewControllerWithIdentifier:kPaymentOrdersController];
    orderController.paymentOrdersArray = self.serviceDetailsArray;
    orderController.categoryInfoObj = self.categoryInfoObj;
    orderController.basketInfo = self.priceSummaryInfo;
    orderController.paymentOrderInfo = paymentOrderInfo;
    [self.navigationController pushViewController:orderController animated:YES];
    
}

- (void)getDateAndTitleFrompaymentOrderInfo:(PaymentOrderInfo*)paymentOrderInfo {
    
    //    NSString *notificationIdentifire = paymentOrderInfo.orderId;
    
    NSMutableDictionary *dictInfo = [self filterServicesArrayAccordingToStartDate:paymentOrderInfo.appointmentsInfo];
    
    if (APP_DEBUG_MODE) { NSLog(@"Dict info is %@",dictInfo);}
    
    NSMutableArray *sectionArray = [dictInfo valueForKey:@"servicesSectionArray"];
    
    for (NSString *key in sectionArray) {
        
        NSMutableArray *infoArray = [dictInfo valueForKey:key];
        
        NSString *tilte = [NSString stringWithFormat:@"Appointment booked with %@ provider",self.categoryInfoObj.categoryName];
        
        if (infoArray.count == 1) {
            
            AllAppointmentsInfo *detailInfo = infoArray[0];
            ServicesDetail *serviceDetail = detailInfo.serviceDetail;
            NSString *timeString = [detailInfo.startDate convertDateToNSString:DATE_FORMAT_WITH_HOURS_MINUTES date:detailInfo.startDate];
            //           StaffDetail *staffDetail = detailInfo.staffObj;
            NSString *bodyMessage = [NSString stringWithFormat:@"%@ appointment on %@",serviceDetail.categoryName,timeString];
            NSMutableArray *appointmentIdsArray = [NSMutableArray new];
            [appointmentIdsArray addObject:detailInfo.appointmentId];
            NSDictionary *appointmentInfo = @{@"BookingId": paymentOrderInfo.orderId,@"Type": @"Single",@"AppointmentIDs": appointmentIdsArray, @"Date": detailInfo.startDate};
            
            [self addLocalNotificationForAppointment:detailInfo.startDate tile:tilte body:bodyMessage identifier:detailInfo.appointmentId appointmentInfo:appointmentInfo];
            
        }else {
            NSString *bodyMessage = @"";
            AllAppointmentsInfo *detailInfo = infoArray[0];
            NSMutableArray *appointmentIdsArray = [NSMutableArray new];
            
            for (AllAppointmentsInfo *detailInfo in infoArray) {
                
                [appointmentIdsArray addObject:detailInfo.appointmentId];
                
                ServicesDetail *serviceDetail = detailInfo.serviceDetail;
                
                NSString *timeString = [detailInfo.startDate convertDateToNSString:DATE_FORMAT_WITH_HOURS_MINUTES date:detailInfo.startDate];
                
                //               StaffDetail *staffDetail = detailInfo.staffObj;
                
                bodyMessage = [NSString stringWithFormat:@"%@ %@ appointment on %@",bodyMessage, serviceDetail.categoryName,timeString];
                
            }
            NSDictionary *appointmentInfo = @{@"BookingId": paymentOrderInfo.orderId,@"Type": @"Multiple",@"AppointmentIDs": appointmentIdsArray, @"Date": detailInfo.startDate};
            
            [self addLocalNotificationForAppointment:detailInfo.startDate tile:tilte body:bodyMessage identifier:paymentOrderInfo.orderId appointmentInfo:appointmentInfo];
        }
        
    }
    
}

- (NSMutableDictionary*)filterServicesArrayAccordingToStartDate:(NSMutableArray*)servicesDetailArray {
    
    NSString *startDateString = @"";
    
    NSMutableDictionary *serviceListDict = [NSMutableDictionary new];
    NSMutableArray *appointmentsArray ;
    NSMutableArray *serviceSectionArray = [NSMutableArray new];
    
    
    for (int i =0;i<servicesDetailArray.count;i++) {
        
        AllAppointmentsInfo *detailObject = servicesDetailArray[i];
        
        NSString *startDate = [detailObject.startDate convertDateToNSString:CALENDAR_DATE_FORMAT date:detailObject.startDate];
        
        NSLog(@"Appointment Start date is %@",startDate);
        
        if (![startDate  isEqualToString:startDateString]) {
            
            appointmentsArray = [[NSMutableArray alloc]init];
            
            [appointmentsArray addObject:detailObject];
            [serviceSectionArray addObject:startDate];
            startDateString = startDate;
        }else {
            [appointmentsArray addObject:detailObject];
            startDateString = startDate;
        }
        if (appointmentsArray.count>0){
            [serviceListDict setObject:appointmentsArray forKey:[NSString stringWithFormat:@"%@",startDateString]];
        }
    }
    
    [serviceListDict setObject:serviceSectionArray forKey:@"servicesSectionArray"];
    return serviceListDict;
}


#pragma mark- add local Notification for reminder task

- (void)addLocalNotificationForAppointment:(NSDate *)fireDateTime tile:(NSString*)title body:(NSString*)messageBody identifier:(NSString*)identifier appointmentInfo:(NSDictionary*)infoDict {
    
    NSString  *status = [[NSUserDefaults standardUserDefaults] valueForKey:@"NotificationStatus"];
    
    if ([status isEqualToString:@"Off"]) {
        
        return;
    }
    
    //Add notification only if fire datetime is of future.
    if ([fireDateTime compare:[NSDate date]] == NSOrderedDescending) {
        
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        [calendar setTimeZone:[NSTimeZone localTimeZone]];
        
        NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:fireDateTime];
        components.hour = 8;
        components.minute = 0;
        components.second = 0;
        
        UNMutableNotificationContent *objNotificationContent = [[UNMutableNotificationContent alloc] init];
        
        objNotificationContent.title = title;
        objNotificationContent.body = messageBody;
        objNotificationContent.sound = [UNNotificationSound soundNamed:@"ios_7.m4r"];
        
        objNotificationContent.userInfo = infoDict;
        
        // 4. update application icon badge number
        objNotificationContent.badge = @([[UIApplication sharedApplication] applicationIconBadgeNumber] + 1);
        UNCalendarNotificationTrigger *trigger = [UNCalendarNotificationTrigger triggerWithDateMatchingComponents:components repeats:NO];
        UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:identifier content:objNotificationContent trigger:trigger];
        /// 3. schedule localNotification
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
            if (!error) {
                if (APP_DEBUG_MODE)
                    NSLog(@"Location Notification succeeded");
            }
            else {
                if (APP_DEBUG_MODE)
                    NSLog(@"Local Notification failed");
            }
        }];
    }
}

#pragma mark -  Payment For Services

- (void)getPaymentDoneFor_Services:(NSMutableDictionary*)transactionInfo {
    
    self.basketID = [[NSUserDefaults standardUserDefaults]valueForKey:@"BasketId"];
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_PAYMENT_ORDERS];
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSString *businessID = [NSString stringWithFormat:@"%@",self.categoryInfoObj.Id];
    NSString *customerID = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:KUserId]];
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    
    
    PaymentOptions *paymentOptionsInfo;
    NSString *paymentMethod;
    if (self.priceSummaryInfo.paymentOptions.count>0) {
        paymentOptionsInfo = self.priceSummaryInfo.paymentOptions[self.paymentOption];
        paymentMethod = [NSString stringWithFormat:@"%ld",paymentOptionsInfo.iD];
    }
    NSString *paymentTransactionId = [transactionInfo valueForKey:@"pt_transaction_id"];
    NSString *PaymentResponseCode = [transactionInfo valueForKey:@"pt_response_code"];
    NSString *PaymentResponseFull = [transactionInfo valueForKey:@"pt_description"];
    NSString *orderPlacedFrom = @"0";
    
    NSDictionary *dictInfo = [NSDictionary dictionaryWithObjectsAndKeys:accessToken,@"Authorization",businessID,@"BusinessId",customerID,@"CustomerId",self.basketID,@"BasketId",paymentMethod,@"PaymentMethod",PaymentResponseCode,@"PaymentResponseCode",paymentTransactionId,@"PaymentTransactionId",PaymentResponseFull,@"PaymentResponseFull",orderPlacedFrom,@"OrderPlacedFrom",nil];
    
    if (internetStatus != NotReachable) {
        
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonPostData:url params:dictInfo onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                @try {
                    if (completed) {
                        
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSDictionary *tempInfoDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE) { NSLog(@"Payment Order Info Dict is %@", tempInfoDict); }
                        
                        if ([tempInfoDict valueForKey:@"BookingId"]) {
                            
                            PaymentOrderInfo *paymentOrderInfo = [PaymentOrderInfo getSharedInstance];
                            paymentOrderInfo = [paymentOrderInfo getPaymentOrderSummaryInfoObject:tempInfoDict];
                            if (APP_DEBUG_MODE) { NSLog(@"Payment Order Info Obj is %@", paymentOrderInfo); }
                            
                            [[NSUserDefaults standardUserDefaults]setValue:@"" forKey:@"BasketId"];
                            [self.delegate.loopTimer invalidate];
                            [self switchToOrderHistoryScreen:paymentOrderInfo];
                        }
                    }else {
                        if (APP_DEBUG_MODE)
                            NSLog(@"error is %@", error.localizedDescription);
                    }
                    
                } @catch (NSException *exception) {
                    [iBeautyUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}


#pragma mark - Continue To Pay

- (IBAction)continueToPay:(id)sender {
    
    if (self.paymentOption == 0) {
        
        NSMutableDictionary *dictInfo = [self getTransactionDictInfo:NO];
        [self switchToPaymentHistoryScreen:dictInfo];
        
    }else {
        [self OpenPayTabsCardReaderController];
        
    }
    
}

-(void)OpenPayTabsCardReaderController {
    
 /*   float amount = [[NSString stringWithFormat:@"%@",self.priceSummaryInfo.totalPrice]floatValue];
    //  NSString *title = @"Hotel";
    NSString *currency = @"USD";
    
    NSString *language = [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];
    
    NSDictionary *profileInfo = [[NSUserDefaults standardUserDefaults]valueForKey:KLoginInfo];
    NSString *customerName = [profileInfo valueForKey:KProfileName];
    NSString *customerEmail = [profileInfo valueForKey:KEmailId];
    
    float tax = self.priceSummaryInfo.totalTax;
    
    NSBundle *bundle = [NSBundle bundleWithURL:[[NSBundle mainBundle] URLForResource:@"Resources" withExtension:@"bundle"]];
  
    
  /*  paytabsVC = [[PTFWInitialSetupViewController alloc] initWithNibName:@"PTFWInitialSetupView" bundle:bundle andWithViewFrame:self.view.frame andWithAmount:amount andWithCustomerTitle:customerName andWithCurrencyCode:currency andWithTaxAmount:tax andWithSDKLanguage:language andWithShippingAddress:@"Sector SeventyFour Mohali" andWithShippingCity:@"Mohali" andWithShippingCountry:@"IND" andWithShippingState:@"Punjab" andWithShippingZIPCode:@"160059" andWithBillingAddress:@"Sector SeventyFour Mohali" andWithBillingCity:@"Mohali" andWithBillingCountry:@"IND" andWithBillingState:@"Punjab" andWithBillingZIPCode:@"160059" andWithOrderID:@"123456" andWithPhoneNumber:@"+917696029144" andWithCustomerEmail:customerEmail andWithCustomerPassword:@"12345678" andIsTokenization:YES andIsExistingCustomer:NO andWithPayTabsToken:@"1234567890123456789" andWithMerchantEmail:MERCHANT_EMAIL_ID andWithMerchantSecretKey:MERCHANT_SECRET_KEY andWithRequestTimeoutSeconds:20100 andWithAssigneeCode:@"SDK"];*/
    
     /* paytabsVC = [[PTFWInitialSetupViewController alloc]initWithBundle:bundle andWithViewFrame:self.view.frame andWithAmount:amount andWithCustomerTitle:customerName andWithCurrencyCode:currency andWithTaxAmount:tax andWithSDKLanguage:language andWithShippingAddress:@"Sector SeventyFour Mohali" andWithShippingCity:@"Mohali" andWithShippingCountry:@"IND" andWithShippingState:@"Punjab" andWithShippingZIPCode:@"160059" andWithBillingAddress:@"Sector SeventyFour Mohali" andWithBillingCity:@"Mohali" andWithBillingCountry:@"IND" andWithBillingState:@"Punjab" andWithBillingZIPCode:@"160059" andWithOrderID:@"123456" andWithPhoneNumber:@"+917696029144" andWithCustomerEmail:customerEmail andIsTokenization:YES andIsPreAuth:NO andWithMerchantEmail:MERCHANT_EMAIL_ID andWithMerchantSecretKey:MERCHANT_SECRET_KEY andWithAssigneeCode:@"SDK" andWithThemeColor:[UIColor iBeautyThemeColor] andIsThemeColorLight:YES];
    
    __weak typeof(self) weakSelf = self;
    paytabsVC.didReceiveBackButtonCallback = ^{
        [weakSelf closePayTabsView:0 transactionId:0 result:nil];
    };
    
    paytabsVC.didReceiveFinishTransactionCallback = ^(int responseCodes, NSString * _Nonnull result, int transactionID, NSString * _Nonnull tokenizedCustomerEmail, NSString * _Nonnull tokenizedCustomerPassword, NSString * _Nonnull token, BOOL transactionState) {
        
        if (transactionState){
            // true state
        }else {
            // false state
        }
        NSLog(@"%d",responseCodes);
        NSLog(@"%@",result);
        NSLog(@"%d",transactionID);
        NSLog(@"%@",tokenizedCustomerEmail);
        NSLog(@"%@",tokenizedCustomerPassword);
        NSLog(@"%@",token);
        [weakSelf closePayTabsView:responseCodes transactionId:transactionID result:result];
    };
    
    [self.view addSubview:paytabsVC.view];
    [self addChildViewController:paytabsVC];
    [paytabsVC didMoveToParentViewController:self];
    */
    
}

/*
- (void) closePayTabsView:(NSInteger)responseCodes transactionId:(NSInteger)transactionId result:(NSString*)description {
    [self->paytabsVC willMoveToParentViewController:self];
    [self->paytabsVC.view removeFromSuperview];
    [self->paytabsVC removeFromParentViewController];
    
    if (responseCodes  == 100) {
        
        NSMutableDictionary *dict  = [[NSMutableDictionary alloc]init];
        
        [dict setValue:[NSNumber numberWithInteger:responseCodes] forKey:@"pt_response_code"];
        [dict setValue:[NSNumber numberWithInteger:transactionId] forKey:@"pt_transaction_id"];
        [dict setValue:description forKey:@"pt_description"];
        [self switchToPaymentHistoryScreen:dict];
    }
}
*/

#pragma mark - Get Basket Items Price Info

- (void)getBasketItemsPriceInfo {
    
    self.basketID = [[NSUserDefaults standardUserDefaults]valueForKey:@"BasketId"];
    
    NSString *businessID = self.categoryInfoObj.Id;
    NSString *customerID = [[NSUserDefaults standardUserDefaults]valueForKey:KUserId];
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_PRICING];
    
    //    url = [NSString stringWithFormat:@"%@%@/pricing?businessId=%ld&customerId=%@",url,self.basketID,(long)businessID,customerID];
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    
    NSDictionary *dictInfo = [NSDictionary dictionaryWithObjectsAndKeys:accessToken,@"Authorization",businessID,@"BusinessId",customerID,@"CustomerId",self.basketID,@"BasketId",nil];
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable) {
        
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonPostData:url params:dictInfo onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                
                @try {
                    if (completed) {
                        
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSDictionary *tempInfoDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE)
                            NSLog(@"Temp Info Dict is %@", tempInfoDict);
                        
                        self.priceSummaryInfo = [BasketPriceSummary getSharedInstance];
                        self.priceSummaryInfo = [self.priceSummaryInfo getBasketPriceSummaryInfoObject:tempInfoDict];
                        PaymentOptions *paymentInfo = [PaymentOptions getSharedInstance];
                        self.paymentOptionsArray = [paymentInfo getPaymentOptionsInfoObject:tempInfoDict];
                        self.priceSummaryInfo.paymentOptions = self.paymentOptionsArray;
                        self.taxAmountsArray = self.priceSummaryInfo.tax;
                        self.cartCollectionView.hidden = NO;
                        [self.cartCollectionView reloadData];
                        
                        //                        if (APP_DEBUG_MODE)
                        //                            NSLog(@"Payment Options Array is %@",self.paymentOptionsArray);
                    }else {
                        if (APP_DEBUG_MODE)
                            NSLog(@"error is %@", error.localizedDescription);
                    }
                    
                } @catch (NSException *exception) {
                    [iBeautyUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
    
}

#pragma mark - Back Action

- (IBAction)backActionMethod:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark - CollectioView DataSource and Delegate methods


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 4;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    if (section == 0) {
        
        return 1; //self.paymentOptionsArray.count;
        
    }else if (section == 1) {
        
        return self.serviceDetailsArray.count;
        
    }else if (section == 2) {
        
        return self.taxAmountsArray.count;
    }else {
        return 0;
        
    }
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        
        PaymentOptionsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:paymentOptionsCellIdentifier forIndexPath:indexPath];
        PaymentOptions *paymentMethodInfo = self.paymentOptionsArray[indexPath.row];
        [cell configurePaymentOptionCell:paymentMethodInfo index:indexPath.row pickUpStatus:self.pickStatus];
        
        if (self.paymentOption == indexPath.row) {
            cell.radioBtnImageView.image = [UIImage imageNamed:@"Check"];
        }else {
            cell.radioBtnImageView.image = [UIImage imageNamed:@"Uncheck"];
        }
        return cell;
        
    }else if (indexPath.section == 1) {
        
        PriceCartCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cartCellIdentifier forIndexPath:indexPath];
        BasketInfoObj *basketInfo = self.serviceDetailsArray[indexPath.row];
        [cell configurePriceCartCellWithPriceInfo:basketInfo.serviceInfoObj screenStatus:self.screenStatus];
        return cell;
        
    }else {
        OrderTaxInfoCollCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:taxAmountCellIdentifier forIndexPath:indexPath];
        TaxInfoModel *infoObj = self.taxAmountsArray[indexPath.row];
        [cell configureTaxInfoCellWithInfo:infoObj];
        return cell;
    }
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat screenWidth = CGRectGetWidth([UIScreen mainScreen].bounds);
    
    CGFloat itemWidth = 0.0f, itemHeight = 0.0f;
    
    itemWidth = floor(screenWidth);
    
    if (indexPath.section == 0) {
        
        itemHeight = 75.0f;
        
    }else if (indexPath.section == 1) {
        
        itemHeight = 109.0f;
        
    }else if (indexPath.section == 2) {
        itemHeight = 41.0f;
    }else {
        itemHeight = 0.0f;
    }
    
    return CGSizeMake(itemWidth, itemHeight);
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 1.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 1.0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    
    CGFloat screenWidth = CGRectGetWidth([UIScreen mainScreen].bounds);
    
    if (section == 0) {
        return CGSizeMake(screenWidth, 50.0f);
        
    }else if (section == 1) {
        return CGSizeMake(screenWidth, 50.0f);
        
    }else if (section == 2) {
        if (self.taxAmountsArray.count > 0) {
            return CGSizeMake(screenWidth, 41.0f);
        }else {
            return CGSizeMake(0.0f, 0.0f);
        }
    }else {
        if (self.addressModel != nil) {
            return CGSizeMake(screenWidth, 50.0f);
        }else {
            return CGSizeMake(0.0f, 0.0f);
        }
    }
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    
    CGFloat screenWidth = CGRectGetWidth([UIScreen mainScreen].bounds);
    
    if (section == 0) {
        return CGSizeMake(0.0f, 0.0f);
    }else if (section == 1) {
        return CGSizeMake(0.0f, 0.0f);
    }else if (section == 2) {
        return CGSizeMake(screenWidth, 83.0f);
    }else {
        if (self.addressModel != nil) {
            return CGSizeMake(screenWidth, 109.0f);
        }else {
            return CGSizeMake(0, 0.0f);
        }
    }
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    id reusableview = [[UICollectionReusableView alloc]init];
    
    if (kind == UICollectionElementKindSectionHeader) {
        
        OrderSummaryHeaderView *collHeaderView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:paymentHeaderIdentifier forIndexPath:indexPath];
        
        if (indexPath.section == 0) {
            
            [collHeaderView configurePaymentOptionsHeaderView:NSLocalizedString(@"Payment Methods", @"")];
            
        }else if (indexPath.section == 1) {
            
            [collHeaderView configurePaymentOptionsHeaderView:NSLocalizedString(@"Services", @"")];
            
        }else if (indexPath.section == 2) {
            
            PriceCartHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:cartHeaderViewIdentifier forIndexPath:indexPath];
            
            [headerView configureHeaderViewWithTitle:self.priceSummaryInfo];
            reusableview = headerView;
            return reusableview;
            
        }else {
            [collHeaderView configurePaymentOptionsHeaderView:NSLocalizedString(@"Address", @"")];
        }
        reusableview = collHeaderView;
        
        return reusableview;
        
    }else if (kind == UICollectionElementKindSectionFooter) {
        
        if (indexPath.section == 2) {
            
            PriceCarFooterCollView *footerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:cartFooterViewIdentifier forIndexPath:indexPath];
            [footerView configureFooterViewWithTitle:self.priceSummaryInfo];
            reusableview = footerView;
            return reusableview;
            
        }
        if (indexPath.section == 3) {
            AddressFooterView *footerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:addressFooterViewIdentifier forIndexPath:indexPath];
            
            [footerView configureAddressFooterView:self.addressModel];
            reusableview = footerView;
            return reusableview;
            
        }
        
    }
    
    return reusableview;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        
        self.paymentOption = indexPath.row;
        [self.cartCollectionView reloadData];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
