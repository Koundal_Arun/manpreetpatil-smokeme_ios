//
//  EnterInformationController.h
//  iBeauty
//
//  Created by App Innovation on 27/09/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EnterInformationController : UIViewController

@property (nonatomic,strong) NSString *emailText;
@property (nonatomic,assign) BOOL status;

@end
