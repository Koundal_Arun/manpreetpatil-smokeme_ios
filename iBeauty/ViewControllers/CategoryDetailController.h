//
//  CategoryDetailController.h
//  iBeauty
//
//  Created by App Innovation on 29/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServicesCategoriesInfo.h"
#import "Constants.h"
#import <CoreLocation/CoreLocation.h>

@interface CategoryDetailController : UIViewController

@property (nonatomic,strong) ServicesCategoriesInfo *categoryInfoObj;
@property (nonatomic,strong) CLLocation *currentLocation;
@property (nonatomic,strong) NSMutableArray *mainInfoArray;

@property (nonatomic,strong) NSString *businessID;

@end
