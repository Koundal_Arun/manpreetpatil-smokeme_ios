//
//  MyProfileViewController.h
//  iBeauty
//
//  Created by App Innovation on 10/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"

@interface MyProfileViewController : UIViewController

@property (nonatomic,strong) NSDictionary *profileInfo;

@end
