//
//  MyProfileViewController.m
//  iBeauty
//
//  Created by App Innovation on 10/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "MyProfileViewController.h"
#import "ProfileCollectionCell.h"
#import "ProfileCollectionHeader.h"
#import "ProfileCollectionFooter.h"
#import "Constants.h"
#import "AddAddressController.h"
#import "ChangePasswordController.h"
#import "UpdateProfileController.h"
#import "SearchAddressViewController.h"


static NSString *profileCollCellIdentifier = @"ProfileCollectionCell";
static NSString *profileCollHeader = @"ProfileCollectionHeader";
static NSString *profileCollFooter = @"ProfileCollectionFooter";

@interface MyProfileViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic,strong) NSMutableArray *addressInfoArray;
@property (nonatomic,weak) IBOutlet UICollectionView *profileCollectionView;

@property (nonatomic,strong) NSString *loginType;

@property (nonatomic,weak)  IBOutlet UINavigationBar *navBar;

@end

@implementation MyProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.profileInfo = [[NSUserDefaults standardUserDefaults]valueForKey:KLoginInfo];
    self.loginType = [[NSUserDefaults standardUserDefaults]valueForKey:KLoginType];
    //[self setGradient];

}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    [self getExistingAddressListOfLoggedUser];

}

#pragma mark - Set Gradient

- (void)setGradient {
    
    self.navBar.frame = [iBeautyUtility setUpFrame:self.navBar.frame];
    CAGradientLayer *gradient = [iBeautyUtility setNavigationGradient:self.navBar];
    [self.navBar setBackgroundImage:[iBeautyUtility imageFromLayer:gradient] forBarMetrics:UIBarMetricsDefault];
}

#pragma mark - Get list of addresss

- (void)getExistingAddressListOfLoggedUser {
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_GET_ADDRESSES];
    
    //NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    
    NSDictionary *profileInfo = [[NSUserDefaults standardUserDefaults]valueForKey:KLoginInfo];
    NSString *userID = [profileInfo valueForKey:@"UserID"];
    
    NSString *urlString = [NSString stringWithFormat:@"%@/%@",url,userID];
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable) {
        // [SVProgressHUD showWithStatus:KLoadingMessage];
        [handler jsonGETData:urlString parameters:nil onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                // [SVProgressHUD dismiss];
                @try {
                    if (completed) {
                        
                        self.addressInfoArray  = [AddressModel getAddressListObj:completed screenStatus:YES];
                        [self.profileCollectionView reloadData];
                    }else{
                    }
                } @catch (NSException *exception) {
                    //[EShopUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                } @finally {
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}


#pragma mark - Back Action

- (IBAction)backActionMethod:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark - CollectioView DataSource and Delegate methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.addressInfoArray.count;
    
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    ProfileCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:profileCollCellIdentifier forIndexPath:indexPath];
    
    AddressModel *infoObj = self.addressInfoArray[indexPath.row];

    [cell.editButton addTarget:self action:@selector(editAddress:) forControlEvents:UIControlEventTouchUpInside];
    cell.editButton.tag = indexPath.row;
    [cell.deleteButton addTarget:self action:@selector(deleteAddress:) forControlEvents:UIControlEventTouchUpInside];
    cell.deleteButton.tag = indexPath.row;
    
    [cell configureProfileCollectionCellView:infoObj];
    
    return cell;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat screenWidth = CGRectGetWidth([UIScreen mainScreen].bounds);
    
    AddressModel *infoObj = self.addressInfoArray[indexPath.row];

    CGFloat itemWidth = 0.0f;
    CGFloat itemHeight = infoObj.cellHeight;

    itemWidth = floor(screenWidth);
    
    return CGSizeMake(itemWidth, itemHeight);
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 1.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 1.0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    
    if(self.addressInfoArray.count>0) {
        
        if ([self.loginType isEqualToString:@"InApp"]) {
            return CGSizeMake(screenWidth,199);
        }else {
            return CGSizeMake(screenWidth,155);
        }
    }else {
        if ([self.loginType isEqualToString:@"InApp"]) {
            return CGSizeMake(screenWidth,175);
        }else {
            return CGSizeMake(screenWidth,128);
        }
    }
    
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    id reusableview = [[UICollectionReusableView alloc]init];
    
    if (kind == UICollectionElementKindSectionHeader) {
        
        ProfileCollectionHeader *collHeaderView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:profileCollHeader forIndexPath:indexPath];
        [collHeaderView configureProfileCollectionHeaderView:self.profileInfo];
        
        [collHeaderView.editButton addTarget:self action:@selector(editProfile:) forControlEvents:UIControlEventTouchUpInside];
        [collHeaderView.changePassButton addTarget:self action:@selector(changePassword:) forControlEvents:UIControlEventTouchUpInside];
        
        collHeaderView.savedAddressLabel.translatesAutoresizingMaskIntoConstraints = YES;
        collHeaderView.passwordView.translatesAutoresizingMaskIntoConstraints = YES;

        if (self.addressInfoArray.count<1) {
            [collHeaderView.savedAddressLabel setHidden:YES];
            if ([self.loginType isEqualToString:@"InApp"]) {
                
                collHeaderView.passwordView.hidden = NO;
                collHeaderView.passwordView.frame = CGRectMake(collHeaderView.passwordView.frame.origin.x, 108.0f, SCREEN_WIDTH-16.0f, collHeaderView.passwordView.frame.size.height);

            }else {
                collHeaderView.passwordView.hidden = YES;
            }
        }else {
            
            [collHeaderView.savedAddressLabel setHidden:NO];
            if ([self.loginType isEqualToString:@"InApp"]) {
                collHeaderView.passwordView.hidden = NO;
                collHeaderView.passwordView.frame = CGRectMake(collHeaderView.passwordView.frame.origin.x, 108.0f, SCREEN_WIDTH-16.0f, collHeaderView.passwordView.frame.size.height);
                collHeaderView.savedAddressLabel.frame = CGRectMake(collHeaderView.savedAddressLabel.frame.origin.x, 172.0f, collHeaderView.savedAddressLabel.frame.size.width, collHeaderView.savedAddressLabel.frame.size.height);
            }else {
                collHeaderView.passwordView.hidden = YES;
                collHeaderView.savedAddressLabel.frame = CGRectMake(collHeaderView.savedAddressLabel.frame.origin.x, 120.0f, collHeaderView.savedAddressLabel.frame.size.width, collHeaderView.savedAddressLabel.frame.size.height);
            }
        }
        reusableview = collHeaderView;
        
    }
    else if (kind == UICollectionElementKindSectionFooter) {

        ProfileCollectionFooter *collFooterView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:profileCollFooter forIndexPath:indexPath];

        [collFooterView.addNewAddressButton addTarget:self action:@selector(addAddressForExistingCustomer:) forControlEvents:UIControlEventTouchUpInside];
        
        [collFooterView.addNewAddressButton setTitle: NSLocalizedString(@"+ Add New Address", @"") forState:UIControlStateNormal];
        
        reusableview = collFooterView;

    }
    
    return reusableview;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
}

#pragma mark - Button Action methods

- (void)addAddressForExistingCustomer:(UIButton*)sender {
    
    SearchAddressViewController *addressView = [self.storyboard instantiateViewControllerWithIdentifier:kSearchAddressController];
    addressView.modeStatus = NO;
    addressView.navigateStatus = YES;
    
    [self.navigationController pushViewController:addressView animated:YES];
}

- (void)changePassword:(UIButton*)sender {
    
    ChangePasswordController *changePasswordView = [self.storyboard instantiateViewControllerWithIdentifier:kChangePasswordController];
    changePasswordView.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:changePasswordView animated:YES completion:nil];
}

- (void)editProfile:(UIButton*)sender {
    
    UpdateProfileController *updateController = [self.storyboard instantiateViewControllerWithIdentifier:kUpdateProfileController];
    updateController.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:updateController animated:YES completion:nil];
}



#pragma mark - edit existing address

- (void)editAddress:(UIButton*)sender {
    
    AddressModel *infoObj = self.addressInfoArray[sender.tag];
    SearchAddressViewController *addressView = [self.storyboard instantiateViewControllerWithIdentifier:kSearchAddressController];
    addressView.addressInfoObj = infoObj;
    addressView.modeStatus = YES;
    addressView.navigateStatus = YES;

    [self.navigationController pushViewController:addressView animated:YES];
}

#pragma mark - delete existing address

- (void)deleteAddress:(UIButton*)sender {
    
    AddressModel *infoObj = self.addressInfoArray[sender.tag];
    [self showAlertDialogForDeleteAddress:infoObj];
}

- (void)DeleteExistingAddressTo_registered_Users:(AddressModel*)addressInfoObj {
    
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    //    NSDictionary *profileInfo = [[NSUserDefaults standardUserDefaults]valueForKey:KLoginInfo];
    //    NSString *userID = [profileInfo valueForKey:@"UserID"];
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_DELETE_ADDRESS];
    
    url = [NSString stringWithFormat:@"%@/%@/%@",url,addressInfoObj.address_Id,accessToken];
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    
    if (internetStatus != NotReachable) {
        [SVProgressHUD showWithStatus:KLoadingMessage];
        
        [handler jsonDELETE_DATA:url header:accessToken parameters:nil onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                @try {
                    if (completed) {
                        if (APP_DEBUG_MODE) {
                            NSLog(@"dict is %@",completed);
                        }
                        [self.addressInfoArray removeObject:addressInfoObj];
                        [self.profileCollectionView reloadData];
                    }
                } @catch (NSException *exception) {
                    //[EShopUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}

#pragma mark - Methods and Alert Diaologs for handling events

- (void)showAlertDialogForDeleteAddress:(AddressModel *)infoObj {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:NSLocalizedString(@"Are you sure? You want to Delete", @"")
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"NO", @"")
                                                           style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                   {
                                       [self dismissViewControllerAnimated:YES completion:nil];
                                   }];
    
    UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"YES", @"")
                                                           style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                   {
                                       [self DeleteExistingAddressTo_registered_Users:infoObj];
                                   }];
    [alert addAction:cancelAction];
    [alert addAction:deleteAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
