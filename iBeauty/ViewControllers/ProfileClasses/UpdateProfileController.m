//
//  UpdateProfileController.m
//  iBeauty
//
//  Created by App Innovation on 26/10/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "UpdateProfileController.h"
#import "Constants.h"

@interface UpdateProfileController ()<UITextFieldDelegate>

@property (nonatomic,weak) IBOutlet UIView *userNameView;
@property (nonatomic,weak) IBOutlet UIView *emailView;
@property (nonatomic,weak) IBOutlet UIView *phoneNumberView;
@property (nonatomic,weak) IBOutlet UITextField *userNameTextField;
@property (nonatomic,weak) IBOutlet UITextField *emailTextField;
@property (nonatomic,weak) IBOutlet UITextField *phoneNumberTextField;

@property (nonatomic,weak) IBOutlet UIButton *updateButton;
@property (nonatomic,weak) IBOutlet UINavigationItem *navigationItem;
@property (nonatomic,weak) IBOutlet UIBarButtonItem *cancelButton;
@property (nonatomic,weak) IBOutlet UILabel *userNameLabel;
@property (nonatomic,weak) IBOutlet UILabel *emailIdLabel;
@property (nonatomic,weak) IBOutlet UILabel *phoneNumberLabel;

@property UIToolbar *toolBar;
@property (nonatomic, strong) UIBarButtonItem   *doneButton;
@property (nonatomic,weak)  IBOutlet UINavigationBar *navBar;

@end

@implementation UpdateProfileController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setUpViews];
    [self setUpLocalizationViews];
    [self setupToolBarOnTextfields];
    [self setUpTextFieldsCursorAccordingToLanguage];
    
    //[self setGradient];
    [self setStatusBarColor];
}

#pragma mark - Set Gradient

- (void)setGradient {
    
    self.navBar.frame = [iBeautyUtility setUpFrame:self.navBar.frame];
    CAGradientLayer *gradient = [iBeautyUtility setNavigationGradient:self.navBar];
    [self.navBar setBackgroundImage:[iBeautyUtility imageFromLayer:gradient] forBarMetrics:UIBarMetricsDefault];
}

#pragma mark - Set Status Bar Color

- (void)setStatusBarColor {

    if (@available(iOS 13.0, *)) {
        
        UIView *statusBar = [[UIView alloc]initWithFrame:[UIApplication sharedApplication].keyWindow.windowScene.statusBarManager.statusBarFrame] ;
        [self.view addSubview:statusBar];
        if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
            
            statusBar.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:130/255.0 blue:90/255.0 alpha:1.0];
            //[statusBar.layer insertSublayer:[self setGradientColor_Views:statusBar] atIndex:0];
        }
    }else {
            UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
            if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
                
                statusBar.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:130/255.0 blue:90/255.0 alpha:1.0];
                //[statusBar.layer insertSublayer:[self setGradientColor_Views:statusBar] atIndex:0];
            }
    }

}

- (CAGradientLayer*)setGradientColor_Views:(UIView*)sender {
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = sender.bounds;
    gradient.startPoint = CGPointMake(0, 0);
    gradient.endPoint = CGPointMake(1, 0);
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:233.0/255.0 green:83/255.0 blue:158/255.0 alpha:1.0] CGColor],(id)[[UIColor colorWithRed:240/255.0 green:59.0/255.0 blue:53.0/255.0 alpha:1.0] CGColor], nil];
    
    return gradient;
}

#pragma mark - set Up localization views

- (void)setUpLocalizationViews {
    
    self.navigationItem.title = NSLocalizedString(@"Update Profile", @"");
    self.cancelButton.title = NSLocalizedString(@"Cancel", @"");
    self.userNameLabel.text = NSLocalizedString(@"User name", @"");
    self.emailIdLabel.text = NSLocalizedString(@"Email id", @"");
    self.phoneNumberLabel.text = NSLocalizedString(@"Phone number", @"");
    [self.updateButton setTitle:NSLocalizedString(@"Update", @"") forState:UIControlStateNormal];
    
    self.userNameTextField.placeholder = NSLocalizedString(@"User name", @"") ;
    self.emailTextField.placeholder = NSLocalizedString(@"Email id", @"");
    self.phoneNumberTextField.placeholder = NSLocalizedString(@"Phone number", @"");

}

- (void)setUpTextFieldsCursorAccordingToLanguage {
    
    NSString *languageCode =   [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];
    
    if ([languageCode isEqualToString:@"en"]) {
        
        self.userNameTextField.textAlignment = NSTextAlignmentLeft;
        self.emailTextField.textAlignment = NSTextAlignmentLeft;
        self.phoneNumberTextField.textAlignment = NSTextAlignmentLeft;
        
    }else if ([languageCode isEqualToString:@"ar"]) {
        
        self.userNameTextField.textAlignment = NSTextAlignmentRight;
        self.emailTextField.textAlignment = NSTextAlignmentRight;
        self.phoneNumberTextField.textAlignment = NSTextAlignmentRight;

    }
}

#pragma mark - set Up views

- (void)setUpViews {
    
    [iBeautyUtility setViewBorderRound:self.userNameView];
    [iBeautyUtility setViewBorderRound:self.emailView];
    [iBeautyUtility setViewBorderRound:self.phoneNumberView];
    self.updateButton.frame = [iBeautyUtility setUpButtonFrame:self.updateButton.frame];
    [iBeautyUtility setButtonCorderRound:self.updateButton];
    //[self.updateButton.layer insertSublayer:[iBeautyUtility setGradientColor_Buttons:self.updateButton] atIndex:0];

}

#pragma mark - Cancel Button

- (IBAction)updateButtonAction:(id)sender {
    
    BOOL validStatus =  [self checkValidationForUpdateFields];
    
    if (validStatus == YES) {
        
        
    }else {
        
        return;
    }
    
}

- (BOOL)checkValidationForUpdateFields {
    
    if (self.userNameTextField.text.length > 0 ) {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"Please enter user name", @"")];
        [self.userNameTextField becomeFirstResponder];
        return NO ;
    }
    if (self.emailTextField.text.length < 1) {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"Please enter email address", @"")];
        [self.emailTextField becomeFirstResponder];
        return NO ;
    }
    if (self.emailTextField.text.length > 0  && ![iBeautyUtility isValidEmailAddress:self.emailTextField.text]) {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"Please enter valid email address", @"")];
        [self.emailTextField becomeFirstResponder];
        return NO ;
    }
    if (self.phoneNumberTextField.text.length < 1) {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"Please enter phone number", @"")];
        [self.phoneNumberTextField becomeFirstResponder];
        return NO ;
    }
    
    return YES;
}


#pragma mark - set up Tool Bar On Text fields

- (void)setupToolBarOnTextfields {
    
    self.toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50)];
    self.toolBar.barStyle = UIBarStyleDefault;
    [self.toolBar sizeToFit];
    [self.toolBar setBackgroundColor:[UIColor lightGrayColor]];
    [self buttonWithinToolbar];

    self.userNameTextField.inputAccessoryView = self.toolBar;
    self.emailTextField.inputAccessoryView = self.toolBar;
    self.phoneNumberTextField.inputAccessoryView = self.toolBar;
    
}
- (void)buttonWithinToolbar {
    
    self.doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", @"") style:UIBarButtonItemStyleDone target:self action:@selector(doneAction:)];
    
    UIBarButtonItem *flexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [self.toolBar setItems:@[flexibleItem,self.doneButton] animated:NO];
}

- (void)doneAction:(id)sender {
    
    [self.userNameTextField resignFirstResponder];
    [self.emailTextField resignFirstResponder];
    [self.phoneNumberTextField resignFirstResponder];
    
}

# pragma mark - Textfield Delegate Methods

-(BOOL) textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Cancel Button

- (IBAction)CancelButtonAction:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
