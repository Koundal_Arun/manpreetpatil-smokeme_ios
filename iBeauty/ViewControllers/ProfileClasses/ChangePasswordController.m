//
//  ChangePasswordController.m
//  iBeauty
//
//  Created by App Innovation on 26/10/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "ChangePasswordController.h"
#import "Constants.h"

@interface ChangePasswordController ()<UITextFieldDelegate>

@property (nonatomic,weak) IBOutlet UIView *oldPasswordView;
@property (nonatomic,weak) IBOutlet UIView *view_newPass;
@property (nonatomic,weak) IBOutlet UIView *view_confirmPassword;
@property (nonatomic,weak) IBOutlet UITextField *oldPasswordTextField;
@property (nonatomic,weak) IBOutlet UITextField *textF_newPass;
@property (nonatomic,weak) IBOutlet UITextField *textF_ConfirmPass;

@property (nonatomic,weak) IBOutlet UIButton *saveButton;
@property (nonatomic,weak) IBOutlet UINavigationItem *navigationItem;
@property (nonatomic,weak) IBOutlet UIBarButtonItem *cancelButton;
@property (nonatomic,weak) IBOutlet UILabel *currentPasswordLabel;
@property (nonatomic,weak) IBOutlet UILabel *passwordNewLabel;
@property (nonatomic,weak) IBOutlet UILabel *confirmPasswordLabel;

@property UIToolbar *toolBar;
@property (nonatomic, strong) UIBarButtonItem   *doneButton;
@property (nonatomic,weak)  IBOutlet UINavigationBar *navBar;

@end

@implementation ChangePasswordController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setUpViews];
    [self setUpLocalizationViews];
    [self setupToolBarOnTextfields];
    [self setUpTextFieldsCursorAccordingToLanguage];
    
    //[self setGradient];
    [self setStatusBarColor];

}

#pragma mark - Set Gradient

- (void)setGradient {
    
    self.navBar.frame = [iBeautyUtility setUpFrame:self.navBar.frame];
    CAGradientLayer *gradient = [iBeautyUtility setNavigationGradient:self.navBar];
    [self.navBar setBackgroundImage:[iBeautyUtility imageFromLayer:gradient] forBarMetrics:UIBarMetricsDefault];
}

#pragma mark - Set Status Bar Color

- (void)setStatusBarColor {

    if (@available(iOS 13.0, *)) {
        
        UIView *statusBar = [[UIView alloc]initWithFrame:[UIApplication sharedApplication].keyWindow.windowScene.statusBarManager.statusBarFrame] ;
        [self.view addSubview:statusBar];
        if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
            
            statusBar.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:130/255.0 blue:90/255.0 alpha:1.0];
            //[statusBar.layer insertSublayer:[self setGradientColor_Views:statusBar] atIndex:0];
        }
    }else {
            UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
            if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
                
                statusBar.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:130/255.0 blue:90/255.0 alpha:1.0];
                //[statusBar.layer insertSublayer:[self setGradientColor_Views:statusBar] atIndex:0];
            }
    }

}

- (CAGradientLayer*)setGradientColor_Views:(UIView*)sender {
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = sender.bounds;
    gradient.startPoint = CGPointMake(0, 0);
    gradient.endPoint = CGPointMake(1, 0);
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:233.0/255.0 green:83/255.0 blue:158/255.0 alpha:1.0] CGColor],(id)[[UIColor colorWithRed:240/255.0 green:59.0/255.0 blue:53.0/255.0 alpha:1.0] CGColor], nil];
    
    return gradient;
}

#pragma mark - set Up localization views

- (void)setUpLocalizationViews {
    
    self.navigationItem.title = NSLocalizedString(@"Change Password", @"");
    self.cancelButton.title = NSLocalizedString(@"Cancel", @"");
    self.currentPasswordLabel.text = NSLocalizedString(@"Current Password", @"");
    self.passwordNewLabel.text = NSLocalizedString(@"New Password", @"");
    self.confirmPasswordLabel.text = NSLocalizedString(@"Confirm Password", @"");
    [self.saveButton setTitle:NSLocalizedString(@"Save", @"") forState:UIControlStateNormal];
    
    self.oldPasswordTextField.placeholder = NSLocalizedString(@"Current Password", @"") ;
    self.textF_newPass.placeholder = NSLocalizedString(@"New Password", @"");
    self.textF_ConfirmPass.placeholder = NSLocalizedString(@"Confirm Password", @"");
    
}

#pragma mark - set Up views

- (void)setUpViews {
    
    [iBeautyUtility setViewBorderRound:self.oldPasswordView];
    [iBeautyUtility setViewBorderRound:self.view_newPass];
    [iBeautyUtility setViewBorderRound:self.view_confirmPassword];
    
    self.saveButton.frame = [iBeautyUtility setUpButtonFrame:self.saveButton.frame];
    [iBeautyUtility setButtonCorderRound:self.saveButton];
    
    //[self.saveButton.layer insertSublayer:[iBeautyUtility setGradientColor_Buttons:self.saveButton] atIndex:0];

}

- (void)setUpTextFieldsCursorAccordingToLanguage {
    
    NSString *languageCode =   [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];
    
    if ([languageCode isEqualToString:@"en"]) {
        
        self.oldPasswordTextField.textAlignment = NSTextAlignmentLeft;
        self.textF_newPass.textAlignment = NSTextAlignmentLeft;
        self.textF_ConfirmPass.textAlignment = NSTextAlignmentLeft;
        
    }else if ([languageCode isEqualToString:@"ar"]) {
        
        self.oldPasswordTextField.textAlignment = NSTextAlignmentRight;
        self.textF_newPass.textAlignment = NSTextAlignmentRight;
        self.textF_ConfirmPass.textAlignment = NSTextAlignmentRight;
        
    }
}

#pragma mark - Cancel Button

- (IBAction)SaveButtonAction:(id)sender {
    
    BOOL validStatus =  [self checkValidationForChangePasswordFields];
    
    if (validStatus == YES) {
        
        [self getChangePassword];
        
    }else {
        
        return;
    }
}

#pragma mark - Change Password

- (void)getChangePassword {
    
    //{"email":"manpreet.patil@gmail.com","current_password":"1234","password":"123"}
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_CHANGE_PASSWORD];
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];

    NSDictionary *profileInfo = [[NSUserDefaults standardUserDefaults]valueForKey:KLoginInfo];
    NSString *emailId = [profileInfo valueForKey:KEmailId];
    
    NSDictionary *dictInfo = [NSDictionary dictionaryWithObjectsAndKeys:accessToken,@"Authorization",emailId,@"email",self.oldPasswordTextField.text,@"current_password",self.textF_newPass.text,@"password", nil];
    
    if (internetStatus != NotReachable) {
        
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonPostData:url params:dictInfo onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                @try {
                    if (completed) {
                        
                        if (APP_DEBUG_MODE)
                            NSLog(@"Value is %@", completed);
                        
                        if ([completed isEqualToString:@"true"]) {
                            [iBeautyUtility showAlertMessage:NSLocalizedString(@"Password updated successfully", @"")];
                            [self dismissViewControllerAnimated:YES completion:nil];
                        }else {
                            [iBeautyUtility showAlertMessage:NSLocalizedString(@"Wrong current password entered", @"")];
                        }
                        
                    }else {
                        if (APP_DEBUG_MODE)
                            NSLog(@"error is %@", error.localizedDescription);
                        [SVProgressHUD dismiss];
                    }
                    
                } @catch (NSException *exception) {
                    //[EShopUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                    [SVProgressHUD dismiss];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}

/*{"email":"arun.ios@gmail.com","current_password":"12345","password":"test123",
 "Authorization": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjQxQkMzNzJFLUMxRDEtMTk0NS00MTI3LTc1M0U0QTNCMzM5MC44MjdjY2IwZWVhOGE3MDZjNGMzNGExNjg5MWY4NGU3YiJ9.bJMhf8bJolz_wiW1sgTHaMaW3gn8vKfLWUStRcWa2qE"
 }*/

- (BOOL)checkValidationForChangePasswordFields {
    

    if (self.oldPasswordTextField.text.length < 1 ) {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"Please enter current password", @"")];
        return NO ;
    }
    if (self.textF_newPass.text.length < 1) {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"Please enter new password", @"")];
        return NO ;
    }
    if (self.textF_ConfirmPass.text.length < 1) {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"Please enter confirm password", @"")];
        return NO ;
    }
    if (![self.textF_ConfirmPass.text isEqualToString: self.textF_newPass.text]) {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"Password does not match", @"")];
        return NO ;
    }

    return YES;
}

#pragma mark - set up Tool Bar On Text fields

- (void)setupToolBarOnTextfields {
    
    self.toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50)];
    self.toolBar.barStyle = UIBarStyleDefault;
    [self.toolBar sizeToFit];
    [self.toolBar setBackgroundColor:[UIColor lightGrayColor]];
    [self buttonWithinToolbar];
    
    self.oldPasswordTextField.inputAccessoryView = self.toolBar;
    self.textF_newPass.inputAccessoryView = self.toolBar;
    self.textF_ConfirmPass.inputAccessoryView = self.toolBar;

}
- (void)buttonWithinToolbar {
    
    self.doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", @"") style:UIBarButtonItemStyleDone target:self action:@selector(doneAction:)];
    
    UIBarButtonItem *flexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [self.toolBar setItems:@[flexibleItem,self.doneButton] animated:NO];
}

- (void)doneAction:(id)sender {
    
    [self.oldPasswordTextField resignFirstResponder];
    [self.textF_newPass resignFirstResponder];
    [self.textF_ConfirmPass resignFirstResponder];

}

# pragma mark - Textfield Delegate Methods

-(BOOL) textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Cancel Button

- (IBAction)CancelButtonAction:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
