//
//  PriceCartViewController.h
//  iBeauty
//
//  Created by App Innovation on 09/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "AddressModel.h"

@interface PriceCartViewController : UIViewController

@property (nonatomic,strong) NSMutableArray *serviceDetailsArray;
@property (nonatomic,strong) ServicesCategoriesInfo *categoryInfoObj;

@property (nonatomic,strong) AddressModel *addressModel;
@property (nonatomic,assign) BOOL screenStatus;
@property (nonatomic,assign) NSInteger pickStatus;

@end
