//
//  OffersViewController.m
//  iBeauty
//
//  Created by App Innovation on 27/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "OffersViewController.h"
#import "Constants.h"
#import "OffersCollCell.h"
#import "OffersDetail.h"
#import "NSString+iBeautyString.h"
#import "OffersDetailViewController.h"

#import <CoreLocation/CoreLocation.h>


static NSString *offersCellIdentifier = @"OffersCollCell";

@interface OffersViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,CLLocationManagerDelegate>

@property (nonatomic,weak) IBOutlet UICollectionView *offersCollectionView;
@property (nonatomic,strong) NSMutableArray *offersArray;

@property (nonatomic,strong) CLLocationManager *locationManager;
@property (nonatomic,strong) CLLocation *currentLocation;
@property (nonatomic,assign) NSInteger count;
@property (nonatomic,strong) NSMutableArray *mainInfoArray;
@property (nonatomic,weak) IBOutlet UIView *noOffersView;
@property (nonatomic,weak)  IBOutlet UINavigationBar *navBar;

@property (nonatomic,strong)  AppDelegate *delegate;

@end

@implementation OffersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self configureLocationManager];
    //[self setGradient];
    self.delegate = [AppDelegate sharedInstance];

}

#pragma mark - View Will Appear

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    self.delegate.tabbarController.tabBar.hidden = NO;
}

#pragma mark - Set Gradient

- (void)setGradient {
    
    self.navBar.frame = [iBeautyUtility setUpFrame:self.navBar.frame];
    CAGradientLayer *gradient = [iBeautyUtility setNavigationGradient:self.navBar];
    [self.navBar setBackgroundImage:[iBeautyUtility imageFromLayer:gradient] forBarMetrics:UIBarMetricsDefault];
}

#pragma mark - View For No Offers Available

- (void)viewForNoOffersAvailable {
    
    if (self.offersArray.count<1) {
        [self.view addSubview:self.noOffersView];
        self.noOffersView.center = self.view.center;
        self.offersCollectionView.hidden = YES;
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"No Offers Available", @"")];
    }else {
        self.offersCollectionView.hidden = NO;
        [self.noOffersView removeFromSuperview];
    }
    
}

#pragma mark - Configure Location Manager

-(void)configureLocationManager {
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        [self.locationManager requestWhenInUseAuthorization];
    
    [self.locationManager startUpdatingLocation];
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    self.currentLocation = locations.lastObject;
    
    if ( self.currentLocation && self.count == 0) {
        self.count += 1;
        [self.locationManager stopUpdatingLocation];
        [self getServicesOffersList];
    }
}


- (void)getServicesOffersList {

    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_SERVICES_OFFER_LIST];
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];

    NSString *latitude = [NSString stringWithFormat:@"%@",[NSNumber numberWithDouble:self.currentLocation.coordinate.latitude]];
    NSString *longitude = [NSString stringWithFormat:@"%@",[NSNumber numberWithDouble:self.currentLocation.coordinate.longitude]];
    
    NSDictionary *dictInfo = [NSDictionary dictionaryWithObjectsAndKeys:accessToken,@"Authorization",latitude,@"Latitude",longitude,@"Longitude",nil];
    
    if (internetStatus != NotReachable) {
        
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonPostData:url params:dictInfo onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                
                @try {
                    if (completed) {
                        
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSMutableArray *infoArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE) { NSLog(@"Info Array is %@", infoArray); }
                        
                        OffersDetail *offersInfoDetail  = [OffersDetail getSharedInstance];
                        self.offersArray = [offersInfoDetail offersDetailInfoObj:infoArray];
                        
                        if (APP_DEBUG_MODE) { NSLog(@"Offers Array ->>> %@", self.offersArray); }
                        
                        [self viewForNoOffersAvailable];
                        [self.offersCollectionView reloadData];
                        
                    }else {
                        if (APP_DEBUG_MODE) { NSLog(@"error is %@", error.localizedDescription); }
                    }
                    
                } @catch (NSException *exception) {
                    [iBeautyUtility showAlertMessage:NSLocalizedString(@"No Response", @"")];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
    
}


#pragma mark - Show menu action method

- (IBAction)showMenu:(id)sender {
    
    NSString *languageCode =   [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];
    
    if ([languageCode isEqualToString:@"en"]) {
        
        [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
        
    }else if ([languageCode isEqualToString:@"ar"]) {
        
        [self.mm_drawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:nil];
        
    }
    
}


#pragma mark - CollectioView DataSource and Delegate methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.offersArray.count;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    OffersCollCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:offersCellIdentifier forIndexPath:indexPath];
    OffersDetail *detailObj = self.offersArray[indexPath.item];
    [cell configureOffersCollectionCell:detailObj];
    cell.viewDetail.tag = indexPath.row;
    [cell.viewDetail addTarget:self action:@selector(viewOfferDetail:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat screenWidth = CGRectGetWidth([UIScreen mainScreen].bounds);
    
    CGFloat itemWidth = 0.0f;
    
    itemWidth = floor(screenWidth);
    
    return CGSizeMake(itemWidth, 317.0f);
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 1.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 1.0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    [self moveToViewOfferDetail:indexPath.row];
    
}

-(void)viewOfferDetail:(UIButton*)sender {
    
    [self moveToViewOfferDetail:sender.tag];
    
}

- (void)extracted:(OffersDetailViewController *)offerDetailController {
    [self.navigationController pushViewController:offerDetailController animated:YES];
}

- (void)moveToViewOfferDetail:(NSInteger)indexRow {
    
    OffersDetail *detailObj = self.offersArray[indexRow];
    
    OffersDetailViewController *offerDetailController = [self.storyboard instantiateViewControllerWithIdentifier:kOfferDetailController];
    offerDetailController.offerInfo = detailObj;
    offerDetailController.offerTypeStatus = NO;
    offerDetailController.screenStatus = NO;
    [self extracted:offerDetailController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
