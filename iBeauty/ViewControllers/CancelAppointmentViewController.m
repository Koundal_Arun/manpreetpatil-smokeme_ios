//
//  CancelAppointmentViewController.m
//  iBeauty
//
//  Created by App Innovation on 17/12/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "CancelAppointmentViewController.h"

@interface CancelAppointmentViewController ()<UITextViewDelegate>

@property (nonatomic,weak) IBOutlet UIView *bkgView;

@property (nonatomic,weak) IBOutlet UITextView *reasonTextView;
@property (nonatomic,weak) IBOutlet UILabel *placeHolderLabel;
@property (nonatomic,weak) IBOutlet UIButton *cancelAppointmentButton;
@property (nonatomic,weak) IBOutlet UINavigationItem *navigationItem;
@property (nonatomic,weak) IBOutlet UILabel *writeAReasonLabel;
@property (nonatomic,weak) IBOutlet UIBarButtonItem *cancelButton;

@property UIToolbar *toolBar;
@property (nonatomic, strong) UIBarButtonItem   *doneButton;
@property (nonatomic,weak)  IBOutlet UINavigationBar *navBar;

@end

@implementation CancelAppointmentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.reasonTextView becomeFirstResponder];
    [self setUpViewForLocalization];
    [self setupToolBarOnTextView];
    [self setUpTextViewCursorAccordingToLanguage];
    //[self setGradient];
    [iBeautyUtility setButtonCorderRound:self.cancelAppointmentButton];

}


#pragma mark - Set Gradient

- (void)setGradient {
    
    self.navBar.frame = [iBeautyUtility setUpFrame:self.navBar.frame];
    CAGradientLayer *gradient = [iBeautyUtility setNavigationGradient:self.navBar];
    [self.navBar setBackgroundImage:[iBeautyUtility imageFromLayer:gradient] forBarMetrics:UIBarMetricsDefault];
    
    self.cancelAppointmentButton.frame = [iBeautyUtility setUpButtonFrame:self.cancelAppointmentButton.frame];
    //[self.cancelAppointmentButton.layer insertSublayer:[iBeautyUtility setGradientColor_Buttons:self.cancelAppointmentButton] atIndex:0];
    
}

#pragma mark - Set Up View For Localization

- (void)setUpViewForLocalization {
    
    self.placeHolderLabel.text = NSLocalizedString(@"Write your reason", @"");
    self.writeAReasonLabel.text = NSLocalizedString(@"Write your reason", @"");
    [self.cancelButton setTitle:NSLocalizedString(@"Cancel", @"")];
    
    if (self.cancelType == 1) {
        [self.navigationItem setTitle:NSLocalizedString(@"Cancel Appointment", @"")];
        [self.cancelAppointmentButton setTitle:NSLocalizedString(@"Cancel Appointment", @"") forState:UIControlStateNormal];
    }else {
        [self.navigationItem setTitle:NSLocalizedString(@"Cancel Order", @"")];
        [self.cancelAppointmentButton setTitle:NSLocalizedString(@"Cancel Order", @"") forState:UIControlStateNormal];
    }

}

- (void)setUpTextViewCursorAccordingToLanguage {
    
    NSString *languageCode = [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];
    
    if ([languageCode isEqualToString:@"en"]) {
        
        self.reasonTextView.textAlignment = NSTextAlignmentLeft;
        
    }else if ([languageCode isEqualToString:@"ar"]) {
        
        self.reasonTextView.textAlignment = NSTextAlignmentRight;
    }
}

#pragma mark - Cancel Appointment/Package/Order

- (IBAction)cancelAppointmentAction:(UIButton*)sender {
    
    if (self.cancelType == 1) {
        [self cancelAppointmentAPICall];
    }
    
}

#pragma mark - Cancel Appointment

- (void)cancelAppointmentAPICall {
    
    NSString *url;
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    NSString *reasonToCancel = self.reasonTextView.text;
    
    NSDictionary *dictInfo;
    
    if (self.appointmentOrderObj.isOfferApplied == NO) {
        
        url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_CANCEL_APPOINTMENT];
        
        dictInfo = [NSDictionary dictionaryWithObjectsAndKeys:accessToken,@"Authorization",self.appointmentOrderObj.orderId,@"BookingId",self.appointmentOrderDetailObj.appointmentId,@"ServiceBookingId",reasonToCancel,@"ReasonToCancel",nil];
    }else {
        
        url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_CANCEL_PACKAGE];

        dictInfo = [NSDictionary dictionaryWithObjectsAndKeys:accessToken,@"Authorization",self.appointmentOrderObj.orderId,@"BookingId",self.appointmentOrderObj.offerID,@"OfferId",reasonToCancel,@"ReasonToCancel",nil];
    }
    
    if (internetStatus != NotReachable) {
        
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonPostData:url params:dictInfo onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                
                @try {
                    if (completed) {
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSDictionary *tempInfoDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE)
                            NSLog(@"Info Dict is %@", tempInfoDict);
                        [self removeNotificationsForAppointment];
                        NSInteger status = [[tempInfoDict valueForKey:@"status"]integerValue];
                        if (status == 200) {
                            [self moveToBackView:tempInfoDict];
                        }
                    }else {
                        if (APP_DEBUG_MODE)
                            NSLog(@"error is %@", error.localizedDescription);
                    }
                    
                } @catch (NSException *exception) {
                    [iBeautyUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}


#pragma mark- remove Notification for task

- (void)removeNotificationsForAppointment {
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"10.0")) {
        
        NSMutableArray *identifiersArray = [[NSMutableArray alloc]init];
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        [center getPendingNotificationRequestsWithCompletionHandler:^(NSArray<UNNotificationRequest *> * _Nonnull notifications) {
            for (UNNotificationRequest *request in notifications) {
                NSMutableArray *idsArray = [request.content.userInfo valueForKey:@"AppointmentIDs"];
                if ([idsArray containsObject:self.appointmentOrderObj.appointmentId]) {
                    [identifiersArray addObject:self.appointmentOrderObj.appointmentId] ;
                }
            }
            [center removePendingNotificationRequestsWithIdentifiers:identifiersArray];
        }];
    }
    
}

- (void)moveToBackView:(NSDictionary*)tempInfoDict {
    
//    [iBeautyUtility showAlertMessage:[tempInfoDict valueForKey:@"message"]];
    
    if (self.appointmentOrderObj.isOfferApplied == NO) {
        if (self.appointmentsNumber > 1) {
            NSDictionary *infoDict  = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.selectedIndex],@"index",nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"cancelAppointment" object:nil userInfo:infoDict];
            [self.navigationController popViewControllerAnimated:YES];
        }else {
            [self popToParticularView];
        }
    }else {
        [self popToParticularView];
    }
    
}

- (void)popToParticularView {
    
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++){
        id obj=[viewControllers objectAtIndex:i];
        if([obj isKindOfClass:[MyAppointmentViewController class]]){
            [[self navigationController] popToViewController:obj animated:YES];
            return;
        }
    }
    
}

#pragma mark - Cancel Order


- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];

}

#pragma mark - set up Tool Bar On Text View

- (void)setupToolBarOnTextView {
    
    self.toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50)];
    self.toolBar.barStyle = UIBarStyleDefault;
    [self.toolBar sizeToFit];
    [self.toolBar setBackgroundColor:[UIColor lightGrayColor]];
    [self buttonWithinToolbar];
    
    self.reasonTextView.inputAccessoryView = self.toolBar;
    
}

- (void)buttonWithinToolbar {
    
    self.doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", @"") style:UIBarButtonItemStyleDone target:self action:@selector(doneAction:)];
    
    UIBarButtonItem *flexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [self.toolBar setItems:@[flexibleItem,self.doneButton] animated:NO];
}

- (void)doneAction:(id)sender {
    
    [self.reasonTextView resignFirstResponder];
    
}


#pragma mark - Text view delegates

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    
    self.reasonTextView.text = @"";
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    
    if (![self.reasonTextView hasText]) {
        self.placeHolderLabel.hidden = NO;
    }
}

- (void) textViewDidChange:(UITextView *)textView {
    
    if(![self.reasonTextView hasText]) {
        self.placeHolderLabel.hidden = NO;
    }
    else{
        self.placeHolderLabel.hidden = YES;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
