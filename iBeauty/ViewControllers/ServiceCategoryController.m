//
//  ServiceCategoryController.m
//  iBeauty
//
//  Created by App Innovation on 28/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "ServiceCategoryController.h"
#import "Constants.h"
#import "ServicesCategoriesInfo.h"
#import "AllMassagesCell.h"
#import "CategoryTableHeaderView.h"
#import "NSString+iBeautyString.h"
#import "PromotionalAds.h"
#import "OffersDetailViewController.h"
#import <SafariServices/SafariServices.h>
#import <CoreLocation/CoreLocation.h>

#import "SelectCityController.h"
#import "CityInfoModel.h"


static NSString *recommendedCellIdentifier = @"RecommendedCell";
static NSString *allCategoryCellIdentifier = @"allCategoryCell";

@interface ServiceCategoryController ()<CLLocationManagerDelegate,SFSafariViewControllerDelegate>

@property (nonatomic,strong) NSMutableArray *serviceCategoriesArray;
@property (nonatomic,weak) IBOutlet UITableView *serviceCategoryTableView;

@property (nonatomic,strong) NSMutableArray *array_AroundYou;
@property (nonatomic,strong) NSMutableArray *array_TopRated;
@property (nonatomic,strong) NSMutableArray *array_ViewAll;

@property (nonatomic,weak) IBOutlet UISegmentedControl *segmentControl;
@property (nonatomic,assign) NSInteger segmentIndex;

@property (nonatomic,weak) IBOutlet UIView *blankScreenView;
@property (nonatomic,weak) IBOutlet UILabel *noDataLabel;

@property (nonatomic,weak) IBOutlet UIView *fiterView;
@property (nonatomic,weak) IBOutlet UIView *fiterButton;
@property (nonatomic,strong) NSDictionary *filterDict;
@property (nonatomic,strong) NSMutableArray *filterInfoArray;
@property (nonatomic,assign) BOOL filterStatus;

@property (nonatomic,strong) CLLocationManager *locationManager;
@property (nonatomic,strong) CLLocation *currentLocation;
@property (nonatomic,assign) NSInteger count;


@property (nonatomic,weak) IBOutlet UIView *cityView;
@property (nonatomic,weak) IBOutlet UILabel *cityNameLabel;
@property (nonatomic,weak) IBOutlet UIButton *selectCityButton;
@property (nonatomic,weak) IBOutlet UILabel *cityTitle;

@property (nonatomic,strong) AppDelegate *delegate;
@property (nonatomic,weak)  IBOutlet UINavigationBar *navBar;

@property  UIImage *gradientImage;

@end

@implementation ServiceCategoryController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.count = 0;
    self.fiterView.hidden = YES;
    [self setUpViews];
    [self configureLocationManager];
    [self setUpNamesForSegmentControl];
    [self addObseverForPromotionalOffer];
    self.delegate = [AppDelegate sharedInstance];
    [self setUpViewForLocalization];
    [self setUpLocationLabel];
    //[self setGradient];
}

#pragma mark - View Will Appear

- (void)viewWillAppear:(BOOL)animated {
    
    [self setUpTableViewForAllCategoryCell];
    [super viewWillAppear:YES];
    
}

- (void) viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    if (self.segmentIndex == 1) {
        self.serviceCategoryTableView.tableHeaderView = nil ;
    }
}

#pragma mark - Set Gradient

- (void)setGradient {
    
    self.navBar.frame = [iBeautyUtility setUpFrame:self.navBar.frame];
    CAGradientLayer *gradient = [iBeautyUtility setNavigationGradient:self.navBar];
    [self.navBar setBackgroundImage:[iBeautyUtility imageFromLayer:gradient] forBarMetrics:UIBarMetricsDefault];
    
//    CAGradientLayer *selectedSegGradient = [iBeautyUtility setSegmentGradient:self.segmentControl];
//    CAGradientLayer *unSelectedSegGradient = [iBeautyUtility setSegmentGradient:self.segmentControl];
//    self.segmentControl.tintColor = [UIColor colorWithPatternImage:[iBeautyUtility imageFromLayer:selectedSegGradient]];
//    [self.segmentControl setBackgroundImage:[iBeautyUtility imageFromLayer:selectedSegGradient] forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
//    [self.segmentControl setBackgroundImage:[iBeautyUtility imageFromLayer:unSelectedSegGradient] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];

}

#pragma mark - Set Up location label

- (void)setUpViewForLocalization {
    
    self.cityTitle.text = NSLocalizedString(@"Selected Location", @"");
}

#pragma mark - Set Up location label

- (void)setUpLocationLabel {
    
    self.delegate = [AppDelegate sharedInstance];
    
    NSString *languageCode =   [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];
    if ([languageCode isEqualToString:@"en"]) {
        if ([self.delegate.selectedCityName isEqualToString:@"الموقع الحالي"]) {
            self.delegate.selectedCityName = @"Current Location";
        }
    }else if ([languageCode isEqualToString:@"ar"]) {
        if ([self.delegate.selectedCityName isEqualToString:@"Current Location"]) {
            self.delegate.selectedCityName = @"الموقع الحالي";
        }
    }
    
    if ([self.delegate.selectedCityName isEqualToString:NSLocalizedString(@"Current Location", @"")]) {
        self.providerStatus = NO;
    }else {
        self.providerStatus = YES;
    }
    self.cityNameLabel.text = self.delegate.selectedCityName;
}


#pragma mark - Select City View

- (IBAction)selectCityView:(id)sender {
    
    SelectCityController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:kSelectCityController];
    viewController.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:viewController animated:YES completion:nil];
}

#pragma mark - Show menu action method

- (IBAction)showMenu:(id)sender {
    
    NSString *languageCode =   [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];
    
    if ([languageCode isEqualToString:@"en"]) {
        
        [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
        
    }else if ([languageCode isEqualToString:@"ar"]) {
        
        [self.mm_drawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:nil];
        
    }
}

#pragma mark - Configure Location Manager

-(void)configureLocationManager {
    
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        [self.locationManager requestWhenInUseAuthorization];
    
    [self.locationManager startUpdatingLocation];
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    self.currentLocation = locations.lastObject;
    
    self.delegate = [AppDelegate sharedInstance];
    self.delegate.latitude = [NSString stringWithFormat:@"%@",[NSNumber numberWithDouble:self.currentLocation.coordinate.latitude]];
    self.delegate.longitude = [NSString stringWithFormat:@"%@",[NSNumber numberWithDouble:self.currentLocation.coordinate.longitude]];
    
    if ( self.currentLocation && self.count == 0) {
        self.count += 1;
        [self.locationManager stopUpdatingLocation];
        [self getServiceCategories:NO];
    }
}

#pragma mark - Promotional Oberver

- (void)addObseverForPromotionalOffer {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getPromotionOfferIndex:) name:@"promotionalOfferIndex" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getCityInfo:) name:@"getCityInfoObject" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(clearLocationInfo:) name:@"clearLocationInfo" object:nil];

}

- (void)getPromotionOfferIndex:(NSNotification*)Info {
    
    NSDictionary *promotionInfo = Info.userInfo;
    if (APP_DEBUG_MODE) { NSLog(@"Info is %@",promotionInfo); }
    
    NSInteger indexValue = [[promotionInfo valueForKey:@"Index"]integerValue];
    PromotionalAds *adInfo = self.promotionAdsInfo[indexValue];
    
    if ([adInfo.promotionalType isEqualToString:@"Internal"]) {
        
        [self getPromotionOfferData:adInfo];
        
    }else {
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:adInfo.referralUrl] options:@{} completionHandler:nil];
        SFSafariViewController *svc = [[SFSafariViewController alloc] initWithURL:[NSURL URLWithString:adInfo.referralUrl]];
        svc.delegate = self;
        svc.modalPresentationStyle = UIModalPresentationFullScreen;
        [self presentViewController:svc animated:YES completion:nil];
    }
}

#pragma mark - Set City info object

- (void)getCityInfo:(NSNotification*)Info {
    
    NSDictionary *cityInfoDict = Info.userInfo;
    if (APP_DEBUG_MODE) { NSLog(@"Info is %@",cityInfoDict); }
    NSString *city =  [cityInfoDict valueForKey:@"cityInfo"];
    self.delegate.selectedCityName = city;
    self.cityNameLabel.text = self.delegate.selectedCityName;
    
    if ([city isEqualToString:NSLocalizedString(@"Current Location", @"")]) {
        self.providerStatus = NO;
    }else {
        self.providerStatus = YES;
    }
    
    [self getServiceCategories:NO];

}

#pragma mark - Clear Location Info

- (void)clearLocationInfo:(NSNotification*)Info {
    
    self.delegate.selectedCityName = NSLocalizedString(@"Current Location", @"");
    self.cityNameLabel.text = self.delegate.selectedCityName;
    self.providerStatus = NO;
    self.count = 0;
    [self configureLocationManager];
}


#pragma mark - Set Up Views

- (void)setUpViews {
    
    self.fiterView.hidden = YES;
    [self.serviceCategoryTableView setHidden:YES];
    [iBeautyUtility setViewBorderRound:self.fiterView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getFilterInfo:) name:@"selectedFiltersInfo" object:nil];
    self.cityNameLabel.text = self.selectedCityName;
}


#pragma mark - Filter Services

- (void)getFilterInfo:(NSNotification*)filterInfo {
    
    NSDictionary *dictInfo = filterInfo.userInfo;
    self.filterDict = [dictInfo valueForKey:@"filterInfo"];
    if (APP_DEBUG_MODE)
        NSLog(@"Filter info is %@",self.filterDict);
    
    [self getServiceCategories:YES];

}

- (IBAction)filterServices:(id)sender {
    
    self.filterInfoArray = [NSMutableArray new];
    self.filterStatus = YES;
    FilterViewController *filterController = [self.storyboard instantiateViewControllerWithIdentifier:kFilterViewController];
    filterController.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:filterController animated:YES completion:nil];
    
}

#pragma mark - Set up Filter View

-(void)setUpFilterView {
    
    self.fiterView.hidden = YES;
    self.filterStatus = NO;
    [self.filterDict setValue:[NSNumber numberWithInteger:0] forKey:@"Men"];
    [self.filterDict setValue:[NSNumber numberWithInteger:0] forKey:@"Women"];
    [self.filterDict setValue:[NSNumber numberWithInteger:0] forKey:@"Home"];
    [self.filterDict setValue:[NSNumber numberWithInteger:0] forKey:@"Shop"];
}


#pragma mark - Segment Control

- (void)setUpNamesForSegmentControl {
    
    NSString *allString = NSLocalizedString(@"All", @"");
    NSString *serviceString = NSLocalizedString(@"Women", @"");

    NSString * segmentName = [[NSString stringWithFormat:@"%@ %@",allString,serviceString]uppercaseString];
    [self.segmentControl setTitle:segmentName forSegmentAtIndex:1];

    
}

- (IBAction)segmentControlValueChanged:(UISegmentedControl*)sender {
    
    self.segmentIndex = sender.selectedSegmentIndex;
    [self setUpSegmentIndex];
}

- (void)setUpSegmentIndex {
    
    if (self.segmentIndex == 0) {
        [self noDataAvaialbleView:self.segmentIndex];
        [self setUpTableHeaderView];
        
    }else{
        self.serviceCategoryTableView.tableHeaderView = nil ;
        
        if (self.filterStatus == YES) {
            self.array_ViewAll = [self.filterInfoArray mutableCopy];
        }else {
            self.array_ViewAll = [self.serviceCategoriesArray mutableCopy];
        }
        [self noDataAvaialbleView:self.segmentIndex];
    }
    
    [self.serviceCategoryTableView reloadData];
    
}

- (void)setUpSegmentGradient {
    
    CAGradientLayer *selectedSegGradient = [iBeautyUtility setSegmentGradient:self.segmentControl];
//    CAGradientLayer *unSelectedSegGradient = [iBeautyUtility setSegmentGradient:self.segmentControl];

    NSArray *views = self.segmentControl.subviews;
    
    for (int index =0; index<views.count; index++) {
        UIView *view = views[index];
        if (index == self.segmentControl.selectedSegmentIndex) {
            view.backgroundColor = [UIColor colorWithPatternImage:[iBeautyUtility imageFromLayer:selectedSegGradient]];
            view.tintColor = UIColor.whiteColor;
        }else {
            view.backgroundColor = UIColor.clearColor;
            view.tintColor = [UIColor iBeautyThemeColor];
        }
    }
}

- (void)updateSegmentGradientBackGround {
    
    if (self.segmentIndex == 0) {
        
    }else {
        
    }
    
}


#pragma mark - Set up All Category Table Cell

- (void)noDataAvaialbleView:(NSInteger)type {

    [self.blankScreenView setFrame:CGRectMake(0.0f, 346.0f, self.blankScreenView.frame.size.width, self.blankScreenView.frame.size.height)];
    self.serviceCategoryTableView.translatesAutoresizingMaskIntoConstraints = YES;
    if (type == 0) {
        [self.serviceCategoryTableView setHidden:NO];
        if (self.serviceCategoriesArray.count<1) {
            self.serviceCategoryTableView.frame = CGRectMake(self.serviceCategoryTableView.frame.origin.x, self.serviceCategoryTableView.frame.origin.y,SCREEN_WIDTH, 220.0f);
            [self.view addSubview:self.blankScreenView];
        }else {
            if (IS_IPHONE_X || IS_IPHONE_XS_MAX) {
                self.serviceCategoryTableView.frame = CGRectMake(self.serviceCategoryTableView.frame.origin.x, self.serviceCategoryTableView.frame.origin.y,SCREEN_WIDTH, SCREEN_HEIGHT-self.serviceCategoryTableView.frame.origin.y - 34.0f);
            }else {
                self.serviceCategoryTableView.frame = CGRectMake(self.serviceCategoryTableView.frame.origin.x, self.serviceCategoryTableView.frame.origin.y,SCREEN_WIDTH, SCREEN_HEIGHT-self.serviceCategoryTableView.frame.origin.y);
            }

            [self.blankScreenView removeFromSuperview];
        }
        [self setUpFilterView];
    }else {
        
        if (self.array_ViewAll.count<1) {
            [self.serviceCategoryTableView setHidden:YES];
            [self.view addSubview:self.blankScreenView];
        }else {
            [self.serviceCategoryTableView setHidden:NO];
            [self.blankScreenView removeFromSuperview];
        }
        self.fiterView.hidden = NO;
    }
    
}


#pragma mark - Set up All Category Table Cell

- (void)setUpTableViewForAllCategoryCell {
    
    UINib *cellNib = [UINib nibWithNibName:@"AllMassagesCell" bundle:nil];
    [self.serviceCategoryTableView registerNib:cellNib forCellReuseIdentifier:allCategoryCellIdentifier];
    
}

#pragma mark - Table header view

- (void)setUpTableHeaderView {
    
    CategoryTableHeaderView *tableHeader = [CategoryTableHeaderView headerView:self.promotionImages promotionalInfo:self.promotionAdsInfo];
    [tableHeader.imagePager setDataSource:tableHeader.imagePager.dataSource];
    [tableHeader.imagePager setDelegate:tableHeader.imagePager.delegate];
    self.serviceCategoryTableView.tableHeaderView = tableHeader;
    
}

#pragma mark - Get Promotional Ads Info

- (void)getPromotionalAdsInfo {
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_PROMOTIONAL_ADS];
    
    //NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    
    NSString *urlString = [NSString stringWithFormat:@"%@",url];
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable) {
        
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonGETData:urlString parameters:nil onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                @try {
                    if (completed) {
                        PromotionalAds *instanceObj = [PromotionalAds getSharedInstance];
                        self.promotionAdsInfo = [instanceObj promotionalInfoObj:completed];
                        if (APP_DEBUG_MODE)
                            NSLog(@"Info array is %@", self.promotionAdsInfo);
                        [self getPromotionalImagesArray];
                        [self setUpSegmentIndex];
                    }else {
                        if (APP_DEBUG_MODE)
                            NSLog(@"error is %@", error.localizedDescription);
                    }
                    
                } @catch (NSException *exception) {
                    //[EShopUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
    
}

- (void)getPromotionalImagesArray {
    
    self.promotionImages = [NSMutableArray new];
    
    for (PromotionalAds * infoObj in self.promotionAdsInfo) {
        
        [self.promotionImages addObject:[NSString stringWithFormat:@"%@%@",IMAGE_BASE_URL,infoObj.adImage]];
    }
    
}

#pragma mark - Back Action

- (IBAction)backActionMethod:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark - Get Providers List regarding service

- (void)getServiceCategories:(BOOL)serviceStatus {
    
    NSString *apiUrl;
    
    if (self.providerStatus == NO) {
        apiUrl = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_SERVICE_PROVIDER_CATEGORIES];

    }else {
        apiUrl = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_PROVIDERBY_CITIES];
    }

    NSString *url;
    if (serviceStatus == NO) {
        url = [NSString stringWithFormat:@"%@",apiUrl];
    }else {
        url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_FILTER_PROVIDERS];
    }
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];

    NSString *serviceId =  @"5ca590942a72b"; //self.serviceInfo.serviceID;

    NSString *currentDay =  [[NSString string] getWeekDay];
    
    NSString *latitude = [NSString stringWithFormat:@"%@",[NSNumber numberWithDouble:self.currentLocation.coordinate.latitude]];
    NSString *longitude = [NSString stringWithFormat:@"%@",[NSNumber numberWithDouble:self.currentLocation.coordinate.longitude]];
    
    NSMutableDictionary *dictInfo;
    
    if (self.providerStatus == NO) {
        
        if (serviceStatus == NO) {
            
            dictInfo = [NSMutableDictionary dictionaryWithObjectsAndKeys:accessToken,@"Authorization",serviceId,@"businessCatId",currentDay,@"dayName",latitude,@"Latitude",longitude,@"Longitude", nil];
        }else {
            dictInfo = [self getParametersDictForFilter];
        }

    }else {
        
          if (serviceStatus == NO) {
                dictInfo = [NSMutableDictionary dictionaryWithObjectsAndKeys:accessToken,@"Authorization",serviceId,@"BusinessCatId",currentDay,@"DayName",self.cityNameLabel.text,@"City",latitude,@"CurrentLatitude",longitude,@"CurrentLongitude", nil];
          }else {
              dictInfo = [self getParametersDictForFilter];
          }
    }
    
    if (internetStatus != NotReachable) {
        
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonPostData:url params:dictInfo onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                @try {
                    if (completed) {
                        
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSMutableArray *tempInfoArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE) { NSLog(@"Temp Info Array is %@", tempInfoArray); }
                        
                        if ([tempInfoArray isKindOfClass:[NSMutableArray class]]) {

                            ServicesCategoriesInfo *serviceCatObj = [ServicesCategoriesInfo getSharedInstance];
                            
                            if (self.filterStatus == NO) {
                                
                                self.serviceCategoriesArray = [serviceCatObj serviceProvidersListInfoObject:tempInfoArray];
                                if (APP_DEBUG_MODE) { NSLog(@"Service Provider Array is %@", self.serviceCategoriesArray);}
                                [self filterArrayByDistanceAndTopRated:self.serviceCategoriesArray];
                                
                            }else {
                                self.filterInfoArray = [serviceCatObj serviceProvidersListInfoObject:tempInfoArray];
                                if (APP_DEBUG_MODE) { NSLog(@"Filter Array is %@", self.filterInfoArray);}
                            }
                        }else {
                            if (self.filterStatus == NO) {
                                [self.serviceCategoriesArray removeAllObjects];
                            }else {
                                [self.filterInfoArray removeAllObjects];
                            }
                        }
                        [self getPromotionalAdsInfo];
                        [self setUpSegmentIndex];
                    }else {
                        if (APP_DEBUG_MODE) { NSLog(@"error is %@", error.localizedDescription);}
                        [SVProgressHUD dismiss];
                    }
                    
                } @catch (NSException *exception) {
                    //[EShopUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                    [SVProgressHUD dismiss];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}

- (NSMutableDictionary*)getParametersDictForFilter {
    
    NSMutableDictionary *infoDict = [[NSMutableDictionary alloc]init];
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    NSString *serviceId =  self.serviceInfo.serviceID;
    NSString *currentDay =  [[NSString string] getWeekDay];
    NSString *latitude = [NSString stringWithFormat:@"%@",[NSNumber numberWithDouble:self.currentLocation.coordinate.latitude]];
    NSString *longitude = [NSString stringWithFormat:@"%@",[NSNumber numberWithDouble:self.currentLocation.coordinate.longitude]];
    NSString * cityName = @"";
    
    if (![self.cityNameLabel.text isEqualToString:NSLocalizedString(@"Current Location", @"")]) {
        cityName = self.cityNameLabel.text;
    }
    
    NSMutableArray *serviceProvideAt = [NSMutableArray new];
    
    if ([[self.filterDict valueForKey:@"Home"]integerValue] > 0) {
        NSMutableDictionary *homeDict = [NSMutableDictionary new];
        [homeDict setValue:@"Home" forKey:@"Name"];
        [serviceProvideAt addObject:homeDict];
    }
    
    if ([[self.filterDict valueForKey:@"Shop"]integerValue] > 0) {
        NSMutableDictionary *shopDict = [NSMutableDictionary new];
        [shopDict setValue:@"Shop" forKey:@"Name"];
        [serviceProvideAt addObject:shopDict];
    }
    
    NSMutableArray *serviceProvideFor = [NSMutableArray new];
    
    if ([[self.filterDict valueForKey:@"Men"]integerValue] > 0) {
        NSMutableDictionary *menDict = [NSMutableDictionary new];
        [menDict setValue:@"Men" forKey:@"Name"];
        [serviceProvideFor addObject:menDict];
    }
    if ([[self.filterDict valueForKey:@"Women"]integerValue] > 0) {
        NSMutableDictionary *womenDict = [NSMutableDictionary new];
        [womenDict setValue:@"Women" forKey:@"Name"];
        [serviceProvideFor addObject:womenDict];
    }
    if ([[self.filterDict valueForKey:@"Unisex"]integerValue] > 0) {
        NSMutableDictionary *unisexDict = [NSMutableDictionary new];
        [unisexDict setValue:@"Unisex" forKey:@"Name"];
        [serviceProvideFor addObject:unisexDict];
    }
    
    [infoDict setValue:accessToken forKey:@"Authorization"];
    [infoDict setValue:serviceId forKey:@"BusinessCatId"];
    [infoDict setValue:currentDay forKey:@"DayName"];
    [infoDict setValue:cityName forKey:@"City"];
    [infoDict setValue:latitude forKey:@"CurrentLatitude"];
    [infoDict setValue:longitude forKey:@"CurrentLongitude"];
    
    [infoDict setObject:serviceProvideAt forKey:@"ServiceProvideAt"];
    [infoDict setObject:serviceProvideFor forKey:@"ServiceProvideFor"];

    return infoDict;
}


#pragma mark - Get Filter Url String


- (NSString *)getFilterUrlString:(NSString *)urlString {
    
    NSString *filterUrl;
    
    NSInteger menFilter = [[self.filterDict valueForKey:@"Men"]integerValue];
    NSInteger womenFilter = [[self.filterDict valueForKey:@"Women"]integerValue];
    NSInteger homeFilter = [[self.filterDict valueForKey:@"Home"]integerValue];
    NSInteger shopFilter = [[self.filterDict valueForKey:@"Shop"]integerValue];

    if (menFilter > 0 && womenFilter > 0 && homeFilter > 0 && shopFilter > 0) {
        
        filterUrl = [NSString stringWithFormat:@"%@&servicefor=%ld&servicefor=%ld&serviceat=%ld&serviceat=%ld",urlString,(long)menFilter,(long)womenFilter,(long)homeFilter,(long)shopFilter];

    }else if (menFilter > 0 && womenFilter > 0 && homeFilter > 0) {
        
        filterUrl = [NSString stringWithFormat:@"%@&servicefor=%ld&servicefor=%ld&serviceat=%ld",urlString,(long)menFilter,(long)womenFilter,(long)homeFilter];

    }else if (womenFilter > 0 && homeFilter > 0 && shopFilter > 0) {
        
        filterUrl = [NSString stringWithFormat:@"%@&servicefor=%ld&serviceat=%ld&serviceat=%ld",urlString,(long)womenFilter,(long)homeFilter,(long)shopFilter];
        
    }else if (homeFilter > 0 && shopFilter > 0 && menFilter > 0) {
        
        filterUrl = [NSString stringWithFormat:@"%@&serviceat=%ld&serviceat=%ld&servicefor=%ld",urlString,(long)homeFilter,(long)shopFilter,(long)menFilter];

    }else if (shopFilter > 0 && menFilter > 0 && womenFilter > 0 ) {
        
        filterUrl = [NSString stringWithFormat:@"%@&serviceat=%ld&servicefor=%ld&servicefor=%ld",urlString,(long)shopFilter,(long)menFilter,(long)womenFilter];
        
    }else if (menFilter > 0 && womenFilter > 0 ) {
        
        filterUrl = [NSString stringWithFormat:@"%@&servicefor=%ld&servicefor=%ld",urlString,(long)menFilter,(long)womenFilter];

    }else if (menFilter > 0 && homeFilter > 0 ) {
        
        filterUrl = [NSString stringWithFormat:@"%@&servicefor=%ld&serviceat=%ld",urlString,(long)menFilter,(long)homeFilter];

    }else if (menFilter > 0 && shopFilter > 0 ) {
        
        filterUrl = [NSString stringWithFormat:@"%@&servicefor=%ld&serviceat=%ld",urlString,(long)menFilter,(long)shopFilter];

    }else if (womenFilter > 0 && homeFilter > 0 ) {
        
        filterUrl = [NSString stringWithFormat:@"%@&servicefor=%ld&serviceat=%ld",urlString,(long)womenFilter,(long)homeFilter];

    }else if (womenFilter > 0 && shopFilter > 0 ) {
        
        filterUrl = [NSString stringWithFormat:@"%@&servicefor=%ld&serviceat=%ld",urlString,(long)womenFilter,(long)shopFilter];

    }else if (homeFilter > 0 && shopFilter > 0 ) {
        
        filterUrl = [NSString stringWithFormat:@"%@&serviceat=%ld&serviceat=%ld",urlString,(long)homeFilter,(long)shopFilter];

    }else if (menFilter > 0 ) {
        
        filterUrl = [NSString stringWithFormat:@"%@&servicefor=%ld",urlString,(long)menFilter];

    }else if (womenFilter > 0 ) {
        
        filterUrl = [NSString stringWithFormat:@"%@&servicefor=%ld",urlString,(long)womenFilter];

    }else if (homeFilter > 0 ) {
        
        filterUrl = [NSString stringWithFormat:@"%@&serviceat=%ld",urlString,(long)homeFilter];

    }else if (shopFilter > 0 ) {
        
        filterUrl = [NSString stringWithFormat:@"%@&serviceat=%ld",urlString,(long)shopFilter];

    }else {
        filterUrl = [NSString stringWithFormat:@"%@",urlString];
    }
    
    return filterUrl;
    
}

#pragma mark - Filter Array By DistanceAndTopRated

- (void)filterArrayByDistanceAndTopRated:(NSMutableArray*)categoryArray {
    
    self.array_AroundYou = [NSMutableArray new];
    self.array_TopRated = [NSMutableArray new];

    NSSortDescriptor *sortByDistance = [NSSortDescriptor sortDescriptorWithKey:@"providerDistance" ascending:YES];
    NSArray *distanceArray = [categoryArray sortedArrayUsingDescriptors:@[sortByDistance]];
    [self.array_AroundYou addObjectsFromArray:distanceArray];

    NSSortDescriptor *sortByRate = [NSSortDescriptor sortDescriptorWithKey:@"rating" ascending:NO];
    NSArray *sortedArray = [categoryArray sortedArrayUsingDescriptors:@[sortByRate]];
    [self.array_TopRated addObjectsFromArray:sortedArray];

}

#pragma mark - UITableViewDataSouce

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if (self.segmentIndex == 0) {
        
        return 2;

    }else {
        return 1;

    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (self.segmentIndex == 0) {
        
        if (section == 0) {
            
            return 1;
            
        }else {
            
            return 1;
        }
        
    }else {
        if (self.filterStatus == NO) {
            return self.serviceCategoriesArray.count;
        }else {
            return self.array_ViewAll.count;
        }
    }

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell;
    
    if (self.segmentIndex == 0) {
        
        RecommendedCategoryCell *cell = [tableView dequeueReusableCellWithIdentifier:recommendedCellIdentifier];
        
        if (!cell){
            cell = [[RecommendedCategoryCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:recommendedCellIdentifier];
        }
        
        cell.delegateObj = self;
        cell.collectionView_Categories.tag = indexPath.section;
        if(indexPath.section == 0) {
            
            [cell configureCellWithInfo:self.array_AroundYou];
            
        }else {
            [cell configureCellWithInfo:self.array_TopRated];
        }
        return cell;

    }else {
        
        AllMassagesCell *allCatCell = [tableView dequeueReusableCellWithIdentifier:allCategoryCellIdentifier];
        
        if (!allCatCell){
            allCatCell = [[AllMassagesCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:allCategoryCellIdentifier];
        }
        allCatCell.selectionStyle = UITableViewCellSelectionStyleNone;
        ServicesCategoriesInfo *infoObj = self.array_ViewAll[indexPath.row];
        [allCatCell configureAllCategoryCell:infoObj];
        return allCatCell;
    }
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.segmentIndex == 0) {
        
        if (self.serviceCategoriesArray.count<1) {
            return 0.0f;
        }else {
            
            NSString *deviceType = [UIDevice currentDevice].model;
            if ([deviceType isEqualToString:@"iPhone"]) {
                return 271.0f;
            }else {
                return 370.0f;
            }
        }

    }else {
        if (self.array_ViewAll.count<1) {
            return 0.0f;

        }else {
            return 140.0f;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if (self.segmentIndex == 0) {
        
        if (self.serviceCategoriesArray.count<1) {
            
            return 00.f;

        }else {
            return 50.f;
        }
    }else {
        return 00.f;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.serviceCategoryTableView deselectRowAtIndexPath:indexPath animated:NO];

    if (self.segmentIndex == 1) {
        CategoryDetailController *detailController = [self.storyboard instantiateViewControllerWithIdentifier:kCategoryDetailController];
        ServicesCategoriesInfo *infoObj = self.array_ViewAll[indexPath.row];
        detailController.categoryInfoObj = infoObj;
        detailController.currentLocation = self.currentLocation;
        [self.navigationController pushViewController:detailController animated:YES];
    }

}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 50.0f)];
    UILabel *tempLabel= [[UILabel alloc]initWithFrame:CGRectMake(15,10,tableView.frame.size.width-30,30)];
    tempLabel.textColor = [UIColor blackColor];
    tempLabel.font = [UIFont fontWithName:@"SegoeUI" size:18.0];
    
    UIButton *viewAllButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [viewAllButton addTarget:self action:@selector(viewAll:) forControlEvents:UIControlEventTouchUpInside];
    [viewAllButton setTitle:NSLocalizedString(@"View All", @"") forState:UIControlStateNormal];
    
    NSString *languageCode =   [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];
    
    if ([languageCode isEqualToString:@"en"]) {
        
        viewAllButton.frame = CGRectMake(tableView.frame.size.width-105.0f, 10.0, 90.0, 30.0);

    }else if ([languageCode isEqualToString:@"ar"]) {
        
        viewAllButton.frame = CGRectMake(15.0f, 10.0, 90.0, 30.0);
    }
    viewAllButton.tag = section;
    viewAllButton.titleLabel.textColor = [UIColor whiteColor];
    viewAllButton.titleLabel.font = [UIFont fontWithName:@"SegoeUI" size:13.0];

    [headerView addSubview:viewAllButton];
    
    //[viewAllButton.layer insertSublayer:[iBeautyUtility setGradientColor_Buttons:viewAllButton] atIndex:0];
    
    [iBeautyUtility setButtonCorderRound:viewAllButton];

    if (section == 0) {
        tempLabel.text = NSLocalizedString(@"Around You", @"");
    }else{
        tempLabel.text = NSLocalizedString(@"Top Rated", @"");
    }
    headerView.backgroundColor = [UIColor whiteColor];
    
    [headerView addSubview:tempLabel];
    return headerView;
}


- (void)viewAll:(UIButton*)sender {
    
    self.serviceCategoryTableView.tableHeaderView = nil ;

    self.segmentControl.selectedSegmentIndex = 1;
    self.segmentIndex = 1;
    if (sender.tag == 0) {
        self.array_ViewAll = [self.array_AroundYou mutableCopy];
        
    }else  {
        self.array_ViewAll = [self.array_TopRated mutableCopy];
    }
//  [self setUpFilterView];
    self.fiterView.hidden = NO;
    [self.serviceCategoryTableView reloadData];
    
}

#pragma mark - CustomCell Delegate Method

- (void)delMethod_didSelectCollection:(NSInteger)rowSelected section:(NSInteger)sectionIndex {
    
    if (APP_DEBUG_MODE)
        NSLog(@"CollectionView row Selected : %ld in section : %ld ",(long)rowSelected, (long)sectionIndex);
    
    CategoryDetailController *detailController = [self.storyboard instantiateViewControllerWithIdentifier:kCategoryDetailController];
    ServicesCategoriesInfo *infoObj;
    if (sectionIndex == 0) {
        infoObj = self.array_AroundYou[rowSelected];
    }else {
        infoObj = self.array_TopRated[rowSelected];
    }
    detailController.categoryInfoObj = infoObj;
    detailController.currentLocation = self.currentLocation;
    [self.navigationController pushViewController:detailController animated:YES];

}

#pragma mark - Safari view controller delegate

- (void)safariViewControllerDidFinish:(SFSafariViewController *)controller {
    [self dismissViewControllerAnimated:true completion:nil];
}


#pragma mark - get Promotion Offer Information

- (void)getPromotionOfferData:(PromotionalAds*)adInfo {
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_PROMOTIONAL_OFFER];
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    
    NSDictionary *dictInfo = [NSDictionary dictionaryWithObjectsAndKeys:accessToken,@"Authorization",adInfo.businessID,@"BusinessId",adInfo.offerId,@"OfferId",nil];
    
    if (internetStatus != NotReachable) {
        
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonPostData:url params:dictInfo onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                
                @try {
                    if (completed) {
                        
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSMutableArray *infoArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE) { NSLog(@"Info Array is %@", infoArray); }
                        
                        NSMutableArray *offersArray;
                        OffersDetail *offersInfoDetail  = [OffersDetail getSharedInstance];
                        OffersDetailViewController *offerDetailController = [self.storyboard instantiateViewControllerWithIdentifier:kOfferDetailController];
                        if (infoArray.count>0) {
                            NSDictionary *dictInfo = infoArray[0];
                            if ([[dictInfo valueForKey:OfferFrom] isEqualToString:ServiceProvider]) {
                                offersArray = [offersInfoDetail offersDetailInfoObj_promotionalOffers:infoArray];
                            }else {
                                offersArray = [offersInfoDetail offersDetailInfoObjFor_Products_SingleSeller:infoArray];
                            }
                        }
                        
                        if (APP_DEBUG_MODE) { NSLog(@"Offers Array ->>> %@", offersArray);}
                        
                        if (offersArray.count>0) {
                            offerDetailController.offerInfo = offersArray[0];
                            offerDetailController.promotionOfferInfo = adInfo;
                            offerDetailController.offerTypeStatus = YES;
                            [self.navigationController pushViewController:offerDetailController animated:YES];
                        }
                        
                    }else {
                        if (APP_DEBUG_MODE) { NSLog(@"error is %@", error.localizedDescription);}
                    }
                    
                } @catch (NSException *exception) {
                    //[iBeautyUtility showAlertMessage:NSLocalizedString(@"No Response", @"")];
                } @finally { }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
