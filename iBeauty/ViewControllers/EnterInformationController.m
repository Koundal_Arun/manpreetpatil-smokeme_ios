//
//  EnterInformationController.m
//  iBeauty
//
//  Created by App Innovation on 27/09/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "EnterInformationController.h"
#import "Constants.h"

@interface EnterInformationController ()<UITextFieldDelegate>

@property (nonatomic,weak) IBOutlet UITextField *emailTextField;
@property (nonatomic,weak) IBOutlet UIButton *continueButton;
@property (nonatomic,weak) IBOutlet UIView *emailView;
@property (nonatomic,weak) IBOutlet UIView *IBCircleView;

@property UIToolbar *toolBar;
@property (nonatomic, strong) UIBarButtonItem   *doneButton;

@end

@implementation EnterInformationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setUpTextFieldData];
    [iBeautyUtility setButtonCorderRound:self.continueButton];
    [iBeautyUtility setViewBorderRound:self.emailView];
    [iBeautyUtility setViewBorderRound:self.IBCircleView];
    [self setupToolBarOnTextfields];
    
    //[self.continueButton.layer addSublayer:[iBeautyUtility setGradientColor_Buttons:self.continueButton]];
    [self setUpButtonLayer];
    [self setStatusBarColor];
}

- (void)setUpTextFieldData {
    
    if (self.emailText.length>0) {
        self.emailTextField.text = self.emailText;
    }
    
}

- (void)setUpButtonLayer {
    
    CGRect buttonFrame = self.continueButton.frame;
    buttonFrame.size.width = [UIScreen mainScreen].bounds.size.width-40.0f;
    self.continueButton.frame = buttonFrame;
    //[self.continueButton.layer insertSublayer:[iBeautyUtility setGradientColor_Buttons:self.continueButton] atIndex:0];
}

#pragma mark - Set Status Bar Color

- (void)setStatusBarColor {

    if (@available(iOS 13.0, *)) {
        
        UIView *statusBar = [[UIView alloc]initWithFrame:[UIApplication sharedApplication].keyWindow.windowScene.statusBarManager.statusBarFrame] ;
        [self.view addSubview:statusBar];
        if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
            
            statusBar.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:130/255.0 blue:90/255.0 alpha:1.0];
            //[statusBar.layer insertSublayer:[self setGradientColor_Views:statusBar] atIndex:0];
        }
    }else {
            UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
            if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
                
                statusBar.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:130/255.0 blue:90/255.0 alpha:1.0];
               //[statusBar.layer insertSublayer:[self setGradientColor_Views:statusBar] atIndex:0];
            }
    }

}


- (CAGradientLayer*)setGradientColor_Views:(UIView*)sender {
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = sender.bounds;
    gradient.startPoint = CGPointMake(0, 0);
    gradient.endPoint = CGPointMake(1, 0);
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:233.0/255.0 green:83/255.0 blue:158/255.0 alpha:1.0] CGColor],(id)[[UIColor colorWithRed:240/255.0 green:59.0/255.0 blue:53.0/255.0 alpha:1.0] CGColor], nil];
    
    return gradient;
}

#pragma mark - Continue Button

- (IBAction)continueButton:(id)sender {
    
    if (self.emailTextField.text.length>0) {
        
        NSDictionary *emailInfoDict  = [NSDictionary dictionaryWithObjectsAndKeys:self.emailTextField.text,KEmail,nil];
        if (self.status == NO) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"enterEmailViaFBGoogleLogin" object:nil userInfo:emailInfoDict];

        }else {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"enterEmailFromRegisterViaFBGoogleLogin" object:nil userInfo:emailInfoDict];
        }
        [self dismissViewControllerAnimated:YES completion:nil];
    }else {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"Please enter email address", @"")];
    }
}

#pragma mark - set up Tool Bar On Text fields

- (void)setupToolBarOnTextfields {
    
    self.toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50)];
    self.toolBar.barStyle = UIBarStyleDefault;
    [self.toolBar sizeToFit];
    [self.toolBar setBackgroundColor:[UIColor lightGrayColor]];
    [self buttonWithinToolbar];
    
    self.emailTextField.inputAccessoryView = self.toolBar;

}

- (void)buttonWithinToolbar {
    
    self.doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", @"") style:UIBarButtonItemStyleDone target:self action:@selector(doneAction:)];
    
    UIBarButtonItem *flexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [self.toolBar setItems:@[flexibleItem,self.doneButton] animated:NO];
}

- (void)doneAction:(id)sender {
    
    [self.emailTextField resignFirstResponder];
    
}

# pragma mark - Textfield Delegate Methods

-(BOOL) textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
