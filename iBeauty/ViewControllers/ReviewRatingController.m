//
//  ReviewRatingController.m
//  iBeauty
//
//  Created by App Innovation on 14/11/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "ReviewRatingController.h"
#import <HCSStarRatingView/HCSStarRatingView.h>
#import "UIColor+iBeauty.h"
#import "Constants.h"

@interface ReviewRatingController ()<UITextViewDelegate>

@property (nonatomic, weak) IBOutlet UIButton *submitButton;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *leaveYourComment;
@property (nonatomic, weak) IBOutlet UITextView *commentTextView;
@property (nonatomic, strong) NSString *ratingString;
@property (nonatomic, weak) IBOutlet UILabel *placeHolderLabel;
@property (nonatomic, weak) IBOutlet UINavigationItem *navigationTitle;
@property (nonatomic,weak)  IBOutlet UINavigationBar *navBar;

@end

@implementation ReviewRatingController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setUpStarRatingView];
    self.submitButton.frame = [iBeautyUtility setUpButtonFrame:self.submitButton.frame];
    [iBeautyUtility setButtonCorderRound:self.submitButton];
    
    //[self.submitButton.layer insertSublayer:[iBeautyUtility setGradientColor_Buttons:self.submitButton] atIndex:0];

    [self setUpLocalizationsViews];
    //[self setGradient];
    [self setStatusBarColor];

}

#pragma mark - Set Gradient

- (void)setGradient {
    
    self.navBar.frame = [iBeautyUtility setUpFrame:self.navBar.frame];
    CAGradientLayer *gradient = [iBeautyUtility setNavigationGradient:self.navBar];
    [self.navBar setBackgroundImage:[iBeautyUtility imageFromLayer:gradient] forBarMetrics:UIBarMetricsDefault];
}

#pragma mark - Set Status Bar Color

- (void)setStatusBarColor {

    if (@available(iOS 13.0, *)) {
        
        UIView *statusBar = [[UIView alloc]initWithFrame:[UIApplication sharedApplication].keyWindow.windowScene.statusBarManager.statusBarFrame] ;
        [self.view addSubview:statusBar];
        if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
            
            statusBar.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:130/255.0 blue:90/255.0 alpha:1.0];
            //[statusBar.layer insertSublayer:[self setGradientColor_Views:statusBar] atIndex:0];
        }
    }else {
            UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
            if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
                
                statusBar.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:130/255.0 blue:90/255.0 alpha:1.0];
                //[statusBar.layer insertSublayer:[self setGradientColor_Views:statusBar] atIndex:0];
            }
    }

}

- (CAGradientLayer*)setGradientColor_Views:(UIView*)sender {
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = sender.bounds;
    gradient.startPoint = CGPointMake(0, 0);
    gradient.endPoint = CGPointMake(1, 0);
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:233.0/255.0 green:83/255.0 blue:158/255.0 alpha:1.0] CGColor],(id)[[UIColor colorWithRed:240/255.0 green:59.0/255.0 blue:53.0/255.0 alpha:1.0] CGColor], nil];
    
    return gradient;
}

#pragma mark - Set Up Localizations Views

- (void)setUpLocalizationsViews {
    
    [self.submitButton setTitle:NSLocalizedString(@"Submit", @"") forState:UIControlStateNormal];
    self.titleLabel.text = NSLocalizedString(@"How would you rate the service?", @"");
    self.leaveYourComment.text = NSLocalizedString(@"Leave your comments", @"");
    self.placeHolderLabel.text = NSLocalizedString(@"Write a review", @"");
    self.navigationTitle.title = NSLocalizedString(@"Give Feedback", @"");

}

- (void)setUpStarRatingView {
    
    HCSStarRatingView *starRatingView = [HCSStarRatingView new];
    starRatingView.userInteractionEnabled = TRUE;
    starRatingView.maximumValue = 5;
    starRatingView.minimumValue = 0;
    starRatingView.value = 0;
    starRatingView.tintColor = [UIColor iBeautyThemeColor];
    starRatingView.allowsHalfStars = YES;
    starRatingView.accurateHalfStars = YES;
    starRatingView.emptyStarImage = [[UIImage imageNamed:@"star_grey"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    starRatingView.filledStarImage = [[UIImage imageNamed:@"star"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [starRatingView addTarget:self action:@selector(didChangeValue:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:starRatingView];
    
    
}

#pragma mark - Back Action

- (IBAction)backActionMethod:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];

}

- (IBAction)didChangeValue:(HCSStarRatingView *)sender {
    
    self.ratingString = [NSString stringWithFormat:@"%.1f",sender.value];
    if (APP_DEBUG_MODE) { NSLog(@"Rating value is %@", self.ratingString);}

}

#pragma mark - Submit Action

- (IBAction)submitActionMethod:(id)sender {
    
    if (self.ratingString.length>0) {
        [self giveRatingAPI];
    }else {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"Please select rating", @"")];
    }
}

#pragma mark - Get Service Provider Detail Info

- (void)giveRatingAPI {
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_RATING];
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    NSDictionary *profileInfo = [[NSUserDefaults standardUserDefaults]valueForKey:KLoginInfo];
    NSString *userID = [profileInfo valueForKey:@"UserID"];
    NSString *businessID = [NSString stringWithFormat:@"%@",self.appointmentInfo.providerId];
    
    NSDictionary *dictInfo = [NSDictionary dictionaryWithObjectsAndKeys:accessToken,@"Authorization",self.appointmentInfo.orderId,@"OrderId",userID,@"CustomerId",businessID,@"BusinessId",@"service",@"OrderType",self.ratingString,@"Rating",self.commentTextView.text,@"Message",nil];
    
    if (internetStatus != NotReachable) {
        
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonPostData:url params:dictInfo onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                @try {
                    if (completed) {
                        
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSDictionary *tempInfoDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE)
                            NSLog(@"Info Dict is %@", tempInfoDict);
                        if ([tempInfoDict valueForKey:@"OrderId"]) {
                            [self dismissViewControllerAnimated:YES completion:nil];
                        }
                    }else {
                        if (APP_DEBUG_MODE)
                            NSLog(@"error is %@", error.localizedDescription);
                        [SVProgressHUD dismiss];
                    }
                    
                } @catch (NSException *exception) {
                    //[EShopUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                    [SVProgressHUD dismiss];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}

#pragma mark - Text view delegates

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    
    self.commentTextView.text = @"";
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    
    if (![self.commentTextView hasText]) {
        self.placeHolderLabel.hidden = NO;
    }
}

- (void) textViewDidChange:(UITextView *)textView
{
    if(![self.commentTextView hasText]) {
        self.placeHolderLabel.hidden = NO;
    }
    else{
        self.placeHolderLabel.hidden = YES;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
