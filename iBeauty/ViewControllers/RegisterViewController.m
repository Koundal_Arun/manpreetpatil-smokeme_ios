//
//  RegisterViewController.m
//  iBeauty
//
//  Created by App Innovation on 27/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "RegisterViewController.h"
#import "Constants.h"
#import "EnterInformationController.h"
#import "AppDelegate.h"

@interface RegisterViewController()<UITextFieldDelegate>

@property (nonatomic,weak) IBOutlet UIView *userNameView;
@property (nonatomic,weak) IBOutlet UIView *phNumberView;
@property (nonatomic,weak) IBOutlet UIView *emailView;
@property (nonatomic,weak) IBOutlet UIView *passwordView;

@property (nonatomic,weak) IBOutlet UITextField *userNameTextField;
@property (nonatomic,weak) IBOutlet UITextField *phNumberTextField;
@property (nonatomic,weak) IBOutlet UITextField *emailTextField;
@property (nonatomic,weak) IBOutlet UITextField *passwordTextField;

@property (nonatomic,weak) IBOutlet UIButton *loginButton;

@property (nonatomic,weak) IBOutlet UIView *facebookView;
@property (nonatomic,weak) IBOutlet UIView *googleView;
@property (nonatomic,weak) IBOutlet UIButton *faceBookButton;
@property (nonatomic,weak) IBOutlet UIButton *googleButton;
@property (nonatomic,weak) IBOutlet UIButton *registerButton;

@property (nonatomic,strong) NSString *loginType;
@property (nonatomic,strong) NSDictionary *profileInfoDict;

@property UIToolbar *toolBar;
@property (nonatomic, strong) UIBarButtonItem   *doneButton;
@property (nonatomic,weak) IBOutlet UIScrollView *scrollView;
@property (nonatomic,weak) IBOutlet UIView *containerView;

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    
    //[GIDSignIn sharedInstance].delegate = self;
    
    [GIDSignIn sharedInstance].presentingViewController = self;
    // Automatically sign in the user.
    [[GIDSignIn sharedInstance] restorePreviousSignIn];
    
    [self showGoogleNotificationListener];
    [self setUpViews];
    [self addGetEmailTextOberver];
    [self setupToolBarOnTextfields];
    
}

#pragma mark - set Up views

- (void)setUpViews {
    
    [iBeautyUtility setViewBorderRound:self.userNameView];
    [iBeautyUtility setViewBorderRound:self.phNumberView];
    [iBeautyUtility setViewBorderRound:self.emailView];
    [iBeautyUtility setViewBorderRound:self.passwordView];
    [iBeautyUtility setViewBorderRound:self.facebookView];
    [iBeautyUtility setViewBorderRound:self.googleView];
    [iBeautyUtility setButtonCorderRound:self.registerButton];
    [self setUpTextFieldsCursorAccordingToLanguage];
    [self setLoginButtonAttributedTitle];
    
    CGRect buttonFrame = self.registerButton.frame;
    buttonFrame.size.width = [UIScreen mainScreen].bounds.size.width-40.0f;
    self.registerButton.frame = buttonFrame;
    //[self.registerButton.layer insertSublayer:[iBeautyUtility setGradientColor_Buttons:self.registerButton] atIndex:0];
}


- (void)setLoginButtonAttributedTitle {
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithAttributedString:[self.loginButton attributedTitleForState:UIControlStateNormal]];
    [attributedString replaceCharactersInRange:NSMakeRange(0, attributedString.length) withString:NSLocalizedString(@"LOGIN",@"")];
    [self.loginButton setAttributedTitle:attributedString forState:UIControlStateNormal];
}

- (void)setUpTextFieldsCursorAccordingToLanguage {
    
    NSString *languageCode =   [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];
    
    if ([languageCode isEqualToString:@"en"]) {

        self.userNameTextField.textAlignment = NSTextAlignmentLeft;
        self.phNumberTextField.textAlignment = NSTextAlignmentLeft;
        self.emailTextField.textAlignment = NSTextAlignmentLeft;
        self.passwordTextField.textAlignment = NSTextAlignmentLeft;
        
    }else if ([languageCode isEqualToString:@"ar"]) {

        self.userNameTextField.textAlignment = NSTextAlignmentRight;
        self.phNumberTextField.textAlignment = NSTextAlignmentRight;
        self.emailTextField.textAlignment = NSTextAlignmentRight;
        self.passwordTextField.textAlignment = NSTextAlignmentRight;
    }
}


- (IBAction)goToLoginView:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark - Register User

- (IBAction)registerUsersAccount:(id)sender {
    
    BOOL validStatus =  [self checkValidationForuser_RegisterAccount];
    if (validStatus == YES) {
        self.loginType = @"app";
        [self registerAccountMethod:^(id result, NSError *error){
            
            NSDictionary *dataDict = [result valueForKey:@"data"];

            if ([dataDict isKindOfClass:[NSDictionary class]]) {
                [self setUpUserInfo:dataDict];
            }
            NSString *existanceStr = [result valueForKey:@"message"];//["User already exist"];
                
            if ([existanceStr isEqualToString:@"User already exist"]) {
                [iBeautyUtility showAlertMessage:[result valueForKey:@"message"]];
            }else {
                [iBeautyUtility showAlertMessage:NSLocalizedString(@"Registered successfully. Please login to proceed", @"")];
                [self.navigationController popViewControllerAnimated:YES];
            }
        }];
    }else {
        return;
    }
}

#pragma mark - Set Up User Info

-(void)setUpUserInfo:(NSDictionary*)userInfoDict {
    
    NSDictionary *userProfileInfo = [self getUserProfileInfoByInAppRegister:userInfoDict];
    
    [self saveUserInfo:userProfileInfo];
}

- (NSDictionary*)getUserProfileInfoByInAppRegister:(NSDictionary*)userInfo {
    
    NSString *loginType = [NSString stringWithFormat:@"%@",[iBeautyUtility trimString:[userInfo valueForKey:@"AccountType"]]];
    NSString *birthday  = [NSString stringWithFormat:@"%@",[iBeautyUtility trimString:[userInfo valueForKey:@"Birthday"]]];
    NSString *createdOn  = [NSString stringWithFormat:@"%@",[iBeautyUtility trimString:[userInfo valueForKey:@"CreatedOn"]]];
    NSString *email = [NSString stringWithFormat:@"%@",[iBeautyUtility trimString:[userInfo valueForKey:@"Email"]]];
    NSString *fbUserId = [NSString stringWithFormat:@"%@",[iBeautyUtility trimString:[userInfo valueForKey:@"FGUserId"]]];
    NSString *imageURLString = [NSString stringWithFormat:@"%@",[iBeautyUtility trimString:[userInfo valueForKey:@"ImageUrl"]]];
    NSString *fullName = [NSString stringWithFormat:@"%@",[iBeautyUtility trimString:[userInfo valueForKey:@"Name"]]];
    NSString *password = [NSString stringWithFormat:@"%@",[iBeautyUtility trimString:[userInfo valueForKey:@"Password"]]];
    NSString *phoneNo = [NSString stringWithFormat:@"%@",[iBeautyUtility trimString:[userInfo valueForKey:@"PhoneNumber"]]];
    NSString *userID = [NSString stringWithFormat:@"%@",[iBeautyUtility trimString:[userInfo valueForKey:@"UserId"]]];
    NSString *userName = [NSString stringWithFormat:@"%@",[iBeautyUtility trimString:[userInfo valueForKey:@"Username"]]];

    NSDictionary *parametersDict   = [NSDictionary dictionaryWithObjectsAndKeys:loginType,KLoginType,birthday,KBirthday,createdOn,KCreatedOn,email,KEmail,fbUserId,KFBUserId,imageURLString,KProfileImageUrl,fullName,KProfileName,password,KPassword,phoneNo,KPhoneNumber,userID,KUserId,userName,KUserName,nil];
    
    return parametersDict;
}

#pragma mark - Save Users Info

-(void)saveUserInfo:(NSDictionary*)profileInfo {
    
    [[NSUserDefaults standardUserDefaults]setObject:profileInfo forKey:KLoginInfo];
    [[NSUserDefaults standardUserDefaults]setValue:@"Success" forKey:KLoginStatus];
    [[NSUserDefaults standardUserDefaults]setValue:[profileInfo valueForKey:KUserId] forKey:KUserId];
}


#pragma mark - Register Account Method

-(void)registerAccountMethod:(void(^)(id result, NSError *error))completion {
    
   // {"name":"","username":"","email":"","phone":"","password":"","birthday":"","accounttype":"","imageurl":"","fguserid":""}
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_REGISTER];
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    
    if (APP_DEBUG_MODE) { NSLog(@"accessToken is %@",accessToken); }
    
    NSDictionary *parametersDict  = [NSDictionary dictionaryWithObjectsAndKeys:accessToken,@"Authorization",self.userNameTextField.text,@"name",self.userNameTextField.text,@"username",self.phNumberTextField.text,@"phone",self.emailTextField.text,@"email",self.passwordTextField.text,@"password",@"",@"birthday",self.loginType,@"accounttype",@"",@"imageurl",@"",@"fguserid",nil];
    
    if (APP_DEBUG_MODE)
        NSLog(@"parametersDict is %@",parametersDict);
    
    if (internetStatus != NotReachable) {
        
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonPostData:url params:parametersDict onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                
                @try {
                    if (completed) {
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSDictionary *userInfoDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE)
                            NSLog(@"User info is %@", userInfoDict);
                             completion (userInfoDict,nil);
                    }else {
                        if (APP_DEBUG_MODE)
                            NSLog(@"error is %@", error.localizedDescription);
                        [SVProgressHUD dismiss];
                    }
                    
                } @catch (NSException *exception) {
                    //[iBeautyUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}


#pragma mark - check Validations For Register

- (BOOL)checkValidationForuser_RegisterAccount {
    
    if (self.userNameTextField.text.length < 1) {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"Please enter user name", @"")];
        return NO ;
    }
    if (self.phNumberTextField.text.length < 10) {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"Please enter phone number", @"")];
        return NO ;
    }
    if (self.emailTextField.text.length > 0  && ![iBeautyUtility isValidEmailAddress:self.emailTextField.text]) {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"Please enter valid email address", @"")];
        return NO ;
    }
    if (self.emailTextField.text.length < 1) {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"Please enter email address", @"")];
        return NO ;
    }
    if (self.passwordTextField.text.length < 1) {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"Please enter password", @"")];
        return NO ;
    }
    return YES;
}

#pragma mark - Add Oberver

- (void)addGetEmailTextOberver {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getEmailTextFromRegisterScreen:) name:@"enterEmailFromRegisterViaFBGoogleLogin" object:nil];
    
}

- (void)getEmailTextFromRegisterScreen:(NSNotification*)emailInfo {
    
    NSDictionary *userEmailInfo = emailInfo.userInfo;
    if (APP_DEBUG_MODE)
        NSLog(@"User Email is %@",userEmailInfo);
    NSString *emailString = [userEmailInfo valueForKey:KEmail];
    
    NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
    [newDict addEntriesFromDictionary:self.profileInfoDict];
    [newDict setObject:emailString forKey:@"email"];
    
    if (APP_DEBUG_MODE)
        NSLog(@"User Info Dict is %@",newDict);
    
    NSDictionary *profileInfo = [self getUserProfileInfo:newDict];
    [self registerAccount:profileInfo];
    
}

#pragma mark - Google Login

- (IBAction)googleLogin:(id)sender {
    
    AppDelegate *delegate = [AppDelegate sharedInstance];
    delegate.oberverStatus = YES;
    
    self.loginType = @"google";
    [[GIDSignIn sharedInstance] signIn];
    
}

- (void)showGoogleNotificationListener{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveGoogleAuthInfoNotification:) name:@"ToggleAuthFromRegisterUINotification" object:nil];
}
- (void)receiveGoogleAuthInfoNotification:(NSNotification*)notification {
    
    NSDictionary *userProfileInfo = notification.userInfo;
    if (APP_DEBUG_MODE)
        NSLog(@"User Profile Info%@",userProfileInfo);
    
    [self goToEnterEmailScreen:userProfileInfo type:1];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ToggleAuthFromRegisterUINotification" object:nil];
}

#pragma mark - Facebook Login

- (IBAction)facebookLogin:(id)sender {
    
    self.loginType = @"facebook";
    
    LoginManager *loginMananger = [[LoginManager alloc]init];
    [loginMananger loginWithFacebook:^(id result, NSError *error) {
        
        [SVProgressHUD dismiss];
        
        if (result) {
            if (APP_DEBUG_MODE)
                NSLog(@"Response is %@",result);
            
            [self goToEnterEmailScreen:result type:0];
            
        }else{
            if (APP_DEBUG_MODE)
                NSLog(@"error is %@",error.localizedDescription);
            [iBeautyUtility showAlertMessage:ERROR_MESSAGE];
        }
    }];
    
}

- (void)goToEnterEmailScreen:(NSDictionary*)result type:(NSInteger)type {
    
    self.profileInfoDict = result;
    NSString *emailString = [result valueForKey:@"email"];
    
    if (emailString.length>0) {
        
        if (type == 0) {
            NSDictionary *profileInfo = [self getUserProfileInfo:result];
            [self registerAccount:profileInfo];
        }else {
            [self registerAccount:result];
        }
        
    }else {
        
        EnterInformationController *infoController = [self.storyboard instantiateViewControllerWithIdentifier:kEnterInformationController];
        infoController.emailText = emailString;
        infoController.status = YES;
        infoController.modalPresentationStyle = UIModalPresentationFullScreen;
        [self presentViewController:infoController animated:YES completion:nil];
    }
}

#pragma mark - Register Account Method

-(void)registerAccount:(NSDictionary*)userInfo {
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_REGISTER];
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSDictionary *parametersDict  = [self getUserInfoDict:userInfo];
    
    if (APP_DEBUG_MODE) { NSLog(@"parametersDict is %@",parametersDict);}
    
    if (internetStatus != NotReachable) {
        
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonPostData:url params:parametersDict onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                
                @try {
                    if (completed) {
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSDictionary *userInfoDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE)
                            NSLog(@"User info is %@", userInfoDict);
                        
                        if ([userInfoDict valueForKey:@"token"]) {
                            [[NSUserDefaults standardUserDefaults]setValue:[userInfoDict valueForKey:@"token"] forKey:KAccessToken];
                            [[NSUserDefaults standardUserDefaults]setValue:self.loginType forKey:KLoginType];
                            [[NSUserDefaults standardUserDefaults]synchronize];
                            [self setUpUserInfo:[userInfoDict valueForKey:@"userInfo"]];
                            [self goTohomeScreen];
//                            [iBeautyUtility showAlertMessage:NSLocalizedString(@"Logged in successfully", @"")];
                        }else {
                            //                            [iBeautyUtility showAlertMessage:@"Something went wrong."];
                            [iBeautyUtility showAlertMessage:[userInfoDict valueForKey:@"message"]];
                        }
                        
                    }else {
                        if (APP_DEBUG_MODE)
                            NSLog(@"error is %@", error.localizedDescription);
                        [SVProgressHUD dismiss];
                    }
                    
                } @catch (NSException *exception) {
                    //[iBeautyUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}

- (void)goTohomeScreen {
    
    AppDelegate *delegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    MainViewController *mainVC = [self.storyboard instantiateViewControllerWithIdentifier:kMainViewController];
    [delegate setRootViewController:mainVC];
}

#pragma mark - Get User ProfileInfo from FB/Google

-(NSDictionary*)getUserInfoDict:(NSDictionary *)userInfo {
    
    // {"name":"","username":"","email":"","phone":"","password":"","birthday":"","accounttype":"","imageurl":"","fguserid":""}
    
    NSString *name = [NSString stringWithFormat:@"%@",[iBeautyUtility trimString:[userInfo valueForKey:KProfileName]]];
    NSString *phone = @"";
    NSString *password = @"";
    NSString *birthday = @"";
    NSString *email = [NSString stringWithFormat:@"%@",[iBeautyUtility trimString:[userInfo valueForKey:KEmailId]]];
    NSString *imageURLString = [NSString stringWithFormat:@"%@",[iBeautyUtility trimString:[userInfo valueForKey:KProfileImageUrl]]];;
    NSString *accountType  = self.loginType;
    NSString *userID       = [NSString stringWithFormat:@"%@",[iBeautyUtility trimString:[userInfo valueForKey:KLoginIdentifier]]];
    
    NSDictionary *parametersDict  = [NSDictionary dictionaryWithObjectsAndKeys:name,@"name",name,@"username",phone,@"phone",email,@"email",password,@"password",birthday,@"birthday",accountType,@"accounttype",imageURLString,@"imageurl",userID,@"fguserid",nil];
    
    return parametersDict;
    
}

# pragma mark - Get User Profiles Info Dict

- (NSDictionary*)getUserProfileInfo:(NSDictionary*)userInfo {
    
    NSString *email = [NSString stringWithFormat:@"%@",[iBeautyUtility trimString:[userInfo valueForKey:@"email"]]];
    NSString *userId = [NSString stringWithFormat:@"%@",[iBeautyUtility trimString:[userInfo valueForKey:@"id"]]];
    NSString *fullName = [NSString stringWithFormat:@"%@",[iBeautyUtility trimString:[userInfo valueForKey:@"name"]]];
    NSDictionary *pictureInfo = [[userInfo valueForKey:@"picture"]valueForKey:@"data"];
    NSString *imageURLString = [NSString stringWithFormat:@"%@",[iBeautyUtility trimString:[pictureInfo valueForKey:@"url"]]];
    NSString *loginType   = @"Facebook";
    
    NSDictionary *parametersDict   = [NSDictionary dictionaryWithObjectsAndKeys:fullName,KProfileName,imageURLString,KProfileImageUrl,loginType,KLoginType,userId,KLoginIdentifier,email,KEmailId,nil];
    
    return parametersDict;
}

#pragma mark - set up Tool Bar On Text fields

- (void)setupToolBarOnTextfields {
    
    self.toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50)];
    self.toolBar.barStyle = UIBarStyleDefault;
    [self.toolBar sizeToFit];
    [self.toolBar setBackgroundColor:[UIColor lightGrayColor]];
    [self buttonWithinToolbar];

    self.userNameTextField.inputAccessoryView = self.toolBar;
    self.phNumberTextField.inputAccessoryView = self.toolBar;
    self.emailTextField.inputAccessoryView = self.toolBar;
    self.passwordTextField.inputAccessoryView = self.toolBar;
}

- (void)buttonWithinToolbar {
    
    self.doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", @"") style:UIBarButtonItemStyleDone target:self action:@selector(doneAction:)];
    
    UIBarButtonItem *flexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [self.toolBar setItems:@[flexibleItem,self.doneButton] animated:NO];
}

- (void)doneAction:(id)sender {
    
    [self.userNameTextField resignFirstResponder];
    [self.phNumberTextField resignFirstResponder];
    [self.emailTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    
}

# pragma mark - Textfield Delegate Methods

-(BOOL) textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString *currentString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSInteger length = [currentString length];
    
        if (textField == self.phNumberTextField) {
            
            if (length > 10 ) {
                return NO;
            }
        }
   
    return YES ;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
