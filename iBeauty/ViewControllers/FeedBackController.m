//
//  FeedBackController.m
//  iBeauty
//
//  Created by App Innovation on 30/11/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "FeedBackController.h"
#import "Constants.h"

@interface FeedBackController ()<UITextFieldDelegate,UITextViewDelegate>

@property (nonatomic,weak) IBOutlet UIView *reportBugView;
@property (nonatomic,weak) IBOutlet UIView *queryForView;
@property (nonatomic,weak) IBOutlet UIView *queryTextBkgView;
@property (nonatomic,weak) IBOutlet UITextView *messageTextView;

@property (nonatomic,weak) IBOutlet UILabel *placeHolderLabel;
@property (nonatomic,strong)  NSString *queryForString;

@property (nonatomic,weak) IBOutlet UIView *phoneNumberView;
@property (nonatomic,weak) IBOutlet UITextField *phoneNumberTxtField;

@property (nonatomic,weak) IBOutlet UIView *emailView;
@property (nonatomic,weak) IBOutlet UITextField *emailTxtField;

@property (nonatomic,weak) IBOutlet UIButton *sendButton;
@property (nonatomic,weak) IBOutlet UIButton *issuesButton;
@property (nonatomic,weak) IBOutlet UIButton *suggestionsButton;
@property (nonatomic,weak) IBOutlet UIImageView *issuesImgView;
@property (nonatomic,weak) IBOutlet UIImageView *suggestionsImgView;
@property (nonatomic,weak) IBOutlet UILabel *phoneNumberLabel;
@property (nonatomic,weak) IBOutlet UILabel *emailLable;
@property (nonatomic,weak) IBOutlet UILabel *headingLable;
@property (nonatomic,weak) IBOutlet UILabel *issuesLable;
@property (nonatomic,weak) IBOutlet UILabel *suggestionsLable;
@property (nonatomic,weak) IBOutlet UINavigationItem *navigationTitle;

@property UIToolbar *toolBar;
@property (nonatomic, strong) UIBarButtonItem   *doneButton;
@property (nonatomic,weak)  IBOutlet UINavigationBar *navBar;

@end

@implementation FeedBackController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setUpRadioButton];
    [self setUpLocalizationsViews];
    [self setupToolBarOnTextfields];
    [self setUpTextFieldsCursorAccordingToLanguage];
    //[self setGradient];

}

#pragma mark - View Will Appear

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    self.tabBarController.tabBar.hidden = YES;
}


#pragma mark - Set Gradient

- (void)setGradient {
    
    self.navBar.frame = [iBeautyUtility setUpFrame:self.navBar.frame];
    CAGradientLayer *gradient = [iBeautyUtility setNavigationGradient:self.navBar];
    [self.navBar setBackgroundImage:[iBeautyUtility imageFromLayer:gradient] forBarMetrics:UIBarMetricsDefault];
    
}

#pragma mark - Send Feedback

-(IBAction)sendButtonActionMethod:(UIButton*)sender {
    
    BOOL validStatus =  [self checkValidationForSendSuggestions];
    
    if (validStatus == YES) {
        
        [self sendSuggestionsIssues];
        
    }else {
        return;
    }
    
}

#pragma mark - Set Localizations

- (void)setUpLocalizationsViews {
    
    self.sendButton.frame = [iBeautyUtility setUpFrame:self.sendButton.frame];
    //[self.sendButton.layer insertSublayer:[iBeautyUtility setGradientColor_Buttons:self.sendButton] atIndex:0];
    [iBeautyUtility setButtonCorderRound:self.sendButton];

    [self.sendButton setTitle:NSLocalizedString(@"Send", @"") forState:UIControlStateNormal];
    self.issuesLable.text = NSLocalizedString(@"Issues", @"");
    self.suggestionsLable.text = NSLocalizedString(@"Suggestions", @"");

    self.phoneNumberLabel.text = NSLocalizedString(@"Phone number", @"");
    self.emailLable.text = NSLocalizedString(@"Email", @"");
    self.headingLable.text = NSLocalizedString(@"Report bugs every time you encounter problems to help us solve them faster.", @"");
    self.placeHolderLabel.text = NSLocalizedString(@"Describe the issues you've encountered", @"");
    self.navigationTitle.title = NSLocalizedString(@"Feedback", @"");
    
    self.phoneNumberTxtField.placeholder = NSLocalizedString(@"Phone number", @"") ;
    self.emailTxtField.placeholder = NSLocalizedString(@"Email id", @"");

}

#pragma mark - Text Fields Cursors

- (void)setUpTextFieldsCursorAccordingToLanguage {
    
    NSString *languageCode =   [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];
    
    if ([languageCode isEqualToString:@"en"]) {
        
        self.phoneNumberTxtField.textAlignment = NSTextAlignmentLeft;
        self.emailTxtField.textAlignment = NSTextAlignmentLeft;
        self.messageTextView.textAlignment = NSTextAlignmentLeft;
        
    }else if ([languageCode isEqualToString:@"ar"]) {
        
        self.phoneNumberTxtField.textAlignment = NSTextAlignmentRight;
        self.emailTxtField.textAlignment = NSTextAlignmentRight;
        self.messageTextView.textAlignment = NSTextAlignmentRight;
        
    }
}

#pragma mark - Send Suggestions Call

-(void)sendSuggestionsIssues {
    
    NSString *url = [ConnectionHandler getServiceURL_NewAPIs:TYPE_SERVICE_FEEDBACK];
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    NSString *customerID = [[NSUserDefaults standardUserDefaults]valueForKey:KUserId];

    NSDictionary *parametersDict  = [NSDictionary dictionaryWithObjectsAndKeys:accessToken,@"Authorization",customerID,@"CustomerId",self.queryForString,@"QueryFor",self.messageTextView.text,@"Concern",self.emailTxtField.text,@"Email",self.phoneNumberTxtField.text,@"Phone",nil];
    
    if (APP_DEBUG_MODE)
        NSLog(@"parametersDict is %@",parametersDict);
    
    if (internetStatus != NotReachable) {
        
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonPostData:url params:parametersDict onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                
                @try {
                    if (completed) {
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSDictionary *infoDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE)
                            NSLog(@"info is %@", infoDict);
                        NSInteger status = [[infoDict valueForKey:@"status"]integerValue];
                        if (status == 200) {
                            [iBeautyUtility showAlertMessage:[infoDict valueForKey:@"message"]];
                            [self setUpViewBlanks];
                        }

                    }else {
                        if (APP_DEBUG_MODE)
                            NSLog(@"error is %@", error.localizedDescription);
                        [SVProgressHUD dismiss];
                    }
                    
                } @catch (NSException *exception) {
                    //[iBeautyUtility showAlertMessage:NETWORK_ERROR_MESSAGE];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
    
}

- (void)setUpViewBlanks {
    
    self.phoneNumberTxtField.text = @"";
    self.emailTxtField.text = @"";
    self.messageTextView.text = @"";
}

#pragma mark - Validations

- (BOOL)checkValidationForSendSuggestions {
    

    if (self.queryForString.length < 1) {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"Please select option issue/suggestion", @"")];
        return NO ;
    }
    if (self.messageTextView.text.length < 1) {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"Please write your issues/suggestions", @"")];
        return NO ;
    }
    if (self.emailTxtField.text.length > 0  && ![iBeautyUtility isValidEmailAddress:self.emailTxtField.text]) {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"Please enter valid email address", @"")];
        return NO ;
    }
    return YES;
    
}

#pragma mark - Radio Buttons

- (void)setUpRadioButton {
    
    self.issuesImgView.image = [UIImage imageNamed:@"Uncheck"];
    self.suggestionsImgView.image = [UIImage imageNamed:@"Uncheck"];
}

#pragma mark - Select Issue Suggestions

- (IBAction)selectIssueSuggestionsOptions:(UIButton*)sender {
    
    switch ([sender tag]) {
            
        case 0:
            [self.issuesButton setSelected:YES];
            [self.suggestionsButton setSelected:NO];
            [self.issuesImgView setImage:[UIImage imageNamed:@"Check"]];
            [self.suggestionsImgView setImage:[UIImage imageNamed:@"Uncheck"]];
            
            self.queryForString = @"issue";
            self.placeHolderLabel.text = NSLocalizedString(@"Describe the issues you've encountered", @"");
            break;
            
        case 1:
            [self.suggestionsButton setSelected:YES];
            [self.issuesButton setSelected:NO];
            [self.suggestionsImgView setImage:[UIImage imageNamed:@"Check"]];
            [self.issuesImgView setImage:[UIImage imageNamed:@"Uncheck"]];
            
            self.queryForString = @"suggestion";
            self.placeHolderLabel.text = NSLocalizedString(@"Describe the suggestions you've encountered", @"");
            break;
            
        default:
            break;
    }
    
}

#pragma mark - set up Tool Bar On Text fields

- (void)setupToolBarOnTextfields {
    
    self.toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50)];
    self.toolBar.barStyle = UIBarStyleDefault;
    [self.toolBar sizeToFit];
    [self.toolBar setBackgroundColor:[UIColor lightGrayColor]];
    [self buttonWithinToolbar];
    self.phoneNumberTxtField.inputAccessoryView = self.toolBar;
    self.emailTxtField.inputAccessoryView = self.toolBar;
    self.messageTextView.inputAccessoryView = self.toolBar;
}

- (void)buttonWithinToolbar {
    
    self.doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", @"") style:UIBarButtonItemStyleDone target:self action:@selector(doneAction:)];
    
    UIBarButtonItem *flexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [self.toolBar setItems:@[flexibleItem,self.doneButton] animated:NO];
}

- (void)doneAction:(id)sender {
    
    [self.phoneNumberTxtField resignFirstResponder];
    [self.emailTxtField resignFirstResponder];
    [self.messageTextView resignFirstResponder];
}

# pragma mark - Textfield Delegate Methods

-(BOOL) textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}


#pragma mark - Text view delegates

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    
    self.messageTextView.text = @"";
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    
    if (![self.messageTextView hasText]) {
        self.placeHolderLabel.hidden = NO;
    }
}

- (void) textViewDidChange:(UITextView *)textView {
    
    if(![self.messageTextView hasText]) {
        self.placeHolderLabel.hidden = NO;
    }
    else{
        self.placeHolderLabel.hidden = YES;
    }
}

#pragma mark - Back Action Method

- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
