//
//  LoginViewController.h
//  iBeauty
//
//  Created by App Innovation on 27/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <Google/SignIn.h>
#import <AuthenticationServices/AuthenticationServices.h>

@import GoogleSignIn;
@import SkyFloatingLabelTextField;

@interface LoginViewController : UIViewController<GIDSignInDelegate,ASAuthorizationControllerDelegate,ASAuthorizationControllerPresentationContextProviding>

@end
