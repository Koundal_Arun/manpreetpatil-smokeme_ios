//
//  CancelAppointmentViewController.h
//  iBeauty
//
//  Created by App Innovation on 17/12/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AllAppointmentsInfo.h"
#import "Constants.h"

NS_ASSUME_NONNULL_BEGIN

@interface CancelAppointmentViewController : UIViewController

@property (nonatomic,strong) AllAppointmentsInfo *appointmentOrderObj;
@property (nonatomic,strong) AllAppointmentsInfo *appointmentOrderDetailObj;
@property (nonatomic,assign) NSInteger appointmentsNumber;
@property (nonatomic,assign) NSInteger selectedIndex;

@property (nonatomic,assign) NSInteger cancelType;

@end

NS_ASSUME_NONNULL_END
