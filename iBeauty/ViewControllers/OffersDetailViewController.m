//
//  OffersDetailViewController.m
//  iBeauty
//
//  Created by App Innovation on 11/09/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "OffersDetailViewController.h"
#import "OfferDetailCell.h"
#import "OffersDetail.h"
#import "OfferDetailHeader.h"
#import "OfferDetailFooter.h"
#import "ServicesDetail.h"
#import "Constants.h"
#import "UILabel+FlexibleWidHeight.h"
#import "NSString+iBeautyString.h"
#import "NSDate+iBeautyDateFormatter.h"

static NSString *detailCellIdentifier = @"OfferDetailCell";
static NSString *headerCellIdentifier = @"OfferDetailHeader";
static NSString *footerCellIdentifier = @"OfferDetailFooter";

@interface OffersDetailViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic,weak) IBOutlet UICollectionView *offerDetailCollView;
@property (nonatomic,strong)  NSMutableArray *offerDetailArray;

@property (nonatomic,weak)  IBOutlet UIButton *bookNowButton;
@property (nonatomic,weak)  IBOutlet UILabel  *servicesLabel;
@property (nonatomic,weak)  IBOutlet UIView   *bookNowView;
@property (nonatomic,weak)  IBOutlet UILabel  *itemTitle;

@property (nonatomic,assign)  NSInteger servicesCount;

@property (nonatomic,strong)  NSMutableArray *countInfoArray;
@property (nonatomic,strong)  NSMutableArray *selectedServicesArray;
@property (nonatomic,strong)  NSMutableDictionary *priceInfoDict;

@property (nonatomic,strong)  NSMutableArray *servicesInfoArray;
@property (nonatomic,assign)  BOOL decrementStatus;
@property (nonatomic,strong)  AppDelegate *delegate;
@property (nonatomic,weak)  IBOutlet UINavigationBar *navBar;

@end

@implementation OffersDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.delegate = [AppDelegate sharedInstance];
    [self setUpView];
    
    [iBeautyUtility setBookNowButtonCorderRound:self.bookNowButton];
    
    //[self.bookNowButton.layer insertSublayer:[iBeautyUtility setGradientColor_Buttons:self.bookNowButton] atIndex:0];

    self.countInfoArray = [NSMutableArray new];
    self.selectedServicesArray = [NSMutableArray new];

    [self setCountOfServices];
    self.itemTitle.text = NSLocalizedString(@"Your Appointments", @"");
    
    [self setInfoValuesToViews];
    //[self setGradient];

}

#pragma mark - View Will Appear

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    self.delegate.tabbarController.tabBar.hidden = YES;
}

#pragma mark - Set Gradient

- (void)setGradient {
    
    self.navBar.frame = [iBeautyUtility setUpFrame:self.navBar.frame];
    CAGradientLayer *gradient = [iBeautyUtility setNavigationGradient:self.navBar];
    [self.navBar setBackgroundImage:[iBeautyUtility imageFromLayer:gradient] forBarMetrics:UIBarMetricsDefault];
}


- (void)setInfoValuesToViews {
    
    NSString *servicesAdded = @"";
    servicesAdded = [self setUpServicesAddedLabel];
    
    self.servicesLabel.text = [NSString stringWithFormat:@"%ld %@",(long)self.servicesCount,servicesAdded];
}

- (void)setCountOfServices {
    
    self.offerDetailArray = self.offerInfo.servicesArray;

    if ([self.offerInfo.discountType isEqualToString:@"Flat"]) {
        self.servicesCount = 0;
    }else {
        self.servicesCount = self.offerDetailArray.count;
    }
}

- (void)setCountOfProducts {
    
    self.offerDetailArray = self.offerInfo.productsArray;
    
    if ([self.offerInfo.discountType isEqualToString:@"Flat"]) {
        self.servicesCount = 0;
    }else {
        self.servicesCount = self.offerDetailArray.count;
    }
}

- (NSString *)setUpServicesAddedLabel {
    
    NSString *itemsAdded = @"";
    
    if (self.servicesCount == 0 || self.servicesCount == 1) {
        itemsAdded = NSLocalizedString(@"Service added", @"");
    }else {
        itemsAdded = NSLocalizedString(@"Services added", @"");
    }
    return itemsAdded;
}

- (NSString *)setUpProductsAddedLabel {
    
    NSString *itemsAdded = @"";
    
    if (self.servicesCount == 0 || self.servicesCount == 1) {
        itemsAdded = NSLocalizedString(@"Product added", @"");
    }else {
        itemsAdded = NSLocalizedString(@"Products added", @"");
    }
    return itemsAdded;
}

- (void)setUpView {
    
    self.offerDetailCollView.translatesAutoresizingMaskIntoConstraints = YES;
    
    if (self.screenStatus == YES) {
        
        self.bookNowView.hidden = YES;
        self.offerDetailCollView.frame = CGRectMake(self.offerDetailCollView.frame.origin.x, self.offerDetailCollView.frame.origin.y, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-20.0f);
    }else {
        self.bookNowView.hidden = NO;
        if (IS_IPHONE_X || IS_IPHONE_XS_MAX) {
            
            self.offerDetailCollView.frame = CGRectMake(self.offerDetailCollView.frame.origin.x, self.offerDetailCollView.frame.origin.y, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-20.0f-66.0f-24.0f);

        }else {
            self.offerDetailCollView.frame = CGRectMake(self.offerDetailCollView.frame.origin.x, self.offerDetailCollView.frame.origin.y, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-20.0f-66.0f);
        }
    }
    
}


#pragma mark - Add Notification Observer For offers services


- (void)addNotificationObserverForOffersInfo {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getOfferServicesInfo:) name:@"selectedOfferServicesInfo" object:nil];
    
}

- (void)getOfferServicesInfo:(NSNotification*)appointmentInfo {
    
    NSMutableArray *infoArray = [[NSMutableArray alloc]init];
    
    if (APP_DEBUG_MODE) { NSLog(@"Info  is %@",appointmentInfo); }
    
    NSDictionary *appointmentDict = appointmentInfo.userInfo;
    if ([appointmentDict valueForKey:@"appointmentsInfo"]) {
        infoArray = [appointmentDict valueForKey:@"appointmentsInfo"];
    }
    
    self.decrementStatus =  [[appointmentDict valueForKey:@"DecrementStatus"]boolValue];
    self.servicesInfoArray = [NSMutableArray new];

    if (self.decrementStatus == NO) {
        
        if (self.selectedServicesArray.count>0) {
            [self.selectedServicesArray removeAllObjects];
        }
    }
    
    if (infoArray.count>0) {
        
        for (int i =0;i<infoArray.count;i++) {
            NSString *deletedServiceID = infoArray[i];
            for (ServicesDetail *detailInfo in self.offerDetailArray) {
                if ([deletedServiceID  isEqualToString:detailInfo.serviceId]) {
                    if (detailInfo.increaseIndex >= 1) {
                        detailInfo.increaseIndex = detailInfo.increaseIndex-1;
                    }
                }
            }
        }
    }
    
    [self addTotalAppointments];
}


#pragma mark - Book Appointment

-(IBAction)bookNowAction:(id)sender {
    
    NSString *nowDateString = [[NSDate date] convertDateToNSString:DATE_FORMAT_APPOINTMENT_DETAIL date:[NSDate date]];
    NSDate *nowDate = [NSDate convertNSStringToNSDate:nowDateString format:DATE_FORMAT_APPOINTMENT_DETAIL];
    
    if ([nowDate compare:self.offerInfo.offer_EndDate] == NSOrderedDescending) {
        [iBeautyUtility showAlertMessage:@"Offer date is expired"];
        return;
    }

    BookAppointmentController *bookAppointmentController = [self.storyboard instantiateViewControllerWithIdentifier:kAppointmentViewController];
    
    if ([self.offerInfo.discountType isEqualToString:@"Flat"]) {
        if (self.servicesCount>0) {
            bookAppointmentController.appointmentsDetailArray = self.selectedServicesArray;

        }else {
            [iBeautyUtility showAlertMessage:@"Please select services"];
            return;
        }
    }else {
        bookAppointmentController.appointmentsDetailArray = self.offerInfo.servicesArray;
    }
    
    bookAppointmentController.offerInfoObj = self.offerInfo;
    bookAppointmentController.screenStatus = YES;
    bookAppointmentController.categoryInfoObj = self.categoryInfoObj;
    [self.navigationController pushViewController:bookAppointmentController animated:YES];

}

#pragma mark - CollectioView DataSource and Delegate methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.offerDetailArray.count;
    
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    OfferDetailCell *detailCell = [collectionView dequeueReusableCellWithReuseIdentifier:detailCellIdentifier forIndexPath:indexPath];
    
    ServicesDetail *detailObj = self.offerDetailArray[indexPath.row];
    
    [detailCell configureOfferDetailCell:detailObj offerInfo:self.offerInfo];
        [detailCell.decreaseButton addTarget:self action:@selector(removeService:) forControlEvents:UIControlEventTouchUpInside];
    detailCell.decreaseButton.tag = indexPath.row;
    
    [detailCell.increaseButton addTarget:self action:@selector(addService:) forControlEvents:UIControlEventTouchUpInside];
    detailCell.increaseButton.tag = indexPath.row;
    
    if (self.decrementStatus == YES) {
        detailCell.decreaseButton.hidden = YES;
    }else {
        detailCell.decreaseButton.hidden = NO;
    }
    
    return detailCell;
    
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat screenWidth = CGRectGetWidth([UIScreen mainScreen].bounds);
    return CGSizeMake(screenWidth, 31);
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 1.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 1.0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    
    return UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
        
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    id reusableview = [[UICollectionReusableView alloc]init];
    
    if (kind == UICollectionElementKindSectionHeader) {
        
        OfferDetailHeader *collHeaderView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:headerCellIdentifier forIndexPath:indexPath];
        [collHeaderView.backButton addTarget:self action:@selector(backActionMethod:) forControlEvents:UIControlEventTouchUpInside];
        [collHeaderView configureOfferDetailHeaderView:self.offerInfo screenStatus:self.screenStatus serviceCategoriesInfo:self.categoryInfoObj];
        
        [collHeaderView.addCartButton addTarget:self action:@selector(addToCartMethod:) forControlEvents:UIControlEventTouchUpInside];

        reusableview = collHeaderView;
        
    }else if (kind == UICollectionElementKindSectionFooter) {
        
        OfferDetailFooter *collFooterView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:footerCellIdentifier forIndexPath:indexPath];
        [collFooterView configureOfferDetailFooterView:self.offerInfo screenStatus:self.screenStatus serviceCategoriesInfo:self.categoryInfoObj];
        reusableview = collFooterView;
        
    }
    
    return reusableview;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    
    CGFloat screenWidth = CGRectGetWidth([UIScreen mainScreen].bounds);
    CGFloat sectionHeight = 0.0f;
    
    if ([self.offerInfo.discountType isEqualToString:@"Flat"]) {
        sectionHeight = 293.0f;
    }else {
        sectionHeight = 429.0f;
    }
    
    return CGSizeMake(screenWidth,sectionHeight);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    
    CGFloat screenWidth = CGRectGetWidth([UIScreen mainScreen].bounds);
    
    NSString *description = self.offerInfo.detailDesc;
    NSString *termsCondition = self.offerInfo.termsAndConditions;

    CGFloat descHeight = [UILabel heightOfTextForString:description andFont:[UIFont fontWithName:@"SegoeUI" size:15.0] maxSize:(CGSize){screenWidth-35, MAXFLOAT}];
    CGFloat termsConditionHeight = [UILabel heightOfTextForString:termsCondition andFont:[UIFont fontWithName:@"SegoeUI" size:15.0] maxSize:(CGSize){screenWidth-35, MAXFLOAT}];

    
    CGFloat sectionHeight= 0.0f;
    
    if (termsCondition.length > 0) {
        
        if (termsCondition.length > 0) {
            
            sectionHeight = 72.0f + termsConditionHeight + 10.0f ;
            
        }else {
            sectionHeight = 41.0f;
        }
    }
    
    if (description.length > 0) {
        if (description.length > 0) {
            sectionHeight = 72.0f + descHeight + 10.0f ;
        }else {
            sectionHeight = 41.0f  ;
        }
    }
    
    return CGSizeMake(screenWidth,sectionHeight);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
}

#pragma mark - Add/Remove Services



- (void)addService:(UIButton*)sender {
    
    ServicesDetail*detailInfo = self.offerDetailArray[sender.tag];
    detailInfo.increaseIndex += 1;
    
    [self addTotalAppointments];
    
}

- (void)removeService:(UIButton*)sender {

    ServicesDetail*detailInfo = self.offerDetailArray[sender.tag];
    if (detailInfo.increaseIndex>0) {
        detailInfo.increaseIndex -= 1;
    }
    [self addTotalAppointments];
}


- (void)addTotalAppointments {
    
    self.servicesCount = 0;
    
    if (self.selectedServicesArray.count>0) {
        [self.selectedServicesArray removeAllObjects];
    }
    
    for (ServicesDetail *detailInfo in self.offerDetailArray) {
            
        if (detailInfo.increaseIndex>0) {
                
            self.servicesCount = self.servicesCount + detailInfo.increaseIndex;
            
            for (int i = 0; i < detailInfo.increaseIndex; i++) {
                [self.selectedServicesArray addObject:detailInfo];
            }
        }
    }
    [self setInfoValuesToViews];
    [self.offerDetailCollView reloadData];

}


#pragma mark - Remove Total Appointments

- (void)removeTotalAppointments {
    
    self.servicesCount = 0;
    
    for (ServicesDetail *detailInfo in self.offerDetailArray) {
            
        if (detailInfo.increaseIndex>0) {
            detailInfo.increaseIndex = 0;
        }
    }
    
    if (self.selectedServicesArray.count>0) {
        [self.selectedServicesArray removeAllObjects];
    }
}


#pragma mark - Add to cart Action

- (void)addToCartMethod:(id)sender {
    
    NSMutableDictionary *dictInfo = [[NSMutableDictionary alloc]init];
    [dictInfo setValue:[NSNumber numberWithInteger:self.indexValue] forKey:@"Index"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"selectedOfferInfo" object:nil userInfo:dictInfo];
     
    [self.navigationController popViewControllerAnimated:YES];
    
}


#pragma mark - Back Action

- (void)backActionMethod:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    [self removeTotalAppointments];

}


#pragma mark - Pick services price information

- (void)getServicesInfoArray {
    
    if (self.selectedServicesArray.count>0) {
        [self.selectedServicesArray removeAllObjects];
    }
    
    float totalPrice = 0.0f;
    float totalDiscountedPrice = 0.0f;
    
    for (int i = 0; i< self.offerDetailArray.count; i++) {
        
        NSMutableDictionary *dictInfo = self.countInfoArray[i];
        if ([[dictInfo valueForKey:@"addStatus"]boolValue] == YES) {
            [self.selectedServicesArray addObject:self.offerDetailArray[i]];
            ServicesDetail *serviceObj = self.offerDetailArray[i];
            float serviceAmount = [serviceObj.serviceAmount floatValue];
            float discountedValue = serviceAmount - (serviceAmount*self.offerInfo.discountValue)/100;
            totalDiscountedPrice = totalDiscountedPrice + discountedValue ;
            totalPrice = totalPrice + serviceAmount ;
        }
    }
    self.priceInfoDict = [[NSMutableDictionary alloc]init];
    [self.priceInfoDict setValue:[NSNumber numberWithFloat:totalDiscountedPrice] forKey:@"TotalDiscountedPrice"];
    [self.priceInfoDict setValue:[NSNumber numberWithFloat:totalPrice] forKey:@"TotalPrice"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
