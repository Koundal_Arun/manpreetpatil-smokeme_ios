//
//  SelectTimeSlotController.h
//  iBeauty
//
//  Created by App Innovation on 04/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "ServicesDetail.h"
#import "StaffDetail.h"
#import "ServiceTimeSlot.h"
#import "ServicesCategoriesInfo.h"
#import "OffersDetail.h"

@interface SelectTimeSlotController : UIViewController

@property (nonatomic,strong) ServicesDetail *serviceInfo;
@property (nonatomic,assign) NSInteger  slotIndex;
@property (nonatomic,strong) ServicesCategoriesInfo *categoryInfoObj;
@property (nonatomic,strong) StaffDetail *staffInfo;
@property (nonatomic,strong) NSString *serviceId;

@property (nonatomic,assign) BOOL screenStatus;
@property (nonatomic,strong) OffersDetail *offerInfo;

@end
