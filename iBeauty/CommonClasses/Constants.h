//
//  Constants.h
//  iBeauty
//
//  Created by App Innovation on 27/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#import "AppDelegate.h"
#import "CommonClass.h"
#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "UIColor+iBeauty.h"
#import "iBeautyUtility.h"
#import "SVProgressHUD.h"
#import "AFNetworking.h"
#import "LoginManager.h"
#import "HomesViewController.h"
#import "MoreViewController.h"
#import "NearbyViewController.h"
#import "SlideMenuViewController.h"
#import "MenuListCell.h"
#import "UIViewController+MMDrawerController.h"
#import "MMDrawerController.h"
#import "MMDrawerVisualState.h"
#import "MenuHeaderView.h"
#import "ConnectionHandler.h"
#import "Reachability.h"
#import "Services.h"
#import "UIImageView+WebCache.h"
#import "ServiceCategoryController.h"
#import "CategoryDetailController.h"
#import "ServicesCategoriesInfo.h"
#import "UIColor+iBeauty.h"
#import "ServicesDetailViewController.h"
#import "BookAppointmentController.h"
#import "SelectTimeSlotController.h"
#import "SelectStaffViewController.h"
#import "PriceCartViewController.h"
#import "MyProfileViewController.h"
#import "PaymentOrdersController.h"
#import "MyAppointmentViewController.h"
#import "AppointmentDetailViewController.h"
#import "FilterViewController.h"
#import "OffersViewController.h"
#import "MainViewController.h"
#import "ProvidersViewController.h"
#import "ReferEarnViewController.h"
#import "SettingsViewController.h"
#import "FeedBackController.h"
#import "FAQViewController.h"
#import "ServiceProvideAtController.h"
#import "NoInternetViewController.h"
#import "ForgotPasswordController.h"
#import "VerifyViewController.h"
#import "ResetPasswordController.h"


#define UIKitLocalizedString(key) [[NSBundle bundleWithIdentifier:@"com.apple.UIKit"] localizedStringForKey:key value:@"" table:nil]

#define ERROR_MESSAGE              @"Invalid Response"
#define NETWORK_ERROR_MESSAGE      @"Network problem please try again.."
#define NETWORK_STATUS_INFO        @"No internet Connection found"
#define REGISTER_STATUS_MESSAGE    @"Registered successfully. Please login to proceed"
#define LOGIN_STATUS_MESSAGE       @"Logedin successfully."
#define NO_MESSAGE                 @"No Response"

#define ServiceProvider @"Service Provider"
#define OfferFrom @"OfferFrom"

#define kLMSelectedLanguageKey  @"kLMSelectedLanguageKey"

#define GOOGLE_CLIENT_ID  @"578917072794-evqcr2t63d9ej90s83l6dr6496ee68cd.apps.googleusercontent.com"

#define APP_DEBUG_MODE 1

#define STR_BASE_URL_NEW  @"https://smukme-webapi.appinnovation.in/" //@"http://webapis-beautysalons.appinnovation.co.uk/" //@"http://ibeautywebapi.appinnovation.co.uk/"

#define STR_BASE_URL      @"http://ibeautyapi.appinnovation.co.uk/api/Api/"

#define IMAGE_BASE_URL    @"https://smukme-images.appinnovation.in/" //@"http://images-demobeautysalon.appinnovation.co.uk/" //@"http://imagesibeauty.appinnovation.co.uk/uploads/index.php"

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)
#define BackGrounView           UIView *backgroundView = [[UIView alloc]initWithFrame:CGRectMake(0.0f, 0.0f, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)]; [backgroundView setAlpha:0.9]; [backgroundView setBackgroundColor:[UIColor blackColor]];

#define IS_IPHONE_4 (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)480) < DBL_EPSILON)
#define IS_IPHONE_5 (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)568) < DBL_EPSILON)
#define IS_IPHONE_6 (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)667) < DBL_EPSILON)
#define IS_IPHONE_6_PLUS (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)736) < DBL_EPSILON)
#define IS_IPHONE_7 (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)667) < DBL_EPSILON)
#define IS_IPHONE_7_PLUS (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)736) < DBL_EPSILON)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_X  (IS_IPHONE && SCREEN_MAX_LENGTH >= 812.0)
#define IS_IPHONE_XS_MAX  (IS_IPHONE && SCREEN_MAX_LENGTH == 896.0)

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define en_US @"en_US"
#define en_GB @"en_GB"
#define SETTING_TIME_FORMAT_12  @"hh:mm a"
#define SETTING_TIME_FORMAT_24  @"HH:mm:ss"
#define DATE_FORMAT_WITH_HOURS_MINUTES @"yyyy-MM-dd hh:mm a"
#define UTC_DATE_FORMAT         @"yyyy-MM-dd'T'HH:mm:ss" 
#define SLOT_DATE_FORMAT        @"E, MMM dd"        // @"E dd MMM yyyy"
#define CALENDAR_DATE_FORMAT    @"yyyy-MM-dd"
#define DATE_FORMAT             @"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"//@"yyyy-MM-dd HH:mm:ss"
#define PAYMENT_DATE_FORMAT     @"MMM dd, yyyy"        // @"E dd MMM yyyy"
#define APPOINTMENT_DATE_FORMAT @"MMM dd"        // @"E dd MMM yyyy"


#define DATE_FORMAT_NEW_SLOT              @"yyyy-MM-dd HH:mm"
#define DATE_FORMAT_APPOINTMENT_DETAIL    @"yyyy-MM-dd HH:mm:ss"
#define DATE_FORMAT_WITH_MINUTES_HOURS    @"hh:mm a,MMM dd, yyyy"


#define MERCHANT_EMAIL_ID    @"ankit.appinno@gmail.com"
#define MERCHANT_SECRET_KEY    @"0byMClfClYdCHEaGZ4vKOdqPi4MG35t3g1QTn5vI4JlLnwsmAY7pbxeuC0aF5EJmFzf2ELLJu7uSDXfeVmD7URCwSMcYrVk6uz99"

static NSInteger phoneNumberMaxLength = 10;

typedef enum {
    
    TYPE_SERVICE_SERVICECATEGORIES                      = 0x100,
    TYPE_SERVICE_SERVICE_PROVIDER_CATEGORIES,
    TYPE_SERVICE_SERVICE_PROVIDER_DETAIL,
    TYPE_SERVICE_GET_STAFF_DETAILS,
    TYPE_SERVICE_GET_DATE_TIME_SLOTS,
    TYPE_SERVICE_CREATE_BASKET,
    TYPE_SERVICE_UPDATE_BASKET,
    TYPE_SERVICE_GET_BASKET,
    TYPE_SERVICE_PAYMENT_ORDERS,
    TYPE_SERVICE_ALL_APPOINTMENTS,
    TYPE_SERVICE_ALL_OFFERS,
    TYPE_SERVICE_LOGIN,
    TYPE_SERVICE_REGISTER,
    TYPE_SERVICE_PRICING,
    TYPE_SERVICE_ADD_ADDRESS,
    TYPE_SERVICE_UPDATE_ADDRESS,
    TYPE_SERVICE_GET_ADDRESSES,
    TYPE_SERVICE_DELETE_ADDRESS,
    TYPE_SERVICE_CHANGE_PASSWORD,
    TYPE_SERVICE_FAQ,
    TYPE_SERVICE_RATING,
    TYPE_SERVICE_FEEDBACK,
    TYPE_SERVICE_PROMOTIONAL_ADS,
    TYPE_SERVICE_PROMOTIONAL_OFFER,
    TYPE_SERVICE_CANCEL_APPOINTMENT,
    TYPE_SERVICE_CANCEL_PACKAGE,
    TYPE_SERVICE_BUSINESS_CITIES,
    TYPE_SERVICE_PROVIDERBY_CITIES,
    TYPE_SERVICE_FILTER_PROVIDERS,
    
    TYPE_SERVICE_PRODUCT_SELLERS,
    TYPE_SERVICE_PRODUCT_SELLER_DETAIL,
    TYPE_SERVICE_PRODUCT_CREATE_BASKET,
    TYPE_SERVICE_GET_PRODUCT_BASKET,
    TYPE_SERVICE_GET_PRODUCT_UPDATE_BASKET,
    TYPE_SERVICE_GET_PRODUCTS_PRICING_INFO,
    TYPE_SERVICE_GET_PRODUCTS_PAYMENT_ORDER,
    TYPE_SERVICE_GET_PRODUCTS_CUSTOMERS_ORDER,
    TYPE_SERVICE_GET_PRODUCTS_BY_CITY,
    TYPE_SERVICE_CANCEL_ORDER_PRODUCT,
    TYPE_SERVICE_BUSINESS_PROVIDER_INFO,
    TYPE_SERVICE_SERVICES_OFFER_LIST,
    TYPE_SERVICE_PRODUCTS_OFFER_LIST,
    TYPE_SERVICE_CLEAR_SERVICE_BASKET,
    TYPE_SERVICE_CLEAR_PRODUCT_BASKET,
    
    TYPE_SERVICE_GET_ALL_CATEGORIES,
    TYPE_SERVICE_GET_OFFER_DETAIL,
    TYPE_SERVICE_FORGOT_PASSWORD,
    TYPE_SERVICE_RESET_PASSWORD,
    
} ServiceType;

typedef NS_ENUM(NSInteger, ServiceCategoryType) {
    
    Service=1,
    Product=2,
};

typedef NS_ENUM(NSInteger, operationType) {
    New=0,
    Delete=1,
    Update=2,
};

typedef NS_ENUM(NSInteger, orderStatus) {
    
    Cancelled=0,
    Placed=1,
    Shipped=2,
    Delivered=3,
};


#endif /* Constants_h */
