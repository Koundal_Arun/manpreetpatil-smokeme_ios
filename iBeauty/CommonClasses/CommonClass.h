//
//  CommonClass.h
//  iBeauty
//
//  Created by App Innovation on 27/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#ifndef CommonClass_h
#define CommonClass_h

#define KLoadingMessage        @"Please wait.."
#define KSyncingMessage        @"Syncing your orders. Please wait.."

#define KAccessToken           @"token"
//#define KLoginType             @"loginType"


//----- Identifiers Keys -----//

#define kNavigationController           @"navigation"
#define kLoginController                @"LoginViewController"
#define kRegisterController             @"RegisterViewController"
#define kHomeViewController             @"HomeViewController"
#define kHomesViewController            @"HomesViewController"
#define kMyAppointmentViewController    @"MyAppointmentViewController"
#define kOffersViewController           @"OffersViewController"
#define kPaymentViewController          @"PaymentViewController"
#define kRegerEarnViewController        @"ReferEarnViewController"
#define kSettingsController             @"SettingsViewController"
#define kFAQController                  @"FAQViewController"
#define kMenuViewController             @"SlideMenuViewController"
#define kServiceCategoryController      @"ServiceCategoryController"
#define kCategoryDetailController       @"CategoryDetailController"
#define kServicesDetailViewController   @"ServicesDetailViewController"
#define kAppointmentViewController      @"BookAppointmentController"
#define kSelectTimeSlotController       @"SelectTimeSlotController"
#define kSelectStaffViewController      @"SelectStaffViewController"
#define kPriceCartViewController        @"PriceCartViewController"
#define kMyProfileViewController        @"MyProfileViewController"
#define kPaymentOrdersController        @"PaymentOrdersController"
#define kAppointmentDetailController    @"AppointmentDetailViewController"
#define kFilterViewController           @"FilterViewController"
#define kOfferDetailController          @"OffersDetailViewController"
#define kEnterInformationController     @"EnterInformationController"
#define kEnterInformationController     @"EnterInformationController"
#define kAddAddressController           @"AddAddressController"
#define kAddressesViewController        @"AddressesViewController"
#define kChangePasswordController       @"ChangePasswordController"
#define kUpdateProfileController        @"UpdateProfileController"
#define kReviewRatingController         @"ReviewRatingController"
#define kFeedBackController             @"FeedBackController"
#define kCancelAppointmentController    @"CancelAppointmentController"
#define kSelectCityController           @"SelectCityController"
#define kSearchAddressController        @"SearchAddressController"
#define kNearByViewController           @"NearbyViewController"
#define kMoreViewController             @"MoreViewController"
#define kMainViewController             @"MainViewController"
#define kProvidersViewController        @"ProvidersViewController"
#define kServiceProvideAtController     @"ServiceProvideAtController"
#define kAddNewAddressController        @"AddNewAddressController"
#define kNoInternetViewController       @"NoInternetViewController"
#define kVerifyViewController           @"VerifyViewController"
#define kForgotPassController           @"ForgotPasswordController"
#define kResetPassController            @"ResetPasswordController"

//--- Create Profile Parameters ---//

#define KProfileName             @"Name"
#define KProfileImageUrl         @"ProfileImageUrl"
#define KLoginType               @"LoginType"
#define KLoginIdentifier         @"LoginIdentifier"
#define KEmail                   @"Email"
#define KLoginInfo               @"LoginInfo"
#define KLoginStatus             @"LoginStatus"
#define KUserId                  @"UserID"
#define KAccountType             @"AccountType"
#define KBirthday                @"Birthday"
#define KCreatedOn               @"CreatedOn"
#define KFBUserId                @"FBUserId"
#define KPassword                @"Password"
#define KPhoneNumber             @"PhoneNumber"
#define KUserName                @"UserName"
#define KEmailId                 @"email"

#define kImageUpload             @"imagetoadd"

//--- Services Keys ---//

#define KServiceImage            @"ImageUrl"
#define KServiceName             @"Name"
#define KServiceId               @"Id"
#define KServiceDisplayOrder     @"DisplayOrder"



#endif /* CommonClass_h */
