//
//  AppDelegate.h
//  iBeauty
//
//  Created by App Innovation on 27/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
//#import <Google/SignIn.h>

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <UserNotifications/UserNotifications.h>
#import <CoreLocation/CoreLocation.h>

@import UserNotifications;
@import GoogleSignIn;

@interface AppDelegate : UIResponder <UIApplicationDelegate,GIDSignInDelegate,UNUserNotificationCenterDelegate>

@property (strong, nonatomic) UIWindow *window;

@property(nonatomic,strong) FBSDKLoginManager *loginManager;

@property (strong, nonatomic) UINavigationController *navCGlobal;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

@property (strong, nonatomic) NSTimer *loopTimer;

- (void)saveContext;

+ (AppDelegate *) sharedInstance ;

- (void)mainViewSwitch:(UIViewController *)controller;

@property (nonatomic,strong) NSString *selectedCityName;
@property (nonatomic,assign) BOOL oberverStatus;
@property (nonatomic,strong) NSString *latitude;
@property (nonatomic,strong) NSString *longitude;

@property (nonatomic,assign) BOOL navigateStatus;
@property (nonatomic,strong) NSDictionary *dictInfo;

@property(nonatomic,strong) UITabBarController *tabbarController;
@property (nonatomic,strong) CLLocation *currentLocation;
@property (nonatomic,strong) NSString *currentAddress;
@property (nonatomic,strong) NSString *selectedAddress;

- (void)clearBasket;
- (void)setUpTabViewController;
- (void)setRootViewController:(UIViewController *)controller;

@end

