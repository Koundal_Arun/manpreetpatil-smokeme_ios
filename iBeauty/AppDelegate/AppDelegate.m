//
//  AppDelegate.m
//  iBeauty
//
//  Created by App Innovation on 27/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "AppDelegate.h"
#import "Constants.h"
#import "TSLanguageManager.h"
#import "LanguageManager.h"
#import "CategoryDetailController.h"
#import "NSString+iBeautyString.h"
#import "MyAppointmentViewController.h"

//#import <paytabs-iOS/paytabs_iOS.h>

@interface AppDelegate ()

@property(nonatomic,strong) UIStoryboard *storyboard;
@property (nonatomic,strong) AppDelegate *appDelegate;

@end

@implementation AppDelegate


+ (AppDelegate *) sharedInstance {
    
    return (AppDelegate *)[UIApplication sharedApplication].delegate;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    self.storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    self.selectedCityName = NSLocalizedString(@"Current Location", @"");
    
    NSString *loginStatus =  [[NSUserDefaults standardUserDefaults] valueForKey:KLoginStatus];
    
    // Facebook login startUp
    [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];

    // Google login startUp
    [GIDSignIn sharedInstance].clientID = GOOGLE_CLIENT_ID;
    [GIDSignIn sharedInstance].delegate = self;
    
    if (loginStatus.length>0) {
        MainViewController *mainVC = [self.storyboard instantiateViewControllerWithIdentifier:kMainViewController];
        [self setRootViewController:mainVC];
    }else {
        LoginViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:kLoginController];
        [self setRootViewController:loginVC];
    }

    [self setStatusBarColor];
    [self requestPermissionForLocalNotifications];
    
    return YES;
}

#pragma mark - Register for Local Notifications


- (void)requestPermissionForLocalNotifications {
    
   [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"10.0")) {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        UNAuthorizationOptions options = UNAuthorizationOptionAlert + UNAuthorizationOptionSound +UNAuthorizationOptionBadge+UNAuthorizationOptionCarPlay;
        center.delegate = self;
        [center requestAuthorizationWithOptions:options
                              completionHandler:^(BOOL granted, NSError * _Nullable error) {
                                  if (!granted) {
                                      if (APP_DEBUG_MODE)
                                          NSLog(@"Something went wrong");
                                  }else{
                                      if (APP_DEBUG_MODE)
                                          NSLog(@"Permission Authorized");
                                  }
                              }];
    }
    
}

- (void)setRootViewController:(UIViewController *)controller {

    [LanguageManager setupCurrentLanguage];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    self.navCGlobal = [storyboard instantiateViewControllerWithIdentifier:kNavigationController];
    
    NSString *languageCode =   [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];
    if (languageCode.length<1) {
        [[NSUserDefaults standardUserDefaults] setValue:@"en" forKey:kLMSelectedLanguageKey];
        languageCode = @"en";
    }
    [self.navCGlobal setViewControllers:@[controller]];
    [self.window setRootViewController:self.navCGlobal];

}

- (void)mainViewSwitch:(UIViewController *)controller {
    
    [LanguageManager setupCurrentLanguage];

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    self.navCGlobal = [storyboard instantiateViewControllerWithIdentifier:kNavigationController];
    
    SlideMenuViewController * leftViewController_O = [storyboard instantiateViewControllerWithIdentifier:kMenuViewController];
    MMDrawerController * drawerController;
    
    NSString *languageCode =   [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];
    
    if (languageCode.length<1) {
        [[NSUserDefaults standardUserDefaults] setValue:@"en" forKey:kLMSelectedLanguageKey];
        languageCode = @"en";
    }
    
    if ([languageCode isEqualToString:@"en"]) {
        
        drawerController = [[MMDrawerController alloc]initWithCenterViewController:controller leftDrawerViewController:leftViewController_O];

        [drawerController setMaximumLeftDrawerWidth:280];
        
    }else if ([languageCode isEqualToString:@"ar"]) {
        
        drawerController = [[MMDrawerController alloc]initWithCenterViewController:controller rightDrawerViewController:leftViewController_O];

        [drawerController setMaximumRightDrawerWidth:280];
    }

    [drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeNone];
    [drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    
    [self.navCGlobal setViewControllers:@[drawerController]];
    
    [self.window setRootViewController:self.navCGlobal];
}

- (void)setUpTabViewController {
    
    HomesViewController *homeView = [[HomesViewController alloc]init];
    
    homeView = [self.storyboard instantiateViewControllerWithIdentifier:kHomesViewController];
    homeView.tabBarItem.image = [UIImage imageNamed:@"home_icon"];
    UINavigationController *navigationCHomeView = [[UINavigationController alloc]initWithRootViewController:homeView];
    navigationCHomeView.navigationBarHidden = YES;
    [homeView setTitle:@"Home"];
    
    NearbyViewController *nearByView = [[NearbyViewController alloc]init];
    nearByView = [self.storyboard instantiateViewControllerWithIdentifier:kNearByViewController];
    nearByView.tabBarItem.image = [UIImage imageNamed:@"address"];
    UINavigationController *navigationCNearBy = [[UINavigationController alloc]initWithRootViewController:nearByView];
    navigationCNearBy.navigationBarHidden = YES;
    [nearByView setTitle:@"Nearby"];
    
    OffersViewController *offersView = [[OffersViewController alloc]init];
    offersView = [self.storyboard instantiateViewControllerWithIdentifier:kOffersViewController];
    offersView.tabBarItem.image = [UIImage imageNamed:@"offers"];
    offersView.title = @"Offers";
    UINavigationController *navigationCOffers = [[UINavigationController alloc]initWithRootViewController:offersView];
    navigationCOffers.navigationBarHidden = YES;
    
    MoreViewController *moreView = [[MoreViewController alloc]init];
    moreView = [self.storyboard instantiateViewControllerWithIdentifier:kMoreViewController];
    moreView.tabBarItem.image=[UIImage imageNamed:@"more_icon"];
    UINavigationController *navigationCMoreView = [[UINavigationController alloc]initWithRootViewController:moreView];
    [moreView setTitle:@"More"];
    
    NSArray *viewControllers = [NSArray arrayWithObjects:navigationCHomeView,navigationCNearBy,navigationCOffers,navigationCMoreView,nil];
    
    self.appDelegate.tabbarController = [[UITabBarController alloc]init];
    self.appDelegate.tabbarController.viewControllers = viewControllers;
    
    [[UITabBar appearance] setTintColor:[UIColor iBeautyThemeColor]];
    
    [AppDelegate sharedInstance].window.rootViewController = self.appDelegate.tabbarController;
    
}

#pragma mark - Set Status Bar Color

- (void)setStatusBarColor {
    
//    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
//    self.navCGlobal.navigationBar.barStyle = UIBarStyleBlack;
    
    if (@available(iOS 13.0, *)) {
        
        UIView *statusBar = [[UIView alloc]initWithFrame:[UIApplication sharedApplication].keyWindow.windowScene.statusBarManager.statusBarFrame] ;
        statusBar.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:130/255.0 blue:90/255.0 alpha:1.0];
        [[UIApplication sharedApplication].keyWindow addSubview:statusBar];
        if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
            //[statusBar.layer insertSublayer:[self setGradientColor_Views:statusBar] atIndex:0];
        }
        
    }else {
            UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
            if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
                statusBar.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:130/255.0 blue:90/255.0 alpha:1.0];
                //[statusBar.layer insertSublayer:[self setGradientColor_Views:statusBar] atIndex:0];
            }
    }

}

- (CAGradientLayer*)setGradientColor_Views:(UIView*)sender {
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = sender.bounds;
    gradient.startPoint = CGPointMake(0, 0);
    gradient.endPoint = CGPointMake(1, 0);
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:233.0/255.0 green:83/255.0 blue:158/255.0 alpha:1.0] CGColor],(id)[[UIColor colorWithRed:240/255.0 green:59.0/255.0 blue:53.0/255.0 alpha:1.0] CGColor], nil];
    
    return gradient;
}


#pragma mark - Social Login UIApplication Delegate


- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary *)options {
    
    if ([url.scheme hasPrefix:@"fb"]) {
        if ([[FBSDKApplicationDelegate sharedInstance] application:app
                                                           openURL:url
                                                 sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                        annotation:options[UIApplicationOpenURLOptionsAnnotationKey]
             ]) {
            return YES;
        }
        
    }else {
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        return [[GIDSignIn sharedInstance] handleURL:url];
    }
    return NO;
}

- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error {
    
    // Perform any operations on signed in user here.
    NSString *email = [NSString stringWithFormat:@"%@",[iBeautyUtility trimString:user.profile.email]];
    NSString *userId = [NSString stringWithFormat:@"%@",[iBeautyUtility trimString:user.userID]];
    NSString *idToken = [NSString stringWithFormat:@"%@",[iBeautyUtility trimString:user.authentication.idToken]];
    NSString *fullName = [NSString stringWithFormat:@"%@",[iBeautyUtility trimString:user.profile.name]];
    NSString *givenName = [NSString stringWithFormat:@"%@",[iBeautyUtility trimString:user.profile.givenName]];
    NSString *familyName = [NSString stringWithFormat:@"%@",[iBeautyUtility trimString:user.profile.familyName]];
    NSString *loginType   = @"Google";
    
    NSString *imageURLString = nil;
    if ([GIDSignIn sharedInstance].currentUser.profile.hasImage) {
        
        NSUInteger dimension = round(200 * [[UIScreen mainScreen] scale]);
        NSURL *imageURL = [user.profile imageURLWithDimension:dimension];
        imageURLString = [imageURL absoluteString];
    }
    if (APP_DEBUG_MODE)
        NSLog(@"User details: %@ %@ %@ %@ %@ %@", userId, idToken, fullName, givenName,familyName, email);
    
    NSDictionary *parametersDict   = [NSDictionary dictionaryWithObjectsAndKeys:fullName,KProfileName,imageURLString,KProfileImageUrl,loginType,KLoginType,userId,KLoginIdentifier,email,KEmailId,nil];
    if (![[parametersDict valueForKey:@"Name"] isEqualToString:@"(null)"]) {
        
        if (self.oberverStatus == NO) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ToggleAuthUINotification" object:nil userInfo:parametersDict];
        }else {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ToggleAuthFromRegisterUINotification" object:nil userInfo:parametersDict];
        }
    }
}

- (void)signIn:(GIDSignIn *)signIn didDisconnectWithUser:(GIDGoogleUser *)user withError:(NSError *)error {
    
    // Perform any operations when the user disconnects from app here.
    
    [[GIDSignIn sharedInstance] disconnect];
    
}

#pragma mark - Universal Link User Activity Delegate

- (BOOL) application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(nonnull void (^)(NSArray<id<UIUserActivityRestoring>> * _Nullable))restorationHandler {
    
    if ([userActivity.activityType isEqualToString:NSUserActivityTypeBrowsingWeb]) {
        [self handleRouting:userActivity.webpageURL];
    }
    return YES;
}

- (void)handleRouting:(NSURL *)url {
    
    NSString *businessID = [url lastPathComponent];
    
    NSString *loginStatus = [[NSUserDefaults standardUserDefaults]valueForKey:KLoginStatus];

    if (loginStatus.length > 0) {

        [self getServiceProviderDetailInfo:businessID];
    }else {
        [iBeautyUtility showAlertMessage:NSLocalizedString(@"Please login to proceed", @"")];
    }
}

#pragma mark - Get Provider Detail Info

- (void)getServiceProviderDetailInfo:(NSString*)businessID {
    
    NSString *url = [ConnectionHandler getServiceURL_Products:TYPE_SERVICE_BUSINESS_PROVIDER_INFO];
    
    ConnectionHandler *handler = [ConnectionHandler getSharedInstance];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:KAccessToken];
    NSString *serviceId = businessID;
    NSString *currentDay =  [[NSString string] getWeekDay];
    
    NSDictionary *dictInfo = [NSDictionary dictionaryWithObjectsAndKeys:accessToken,@"Authorization",serviceId,@"BusinessId",self.latitude,@"Latitude",self.longitude,@"Longitude",currentDay,@"DayName",nil];
    
    if (internetStatus != NotReachable) {
        
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", @"")];
        
        [handler jsonPostData:url params:dictInfo onCompletion:^(id completed, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                @try {
                    if (completed) {
                        
                        NSData* data = [completed dataUsingEncoding:NSUTF8StringEncoding];
                        NSMutableArray *tempInfoArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        if (APP_DEBUG_MODE) { NSLog(@"Temp Info array is %@", tempInfoArray); }
                        
                        ServicesCategoriesInfo *serviceCatObj = [ServicesCategoriesInfo getSharedInstance];
                        NSMutableArray *infoArray;
                        
                        if (tempInfoArray.count > 0) {
                            NSDictionary *tempDict = tempInfoArray[0];
                            NSDictionary *dictInfo = [tempDict valueForKey:@"business"];
                            
                            if ([[dictInfo valueForKey:@"RegisterType"] isEqualToString:@"0"]) {
                                infoArray = [serviceCatObj serviceCategoryInfoObject:tempInfoArray];
                            }
                        }
                        
                        if (infoArray.count > 0) {
                            [self switchToBusinessDetailController:infoArray];
                        }
                        
                        if (APP_DEBUG_MODE) { NSLog(@"Provider Info Array is %@", infoArray); }
                        
                    }else {
                        if (APP_DEBUG_MODE) { NSLog(@"error is %@", error.localizedDescription); }
                        [SVProgressHUD dismiss];
                    }
                    
                } @catch (NSException *exception) {
                    [SVProgressHUD dismiss];
                } @finally {
                    
                }
            });
        }];
    }
    else {
        //[iBeautyUtility showAlertMessage:NETWORK_STATUS_INFO];
        NoInternetViewController *noInternetController = [self.storyboard instantiateViewControllerWithIdentifier:kNoInternetViewController];
        noInternetController.modalPresentationStyle = UIModalPresentationFullScreen;
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:noInternetController animated:YES completion:nil];
    }
}

- (void)switchToBusinessDetailController:(NSMutableArray*)infoArray {
    
    ServicesCategoriesInfo *infoObj = infoArray[0];

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CategoryDetailController *categoryController = [storyboard instantiateViewControllerWithIdentifier:kCategoryDetailController];
    categoryController.businessID = infoObj.Id;
    categoryController.mainInfoArray = infoArray;
    
    [self.navCGlobal pushViewController:categoryController animated:YES];

}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"clearLocationInfo" object:nil userInfo:nil];

}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

#pragma mark - Notifications greater than or equal to iOS 10

// trigger when app is in active state
- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler {
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}

// trigger when app is in background state
- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)(void))completionHandler {
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    self.navigateStatus = YES;
    self.dictInfo = response.notification.request.content.userInfo;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MyAppointmentViewController *appointmentController = [storyboard instantiateViewControllerWithIdentifier:kMyAppointmentViewController];
    [self mainViewSwitch:appointmentController];
}

#pragma mark - Clear Basket

- (void)clearBasket {
    
    [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:nil];
    self.loopTimer = [NSTimer scheduledTimerWithTimeInterval:60*30 target:self selector:@selector(clearBasketByRemovingAppointments) userInfo:nil repeats:NO];
    [[NSRunLoop currentRunLoop] addTimer:self.loopTimer forMode:NSRunLoopCommonModes];
}

- (void)clearBasketByRemovingAppointments { //TODO
    
    [self.loopTimer invalidate];
    
    UIViewController *controller = [self visibleViewController];
    
    if (![controller isKindOfClass:[PriceCartViewController class]]) {
        UIApplicationState state = [[UIApplication sharedApplication] applicationState];
        if (state == UIApplicationStateBackground || state == UIApplicationStateInactive) {
            BookAppointmentController *appointmentController = [[BookAppointmentController alloc]init];
            [appointmentController clearBasket_services:YES];
        }else {
            [iBeautyUtility showAlertMessageForClearBasket:NSLocalizedString(@"Session_expired_message", @"")];
        }
    }
    
    //    if (![controller isKindOfClass:[PriceCartViewController class]] || ![controller isKindOfClass:[PTFWInitialSetupViewController class]])
}

- (UIViewController *)visibleViewController {
    UIViewController *rootViewController = self.window.rootViewController;
    return [self getVisibleViewControllerFrom:rootViewController];
}

- (UIViewController *) getVisibleViewControllerFrom:(UIViewController *) vc {
    if ([vc isKindOfClass:[UINavigationController class]]) {
        return [self getVisibleViewControllerFrom:[((UINavigationController *) vc) visibleViewController]];
    } else if ([vc isKindOfClass:[UITabBarController class]]) {
        return [self getVisibleViewControllerFrom:[((UITabBarController *) vc) selectedViewController]];
    } else {
        if (vc.presentedViewController) {
            return [self getVisibleViewControllerFrom:vc.presentedViewController];
        } else {
            return vc;
        }
    }
}

#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"iBeauty"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                    */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}

@end
