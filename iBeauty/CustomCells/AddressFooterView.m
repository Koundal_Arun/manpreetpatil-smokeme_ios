//
//  AddressFooterView.m
//  iBeauty
//
//  Created by App Innovation on 18/01/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import "AddressFooterView.h"
#import "UILabel+FlexibleWidHeight.h"
#import "NSString+iBeautyString.h"

@interface AddressFooterView ()

@property (nonatomic,weak) IBOutlet UILabel *nameLabel;
@property (nonatomic,weak) IBOutlet UILabel *addressLabel;
@property (nonatomic,weak) IBOutlet UILabel *phoneNumberLabel;
@property (nonatomic,weak) IBOutlet UIView *bkgView;

@end

@implementation AddressFooterView

- (void)configureAddressFooterView:(AddressModel*)addressObj {
    
    self.nameLabel.text = addressObj.name;
    self.addressLabel.text = addressObj.address;
    NSString *phoneNoStr = addressObj.contactNumber;
    NSMutableAttributedString *phoneNoString = [phoneNoStr getOrdersAttributedTextWithId:@"Phone no - " openingStatus:phoneNoStr screenType:3];
    self.phoneNumberLabel.attributedText = phoneNoString;

    self.addressLabel.translatesAutoresizingMaskIntoConstraints = YES;
    self.bkgView.translatesAutoresizingMaskIntoConstraints = YES;
    self.phoneNumberLabel.translatesAutoresizingMaskIntoConstraints = YES;

    CGFloat addressWidth = [UIScreen mainScreen].bounds.size.width- 33.0f;
    CGFloat height = [UILabel heightOfTextForString:addressObj.address andFont:[UIFont fontWithName:@"SegoeUI" size:15.0] maxSize:CGSizeMake(addressWidth, MAXFLOAT)];
    
    if (addressObj.name.length > 0) {
        self.nameLabel.hidden = NO;
        self.addressLabel.frame = CGRectMake(self.addressLabel.frame.origin.x, 41.0f, addressWidth, height);
        self.phoneNumberLabel.frame = CGRectMake(self.phoneNumberLabel.frame.origin.x, 41.0f + height + 8.0f, addressWidth, 21.0f);
        self.bkgView.frame = CGRectMake(self.bkgView.frame.origin.x, self.bkgView.frame.origin.y, [UIScreen mainScreen].bounds.size.width-16.0f, 10.0f + self.nameLabel.frame.size.height + 10.0f + height + 8.0f + self.phoneNumberLabel.frame.size.height + 10.0f);
    }else {
        self.nameLabel.hidden = YES;
        self.addressLabel.frame = CGRectMake(self.addressLabel.frame.origin.x, 10.0f, addressWidth, height);
        self.phoneNumberLabel.frame = CGRectMake(self.phoneNumberLabel.frame.origin.x, 10.0f + height + 8.0f, addressWidth, 21.0f);
        self.bkgView.frame = CGRectMake(self.bkgView.frame.origin.x, self.bkgView.frame.origin.y, [UIScreen mainScreen].bounds.size.width-16.0f, 10.0f + height + 8.0f + self.phoneNumberLabel.frame.size.height + 10.0f);
    }
    
}

- (void)configureAddressFooterView_PickUp:(ServicesCategoriesInfo*)productSellerInfo {
    
    self.addressLabel.translatesAutoresizingMaskIntoConstraints = YES;
    self.bkgView.translatesAutoresizingMaskIntoConstraints = YES;
    
    self.nameLabel.text = productSellerInfo.categoryName;
    self.addressLabel.text = productSellerInfo.location;
    
    CGFloat addressWidth = [UIScreen mainScreen].bounds.size.width- 33.0f;
    CGFloat height = [UILabel heightOfTextForString:productSellerInfo.location andFont:[UIFont fontWithName:@"SegoeUI" size:15.0] maxSize:CGSizeMake(addressWidth, MAXFLOAT)];
    self.addressLabel.frame = CGRectMake(self.addressLabel.frame.origin.x, 8.0f + self.nameLabel.frame.size.height + 10.0f, self.addressLabel.frame.size.width, height);
    self.bkgView.frame = CGRectMake(self.bkgView.frame.origin.x, self.bkgView.frame.origin.y, [UIScreen mainScreen].bounds.size.width-16.0f, 10.0f + self.nameLabel.frame.size.height + 10.0f + height + 10.0f);
}


@end
