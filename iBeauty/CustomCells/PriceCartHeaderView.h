//
//  PriceCartHeaderView.h
//  iBeauty
//
//  Created by App Innovation on 18/01/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasketPriceSummary.h"

NS_ASSUME_NONNULL_BEGIN

@interface PriceCartHeaderView : UICollectionReusableView

- (void)configureHeaderViewWithTitle:(BasketPriceSummary*)basketPriceInfo;

- (void)configureHeaderViewWithTitle_products:(BasketPriceSummary*)basketPriceInfo;

@end

NS_ASSUME_NONNULL_END
