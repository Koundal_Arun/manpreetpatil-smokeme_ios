//
//  ProductSellerCell.h
//  iBeauty
//
//  Created by App Innovation on 23/01/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServicesCategoriesInfo.h"
#import "Constants.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProductSellerCell : UICollectionViewCell

- (void)configureProductSellerCell:(ServicesCategoriesInfo*)infoObj;


@end

NS_ASSUME_NONNULL_END
