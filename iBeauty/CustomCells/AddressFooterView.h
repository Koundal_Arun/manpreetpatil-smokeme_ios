//
//  AddressFooterView.h
//  iBeauty
//
//  Created by App Innovation on 18/01/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddressModel.h"
#import "ServicesCategoriesInfo.h"

NS_ASSUME_NONNULL_BEGIN

@interface AddressFooterView : UICollectionReusableView

- (void)configureAddressFooterView:(AddressModel*)addressObj;

- (void)configureAddressFooterView_PickUp:(ServicesCategoriesInfo*)productSellerInfo;

@end

NS_ASSUME_NONNULL_END
