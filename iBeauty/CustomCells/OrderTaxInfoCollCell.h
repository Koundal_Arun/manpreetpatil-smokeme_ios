//
//  OrderTaxInfoCollCell.h
//  iBeauty
//
//  Created by App Innovation on 18/01/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TaxInfoModel.h"


NS_ASSUME_NONNULL_BEGIN

@interface OrderTaxInfoCollCell : UICollectionViewCell


- (void)configureTaxInfoCellWithInfo:(TaxInfoModel*)infoObj;


@end

NS_ASSUME_NONNULL_END
