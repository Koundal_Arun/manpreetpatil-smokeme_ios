//
//  AppointmentDetailCell.m
//  iBeauty
//
//  Created by App Innovation on 13/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "AppointmentDetailCell.h"
#import "UIImageView+WebCache.h"
#import "NSDate+iBeautyDateFormatter.h"
#import "NSString+iBeautyString.h"

@interface AppointmentDetailCell ()

@property (nonatomic,weak) IBOutlet UIImageView *serviceImageView;
@property (nonatomic,weak) IBOutlet UILabel *serviceName;
@property (nonatomic,weak) IBOutlet UILabel *servicePrice;
@property (nonatomic,weak) IBOutlet UILabel *serviceStartDate;
@property (nonatomic,weak) IBOutlet UILabel *staffName;
@property (nonatomic,weak) IBOutlet UIView *servicesBkgView;

@end

@implementation AppointmentDetailCell

- (void)configureAppointmentDetailCell:(AllAppointmentsInfo*)appointmentDetail mainAppointmentObj:(AllAppointmentsInfo*)appointmentObj {
    
    ServicesDetail *appointmentsInfo = appointmentDetail.serviceDetail;
    
    NSMutableArray *staffArray =  appointmentDetail.staffArray;
    StaffDetail *staffInfo;
    if (staffArray.count>0) {
        staffInfo = staffArray[0];
    }
    
    [self.serviceImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",IMAGE_BASE_URL, appointmentsInfo.serviceImage]] placeholderImage:[UIImage imageNamed:@"image3.png"]];
    
    self.serviceName.text = [NSString stringWithFormat:@"%@ (%@ min)",appointmentDetail.serviceName,appointmentsInfo.serviceTime];
    

    if (appointmentsInfo.salesPrice.length > 0) {
        
        self.servicePrice.text = [NSString stringWithFormat:@"$%.2f",[appointmentsInfo.salesPrice floatValue]];

     /*   NSString *totalPriceString = [NSString stringWithFormat:@"$%@",appointmentsInfo.serviceAmount];
        NSString *finalDiscountString = [NSString stringWithFormat:@"%@ $%.2f",totalPriceString,[appointmentsInfo.salesPrice floatValue]];
        
        NSRange range = [finalDiscountString rangeOfString:totalPriceString];
        
        NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:finalDiscountString];
        [attributeString addAttribute:NSStrikethroughStyleAttributeName value:@2 range:NSMakeRange(range.location, [totalPriceString length])];
        self.servicePrice.attributedText = attributeString; */
        
    }else {
        self.servicePrice.text = [NSString stringWithFormat:@"$%.2f",[appointmentsInfo.serviceAmount floatValue]];
    }
    
    NSDate *orderDate = [NSDate convertNSStringToNSDate:appointmentDetail.startTimeAppointment format:DATE_FORMAT_APPOINTMENT_DETAIL];
    NSString *dateString = [orderDate convertDateValueToNSString:APPOINTMENT_DATE_FORMAT localeStatus:false];
    NSString *timeString = [orderDate convertDateValueToNSString:SETTING_TIME_FORMAT_12 localeStatus:false];
    self.serviceStartDate.text = [NSString stringWithFormat:@"%@ @%@",dateString,timeString];
    
    self.servicesBkgView.translatesAutoresizingMaskIntoConstraints = YES;
    self.staffName.hidden = YES;
    self.servicesBkgView.frame = CGRectMake(10, 5.0f, [UIScreen mainScreen].bounds.size.width - 20.0f, 72.0f);
    
    if (staffInfo) {
        self.staffName.hidden = NO;
        NSString *localizedString = NSLocalizedString(@"With", @"");
        NSMutableAttributedString *staffName = [dateString getAttributedTextWithTimeString:localizedString openingStatus:staffInfo.staffName];
        self.staffName.attributedText = staffName;
        self.servicesBkgView.frame = CGRectMake(10, 5.0f, [UIScreen mainScreen].bounds.size.width - 20.0f, 97.0f);
    }
    [self setUpViews:appointmentObj];
    [iBeautyUtility setimageViewRound:self.serviceImageView];
    [self setUpCancelViewAccordingToDate:appointmentDetail mainAppointmentObj:appointmentObj];
}

- (void)setUpCancelViewAccordingToDate:(AllAppointmentsInfo*)appointmentDetail mainAppointmentObj:(AllAppointmentsInfo*)appointmentObj {
    
    if (appointmentObj.isOfferApplied == YES) {
        [self.cancelBkgView setHidden:YES];
    }else {
        NSComparisonResult result = [[NSDate date] compare:appointmentDetail.startDate];
        if (result == NSOrderedAscending) {
            [self.cancelBkgView setHidden:NO];
        }else {
            [self.cancelBkgView setHidden:YES];
        }
        [self setUpAppointmentCancelStatus:appointmentDetail];
    }
}

- (void)setUpViews:(AllAppointmentsInfo*)appointmentObj {
    
    NSString *languageCode =   [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];
    CGFloat xAxis = 0.0f;
    if ([languageCode isEqualToString:@"en"]) {
        xAxis = self.servicesBkgView.frame.size.width - 110.0f;
        self.servicePrice.textAlignment = NSTextAlignmentRight;
    }else {
        xAxis = 10.0f;
        self.servicePrice.textAlignment = NSTextAlignmentLeft;
    }
    self.servicePrice.translatesAutoresizingMaskIntoConstraints = YES;
    
    if (appointmentObj.isOfferApplied == NO) {
        
        self.servicePrice.frame = CGRectMake(xAxis, 92-40.0f, self.servicePrice.frame.size.width, self.servicePrice.frame.size.height);
        self.servicePrice.hidden = NO;
    }else {
        self.servicePrice.frame = CGRectMake(xAxis,self.servicesBkgView.frame.size.height/2 - self.servicePrice.frame.size.height/2, self.servicePrice.frame.size.width, self.servicePrice.frame.size.height);
        self.servicePrice.hidden = YES;
    }
    
}

- (void)setUpAppointmentCancelStatus:(AllAppointmentsInfo*)appointmentObj {
    
    [iBeautyUtility setCancelViewBorderRound:self.cancelBkgView];
    [iBeautyUtility setBookNowButtonCorderRound:self.cancelButton];
    //[self.cancelButton.layer insertSublayer:[iBeautyUtility setGradientColor_Buttons:self.cancelButton] atIndex:0];

    if (appointmentObj.status == YES) {
        [self.cancelButton setTitle:NSLocalizedString(@"Cancel", @"") forState:UIControlStateNormal];
        self.cancelButton.userInteractionEnabled = YES;
    }else {
        self.cancelButton.userInteractionEnabled = NO;
        [self.cancelButton setBackgroundColor:[UIColor cancelAppointmentThemeColor]];
        [self.cancelButton setTitle:NSLocalizedString(@"Cancelled", @"") forState:UIControlStateNormal];
        [self.cancelButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        self.cancelBkgView.layer.shadowOpacity = 0.0f;
    }
}

@end
