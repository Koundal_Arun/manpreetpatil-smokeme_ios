//
//  CategoryCollectionCell.h
//  iBeauty
//
//  Created by App Innovation on 28/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "ServicesCategoriesInfo.h"

@interface CategoryCollectionCell : UICollectionViewCell

- (void)configureCollectionCell:(ServicesCategoriesInfo*)catInfoObj;

@property (nonatomic,weak ) IBOutlet UIButton * indexButton;

@end
