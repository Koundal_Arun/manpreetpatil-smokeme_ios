//
//  OrderTaxInfoCollCell.m
//  iBeauty
//
//  Created by App Innovation on 18/01/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import "OrderTaxInfoCollCell.h"
#import "Constants.h"

@interface OrderTaxInfoCollCell ()

@property (nonatomic,weak) IBOutlet UILabel *taxTitle;
@property (nonatomic,weak) IBOutlet UILabel *taxValue;
@property (nonatomic,weak) IBOutlet UIView  *bkgView;

@end

@implementation OrderTaxInfoCollCell

- (void)configureTaxInfoCellWithInfo:(TaxInfoModel*)infoObj {
    
    self.taxTitle.text = [NSString stringWithFormat:@"%@ (%.f%%) :",infoObj.taxName,infoObj.taxPercentage];
    self.taxValue.text = [NSString stringWithFormat:@"$%.2f",infoObj.taxAmount];
    
    NSString *languageCode =   [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];
    
    if ([languageCode isEqualToString:@"en"]) {
        self.taxTitle.textAlignment = NSTextAlignmentLeft;
        self.taxValue.textAlignment = NSTextAlignmentRight;

    }else if ([languageCode isEqualToString:@"ar"]) {
        self.taxTitle.textAlignment = NSTextAlignmentRight;
        self.taxValue.textAlignment = NSTextAlignmentLeft;
    }

}


@end
