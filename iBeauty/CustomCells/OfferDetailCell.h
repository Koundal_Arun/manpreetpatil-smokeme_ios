//
//  OfferDetailCell.h
//  iBeauty
//
//  Created by App Innovation on 11/09/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServicesDetail.h"
#import "OffersDetail.h"

@interface OfferDetailCell : UICollectionViewCell

@property (nonatomic,weak) IBOutlet UIButton *decreaseButton;
@property (nonatomic,weak) IBOutlet UIButton *increaseButton;

- (void)configureOfferDetailCell:(ServicesDetail*)infoObj offerInfo:(OffersDetail*)offerInfo;

@end
