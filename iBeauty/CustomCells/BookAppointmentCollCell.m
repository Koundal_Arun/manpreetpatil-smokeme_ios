//
//  BookAppointmentCollCell.m
//  iBeauty
//
//  Created by App Innovation on 03/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "BookAppointmentCollCell.h"
#import "ServicesDetail.h"
#import "UIImageView+WebCache.h"
#import "ServiceTimeSlot.h"
#import "NSDate+iBeautyDateFormatter.h"
#import "UILabel+FlexibleWidHeight.h"

@interface BookAppointmentCollCell ()

@property (nonatomic,strong) StaffDetail *staffInfo;

@end

@implementation BookAppointmentCollCell

- (void)configureCollectionCell:(BasketInfoObj*)basketObj screenStatus:(BOOL)screenStatus {
    
    ServicesDetail *detailObj = basketObj.serviceInfoObj;
    if (screenStatus == YES) {
        self.serviceDeleteView.hidden = YES;
        [self.priceLabel setHidden:YES];
    }else {
        self.serviceDeleteView.hidden = NO;
        [self.priceLabel setHidden:NO];
    }
    
    self.dateTimeView.translatesAutoresizingMaskIntoConstraints = YES;
    self.staffView.translatesAutoresizingMaskIntoConstraints = YES;
    self.staffDeleteView.translatesAutoresizingMaskIntoConstraints = YES;
    self.bkgView.translatesAutoresizingMaskIntoConstraints = YES;
    self.anyStaffLabel.translatesAutoresizingMaskIntoConstraints = YES;
    self.dateTimeLabel.translatesAutoresizingMaskIntoConstraints = YES;
    self.staffImageView.translatesAutoresizingMaskIntoConstraints = YES;
    self.dateTimeImageView.translatesAutoresizingMaskIntoConstraints = YES;
    self.staffButton.translatesAutoresizingMaskIntoConstraints = YES;

    [self.serviceImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",IMAGE_BASE_URL,detailObj.serviceImage]] placeholderImage:[UIImage imageNamed:@""]];
    [iBeautyUtility setimageViewRound:self.serviceImageView];
    
    self.serviceNameLabel.text = [NSString stringWithFormat:@"%@ (%@ min)",detailObj.categoryName,detailObj.serviceTime];

    [self setUpPriceInfoView:detailObj];
    
    if (basketObj.staffInfo) {
        
        self.staffInfo = basketObj.staffInfo;
        
        NSLog(@"staff name is %@",self.staffInfo.staffName);
        if (self.staffInfo.staffImage.length>0) {
            
            [self.staffImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",IMAGE_BASE_URL,self.staffInfo.staffImage]] placeholderImage:[UIImage imageNamed:@"any_staff"]];
        }
        self.anyStaffLabel.text = self.staffInfo.staffName;
    }else {
        self.anyStaffLabel.text = NSLocalizedString(@"Any staff", @"");
        [self.staffImageView setImage: [UIImage imageNamed:@"any_staff"]];
    }
    [self setUpSlotInfo:basketObj];
}

- (void)setUpPriceInfoView:(ServicesDetail*)detailObj {
    
    NSString *discountPrice = @"";
    
    if (detailObj.salesPrice.length > 0) {
        
        discountPrice = detailObj.salesPrice;
        discountPrice = [NSString stringWithFormat:@"$ %@",discountPrice];
        
        NSString *totalPriceString = [NSString stringWithFormat:@"($%@)",detailObj.serviceAmount];
        NSString *finalDiscountString = [NSString stringWithFormat:@"%@ %@",discountPrice,totalPriceString];
        
        NSRange range = [finalDiscountString rangeOfString:totalPriceString];
        
        UIFont *boldFont = [UIFont fontWithName:@"SegoeUI" size:15.0];
        
        NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:finalDiscountString];
        [attributeString setAttributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor],NSFontAttributeName:boldFont} range:range];
        [attributeString addAttribute:NSStrikethroughStyleAttributeName value:@2 range:NSMakeRange(range.location, [totalPriceString length])];
        self.priceLabel.attributedText = attributeString;
    }else {
        discountPrice = detailObj.serviceAmount;
        discountPrice = [NSString stringWithFormat:@"$ %@",discountPrice];
        self.priceLabel.text = discountPrice;
    }
    
}


- (void)setUpSlotInfo:(BasketInfoObj*)detailObj {
    
    if (detailObj.slotInfObj) {
        ServiceTimeSlot *slotInfo = detailObj.slotInfObj;
        NSString *dateString = [slotInfo.selectedDate convertDateValueToNSString:SLOT_DATE_FORMAT localeStatus:false];
        self.dateTimeLabel.text = [NSString stringWithFormat:@"%@ at %@",dateString,slotInfo.startTime];

    }else {
        self.dateTimeLabel.text = NSLocalizedString(@"Date & time", @"");
    }
    [self setUpViewForDateTimeAndStaff:self.dateTimeLabel.text detailObj:self.staffInfo];

}

- (void)setUpViewForDateTimeAndStaff:(NSString*)selectedDateString detailObj:(StaffDetail*)detailObj {
    
    CGFloat containerViewWidth = [UIScreen mainScreen].bounds.size.width;
    containerViewWidth = containerViewWidth - 16;
    
    CGFloat dateStringWidth = [UILabel widthOfTextForString:selectedDateString andFont:[UIFont fontWithName:@"SegoeUI" size:13.0] maxSize:(CGSize){MAXFLOAT,21 }];
    
    CGFloat dateViewWidth = 7 + self.dateTimeImageView.frame.size.width+5+dateStringWidth+7;
    
    [self.dateTimeView setFrame:CGRectMake(containerViewWidth-dateViewWidth-10, self.dateTimeView.frame.origin.y, dateViewWidth, self.dateTimeView.frame.size.height)];
    
    [self.dateTimeImageView setFrame:CGRectMake(7.0f, self.dateTimeImageView.frame.origin.y, self.dateTimeImageView.frame.size.width, self.dateTimeImageView.frame.size.height)];
    
    [self.dateTimeLabel setFrame:CGRectMake(7.0f+self.dateTimeImageView.frame.size.width+5.0f, self.dateTimeLabel.frame.origin.y, dateStringWidth, self.dateTimeLabel.frame.size.height)];

    
    CGFloat staffStringWidth = [UILabel widthOfTextForString:self.anyStaffLabel.text andFont:[UIFont fontWithName:@"SegoeUI" size:13.0] maxSize:(CGSize){MAXFLOAT,21 }];

    CGFloat staffViewWidth = 0;
    
    [self.staffImageView setFrame:CGRectMake(7.0f, self.staffImageView.frame.origin.y, 20.0f, 20.0f)];
    
    self.anyStaffLabel.numberOfLines = 1;

    [self.anyStaffLabel setFrame:CGRectMake(7.0f+self.staffImageView.frame.size.width+5.0f, self.anyStaffLabel.frame.origin.y, staffStringWidth, self.anyStaffLabel.frame.size.height)];
    
    if (detailObj) {
        
        staffViewWidth = 7 + self.staffImageView.frame.size.width+5+staffStringWidth+36;
        
        self.staffDeleteView.hidden = NO;
        [self.staffDeleteView setFrame:CGRectMake(7.0f+self.staffImageView.frame.size.width+5.0f+self.anyStaffLabel.frame.size.width+1.0f, 0.0f, 35.0f, 35.0f)];
    }else {
        self.staffDeleteView.hidden = YES;
        staffViewWidth = 7 + self.staffImageView.frame.size.width+5+staffStringWidth+7;
    }
    
    CGFloat estimatedWidth = containerViewWidth-dateViewWidth-30;
    CGFloat difference = estimatedWidth - staffViewWidth;
    if (estimatedWidth < staffViewWidth) {

        self.anyStaffLabel.numberOfLines = 1;
        [self.anyStaffLabel setFrame:CGRectMake(7.0f+self.staffImageView.frame.size.width+5.0f, 8.0f, staffStringWidth +difference, 21.0f)];
        [self.staffView setFrame:CGRectMake(10, self.staffView.frame.origin.y, staffViewWidth + difference, 35.0f)];

        [self.staffDeleteView setFrame:CGRectMake(7.0f+self.staffImageView.frame.size.width+5.0f+self.anyStaffLabel.frame.size.width+1.0f, 0.0f, 35.0f, 35.0f)];
        
        if (IS_IPHONE_5) {
            [self.dateTimeView setFrame:CGRectMake(containerViewWidth-dateViewWidth-10+50, self.dateTimeView.frame.origin.y, dateViewWidth-50, self.dateTimeView.frame.size.height)];
            
            [self.dateTimeLabel setFrame:CGRectMake(7.0f+self.dateTimeImageView.frame.size.width+5.0f, self.dateTimeLabel.frame.origin.y, dateStringWidth-50, self.dateTimeLabel.frame.size.height)];
            
            [self.anyStaffLabel setFrame:CGRectMake(7.0f+self.staffImageView.frame.size.width+5.0f, 8.0f, staffStringWidth +difference+50.0f, 21.0f)];
            [self.staffDeleteView setFrame:CGRectMake(7.0f+self.staffImageView.frame.size.width+5.0f+self.anyStaffLabel.frame.size.width+1.0f, 0.0f, 35.0f, 35.0f)];
            
            [self.staffView setFrame:CGRectMake(10, self.staffView.frame.origin.y, staffViewWidth + difference + 50.0f, 35.0f)];

        }

        [self.bkgView setFrame:CGRectMake(8.0f, self.bkgView.frame.origin.y, containerViewWidth, 152.0f)];

    }else {
    
        [self.anyStaffLabel setFrame:CGRectMake(7.0f+self.staffImageView.frame.size.width+5.0f, 8.0f, staffStringWidth, 21.0f)];
        self.anyStaffLabel.numberOfLines = 1;
        [self.staffView setFrame:CGRectMake(containerViewWidth-dateViewWidth-10-staffViewWidth-10, self.staffView.frame.origin.y, staffViewWidth, 35.0f)];
        [self.bkgView setFrame:CGRectMake(8.0f, self.bkgView.frame.origin.y, containerViewWidth, 152.0f)];

    }
    [self.staffButton setFrame:CGRectMake(7.0f, self.staffButton.frame.origin.y, self.staffImageView.frame.size.width+5.0f+self.anyStaffLabel.frame.size.width,self.staffButton.frame.size.height)];

}

@end
