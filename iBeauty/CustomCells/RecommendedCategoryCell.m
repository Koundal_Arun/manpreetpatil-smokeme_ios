//
//  RecommendedCategoryCell.m
//  iBeauty
//
//  Created by App Innovation on 28/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "RecommendedCategoryCell.h"
#import "Constants.h"
#import "CategoryCollectionCell.h"
#import "ServicesCategoriesInfo.h"

static NSString *categoryCellIdentifier = @"CategoryCollectionCell";

@interface RecommendedCategoryCell()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (strong, nonatomic) NSMutableArray *collectionData;

@end

@implementation RecommendedCategoryCell

- (void)awakeFromNib {
    
    [super awakeFromNib];
    // Initialization code

}

- (void)configureCellWithInfo:(NSMutableArray*)categoriesInfo {
    
    
    if (APP_DEBUG_MODE)
        NSLog(@"Arrar is %@",categoriesInfo);
    
    _collectionData = categoriesInfo ;
    
    [self.collectionView_Categories reloadData];
}


#pragma mark - UICollectionViewDataSource methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.collectionData.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CategoryCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:categoryCellIdentifier forIndexPath:indexPath];
    ServicesCategoriesInfo *infoObj = self.collectionData[indexPath.row];
    [cell configureCollectionCell:infoObj];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat screenWidth = CGRectGetWidth([UIScreen mainScreen].bounds);
    
    CGFloat itemWidth = 0.0f;
    CGFloat itemHeight = 0.0f;

    NSString *deviceType = [UIDevice currentDevice].model;
    
    if ([deviceType isEqualToString:@"iPhone"]) {
        itemWidth = floor(screenWidth -100.0f);
        itemHeight = 261.0f;
    }else {
        itemWidth = floor(screenWidth -400.0f);
        itemHeight = 360.0f;
    }
    
    return CGSizeMake(itemWidth, itemHeight);
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 5.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 5.0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(5.0, 5.0, 5.0, 5.0);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
      if ( (self.delegateObj != nil) && ([self.delegateObj respondsToSelector:@selector(delMethod_didSelectCollection:section:)]) ) {
        [self.delegateObj delMethod_didSelectCollection:indexPath.item section:collectionView.tag];
        
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
