//
//  PaymentOptionsCell.h
//  iBeauty
//
//  Created by App Innovation on 31/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaymentOptions.h"

@interface PaymentOptionsCell : UICollectionViewCell

@property (nonatomic,weak) IBOutlet UIImageView *radioBtnImageView;
@property (nonatomic,weak) IBOutlet UILabel *paymentOptionLabel;
@property (nonatomic,weak) IBOutlet UIImageView *currencyIcon;


- (void)configurePaymentOptionCell:(PaymentOptions*)paymentMethodInfo index:(NSInteger)indexValue pickUpStatus:(NSInteger)pickStatus;

@end
