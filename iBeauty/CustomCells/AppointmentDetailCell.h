//
//  AppointmentDetailCell.h
//  iBeauty
//
//  Created by App Innovation on 13/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AllAppointmentsInfo.h"
#import "ServicesDetail.h"

@interface AppointmentDetailCell : UICollectionViewCell

@property (nonatomic,weak) IBOutlet UIButton *cancelButton;
@property (nonatomic,weak) IBOutlet UIView *cancelBkgView;

- (void)configureAppointmentDetailCell:(AllAppointmentsInfo*)appointmentDetail mainAppointmentObj:(AllAppointmentsInfo*)appointmentObj;

@end
