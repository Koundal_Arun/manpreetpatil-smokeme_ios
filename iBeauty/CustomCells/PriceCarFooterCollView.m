//
//  PriceCarFooterCollView.m
//  iBeauty
//
//  Created by App Innovation on 31/01/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import "PriceCarFooterCollView.h"

@interface PriceCarFooterCollView ()

@property (weak, nonatomic) IBOutlet UILabel *totalLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalValue;

@property (weak, nonatomic) IBOutlet UILabel *noteLabel;
@property (weak, nonatomic) IBOutlet UILabel *minimumOrderLabel;

@property (weak, nonatomic) IBOutlet UIView  *bkgView;

@end

@implementation PriceCarFooterCollView

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)configureFooterViewWithTitle:(BasketPriceSummary*)basketPriceInfo {
    
    
    self.totalLabel.text = [NSString stringWithFormat:@"%@ :",NSLocalizedString(@"Total", @"")];
    self.totalValue.text = [NSString stringWithFormat:@"$%.2f",[basketPriceInfo.totalPrice floatValue]];
    
    NSString *noteString = NSLocalizedString(@"Price including all taxes.", @"");
    self.noteLabel.text = [NSString stringWithFormat:@"* %@",noteString];
    
    self.minimumOrderLabel.hidden = YES;
    
    NSString *minimumOrderString = NSLocalizedString(@"Minimum order value for delivery : $30", @"");
    self.minimumOrderLabel.text = [NSString stringWithFormat:@"* %@",minimumOrderString];
    
    [self setTextAlignmentAccordingToLanguage];
    
}

- (void)configureFooterViewWithTitle_products:(BasketPriceSummary*)basketPriceInfo {
    
   self.totalLabel.text = [NSString stringWithFormat:@"%@ :",NSLocalizedString(@"Total", @"")];
    self.totalValue.text = [NSString stringWithFormat:@"$%.2f",[basketPriceInfo.totalPrice floatValue]];
    
    NSString *noteString = NSLocalizedString(@"Price including all taxes.", @"");
    self.noteLabel.text = [NSString stringWithFormat:@"* %@",noteString];
    
    NSString *minimumOrderString = NSLocalizedString(@"Minimum order value for delivery : $30", @"");
    self.minimumOrderLabel.text = [NSString stringWithFormat:@"* %@",minimumOrderString];
    
    [self setTextAlignmentAccordingToLanguage];
    
}

- (void)setTextAlignmentAccordingToLanguage {
    
    NSString *languageCode =   [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];
    if ([languageCode isEqualToString:@"en"]) {
        self.noteLabel.textAlignment = NSTextAlignmentRight;
        self.minimumOrderLabel.textAlignment = NSTextAlignmentRight;
        self.totalValue.textAlignment = NSTextAlignmentRight;
    }else {
        self.noteLabel.textAlignment = NSTextAlignmentLeft;
        self.minimumOrderLabel.textAlignment = NSTextAlignmentLeft;
        self.totalValue.textAlignment = NSTextAlignmentLeft;
    }
}

@end
