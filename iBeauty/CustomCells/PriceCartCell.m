//
//  PriceCartCell.m
//  iBeauty
//
//  Created by App Innovation on 09/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "PriceCartCell.h"

@interface PriceCartCell ()

@property (weak, nonatomic) IBOutlet UIImageView *serviceImageView;
@property (weak, nonatomic) IBOutlet UILabel *serviceNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIView  *bkgView;

@end

@implementation PriceCartCell

-(void)configurePriceCartCellWithPriceInfo:(ServicesDetail*)serviceInfo screenStatus:(BOOL)screenStatus {
    
    [self.serviceImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",IMAGE_BASE_URL,serviceInfo.serviceImage]] placeholderImage:[UIImage imageNamed:@""]];
    
    self.serviceNameLabel.text = [NSString stringWithFormat:@"%@ (%@ min)",serviceInfo.categoryName,serviceInfo.serviceTime];
    
    if (screenStatus == YES) {
        self.priceLabel.hidden = YES;
    }else {
        self.priceLabel.hidden = NO;
        [self setUpPriceInfoView:serviceInfo];
    }
    
}

- (void)setUpPriceInfoView:(ServicesDetail*)detailObj {
    
    NSString *discountPrice = @"";
    
    
    if (detailObj.salesPrice.length > 0) {
        
        discountPrice = detailObj.salesPrice;
        discountPrice = [NSString stringWithFormat:@"$ %@",discountPrice];
        
        NSString *totalPriceString = [NSString stringWithFormat:@"($%@)",detailObj.serviceAmount];
        NSString *finalDiscountString = [NSString stringWithFormat:@"%@ %@",totalPriceString,discountPrice];
        
        NSRange range = [finalDiscountString rangeOfString:totalPriceString];
        
        UIFont *boldFont = [UIFont fontWithName:@"SegoeUI" size:15.0];
        
        NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:finalDiscountString];
        [attributeString setAttributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor],NSFontAttributeName:boldFont} range:range];
        [attributeString addAttribute:NSStrikethroughStyleAttributeName value:@2 range:NSMakeRange(range.location, [totalPriceString length])];
        self.priceLabel.attributedText = attributeString;
    }else {
        discountPrice = detailObj.serviceAmount;
        discountPrice = [NSString stringWithFormat:@"$ %@",discountPrice];
        self.priceLabel.text = discountPrice;
    }
    
}


@end
