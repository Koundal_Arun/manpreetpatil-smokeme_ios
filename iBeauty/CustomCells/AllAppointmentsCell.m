//
//  AllAppointmentsCell.m
//  iBeauty
//
//  Created by App Innovation on 12/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "AllAppointmentsCell.h"
#import "Constants.h"
#import "AppointmentCollectionCell.h"
#import "Constants.h"
#import "NSDate+iBeautyDateFormatter.h"
#import "ProviderImages.h"

static NSString *appointmentCellIdentifier = @"AppointmentCollectionCell";

@interface AllAppointmentsCell()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (strong, nonatomic) NSMutableArray *collectionData;
@property (weak, nonatomic) IBOutlet UIImageView *servicePImageView;
@property (weak, nonatomic) IBOutlet UILabel *providerName;
@property (weak, nonatomic) IBOutlet UILabel *dateTime;
@property (weak, nonatomic) IBOutlet UILabel *bookingId;
@property (weak, nonatomic) IBOutlet UILabel *totalPrice;
@property (weak, nonatomic) IBOutlet UILabel *cancelledLabel;
@property (weak, nonatomic) IBOutlet UIView  *cancelledBkgView;
@property (weak, nonatomic) IBOutlet UIView  *cancelContainerView;


@end

@implementation AllAppointmentsCell


- (void)configureCellWithInfo:(AllAppointmentsInfo*)appointmentsInfo {
    
    if (APP_DEBUG_MODE)
        NSLog(@"Arrar is %@",appointmentsInfo.appointmentsInfo);
    
    [self setUpAppointmentCell:appointmentsInfo];
    
    self.collectionData = [self filterOutDuplicateValues:appointmentsInfo.appointmentsInfo] ;
    
    [self.collectionView_Categories reloadData];
}

- (void)setUpAppointmentCell:(AllAppointmentsInfo*)appointmentsInfo {
    
    [iBeautyUtility setLabelRound:self.dateTime];
    
    [self.servicePImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",IMAGE_BASE_URL,appointmentsInfo.businessThumb]] placeholderImage:[UIImage imageNamed:@""]];
    self.providerName.text = appointmentsInfo.providerName;
    
//    NSDate *orderDate = [NSDate convertNSStringToNSDate:appointmentsInfo.orderDateTime format:UTC_DATE_FORMAT];
//    NSString *dateString = [orderDate convertDateValueToNSString:APPOINTMENT_DATE_FORMAT];
//    NSString *timeString = [orderDate convertDateValueToNSString:SETTING_TIME_FORMAT_12];
//    self.dateTime.text = [NSString stringWithFormat:@"%@ @%@",dateString,timeString];
    
    [self.writeAReviewButton setTitle:NSLocalizedString(@"Write Review", @"") forState:UIControlStateNormal];
    NSString *bookingId = NSLocalizedString(@"Booking Id", @"");
    self.bookingId.text = [NSString stringWithFormat:@"%@ - %@",bookingId,appointmentsInfo.orderId];
    self.totalPrice.text = [NSString stringWithFormat:@"$%.2f",[appointmentsInfo.totalPrice floatValue]];
    
    [iBeautyUtility setBookNowButtonCorderRound:self.writeAReviewButton];
    //[self.writeAReviewButton.layer insertSublayer:[iBeautyUtility setGradientColor_Buttons:self.writeAReviewButton] atIndex:0];

    if (appointmentsInfo.status == NO) {
//        self.cancelContainerView.hidden = NO;
        self.cancelledBkgView.hidden = NO;
        self.cancelledLabel.text = NSLocalizedString(@"Cancelled", @"");
        self.writeAReviewButton.hidden = YES;
    }else {
//        self.cancelContainerView.hidden = YES;
        self.cancelledBkgView.hidden = YES;
        if (appointmentsInfo.endDateCount > 0) {
            self.writeAReviewButton.hidden = NO;
        }else {
            self.writeAReviewButton.hidden = YES;
        }
    }
   // [self.cancelledBkgView.layer insertSublayer:[iBeautyUtility setGradientColor_Views:self.cancelledBkgView status:YES] atIndex:0];

}

- (NSMutableArray *)filterOutDuplicateValues:(NSMutableArray *)infoArray {
    
    NSMutableArray * tempArray = [NSMutableArray new];
    
    NSString *serviceId = @"";
    
    for (AllAppointmentsInfo *appointmentObj in infoArray) {
        
        ServicesDetail *detailInfo = appointmentObj.serviceDetail;
        
        if (![detailInfo.serviceId isEqualToString:serviceId]) {
            [tempArray addObject:appointmentObj];
            serviceId = detailInfo.serviceId;
        }
    }
    
    return tempArray;
    
}


#pragma mark - UICollectionViewDataSource methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.collectionData.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    AppointmentCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:appointmentCellIdentifier forIndexPath:indexPath];
    AllAppointmentsInfo *appointmentInfo = self.collectionData[indexPath.row];
    ServicesDetail *detailInfo = appointmentInfo.serviceDetail;
    [cell configureCollectionCell:detailInfo];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat screenWidth = CGRectGetWidth([UIScreen mainScreen].bounds);
    
    CGFloat itemWidth = 0.0f;
    
    itemWidth = floor(screenWidth -310.0f);
    
    return CGSizeMake(65.0f, 67.0f);
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 5.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 5.0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if ( (self.delegateObj != nil) && ([self.delegateObj respondsToSelector:@selector(delMethod_didSelectCollection:section:)]) ) {
        [self.delegateObj delMethod_didSelectCollection:indexPath.item section:collectionView.tag];
        
    }
}


@end
