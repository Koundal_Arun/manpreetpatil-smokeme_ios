//
//  AboutCollectionCell.m
//  iBeauty
//
//  Created by App Innovation on 03/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "AboutCollectionCell.h"
#import "NSString+iBeautyString.h"
#import "UIImageView+WebCache.h"
#import <HCSStarRatingView/HCSStarRatingView.h>

@interface AboutCollectionCell ()

@property (nonatomic, weak) IBOutlet UIImageView *reviewerImageView;
@property (nonatomic, weak) IBOutlet UILabel *reviewerName;
@property (nonatomic, weak) IBOutlet UILabel *reviewText;
@property (nonatomic, weak) IBOutlet UIImageView *ratingImageView;
@property (nonatomic, weak) IBOutlet UILabel *rating;

@property (nonatomic, weak) IBOutlet UIView *bkgView;
@property (nonatomic, weak) IBOutlet UIView *ratingView;

@property (nonatomic,strong ) IBOutlet HCSStarRatingView * starRatingView;

@end

@implementation AboutCollectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code

}

-(void)configureAboutCollectionCell:(BusinessReviews*)reviewsInfo {
    
    [self.reviewerImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",reviewsInfo.imageUrl]] placeholderImage:[UIImage imageNamed:@"No_Image"]];
    self.reviewerName.text = reviewsInfo.reviewedBy;
    self.reviewText.text = reviewsInfo.reviewText;
//    if (reviewsInfo.rating < 1.0) {
//        self.ratingView.hidden = YES;
//        self.rating.text = @"";
//    }else {
//        self.ratingView.hidden = NO;
//        self.rating.text = [NSString stringWithFormat:@"%.0f",reviewsInfo.rating];
//    }
    [self setUpStarRatingView:reviewsInfo];

}

- (void)setUpStarRatingView:(BusinessReviews*)reviewsInfo {
    
    [self.ratingImageView setHidden:YES];
    [self.rating setHidden:YES];
    
    if (reviewsInfo.rating > 0) {
        
        [self.ratingView setHidden:NO];
        CGFloat ratingValue = reviewsInfo.rating;
        self.starRatingView.userInteractionEnabled = false;
        self.starRatingView.maximumValue = 5;
        self.starRatingView.minimumValue = 0;
        self.starRatingView.value = ratingValue;
        self.starRatingView.tintColor = [UIColor iBeautyThemeColor];
        self.starRatingView.allowsHalfStars = YES;
        self.starRatingView.accurateHalfStars = YES;
        self.starRatingView.filledStarImage = [[UIImage imageNamed:@"star"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        self.starRatingView.emptyStarImage = [[UIImage imageNamed:@"star_grey"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        
    }else {
        [self.ratingView setHidden:YES];
    }
    
}


@end
