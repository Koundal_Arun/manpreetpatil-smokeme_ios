//
//  AllAppointmentsCell.h
//  iBeauty
//
//  Created by App Innovation on 12/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AllAppointmentsInfo.h"

@protocol AllAppointmentCellDelegate <NSObject>

- (void)delMethod_didSelectCollection:(NSInteger)rowSelected section:(NSInteger)sectionIndex;

@end


@interface AllAppointmentsCell : UITableViewCell

@property (weak, nonatomic) id <AllAppointmentCellDelegate> delegateObj;

@property (nonatomic, weak) IBOutlet UICollectionView *collectionView_Categories;

@property (weak, nonatomic) IBOutlet UIButton *writeAReviewButton;

- (void)configureCellWithInfo:(AllAppointmentsInfo*)appointmentsInfo;


@end
