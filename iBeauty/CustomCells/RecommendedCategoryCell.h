//
//  RecommendedCategoryCell.h
//  iBeauty
//
//  Created by App Innovation on 28/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CustomCellDelegate <NSObject>

- (void)delMethod_didSelectCollection:(NSInteger)rowSelected section:(NSInteger)sectionIndex;

@end


@interface RecommendedCategoryCell : UITableViewCell

@property (weak, nonatomic) id <CustomCellDelegate> delegateObj;

@property (nonatomic, weak) IBOutlet UICollectionView *collectionView_Categories;

- (void)configureCellWithInfo:(NSMutableArray*)categoriesInfo ;

@end
