//
//  BookAppointmentView.h
//  iBeauty
//
//  Created by App Innovation on 03/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookAppointmentView : UICollectionReusableView


@property (weak, nonatomic) IBOutlet UIButton *headerButton;

- (void)configureHeaderViewWithTitle:(NSString *)title status:(BOOL)screenStatus;

@end
