//
//  BookAppointmentCollCell.h
//  iBeauty
//
//  Created by App Innovation on 03/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServicesDetail.h"
#import "StaffDetail.h"
#import "BasketInfoObj.h"

@interface BookAppointmentCollCell : UICollectionViewCell

@property (nonatomic,weak) IBOutlet UILabel *serviceNameLabel;
@property (nonatomic,weak) IBOutlet UILabel *priceLabel;
@property (nonatomic,weak) IBOutlet UIImageView *deleteImageView;
@property (nonatomic,weak) IBOutlet UIImageView *serviceImageView;
@property (nonatomic,weak) IBOutlet UIImageView *staffImageView;
@property (nonatomic,weak) IBOutlet UIImageView *dateTimeImageView;
@property (nonatomic,weak) IBOutlet UILabel *anyStaffLabel;
@property (nonatomic,weak) IBOutlet UILabel *dateTimeLabel;
@property (nonatomic,weak) IBOutlet UILabel *actualPriceLabel;

@property (nonatomic,weak) IBOutlet UIView *bkgView;
@property (nonatomic,weak) IBOutlet UIView *staffView;
@property (nonatomic,weak) IBOutlet UIView *dateTimeView;
@property (nonatomic,weak) IBOutlet UIView *serviceDeleteView;

@property (nonatomic,weak) IBOutlet UIButton *dateTimeButton;
@property (nonatomic,weak) IBOutlet UIButton *staffButton;
@property (nonatomic,weak) IBOutlet UIButton *deleteButton;

@property (nonatomic,weak) IBOutlet UIButton *staffDeleteButton;
@property (nonatomic,weak) IBOutlet UIView *staffDeleteView;

- (void)configureCollectionCell:(BasketInfoObj*)basketObj screenStatus:(BOOL)screenStatus;

@end
