//
//  SettingsTableCell.h
//  iBeauty
//
//  Created by App Innovation on 28/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsTableCell : UITableViewCell

- (void)configureSettingCell:(NSString*)title indexPath:(NSIndexPath*)indexPath language:(NSString*)language;

@end
