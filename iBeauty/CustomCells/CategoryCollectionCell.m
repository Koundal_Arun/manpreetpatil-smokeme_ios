//
//  CategoryCollectionCell.m
//  iBeauty
//
//  Created by App Innovation on 28/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "CategoryCollectionCell.h"
#import "ProvideServiceAt.h"
#import <HCSStarRatingView/HCSStarRatingView.h>

@interface CategoryCollectionCell ()

@property (nonatomic,weak ) IBOutlet UIImageView * categoryImageView;
@property (nonatomic,weak ) IBOutlet UILabel * categoryNameLable;
@property (nonatomic,weak ) IBOutlet UILabel * distanceLable;
@property (nonatomic,weak ) IBOutlet UILabel * locationLable;
@property (nonatomic,weak ) IBOutlet UILabel * ratingLable;
@property (nonatomic,weak ) IBOutlet UIButton * homeButton;
@property (nonatomic,weak ) IBOutlet UIButton * shopButton;
@property (nonatomic,weak ) IBOutlet UIView * bkgView;
@property (nonatomic,weak ) IBOutlet UIImageView * ratingImageView;

@property (nonatomic,strong ) IBOutlet HCSStarRatingView * ratingView;

@end


@implementation CategoryCollectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)configureCollectionCell:(ServicesCategoriesInfo*)catInfoObj {
    
    [self.categoryImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",IMAGE_BASE_URL,catInfoObj.categoryImage]] placeholderImage:[UIImage imageNamed:@""]];
    self.categoryNameLable.text = catInfoObj.categoryName;
    
    NSString *distance ;
    
    if (catInfoObj.distance != nil) {
        distance = catInfoObj.distance;
    }else {
        distance = @"";
    }
    self.distanceLable.text = [NSString stringWithFormat:@"%@ Km",distance];
    self.locationLable.text = catInfoObj.location;

//    if (catInfoObj.reviewsMeta != nil && ![[catInfoObj.reviewsMeta valueForKey:@"Rating"] isEqual:[NSNull null]]) {
//        [self.ratingImageView setHidden:NO];
//        self.ratingLable.text = [NSString stringWithFormat:@"%@",[catInfoObj.reviewsMeta valueForKey:@"Rating"]];
//    }else {
//        self.ratingLable.text = @"";
//        [self.ratingImageView setHidden:YES];
//    }
    [self setUpProvideServiceAtViews:catInfoObj];
    [self setUpStarRatingView:catInfoObj];
}

- (void)setUpProvideServiceAtViews:(ServicesCategoriesInfo*)catInfoObj {
    
    [iBeautyUtility setButtonCorderRound:self.homeButton];
    [iBeautyUtility setButtonCorderRound:self.shopButton];
    [self.homeButton setHidden:YES];
    [self.shopButton setHidden:YES];

    NSMutableArray *provideServiceAt = catInfoObj.provideServiceAt;
    
    for (ProvideServiceAt * provideServiceObj in provideServiceAt) {
        
        if ( [provideServiceObj.name isEqualToString:@"Home"]) {
            
            [self.homeButton setHidden:NO];
        }
        if ( [provideServiceObj.name isEqualToString:@"Shop"]) {
            
            [self.shopButton setHidden:NO];
        }
    }
    
}

- (void)setUpStarRatingView:(ServicesCategoriesInfo*)catInfoObj {
    
    [self.ratingImageView setHidden:YES];
    [self.ratingLable setHidden:YES];
    
    if (catInfoObj.reviewsMeta != nil && ![[catInfoObj.reviewsMeta valueForKey:@"Rating"] isEqual:[NSNull null]]) {
        
        [self.ratingView setHidden:NO];
        self.ratingView.userInteractionEnabled = false;
        CGFloat ratingValue = [[catInfoObj.reviewsMeta valueForKey:@"Rating"]floatValue];
        self.ratingView.value = ratingValue;
        self.ratingView.maximumValue = 5;
        self.ratingView.minimumValue = 0;
        self.ratingView.tintColor = [UIColor iBeautyThemeColor];
        self.ratingView.allowsHalfStars = YES;
        self.ratingView.accurateHalfStars = YES;
        self.ratingView.filledStarImage = [[UIImage imageNamed:@"star"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        self.ratingView.emptyStarImage = [[UIImage imageNamed:@"star_grey"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];

    }else {
        [self.ratingView setHidden:YES];
    }
    
}


@end
