//
//  ServicesDetailCollCell.h
//  iBeauty
//
//  Created by App Innovation on 02/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServicesDetail.h"
#import "Constants.h"
#import "OffersDetail.h"


@interface ServicesDetailCollCell : UICollectionViewCell

@property (nonatomic,weak) IBOutlet UIButton *increaseButton;
@property (nonatomic,weak) IBOutlet UIButton *decreaseButton;
@property (nonatomic,weak) IBOutlet UILabel *quantityLabel;
@property (nonatomic,weak) IBOutlet UIButton *viewDetailBtn;
@property (nonatomic,weak) IBOutlet UIButton *viewMoreBtn;
@property (nonatomic,weak) IBOutlet UIImageView *viewMoreImageView;
@property (nonatomic,weak) IBOutlet UILabel *viewMoreLabel;
@property (nonatomic,weak) IBOutlet UIView *viewMoreView;

- (void)configureDetailCollectionCellWithInfo:(ServicesDetail*)detailObj;

- (void)configureOffersDetailCollectionCellWithInfo:(OffersDetail*)detailObj;

@end
