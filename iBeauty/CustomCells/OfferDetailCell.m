//
//  OfferDetailCell.m
//  iBeauty
//
//  Created by App Innovation on 11/09/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "OfferDetailCell.h"
#import "ServicesDetail.h"
#import "UIImageView+WebCache.h"
#import "Constants.h"

@interface OfferDetailCell ()

@property (nonatomic,weak) IBOutlet UIImageView *offerSerImageView;
@property (nonatomic,weak) IBOutlet UILabel *offerServiceName;
@property (nonatomic,weak) IBOutlet UILabel *actualPriceLabel;
@property (nonatomic,weak) IBOutlet UILabel *discountPriceLabel;
@property (nonatomic,weak) IBOutlet UILabel *countLabel;
@property (nonatomic,weak) IBOutlet UIView *priceView;

@end

@implementation OfferDetailCell

- (void)configureOfferDetailCell:(ServicesDetail*)infoObj offerInfo:(OffersDetail*)offerInfo {
    
    [self setUpViewsInterface];

    self.offerServiceName.text = [NSString stringWithFormat:@"- %@",infoObj.categoryName];

}

- (void)setUpPriceInfoView:(ServicesDetail*)detailObj {
    
    NSString *discountPrice = @"";
    
    if (detailObj.salesPrice.length > 0) {
        
        discountPrice = detailObj.salesPrice;
        
        NSString *totalPriceString = [NSString stringWithFormat:@"$%@",detailObj.serviceAmount];
        NSString *finalDiscountString = [NSString stringWithFormat:@"%@ (%@)",discountPrice,totalPriceString];
        
        NSRange range = [finalDiscountString rangeOfString:totalPriceString];
        
        NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:finalDiscountString];
        [attributeString addAttribute:NSStrikethroughStyleAttributeName value:@2 range:NSMakeRange(range.location, [totalPriceString length])];
        self.actualPriceLabel.attributedText = attributeString;
    }else {
        discountPrice = detailObj.serviceAmount;
        self.actualPriceLabel.text = [NSString stringWithFormat:@"$%@",discountPrice];
    }
    
}

- (void)setUpViewsInterface {
    
    [iBeautyUtility setIncreseDecreaseButtonCordersRound:self.decreaseButton];
    [iBeautyUtility setIncreseDecreaseButtonCordersRound:self.increaseButton];
    [iBeautyUtility setLabelCorderRound:self.countLabel];
    [iBeautyUtility setimageViewRound:self.offerSerImageView];
    
}


@end
