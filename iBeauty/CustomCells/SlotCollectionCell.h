//
//  SlotCollectionCell.h
//  iBeauty
//
//  Created by App Innovation on 05/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServiceTimeSlot.h"

@interface SlotCollectionCell : UICollectionViewCell

@property (nonatomic,weak) IBOutlet UIView *bgkView;
@property (nonatomic,weak) IBOutlet UILabel *slotTimeLabel;
@property (nonatomic,weak) IBOutlet UIButton *selectSlotButton;


- (void)configureTimeSlotCollectionCell:(ServiceTimeSlot*)infoObj ;

@end
