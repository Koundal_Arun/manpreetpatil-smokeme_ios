//
//  ProductSellerCell.m
//  iBeauty
//
//  Created by App Innovation on 23/01/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import "ProductSellerCell.h"

#import "NSDate+iBeautyDateFormatter.h"
#import "NSString+iBeautyString.h"
#import "ProvideServiceAt.h"
#import <HCSStarRatingView/HCSStarRatingView.h>

@interface ProductSellerCell ()

@property (nonatomic,weak) IBOutlet UIImageView *categoryImageView;
@property (nonatomic,weak) IBOutlet UILabel *Lbl_categoryName;
@property (nonatomic,weak) IBOutlet UILabel *Lbl_openingHours;
@property (nonatomic,weak) IBOutlet UILabel *Lbl_distance;
@property (nonatomic,weak) IBOutlet UILabel *Lbl_ratings;
@property (nonatomic,weak) IBOutlet UILabel *Lbl_reviews;
@property (nonatomic,weak) IBOutlet UIButton *home_Button;
@property (nonatomic,weak) IBOutlet UIButton *shop_Button;

@property (nonatomic,weak) IBOutlet UIView * view_Background;
@property (nonatomic,weak) IBOutlet UIView * view_Ratings;
@property (nonatomic,weak) IBOutlet UIImageView * imageView_Ratings;

@property (nonatomic,weak) IBOutlet UIView * view_provideAt;

@property (nonatomic,strong ) IBOutlet HCSStarRatingView * ratingView;


@end

@implementation ProductSellerCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)configureProductSellerCell:(ServicesCategoriesInfo*)infoObj {
    
    self.Lbl_categoryName.text = infoObj.categoryName;
    [self.categoryImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",IMAGE_BASE_URL,infoObj.categoryImage]] placeholderImage:[UIImage imageNamed:@"No_Image"]];
    [iBeautyUtility setImageViewCornerRound:self.categoryImageView];
    NSString *distance ;
    
    if (infoObj.distance != nil) {
        distance = infoObj.distance;
    }else {
        distance = @"";
    }
    self.Lbl_distance.text = [NSString stringWithFormat:@"%@, %@ Km",infoObj.location,distance];
    
    [self setUpStarRatingView:infoObj];
    [self setUpReviews:infoObj];
    
    self.Lbl_ratings.textColor = [UIColor iBeautyThemeColor];

    NSString *finalString =  [NSString stringWithFormat:@"(%@-%@)",[infoObj.openingHours valueForKey:@"Fromtime"],[infoObj.openingHours valueForKey:@"Totime"]];
    NSString *openingTimeStr = [[NSDate date] getOpeningStringBasedOnStartAndEndTime:infoObj.openingHours format:SETTING_TIME_FORMAT_12];
    NSMutableAttributedString *openingHrString = [openingTimeStr getAttributedTextWithTimeString:finalString openingStatus:openingTimeStr];
    self.Lbl_openingHours.attributedText = openingHrString;
    [self setUpProvideServiceAtViews:infoObj];
    
}

- (void)setUpReviews:(ServicesCategoriesInfo*)catInfoObj {
    
    NSString *reviewText;
    
    if (catInfoObj.reviewsMeta != nil && ![[catInfoObj.reviewsMeta valueForKey:@"Reviews"] isEqual:[NSNull null]]) {
        
        NSInteger reviews = [[catInfoObj.reviewsMeta valueForKey:@"Reviews"]integerValue];
        
        if (reviews > 1) {
            reviewText = NSLocalizedString(@"Reviews", @"");
            self.Lbl_reviews.text = [NSString stringWithFormat:@"%ld %@",(long)reviews,reviewText];
        }else if (reviews == 1) {
            reviewText = NSLocalizedString(@"Review", @"");
            self.Lbl_reviews.text = [NSString stringWithFormat:@"%ld %@",(long)reviews,reviewText];
        }else {
//            reviewText = NSLocalizedString(@"Review", @"");
//            self.Lbl_reviews.text = [NSString stringWithFormat:@"0 %@",reviewText];
            self.Lbl_reviews.text = @"";
        }
    }else {
//        reviewText = NSLocalizedString(@"Review", @"");
//        self.Lbl_reviews.text = [NSString stringWithFormat:@"0 %@",reviewText];
        self.Lbl_reviews.text = @"";
    }
    
}

- (void)setUpStarRatingView:(ServicesCategoriesInfo*)catInfoObj {
    
    [self.imageView_Ratings setHidden:YES];
    [self.Lbl_ratings setHidden:YES];
    
    if (catInfoObj.reviewsMeta != nil && ![[catInfoObj.reviewsMeta valueForKey:@"Rating"] isEqual:[NSNull null]]) {
        
        [self.ratingView setHidden:NO];
        CGFloat ratingValue = [[catInfoObj.reviewsMeta valueForKey:@"Rating"]floatValue];
        self.ratingView.userInteractionEnabled = false;
        
        self.ratingView.value = ratingValue;
        self.ratingView.maximumValue = 5;
        self.ratingView.minimumValue = 0;
        self.ratingView.tintColor = [UIColor iBeautyThemeColor];
        self.ratingView.allowsHalfStars = YES;
        self.ratingView.accurateHalfStars = YES;
        self.ratingView.filledStarImage = [[UIImage imageNamed:@"star"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        self.ratingView.emptyStarImage = [[UIImage imageNamed:@"star_grey"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        
    }else {
        [self.ratingView setHidden:YES];
    }
    
}

- (void)setUpProvideServiceAtViews:(ServicesCategoriesInfo*)catInfoObj {
    
    [self setUpViewForLocalization];
    
    [iBeautyUtility setButtonCorderRound:self.home_Button];
    [iBeautyUtility setButtonCorderRound:self.shop_Button];
    [self.home_Button setHidden:YES];
    [self.shop_Button setHidden:YES];
    self.home_Button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    self.shop_Button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    self.home_Button.translatesAutoresizingMaskIntoConstraints = YES;
    self.shop_Button.translatesAutoresizingMaskIntoConstraints = YES;

    NSMutableArray *provideServiceAt = catInfoObj.provideServiceAt;
    
    BOOL status = NO;
    
    for (ProvideServiceAt * provideServiceObj in provideServiceAt) {
        
        if ( [provideServiceObj.name isEqualToString:@"Delivery"]) {
            
            [self.home_Button setHidden:NO];
        }else {
            if (provideServiceAt.count == 1) {
                status = YES;
            }
        }
        NSString *languageCode =   [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];
        
        if ( [provideServiceObj.name isEqualToString:@"Pickup"]) {
            [self.shop_Button setHidden:NO];
            self.shop_Button.translatesAutoresizingMaskIntoConstraints = YES;
            
            if (status == YES) {
                
                [self showOnlyShopServiceProvideLabel:languageCode];
                
            }else {
                
                [self showBothHomeShopServiceProvideLabel:languageCode];
            }
        }
        else {
            
            [self showOnlyHomeServiceProvideLabel:languageCode];
        }
    }
    
}

- (void)showOnlyShopServiceProvideLabel:(NSString*)languageCode {
    
    if ([languageCode isEqualToString:@"en"]) {
        
        self.shop_Button.frame = CGRectMake(0, self.shop_Button.frame.origin.y, 52.0f, self.shop_Button.frame.size.height);
    }
    
}

- (void)showOnlyHomeServiceProvideLabel:(NSString*)languageCode {
    
    if ([languageCode isEqualToString:@"en"]) {
        self.home_Button.frame = CGRectMake(0, self.home_Button.frame.origin.y, 62.0f, self.home_Button.frame.size.height);
    }else {
        self.home_Button.frame = CGRectMake(58, self.home_Button.frame.origin.y, 62.0f, self.home_Button.frame.size.height);
    }
    
}

- (void)showBothHomeShopServiceProvideLabel:(NSString*)languageCode {
    
    if ([languageCode isEqualToString:@"en"]) {
        
        self.shop_Button.frame = CGRectMake(67.0f, self.shop_Button.frame.origin.y, 52.0f, self.shop_Button.frame.size.height);
    }else {
        self.home_Button.frame = CGRectMake(0, self.home_Button.frame.origin.y, 62.0f, self.home_Button.frame.size.height);
    }
}

- (void)setUpViewForLocalization {
    
    [self.home_Button setTitle:NSLocalizedString(@"DELIVERY", @"") forState:UIControlStateNormal];
    [self.shop_Button setTitle:NSLocalizedString(@"PICKUP", @"") forState:UIControlStateNormal];
    
}

- (NSDictionary*)getCurrentOpeningHoursDict:(ServicesCategoriesInfo*)infoObj {
    
    NSString *weekDay = [[NSString string] getWeekDay];
    NSDictionary *openingHourDict;
    for (NSDictionary *openingHoursDict in infoObj.StandardOpeningHours) {
        
        NSString *dayString = [openingHoursDict valueForKey:@"Dayname"];
        
        if ([dayString isEqualToString:weekDay]) {
            openingHourDict = openingHoursDict;
        }
    }
    return  openingHourDict;
}


@end
