//
//  ServicesCollectionCell.h
//  iBeauty
//
//  Created by App Innovation on 27/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"

@interface ServicesCollectionCell : UICollectionViewCell

@property (nonatomic,weak) IBOutlet UILabel *serviceName;
@property (nonatomic,weak) IBOutlet UIImageView *serviceImageView;
@property (nonatomic,weak) IBOutlet UIView *imageOuterView;

- (void)configureCollectionCellWithInfo:(Services*)servicesInfo;

@end
