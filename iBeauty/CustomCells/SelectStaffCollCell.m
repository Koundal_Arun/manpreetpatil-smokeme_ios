//
//  SelectStaffCollCell.m
//  iBeauty
//
//  Created by App Innovation on 04/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "SelectStaffCollCell.h"
#import "UIImageView+WebCache.h"
#import "Constants.h"

@interface SelectStaffCollCell ()

@property (nonatomic,weak) IBOutlet UIImageView *staffImageView;
@property (nonatomic,weak) IBOutlet UILabel *nameLabel;
@property (nonatomic,weak) IBOutlet UILabel *regionLabel;
@property (nonatomic,weak) IBOutlet UIView *bkgView;


@end

@implementation SelectStaffCollCell

- (void)configureStaffCollectionCell:(StaffDetail*)infoObj {
    
    [self.staffImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",IMAGE_BASE_URL,infoObj.staffImage]] placeholderImage:[UIImage imageNamed:@""]];
    self.nameLabel.text = infoObj.staffName;
    self.regionLabel.text = infoObj.nationality;

}

@end
