//
//  AppointmentCollectionCell.m
//  iBeauty
//
//  Created by App Innovation on 12/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "AppointmentCollectionCell.h"
#import "UIImageView+WebCache.h"
#import "iBeautyUtility.h"
#import "Constants.h"

@implementation AppointmentCollectionCell

- (void)configureCollectionCell:(ServicesDetail*)infoObj {
    
    [self.servicesImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",IMAGE_BASE_URL,infoObj.serviceImage]] placeholderImage:[UIImage imageNamed:@""]];
    [iBeautyUtility setimageViewRound:self.servicesImageView];
    
}

@end
