//
//  ServicesDetailCollCell.m
//  iBeauty
//
//  Created by App Innovation on 02/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "ServicesDetailCollCell.h"
#import "UIImageView+WebCache.h"

@interface ServicesDetailCollCell ()

@property (nonatomic,weak) IBOutlet UILabel *serviceNameLabel;
@property (nonatomic,weak) IBOutlet UILabel *serviceDescriptionLabel;
@property (nonatomic,weak) IBOutlet UILabel *priceLabel;
@property (nonatomic,weak) IBOutlet UIImageView *serviceImageView;
@property (nonatomic,weak) IBOutlet UILabel *actualPriceLabel;

@property (nonatomic,weak) IBOutlet UIView *backGroundView;
@property (nonatomic,weak) IBOutlet UIView *priceView;
@property (nonatomic,weak) IBOutlet UIView *quantityView;
@property (nonatomic,assign) CGFloat xAxis;
@property (nonatomic,assign) CGFloat viewMoreXAxis;

@end

@implementation ServicesDetailCollCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

#pragma mark - Service Detail Cell


- (void)configureDetailCollectionCellWithInfo:(ServicesDetail*)detailObj {
    
    self.viewDetailBtn.hidden = YES;
    self.quantityView.hidden = NO;

    [iBeautyUtility setIncreseDecreaseButtonCordersRound:self.decreaseButton];
    [iBeautyUtility setIncreseDecreaseButtonCordersRound:self.increaseButton];
    [iBeautyUtility setLabelCorderRound:self.quantityLabel];
    [iBeautyUtility setImageViewCornerRound:self.serviceImageView];
    
    [self.serviceNameLabel setTranslatesAutoresizingMaskIntoConstraints:YES];
    [self.serviceDescriptionLabel setTranslatesAutoresizingMaskIntoConstraints:YES];
    [self.priceView setTranslatesAutoresizingMaskIntoConstraints:YES];
    
    [self.serviceImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",IMAGE_BASE_URL,detailObj.serviceImage]] placeholderImage:[UIImage imageNamed:@"No_Image"]];
    
    [self setUpXAxisAccordingToLanguage];
    [self setUpViewMoreView:detailObj];
    
    self.serviceNameLabel.text = [NSString stringWithFormat:@"%@ (%@ min)",detailObj.categoryName,detailObj.serviceTime];
    self.serviceDescriptionLabel.text = detailObj.serviceDescription;

//    if (detailObj.serviceDescription.length > 0) {
//        self.serviceDescriptionLabel.text = detailObj.serviceDescription;
//        self.serviceDescriptionLabel.hidden = NO;
//    }else {
//        self.serviceDescriptionLabel.hidden = YES;
//    }

    [self setUpPriceInfoViewForService:detailObj];
    self.quantityLabel.text = [NSString stringWithFormat:@"%ld",(long)detailObj.increaseIndex];

}

- (void)setUpXAxisAccordingToLanguage {
    
    NSString *languageCode =   [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];
    
    if ([languageCode isEqualToString:@"en"]) {
        
        self.xAxis = 100.0f;
        self.viewMoreXAxis = SCREEN_WIDTH - 125.0f;

    }else if ([languageCode isEqualToString:@"ar"]) {
        
        self.xAxis = 10.0f;
        self.viewMoreXAxis = 10.0f;
    }
}

- (void)setUpViewMoreView:(ServicesDetail*)detailObj {
    
    if (detailObj.descriptionHeight > 21.0f) {
        
        self.viewMoreView.hidden = NO;
        self.serviceDescriptionLabel.hidden = NO;

        if (detailObj.viewMoreStatus == YES) {
            
            self.viewMoreLabel.text = NSLocalizedString(@"View Less",@"");
            
            [self.serviceNameLabel setFrame:CGRectMake(self.xAxis, 10.0f, [UIScreen mainScreen].bounds.size.width - 125.0f, detailObj.nameHeight)];
            
            [self.serviceDescriptionLabel setFrame:CGRectMake(self.xAxis, 10.0f +detailObj.nameHeight + 10.0f , [UIScreen mainScreen].bounds.size.width - 125.0f, detailObj.descriptionHeight)];

            [self.viewMoreView setFrame:CGRectMake(self.viewMoreXAxis, 10.0f +detailObj.nameHeight + 10.0f + detailObj.descriptionHeight + 5.0f, self.viewMoreView.frame.size.width, self.viewMoreView.frame.size.height)];

            [self.priceView setFrame:CGRectMake(self.xAxis,10.0f + detailObj.nameHeight + 10.0f +  detailObj.descriptionHeight + 5.0f+25+5.0f, [UIScreen mainScreen].bounds.size.width - 125.0f, self.priceView.frame.size.height)];
            
        }else {
            
            self.viewMoreLabel.text = NSLocalizedString(@"View More",@"");

            [self.serviceNameLabel setFrame:CGRectMake(self.xAxis, 10.0f, [UIScreen mainScreen].bounds.size.width - 125.0f, detailObj.nameHeight)];
            
            [self.serviceDescriptionLabel setFrame:CGRectMake(self.xAxis, 10.0f +detailObj.nameHeight + 10.0f , [UIScreen mainScreen].bounds.size.width - 125.0f, 21.0f)];
            
            [self.viewMoreView setFrame:CGRectMake(self.viewMoreXAxis, 10.0f +detailObj.nameHeight + 10.0f + 21.0f + 5.0f, self.viewMoreView.frame.size.width, self.viewMoreView.frame.size.height)];
            
            [self.priceView setFrame:CGRectMake(self.xAxis,10.0f + detailObj.nameHeight + 10.0f + 21.0f + 5.0f + 25 + 5.0f, [UIScreen mainScreen].bounds.size.width - 125.0f, self.priceView.frame.size.height)];
            
        }
        
    }else {
        
        self.viewMoreView.hidden = YES;
        
        [self.serviceNameLabel setFrame:CGRectMake(self.xAxis, 10.0f, [UIScreen mainScreen].bounds.size.width - 125.0f, detailObj.nameHeight)];
        
        [self.serviceDescriptionLabel setFrame:CGRectMake(self.xAxis, 10.0f +detailObj.nameHeight + 10.0f , [UIScreen mainScreen].bounds.size.width - 125.0f, detailObj.descriptionHeight)];
        
        [self.priceView setFrame:CGRectMake(self.xAxis,10.0f + detailObj.nameHeight + 10.0f + detailObj.descriptionHeight + 10.0f, [UIScreen mainScreen].bounds.size.width - 125.0f, self.priceView.frame.size.height)];
    }

}

#pragma mark - Service Price Info View

- (void)setUpPriceInfoViewForService:(ServicesDetail*)detailObj {
    
    NSString *discountPrice = @"";
    
     if (detailObj.salesPrice.length > 0) {
         discountPrice = detailObj.salesPrice;
     }else {
         discountPrice = detailObj.serviceAmount;
     }
    
    if (detailObj.increaseIndex>0) {
        discountPrice = [NSString stringWithFormat:@"%ld × $ %@",(long)detailObj.increaseIndex,discountPrice];
    }else {
        discountPrice = [NSString stringWithFormat:@"$ %@",discountPrice];
    }
    
    if (detailObj.salesPrice.length > 0) {
        
        NSString *totalPriceString = [NSString stringWithFormat:@"($%@)",detailObj.serviceAmount];
        NSString *finalDiscountString = [NSString stringWithFormat:@"%@ %@",discountPrice,totalPriceString];
        
        NSRange range = [finalDiscountString rangeOfString:totalPriceString];
        
        UIFont *boldFont = [UIFont fontWithName:@"SegoeUI" size:14.0];

        NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:finalDiscountString];
        [attributeString setAttributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor],NSFontAttributeName:boldFont} range:range];
        [attributeString addAttribute:NSStrikethroughStyleAttributeName value:@2 range:NSMakeRange(range.location, [totalPriceString length])];

        self.priceLabel.attributedText = attributeString;
    }else {
        self.priceLabel.text = discountPrice;
    }
}

#pragma mark - Offer Detail Cell

- (void)configureOffersDetailCollectionCellWithInfo:(OffersDetail*)detailObj {
    
    //[self.viewDetailBtn.layer insertSublayer:[iBeautyUtility setGradientColor_Buttons:self.viewDetailBtn] atIndex:0];
    [iBeautyUtility setButtonCorderRound:self.viewDetailBtn];
    
    self.viewDetailBtn.hidden = NO;
    self.quantityView.hidden = YES;
    self.viewMoreView.hidden = YES;

    [iBeautyUtility setIncreseDecreaseButtonCordersRound:self.decreaseButton];
    [iBeautyUtility setIncreseDecreaseButtonCordersRound:self.increaseButton];
    [iBeautyUtility setLabelCorderRound:self.quantityLabel];
    
    [self.serviceNameLabel setTranslatesAutoresizingMaskIntoConstraints:YES];
    [self.serviceDescriptionLabel setTranslatesAutoresizingMaskIntoConstraints:YES];
    [self.priceView setTranslatesAutoresizingMaskIntoConstraints:YES];
    
    [self.serviceImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",IMAGE_BASE_URL,detailObj.imageUrl]] placeholderImage:[UIImage imageNamed:@"No_Image"]];
    
    [self setUpXAxisAccordingToLanguage];
    
    self.serviceNameLabel.text = [NSString stringWithFormat:@"%@",detailObj.offerTitle];
    
        if (detailObj.detailDesc.length>0) {
            self.serviceDescriptionLabel.text = detailObj.detailDesc;
            self.serviceDescriptionLabel.hidden = NO;
            [self.serviceNameLabel setFrame:CGRectMake(self.xAxis, 10.0f, [UIScreen mainScreen].bounds.size.width - 125.0f, self.serviceNameLabel.frame.size.height)];
            [self.serviceDescriptionLabel setFrame:CGRectMake(self.xAxis, 10.0f +self.serviceNameLabel.frame.size.height +5.0f , [UIScreen mainScreen].bounds.size.width - 125.0f, self.serviceDescriptionLabel.frame.size.height)];
            [self.priceView setFrame:CGRectMake(self.xAxis, 64.0f, [UIScreen mainScreen].bounds.size.width - 125.0f, self.priceView.frame.size.height)];
        }else {
            self.serviceDescriptionLabel.hidden = NO;
            self.serviceDescriptionLabel.text = @"";
            [self.serviceNameLabel setFrame:CGRectMake(self.xAxis, 17.0f, [UIScreen mainScreen].bounds.size.width - 125.0f, self.serviceNameLabel.frame.size.height)];
            [self.priceView setFrame:CGRectMake(self.xAxis, 47.0f, [UIScreen mainScreen].bounds.size.width - 125.0f, self.priceView.frame.size.height)];
        }
    
    [self setUpPriceInfoViewForOffer:detailObj];

    self.quantityLabel.text = [NSString stringWithFormat:@"%ld",(long)detailObj.increaseIndex];
   
}

#pragma mark - Offer Price Info View

- (void)setUpPriceInfoViewForOffer:(OffersDetail*)detailObj {
    
    NSString *discountPrice = @"";

    if (detailObj.totalPrice.length > 0) {
        
        discountPrice = detailObj.discountPrice;
        
        NSString *totalPriceString = [NSString stringWithFormat:@"($%@)",detailObj.totalPrice];
        NSString *finalDiscountString = [NSString stringWithFormat:@"%@ %@",discountPrice,totalPriceString];
        
        NSRange range = [finalDiscountString rangeOfString:totalPriceString];
        
        UIFont *boldFont = [UIFont fontWithName:@"SegoeUI" size:15.0];

        NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:finalDiscountString];
        
        [attributeString setAttributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor],NSFontAttributeName:boldFont} range:range];

        [attributeString addAttribute:NSStrikethroughStyleAttributeName value:@2 range:NSMakeRange(range.location, [totalPriceString length])];
        self.priceLabel.attributedText = attributeString;
    }else {
        discountPrice = [NSString stringWithFormat:@"$ %@",detailObj.discountPrice];
        self.priceLabel.text = discountPrice;
    }
    
}


@end
