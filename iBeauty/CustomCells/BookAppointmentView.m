//
//  BookAppointmentView.m
//  iBeauty
//
//  Created by App Innovation on 03/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "BookAppointmentView.h"

@interface BookAppointmentView ()

@property (weak, nonatomic) IBOutlet UIImageView *headerImageIcon;
@property (weak, nonatomic) IBOutlet UILabel *titleHeader;
@property (weak, nonatomic) IBOutlet UIView *headerImageView;

@end

@implementation BookAppointmentView

- (void)configureHeaderViewWithTitle:(NSString *)title status:(BOOL)screenStatus {
    
    self.titleHeader.text = title;
    if (screenStatus == NO) {
        self.headerImageView.hidden = NO;
    }else {
        self.headerImageView.hidden = YES;
    }
}

@end
