//
//  ProfileCollectionCell.m
//  iBeauty
//
//  Created by App Innovation on 10/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "ProfileCollectionCell.h"
#import "Constants.h"
#import "UILabel+FlexibleWidHeight.h"

@interface ProfileCollectionCell ()

@property (nonatomic,weak) IBOutlet UILabel *userName;
@property (nonatomic,weak) IBOutlet UILabel *userAddress;
@property (nonatomic,weak) IBOutlet UIView  *addressbackg_View;

@end

@implementation ProfileCollectionCell

- (void)configureProfileCollectionCellView:(AddressModel*)addressObj {
    
    self.userName.text = addressObj.name;
    self.userAddress.text = addressObj.completeAddress;
    self.userAddress.translatesAutoresizingMaskIntoConstraints = YES;
    self.userAddress.numberOfLines = 0;
    [UILabel fixHeightOfThisLabel:self.userAddress];
    
    NSString *languageCode =   [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];

    if (addressObj.name.length > 0) {
        self.userName.hidden = NO;
        
        if ([languageCode isEqualToString:@"en"]) {
            [self.userAddress setFrame:CGRectMake(8.0f, 36.0f, [UIScreen mainScreen].bounds.size.width-118, self.userAddress.frame.size.height)];
        }else if ([languageCode isEqualToString:@"ar"]) {
            [self.userAddress setFrame:CGRectMake(100.0f, 36.0f, [UIScreen mainScreen].bounds.size.width-118, self.userAddress.frame.size.height)];
        }
        
    }else {
        self.userName.hidden = YES;
        
        if ([languageCode isEqualToString:@"en"]) {
            [self.userAddress setFrame:CGRectMake(8.0f, 10.0f, [UIScreen mainScreen].bounds.size.width-118, self.userAddress.frame.size.height)];
        }else if ([languageCode isEqualToString:@"ar"]) {
            [self.userAddress setFrame:CGRectMake(100.0f, 10.0f, [UIScreen mainScreen].bounds.size.width-118, self.userAddress.frame.size.height)];
        }

    }

    [self.addressbackg_View setFrame:CGRectMake(8.0f, self.addressbackg_View.frame.origin.y, [UIScreen mainScreen].bounds.size.width-16, addressObj.cellHeight-20.0f)];
    
    [iBeautyUtility setViewBorderRound:self.addressbackg_View];
    
}

@end
