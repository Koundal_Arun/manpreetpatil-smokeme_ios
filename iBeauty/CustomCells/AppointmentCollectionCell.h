//
//  AppointmentCollectionCell.h
//  iBeauty
//
//  Created by App Innovation on 12/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServicesDetail.h"

@interface AppointmentCollectionCell : UICollectionViewCell

@property (nonatomic,weak) IBOutlet UIImageView *servicesImageView;

- (void)configureCollectionCell:(ServicesDetail*)infoObj;


@end
