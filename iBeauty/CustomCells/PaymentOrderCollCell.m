//
//  PaymentOrderCollCell.m
//  iBeauty
//
//  Created by App Innovation on 12/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "PaymentOrderCollCell.h"
#import "NSDate+iBeautyDateFormatter.h"

@implementation PaymentOrderCollCell

- (void)configurePaymentOrderCollectionCell:(BasketInfoObj*)basketInfo {
    
    ServicesDetail *serviceInfo = basketInfo.serviceInfoObj;
    self.serviceNameLabel.text = [NSString stringWithFormat:@"1 x %@",serviceInfo.categoryName];
    
    [self setUpServiceLabelTextAlignment];

}

- (void)setUpServiceLabelTextAlignment {
    
    NSString *languageCode =   [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];
    
    if ([languageCode isEqualToString:@"en"]) {
        self.serviceNameLabel.textAlignment = NSTextAlignmentLeft;
    }else if ([languageCode isEqualToString:@"ar"]) {
        self.serviceNameLabel.textAlignment = NSTextAlignmentRight;
    }
}




- (void)configurePaymentOrderCollectionCell_Order_Offers:(OffersDetail*)offerInfo {
    
    self.serviceNameLabel.text = [NSString stringWithFormat:@"%ld x %@",offerInfo.quantitySelected,offerInfo.offerTitle];
    
    [self setUpServiceLabelTextAlignment];
    
}



@end
