//
//  OffersCollectionCell.m
//  iBeauty
//
//  Created by App Innovation on 23/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "OffersCollectionCell.h"
#import "UIImageView+WebCache.h"
#import "OffersImages.h"
#import "NSDate+iBeautyDateFormatter.h"
#import "iBeautyUtility.h"

@interface OffersCollectionCell ()

@property (nonatomic,weak) IBOutlet UIImageView *offerSerImageView;
@property (nonatomic,weak) IBOutlet UILabel *discountLabel;
@property (nonatomic,weak) IBOutlet UILabel *descriptionLabel;
@property (nonatomic,weak) IBOutlet UILabel *validDateLabel;
@property (nonatomic,weak) IBOutlet UILabel *offerLabel;

@end

@implementation OffersCollectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)configureOffersCollectionCell:(OffersDetail*)infoObj {
    
//    OffersImages *offerObj;
//
//    if (infoObj.offerImagesArray) {
//        offerObj = infoObj.offerImagesArray[0];
//    }
    
    [self.offerSerImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",IMAGE_BASE_URL,infoObj.imageUrl]] placeholderImage:[UIImage imageNamed:@"image3.png"]];
    self.discountLabel.text = [NSString stringWithFormat:@"%@",infoObj.offerTitle];
    self.descriptionLabel.text = infoObj.detailDesc;
    NSDate *orderDate = [NSDate convertNSStringToNSDate:infoObj.offerEndDateTime format:DATE_FORMAT_APPOINTMENT_DETAIL];
    NSString *dateString = [orderDate convertDateValueToNSString:PAYMENT_DATE_FORMAT localeStatus:false];
    self.validDateLabel.text = [NSString stringWithFormat:@"Valid till %@",dateString];
    self.offerLabel.text = [NSString stringWithFormat:@"%@ %@%%",infoObj.discountType,infoObj.discountPrice];;
//    [iBeautyUtility setLabelRound:self.offerLabel];
    
    [iBeautyUtility setBookNowButtonCorderRound:self.viewDetailButton];
    //[self.viewDetailButton.layer insertSublayer:[iBeautyUtility setGradientColor_Buttons:self.viewDetailButton] atIndex:0];

}


@end
