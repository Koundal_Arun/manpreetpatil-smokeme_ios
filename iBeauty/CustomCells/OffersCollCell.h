//
//  OffersCollCell.h
//  iBeauty
//
//  Created by App Innovation on 19/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OffersDetail.h"

@interface OffersCollCell : UICollectionViewCell

@property (nonatomic,weak) IBOutlet UIButton *readMoreButton;
@property (nonatomic,weak) IBOutlet UIButton *viewDetail;

- (void)configureOffersCollectionCell:(OffersDetail*)infoObj;

@end
