//
//  ProfileCollectionCell.h
//  iBeauty
//
//  Created by App Innovation on 10/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddressModel.h"

@interface ProfileCollectionCell : UICollectionViewCell

@property (nonatomic,weak) IBOutlet UIButton *deleteButton;
@property (nonatomic,weak) IBOutlet UIButton *editButton;

- (void)configureProfileCollectionCellView:(AddressModel*)addressObj;

@end
