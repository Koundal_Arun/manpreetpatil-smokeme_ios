//
//  OffersCollCell.m
//  iBeauty
//
//  Created by App Innovation on 19/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "OffersCollCell.h"
#import "UIImageView+WebCache.h"
#import "OffersImages.h"
#import "NSDate+iBeautyDateFormatter.h"
#import "iBeautyUtility.h"

@interface OffersCollCell ()

@property (nonatomic,weak) IBOutlet UIImageView *offerSerImageView;
@property (nonatomic,weak) IBOutlet UILabel *discountLabel;
@property (nonatomic,weak) IBOutlet UILabel *descriptionLabel;
@property (nonatomic,weak) IBOutlet UILabel *validDateLabel;
@property (nonatomic,weak) IBOutlet UILabel *offerLabel;
@property (nonatomic,weak) IBOutlet UILabel *underLineLabel;

@end

@implementation OffersCollCell

- (void)configureOffersCollectionCell:(OffersDetail*)infoObj {
    
    [self.offerSerImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",IMAGE_BASE_URL,infoObj.imageUrl]] placeholderImage:[UIImage imageNamed:@"image3.png"]];
    self.discountLabel.text = [NSString stringWithFormat:@"%@",infoObj.offerTitle];
//    self.descriptionLabel.text = infoObj.detailDesc;
    NSDate *orderDate = [NSDate convertNSStringToNSDate:infoObj.offerEndDateTime format:DATE_FORMAT_APPOINTMENT_DETAIL];
    NSString *dateString = [orderDate convertDateValueToNSString:PAYMENT_DATE_FORMAT localeStatus:false];
    self.validDateLabel.text = [NSString stringWithFormat:@"Valid till %@",dateString];
    self.underLineLabel.frame = CGRectMake(self.underLineLabel.frame.origin.x, self.underLineLabel.frame.origin.y,SCREEN_WIDTH-16.0f, 1.0f);
    //[self.viewDetail.layer insertSublayer:[iBeautyUtility setGradientColor_Buttons:self.viewDetail] atIndex:0];
    [iBeautyUtility setBookNowButtonCorderRound:self.viewDetail];

}



@end
