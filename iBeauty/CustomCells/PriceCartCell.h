//
//  PriceCartCell.h
//  iBeauty
//
//  Created by App Innovation on 09/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"

@interface PriceCartCell : UICollectionViewCell


-(void)configurePriceCartCellWithPriceInfo:(ServicesDetail*)serviceInfo screenStatus:(BOOL)screenStatus;

@end
