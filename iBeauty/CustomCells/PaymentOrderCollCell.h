//
//  PaymentOrderCollCell.h
//  iBeauty
//
//  Created by App Innovation on 12/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServicesDetail.h"
#import "BasketInfoObj.h"
#import "OffersDetail.h"

@interface PaymentOrderCollCell : UICollectionViewCell

@property (nonatomic,weak) IBOutlet UILabel *serviceNameLabel;

- (void)configurePaymentOrderCollectionCell:(BasketInfoObj*)basketInfo;

- (void)configurePaymentOrderCollectionCell_Order_Offers:(OffersDetail*)offerInfo;


@end
