//
//  PriceCarFooterCollView.h
//  iBeauty
//
//  Created by App Innovation on 31/01/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasketPriceSummary.h"
#import "Constants.h"

NS_ASSUME_NONNULL_BEGIN

@interface PriceCarFooterCollView : UICollectionReusableView

- (void)configureFooterViewWithTitle:(BasketPriceSummary*)basketPriceInfo ;

- (void)configureFooterViewWithTitle_products:(BasketPriceSummary*)basketPriceInfo;

@end

NS_ASSUME_NONNULL_END
