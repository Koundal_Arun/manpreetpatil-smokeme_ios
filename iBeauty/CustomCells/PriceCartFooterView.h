//
//  PriceCartFooterView.h
//  iBeauty
//
//  Created by App Innovation on 09/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasketPriceSummary.h"
#import "AddressModel.h"

@interface PriceCartFooterView : UICollectionReusableView

- (void)configureFooterViewWithTitle:(BasketPriceSummary*)basketPriceInfo;

@end
