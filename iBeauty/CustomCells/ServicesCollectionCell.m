//
//  ServicesCollectionCell.m
//  iBeauty
//
//  Created by App Innovation on 27/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "ServicesCollectionCell.h"

@implementation ServicesCollectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)configureCollectionCellWithInfo:(Services*)servicesInfo {
    
    self.serviceName.text = servicesInfo.serviceName;
    
    [self.serviceImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",IMAGE_BASE_URL,servicesInfo.serviceImage]] placeholderImage:[UIImage imageNamed:@"No_Image"]];
    
    
    NSString *deviceType = [UIDevice currentDevice].model;
    if ([deviceType isEqualToString:@"iPhone"]) {
        
    self.serviceImageView.translatesAutoresizingMaskIntoConstraints = YES;

        if (IS_IPHONE_5) {
            self.serviceImageView.frame = CGRectMake(self.serviceImageView.frame.origin.x, self.serviceImageView.frame.origin.y, 90.0f, 90.0f);
        }else {
            self.serviceImageView.frame = CGRectMake(self.serviceImageView.frame.origin.x, self.serviceImageView.frame.origin.y, 108.0f, 108.0f);
        }
    }else {
        self.serviceImageView.frame = CGRectMake(self.serviceImageView.frame.origin.x, self.serviceImageView.frame.origin.y, self.serviceImageView.frame.size.width, self.serviceImageView.frame.size.height);
    }

    self.serviceName.font = [UIFont fontWithName:@"SegoeUI" size:15.0];
    [iBeautyUtility setimageViewRound:self.serviceImageView];

}

@end
