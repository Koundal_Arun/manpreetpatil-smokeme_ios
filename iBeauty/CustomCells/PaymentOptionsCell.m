//
//  PaymentOptionsCell.m
//  iBeauty
//
//  Created by App Innovation on 31/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "PaymentOptionsCell.h"

@implementation PaymentOptionsCell

- (void)configurePaymentOptionCell:(PaymentOptions*)paymentMethodInfo index:(NSInteger)indexValue pickUpStatus:(NSInteger)pickStatus {
    
    if (indexValue == 0) {
        self.paymentOptionLabel.text = NSLocalizedString(@"Pay in cash",@"");
        self.currencyIcon.image = [UIImage imageNamed:@"dollar"];
    }else {
        self.paymentOptionLabel.text = NSLocalizedString(@"Pay via Card",@"");
        self.currencyIcon.image = [UIImage imageNamed:@"card_icon"];
    }

//    self.paymentOptionLabel.text = paymentMethodInfo.name;
    
}


@end
