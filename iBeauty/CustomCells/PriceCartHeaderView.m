//
//  PriceCartHeaderView.m
//  iBeauty
//
//  Created by App Innovation on 18/01/19.
//  Copyright © 2019 App Innovation. All rights reserved.
//

#import "PriceCartHeaderView.h"
#import "Constants.h"

@interface PriceCartHeaderView ()

@property (weak, nonatomic) IBOutlet UILabel *titleLable;
@property (weak, nonatomic) IBOutlet UILabel *titleValue;

@property (weak, nonatomic) IBOutlet UIView  *bkgView;

@end

@implementation PriceCartHeaderView

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)configureHeaderViewWithTitle:(BasketPriceSummary*)basketPriceInfo {
    
    NSString *subTotalStr = NSLocalizedString(@"Sub Total", @"");
    self.titleLable.text = [NSString stringWithFormat:@"%@ :",subTotalStr];
    self.titleValue.text = [NSString stringWithFormat:@"$%.2f",[basketPriceInfo.orderPrice floatValue]];
    
    NSString *languageCode =   [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];
    
    if ([languageCode isEqualToString:@"en"]) {
        self.titleLable.textAlignment = NSTextAlignmentLeft;
        self.titleValue.textAlignment = NSTextAlignmentRight;
    }else if ([languageCode isEqualToString:@"ar"]) {
        self.titleLable.textAlignment = NSTextAlignmentRight;
        self.titleValue.textAlignment = NSTextAlignmentLeft;
    }

}

- (void)configureHeaderViewWithTitle_products:(BasketPriceSummary*)basketPriceInfo {
    
    NSString *subTotalStr = NSLocalizedString(@"Sub Total", @"");
    self.titleLable.text = [NSString stringWithFormat:@"%@ :",subTotalStr];
    self.titleValue.text = [NSString stringWithFormat:@"$%.2f",[basketPriceInfo.orderPrice floatValue]];
    
    NSString *languageCode =   [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];
    
    if ([languageCode isEqualToString:@"en"]) {
        self.titleLable.textAlignment = NSTextAlignmentLeft;
        self.titleValue.textAlignment = NSTextAlignmentRight;
    }else if ([languageCode isEqualToString:@"ar"]) {
        self.titleLable.textAlignment = NSTextAlignmentRight;
        self.titleValue.textAlignment = NSTextAlignmentLeft;
    }
    
}



@end
