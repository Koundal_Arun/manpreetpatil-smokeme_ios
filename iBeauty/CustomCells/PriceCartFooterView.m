//
//  PriceCartFooterView.m
//  iBeauty
//
//  Created by App Innovation on 09/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "PriceCartFooterView.h"
#import "Constants.h"
#import "TaxInfoModel.h"

@interface PriceCartFooterView ()

@property (weak, nonatomic) IBOutlet UILabel *titleLable;

@property (weak, nonatomic) IBOutlet UILabel *titleValue;

@property (weak, nonatomic) IBOutlet UIView  *bkgView;


@end

@implementation PriceCartFooterView

- (void)configureFooterViewWithTitle:(BasketPriceSummary*)basketPriceInfo {
    
//    [self setUpBkgView];
    
    self.titleLable.text = [NSString stringWithFormat:@"%@ :",NSLocalizedString(@"Total", @"")];
    self.titleValue.text = [NSString stringWithFormat:@"$%.2f",[basketPriceInfo.totalPrice floatValue]];
    
    NSString *languageCode =   [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];

    if ([languageCode isEqualToString:@"en"]) {
        self.titleLable.textAlignment = NSTextAlignmentLeft;
    }else if ([languageCode isEqualToString:@"ar"]) {
        self.titleLable.textAlignment = NSTextAlignmentRight;
    }
    
}

- (void)setUpBkgView {
    
    self.bkgView.translatesAutoresizingMaskIntoConstraints = YES;
    self.bkgView.frame = CGRectMake(self.bkgView.frame.origin.x, self.bkgView.frame.origin.y, SCREEN_WIDTH-16.0f, self.bkgView.frame.size.height);
    
}


@end
