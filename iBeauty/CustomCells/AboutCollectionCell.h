//
//  AboutCollectionCell.h
//  iBeauty
//
//  Created by App Innovation on 03/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "BusinessReviews.h"

@interface AboutCollectionCell : UICollectionViewCell

-(void)configureAboutCollectionCell:(BusinessReviews*)reviewsInfo;


@end
