//
//  OffersCollectionCell.h
//  iBeauty
//
//  Created by App Innovation on 23/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OffersDetail.h"

@interface OffersCollectionCell : UICollectionViewCell

@property (nonatomic,weak) IBOutlet UIButton *readMoreButton;
@property (nonatomic,weak) IBOutlet UIButton *viewDetailButton;

- (void)configureOffersCollectionCell:(OffersDetail*)infoObj;

@end
