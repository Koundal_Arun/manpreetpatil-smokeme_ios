//
//  CityInfoCell.m
//  iBeauty
//
//  Created by App Innovation on 18/12/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "CityInfoCell.h"

@implementation CityInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)configureCellWithCityInfo:(NSString*)cityName {
    
    self.cityLabel.text = cityName;
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
