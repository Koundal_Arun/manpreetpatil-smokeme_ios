//
//  AllMassagesCell.m
//  iBeauty
//
//  Created by App Innovation on 28/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "AllMassagesCell.h"
#import "NSDate+iBeautyDateFormatter.h"
#import "NSString+iBeautyString.h"
#import "ProvideServiceAt.h"
#import <HCSStarRatingView/HCSStarRatingView.h>

@interface AllMassagesCell ()

@property (nonatomic,weak) IBOutlet UIImageView *categoryImageView;
@property (nonatomic,weak) IBOutlet UILabel *Lbl_categoryName;
@property (nonatomic,weak) IBOutlet UILabel *Lbl_distance;
@property (nonatomic,weak) IBOutlet UILabel *Lbl_Address;
@property (nonatomic,weak) IBOutlet UILabel *Lbl_reviews;
@property (nonatomic,weak) IBOutlet UIButton *home_Button;
@property (nonatomic,weak) IBOutlet UIButton *shop_Button;

@property (nonatomic,weak) IBOutlet UIView * view_Background;
@property (nonatomic,weak) IBOutlet UIView * view_provideAt;
@property (nonatomic,weak) IBOutlet UIView * view_Book;
@property (nonatomic,weak) IBOutlet UIView * view_containerView;

@property (nonatomic,strong ) IBOutlet HCSStarRatingView * ratingView;


@end


@implementation AllMassagesCell

- (void)awakeFromNib {
    
    [super awakeFromNib];
    // Initialization code
}

- (void)configureAllCategoryCell:(ServicesCategoriesInfo*)infoObj {
    
    self.Lbl_categoryName.text = infoObj.categoryName;
    [self.categoryImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",IMAGE_BASE_URL,infoObj.categoryImage]] placeholderImage:[UIImage imageNamed:@"No_Image"]];
    [iBeautyUtility setImageViewCornerRound:self.categoryImageView];
    NSString *distance ;
    
    if (infoObj.distance != nil) {
        distance = infoObj.distance;
    }else {
        distance = @"";
    }
    self.Lbl_Address.text = [NSString stringWithFormat:@"%@",infoObj.location];
    self.Lbl_distance.text = [NSString stringWithFormat:@"%@ Km",distance];
    [self setUpNameAddressLabelsHeight:infoObj];
    [self setUpStarRatingView:infoObj];
    [self setUpProvideServiceAtViews:infoObj];
    [iBeautyUtility setBookViewCornerRound:self.view_Book];
    self.view_Book.backgroundColor = [UIColor iBeautyThemeColor];
    //[self.view_Book.layer insertSublayer:[iBeautyUtility setGradientColor_Views:self.view_Book status:YES] atIndex:0];

}

- (void)setUpNameAddressLabelsHeight:(ServicesCategoriesInfo*)catInfoObj {
    
    self.Lbl_categoryName.translatesAutoresizingMaskIntoConstraints = YES;
    self.Lbl_Address.translatesAutoresizingMaskIntoConstraints = YES;
    self.view_containerView.translatesAutoresizingMaskIntoConstraints = YES;
    self.view_Background.translatesAutoresizingMaskIntoConstraints = YES;
    
    self.Lbl_categoryName.frame = CGRectMake(134.0f, 12.0f, SCREEN_WIDTH-220.0f, catInfoObj.newNameHeight);
    self.Lbl_Address.frame = CGRectMake(134.0f, 12.0f + catInfoObj.newNameHeight + 3.0f, SCREEN_WIDTH-142.0f, catInfoObj.newAddressHeight);
    self.view_containerView.frame = CGRectMake(134.0f, 12.0f + catInfoObj.newNameHeight + 3.0f + catInfoObj.newAddressHeight + 7.0f, SCREEN_WIDTH-134.0f, self.view_containerView.frame.size.height);
    self.view_Background.frame = CGRectMake(0.0f, 0.0f, SCREEN_WIDTH, 12.0f + catInfoObj.newNameHeight + 3.0f + catInfoObj.newAddressHeight + 7.0f + self.view_containerView.frame.size.height);

}

- (void)setUpReviews:(ServicesCategoriesInfo*)catInfoObj {
    
    NSString *reviewText;

    if (catInfoObj.reviewsMeta != nil && ![[catInfoObj.reviewsMeta valueForKey:@"Reviews"] isEqual:[NSNull null]]) {

        NSInteger reviews = [[catInfoObj.reviewsMeta valueForKey:@"Reviews"]integerValue];
        
        if (reviews > 1) {
            reviewText = NSLocalizedString(@"Reviews", @"");
            self.Lbl_reviews.text = [NSString stringWithFormat:@"%ld %@",(long)reviews,reviewText];
        }else if (reviews == 1) {
            reviewText = NSLocalizedString(@"Review", @"");
            self.Lbl_reviews.text = [NSString stringWithFormat:@"%ld %@",(long)reviews,reviewText];
        }else {
            reviewText = NSLocalizedString(@"Review", @"");
            self.Lbl_reviews.text = [NSString stringWithFormat:@"0 %@",reviewText];
        }
    }else {
        reviewText = NSLocalizedString(@"Review", @"");
        self.Lbl_reviews.text = [NSString stringWithFormat:@"0 %@",reviewText];
    }
    
}

- (void)setUpStarRatingView:(ServicesCategoriesInfo*)catInfoObj {
    
    if (catInfoObj.reviewsMeta != nil && ![[catInfoObj.reviewsMeta valueForKey:@"Rating"] isEqual:[NSNull null]]) {
        
        [self.ratingView setHidden:NO];
        CGFloat ratingValue = [[catInfoObj.reviewsMeta valueForKey:@"Rating"]floatValue];
        self.ratingView.userInteractionEnabled = false;

        self.ratingView.value = ratingValue;
        self.ratingView.maximumValue = 5;
        self.ratingView.minimumValue = 0;
        self.ratingView.tintColor = [UIColor iBeautyThemeColor];
        self.ratingView.allowsHalfStars = YES;
        self.ratingView.accurateHalfStars = YES;
        self.ratingView.filledStarImage = [[UIImage imageNamed:@"star"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        self.ratingView.emptyStarImage = [[UIImage imageNamed:@"star_grey"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];

    }else {
        [self.ratingView setHidden:YES];
    }
    
}

- (void)setUpProvideServiceAtViews:(ServicesCategoriesInfo*)catInfoObj {
    
    [self setUpViewForLocalization];

    [iBeautyUtility setButtonCorderRound:self.home_Button];
    [iBeautyUtility setButtonCorderRound:self.shop_Button];
    [self.home_Button setHidden:YES];
    [self.shop_Button setHidden:YES];
    [self.home_Button setTranslatesAutoresizingMaskIntoConstraints:YES];
    [self.shop_Button setTranslatesAutoresizingMaskIntoConstraints:YES];
    self.home_Button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    self.shop_Button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;

    NSMutableArray *provideServiceAt = catInfoObj.provideServiceAt;
    
    NSString *languageCode =   [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];

    if (provideServiceAt.count == 1) {
        
        ProvideServiceAt * provideServiceObj = provideServiceAt[0];
        
        if ( [provideServiceObj.name isEqualToString:@"Home"]) {
            [self.home_Button setHidden:NO];
            [self showOnlyHomeServiceProvideLabel:languageCode];
        }else {
            [self.shop_Button setHidden:NO];
            [self showOnlyShopServiceProvideLabel:languageCode];
        }
        
    }else if (provideServiceAt.count == 2) {
        
        [self.home_Button setHidden:NO];
        [self.shop_Button setHidden:NO];
        [self showBothHomeShopServiceProvideLabel:languageCode];
    }
    
}

- (void)showOnlyShopServiceProvideLabel:(NSString*)languageCode {
    
    if ([languageCode isEqualToString:@"en"]) {
        
        self.shop_Button.frame = CGRectMake(0, self.shop_Button.frame.origin.y, 50.0f, self.shop_Button.frame.size.height);
        
    }else if ([languageCode isEqualToString:@"ar"]) {
        
        if (IS_IPHONE_X || IS_IPHONE_XS_MAX) {
            self.shop_Button.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-118-16.0f-55.0f-15.0f, self.shop_Button.frame.origin.y, 50.0f, self.shop_Button.frame.size.height);
        }else {
            self.shop_Button.frame = CGRectMake(62.0f, self.shop_Button.frame.origin.y, 50.0f, self.shop_Button.frame.size.height);
        }
    }
    
}

- (void)showOnlyHomeServiceProvideLabel:(NSString*)languageCode {
    
    if ([languageCode isEqualToString:@"en"]) {
        
        self.home_Button.frame = CGRectMake(0, self.home_Button.frame.origin.y, 50.0f, self.home_Button.frame.size.height);
        
    }else if ([languageCode isEqualToString:@"ar"]) {
        
        if (IS_IPHONE_X || IS_IPHONE_XS_MAX) {
            self.home_Button.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-118-16.0f-55.0f-15.0f, self.home_Button.frame.origin.y, 50.0f, self.home_Button.frame.size.height);
        }else {
            self.home_Button.frame = CGRectMake(62, self.home_Button.frame.origin.y, 50.0f, self.home_Button.frame.size.height);
        }
    }
    
}

- (void)showBothHomeShopServiceProvideLabel:(NSString*)languageCode {
    
    if ([languageCode isEqualToString:@"en"]) {
        
        self.home_Button.frame = CGRectMake(0, self.home_Button.frame.origin.y, 50.0f, self.home_Button.frame.size.height);
        self.shop_Button.frame = CGRectMake(62.0f, self.shop_Button.frame.origin.y, 50.0f, self.shop_Button.frame.size.height);
        
    }else if ([languageCode isEqualToString:@"ar"]) {
        
        if (IS_IPHONE_X || IS_IPHONE_XS_MAX) {
            
            self.home_Button.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-118-16.0f-55.0f-15.0f, self.home_Button.frame.origin.y, 50.0f, self.home_Button.frame.size.height);
            self.shop_Button.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-118-16.0f-55.0f-55.0f-15.0f, self.shop_Button.frame.origin.y, 50.0f, self.shop_Button.frame.size.height);

        }else {
            self.home_Button.frame = CGRectMake(0, self.home_Button.frame.origin.y, 50.0f, self.home_Button.frame.size.height);
            self.shop_Button.frame = CGRectMake(62.0f, self.shop_Button.frame.origin.y, 50.0f, self.shop_Button.frame.size.height);
        }
    }
}

- (void)setUpViewForLocalization {
    
    [self.home_Button setTitle:NSLocalizedString(@"Home", @"") forState:UIControlStateNormal];
    [self.shop_Button setTitle:NSLocalizedString(@"Shop", @"") forState:UIControlStateNormal];

}

- (NSDictionary*)getCurrentOpeningHoursDict:(ServicesCategoriesInfo*)infoObj {
    
    NSString *weekDay = [[NSString string] getWeekDay];
    NSDictionary *openingHourDict;
    for (NSDictionary *openingHoursDict in infoObj.StandardOpeningHours) {
        
        NSString *dayString = [openingHoursDict valueForKey:@"Dayname"];
        
        if ([dayString isEqualToString:weekDay]) {
            openingHourDict = openingHoursDict;
        }
    }
    return  openingHourDict;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

/*
 //[self setUpReviews:infoObj];
 
 //self.Lbl_ratings.textColor = [UIColor iBeautyThemeColor];
 
 //NSDictionary *openingHourDict = [self getCurrentOpeningHoursDict:infoObj];
 // [dateFormatter dateFromString:[openingHourDict valueForKey:@"Fromtime"]];
 //NSDate *endDate = [dateFormatter dateFromString:[openingHourDict valueForKey:@"Totime"]];
 
 //    NSString *finalString =  [NSString stringWithFormat:@"(%@-%@)",[infoObj.openingHours valueForKey:@"Fromtime"],[infoObj.openingHours valueForKey:@"Totime"]];  //[[NSString string] getOpengingHourString:infoObj.openingHours];
 //    NSString *openingTimeStr = [[NSDate date] getOpeningStringBasedOnStartAndEndTime:infoObj.openingHours format:SETTING_TIME_FORMAT_12];
 //    NSMutableAttributedString *openingHrString = [openingTimeStr getAttributedTextWithTimeString:finalString openingStatus:openingTimeStr];
 //self.Lbl_openingHours.attributedText = openingHrString;
 */

@end
