//
//  SelectStaffCollCell.h
//  iBeauty
//
//  Created by App Innovation on 04/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StaffDetail.h"

@interface SelectStaffCollCell : UICollectionViewCell

- (void)configureStaffCollectionCell:(StaffDetail*)infoObj;

@end
