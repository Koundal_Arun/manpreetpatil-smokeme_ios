//
//  FAQCellTableViewCell.h
//  iBeauty
//
//  Created by App Innovation on 06/11/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FAQInfo.h"

NS_ASSUME_NONNULL_BEGIN

@interface FAQCellTableViewCell : UITableViewCell

@property (nonatomic,weak) IBOutlet UILabel *answerLabel;

- (void)ConfigureCellWithAnswer:(FAQInfo*)infoObj;

@end

NS_ASSUME_NONNULL_END
