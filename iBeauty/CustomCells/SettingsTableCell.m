//
//  SettingsTableCell.m
//  iBeauty
//
//  Created by App Innovation on 28/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "SettingsTableCell.h"

@interface SettingsTableCell ()

@property (nonatomic,weak) IBOutlet UILabel *title;
@property (nonatomic,weak) IBOutlet UILabel *subTitle;
@property (nonatomic,weak) IBOutlet UISwitch *toggleSwitch;


@end


@implementation SettingsTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)configureSettingCell:(NSString*)title indexPath:(NSIndexPath*)indexPath language:(NSString*)language {
    
    if (indexPath.section == 0) {
        
        if (indexPath.row == 0) {
            [self.toggleSwitch setHidden:YES];
            [self.subTitle setHidden:NO];
            self.subTitle.text = NSLocalizedString(language,@"");
        }
//        else if (indexPath.row == 1) {
//            [self.toggleSwitch setHidden:YES];
//            [self.subTitle setHidden:NO];
//            self.subTitle.text = NSLocalizedString(@"USD",@"");
//        }
        else {
            [self.toggleSwitch setHidden:NO];
            [self.subTitle setHidden:YES];
        }
    }else {
        [self.toggleSwitch setHidden:YES];
        [self.subTitle setHidden:YES];
    }
    self.title.text = [NSString stringWithFormat:@"%@",NSLocalizedString(title,@"")];
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
