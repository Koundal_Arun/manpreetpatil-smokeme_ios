//
//  AllMassagesCell.h
//  iBeauty
//
//  Created by App Innovation on 28/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServicesCategoriesInfo.h"
#import "Constants.h"

@interface AllMassagesCell : UITableViewCell

- (void)configureAllCategoryCell:(ServicesCategoriesInfo*)infoObj;


@end
