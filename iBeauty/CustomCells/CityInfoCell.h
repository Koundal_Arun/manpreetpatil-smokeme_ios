//
//  CityInfoCell.h
//  iBeauty
//
//  Created by App Innovation on 18/12/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CityInfoModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CityInfoCell : UITableViewCell

@property (nonatomic,weak) IBOutlet UILabel *cityLabel;

- (void)configureCellWithCityInfo:(NSString*)cityName;


@end

NS_ASSUME_NONNULL_END
