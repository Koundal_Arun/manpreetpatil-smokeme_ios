//
//  SlotCollectionCell.m
//  iBeauty
//
//  Created by App Innovation on 05/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "SlotCollectionCell.h"
#import "Constants.h"

@implementation SlotCollectionCell


- (void)configureTimeSlotCollectionCell:(ServiceTimeSlot*)infoObj  {
    
    self.slotTimeLabel.text = infoObj.startTime;
    
    [iBeautyUtility selectSlotViewCornerRound:self.bgkView selectStatus:infoObj.selectStatus];

    if (infoObj.selectStatus == 1) {
        self.slotTimeLabel.textColor = [UIColor whiteColor];
    }else {
        self.slotTimeLabel.textColor = [UIColor blackColor];
    }
    
}


@end
