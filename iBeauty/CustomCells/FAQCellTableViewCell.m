//
//  FAQCellTableViewCell.m
//  iBeauty
//
//  Created by App Innovation on 06/11/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "FAQCellTableViewCell.h"
#import "UILabel+FlexibleWidHeight.h"
#import "Constants.h"

@implementation FAQCellTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)ConfigureCellWithAnswer:(FAQInfo*)infoObj {
    
    CGFloat answerHeight = [UILabel heightOfTextForString:infoObj.Answer andFont:[UIFont fontWithName:@"SegoeUI" size:14.0] maxSize:(CGSize){SCREEN_WIDTH-30, MAXFLOAT}];
    [self.answerLabel setTranslatesAutoresizingMaskIntoConstraints:YES];
    [self.answerLabel setFrame:CGRectMake(self.answerLabel.frame.origin.x, 10.0f, [UIScreen mainScreen].bounds.size.width - 30.0f, answerHeight)];
    
    self.answerLabel.text = infoObj.Answer;

}

@end
