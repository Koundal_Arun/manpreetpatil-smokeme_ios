//
//  PaymentOrderHeaderView.h
//  iBeauty
//
//  Created by App Innovation on 12/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaymentOrderHeaderView.h"
#import "PaymentOrderInfo.h"

@interface PaymentOrderHeaderView : UICollectionReusableView

@property (nonatomic,weak) IBOutlet UILabel  *bookingIdTitle;
@property (nonatomic,weak) IBOutlet UILabel  *bookingIdLabel;
@property (nonatomic,weak) IBOutlet UILabel  *finalAmountLabel;
@property (nonatomic,weak) IBOutlet UILabel  *dateLabel;

- (void)configurePaymentOrdersCollectionHeaderView:(PaymentOrderInfo *)orderInfo ;

@end
