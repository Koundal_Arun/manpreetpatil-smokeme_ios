//
//  CategoryTableHeaderView.h
//  iBeauty
//
//  Created by App Innovation on 29/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KIImagePager.h"
#import "Constants.h"
#import "PromotionalAds.h"

@interface CategoryTableHeaderView : UIView<KIImagePagerDelegate,KIImagePagerDataSource>

@property (nonatomic,weak) IBOutlet UIView *view_Background;
@property (nonatomic,strong) IBOutlet KIImagePager *imagePager;
@property (nonatomic,strong) NSMutableArray *pagerImages;
@property (nonatomic,strong) NSMutableArray *promotionalInfoArray;

+ (instancetype)headerView:(NSMutableArray*)images promotionalInfo:(NSMutableArray*)promotionalArray;

@end
