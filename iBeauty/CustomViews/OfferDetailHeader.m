//
//  OfferDetailHeader.m
//  iBeauty
//
//  Created by App Innovation on 11/09/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "OfferDetailHeader.h"
#import "UIImageView+WebCache.h"
#import "Constants.h"

@interface OfferDetailHeader ()

@property (nonatomic,weak) IBOutlet UIImageView *offerSerImageView;
@property (nonatomic,weak) IBOutlet UILabel *serviceName;
@property (nonatomic,weak) IBOutlet UILabel *priceLabel;
@property (nonatomic,weak) IBOutlet UILabel *discountLabel;
@property (nonatomic,weak) IBOutlet UILabel *backLabel;
@property (nonatomic,weak) IBOutlet UIImageView *backImageView;
@property (nonatomic,weak) IBOutlet UILabel *underlineLabel;
@property (nonatomic,weak) IBOutlet UILabel *servicesIncludedLabel;
@property (nonatomic,weak) IBOutlet UILabel *underlineLabel0;

@property (nonatomic,weak) IBOutlet UILabel *serviceProvider;
@property (nonatomic,weak) IBOutlet UILabel *providerAddress;
@property (nonatomic,weak) IBOutlet UILabel *serviceProviderTitle;

@end

@implementation OfferDetailHeader

- (void)configureOfferDetailHeaderView:(OffersDetail*)offerInfo screenStatus:(BOOL)status serviceCategoriesInfo:(ServicesCategoriesInfo*)catInfo {
    
    self.backLabel.translatesAutoresizingMaskIntoConstraints = YES;
    self.underlineLabel0.translatesAutoresizingMaskIntoConstraints = YES;
    self.underlineLabel0.frame = CGRectMake(self.underlineLabel0.frame.origin.x, self.underlineLabel0.frame.origin.y, SCREEN_WIDTH, 1.0f);

    if (catInfo) {
        self.serviceProvider.text = catInfo.categoryName;
        if (catInfo.location.length > 0) {
            self.providerAddress.hidden = NO;
            self.providerAddress.text = catInfo.location;
        }else {
            self.providerAddress.text = @"";
            self.providerAddress.hidden = YES;
        }
    }else {
        self.serviceProvider.text = offerInfo.providerName;
        if (offerInfo.providerAddress.length > 0) {
            self.providerAddress.hidden = NO;
            self.providerAddress.text = offerInfo.providerAddress;
        }else {
            self.providerAddress.text = @"";
            self.providerAddress.hidden = YES;
        }
    }

     if (status == NO) {
         self.addCartButton.hidden = YES;
     }else {
         self.addCartButton.hidden = NO;
     }

    self.backImageView.hidden = NO;
    self.backLabel.frame = CGRectMake(43.0f, self.backLabel.frame.origin.y, 45.0f, self.backLabel.frame.size.height);
    
    [self.offerSerImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",IMAGE_BASE_URL,offerInfo.imageUrl]] placeholderImage:[UIImage imageNamed:@"image3.png"]];
    self.serviceName.text = offerInfo.offerTitle;
    
    NSString *totalPriceString;
    NSString *finalDiscountString;
    
    self.servicesIncludedLabel.translatesAutoresizingMaskIntoConstraints = YES;

    if ([offerInfo.discountType isEqualToString:@"Flat"]) {
        
        self.priceLabel.hidden = YES;
        self.discountLabel.hidden = YES;
        self.underlineLabel.hidden = YES;
        self.servicesIncludedLabel.frame = CGRectMake(self.servicesIncludedLabel.frame.origin.x, self.servicesIncludedLabel.frame.origin.y, self.servicesIncludedLabel.frame.size.width, self.servicesIncludedLabel.frame.size.height);

    }else {
        self.priceLabel.hidden = NO;
        self.discountLabel.hidden = NO;
        self.underlineLabel.hidden = NO;
        
        self.servicesIncludedLabel.frame = CGRectMake(self.servicesIncludedLabel.frame.origin.x, self.servicesIncludedLabel.frame.origin.y, self.servicesIncludedLabel.frame.size.width, self.servicesIncludedLabel.frame.size.height);
        
        totalPriceString = [NSString stringWithFormat:@"$%@",offerInfo.totalPrice];
        finalDiscountString = [NSString stringWithFormat:@"%@ (%ld%% off)",totalPriceString,(long)offerInfo.discountValue];
        
        NSRange range = [finalDiscountString rangeOfString:totalPriceString];
        NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:finalDiscountString];
        [attributeString addAttribute:NSStrikethroughStyleAttributeName
                                value:@2 range:NSMakeRange(range.location, [totalPriceString length])];
        
        self.discountLabel.attributedText = attributeString;
        self.priceLabel.text = [NSString stringWithFormat:@"$%@",offerInfo.discountPrice];
    }

    [self setUpViews];
}

- (void)setUpViews {
    
    [iBeautyUtility setButtonCorderRound:self.addCartButton];
    //[self.addCartButton.layer insertSublayer:[iBeautyUtility setGradientColor_Buttons:self.addCartButton] atIndex:0];

    self.backLabel.text = NSLocalizedString(@"Back",@"");
    
    NSString *languageCode =   [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];
    
    if ([languageCode isEqualToString:@"en"]) {
        self.servicesIncludedLabel.textAlignment = NSTextAlignmentLeft;
    }else if ([languageCode isEqualToString:@"ar"]) {
        self.servicesIncludedLabel.textAlignment = NSTextAlignmentRight;
    }
    
    self.servicesIncludedLabel.text = NSLocalizedString(@"Services Included",@"");
    self.serviceProviderTitle.text = NSLocalizedString(@"Service Provider",@"");

}


@end
