//
//  ProfileCollectionHeader.m
//  iBeauty
//
//  Created by App Innovation on 10/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "ProfileCollectionHeader.h"
#import "Constants.h"
#import "UIImageView+WebCache.h"
#import "iBeautyUtility.h"

@interface ProfileCollectionHeader ()

@property (nonatomic,weak) IBOutlet UILabel *nameLabel;
@property (nonatomic,weak) IBOutlet UILabel *emailLabel;
@property (nonatomic,weak) IBOutlet UILabel *phoneNumberLabel;
@property (nonatomic,weak) IBOutlet UIImageView *profileImageView;


@end

@implementation ProfileCollectionHeader

- (void)configureProfileCollectionHeaderView:(NSDictionary *)infoDict {
    
    self.nameLabel.text = [infoDict valueForKey:KProfileName];
    self.emailLabel.text = [infoDict valueForKey:KEmailId];
    if (![[infoDict valueForKey:KPhoneNumber] isEqualToString:@"(null)"]) {
        self.phoneNumberLabel.text = [infoDict valueForKey:KPhoneNumber];
    }else {
        self.phoneNumberLabel.text = @"";
    }
    [self.profileImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[infoDict valueForKey:KProfileImageUrl]]] placeholderImage:[UIImage imageNamed:@"image_placeholder"]];
    [iBeautyUtility setimageViewRound:self.profileImageView];
    
    self.savedAddressLabel.text = NSLocalizedString(@"Saved Addresses", @"");

}

@end
