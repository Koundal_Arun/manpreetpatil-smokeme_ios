//
//  AppointmentDetailHeader.h
//  iBeauty
//
//  Created by App Innovation on 13/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AllAppointmentsInfo.h"

@interface AppointmentDetailHeader : UICollectionReusableView

@property (nonatomic,weak) IBOutlet UIButton *getDirectionButton;

- (void)configureAppointmentDetailHeaderView:(AllAppointmentsInfo*)appointmentInfo;

@end
