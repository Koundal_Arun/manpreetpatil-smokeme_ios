//
//  ProfileCollectionHeader.h
//  iBeauty
//
//  Created by App Innovation on 10/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"

@interface ProfileCollectionHeader : UICollectionReusableView

@property (nonatomic,weak) IBOutlet UIButton *editButton;
@property (nonatomic,weak) IBOutlet UIButton *changePassButton;
@property (nonatomic,weak) IBOutlet UILabel  *savedAddressLabel;
@property (nonatomic,weak) IBOutlet UIView   *passwordView;

- (void)configureProfileCollectionHeaderView:(NSDictionary *)infoDict;

@end
