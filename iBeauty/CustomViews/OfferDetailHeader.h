//
//  OfferDetailHeader.h
//  iBeauty
//
//  Created by App Innovation on 11/09/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OffersDetail.h"

@interface OfferDetailHeader : UICollectionReusableView
@property (nonatomic,weak) IBOutlet UIButton *backButton;
@property (nonatomic,weak) IBOutlet UIButton *addCartButton;

- (void)configureOfferDetailHeaderView:(OffersDetail*)offerInfo screenStatus:(BOOL)status serviceCategoriesInfo:(ServicesCategoriesInfo*)catInfo ;


@end
