//
//  OfferDetailFooter.m
//  iBeauty
//
//  Created by App Innovation on 11/09/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "OfferDetailFooter.h"
#import "NSDate+iBeautyDateFormatter.h"
#import "UILabel+FlexibleWidHeight.h"

@interface OfferDetailFooter ()

@property (nonatomic,weak) IBOutlet UILabel *validDateLabel;
@property (nonatomic,weak) IBOutlet UILabel *serviceProvider;
@property (nonatomic,weak) IBOutlet UILabel *descriptionLabel;
@property (nonatomic,weak) IBOutlet UILabel *descriptionTitle;
@property (nonatomic,weak) IBOutlet UILabel *providerAddress;

@end

@implementation OfferDetailFooter

- (void)configureOfferDetailFooterView:(OffersDetail*)offerInfo screenStatus:(BOOL)status serviceCategoriesInfo:(ServicesCategoriesInfo*)catInfo {
    
    NSDate *orderDate = [NSDate convertNSStringToNSDate:offerInfo.offerEndDateTime format:DATE_FORMAT_APPOINTMENT_DETAIL];
    NSString *dateString = [orderDate convertDateValueToNSString:PAYMENT_DATE_FORMAT localeStatus:false];
    NSString *validTill = NSLocalizedString(@"Valid till", @"");
    self.validDateLabel.text = [NSString stringWithFormat:@"%@ %@",validTill,dateString];
    
    CGFloat screenWidth = CGRectGetWidth([UIScreen mainScreen].bounds);
    NSString *description = offerInfo.detailDesc;
    NSString *termsCondition = offerInfo.termsAndConditions;

    CGFloat descHeight = [UILabel heightOfTextForString:description andFont:[UIFont fontWithName:@"SegoeUI" size:15.0] maxSize:(CGSize){screenWidth-35, MAXFLOAT}];
    CGFloat termsConditionHeight = [UILabel heightOfTextForString:termsCondition andFont:[UIFont fontWithName:@"SegoeUI" size:15.0] maxSize:(CGSize){screenWidth-35, MAXFLOAT}];

    if (description.length > 0) {
        self.descriptionTitle.hidden = NO;
        self.descriptionLabel.hidden = NO;
        self.descriptionTitle.text = NSLocalizedString(@"Description", @"");
        self.descriptionLabel.text = offerInfo.detailDesc;
        [self setUpDesciptionLabelFrame:offerInfo descHeight:descHeight];
    }else {
        if (termsCondition.length < 1) {
            self.descriptionTitle.hidden = YES;
            self.descriptionLabel.hidden = YES;
        }
    }
    
    if (termsCondition.length > 0) {
        self.descriptionTitle.hidden = NO;
        self.descriptionLabel.hidden = NO;
        self.descriptionTitle.text = NSLocalizedString(@"Terms & Conditions", @"");
        self.descriptionLabel.text = offerInfo.termsAndConditions;
        [self setUpDesciptionLabelFrame:offerInfo descHeight:termsConditionHeight];
    }else {
        if (description.length < 1) {
            self.descriptionTitle.hidden = YES;
            self.descriptionLabel.hidden = YES;
        }
    }
}

- (void)setUpDesciptionLabelFrame:(OffersDetail*)offerInfo descHeight:(CGFloat)descHeight {
    
    self.descriptionTitle.translatesAutoresizingMaskIntoConstraints = YES;
    self.descriptionLabel.translatesAutoresizingMaskIntoConstraints = YES;

    if (offerInfo.providerAddress.length > 0) {
        
        self.descriptionTitle.frame = CGRectMake(self.descriptionTitle.frame.origin.x, self.descriptionTitle.frame.origin.y, SCREEN_WIDTH-35.0f, 21.0f);
        self.descriptionLabel.frame = CGRectMake(self.descriptionLabel.frame.origin.x, self.descriptionLabel.frame.origin.y, SCREEN_WIDTH-35.0f, descHeight);
        
    }else {
        self.descriptionTitle.frame = CGRectMake(self.descriptionTitle.frame.origin.x, self.descriptionTitle.frame.origin.y, SCREEN_WIDTH-35.0f, 21.0f);
        self.descriptionLabel.frame = CGRectMake(self.descriptionLabel.frame.origin.x, self.descriptionLabel.frame.origin.y, SCREEN_WIDTH-35.0f, descHeight);
    }
}

@end
