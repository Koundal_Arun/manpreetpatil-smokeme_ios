//
//  OrderSummaryHeaderView.m
//  iBeauty
//
//  Created by App Innovation on 31/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "OrderSummaryHeaderView.h"
#import "Constants.h"

@implementation OrderSummaryHeaderView

- (void)configurePaymentOptionsHeaderView:(NSString*)headerName {
    
    self.orderSummaryLabel.text = headerName;
    [self makeLabelAlignmentAccordingToLanguage];
}

- (void)makeLabelAlignmentAccordingToLanguage {
    
    NSString *languageCode =   [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];
    
    if ([languageCode isEqualToString:@"en"]) {
        self.orderSummaryLabel.textAlignment = NSTextAlignmentLeft;
        self.minimumOrderLabel.textAlignment = NSTextAlignmentLeft;
    }else if ([languageCode isEqualToString:@"ar"]) {
        self.orderSummaryLabel.textAlignment = NSTextAlignmentRight;
        self.minimumOrderLabel.textAlignment = NSTextAlignmentRight;
    }
}

- (void)configurePaymentOptionsHeaderView_products:(NSString*)headerName deliveryType:(NSInteger)deliveryType amount:(NSString*)finalAmont {
    
    self.orderSummaryLabel.text = headerName;
    self.orderSummaryLabel.translatesAutoresizingMaskIntoConstraints = YES;
    if (deliveryType == 1) {
        
        NSInteger amount = [[NSString stringWithFormat:@"%@",finalAmont]integerValue];
        if (amount < 30) {
            self.minimumOrderLabel.hidden = NO;
            self.minimumOrderLabel.text = NSLocalizedString(@"Minimum order value for delivery : $30", @"");
        }else {
            self.minimumOrderLabel.hidden = YES;
        }
    }else {
        self.minimumOrderLabel.hidden = YES;
    }
    self.orderSummaryLabel.frame = CGRectMake(8.0f, 14.0f, self.orderSummaryLabel.frame.size.width, self.orderSummaryLabel.frame.size.height);
    
    [self makeLabelAlignmentAccordingToLanguage];

}


@end
