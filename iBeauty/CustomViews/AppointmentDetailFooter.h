//
//  AppointmentDetailFooter.h
//  iBeauty
//
//  Created by App Innovation on 13/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppointmentDetailFooter : UICollectionReusableView

@property (nonatomic,weak) IBOutlet UIButton *appointmentCancelButton;
@property (nonatomic,weak) IBOutlet UIView   *cancelView;


@end
