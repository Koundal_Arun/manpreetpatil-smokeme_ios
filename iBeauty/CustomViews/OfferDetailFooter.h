//
//  OfferDetailFooter.h
//  iBeauty
//
//  Created by App Innovation on 11/09/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OffersDetail.h"
#import "ServicesCategoriesInfo.h"

@interface OfferDetailFooter : UICollectionReusableView


- (void)configureOfferDetailFooterView:(OffersDetail*)offerInfo screenStatus:(BOOL)status serviceCategoriesInfo:(ServicesCategoriesInfo*)catInfo ;


@end
