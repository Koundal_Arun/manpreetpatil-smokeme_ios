//
//  AppointmentDetailHeader.m
//  iBeauty
//
//  Created by App Innovation on 13/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "AppointmentDetailHeader.h"
#import "UIImageView+WebCache.h"
#import "iBeautyUtility.h"
#import "ProviderImages.h"
#import "Constants.h"
#import "NSString+iBeautyString.h"

@interface AppointmentDetailHeader ()

@property (nonatomic,weak) IBOutlet UIImageView *providerImageView;
@property (nonatomic,weak) IBOutlet UILabel *bookingIdLabel;
@property (nonatomic,weak) IBOutlet UILabel *priceLabel;
@property (nonatomic,weak) IBOutlet UILabel *activeStatusLabel;
@property (nonatomic,weak) IBOutlet UILabel *providerName;
@property (nonatomic,weak) IBOutlet UILabel *provideAddress;
@property (nonatomic,weak) IBOutlet UIView *addressContainerView;
@property (nonatomic,weak) IBOutlet UILabel *servicesIncludedLabel;

@end

@implementation AppointmentDetailHeader

- (void)configureAppointmentDetailHeaderView:(AllAppointmentsInfo*)appointmentInfo {
    
//    NSMutableArray *providerImages = appointmentInfo.providerImageArray;
//    ProviderImages *imagesInfo = providerImages[0];
    
    [self.providerImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",IMAGE_BASE_URL,appointmentInfo.businessThumb]] placeholderImage:[UIImage imageNamed:@""]];
    
    NSString *bookingId = [NSString stringWithFormat:@"%@ - ",NSLocalizedString(@"Booking Id", @"")];
    NSMutableAttributedString *bookingIdString = [appointmentInfo.orderId getAppointmentBookingAttributedTextWithId:bookingId openingStatus:appointmentInfo.orderId];
    self.bookingIdLabel.attributedText = bookingIdString;
    
    self.priceLabel.text = [NSString stringWithFormat:@"$%.2f",[appointmentInfo.totalPrice floatValue]];
    [self setAddressContainerFrame:appointmentInfo];
    self.providerName.text = appointmentInfo.providerName;
    self.provideAddress.text = appointmentInfo.address;
    [iBeautyUtility setLabelRound:self.activeStatusLabel];
    
}

- (void)setAddressContainerFrame:(AllAppointmentsInfo*)appointmentInfo {
    
    self.addressContainerView.translatesAutoresizingMaskIntoConstraints = YES;
    self.providerName.translatesAutoresizingMaskIntoConstraints = YES;
    self.provideAddress.translatesAutoresizingMaskIntoConstraints = YES;
    self.servicesIncludedLabel.translatesAutoresizingMaskIntoConstraints = YES;

    self.providerName.frame = CGRectMake(self.providerName.frame.origin.x, 50.0f, SCREEN_WIDTH-110, appointmentInfo.providerNameHeight);
    
    self.provideAddress.frame = CGRectMake(self.provideAddress.frame.origin.x, 50.0f +appointmentInfo.providerNameHeight + 5.0f, SCREEN_WIDTH-110, appointmentInfo.providerAddressHeight);
    self.addressContainerView.frame = CGRectMake(self.addressContainerView.frame.origin.x, 207.0f , SCREEN_WIDTH-20, 50.0f + appointmentInfo.providerNameHeight + 5.0f + appointmentInfo.providerAddressHeight + 10.0f);
    self.servicesIncludedLabel.frame = CGRectMake(self.servicesIncludedLabel.frame.origin.x, 207.0f + self.addressContainerView.frame.size.height + 10.0f , self.servicesIncludedLabel.frame.size.width , 21.0f);

}

@end
