//
//  PaymentOrderHeaderView.m
//  iBeauty
//
//  Created by App Innovation on 12/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "PaymentOrderHeaderView.h"
#import "NSDate+iBeautyDateFormatter.h"

@implementation PaymentOrderHeaderView

- (void)configurePaymentOrdersCollectionHeaderView:(PaymentOrderInfo *)orderInfo {
    
    self.bookingIdTitle.text = NSLocalizedString(@"Booking Id", @"");
    
    self.bookingIdLabel.text = orderInfo.orderId;
    self.finalAmountLabel.text = [NSString stringWithFormat:@"$%.2f",[orderInfo.totalPrice floatValue]];
    NSString *dateString = [[NSDate date] convertDateValueToNSString:SLOT_DATE_FORMAT localeStatus:false];
    self.dateLabel.text = dateString;
    [self setViewAccordingToLanguageCode];
}

- (void)setViewAccordingToLanguageCode {
    
    NSString *languageCode =   [[NSUserDefaults standardUserDefaults] valueForKey:kLMSelectedLanguageKey];
    
    if ([languageCode isEqualToString:@"en"]) {
        self.bookingIdLabel.textAlignment = NSTextAlignmentRight;
        self.finalAmountLabel.textAlignment = NSTextAlignmentRight;

    }else if ([languageCode isEqualToString:@"ar"]) {
        self.bookingIdLabel.textAlignment = NSTextAlignmentLeft;
        self.finalAmountLabel.textAlignment = NSTextAlignmentLeft;
    }
    
}


@end
