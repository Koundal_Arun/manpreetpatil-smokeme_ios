//
//  ProviderCollectionHeaderView.h
//  iBeauty
//
//  Created by App Innovation on 23/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServicesCategoriesInfo.h"
#import "KIImagePager.h"
#import "ProviderImages.h"

@interface ProviderCollectionHeaderView : UICollectionReusableView<KIImagePagerDelegate,KIImagePagerDataSource>

@property (nonatomic,weak) IBOutlet UISegmentedControl *segmentControl;
@property (nonatomic,weak) IBOutlet UIButton *backButton;

@property (nonatomic,strong) IBOutlet KIImagePager *imagePager;
@property (nonatomic,strong) NSMutableArray *providerImages;
@property (nonatomic,strong) NSMutableArray *providerAllInfo;

- (void)configureProviderCollectionHeaderView:(ServicesCategoriesInfo *)categoryInfoObj distanceString:(NSString*)distance;

- (void)configureProductDetailCollectionHeaderView:(ServicesCategoriesInfo *)categoryInfoObj distanceString:(NSString*)distance;


@end
