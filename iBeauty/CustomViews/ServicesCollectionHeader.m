//
//  ServicesCollectionHeader.m
//  iBeauty
//
//  Created by App Innovation on 10/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "ServicesCollectionHeader.h"

@implementation ServicesCollectionHeader


- (void)configureCollectionHeaderView {
    
    self.imagePager.pageControl.currentPageIndicatorTintColor = [UIColor whiteColor];
    self.imagePager.pageControl.pageIndicatorTintColor = [UIColor blackColor];
    self.imagePager.slideshowTimeInterval = 5.0f;
    self.imagePager.slideshowShouldCallScrollToDelegate = YES;
    [iBeautyUtility setViewBorderRound:self.view_Background];
    [self.imagePager reloadData];
}


@end
