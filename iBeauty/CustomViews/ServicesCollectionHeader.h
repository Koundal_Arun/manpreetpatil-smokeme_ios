//
//  ServicesCollectionHeader.h
//  iBeauty
//
//  Created by App Innovation on 10/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KIImagePager.h"
#import "Constants.h"

@interface ServicesCollectionHeader : UICollectionReusableView

@property (nonatomic,weak) IBOutlet UIView *view_Background;
@property (nonatomic,strong) IBOutlet KIImagePager *imagePager;
@property (nonatomic,weak) IBOutlet UISegmentedControl *segmentControl;

- (void)configureCollectionHeaderView;


@end
