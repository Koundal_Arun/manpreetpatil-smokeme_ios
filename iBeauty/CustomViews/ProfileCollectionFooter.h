//
//  ProfileCollectionFooter.h
//  iBeauty
//
//  Created by App Innovation on 10/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileCollectionFooter : UICollectionReusableView

@property (nonatomic,weak) IBOutlet UIButton *addNewAddressButton;

@end
