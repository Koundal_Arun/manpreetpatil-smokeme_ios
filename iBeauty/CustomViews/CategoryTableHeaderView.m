//
//  CategoryTableHeaderView.m
//  iBeauty
//
//  Created by App Innovation on 29/06/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "CategoryTableHeaderView.h"

@implementation CategoryTableHeaderView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

+ (instancetype)headerView:(NSMutableArray*)images promotionalInfo:(NSMutableArray*)promotionalArray {
    
    CategoryTableHeaderView *tableHeaderView = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil
                           
                                                                            options:nil] firstObject];
    
    tableHeaderView.imagePager.pageControl.currentPageIndicatorTintColor = [UIColor whiteColor];
    tableHeaderView.imagePager.pageControl.pageIndicatorTintColor = [UIColor blackColor];
    tableHeaderView.imagePager.slideshowTimeInterval = 5.0f;
    tableHeaderView.imagePager.slideshowShouldCallScrollToDelegate = YES;
    [iBeautyUtility setViewBorderRound:tableHeaderView.view_Background];
    tableHeaderView.pagerImages = images;
    [tableHeaderView.imagePager reloadData];
    tableHeaderView.promotionalInfoArray = promotionalArray;
    
    CGRect headerFrame = tableHeaderView.frame;
    
    NSString *deviceType = [UIDevice currentDevice].model;
    if ([deviceType isEqualToString:@"iPhone"]) {
        headerFrame.size.height = 220.0f;
    }else {
        headerFrame.size.height = 350.0f;
    }
    
    tableHeaderView.frame = headerFrame;

    return tableHeaderView;
}

#pragma mark - KIImagePager DataSource

- (NSArray *) arrayWithImages:(KIImagePager*)pager {
    
    return  self.pagerImages;
    
//    return @[[UIImage imageNamed:@"image1"],[UIImage imageNamed:@"image2"],[UIImage imageNamed:@"image1"],[UIImage imageNamed:@"image2"]];
}

- (UIViewContentMode) contentModeForImage:(NSUInteger)image inPager:(KIImagePager *)pager {
    
    return UIViewContentModeScaleToFill;
}

- (NSArray*)arrayWithProviderInfo:(KIImagePager *)pager {
    
    return nil;
}

- (NSString *) captionForImageAtIndex:(NSInteger)index inPager:(KIImagePager *)pager {
    
    PromotionalAds *infoObj = self.promotionalInfoArray[index];
    NSString *captionStr = [NSString stringWithFormat:@"%@",infoObj.title];
    return captionStr;
}

#pragma mark - KIImagePager Delegate

- (void) imagePager:(KIImagePager *)imagePager didScrollToIndex:(NSUInteger)index {
    
    //NSLog(@"%s %lu", __PRETTY_FUNCTION__, (unsigned long)index);
}

- (void) imagePager:(KIImagePager *)imagePager didSelectImageAtIndex:(NSUInteger)index {
    
    //NSLog(@"%s %lu", __PRETTY_FUNCTION__, (unsigned long)index);
    NSDictionary *promotionIndexInfo = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:index],@"Index", nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"promotionalOfferIndex" object:nil userInfo:promotionIndexInfo];
}

@end
