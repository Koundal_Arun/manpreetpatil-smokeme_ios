//
//  ProviderCollectionHeaderView.m
//  iBeauty
//
//  Created by App Innovation on 23/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import "ProviderCollectionHeaderView.h"
#import "UIImageView+WebCache.h"
#import "NSString+iBeautyString.h"
#import "NSDate+iBeautyDateFormatter.h"
#import "ProvideServiceAt.h"
#import <HCSStarRatingView/HCSStarRatingView.h>
#import "UIColor+iBeauty.h"

@interface ProviderCollectionHeaderView ()


@property (nonatomic,weak) IBOutlet UIImageView *categoryImageView;
@property (nonatomic,weak) IBOutlet UILabel *Lbl_categoryName;
@property (nonatomic,weak) IBOutlet UILabel *Lbl_openingHours;
@property (nonatomic,weak) IBOutlet UILabel *Lbl_distance;
@property (nonatomic,weak) IBOutlet UILabel *Lbl_ratings;
@property (nonatomic,weak) IBOutlet UILabel *Lbl_reviews;
@property (nonatomic,weak) IBOutlet UIView * view_Ratings;
@property (nonatomic,weak) IBOutlet UIImageView * imageView_Ratings;
@property (nonatomic,weak) IBOutlet UIView * view_CategoryDetail;
@property (nonatomic,weak) IBOutlet UIButton * homeButton;
@property (nonatomic,weak) IBOutlet UIButton * shopButton;

@property (nonatomic,strong ) IBOutlet HCSStarRatingView * ratingView;

@end

@implementation ProviderCollectionHeaderView


#pragma mark - Service Provider Detail Header


- (void)configureProviderCollectionHeaderView:(ServicesCategoriesInfo *)categoryInfoObj distanceString:(NSString*)distance {
    
    if (APP_DEBUG_MODE)
        NSLog(@"Provider image is %@",categoryInfoObj.categoryImage);
    
    [self getProviderImgaes:categoryInfoObj];
    
//    [self.categoryImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",IMAGE_BASE_URL,categoryInfoObj.categoryImage]] placeholderImage:[UIImage imageNamed:@""]];
    
    self.Lbl_categoryName.text = categoryInfoObj.categoryName;
    
    if (distance.length > 0) {
        distance = [NSString stringWithFormat:@"(%@ Km)",distance];
    }else {
        distance = @"";
    }
    NSMutableAttributedString *distanceString = [distance getAttributedTextWithTimeString:categoryInfoObj.location openingStatus:distance];
    
    self.Lbl_distance.attributedText = distanceString;
    
//    if (categoryInfoObj.reviewsMeta != nil && ![[categoryInfoObj.reviewsMeta valueForKey:@"Rating"] isEqual:[NSNull null]]) {
//        [self.imageView_Ratings setHidden:NO];
//        self.Lbl_ratings.text = [NSString stringWithFormat:@"%@",[categoryInfoObj.reviewsMeta valueForKey:@"Rating"]];
//    }else {
//        self.Lbl_ratings.text = @"";
//        [self.imageView_Ratings setHidden:YES];
//    }
    [self setUpStarRatingView:categoryInfoObj];
    self.Lbl_ratings.textColor = [UIColor iBeautyThemeColor];
    
    NSString *currentDay = [[NSString string] getWeekDay];
    
    StandardOpeningHours *hoursInfoObj;
    NSMutableArray *hoursInfoArray = categoryInfoObj.StandardOpeningHours;
    if (hoursInfoArray.count > 0) {
        for (int i = 0; i < hoursInfoArray.count; i++) {
            StandardOpeningHours *infoObj = hoursInfoArray[i];
            if ([currentDay isEqualToString:infoObj.dayName]) {
                hoursInfoObj = infoObj;
            }
        }
    }
    
    NSString *finalString = [[NSString string] getOpengingHourStringFromModel:hoursInfoObj];
//    NSString *finalString =  [NSString stringWithFormat:@"(%@-%@)",[categoryInfoObj.openingHours valueForKey:@"Fromtime"],[categoryInfoObj.openingHours valueForKey:@"Totime"]];

    NSString *openingTimeStr = [[NSDate date] getOpeningStringBasedOnStartAndEndTimeForAllDaysOpeningTime:hoursInfoObj format:SETTING_TIME_FORMAT_24];
    NSMutableAttributedString *openingHrString = [openingTimeStr getAttributedTextWithTimeString:finalString openingStatus:openingTimeStr];
    self.Lbl_openingHours.attributedText = openingHrString;
    
    [self setReviewView:categoryInfoObj];
    [self setUpProvideServiceAtViews:categoryInfoObj];
    [self setUpSegmentControlTint];
}

- (void)setUpSegmentControlTint {

    if (@available(iOS 13.0, *)) {

        [self.segmentControl setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor], NSFontAttributeName: [UIFont fontWithName:@"SegoeUI" size:15.0]} forState:UIControlStateSelected];
        [self.segmentControl setSelectedSegmentTintColor:[UIColor iBeautyThemeColor]];
        
    } else {

        [self.segmentControl setTintColor:[UIColor iBeautyThemeColor]];
    }
    
}

#pragma mark - Star Rating View

- (void)setUpStarRatingView:(ServicesCategoriesInfo*)catInfoObj {
    
    [self.imageView_Ratings setHidden:YES];
    [self.Lbl_ratings setHidden:YES];
    
    if (catInfoObj.reviewsMeta != nil && ![[catInfoObj.reviewsMeta valueForKey:@"Rating"] isEqual:[NSNull null]]) {
        
        [self.ratingView setHidden:NO];
        CGFloat ratingValue = [[catInfoObj.reviewsMeta valueForKey:@"Rating"]floatValue];
        self.ratingView.userInteractionEnabled = false;

        self.ratingView.value = ratingValue;
        self.ratingView.maximumValue = 5;
        self.ratingView.minimumValue = 0;
        self.ratingView.tintColor = [UIColor iBeautyThemeColor];
        self.ratingView.allowsHalfStars = YES;
        self.ratingView.accurateHalfStars = YES;
        self.ratingView.filledStarImage = [[UIImage imageNamed:@"star"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        self.ratingView.emptyStarImage = [[UIImage imageNamed:@"star_grey"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        
    }else {
        [self.ratingView setHidden:YES];
    }
    
}

#pragma mark - Review View

- (void)setReviewView:(ServicesCategoriesInfo*)catInfoObj {
    
    NSString *reviewText;
    
    if (catInfoObj.reviewsCount<1) {
//        reviewText = NSLocalizedString(@"Review", @"");
//        self.Lbl_reviews.text =  [NSString stringWithFormat:@"0 %@",reviewText];
        self.Lbl_reviews.text =  @"";
    }else {
        if (catInfoObj.reviewsCount == 1) {
            reviewText = NSLocalizedString(@"Review", @"");
            self.Lbl_reviews.text = [NSString stringWithFormat:@"%ld %@",(long)catInfoObj.reviewsCount,reviewText];
        }else {
            reviewText = NSLocalizedString(@"Reviews", @"");
            self.Lbl_reviews.text = [NSString stringWithFormat:@"%ld %@",(long)catInfoObj.reviewsCount,reviewText];
        }
    }
    
}

#pragma mark - Provider Service At

- (void)setUpProvideServiceAtViews:(ServicesCategoriesInfo*)catInfoObj {
    
    [self setUpServiceProviderViewForLocalization];
    
    [iBeautyUtility setButtonCorderRound:self.homeButton];
    [iBeautyUtility setButtonCorderRound:self.shopButton];
    [self.homeButton setHidden:YES];
    [self.shopButton setHidden:YES];
    
    NSMutableArray *provideServiceAt = catInfoObj.provideServiceAt;
    
    for (ProvideServiceAt * provideServiceObj in provideServiceAt) {
        
        if ( [provideServiceObj.name isEqualToString:@"Home"]) {
            
            [self.homeButton setHidden:NO];
        }
        if ( [provideServiceObj.name isEqualToString:@"Shop"]) {
            
            [self.shopButton setHidden:NO];
        }
    }
    
}

- (void)setUpServiceProviderViewForLocalization {
    
    [self.homeButton setTitle:NSLocalizedString(@"Home", @"") forState:UIControlStateNormal];
    [self.shopButton setTitle:NSLocalizedString(@"Shop", @"") forState:UIControlStateNormal];
    
}


#pragma mark - Pager Images

-(void)getProviderImgaes:(ServicesCategoriesInfo*)categoryInfoObj {
    
    self.providerImages = [NSMutableArray new];
    self.providerAllInfo = [NSMutableArray new];
    self.providerAllInfo = categoryInfoObj.images;
    
    for (ProviderImages *imageObj in categoryInfoObj.images) {
        NSString *imageUrl = [NSString stringWithFormat:@"%@%@",IMAGE_BASE_URL,imageObj.Url];
        [self.providerImages addObject:imageUrl];
    }
    NSLog(@"Provider images are %@",self.providerImages);
    self.imagePager.pageControl.currentPageIndicatorTintColor = [UIColor whiteColor];
    self.imagePager.pageControl.pageIndicatorTintColor = [UIColor blackColor];
    self.imagePager.slideshowTimeInterval = 6.0f;
//    self.imagePager.imageCounterDisabled = YES;
    self.imagePager.slideshowShouldCallScrollToDelegate = YES;
    [self.imagePager reloadData];
}


#pragma mark - Product Detail Header

- (void)configureProductDetailCollectionHeaderView:(ServicesCategoriesInfo *)categoryInfoObj distanceString:(NSString*)distance {
    
    
    if (APP_DEBUG_MODE) { NSLog(@"Provider image is %@",categoryInfoObj.categoryImage); }
    
    [self getProviderImgaes:categoryInfoObj];
    
    self.Lbl_categoryName.text = categoryInfoObj.categoryName;
    
    if (distance.length > 0) {
        distance = [NSString stringWithFormat:@"(%@ Km)",distance];
    }else {
        distance = @"";
    }
    NSMutableAttributedString *distanceString = [distance getAttributedTextWithTimeString:categoryInfoObj.location openingStatus:distance];
    
    self.Lbl_distance.attributedText = distanceString;
    
    [self setUpStarRatingView:categoryInfoObj];
    self.Lbl_ratings.textColor = [UIColor iBeautyThemeColor];
    
    NSString *currentDay = [[NSString string] getWeekDay];
    
    StandardOpeningHours *hoursInfoObj;
    NSMutableArray *hoursInfoArray = categoryInfoObj.StandardOpeningHours;
    if (hoursInfoArray.count > 0) {
        for (int i = 0; i < hoursInfoArray.count; i++) {
            StandardOpeningHours *infoObj = hoursInfoArray[i];
            if ([currentDay isEqualToString:infoObj.dayName]) {
                hoursInfoObj = infoObj;
            }
        }
    }
    
    NSString *finalString = [[NSString string] getOpengingHourStringFromModel:hoursInfoObj];
    
    NSString *openingTimeStr = [[NSDate date] getOpeningStringBasedOnStartAndEndTimeForAllDaysOpeningTime:hoursInfoObj format:SETTING_TIME_FORMAT_24];
    NSMutableAttributedString *openingHrString = [openingTimeStr getAttributedTextWithTimeString:finalString openingStatus:openingTimeStr];
    self.Lbl_openingHours.attributedText = openingHrString;
    
    [self setReviewView:categoryInfoObj];
    [self setUpProductProvideAtViews:categoryInfoObj];
    
}

#pragma mark - Product Provide At

- (void)setUpProductProvideAtViews:(ServicesCategoriesInfo*)catInfoObj {
    
    [self setUpProductsViewForLocalization];
    
    [iBeautyUtility setButtonCorderRound:self.homeButton];
    [iBeautyUtility setButtonCorderRound:self.shopButton];
    [self.homeButton setHidden:YES];
    [self.shopButton setHidden:YES];
    
    NSMutableArray *provideServiceAt = catInfoObj.provideServiceAt;
    
    for (ProvideServiceAt * provideServiceObj in provideServiceAt) {
        
        if ( [provideServiceObj.name isEqualToString:@"Delivery"]) {
            
            [self.homeButton setHidden:NO];
        }
        if ( [provideServiceObj.name isEqualToString:@"Pickup"]) {
            
            [self.shopButton setHidden:NO];
        }
    }
    
}


- (void)setUpProductsViewForLocalization {
    
    [self.homeButton setTitle:NSLocalizedString(@"DELIVERY", @"") forState:UIControlStateNormal];
    [self.shopButton setTitle:NSLocalizedString(@"PICKUP", @"") forState:UIControlStateNormal];
    
}

#pragma mark - KIImagePager DataSource

- (NSArray *) arrayWithImages:(KIImagePager*)pager {
    
    return  self.providerImages;
    
    // return @[[UIImage imageNamed:@"image1"],[UIImage imageNamed:@"image2"],[UIImage imageNamed:@"image1"],[UIImage imageNamed:@"image2"]];
}

- (UIViewContentMode) contentModeForImage:(NSUInteger)image inPager:(KIImagePager *)pager {
    
    return UIViewContentModeScaleToFill;
}

- (NSArray*)arrayWithProviderInfo:(KIImagePager *)pager {
    
    return self.providerAllInfo;
}

- (NSString *) captionForImageAtIndex:(NSInteger)index inPager:(KIImagePager *)pager {
    
    return nil;
}

#pragma mark - KIImagePager Delegate

- (void) imagePager:(KIImagePager *)imagePager didScrollToIndex:(NSUInteger)index {
    
//    NSLog(@"%s %lu", __PRETTY_FUNCTION__, (unsigned long)index);
}

- (void) imagePager:(KIImagePager *)imagePager didSelectImageAtIndex:(NSUInteger)index {
    
//    NSLog(@"%s %lu", __PRETTY_FUNCTION__, (unsigned long)index);

}


@end
