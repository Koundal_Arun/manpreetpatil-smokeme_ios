//
//  OrderSummaryHeaderView.h
//  iBeauty
//
//  Created by App Innovation on 31/07/18.
//  Copyright © 2018 App Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderSummaryHeaderView : UICollectionReusableView

@property (nonatomic,weak) IBOutlet UILabel *orderSummaryLabel;
@property (nonatomic,weak) IBOutlet UILabel *minimumOrderLabel;

- (void)configurePaymentOptionsHeaderView:(NSString*)headerName;
- (void)configurePaymentOptionsHeaderView_products:(NSString*)headerName deliveryType:(NSInteger)deliveryType amount:(NSString*)finalAmont;

@end
